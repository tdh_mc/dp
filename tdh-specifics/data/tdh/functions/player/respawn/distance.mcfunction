# Cette fonction est exécutée à la position d'un joueur avec le tag (unique) pendingDeath
# - temp contient la distance du meilleur point de spawn trouvé jusqu'à présent
# - temp3 et temp4 contiennent les coordonnées X et Z du point de spawn à examiner
# - temp5 = l'ID du point de spawn à examiner, et temp2 = l'ID du meilleur point de spawn trouvé jusqu'à présent
# - temp6 et temp7 contiennent la position du joueur

# Après ce code, temp3 et temp4 contiendront la distance respectivement sur l'axe des X et l'axe des Z
scoreboard players set @p[tag=pendingDeath] temp8 -1
scoreboard players operation @p[tag=pendingDeath] temp3 -= @p[tag=pendingDeath] temp6
scoreboard players operation @p[tag=pendingDeath] temp4 -= @p[tag=pendingDeath] temp7
execute if score @p[tag=pendingDeath] temp3 matches ..-1 run scoreboard players operation @p[tag=pendingDeath] temp3 *= @p[tag=pendingDeath] temp8
execute if score @p[tag=pendingDeath] temp4 matches ..-1 run scoreboard players operation @p[tag=pendingDeath] temp4 *= @p[tag=pendingDeath] temp8

# Comme on ne se fait pas chier, on additionne la distance sur les 2 axes pour obtenir une "distance taxi" dans temp3
scoreboard players operation @p[tag=pendingDeath] temp3 += @p[tag=pendingDeath] temp4

# Si cette distance est inférieure à la distance la plus courte trouvée pour l'instant, on assigne les nouvelles valeurs
execute if score @p[tag=pendingDeath] temp3 < @p[tag=pendingDeath] temp run scoreboard players operation @p[tag=pendingDeath] temp2 = @p[tag=pendingDeath] temp5
execute if score @p[tag=pendingDeath] temp3 < @p[tag=pendingDeath] temp run scoreboard players operation @p[tag=pendingDeath] temp = @p[tag=pendingDeath] temp3

# On détermine le point de respawn
# Pour ce faire, on donne au joueur 2 variables temporaires :
# - temp représente la distance du joueur au point de spawn le plus proche trouvé jusqu'à présent
# - temp2 représente l'ID du point de spawn le plus proche trouvé jusqu'à présent
scoreboard players set @p[tag=pendingDeath] temp 999999
scoreboard players set @p[tag=pendingDeath] temp2 -1

# On utilise une fonction dont le fonctionnement est comme suit :
# - on remplit temp6 et temp7 avec la position du joueur
# - on remplit temp3 et temp4 par les coordonnées X et Z du point de spawn
# - on remplit temp5 = l'ID du point de spawn, et on utilise temp3/4 pour calculer la distance du joueur
# - si la distance calculée avec temp3/4 est <temp, on assigne cette distance et temp5 comme nouvelles valeurs de temp et temp2

# Initialisation de la position du joueur
execute store result score @p[tag=pendingDeath] temp6 run data get entity @p[tag=pendingDeath] Pos[0]
execute store result score @p[tag=pendingDeath] temp7 run data get entity @p[tag=pendingDeath] Pos[2]

# Point de spawn 1 [GLOBAL] - Entre Le Roustiflet et Litoréa
# 2063 / 77 / 2903 || 165 / 14
scoreboard players set @p[tag=pendingDeath] temp3 2063
scoreboard players set @p[tag=pendingDeath] temp4 2903
scoreboard players set @p[tag=pendingDeath] temp5 1
function tdh:player/respawn/distance

# Point de spawn 2 - Auberge du Donjon (entre Tolbrok et Néronil)
# 2131 / 64 / 1627 || 0 / 0
scoreboard players set @p[tag=pendingDeath] temp3 2131
scoreboard players set @p[tag=pendingDeath] temp4 1627
scoreboard players set @p[tag=pendingDeath] temp5 2
function tdh:player/respawn/distance

# Point de spawn 3 - Grenat Récif / Hôtel de Preskrik
# 32 / 69 / 1287 || -165 / 0
scoreboard players set @p[tag=pendingDeath] temp3 32
scoreboard players set @p[tag=pendingDeath] temp4 1287
scoreboard players set @p[tag=pendingDeath] temp5 3
function tdh:player/respawn/distance

# Point de spawn 4 - Grenat / Phare Port de Plaisance
# -673 / 64 / 1386 || 60 / 0
scoreboard players set @p[tag=pendingDeath] temp3 -673
scoreboard players set @p[tag=pendingDeath] temp4 1386
scoreboard players set @p[tag=pendingDeath] temp5 4
function tdh:player/respawn/distance

# Point de spawn 5 - Evrocq / Tour d'Evrocq
# 1657 / 83 / 4249 || 57 / 5
scoreboard players set @p[tag=pendingDeath] temp3 1657
scoreboard players set @p[tag=pendingDeath] temp4 4249
scoreboard players set @p[tag=pendingDeath] temp5 5
function tdh:player/respawn/distance

# Point de spawn 6 - XXMETROB
# 2061 / 72 / 5224 || 96 / 8
scoreboard players set @p[tag=pendingDeath] temp3 2061
scoreboard players set @p[tag=pendingDeath] temp4 5224
scoreboard players set @p[tag=pendingDeath] temp5 6
function tdh:player/respawn/distance

# Point de spawn 7 - Illysia / Croisement route 190
# -835 / 74 / 138 || -37 / 5
scoreboard players set @p[tag=pendingDeath] temp3 -835
scoreboard players set @p[tag=pendingDeath] temp4 138
scoreboard players set @p[tag=pendingDeath] temp5 7
function tdh:player/respawn/distance

# Point de spawn 8 - Ouest de Béothas / XXPresquIleA
# 395 / 70 / 1764
scoreboard players set @p[tag=pendingDeath] temp3 395
scoreboard players set @p[tag=pendingDeath] temp4 1764
scoreboard players set @p[tag=pendingDeath] temp5 8
function tdh:player/respawn/distance

# Point de spawn 9 - Auberge du Grand Large / Litoréa
# 2689 / 89 / 2358
scoreboard players set @p[tag=pendingDeath] temp3 2689
scoreboard players set @p[tag=pendingDeath] temp4 2358
scoreboard players set @p[tag=pendingDeath] temp5 9
function tdh:player/respawn/distance


# On spawne ensuite à l'endroit calculé comme le plus proche
execute if score @p[tag=pendingDeath] temp2 matches 1 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître à l'","color":"gold"},{"text":"Auberge du Profane Éméché","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 1 run spawnpoint @p[tag=pendingDeath] 2063 77 2903 165

execute if score @p[tag=pendingDeath] temp2 matches 2 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître à l'","color":"gold"},{"text":"Auberge du Donjon","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 2 run spawnpoint @p[tag=pendingDeath] 2131 64 1627

execute if score @p[tag=pendingDeath] temp2 matches 3 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître au ","color":"gold"},{"text":"Hôtel de Preskrik","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 3 run spawnpoint @p[tag=pendingDeath] 32 69 1287 -165

execute if score @p[tag=pendingDeath] temp2 matches 4 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître au ","color":"gold"},{"text":"Port Sud de Grenat","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 4 run spawnpoint @p[tag=pendingDeath] -673 64 1386 60

execute if score @p[tag=pendingDeath] temp2 matches 5 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître à la ","color":"gold"},{"text":"Tour d'Évrocq","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 5 run spawnpoint @p[tag=pendingDeath] 1657 83 4249 57

execute if score @p[tag=pendingDeath] temp2 matches 6 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître à l'","color":"gold"},{"text":"Auberge du Repu Rabougri","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 6 run spawnpoint @p[tag=pendingDeath] 2061 72 5224 96

execute if score @p[tag=pendingDeath] temp2 matches 7 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître à l'","color":"gold"},{"text":"Auberge d'Illysia","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 7 run spawnpoint @p[tag=pendingDeath] -835 74 138 -37

execute if score @p[tag=pendingDeath] temp2 matches 8 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître à l'","color":"gold"},{"text":"Auberge du Béothien Repenti","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 8 run spawnpoint @p[tag=pendingDeath] 395 70 1764

execute if score @p[tag=pendingDeath] temp2 matches 9 run tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître à l'","color":"gold"},{"text":"Auberge du Grand Large","color":"red"},{"text":".","color":"gold"}]
execute if score @p[tag=pendingDeath] temp2 matches 9 run spawnpoint @p[tag=pendingDeath] 2689 89 2358


# On supprime le tag temporaire et les variables temporaires
scoreboard players reset @p[tag=pendingDeath] temp
scoreboard players reset @p[tag=pendingDeath] temp2
scoreboard players reset @p[tag=pendingDeath] temp3
scoreboard players reset @p[tag=pendingDeath] temp4
scoreboard players reset @p[tag=pendingDeath] temp5
scoreboard players reset @p[tag=pendingDeath] temp6
scoreboard players reset @p[tag=pendingDeath] temp7
scoreboard players reset @p[tag=pendingDeath] temp8
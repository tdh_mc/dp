# On calcule la ville la plus proche
function tdh:player/locate/nearest

# On affiche le résultat avec la distance si on n'est pas en ville
execute if score #locateResult distance matches 1.. run tellraw @a[distance=..1] [{"text":"La ville la plus proche est ","color":"gray"},{"nbt":"resultat.ville.prefixe","storage":"tdh:locate"},{"nbt":"resultat.ville.nom","storage":"tdh:locate","color":"gold"},{"text":".\nIl faut compter "},{"nbt":"resultat.distance","storage":"tdh:locate","color":"yellow"},{"text":" de route, en direction "},{"nbt":"resultat.direction.deDu","storage":"tdh:locate"},{"nbt":"resultat.direction.nom","storage":"tdh:locate","color":"yellow"},{"text":"."}]

# On affiche simplement le nom de la ville si on s'y trouve déjà
execute if score #locateResult distance matches ..-1 run tellraw @a[distance=..1] [{"text":"Vous êtes ","color":"gray"},{"nbt":"resultat.ville.alAu","storage":"tdh:locate"},{"nbt":"resultat.ville.nom","storage":"tdh:locate","color":"gold"},{"text":" !"}]
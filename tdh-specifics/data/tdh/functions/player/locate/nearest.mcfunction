# On stocke la position du joueur exécutant la commande
function tdh:player/locate/save_pos

# On stocke les valeurs par défaut des variables de la "boucle"
data modify storage tdh:locate resultat set value {"ville":{"nom":"","alAu":"","deDu":""},"distance":"","direction":{"nom":"","alAu":"","deDu":""}}
scoreboard players set #locateResult distance -1
scoreboard players set #locateResult weight 0
scoreboard players set #locateResult posX 0
scoreboard players set #locateResult posZ 0
scoreboard players set #locateResult deltaX 0
scoreboard players set #locateResult deltaZ 0

# On itère chaque ville pour trouver la plus proche
function tdh:player/locate/ville/argencon
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/athes
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/aulnoy
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/beothas
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/bussy_nigel
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/calmeflot
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/chassy
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/crestali
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/cyseal
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/dorlinor
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/duerrom
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/eidin
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/evrocq
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/fenicia
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/fort_du_val
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/frambourg
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/fultez
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/georlie
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/grenat
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/hades
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/la_dodene
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/le_hameau
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/le_relais
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/les_sablons
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/litorea
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/mides
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/milloreau
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/moronia
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/neronil
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/orea
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/pieuze
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/procyon
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/quecol
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/le_roustiflet
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/ternelieu
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/thalrion
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/tolbrok
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/verchamps
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/vernoglie
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/villonne
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

function tdh:player/locate/ville/ygriak
function tdh:player/locate/get_distance
function tdh:player/locate/save_if_nearest

# On calcule la distance du résultat
function tdh:player/locate/compute_result
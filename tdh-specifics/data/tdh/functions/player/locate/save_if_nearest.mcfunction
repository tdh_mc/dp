# Si la distance calculée est inférieure à minDistance, on save
scoreboard players set #locate temp 0
execute if score #locateResult distance matches ..-1 run scoreboard players set #locate temp 1
execute if score #locateResult distance > #locate distance run scoreboard players set #locate temp 1

execute if score #locate temp matches 1 run function tdh:player/locate/save
# On calcule la distance² entre les positions du joueur et de la ville
scoreboard players operation #locate distance = #locate posX
scoreboard players operation #locate distance -= #locateTarget posX
scoreboard players operation #locate distance *= #locate distance
scoreboard players operation #locate temp = #locate posZ
scoreboard players operation #locate temp -= #locateTarget posZ
scoreboard players operation #locate temp *= #locate temp
scoreboard players operation #locate distance += #locate temp
scoreboard players operation #locate distance *= #locate weight
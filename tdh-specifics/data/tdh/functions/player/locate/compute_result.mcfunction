# On retire le poids appliqué à la distance calculée
scoreboard players operation #locateResult distance /= #locateResult weight

# On génère une chaîne de caractères en fonction de la distance
# Si on est à moins de 400m (dist² < 160 000)
execute if score #locateResult distance matches ..160000 run data modify storage tdh:locate resultat.distance set value "moins d'une heure"
# Si on est à moins de 700m (dist² < 490 000)
execute if score #locateResult distance matches 160001..490000 run data modify storage tdh:locate resultat.distance set value "moins de 2 heures"
# Si on est à moins de 1km (dist² < 1 000 000)
execute if score #locateResult distance matches 490001..1000000 run data modify storage tdh:locate resultat.distance set value "moins de 3 heures"
# Si on est à moins de 1.33km (dist² < 1 768 900)
execute if score #locateResult distance matches 1000001..1768900 run data modify storage tdh:locate resultat.distance set value "moins de 4 heures"
# Si on est à moins de 1.67km (dist² < 2 788 900)
execute if score #locateResult distance matches 1768901..2788900 run data modify storage tdh:locate resultat.distance set value "moins de 5 heures"
# Si on est à moins de 2km (dist² < 4 000 000)
execute if score #locateResult distance matches 2788901..4000000 run data modify storage tdh:locate resultat.distance set value "environ 6 heures"
# Si on est à moins de 2.33km (dist² < 5 428 900)
execute if score #locateResult distance matches 4000001..5428900 run data modify storage tdh:locate resultat.distance set value "environ 7 heures"
# Si on est à moins de 2.67km (dist² < 7 128 900)
execute if score #locateResult distance matches 5428901..7128900 run data modify storage tdh:locate resultat.distance set value "environ 8 heures"
# Si on est à moins de 3km (dist² < 9 000 000)
execute if score #locateResult distance matches 7128901..9000000 run data modify storage tdh:locate resultat.distance set value "environ 9 heures"
# Si on est à moins de 3.33km (dist² < 11 088 900)
execute if score #locateResult distance matches 9000001..11088900 run data modify storage tdh:locate resultat.distance set value "environ 10 heures"
# Si on est à moins de 6km (dist² < 36 000 000)
execute if score #locateResult distance matches 11088900..36000000 run data modify storage tdh:locate resultat.distance set value "une journée"
# Sinon (dist² > 36 000 000)
execute if score #locateResult distance matches 36000001.. run data modify storage tdh:locate resultat.distance set value "plus d'une journée"


# On calcule la distance dans les 2 directions, qu'on stocke dans #locateTarget deltaX et deltaZ
scoreboard players operation #locateTarget deltaX = #locateResult posX
scoreboard players operation #locateTarget deltaX -= #locateTarget posX
scoreboard players operation #locateTarget deltaZ = #locateResult posZ
scoreboard players operation #locateTarget deltaZ -= #locateTarget posZ

# On calcule le pourcentage de variation en X par rapport à la variation en Z, qu'on stocke dans #locateTarget weight
scoreboard players operation #locateTarget weight = #locateTarget deltaX
scoreboard players set #locateTarget temp 100
scoreboard players operation #locateTarget weight *= #locateTarget temp
scoreboard players operation #locateTarget weight /= #locateTarget deltaZ

# Si le ratio est négatif, on le rend positif
execute if score #locateTarget weight matches ..-1 run scoreboard players set #locateTarget temp 0
execute if score #locateTarget weight matches ..-1 run scoreboard players operation #locateTarget temp -= #locateTarget weight
execute if score #locateTarget weight matches ..-1 run scoreboard players operation #locateTarget temp >< #locateTarget weight


# On génère une chaine de caractères en fonction de la direction
# Ratio > 2 (+ proche de l'axe est/ouest que nord/sud)
execute if score #locateTarget weight matches 200.. run function tdh:player/locate/compute_result_direction_x
# Ratio proche de 1 (proche de l'axe NE/SW ou NW/SE)
execute if score #locateTarget weight matches 51..199 run function tdh:player/locate/compute_result_direction_xz
# Ratio < 0.5 (+ proche de l'axe nord/sud que est/ouest)
execute if score #locateTarget weight matches ..50 run function tdh:player/locate/compute_result_direction_z


# On vérifie si on est en ville ou non
# Si les deltaX et Z sont négatifs, on les rend positifs
execute if score #locateTarget deltaX matches ..-1 run scoreboard players set #locateTarget temp 0
execute if score #locateTarget deltaX matches ..-1 run scoreboard players operation #locateTarget temp -= #locateTarget deltaX
execute if score #locateTarget deltaX matches ..-1 run scoreboard players operation #locateTarget temp >< #locateTarget deltaX

execute if score #locateTarget deltaZ matches ..-1 run scoreboard players set #locateTarget temp 0
execute if score #locateTarget deltaZ matches ..-1 run scoreboard players operation #locateTarget temp -= #locateTarget deltaZ
execute if score #locateTarget deltaZ matches ..-1 run scoreboard players operation #locateTarget temp >< #locateTarget deltaZ

# Si deltaX et deltaZ sont inférieurs aux limites de la ville, on est en ville (et on définit donc notre distance à 0)
execute if score #locateTarget deltaX < #locateResult deltaX if score #locateTarget deltaZ < #locateResult deltaZ run scoreboard players set #locateResult distance -1
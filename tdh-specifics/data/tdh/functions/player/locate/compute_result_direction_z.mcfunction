
# Ratio < 0.5 (+ proche de l'axe nord/sud que est/ouest)
# Si deltaZ est positif (vers l'sud)
execute if score #locateTarget deltaZ matches 1.. run data modify storage tdh:locate resultat.direction.nom set value "sud"
# Si deltaZ est négatif (vers l'nord)
execute if score #locateTarget deltaZ matches ..-1 run data modify storage tdh:locate resultat.direction.nom set value "nord"
# Sinon
execute if score #locateTarget deltaZ matches 0 run data modify storage tdh:locate resultat.direction.nom set value "bas"

# On enregistre les pronoms correspondants
data modify storage tdh:locate resultat.direction.deDu set value "du "
data modify storage tdh:locate resultat.direction.alAu set value "au "
execute if score #locateTarget deltaZ matches 0 run data modify storage tdh:locate resultat.direction.alAu set value "en "
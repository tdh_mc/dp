
# Ratio > 2 (+ proche de l'axe est/ouest que nord/sud)
# Si deltaX est positif (vers l'est)
execute if score #locateTarget deltaX matches 1.. run data modify storage tdh:locate resultat.direction.nom set value "est"
# Si deltaX est négatif (vers l'ouest)
execute if score #locateTarget deltaX matches ..-1 run data modify storage tdh:locate resultat.direction.nom set value "ouest"
# Sinon
execute if score #locateTarget deltaX matches 0 run data modify storage tdh:locate resultat.direction.nom set value "bas"

# On enregistre les pronoms correspondants
data modify storage tdh:locate resultat.direction.deDu set value "de l'"
data modify storage tdh:locate resultat.direction.alAu set value "à l'"
execute if score #locateTarget deltaX matches 0 if score #locateTarget deltaZ matches 0 run data modify storage tdh:locate resultat.direction.alAu set value "en "
# On enregistre minDistance et le résultat attendu dans le storage
scoreboard players operation #locateResult distance = #locate distance
scoreboard players operation #locateResult weight = #locate weight
scoreboard players operation #locateResult posX = #locate posX
scoreboard players operation #locateResult posZ = #locate posZ
scoreboard players operation #locateResult deltaX = #locate deltaX
scoreboard players operation #locateResult deltaZ = #locate deltaZ
data modify storage tdh:locate resultat.ville set from storage tdh:locate ville
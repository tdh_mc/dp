
# Ratio proche de 1 (NE/NW/SE/SW)
# Si deltaX est positif et deltaZ est positif
execute if score #locateTarget deltaX matches 1.. if score #locateTarget deltaZ matches 1.. run data modify storage tdh:locate resultat.direction.nom set value "sud-est"
# Si deltaX est négatif et deltaZ est positif
execute if score #locateTarget deltaX matches ..-1 if score #locateTarget deltaZ matches 1.. run data modify storage tdh:locate resultat.direction.nom set value "sud-ouest"
# Si deltaX est positif et deltaZ est négatif
execute if score #locateTarget deltaX matches 1.. if score #locateTarget deltaZ matches ..-1 run data modify storage tdh:locate resultat.direction.nom set value "nord-est"
# Si deltaX est négatif et deltaZ est négatif
execute if score #locateTarget deltaX matches ..-1 if score #locateTarget deltaZ matches ..-1 run data modify storage tdh:locate resultat.direction.nom set value "nord-ouest"
# Sinon
execute if score #locateTarget deltaX matches 0 if score #locateTarget deltaZ matches 0 run data modify storage tdh:locate resultat.direction.nom set value "bas"

# On enregistre les pronoms correspondants
data modify storage tdh:locate resultat.direction.deDu set value "du "
data modify storage tdh:locate resultat.direction.alAu set value "au "
execute if score #locateTarget deltaX matches 0 if score #locateTarget deltaZ matches 0 run data modify storage tdh:locate resultat.direction.alAu set value "en "
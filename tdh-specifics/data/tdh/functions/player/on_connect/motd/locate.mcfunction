# On calcule la ville la plus proche
function tdh:player/locate/nearest

# On affiche la ville la plus proche
# Si on se trouve en ville
execute if score #locateResult distance matches ..-1 run tellraw @a[distance=0] [{"text":"-","color":"gold"},{"text":" Vous êtes ","color":"gold"},{"nbt":"resultat.ville.alAu","storage":"tdh:locate","color":"gold"},{"nbt":"resultat.ville.nom","storage":"tdh:locate","color":"yellow"},{"text":" !","color":"gold"}]
# Si on est à moins de 2h de marche (700m² = 490'000 dist²), on est "proches"
execute if score #locateResult distance matches 1..490000 run tellraw @a[distance=0] [{"text":"-","color":"gold"},{"text":" Vous êtes proche ","color":"gold"},{"nbt":"resultat.ville.deDu","storage":"tdh:locate","color":"gold"},{"nbt":"resultat.ville.nom","storage":"tdh:locate","color":"yellow"},{"text":", à ","color":"gold"},{"nbt":"resultat.distance","storage":"tdh:locate","color":"gold"},{"text":" "},{"nbt":"resultat.direction.alAu","storage":"tdh:locate","color":"gold"},{"nbt":"resultat.direction.nom","storage":"tdh:locate","color":"yellow"},{"text":".","color":"gold"}]
# Si on est à moins de 6h de marche (2000m² = 4'000'000 dist²), on est assez éloignés
execute if score #locateResult distance matches 490001..4000000 run tellraw @a[distance=0] [{"text":"-","color":"gold"},{"text":" Vous êtes assez éloigné ","color":"gold"},{"nbt":"resultat.ville.deDu","storage":"tdh:locate","color":"gold"},{"nbt":"resultat.ville.nom","storage":"tdh:locate","color":"yellow"},{"text":", la ville la plus proche, située ","color":"gold"},{"nbt":"resultat.direction.alAu","storage":"tdh:locate","color":"gold"},{"nbt":"resultat.direction.nom","storage":"tdh:locate","color":"yellow"},{"text":".","color":"gold"}]
# Si on est à plus de 2km, on ne donne pas de nom de ville
execute if score #locateResult distance matches 4000001.. run tellraw @a[distance=0] [{"text":"-","color":"gold"},{"text":" Vous êtes véritablement en pleine nature, loin de ","color":"gold"},{"text":"toutes les villes","color":"yellow"},{"text":" !","color":"gold"}]
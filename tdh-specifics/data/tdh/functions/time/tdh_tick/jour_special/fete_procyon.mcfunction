# On calcule le nombre d'années depuis le débarquement
# On enregistre l'année actuelle
scoreboard players operation #feteProcyon duree = #temps dateAnnee

# On retranche l'année du débarquement des Procyens au Hameau
scoreboard players operation #feteProcyon duree -= #feteProcyon dateAnnee


# On enregistre le texte du jour
data modify storage tdh:datetime date.special set value {"basique":"Joyeuse Fête des Lumières à tous !","formate":'[{"text":"Joyeuse Fête des Lumières à tous !","color":"#d2a450"},{"text":"\\nLes Procyens célèbrent les forces cosmiques qui ont sauvé leur ville de la Grande Épidémie il y a maintenant ","color":"gray"},{"score":{"objective":"duree","name":"#feteProcyon"},"color":"#e4c180"},{"text":" ans !","color":"gray"}]'}
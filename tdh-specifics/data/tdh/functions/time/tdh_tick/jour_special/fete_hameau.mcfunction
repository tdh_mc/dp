# On calcule le nombre d'années depuis le débarquement
# On enregistre l'année actuelle
scoreboard players operation #feteHameau duree = #temps dateAnnee

# On retranche l'année du débarquement des Procyens au Hameau
scoreboard players operation #feteHameau duree -= #feteHameau dateAnnee


# On enregistre le texte du jour
data modify storage tdh:datetime date.special set value {"basique":"C'est la fête de la Fondation du Hameau !","formate":'[{"text":"C\'est la fête de la Fondation du Hameau !","color":"#ff8870"},{"text":"\\nLes Haméliens fêtent le débarquement des exilés Procyens sur l\'île du Hameau, survenu il y a exactement ","color":"gray"},{"score":{"objective":"duree","name":"#feteHameau"},"color":"#ff8870"},{"text":" ans !","color":"gray"}]'}
# On calcule le nombre d'années depuis le débarquement
# On enregistre l'année actuelle
scoreboard players operation #feteDorlinor duree = #temps dateAnnee

# On retranche l'année du débarquement des Procyens au Hameau
scoreboard players operation #feteDorlinor duree -= #feteDorlinor dateAnnee


# On enregistre le texte du jour
data modify storage tdh:datetime date.special set value {"basique":"C'est le Jour des Anciens à Dorlinor !","formate":'[{"text":"C\'est le Jour des Anciens !","color":"#93d292"},{"text":"\\nLes Dorlinois se recueillent en hommage à leurs ancêtres légendaires, qui auraient planté les graines du premier arbre de leur forêt il y a ","color":"gray"},{"score":{"objective":"duree","name":"#feteDorlinor"},"color":"#93d292"},{"text":" ans.","color":"gray"}]'}
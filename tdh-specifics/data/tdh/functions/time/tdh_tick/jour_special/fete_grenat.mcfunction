# On calcule le nombre d'années depuis le débarquement
# On enregistre l'année actuelle
scoreboard players operation #feteGrenat duree = #temps dateAnnee

# On retranche l'année du débarquement des Procyens au Hameau
scoreboard players operation #feteGrenat duree -= #feteGrenat dateAnnee


# On enregistre le texte du jour
data modify storage tdh:datetime date.special set value {"basique":"Bon Carnaval de Grenat à tous !","formate":'[{"text":"Bon Carnaval de Grenat à tous !","color":"#da9ff8"},{"text":"\\nLes Grenois se déguisent et paradent ; c\'est le carnaval traditionnel de la ville, dont on dit qu\'il existe depuis ","color":"gray"},{"score":{"objective":"duree","name":"#feteGrenat"},"color":"#da9ff8"},{"text":" ans...","color":"gray"}]'}
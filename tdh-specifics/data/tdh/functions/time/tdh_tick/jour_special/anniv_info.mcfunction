# On calcule l'âge du capitaine
# On enregistre l'année actuelle
scoreboard players operation #annivInfo duree = #temps dateAnnee

# On retranche l'année du débarquement des Procyens au Hameau
scoreboard players operation #annivInfo duree -= #annivInfo dateAnnee


# On enregistre le texte du jour
data modify storage tdh:datetime date.special set value {"basique":"Souhaitons un joyeux anniversaire à infomase !","formate":'[{"text":"Souhaitons un joyeux anniversaire à ","color":"aqua"},{"text":"infomase","color":"dark_aqua","bold":"true","hoverEvent":{"action":"show_text","value":"Cliquez ici :)"},"clickEvent":{"action":"suggest_command","value":"/msg infomase Joyeux anniversaire !"}},{"text":", qui entre aujourd\'hui dans sa "},{"score":{"name":"#annivInfo","objective":"duree"},"color":"dark_aqua"},{"text":"ème année !"}]'}
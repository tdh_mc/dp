# Joyeux Noël !
execute if score #temps dateJour matches 25 if score #temps dateMois matches 12 run function tdh:time/tdh_tick/jour_special/noel

# Fête des morts (TODO: implémenter des événements spéciaux ce jour-là)
execute if score #temps dateJour matches 2 if score #temps dateMois matches 11 run function tdh:time/tdh_tick/jour_special/fete_des_morts


# Anniversaire Dogo
execute if score #temps dateJour = #annivDogo dateJour if score #temps dateMois = #annivDogo dateMois run function tdh:time/tdh_tick/jour_special/anniv_dogo

# Anniversaire Info
execute if score #temps dateJour = #annivInfo dateJour if score #temps dateMois = #annivInfo dateMois run function tdh:time/tdh_tick/jour_special/anniv_info


# Fête du Hameau
execute if score #temps dateJour = #feteHameau dateJour if score #temps dateMois = #feteHameau dateMois run function tdh:time/tdh_tick/jour_special/fete_hameau
# Fête de Procyon
execute if score #temps dateJour = #feteProcyon dateJour if score #temps dateMois = #feteProcyon dateMois run function tdh:time/tdh_tick/jour_special/fete_procyon
# Fête de Grenat
execute if score #temps dateJour = #feteGrenat dateJour if score #temps dateMois = #feteGrenat dateMois run function tdh:time/tdh_tick/jour_special/fete_grenat
# Fête de Dorlinor
execute if score #temps dateJour = #feteDorlinor dateJour if score #temps dateMois = #feteDorlinor dateMois run function tdh:time/tdh_tick/jour_special/fete_dorlinor
# On calcule l'âge du capitaine
# On enregistre l'année actuelle
scoreboard players operation #annivDogo duree = #temps dateAnnee

# On retranche l'année du débarquement des Procyens au Hameau
scoreboard players operation #annivDogo duree -= #annivDogo dateAnnee


# On enregistre le texte du jour
data modify storage tdh:datetime date.special set value {"basique":"Souhaitons un joyeux anniversaire à Dogo !","formate":'[{"text":"Souhaitons un joyeux anniversaire à ","color":"aqua"},{"text":"Dogo","color":"dark_aqua","bold":"true","hoverEvent":{"action":"show_text","value":"Cliquez ici :)"},"clickEvent":{"action":"suggest_command","value":"/msg Dogo Joyeux anniversaire !"}},{"text":", qui entre aujourd\'hui dans sa "},{"score":{"name":"#annivDogo","objective":"duree"},"color":"dark_aqua"},{"text":"ème année !"}]'}
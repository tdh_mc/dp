execute unless score fakePlayer speedGrassOn matches 1 run tellraw @a [{"text":"Le système ","color":"gray"},{"text":"SpeedGrass","color":"green"},{"text":" vient d'être activé pour ","color":"gray"},{"text":"24","color":"red"},{"text":" heures.","color":"gray"}]
function tdh:time/speedgrass/on_silent
execute if score fakePlayer customTimeflowOn matches 1 run schedule function tdh:time/speedgrass/off 48000t
execute if score fakePlayer customTimeflowOn matches 0 run schedule function tdh:time/speedgrass/off 24000t
# On définit une variable "aléatoire" (temp) avec l'aide de notre position
execute store result score @s temp run data get entity @s Pos[2] 100
scoreboard players set @s temp2 200
scoreboard players operation @s temp %= @s temp2

# En fonction de sa valeur (de 0 à 199) on donne des noms différents
execute if score @s temp matches 0..97 run data merge entity @s {CustomName:"\"Dereck\""}
execute if score @s temp matches 98 run data merge entity @s {CustomName:"{\"text\":\"Dereck\",\"color\":\"yellow\"}"}
execute if score @s temp matches 99 run data merge entity @s {CustomName:"{\"text\":\"Dereck\",\"color\":\"red\"}"}
execute if score @s temp matches 100..117 run data merge entity @s {CustomName:"\"Julien\""}
execute if score @s temp matches 118..119 run data merge entity @s {CustomName:"\"Julien Sallucé\""}
execute if score @s temp matches 120..129 run data merge entity @s {CustomName:"\"Gonzague\""}
execute if score @s temp matches 130..139 run data merge entity @s {CustomName:"\"Douglas\""}
execute if score @s temp matches 140..149 run data merge entity @s {CustomName:"\"Stanislas\""}
execute if score @s temp matches 150..159 run data merge entity @s {CustomName:"\"Oliveira\""}
execute if score @s temp matches 160..169 run data merge entity @s {CustomName:"\"Jimmy\""}
execute if score @s temp matches 170..179 run data merge entity @s {CustomName:"\"José\""}
execute if score @s temp matches 180..189 run data merge entity @s {CustomName:"\"Christopher\""}
execute if score @s temp matches 190..199 run data merge entity @s {CustomName:"\"Mortimer\""}
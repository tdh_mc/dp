# TDH Test de sécurité 2
# --------------------
# Cette fonction teste si l'entité l'invoquant
# est un mob posant un problème de sécurité, ou non.
# L'utiliser avec /execute as <entité>.

# say Test2 @s

execute as @s run function tdh:secu/test
execute if score fakeSecu temp matches 1 run scoreboard players add fakeSecu temp2 1
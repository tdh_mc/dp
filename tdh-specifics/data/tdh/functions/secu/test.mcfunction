# TDH Test de sécurité
# --------------------
# Cette fonction teste si l'entité l'invoquant
# est un mob posant un problème de sécurité, ou non.
# L'utiliser avec /execute as <entité>.

# say Test @s

scoreboard players set fakeSecu temp 0
execute if entity @s[type=#tdh:hostile] run scoreboard players set fakeSecu temp 1
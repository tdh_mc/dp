# Le Hameau - Système de sécurité du port [Initialisation]

# Etats :
# 0 = pas de menace [OUVERT]
# 1 = en cours d'ouverture/de fermeture
# 2 = menace active [FERMÉ]

# Porte CANAL1
scoreboard objectives add secuHamCanal1 dummy "Le Hameau - Sécurité [CANAL1]"
scoreboard players set fakeSecu secuHamCanal1 0
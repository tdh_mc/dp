scoreboard players set fakeSecu secuHamCanal1 1

tellraw @a [{"text":"Fermeture de sécurité secteur ","color":"gray"},{"text":"CANAL1","color":"dark_red"},{"text":" : entités hostiles repérées (","color":"gray"},{"selector":"@s","color":"red"},{"text":").","color":"gray"}]

function tdh:secu/hameau/port/canal1/close1
schedule function tdh:secu/hameau/port/canal1/close2 0.4s

playsound block.piston.contract block @a[distance=..32] ~ ~-1 ~ 1
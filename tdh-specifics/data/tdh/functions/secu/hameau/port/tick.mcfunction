# Le Hameau - Système de sécurité du port

# PORTE CANAL1
# Position de la porte : [42 63 249]
# Position de la détection : [27 58 250 -> 50 64 257]
execute run scoreboard players set fakeSecu temp 0
execute if score fakeSecu secuHamCanal1 matches 0 as @e[type=!player,x=27,y=58,z=250,dx=23,dy=8,dz=7] run function tdh:secu/hameau/port/canal1/close_test
execute if score fakeSecu secuHamCanal1 matches 2 unless entity @e[x=27,y=58,z=250,dx=23,dy=8,dz=7] positioned 42 63 429 run function tdh:secu/hameau/port/canal1/open

execute run scoreboard players set fakeSecu temp2 0
execute if score fakeSecu secuHamCanal1 matches 2 as @e[x=27,y=58,z=250,dx=23,dy=8,dz=7] run function tdh:secu/test2
execute if score fakeSecu secuHamCanal1 matches 2 if score fakeSecu temp2 matches 0 positioned 42 63 429 run function tdh:secu/hameau/port/canal1/open
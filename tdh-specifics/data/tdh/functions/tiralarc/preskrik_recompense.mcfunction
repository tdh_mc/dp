# Gestion des récompenses de Preskrik :

# Si on a déjà eu une récompense aujourd'hui, on met un message d'erreur
execute if score @p TAA_preskrik_d = #temps dateJourTotal run tellraw @p [{"text":"N'exagérez pas!","color":"red"},{"text":" Vous n'avez droit qu'à une seule récompense par jour."},{"text":" Cela dit, bravo pour cette seconde victoire !"}]

# Sinon, on met le message de félicitations
execute unless score @p TAA_preskrik_d = #temps dateJourTotal run tellraw @p [{"text":"Bien joué !","color":"gold"},{"text":" Voici votre récompense. Si vous réussissez à nouveau "},{"text":"demain","color":"red"},{"text":", elle sera plus importante !"}]

# Si on a eu une récompense hier, on lance le script d'incrémentation du nombre de victoires
# Sinon, on réinitialise le nombre de victoires
scoreboard players add @p TAA_preskrik_d 1
execute if score @p TAA_preskrik_d = #temps dateJourTotal run function tdh:tiralarc/preskrik_recompense_increment
execute if score @p TAA_preskrik_d < #temps dateJourTotal unless score @p TAA_preskrik_d matches 0 run function tdh:tiralarc/preskrik_recompense_reset
scoreboard players remove @p TAA_preskrik_d 1

# Si on n'a pas eu le message d'erreur, on donne une récompense
execute unless score @p TAA_preskrik_d = #temps dateJourTotal run function tdh:tiralarc/preskrik_recompense_main

# Dans tous les cas, comme on vient de gagner, on définit la date de victoire à aujourd'hui
scoreboard players operation @p TAA_preskrik_d = #temps dateJourTotal
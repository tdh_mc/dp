# On incrémente le nombre de victoires
scoreboard players add @p TAA_preskrik_n 1

# Si on a dépassé les 12 victoires, on reset à 0
execute if score @p TAA_preskrik_n matches 13.. run scoreboard players set @p TAA_preskrik_n 0
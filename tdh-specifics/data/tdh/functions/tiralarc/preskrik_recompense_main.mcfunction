# On donne une récompense selon le niveau que l'on a atteint
execute unless score @p TAA_preskrik_n matches 1.. run function tdh:tiralarc/recompense_lvl0
execute if score @p TAA_preskrik_n matches 1 run function tdh:tiralarc/recompense_lvl1
execute if score @p TAA_preskrik_n matches 2 run function tdh:tiralarc/recompense_lvl2
execute if score @p TAA_preskrik_n matches 3 run function tdh:tiralarc/recompense_lvl3
execute if score @p TAA_preskrik_n matches 4 run function tdh:tiralarc/recompense_lvl4
execute if score @p TAA_preskrik_n matches 5 run function tdh:tiralarc/recompense_lvl5
execute if score @p TAA_preskrik_n matches 6 run function tdh:tiralarc/recompense_lvl6
execute if score @p TAA_preskrik_n matches 7 run function tdh:tiralarc/recompense_lvl7
execute if score @p TAA_preskrik_n matches 8 run function tdh:tiralarc/recompense_lvl8
execute if score @p TAA_preskrik_n matches 9 run function tdh:tiralarc/recompense_lvl9
execute if score @p TAA_preskrik_n matches 10 run function tdh:tiralarc/recompense_lvl10
execute if score @p TAA_preskrik_n matches 11 run function tdh:tiralarc/recompense_lvl11
execute if score @p TAA_preskrik_n matches 12 run function tdh:tiralarc/recompense_lvl12
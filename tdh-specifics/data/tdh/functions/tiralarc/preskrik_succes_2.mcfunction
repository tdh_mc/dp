# Succès niveau 1 pour le tir à l'arc de Preskrik

playsound minecraft:entity.arrow.hit_player player @a 1.0 67.0 1260.0 1 1
data merge entity @e[type=arrow,sort=nearest,limit=1] {life:1100}
execute positioned 1 67 1260 run tellraw @p[distance=..2] [{"text":"Bien joué !","color":"gold"},{"text":" N'hésitez pas à essayer le niveau 3 (à votre droite)."},{"text":" Il y a une récompense à la clé !","color":"gold"}]
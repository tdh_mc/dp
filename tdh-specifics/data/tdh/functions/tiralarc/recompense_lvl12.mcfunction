# On summon l'item plutôt que de le give pour éviter de le perdre
summon item ~ ~ ~ {Item:{id:"minecraft:emerald",Count:5b}}
tellraw @p [{"text":"Vous remportez 5 "},{"text":"émeraudes","color":"gold","hoverEvent":{"action":"show_item","value":"{id:\"minecraft:emerald\",Count:5b}"}},{"text":" ! Profitez-en pour vous faire plaisir dans les nombreuses boutiques du Récif : vous l'avez bien mérité !"}]
tellraw @p [{"text":"Vous avez atteint le dernier niveau de récompense ! Si vous réussissez à nouveau l'épreuve, vous remporterez la récompense de base.","color":"aqua"}]
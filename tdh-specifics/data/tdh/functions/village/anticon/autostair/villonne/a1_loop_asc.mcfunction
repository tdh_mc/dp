execute store result score @s posX run data get entity @s Pos[0] 100
execute store result score @s posZ run data get entity @s Pos[2] 100

execute if score @s posZ matches 110050.. if score @s posX matches 58951.. run data merge entity @s {Motion:[-0.2d,0.2d,0d]}
execute if score @s posZ matches ..110049 if score @s posX matches 59150.. run data merge entity @s {Motion:[0d,0.2d,0.2d]}
execute if score @s posZ matches ..109850 if score @s posX matches ..59149 run data merge entity @s {Motion:[0.2d,0.2d,0d]}
execute if score @s posZ matches 109851.. if score @s posX matches ..58950 run data merge entity @s {Motion:[0d,0.2d,-0.2d]}
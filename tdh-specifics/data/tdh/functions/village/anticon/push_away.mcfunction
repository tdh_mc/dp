execute if entity @e[distance=..1,tag=PushEast] at @s run function tdh:anticon/push_east
execute if entity @e[distance=..1,tag=PushWest] at @s run function tdh:anticon/push_west
execute if entity @e[distance=..1,tag=PushSouth] at @s run function tdh:village/anticon/push_south
execute if entity @e[distance=..1,tag=PushNorth] at @s run function tdh:village/anticon/push_north
execute if entity @e[distance=..1,tag=PushNW] at @s run data merge entity @s {Motion:[-0.5d,0d,-0.5d]}
execute if entity @e[distance=..1,tag=PushNE] at @s run data merge entity @s {Motion:[0.5d,0d,-0.5d]}
execute if entity @e[distance=..1,tag=PushSW] at @s run data merge entity @s {Motion:[-0.5d,0d,0.5d]}
execute if entity @e[distance=..1,tag=PushSE] at @s run data merge entity @s {Motion:[0.5d,0d,0.5d]}
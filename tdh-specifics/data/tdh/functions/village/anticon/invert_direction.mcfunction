execute store result entity @s Motion[0] double -0.002 run data get entity @s Motion[0] 1000
execute store result entity @s Motion[2] double -0.002 run data get entity @s Motion[2] 1000
execute store result entity @s Motion[1] double -0.0005 run data get entity @s Motion[1] 1000

execute if entity @s[tag=MaybeStuck] run function tdh:village/anticon/push_away

tag @s add MaybeStuck
schedule function tdh:village/anticon/clear_stuck_tag 50t
# Gets and stores the Brain data of the villager who called it

scoreboard players add @p temp4 1

# Does the villager have a bed and/or a workstation ?
execute store success score @s bedSet run data get entity @s Brain.memories.minecraft:home
execute store success score @s wsSet run data get entity @s Brain.memories.minecraft:job_site
execute store success score @s mpSet run data get entity @s Brain.memories.minecraft:meeting_point

# If yes, we store the information
execute if score @s bedSet matches 1.. run scoreboard players add @p temp2 1
execute if score @s wsSet matches 1.. run scoreboard players add @p temp3 1
execute if score @s mpSet matches 1.. run scoreboard players add @p temp7 1

# We then display the information using tellraw
tellraw @p [{"text":"Données du villageois ","color":"gold"},{"selector":"@s","color":"yellow"},{"text":" :","color":"gold"}]

execute if score @s bedSet matches 1.. run tellraw @p [{"text":"Lit [","color":"dark_green"},{"nbt":"Brain.memories.minecraft:home.value.pos[0]","entity":"@s","color":"green"},{"text":", ","color":"dark_green"},{"nbt":"Brain.memories.minecraft:home.value.pos[1]","entity":"@s","color":"green"},{"text":", ","color":"dark_green"},{"nbt":"Brain.memories.minecraft:home.value.pos[2]","entity":"@s","color":"green"},{"text":"]","color":"dark_green"}]
execute if score @s bedSet matches 0 run tellraw @p [{"text":"Sans domicile.","color":"dark_red"}]

execute if score @s wsSet matches 1.. run tellraw @p [{"text":"Travail [","color":"dark_green"},{"nbt":"Brain.memories.minecraft:job_site.value.pos[0]","entity":"@s","color":"green"},{"text":", ","color":"dark_green"},{"nbt":"Brain.memories.minecraft:job_site.value.pos[1]","entity":"@s","color":"green"},{"text":", ","color":"dark_green"},{"nbt":"Brain.memories.minecraft:job_site.value.pos[2]","entity":"@s","color":"green"},{"text":"]","color":"dark_green"}]
execute if score @s wsSet matches 0 run tellraw @p [{"text":"Sans emploi.","color":"dark_red"}]

execute if score @s mpSet matches 1.. run tellraw @p [{"text":"PdR [","color":"dark_green","hoverEvent":{"action":"show_text","value":"point de rencontre"}},{"nbt":"Brain.memories.minecraft:meeting_point.value.pos[0]","entity":"@s","color":"green"},{"text":", ","color":"dark_green"},{"nbt":"Brain.memories.minecraft:meeting_point.value.pos[1]","entity":"@s","color":"green"},{"text":", ","color":"dark_green"},{"nbt":"Brain.memories.minecraft:meeting_point.value.pos[2]","entity":"@s","color":"green"},{"text":"]","color":"dark_green"}]
execute if score @s mpSet matches 0 run tellraw @p [{"text":"Sans point de rencontre.","color":"dark_red"}]

tellraw @p [{"text":" --------------------","color":"gold"}]
scoreboard objectives add bedSet dummy "Lit possédé"
scoreboard objectives add bedCoordX dummy "Lit: Coordonnée X"
scoreboard objectives add bedCoordY dummy "Lit: Coordonnée Y"
scoreboard objectives add bedCoordZ dummy "Lit: Coordonnée Z"

scoreboard objectives add wsSet dummy "Emploi possédé"
scoreboard objectives add wsCoordX dummy "Emploi: Coordonnée X"
scoreboard objectives add wsCoordY dummy "Emploi: Coordonnée Y"
scoreboard objectives add wsCoordZ dummy "Emploi: Coordonnée Z"

scoreboard objectives add mpSet dummy "Point de rencontre possédé"
scoreboard objectives add mpCoordX dummy "PdR: Coordonnée X"
scoreboard objectives add mpCoordY dummy "PdR: Coordonnée Y"
scoreboard objectives add mpCoordZ dummy "PdR: Coordonnée Z"
# On vérifie si chaque villageois ayant récemment fait l'objet d'un message d'avertissement va mieux
execute as @e[type=villager,tag=RisqueDeDeces] at @s run function tdh:village/death_detector/clear_test

# On vérifie si chaque villageois n'ayant pas récemment fait l'objet d'un message d'avertissement est en danger
execute as @e[type=villager,tag=!RisqueDeDeces] at @s run function tdh:village/death_detector/tag_test
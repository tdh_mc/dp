# On vérifie si le villageois ayant appelé cette fonction est à proximité de mobs hostiles
execute at @e[type=#tdh:menace_villageois,distance=..8] run tag @s add RisqueDeDeces

# Si on vient de déclencher l'alerte, on attend 30s avant de revérifier
execute as @s[tag=RisqueDeDeces] run scoreboard players set @s remainingTicks 30

# Si on vient de déclencher l'alerte, on affiche un message
execute as @s[tag=RisqueDeDeces] run function tdh:village/death_detector/tag_tellraw
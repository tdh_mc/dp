execute store result score @s posX run data get entity @s Pos[0]
execute store result score @s posY run data get entity @s Pos[1]
execute store result score @s posZ run data get entity @s Pos[2]

tellraw @a [{"text":"Le villageois ","color":"gray"},{"selector":"@s","color":"gold"},{"text":" est menacé à la position ","color":"gray"},{"score":{"name":"@s","objective":"posX"},"color":"yellow"},{"text":"x / ","color":"yellow"},{"score":{"name":"@s","objective":"posY"},"color":"yellow"},{"text":"y / ","color":"yellow"},{"score":{"name":"@s","objective":"posZ"},"color":"yellow"},{"text":"z","color":"yellow"}]
tellraw @a [{"text":"Entités les plus proches de lui : ","color":"gray"},{"selector":"@e[sort=nearest,limit=5,distance=..8,type=#tdh:hostile]","color":"gold"}]

scoreboard players reset @s posX
scoreboard players reset @s posY
scoreboard players reset @s posZ

tag @s add RisqueDeDeces

playsound tdh:entity.wandering_trader.yes.vlan master @a ~ ~ ~ 99999
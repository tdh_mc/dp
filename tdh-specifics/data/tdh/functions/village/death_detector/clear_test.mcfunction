# On décrémente le nombre de ticks restants
scoreboard players remove @s remainingTicks 1

# Si le nombre de ticks restants est égal à 0, on supprime notre tag
# (ce qui aura pour effet de déclencher un nouveau test immédiatement après dans death_detector.mcfunction)
execute as @s[scores={remainingTicks=..0}] run tag @s remove RisqueDeDeces
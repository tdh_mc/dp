# Displays all the villager's Brain data in a 50 block radius

execute store result score @s temp run gamerule sendCommandFeedback
execute if score @s temp matches 1 run gamerule sendCommandFeedback false

# temp2 = Nombre de villageois avec un lit (%age = temp5)
# temp3 = Nombre de villageois avec un travail (%age = temp6)
# temp4 = Nombre total de villageois
# temp7 = Nombre de villageois avec un point de rass. (%age = temp8)
execute run scoreboard players set @s temp2 0
execute run scoreboard players set @s temp3 0
execute run scoreboard players set @s temp4 0
execute run scoreboard players set @s temp7 0

tellraw @s [{"text":"[Recherche de villageois dans un rayon de ","color":"gold"},{"text":"50","color":"red"},{"text":" blocs...]","color":"gold"}]
execute at @s as @e[type=villager,distance=..50] run function tdh:village/brainmaster

execute run scoreboard players operation @s temp5 = @s temp2
execute run scoreboard players operation @s temp6 = @s temp3
execute run scoreboard players operation @s temp8 = @s temp7
execute run scoreboard players operation @s temp5 *= fakePlayer number100
execute run scoreboard players operation @s temp6 *= fakePlayer number100
execute run scoreboard players operation @s temp8 *= fakePlayer number100
execute run scoreboard players operation @s temp5 /= @s temp4
execute run scoreboard players operation @s temp6 /= @s temp4
execute run scoreboard players operation @s temp8 /= @s temp4

tellraw @s [{"text":"[Recherche de villageois dans un rayon de ","color":"gold"},{"text":"50","color":"red"},{"text":" terminée]","color":"gold"}]
tellraw @s [{"text":"Villageois trouvés au total: ","color":"yellow"},{"score":{"name":"@s","objective":"temp4"},"color":"gold"}]
execute if score @s temp5 matches 90.. run tellraw @s [{"text":"Villageois possédant un lit: ","color":"green"},{"score":{"name":"@s","objective":"temp2"},"color":"dark_green"},{"text":" (","color":"dark_green"},{"score":{"name":"@s","objective":"temp5"},"color":"green"},{"text":"%","color":"green"},{"text":")","color":"dark_green"}]
execute if score @s temp5 matches ..89 run tellraw @s [{"text":"Villageois possédant un lit: ","color":"red"},{"score":{"name":"@s","objective":"temp2"},"color":"dark_red"},{"text":" (","color":"dark_red"},{"score":{"name":"@s","objective":"temp5"},"color":"red"},{"text":"%","color":"red"},{"text":")","color":"dark_red"}]
execute if score @s temp6 matches 90.. run tellraw @s [{"text":"Villageois possédant un emploi: ","color":"green"},{"score":{"name":"@s","objective":"temp3"},"color":"dark_green"},{"text":" (","color":"dark_green"},{"score":{"name":"@s","objective":"temp6"},"color":"green"},{"text":"%","color":"green"},{"text":")","color":"dark_green"}]
execute if score @s temp6 matches ..89 run tellraw @s [{"text":"Villageois possédant un emploi: ","color":"red"},{"score":{"name":"@s","objective":"temp3"},"color":"dark_red"},{"text":" (","color":"dark_red"},{"score":{"name":"@s","objective":"temp6"},"color":"red"},{"text":"%","color":"red"},{"text":")","color":"dark_red"}]
execute if score @s temp8 matches 90.. run tellraw @s [{"text":"Villageois assignés à un ","color":"green"},{"text":"PDR","color":"green","hoverEvent":{"action":"show_text","value":"{\"text\":\"point de rencontre\",\"color\":\"green\"}"}},{"text":": ","color":"green"},{"score":{"name":"@s","objective":"temp7"},"color":"dark_green"},{"text":" (","color":"dark_green"},{"score":{"name":"@s","objective":"temp8"},"color":"green"},{"text":"%","color":"green"},{"text":")","color":"dark_green"}]
execute if score @s temp8 matches ..89 run tellraw @s [{"text":"Villageois assignés à un ","color":"red"},{"text":"PDR","color":"red","hoverEvent":{"action":"show_text","value":"{\"text\":\"point de rencontre\",\"color\":\"red\"}"}},{"text":": ","color":"red"},{"score":{"name":"@s","objective":"temp7"},"color":"dark_red"},{"text":" (","color":"dark_red"},{"score":{"name":"@s","objective":"temp8"},"color":"red"},{"text":"%","color":"red"},{"text":")","color":"dark_red"}]

execute if score @s temp matches 1 run gamerule sendCommandFeedback true
# On calcule le modulo à la demi-heure de l'heure actuelle (temp6),
# et la demi-heure actuelle
scoreboard players set fakePlayer temp7 1000
scoreboard players operation fakePlayer temp6 = #temps currentTdhTick
scoreboard players operation #temps currentHalfHour = #temps currentTdhTick
scoreboard players operation #temps currentHalfHour /= fakePlayer temp7
scoreboard players operation fakePlayer temp6 %= fakePlayer temp7

# Si une horloge n'affiche actuellement pas la bonne heure (son currentHalfHour enregistré est différent de la demi-heure actuelle), on la remet à l'heure
execute unless score fakePlayer temp6 matches 0 as @e[tag=Horloge] unless score @s currentHalfHour = #temps currentHalfHour at @s rotated as @s run function tdh:ambient/horloge/update_display

# Dans le cas général, s'il est une demi-heure pile, on met à jour l'affichage
execute if score fakePlayer temp6 matches 0 as @e[tag=Horloge] at @s rotated as @s run function tdh:ambient/horloge/update_display

# On fait sonner les horloges à midi, minuit, et 6 heures du matin/soir
execute if score #temps currentTdhTick matches 12000 at @e[tag=Horloge] run function tdh:ambient/horloge/gong6
execute if score #temps currentTdhTick matches 24000 at @e[tag=Horloge] run function tdh:ambient/horloge/gong12
execute if score #temps currentTdhTick matches 36000 at @e[tag=Horloge] run function tdh:ambient/horloge/gong6
execute unless score #temps currentTdhTick matches 1..47999 at @e[tag=Horloge] run function tdh:ambient/horloge/gong12

# On réinitialise les variables temporaires
scoreboard players reset fakePlayer temp6
scoreboard players reset fakePlayer temp7
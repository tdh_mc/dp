# On fait sonner toutes les horloges
execute at @e[tag=Horloge] run function tdh:ambient/horloge/gong

# S'il reste des gongs à faire, on décrémente et on schedule un nouveau gong_all
scoreboard players remove fakePlayer gongsRemaining 1
execute if score fakePlayer gongsRemaining matches 1.. run schedule function tdh:ambient/horloge/gong_all 34t
# On fill avec un bloc avant de fill avec de l'air
# pour bien éteindre toutes les lampes de redstone adjacentes
fill ^-3 ^-3 ^ ^3 ^3 ^ black_glazed_terracotta
fill ^-3 ^-3 ^ ^3 ^3 ^ air

# On replace le bloc central et on rallume la lampe centrale
setblock ~ ~ ~ black_concrete
setblock ^ ^ ^-1 redstone_lamp[lit=true]
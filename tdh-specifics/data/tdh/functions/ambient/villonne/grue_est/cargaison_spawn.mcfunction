fill ~0 ~0 ~0 ~1 ~0 ~0 stone_brick_wall
fill ~0 ~-1 ~0 ~1 ~-4 ~0 spruce_fence
fill ~-2 ~-5 ~-1 ~3 ~-6 ~1 hay_block[axis=y]
fill ~0 ~-5 ~0 ~1 ~-5 ~0 jack_o_lantern
fill ~-2 ~-7 ~-1 ~3 ~-7 ~1 oak_slab[type=top]
setblock ~-2 ~-7 ~-1 oak_stairs[facing=north,half=top]
setblock ~3 ~-7 ~-1 oak_stairs[facing=north,half=top]
setblock ~-2 ~-7 ~1 oak_stairs[facing=south,half=top]
setblock ~3 ~-7 ~1 oak_stairs[facing=south,half=top]
fill ~-1 ~-4 ~-1 ~-1 ~-4 ~1 rail
fill ~2 ~-4 ~-1 ~2 ~-4 ~1 rail
fill ~-1 ~-5 ~-2 ~-1 ~-6 ~-2 ladder[facing=north]
fill ~2 ~-5 ~-2 ~2 ~-6 ~-2 ladder[facing=north]
fill ~-1 ~-5 ~2 ~-1 ~-6 ~2 ladder[facing=south]
fill ~2 ~-5 ~2 ~2 ~-6 ~2 ladder[facing=south]
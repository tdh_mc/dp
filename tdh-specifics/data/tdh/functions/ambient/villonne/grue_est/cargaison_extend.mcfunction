fill ~-1 ~-5 ~-2 ~-1 ~-6 ~-2 air
fill ~2 ~-5 ~-2 ~2 ~-6 ~-2 air
fill ~-1 ~-5 ~2 ~-1 ~-6 ~2 air
fill ~2 ~-5 ~2 ~2 ~-6 ~2 air
fill ~-2 ~-4 ~-1 ~3 ~-5 ~1 air

fill ~0 ~-4 ~0 ~1 ~-5 ~0 spruce_fence
fill ~-2 ~-6 ~-1 ~3 ~-7 ~1 hay_block[axis=y]
fill ~0 ~-6 ~0 ~1 ~-6 ~0 jack_o_lantern
fill ~-2 ~-8 ~-1 ~3 ~-8 ~1 oak_slab[type=top]
setblock ~-2 ~-8 ~-1 oak_stairs[facing=north,half=top]
setblock ~3 ~-8 ~-1 oak_stairs[facing=north,half=top]
setblock ~-2 ~-8 ~1 oak_stairs[facing=south,half=top]
setblock ~3 ~-8 ~1 oak_stairs[facing=south,half=top]
fill ~-1 ~-5 ~-1 ~-1 ~-5 ~1 rail
fill ~2 ~-5 ~-1 ~2 ~-5 ~1 rail
fill ~-1 ~-6 ~-2 ~-1 ~-7 ~-2 ladder[facing=north]
fill ~2 ~-6 ~-2 ~2 ~-7 ~-2 ladder[facing=north]
fill ~-1 ~-6 ~2 ~-1 ~-7 ~2 ladder[facing=south]
fill ~2 ~-6 ~2 ~2 ~-7 ~2 ladder[facing=south]
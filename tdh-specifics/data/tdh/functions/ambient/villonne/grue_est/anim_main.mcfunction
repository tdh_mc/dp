execute store result score @s posX run data get entity @s Pos[0]
execute store result score @s posY run data get entity @s Pos[1]
execute store result score @s posZ run data get entity @s Pos[2]

# False => vers le haut
# True => vers le bas
execute if block 726 69 1110 lever[powered=true] if score @s posZ matches 1110 if score @s posX matches 735..743 if score @s posY matches ..82 at @s if blocks ~-2 ~-8 ~-1 ~3 ~-8 ~1 ~ 255 ~ all run function tdh:ambient/villonne/grue_est/anim_down
execute if block 726 69 1110 lever[powered=false] if score @s posZ matches 1110 if score @s posX matches 735..743 if score @s posY matches ..81 run function tdh:ambient/villonne/grue_est/anim_up

# False => vers l'arrière
# True => vers l'avant
execute if block 727 69 1111 lever[powered=true] if block 726 69 1110 lever[powered=false] if score @s posZ matches 1110 if score @s posY matches 82 if score @s posX matches 735..742 run function tdh:ambient/villonne/grue_est/anim_forward
execute if block 727 69 1111 lever[powered=false] if block 726 69 1110 lever[powered=false] if score @s posZ matches 1110 if score @s posY matches 82 if score @s posX matches 736..743 run function tdh:ambient/villonne/grue_est/anim_backward
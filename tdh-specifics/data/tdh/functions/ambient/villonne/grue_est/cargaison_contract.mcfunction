setblock ~-1 ~-7 ~-2 air
setblock ~2 ~-7 ~-2 air
setblock ~-1 ~-7 ~2 air
setblock ~2 ~-7 ~2 air
fill ~-2 ~-8 ~-1 ~3 ~-8 ~1 air

fill ~0 ~-3 ~0 ~1 ~-4 ~0 spruce_fence
fill ~-2 ~-5 ~-1 ~3 ~-6 ~1 hay_block[axis=y]
fill ~0 ~-5 ~0 ~1 ~-5 ~0 jack_o_lantern
fill ~-2 ~-7 ~-1 ~3 ~-7 ~1 oak_slab[type=top]
setblock ~-2 ~-7 ~-1 oak_stairs[facing=north,half=top]
setblock ~3 ~-7 ~-1 oak_stairs[facing=north,half=top]
setblock ~-2 ~-7 ~1 oak_stairs[facing=south,half=top]
setblock ~3 ~-7 ~1 oak_stairs[facing=south,half=top]
fill ~-1 ~-4 ~-1 ~-1 ~-4 ~1 rail
fill ~2 ~-4 ~-1 ~2 ~-4 ~1 rail
setblock ~-1 ~-5 ~-2 ladder[facing=north]
setblock ~2 ~-5 ~-2 ladder[facing=north]
setblock ~-1 ~-5 ~2 ladder[facing=south]
setblock ~2 ~-5 ~2 ladder[facing=south]
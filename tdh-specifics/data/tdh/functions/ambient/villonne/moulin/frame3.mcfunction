#Aile droite
fill ~2 ~0 ~ ~4 ~0 ~ oak_planks keep
fill ~5 ~1 ~ ~7 ~1 ~ oak_planks keep
fill ~8 ~2 ~ ~11 ~2 ~ oak_planks keep
fill ~12 ~3 ~ ~15 ~3 ~ oak_planks keep

fill ~2 ~1 ~ ~4 ~1 ~ white_wool keep
fill ~4 ~2 ~ ~7 ~2 ~ white_wool keep
fill ~6 ~3 ~ ~11 ~3 ~ white_wool keep
fill ~8 ~4 ~ ~15 ~4 ~ white_wool keep
fill ~10 ~5 ~ ~15 ~5 ~ white_wool keep
fill ~12 ~6 ~ ~15 ~6 ~ white_wool keep
fill ~14 ~7 ~ ~15 ~7 ~ white_wool keep

#Aile basse
fill ~0 ~-2 ~ ~0 ~-4 ~ oak_planks keep
fill ~1 ~-5 ~ ~1 ~-7 ~ oak_planks keep
fill ~2 ~-8 ~ ~2 ~-11 ~ oak_planks keep
fill ~3 ~-12 ~ ~3 ~-15 ~ oak_planks keep

fill ~1 ~-2 ~ ~1 ~-4 ~ white_wool keep
fill ~2 ~-4 ~ ~2 ~-7 ~ white_wool keep
fill ~3 ~-6 ~ ~3 ~-11 ~ white_wool keep
fill ~4 ~-8 ~ ~4 ~-15 ~ white_wool keep
fill ~5 ~-10 ~ ~5 ~-15 ~ white_wool keep
fill ~6 ~-12 ~ ~6 ~-15 ~ white_wool keep
fill ~7 ~-14 ~ ~7 ~-15 ~ white_wool keep

#Aile gauche
fill ~-2 ~-0 ~ ~-4 ~-0 ~ oak_planks keep
fill ~-5 ~-1 ~ ~-7 ~-1 ~ oak_planks keep
fill ~-8 ~-2 ~ ~-11 ~-2 ~ oak_planks keep
fill ~-12 ~-3 ~ ~-15 ~-3 ~ oak_planks keep

fill ~-2 ~-1 ~ ~-4 ~-1 ~ white_wool keep
fill ~-4 ~-2 ~ ~-7 ~-2 ~ white_wool keep
fill ~-6 ~-3 ~ ~-11 ~-3 ~ white_wool keep
fill ~-8 ~-4 ~ ~-15 ~-4 ~ white_wool keep
fill ~-10 ~-5 ~ ~-15 ~-5 ~ white_wool keep
fill ~-12 ~-6 ~ ~-15 ~-6 ~ white_wool keep
fill ~-14 ~-7 ~ ~-15 ~-7 ~ white_wool keep

#Aile haute
fill ~-0 ~2 ~ ~-0 ~4 ~ oak_planks keep
fill ~-1 ~5 ~ ~-1 ~7 ~ oak_planks keep
fill ~-2 ~8 ~ ~-2 ~11 ~ oak_planks keep
fill ~-3 ~12 ~ ~-3 ~15 ~ oak_planks keep

fill ~-1 ~2 ~ ~-1 ~4 ~ white_wool keep
fill ~-2 ~4 ~ ~-2 ~7 ~ white_wool keep
fill ~-3 ~6 ~ ~-3 ~11 ~ white_wool keep
fill ~-4 ~8 ~ ~-4 ~15 ~ white_wool keep
fill ~-5 ~10 ~ ~-5 ~15 ~ white_wool keep
fill ~-6 ~12 ~ ~-6 ~15 ~ white_wool keep
fill ~-7 ~14 ~ ~-7 ~15 ~ white_wool keep
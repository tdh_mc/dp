#Aile droite
setblock ~2 ~0 ~ oak_planks
fill ~3 ~1 ~ ~4 ~1 ~ oak_planks
fill ~5 ~2 ~ ~6 ~2 ~ oak_planks
fill ~7 ~3 ~ ~8 ~3 ~ oak_planks
fill ~9 ~4 ~ ~10 ~4 ~ oak_planks
fill ~11 ~5 ~ ~12 ~5 ~ oak_planks
fill ~13 ~6 ~ ~14 ~6 ~ oak_planks

setblock ~2 ~1 ~ white_wool
fill ~2 ~2 ~ ~4 ~2 ~ white_wool
fill ~4 ~3 ~ ~6 ~3 ~ white_wool
fill ~5 ~4 ~ ~8 ~4 ~ white_wool
fill ~6 ~5 ~ ~10 ~5 ~ white_wool
fill ~8 ~6 ~ ~12 ~6 ~ white_wool
fill ~9 ~7 ~ ~14 ~7 ~ white_wool
fill ~10 ~8 ~ ~14 ~8 ~ white_wool
fill ~12 ~9 ~ ~13 ~9 ~ white_wool
setblock ~13 ~10 ~ white_wool

setblock ~1 ~2 ~ air
fill ~0 ~3 ~ ~3 ~3 ~ air
fill ~2 ~4 ~ ~4 ~4 ~ air
fill ~3 ~5 ~ ~5 ~5 ~ air
fill ~4 ~6 ~ ~7 ~6 ~ air
fill ~5 ~7 ~ ~8 ~7 ~ air
fill ~6 ~8 ~ ~9 ~8 ~ air
fill ~7 ~9 ~ ~11 ~9 ~ air
fill ~8 ~10 ~ ~12 ~10 ~ air
fill ~9 ~11 ~ ~11 ~11 ~ air
setblock ~10 ~12 ~ air

#Aile basse
setblock ~0 ~-2 ~ oak_planks
fill ~1 ~-3 ~ ~1 ~-4 ~ oak_planks
fill ~2 ~-5 ~ ~2 ~-6 ~ oak_planks
fill ~3 ~-7 ~ ~3 ~-8 ~ oak_planks
fill ~4 ~-9 ~ ~4 ~-10 ~ oak_planks
fill ~5 ~-11 ~ ~5 ~-12 ~ oak_planks
fill ~6 ~-13 ~ ~6 ~-14 ~ oak_planks

setblock ~1 ~-2 ~ white_wool
fill ~2 ~-2 ~ ~2 ~-4 ~ white_wool
fill ~3 ~-4 ~ ~3 ~-6 ~ white_wool
fill ~4 ~-5 ~ ~4 ~-8 ~ white_wool
fill ~5 ~-6 ~ ~5 ~-10 ~ white_wool
fill ~6 ~-8 ~ ~6 ~-12 ~ white_wool
fill ~7 ~-9 ~ ~7 ~-14 ~ white_wool
fill ~8 ~-10 ~ ~8 ~-14 ~ white_wool
fill ~9 ~-12 ~ ~9 ~-13 ~ white_wool
setblock ~10 ~-13 ~ white_wool

setblock ~2 ~-1 ~ air
fill ~3 ~-0 ~ ~3 ~-3 ~ air
fill ~4 ~-2 ~ ~4 ~-4 ~ air
fill ~5 ~-3 ~ ~5 ~-5 ~ air
fill ~6 ~-4 ~ ~6 ~-7 ~ air
fill ~7 ~-5 ~ ~7 ~-8 ~ air
fill ~8 ~-6 ~ ~8 ~-9 ~ air
fill ~9 ~-7 ~ ~9 ~-11 ~ air
fill ~10 ~-8 ~ ~10 ~-12 ~ air
fill ~11 ~-9 ~ ~11 ~-11 ~ air
setblock ~12 ~-10 ~ air

#Aile gauche
setblock ~-2 ~-0 ~ oak_planks
fill ~-3 ~-1 ~ ~-4 ~-1 ~ oak_planks
fill ~-5 ~-2 ~ ~-6 ~-2 ~ oak_planks
fill ~-7 ~-3 ~ ~-8 ~-3 ~ oak_planks
fill ~-9 ~-4 ~ ~-10 ~-4 ~ oak_planks
fill ~-11 ~-5 ~ ~-12 ~-5 ~ oak_planks
fill ~-13 ~-6 ~ ~-14 ~-6 ~ oak_planks

setblock ~-2 ~-1 ~ white_wool
fill ~-2 ~-2 ~ ~-4 ~-2 ~ white_wool
fill ~-4 ~-3 ~ ~-6 ~-3 ~ white_wool
fill ~-5 ~-4 ~ ~-8 ~-4 ~ white_wool
fill ~-6 ~-5 ~ ~-10 ~-5 ~ white_wool
fill ~-8 ~-6 ~ ~-12 ~-6 ~ white_wool
fill ~-9 ~-7 ~ ~-14 ~-7 ~ white_wool
fill ~-10 ~-8 ~ ~-14 ~-8 ~ white_wool
fill ~-12 ~-9 ~ ~-13 ~-9 ~ white_wool
setblock ~-13 ~-10 ~ white_wool

setblock ~-1 ~-2 ~ air
fill ~-0 ~-3 ~ ~-3 ~-3 ~ air
fill ~-2 ~-4 ~ ~-4 ~-4 ~ air
fill ~-3 ~-5 ~ ~-5 ~-5 ~ air
fill ~-4 ~-6 ~ ~-7 ~-6 ~ air
fill ~-5 ~-7 ~ ~-8 ~-7 ~ air
fill ~-6 ~-8 ~ ~-9 ~-8 ~ air
fill ~-7 ~-9 ~ ~-11 ~-9 ~ air
fill ~-8 ~-10 ~ ~-12 ~-10 ~ air
fill ~-9 ~-11 ~ ~-11 ~-11 ~ air
setblock ~-10 ~-12 ~ air

#Aile haute
setblock ~-0 ~2 ~ oak_planks
fill ~-1 ~3 ~ ~-1 ~4 ~ oak_planks
fill ~-2 ~5 ~ ~-2 ~6 ~ oak_planks
fill ~-3 ~7 ~ ~-3 ~8 ~ oak_planks
fill ~-4 ~9 ~ ~-4 ~10 ~ oak_planks
fill ~-5 ~11 ~ ~-5 ~12 ~ oak_planks
fill ~-6 ~13 ~ ~-6 ~14 ~ oak_planks

setblock ~-1 ~2 ~ white_wool
fill ~-2 ~2 ~ ~-2 ~4 ~ white_wool
fill ~-3 ~4 ~ ~-3 ~6 ~ white_wool
fill ~-4 ~5 ~ ~-4 ~8 ~ white_wool
fill ~-5 ~6 ~ ~-5 ~10 ~ white_wool
fill ~-6 ~8 ~ ~-6 ~12 ~ white_wool
fill ~-7 ~9 ~ ~-7 ~14 ~ white_wool
fill ~-8 ~10 ~ ~-8 ~14 ~ white_wool
fill ~-9 ~12 ~ ~-9 ~13 ~ white_wool
setblock ~-10 ~13 ~ white_wool

setblock ~-2 ~1 ~ air
fill ~-3 ~0 ~ ~-3 ~3 ~ air
fill ~-4 ~2 ~ ~-4 ~4 ~ air
fill ~-5 ~3 ~ ~-5 ~5 ~ air
fill ~-6 ~4 ~ ~-6 ~7 ~ air
fill ~-7 ~5 ~ ~-7 ~8 ~ air
fill ~-8 ~6 ~ ~-8 ~9 ~ air
fill ~-9 ~7 ~ ~-9 ~11 ~ air
fill ~-10 ~8 ~ ~-10 ~12 ~ air
fill ~-11 ~9 ~ ~-11 ~11 ~ air
setblock ~-12 ~10 ~ air
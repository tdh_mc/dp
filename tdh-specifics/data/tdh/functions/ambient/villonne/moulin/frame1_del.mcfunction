#Aile haute
execute if block ~1 ~2 ~ oak_planks run setblock ~1 ~2 ~ air
execute if block ~2 ~2 ~ oak_planks run setblock ~2 ~2 ~ air
execute if block ~3 ~3 ~ oak_planks run setblock ~3 ~3 ~ air
execute if block ~4 ~4 ~ oak_planks run setblock ~4 ~4 ~ air
execute if block ~5 ~4 ~ oak_planks run setblock ~5 ~4 ~ air
execute if block ~6 ~5 ~ oak_planks run setblock ~6 ~5 ~ air
execute if block ~7 ~6 ~ oak_planks run setblock ~7 ~6 ~ air
execute if block ~8 ~6 ~ oak_planks run setblock ~8 ~6 ~ air
execute if block ~9 ~7 ~ oak_planks run setblock ~9 ~7 ~ air
execute if block ~10 ~8 ~ oak_planks run setblock ~10 ~8 ~ air
execute if block ~11 ~8 ~ oak_planks run setblock ~11 ~8 ~ air
execute if block ~12 ~9 ~ oak_planks run setblock ~12 ~9 ~ air

execute if block ~0 ~2 ~ white_wool run setblock ~0 ~2 ~ air
fill ~0 ~3 ~ ~2 ~3 ~ air replace white_wool
fill ~2 ~4 ~ ~3 ~4 ~ air replace white_wool
fill ~3 ~5 ~ ~5 ~5 ~ air replace white_wool
execute if block ~4 ~6 ~ white_wool run setblock ~4 ~6 ~ air
fill ~5 ~6 ~ ~6 ~7 ~ air replace white_wool
execute if block ~6 ~8 ~ white_wool run setblock ~6 ~8 ~ air
fill ~7 ~7 ~ ~8 ~9 ~ air replace white_wool
execute if block ~8 ~10 ~ white_wool run setblock ~8 ~10 ~ air
execute if block ~9 ~8 ~ white_wool run setblock ~9 ~8 ~ air
fill ~9 ~9 ~ ~11 ~11 ~ air replace white_wool
execute if block ~10 ~12 ~ white_wool run setblock ~10 ~12 ~ air
execute if block ~12 ~10 ~ white_wool run setblock ~12 ~10 ~ air

#Aile droite
execute if block ~2 ~-1 ~ oak_planks run setblock ~2 ~-1 ~ air
execute if block ~2 ~-2 ~ oak_planks run setblock ~2 ~-2 ~ air
execute if block ~3 ~-3 ~ oak_planks run setblock ~3 ~-3 ~ air
execute if block ~4 ~-4 ~ oak_planks run setblock ~4 ~-4 ~ air
execute if block ~4 ~-5 ~ oak_planks run setblock ~4 ~-5 ~ air
execute if block ~5 ~-6 ~ oak_planks run setblock ~5 ~-6 ~ air
execute if block ~6 ~-7 ~ oak_planks run setblock ~6 ~-7 ~ air
execute if block ~6 ~-8 ~ oak_planks run setblock ~6 ~-8 ~ air
execute if block ~7 ~-9 ~ oak_planks run setblock ~7 ~-9 ~ air
execute if block ~8 ~-10 ~ oak_planks run setblock ~8 ~-10 ~ air
execute if block ~8 ~-11 ~ oak_planks run setblock ~8 ~-11 ~ air
execute if block ~9 ~-12 ~ oak_planks run setblock ~9 ~-12 ~ air

execute if block ~2 ~0 ~ white_wool run setblock ~2 ~0 ~ air
fill ~3 ~-0 ~ ~3 ~-2 ~ air replace white_wool
fill ~4 ~-2 ~ ~4 ~-3 ~ air replace white_wool
fill ~5 ~-3 ~ ~5 ~-5 ~ air replace white_wool
execute if block ~6 ~-4 ~ white_wool run setblock ~6 ~-4 ~ air
fill ~6 ~-5 ~ ~7 ~-6 ~ air replace white_wool
execute if block ~8 ~-6 ~ white_wool run setblock ~8 ~-6 ~ air
fill ~7 ~-7 ~ ~9 ~-8 ~ air replace white_wool
execute if block ~10 ~-8 ~ white_wool run setblock ~10 ~-8 ~ air
execute if block ~8 ~-9 ~ white_wool run setblock ~8 ~-9 ~ air
fill ~9 ~-9 ~ ~11 ~-11 ~ air replace white_wool
execute if block ~12 ~-10 ~ white_wool run setblock ~12 ~-10 ~ air
execute if block ~10 ~-12 ~ white_wool run setblock ~10 ~-12 ~ air

#Aile basse
execute if block ~-1 ~-2 ~ oak_planks run setblock ~-1 ~-2 ~ air
execute if block ~-2 ~-2 ~ oak_planks run setblock ~-2 ~-2 ~ air
execute if block ~-3 ~-3 ~ oak_planks run setblock ~-3 ~-3 ~ air
execute if block ~-4 ~-4 ~ oak_planks run setblock ~-4 ~-4 ~ air
execute if block ~-5 ~-4 ~ oak_planks run setblock ~-5 ~-4 ~ air
execute if block ~-6 ~-5 ~ oak_planks run setblock ~-6 ~-5 ~ air
execute if block ~-7 ~-6 ~ oak_planks run setblock ~-7 ~-6 ~ air
execute if block ~-8 ~-6 ~ oak_planks run setblock ~-8 ~-6 ~ air
execute if block ~-9 ~-7 ~ oak_planks run setblock ~-9 ~-7 ~ air
execute if block ~-10 ~-8 ~ oak_planks run setblock ~-10 ~-8 ~ air
execute if block ~-11 ~-8 ~ oak_planks run setblock ~-11 ~-8 ~ air
execute if block ~-12 ~-9 ~ oak_planks run setblock ~-12 ~-9 ~ air

execute if block ~-0 ~-2 ~ white_wool run setblock ~-0 ~-2 ~ air
fill ~-0 ~-3 ~ ~-2 ~-3 ~ air replace white_wool
fill ~-2 ~-4 ~ ~-3 ~-4 ~ air replace white_wool
fill ~-3 ~-5 ~ ~-5 ~-5 ~ air replace white_wool
execute if block ~-4 ~-6 ~ white_wool run setblock ~-4 ~-6 ~ air
fill ~-5 ~-6 ~ ~-6 ~-7 ~ air replace white_wool
execute if block ~-6 ~-8 ~ white_wool run setblock ~-6 ~-8 ~ air
fill ~-7 ~-7 ~ ~-8 ~-9 ~ air replace white_wool
execute if block ~-8 ~-10 ~ white_wool run setblock ~-8 ~-10 ~ air
execute if block ~-9 ~-8 ~ white_wool run setblock ~-9 ~-8 ~ air
fill ~-9 ~-9 ~ ~-11 ~-11 ~ air replace white_wool
execute if block ~-10 ~-12 ~ white_wool run setblock ~-10 ~-12 ~ air
execute if block ~-12 ~-10 ~ white_wool run setblock ~-12 ~-10 ~ air

#Aile gauche
execute if block ~-2 ~1 ~ oak_planks run setblock ~-2 ~1 ~ air
execute if block ~-2 ~2 ~ oak_planks run setblock ~-2 ~2 ~ air
execute if block ~-3 ~3 ~ oak_planks run setblock ~-3 ~3 ~ air
execute if block ~-4 ~4 ~ oak_planks run setblock ~-4 ~4 ~ air
execute if block ~-4 ~5 ~ oak_planks run setblock ~-4 ~5 ~ air
execute if block ~-5 ~6 ~ oak_planks run setblock ~-5 ~6 ~ air
execute if block ~-6 ~7 ~ oak_planks run setblock ~-6 ~7 ~ air
execute if block ~-6 ~8 ~ oak_planks run setblock ~-6 ~8 ~ air
execute if block ~-7 ~9 ~ oak_planks run setblock ~-7 ~9 ~ air
execute if block ~-8 ~10 ~ oak_planks run setblock ~-8 ~10 ~ air
execute if block ~-8 ~11 ~ oak_planks run setblock ~-8 ~11 ~ air
execute if block ~-9 ~12 ~ oak_planks run setblock ~-9 ~12 ~ air

execute if block ~-2 ~0 ~ white_wool run setblock ~-2 ~0 ~ air
fill ~-3 ~0 ~ ~-3 ~2 ~ air replace white_wool
fill ~-4 ~2 ~ ~-4 ~3 ~ air replace white_wool
fill ~-5 ~3 ~ ~-5 ~5 ~ air replace white_wool
execute if block ~-6 ~4 ~ white_wool run setblock ~-6 ~4 ~ air
fill ~-6 ~5 ~ ~-7 ~6 ~ air replace white_wool
execute if block ~-8 ~6 ~ white_wool run setblock ~-8 ~6 ~ air
fill ~-7 ~7 ~ ~-9 ~8 ~ air replace white_wool
execute if block ~-10 ~8 ~ white_wool run setblock ~-10 ~8 ~ air
execute if block ~-8 ~9 ~ white_wool run setblock ~-8 ~9 ~ air
fill ~-9 ~9 ~ ~-11 ~11 ~ air replace white_wool
execute if block ~-12 ~10 ~ white_wool run setblock ~-12 ~10 ~ air
execute if block ~-10 ~12 ~ white_wool run setblock ~-10 ~12 ~ air
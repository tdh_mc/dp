#Aile droite
fill ~5 ~0 ~ ~15 ~0 ~ oak_planks
fill ~5 ~1 ~ ~15 ~1 ~ white_wool
fill ~8 ~2 ~ ~15 ~2 ~ white_wool
fill ~12 ~3 ~ ~15 ~3 ~ white_wool

fill ~4 ~2 ~ ~5 ~2 ~ air
fill ~6 ~3 ~ ~9 ~3 ~ air
fill ~8 ~4 ~ ~13 ~4 ~ air
fill ~10 ~5 ~ ~15 ~5 ~ air
fill ~12 ~6 ~ ~15 ~6 ~ air
fill ~14 ~7 ~ ~15 ~7 ~ air

#Aile basse
fill ~0 ~-5 ~ ~0 ~-15 ~ oak_planks
fill ~1 ~-5 ~ ~1 ~-15 ~ white_wool
fill ~2 ~-8 ~ ~2 ~-15 ~ white_wool
fill ~3 ~-12 ~ ~3 ~-15 ~ white_wool

fill ~2 ~-4 ~ ~2 ~-5 ~ air
fill ~3 ~-6 ~ ~3 ~-9 ~ air
fill ~4 ~-8 ~ ~4 ~-13 ~ air
fill ~5 ~-10 ~ ~5 ~-15 ~ air
fill ~6 ~-12 ~ ~6 ~-15 ~ air
fill ~7 ~-14 ~ ~7 ~-15 ~ air

#Aile gauche
fill ~-5 ~-0 ~ ~-15 ~-0 ~ oak_planks
fill ~-5 ~-1 ~ ~-15 ~-1 ~ white_wool
fill ~-8 ~-2 ~ ~-15 ~-2 ~ white_wool
fill ~-12 ~-3 ~ ~-15 ~-3 ~ white_wool

fill ~-4 ~-2 ~ ~-5 ~-2 ~ air
fill ~-6 ~-3 ~ ~-9 ~-3 ~ air
fill ~-8 ~-4 ~ ~-13 ~-4 ~ air
fill ~-10 ~-5 ~ ~-15 ~-5 ~ air
fill ~-12 ~-6 ~ ~-15 ~-6 ~ air
fill ~-14 ~-7 ~ ~-15 ~-7 ~ air

#Aile haute
fill ~-0 ~5 ~ ~-0 ~15 ~ oak_planks
fill ~-1 ~5 ~ ~-1 ~15 ~ white_wool
fill ~-2 ~8 ~ ~-2 ~15 ~ white_wool
fill ~-3 ~12 ~ ~-3 ~15 ~ white_wool

fill ~-2 ~4 ~ ~-2 ~5 ~ air
fill ~-3 ~6 ~ ~-3 ~9 ~ air
fill ~-4 ~8 ~ ~-4 ~13 ~ air
fill ~-5 ~10 ~ ~-5 ~15 ~ air
fill ~-6 ~12 ~ ~-6 ~15 ~ air
fill ~-7 ~14 ~ ~-7 ~15 ~ air
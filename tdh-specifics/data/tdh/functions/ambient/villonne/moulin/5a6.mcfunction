#Aile haute
setblock ~2 ~4 ~ oak_planks
fill ~3 ~5 ~ ~3 ~6 ~ oak_planks
setblock ~4 ~7 ~ oak_planks
fill ~5 ~8 ~ ~5 ~9 ~ oak_planks
fill ~6 ~10 ~ ~6 ~11 ~ oak_planks
fill ~7 ~12 ~ ~7 ~13 ~ oak_planks

setblock ~1 ~4 ~ white_wool
fill ~2 ~5 ~ ~2 ~7 ~ white_wool
fill ~3 ~7 ~ ~3 ~11 ~ white_wool
fill ~4 ~8 ~ ~4 ~13 ~ white_wool
fill ~5 ~10 ~ ~5 ~13 ~ white_wool
fill ~6 ~12 ~ ~6 ~13 ~ white_wool

fill ~0 ~5 ~ ~0 ~15 ~ air
fill ~1 ~9 ~ ~1 ~15 ~ air
fill ~2 ~13 ~ ~2 ~15 ~ air
fill ~3 ~14 ~ ~4 ~15 ~ air

#Aile droite
setblock ~4 ~-2 ~ oak_planks
fill ~5 ~-3 ~ ~6 ~-3 ~ oak_planks
setblock ~7 ~-4 ~ oak_planks
fill ~8 ~-5 ~ ~9 ~-5 ~ oak_planks
fill ~10 ~-6 ~ ~11 ~-6 ~ oak_planks
fill ~12 ~-7 ~ ~13 ~-7 ~ oak_planks

setblock ~4 ~-1 ~ white_wool
fill ~5 ~-2 ~ ~7 ~-2 ~ white_wool
fill ~7 ~-3 ~ ~11 ~-3 ~ white_wool
fill ~8 ~-4 ~ ~13 ~-4 ~ white_wool
fill ~10 ~-5 ~ ~13 ~-5 ~ white_wool
fill ~12 ~-6 ~ ~13 ~-6 ~ white_wool

fill ~5 ~-0 ~ ~15 ~-0 ~ air
fill ~9 ~-1 ~ ~15 ~-1 ~ air
fill ~13 ~-2 ~ ~15 ~-2 ~ air
fill ~14 ~-3 ~ ~15 ~-4 ~ air

#Aile basse
setblock ~-2 ~-4 ~ oak_planks
fill ~-3 ~-5 ~ ~-3 ~-6 ~ oak_planks
setblock ~-4 ~-7 ~ oak_planks
fill ~-5 ~-8 ~ ~-5 ~-9 ~ oak_planks
fill ~-6 ~-10 ~ ~-6 ~-11 ~ oak_planks
fill ~-7 ~-12 ~ ~-7 ~-13 ~ oak_planks

setblock ~-1 ~-4 ~ white_wool
fill ~-2 ~-5 ~ ~-2 ~-7 ~ white_wool
fill ~-3 ~-7 ~ ~-3 ~-11 ~ white_wool
fill ~-4 ~-8 ~ ~-4 ~-13 ~ white_wool
fill ~-5 ~-10 ~ ~-5 ~-13 ~ white_wool
fill ~-6 ~-12 ~ ~-6 ~-13 ~ white_wool

fill ~-0 ~-5 ~ ~-0 ~-15 ~ air
fill ~-1 ~-9 ~ ~-1 ~-15 ~ air
fill ~-2 ~-13 ~ ~-2 ~-15 ~ air
fill ~-3 ~-14 ~ ~-4 ~-15 ~ air

#Aile gauche
setblock ~-4 ~2 ~ oak_planks
fill ~-5 ~3 ~ ~-6 ~3 ~ oak_planks
setblock ~-7 ~4 ~ oak_planks
fill ~-8 ~5 ~ ~-9 ~5 ~ oak_planks
fill ~-10 ~6 ~ ~-11 ~6 ~ oak_planks
fill ~-12 ~7 ~ ~-13 ~7 ~ oak_planks

setblock ~-4 ~1 ~ white_wool
fill ~-5 ~2 ~ ~-7 ~2 ~ white_wool
fill ~-7 ~3 ~ ~-11 ~3 ~ white_wool
fill ~-8 ~4 ~ ~-13 ~4 ~ white_wool
fill ~-10 ~5 ~ ~-13 ~5 ~ white_wool
fill ~-12 ~6 ~ ~-13 ~6 ~ white_wool

fill ~-5 ~0 ~ ~-15 ~0 ~ air
fill ~-9 ~1 ~ ~-15 ~1 ~ air
fill ~-13 ~2 ~ ~-15 ~2 ~ air
fill ~-14 ~3 ~ ~-15 ~4 ~ air
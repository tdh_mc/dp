#Aile droite
fill ~3 ~0 ~ ~4 ~0 ~ oak_planks
fill ~5 ~1 ~ ~7 ~1 ~ oak_planks
fill ~8 ~2 ~ ~11 ~2 ~ oak_planks
fill ~12 ~3 ~ ~15 ~3 ~ oak_planks

fill ~3 ~1 ~ ~4 ~1 ~ white_wool
fill ~5 ~2 ~ ~7 ~2 ~ white_wool
fill ~7 ~3 ~ ~11 ~3 ~ white_wool
fill ~9 ~4 ~ ~15 ~4 ~ white_wool
fill ~11 ~5 ~ ~15 ~5 ~ white_wool
fill ~13 ~6 ~ ~15 ~6 ~ white_wool
setblock ~15 ~7 ~ white_wool

fill ~2 ~2 ~ ~3 ~2 ~ air
fill ~4 ~3 ~ ~5 ~3 ~ air
fill ~5 ~4 ~ ~7 ~4 ~ air
fill ~6 ~5 ~ ~9 ~5 ~ air
fill ~8 ~6 ~ ~11 ~6 ~ air
fill ~9 ~7 ~ ~13 ~7 ~ air
fill ~10 ~8 ~ ~14 ~8 ~ air
fill ~12 ~9 ~ ~13 ~9 ~ air
setblock ~13 ~10 ~ air

#Aile basse
fill ~0 ~-3 ~ ~0 ~-4 ~ oak_planks
fill ~1 ~-5 ~ ~1 ~-7 ~ oak_planks
fill ~2 ~-8 ~ ~2 ~-11 ~ oak_planks
fill ~3 ~-12 ~ ~3 ~-15 ~ oak_planks

fill ~1 ~-3 ~ ~1 ~-4 ~ white_wool
fill ~2 ~-5 ~ ~2 ~-7 ~ white_wool
fill ~3 ~-7 ~ ~3 ~-11 ~ white_wool
fill ~4 ~-9 ~ ~4 ~-15 ~ white_wool
fill ~5 ~-11 ~ ~5 ~-15 ~ white_wool
fill ~6 ~-13 ~ ~6 ~-15 ~ white_wool
setblock ~7 ~-15 ~ white_wool

fill ~2 ~-2 ~ ~2 ~-3 ~ air
fill ~3 ~-4 ~ ~3 ~-5 ~ air
fill ~4 ~-5 ~ ~4 ~-7 ~ air
fill ~5 ~-6 ~ ~5 ~-9 ~ air
fill ~6 ~-8 ~ ~6 ~-11 ~ air
fill ~7 ~-9 ~ ~7 ~-13 ~ air
fill ~8 ~-10 ~ ~8 ~-14 ~ air
fill ~9 ~-12 ~ ~9 ~-13 ~ air
setblock ~10 ~-13 ~ air

#Aile gauche
fill ~-3 ~-0 ~ ~-4 ~-0 ~ oak_planks
fill ~-5 ~-1 ~ ~-7 ~-1 ~ oak_planks
fill ~-8 ~-2 ~ ~-11 ~-2 ~ oak_planks
fill ~-12 ~-3 ~ ~-15 ~-3 ~ oak_planks

fill ~-3 ~-1 ~ ~-4 ~-1 ~ white_wool
fill ~-5 ~-2 ~ ~-7 ~-2 ~ white_wool
fill ~-7 ~-3 ~ ~-11 ~-3 ~ white_wool
fill ~-9 ~-4 ~ ~-15 ~-4 ~ white_wool
fill ~-11 ~-5 ~ ~-15 ~-5 ~ white_wool
fill ~-13 ~-6 ~ ~-15 ~-6 ~ white_wool
setblock ~-15 ~-7 ~ white_wool

fill ~-2 ~-2 ~ ~-3 ~-2 ~ air
fill ~-4 ~-3 ~ ~-5 ~-3 ~ air
fill ~-5 ~-4 ~ ~-7 ~-4 ~ air
fill ~-6 ~-5 ~ ~-9 ~-5 ~ air
fill ~-8 ~-6 ~ ~-11 ~-6 ~ air
fill ~-9 ~-7 ~ ~-13 ~-7 ~ air
fill ~-10 ~-8 ~ ~-14 ~-8 ~ air
fill ~-12 ~-9 ~ ~-13 ~-9 ~ air
setblock ~-13 ~-10 ~ air

#Aile haute
fill ~-0 ~3 ~ ~-0 ~4 ~ oak_planks
fill ~-1 ~5 ~ ~-1 ~7 ~ oak_planks
fill ~-2 ~8 ~ ~-2 ~11 ~ oak_planks
fill ~-3 ~12 ~ ~-3 ~15 ~ oak_planks

fill ~-1 ~3 ~ ~-1 ~4 ~ white_wool
fill ~-2 ~5 ~ ~-2 ~7 ~ white_wool
fill ~-3 ~7 ~ ~-3 ~11 ~ white_wool
fill ~-4 ~9 ~ ~-4 ~15 ~ white_wool
fill ~-5 ~11 ~ ~-5 ~15 ~ white_wool
fill ~-6 ~13 ~ ~-6 ~15 ~ white_wool
setblock ~-7 ~15 ~ white_wool

fill ~-2 ~2 ~ ~-2 ~3 ~ air
fill ~-3 ~4 ~ ~-3 ~5 ~ air
fill ~-4 ~5 ~ ~-4 ~7 ~ air
fill ~-5 ~6 ~ ~-5 ~9 ~ air
fill ~-6 ~8 ~ ~-6 ~11 ~ air
fill ~-7 ~9 ~ ~-7 ~13 ~ air
fill ~-8 ~10 ~ ~-8 ~14 ~ air
fill ~-9 ~12 ~ ~-9 ~13 ~ air
setblock ~-10 ~13 ~ air
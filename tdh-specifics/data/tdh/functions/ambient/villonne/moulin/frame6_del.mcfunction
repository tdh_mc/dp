#Aile haute
fill ~1 ~2 ~ ~1 ~3 ~ air replace oak_planks
execute if block ~2 ~4 ~ oak_planks run setblock ~2 ~4 ~ air
fill ~3 ~5 ~ ~3 ~6 ~ air replace oak_planks
execute if block ~4 ~7 ~ oak_planks run setblock ~4 ~7 ~ air
fill ~5 ~8 ~ ~5 ~9 ~ air replace oak_planks
fill ~6 ~10 ~ ~6 ~11 ~ air replace oak_planks
fill ~7 ~12 ~ ~7 ~13 ~ air replace oak_planks

fill ~0 ~2 ~ ~0 ~4 ~ air replace white_wool
fill ~1 ~4 ~ ~1 ~8 ~ air replace white_wool
fill ~2 ~5 ~ ~2 ~12 ~ air replace white_wool
fill ~3 ~7 ~ ~3 ~13 ~ air replace white_wool
fill ~4 ~8 ~ ~4 ~13 ~ air replace white_wool
fill ~5 ~10 ~ ~5 ~13 ~ air replace white_wool
fill ~6 ~11 ~ ~6 ~13 ~ air replace white_wool

#Aile droite
fill ~2 ~-1 ~ ~3 ~-1 ~ air replace oak_planks
execute if block ~4 ~-2 ~ oak_planks run setblock ~4 ~-2 ~ air
fill ~5 ~-3 ~ ~6 ~-3 ~ air replace oak_planks
execute if block ~7 ~-4 ~ oak_planks run setblock ~7 ~-4 ~ air
fill ~8 ~-5 ~ ~9 ~-5 ~ air replace oak_planks
fill ~10 ~-6 ~ ~11 ~-6 ~ air replace oak_planks
fill ~12 ~-7 ~ ~13 ~-7 ~ air replace oak_planks

fill ~2 ~-0 ~ ~4 ~-0 ~ air replace white_wool
fill ~4 ~-1 ~ ~8 ~-1 ~ air replace white_wool
fill ~5 ~-2 ~ ~12 ~-2 ~ air replace white_wool
fill ~7 ~-3 ~ ~13 ~-3 ~ air replace white_wool
fill ~8 ~-4 ~ ~13 ~-4 ~ air replace white_wool
fill ~10 ~-5 ~ ~13 ~-5 ~ air replace white_wool
fill ~11 ~-6 ~ ~13 ~-6 ~ air replace white_wool

#Aile basse
fill ~-1 ~-2 ~ ~-1 ~-3 ~ air replace oak_planks
execute if block ~-2 ~-4 ~ oak_planks run setblock ~-2 ~-4 ~ air
fill ~-3 ~-5 ~ ~-3 ~-6 ~ air replace oak_planks
execute if block ~-4 ~-7 ~ oak_planks run setblock ~-4 ~-7 ~ air
fill ~-5 ~-8 ~ ~-5 ~-9 ~ air replace oak_planks
fill ~-6 ~-10 ~ ~-6 ~-11 ~ air replace oak_planks
fill ~-7 ~-12 ~ ~-7 ~-13 ~ air replace oak_planks

fill ~-0 ~-2 ~ ~-0 ~-4 ~ air replace white_wool
fill ~-1 ~-4 ~ ~-1 ~-8 ~ air replace white_wool
fill ~-2 ~-5 ~ ~-2 ~-12 ~ air replace white_wool
fill ~-3 ~-7 ~ ~-3 ~-13 ~ air replace white_wool
fill ~-4 ~-8 ~ ~-4 ~-13 ~ air replace white_wool
fill ~-5 ~-10 ~ ~-5 ~-13 ~ air replace white_wool
fill ~-6 ~-11 ~ ~-6 ~-13 ~ air replace white_wool

#Aile gauche
fill ~-2 ~1 ~ ~-3 ~1 ~ air replace oak_planks
execute if block ~-4 ~2 ~ oak_planks run setblock ~-4 ~2 ~ air
fill ~-5 ~3 ~ ~-6 ~3 ~ air replace oak_planks
execute if block ~-7 ~4 ~ oak_planks run setblock ~-7 ~4 ~ air
fill ~-8 ~5 ~ ~-9 ~5 ~ air replace oak_planks
fill ~-10 ~6 ~ ~-11 ~6 ~ air replace oak_planks
fill ~-12 ~7 ~ ~-13 ~7 ~ air replace oak_planks

fill ~-2 ~0 ~ ~-4 ~0 ~ air replace white_wool
fill ~-4 ~1 ~ ~-8 ~1 ~ air replace white_wool
fill ~-5 ~2 ~ ~-12 ~2 ~ air replace white_wool
fill ~-7 ~3 ~ ~-13 ~3 ~ air replace white_wool
fill ~-8 ~4 ~ ~-13 ~4 ~ air replace white_wool
fill ~-10 ~5 ~ ~-13 ~5 ~ air replace white_wool
fill ~-11 ~6 ~ ~-13 ~6 ~ air replace white_wool
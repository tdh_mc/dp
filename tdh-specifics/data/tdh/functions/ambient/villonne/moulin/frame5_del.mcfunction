#Aile haute
fill ~1 ~2 ~ ~1 ~4 ~ air replace oak_planks
fill ~2 ~5 ~ ~2 ~7 ~ air replace oak_planks
fill ~3 ~8 ~ ~3 ~11 ~ air replace oak_planks
fill ~4 ~12 ~ ~4 ~15 ~ air replace oak_planks

fill ~0 ~2 ~ ~0 ~15 ~ air replace white_wool
fill ~1 ~5 ~ ~1 ~15 ~ air replace white_wool
fill ~2 ~8 ~ ~2 ~15 ~ air replace white_wool
fill ~3 ~12 ~ ~3 ~15 ~ air replace white_wool

#Aile droite
fill ~2 ~-1 ~ ~4 ~-1 ~ air replace oak_planks
fill ~5 ~-2 ~ ~7 ~-2 ~ air replace oak_planks
fill ~8 ~-3 ~ ~11 ~-3 ~ air replace oak_planks
fill ~12 ~-4 ~ ~15 ~-4 ~ air replace oak_planks

fill ~2 ~-0 ~ ~15 ~-0 ~ air replace white_wool
fill ~5 ~-1 ~ ~15 ~-1 ~ air replace white_wool
fill ~8 ~-2 ~ ~15 ~-2 ~ air replace white_wool
fill ~12 ~-3 ~ ~15 ~-3 ~ air replace white_wool

#Aile basse
fill ~-1 ~-2 ~ ~-1 ~-4 ~ air replace oak_planks
fill ~-2 ~-5 ~ ~-2 ~-7 ~ air replace oak_planks
fill ~-3 ~-8 ~ ~-3 ~-11 ~ air replace oak_planks
fill ~-4 ~-12 ~ ~-4 ~-15 ~ air replace oak_planks

fill ~-0 ~-2 ~ ~-0 ~-15 ~ air replace white_wool
fill ~-1 ~-5 ~ ~-1 ~-15 ~ air replace white_wool
fill ~-2 ~-8 ~ ~-2 ~-15 ~ air replace white_wool
fill ~-3 ~-12 ~ ~-3 ~-15 ~ air replace white_wool

#Aile gauche
fill ~-2 ~1 ~ ~-4 ~1 ~ air replace oak_planks
fill ~-5 ~2 ~ ~-7 ~2 ~ air replace oak_planks
fill ~-8 ~3 ~ ~-11 ~3 ~ air replace oak_planks
fill ~-12 ~4 ~ ~-15 ~4 ~ air replace oak_planks

fill ~-2 ~0 ~ ~-15 ~0 ~ air replace white_wool
fill ~-5 ~1 ~ ~-15 ~1 ~ air replace white_wool
fill ~-8 ~2 ~ ~-15 ~2 ~ air replace white_wool
fill ~-12 ~3 ~ ~-15 ~3 ~ air replace white_wool
#Aile droite
fill ~2 ~0 ~ ~4 ~0 ~ air replace oak_planks
fill ~5 ~1 ~ ~7 ~1 ~ air replace oak_planks
fill ~8 ~2 ~ ~11 ~2 ~ air replace oak_planks
fill ~12 ~3 ~ ~15 ~3 ~ air replace oak_planks

fill ~2 ~1 ~ ~4 ~1 ~ air replace white_wool
fill ~4 ~2 ~ ~7 ~2 ~ air replace white_wool
fill ~6 ~3 ~ ~11 ~3 ~ air replace white_wool
fill ~8 ~4 ~ ~15 ~4 ~ air replace white_wool
fill ~10 ~5 ~ ~15 ~5 ~ air replace white_wool
fill ~12 ~6 ~ ~15 ~6 ~ air replace white_wool
fill ~14 ~7 ~ ~15 ~7 ~ air replace white_wool

#Aile basse
fill ~0 ~-2 ~ ~0 ~-4 ~ air replace oak_planks
fill ~1 ~-5 ~ ~1 ~-7 ~ air replace oak_planks
fill ~2 ~-8 ~ ~2 ~-11 ~ air replace oak_planks
fill ~3 ~-12 ~ ~3 ~-15 ~ air replace oak_planks

fill ~1 ~-2 ~ ~1 ~-4 ~ air replace white_wool
fill ~2 ~-4 ~ ~2 ~-7 ~ air replace white_wool
fill ~3 ~-6 ~ ~3 ~-11 ~ air replace white_wool
fill ~4 ~-8 ~ ~4 ~-15 ~ air replace white_wool
fill ~5 ~-10 ~ ~5 ~-15 ~ air replace white_wool
fill ~6 ~-12 ~ ~6 ~-15 ~ air replace white_wool
fill ~7 ~-14 ~ ~7 ~-15 ~ air replace white_wool

#Aile gauche
fill ~-2 ~-0 ~ ~-4 ~-0 ~ air replace oak_planks
fill ~-5 ~-1 ~ ~-7 ~-1 ~ air replace oak_planks
fill ~-8 ~-2 ~ ~-11 ~-2 ~ air replace oak_planks
fill ~-12 ~-3 ~ ~-15 ~-3 ~ air replace oak_planks

fill ~-2 ~-1 ~ ~-4 ~-1 ~ air replace white_wool
fill ~-4 ~-2 ~ ~-7 ~-2 ~ air replace white_wool
fill ~-6 ~-3 ~ ~-11 ~-3 ~ air replace white_wool
fill ~-8 ~-4 ~ ~-15 ~-4 ~ air replace white_wool
fill ~-10 ~-5 ~ ~-15 ~-5 ~ air replace white_wool
fill ~-12 ~-6 ~ ~-15 ~-6 ~ air replace white_wool
fill ~-14 ~-7 ~ ~-15 ~-7 ~ air replace white_wool

#Aile haute
fill ~-0 ~2 ~ ~-0 ~4 ~ air replace oak_planks
fill ~-1 ~5 ~ ~-1 ~7 ~ air replace oak_planks
fill ~-2 ~8 ~ ~-2 ~11 ~ air replace oak_planks
fill ~-3 ~12 ~ ~-3 ~15 ~ air replace oak_planks

fill ~-1 ~2 ~ ~-1 ~4 ~ air replace white_wool
fill ~-2 ~4 ~ ~-2 ~7 ~ air replace white_wool
fill ~-3 ~6 ~ ~-3 ~11 ~ air replace white_wool
fill ~-4 ~8 ~ ~-4 ~15 ~ air replace white_wool
fill ~-5 ~10 ~ ~-5 ~15 ~ air replace white_wool
fill ~-6 ~12 ~ ~-6 ~15 ~ air replace white_wool
fill ~-7 ~14 ~ ~-7 ~15 ~ air replace white_wool
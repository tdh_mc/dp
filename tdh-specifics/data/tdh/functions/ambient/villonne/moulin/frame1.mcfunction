#Aile haute
setblock ~1 ~2 ~ oak_planks keep
setblock ~2 ~2 ~ oak_planks keep
setblock ~3 ~3 ~ oak_planks keep
setblock ~4 ~4 ~ oak_planks keep
setblock ~5 ~4 ~ oak_planks keep
setblock ~6 ~5 ~ oak_planks keep
setblock ~7 ~6 ~ oak_planks keep
setblock ~8 ~6 ~ oak_planks keep
setblock ~9 ~7 ~ oak_planks keep
setblock ~10 ~8 ~ oak_planks keep
setblock ~11 ~8 ~ oak_planks keep
setblock ~12 ~9 ~ oak_planks keep

setblock ~0 ~2 ~ white_wool keep
fill ~0 ~3 ~ ~2 ~3 ~ white_wool keep
fill ~2 ~4 ~ ~3 ~4 ~ white_wool keep
fill ~3 ~5 ~ ~5 ~5 ~ white_wool keep
setblock ~4 ~6 ~ white_wool keep
fill ~5 ~6 ~ ~6 ~7 ~ white_wool keep
setblock ~6 ~8 ~ white_wool keep
fill ~7 ~7 ~ ~8 ~9 ~ white_wool keep
setblock ~8 ~10 ~ white_wool keep
setblock ~9 ~8 ~ white_wool keep
fill ~9 ~9 ~ ~11 ~11 ~ white_wool keep
setblock ~10 ~12 ~ white_wool keep
setblock ~12 ~10 ~ white_wool keep

#Aile droite
setblock ~2 ~-1 ~ oak_planks keep
setblock ~2 ~-2 ~ oak_planks keep
setblock ~3 ~-3 ~ oak_planks keep
setblock ~4 ~-4 ~ oak_planks keep
setblock ~4 ~-5 ~ oak_planks keep
setblock ~5 ~-6 ~ oak_planks keep
setblock ~6 ~-7 ~ oak_planks keep
setblock ~6 ~-8 ~ oak_planks keep
setblock ~7 ~-9 ~ oak_planks keep
setblock ~8 ~-10 ~ oak_planks keep
setblock ~8 ~-11 ~ oak_planks keep
setblock ~9 ~-12 ~ oak_planks keep

setblock ~2 ~0 ~ white_wool keep
fill ~3 ~-0 ~ ~3 ~-2 ~ white_wool keep
fill ~4 ~-2 ~ ~4 ~-3 ~ white_wool keep
fill ~5 ~-3 ~ ~5 ~-5 ~ white_wool keep
setblock ~6 ~-4 ~ white_wool keep
fill ~6 ~-5 ~ ~7 ~-6 ~ white_wool keep
setblock ~8 ~-6 ~ white_wool keep
fill ~7 ~-7 ~ ~9 ~-8 ~ white_wool keep
setblock ~10 ~-8 ~ white_wool keep
setblock ~8 ~-9 ~ white_wool keep
fill ~9 ~-9 ~ ~11 ~-11 ~ white_wool keep
setblock ~12 ~-10 ~ white_wool keep
setblock ~10 ~-12 ~ white_wool keep

#Aile basse
setblock ~-1 ~-2 ~ oak_planks keep
setblock ~-2 ~-2 ~ oak_planks keep
setblock ~-3 ~-3 ~ oak_planks keep
setblock ~-4 ~-4 ~ oak_planks keep
setblock ~-5 ~-4 ~ oak_planks keep
setblock ~-6 ~-5 ~ oak_planks keep
setblock ~-7 ~-6 ~ oak_planks keep
setblock ~-8 ~-6 ~ oak_planks keep
setblock ~-9 ~-7 ~ oak_planks keep
setblock ~-10 ~-8 ~ oak_planks keep
setblock ~-11 ~-8 ~ oak_planks keep
setblock ~-12 ~-9 ~ oak_planks keep

setblock ~-0 ~-2 ~ white_wool keep
fill ~-0 ~-3 ~ ~-2 ~-3 ~ white_wool keep
fill ~-2 ~-4 ~ ~-3 ~-4 ~ white_wool keep
fill ~-3 ~-5 ~ ~-5 ~-5 ~ white_wool keep
setblock ~-4 ~-6 ~ white_wool keep
fill ~-5 ~-6 ~ ~-6 ~-7 ~ white_wool keep
setblock ~-6 ~-8 ~ white_wool keep
fill ~-7 ~-7 ~ ~-8 ~-9 ~ white_wool keep
setblock ~-8 ~-10 ~ white_wool keep
setblock ~-9 ~-8 ~ white_wool keep
fill ~-9 ~-9 ~ ~-11 ~-11 ~ white_wool keep
setblock ~-10 ~-12 ~ white_wool keep
setblock ~-12 ~-10 ~ white_wool keep

#Aile gauche
setblock ~-2 ~1 ~ oak_planks keep
setblock ~-2 ~2 ~ oak_planks keep
setblock ~-3 ~3 ~ oak_planks keep
setblock ~-4 ~4 ~ oak_planks keep
setblock ~-4 ~5 ~ oak_planks keep
setblock ~-5 ~6 ~ oak_planks keep
setblock ~-6 ~7 ~ oak_planks keep
setblock ~-6 ~8 ~ oak_planks keep
setblock ~-7 ~9 ~ oak_planks keep
setblock ~-8 ~10 ~ oak_planks keep
setblock ~-8 ~11 ~ oak_planks keep
setblock ~-9 ~12 ~ oak_planks keep

setblock ~-2 ~0 ~ white_wool keep
fill ~-3 ~0 ~ ~-3 ~2 ~ white_wool keep
fill ~-4 ~2 ~ ~-4 ~3 ~ white_wool keep
fill ~-5 ~3 ~ ~-5 ~5 ~ white_wool keep
setblock ~-6 ~4 ~ white_wool keep
fill ~-6 ~5 ~ ~-7 ~6 ~ white_wool keep
setblock ~-8 ~6 ~ white_wool keep
fill ~-7 ~7 ~ ~-9 ~8 ~ white_wool keep
setblock ~-10 ~8 ~ white_wool keep
setblock ~-8 ~9 ~ white_wool keep
fill ~-9 ~9 ~ ~-11 ~11 ~ white_wool keep
setblock ~-12 ~10 ~ white_wool keep
setblock ~-10 ~12 ~ white_wool keep
#Aile haute
fill ~1 ~2 ~ ~1 ~4 ~ oak_planks keep
fill ~2 ~5 ~ ~2 ~7 ~ oak_planks keep
fill ~3 ~8 ~ ~3 ~11 ~ oak_planks keep
fill ~4 ~12 ~ ~4 ~15 ~ oak_planks keep

fill ~0 ~2 ~ ~0 ~15 ~ white_wool keep
fill ~1 ~5 ~ ~1 ~15 ~ white_wool keep
fill ~2 ~8 ~ ~2 ~15 ~ white_wool keep
fill ~3 ~12 ~ ~3 ~15 ~ white_wool keep

#Aile droite
fill ~2 ~-1 ~ ~4 ~-1 ~ oak_planks keep
fill ~5 ~-2 ~ ~7 ~-2 ~ oak_planks keep
fill ~8 ~-3 ~ ~11 ~-3 ~ oak_planks keep
fill ~12 ~-4 ~ ~15 ~-4 ~ oak_planks keep

fill ~2 ~-0 ~ ~15 ~-0 ~ white_wool keep
fill ~5 ~-1 ~ ~15 ~-1 ~ white_wool keep
fill ~8 ~-2 ~ ~15 ~-2 ~ white_wool keep
fill ~12 ~-3 ~ ~15 ~-3 ~ white_wool keep

#Aile basse
fill ~-1 ~-2 ~ ~-1 ~-4 ~ oak_planks keep
fill ~-2 ~-5 ~ ~-2 ~-7 ~ oak_planks keep
fill ~-3 ~-8 ~ ~-3 ~-11 ~ oak_planks keep
fill ~-4 ~-12 ~ ~-4 ~-15 ~ oak_planks keep

fill ~-0 ~-2 ~ ~-0 ~-15 ~ white_wool keep
fill ~-1 ~-5 ~ ~-1 ~-15 ~ white_wool keep
fill ~-2 ~-8 ~ ~-2 ~-15 ~ white_wool keep
fill ~-3 ~-12 ~ ~-3 ~-15 ~ white_wool keep

#Aile gauche
fill ~-2 ~1 ~ ~-4 ~1 ~ oak_planks keep
fill ~-5 ~2 ~ ~-7 ~2 ~ oak_planks keep
fill ~-8 ~3 ~ ~-11 ~3 ~ oak_planks keep
fill ~-12 ~4 ~ ~-15 ~4 ~ oak_planks keep

fill ~-2 ~0 ~ ~-15 ~0 ~ white_wool keep
fill ~-5 ~1 ~ ~-15 ~1 ~ white_wool keep
fill ~-8 ~2 ~ ~-15 ~2 ~ white_wool keep
fill ~-12 ~3 ~ ~-15 ~3 ~ white_wool keep
#Aile haute
setblock ~2 ~3 ~ oak_planks
setblock ~3 ~4 ~ oak_planks
setblock ~4 ~5 ~ oak_planks
setblock ~5 ~6 ~ oak_planks
setblock ~6 ~7 ~ oak_planks
setblock ~7 ~8 ~ oak_planks
setblock ~8 ~9 ~ oak_planks
setblock ~9 ~10 ~ oak_planks
setblock ~10 ~11 ~ oak_planks

setblock ~1 ~3 ~ white_wool
setblock ~2 ~4 ~ white_wool
fill ~3 ~5 ~ ~3 ~6 ~ white_wool
fill ~4 ~6 ~ ~4 ~7 ~ white_wool
fill ~5 ~7 ~ ~5 ~9 ~ white_wool
fill ~6 ~8 ~ ~6 ~11 ~ white_wool
fill ~7 ~9 ~ ~7 ~11 ~ white_wool
fill ~8 ~10 ~ ~8 ~11 ~ white_wool
setblock ~9 ~11 ~ white_wool

setblock ~0 ~4 ~ air
fill ~1 ~5 ~ ~1 ~8 ~ air
fill ~2 ~6 ~ ~2 ~12 ~ air
fill ~3 ~7 ~ ~3 ~13 ~ air
fill ~4 ~9 ~ ~4 ~13 ~ air
fill ~5 ~11 ~ ~5 ~13 ~ air
fill ~6 ~12 ~ ~7 ~13 ~ air

#Aile droite
setblock ~3 ~-2 ~ oak_planks
setblock ~4 ~-3 ~ oak_planks
setblock ~5 ~-4 ~ oak_planks
setblock ~6 ~-5 ~ oak_planks
setblock ~7 ~-6 ~ oak_planks
setblock ~8 ~-7 ~ oak_planks
setblock ~9 ~-8 ~ oak_planks
setblock ~10 ~-9 ~ oak_planks
setblock ~11 ~-10 ~ oak_planks

setblock ~3 ~-1 ~ white_wool
setblock ~4 ~-2 ~ white_wool
fill ~5 ~-3 ~ ~6 ~-3 ~ white_wool
fill ~6 ~-4 ~ ~7 ~-4 ~ white_wool
fill ~7 ~-5 ~ ~9 ~-5 ~ white_wool
fill ~8 ~-6 ~ ~11 ~-6 ~ white_wool
fill ~9 ~-7 ~ ~11 ~-7 ~ white_wool
fill ~10 ~-8 ~ ~11 ~-8 ~ white_wool
setblock ~11 ~-9 ~ white_wool

setblock ~4 ~-0 ~ air
fill ~5 ~-1 ~ ~8 ~-1 ~ air
fill ~6 ~-2 ~ ~12 ~-2 ~ air
fill ~7 ~-3 ~ ~13 ~-3 ~ air
fill ~9 ~-4 ~ ~13 ~-4 ~ air
fill ~11 ~-5 ~ ~13 ~-5 ~ air
fill ~12 ~-6 ~ ~13 ~-7 ~ air

#Aile basse
setblock ~-2 ~-3 ~ oak_planks
setblock ~-3 ~-4 ~ oak_planks
setblock ~-4 ~-5 ~ oak_planks
setblock ~-5 ~-6 ~ oak_planks
setblock ~-6 ~-7 ~ oak_planks
setblock ~-7 ~-8 ~ oak_planks
setblock ~-8 ~-9 ~ oak_planks
setblock ~-9 ~-10 ~ oak_planks
setblock ~-10 ~-11 ~ oak_planks

setblock ~-1 ~-3 ~ white_wool
setblock ~-2 ~-4 ~ white_wool
fill ~-3 ~-5 ~ ~-3 ~-6 ~ white_wool
fill ~-4 ~-6 ~ ~-4 ~-7 ~ white_wool
fill ~-5 ~-7 ~ ~-5 ~-9 ~ white_wool
fill ~-6 ~-8 ~ ~-6 ~-11 ~ white_wool
fill ~-7 ~-9 ~ ~-7 ~-11 ~ white_wool
fill ~-8 ~-10 ~ ~-8 ~-11 ~ white_wool
setblock ~-9 ~-11 ~ white_wool

setblock ~-0 ~-4 ~ air
fill ~-1 ~-5 ~ ~-1 ~-8 ~ air
fill ~-2 ~-6 ~ ~-2 ~-12 ~ air
fill ~-3 ~-7 ~ ~-3 ~-13 ~ air
fill ~-4 ~-9 ~ ~-4 ~-13 ~ air
fill ~-5 ~-11 ~ ~-5 ~-13 ~ air
fill ~-6 ~-12 ~ ~-7 ~-13 ~ air

#Aile haute
setblock ~-3 ~2 ~ oak_planks
setblock ~-4 ~3 ~ oak_planks
setblock ~-5 ~4 ~ oak_planks
setblock ~-6 ~5 ~ oak_planks
setblock ~-7 ~6 ~ oak_planks
setblock ~-8 ~7 ~ oak_planks
setblock ~-9 ~8 ~ oak_planks
setblock ~-10 ~9 ~ oak_planks
setblock ~-11 ~10 ~ oak_planks

setblock ~-3 ~1 ~ white_wool
setblock ~-4 ~2 ~ white_wool
fill ~-5 ~3 ~ ~-6 ~3 ~ white_wool
fill ~-6 ~4 ~ ~-7 ~4 ~ white_wool
fill ~-7 ~5 ~ ~-9 ~5 ~ white_wool
fill ~-8 ~6 ~ ~-11 ~6 ~ white_wool
fill ~-9 ~7 ~ ~-11 ~7 ~ white_wool
fill ~-10 ~8 ~ ~-11 ~8 ~ white_wool
setblock ~-11 ~9 ~ white_wool

setblock ~-4 ~0 ~ air
fill ~-5 ~1 ~ ~-8 ~1 ~ air
fill ~-6 ~2 ~ ~-12 ~2 ~ air
fill ~-7 ~3 ~ ~-13 ~3 ~ air
fill ~-9 ~4 ~ ~-13 ~4 ~ air
fill ~-11 ~5 ~ ~-13 ~5 ~ air
fill ~-12 ~6 ~ ~-13 ~7 ~ air
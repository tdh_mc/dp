#Aile haute
setblock ~2 ~2 ~ oak_planks
setblock ~3 ~3 ~ oak_planks
setblock ~4 ~4 ~ oak_planks
setblock ~5 ~4 ~ oak_planks
setblock ~6 ~5 ~ oak_planks
setblock ~7 ~6 ~ oak_planks
setblock ~8 ~6 ~ oak_planks
setblock ~9 ~7 ~ oak_planks
setblock ~10 ~8 ~ oak_planks
setblock ~11 ~8 ~ oak_planks
setblock ~12 ~9 ~ oak_planks

setblock ~2 ~3 ~ white_wool
setblock ~3 ~4 ~ white_wool
fill ~4 ~5 ~ ~5 ~5 ~ white_wool
fill ~5 ~6 ~ ~6 ~6 ~ white_wool
fill ~6 ~7 ~ ~8 ~7 ~ white_wool
fill ~7 ~8 ~ ~9 ~8 ~ white_wool
fill ~8 ~9 ~ ~11 ~9 ~ white_wool
fill ~9 ~10 ~ ~12 ~10 ~ white_wool
fill ~10 ~11 ~ ~11 ~11 ~ white_wool
setblock ~10 ~12 ~ white_wool

setblock ~1 ~4 ~ air
setblock ~2 ~5 ~ air
setblock ~3 ~6 ~ air
fill ~4 ~7 ~ ~4 ~8 ~ air
fill ~5 ~8 ~ ~5 ~10 ~ air
fill ~6 ~9 ~ ~6 ~11 ~ air
fill ~7 ~10 ~ ~7 ~11 ~ air
setblock ~8 ~11 ~ air

#Aile droite
setblock ~2 ~-2 ~ oak_planks
setblock ~3 ~-3 ~ oak_planks
setblock ~4 ~-4 ~ oak_planks
setblock ~4 ~-5 ~ oak_planks
setblock ~5 ~-6 ~ oak_planks
setblock ~6 ~-7 ~ oak_planks
setblock ~6 ~-8 ~ oak_planks
setblock ~7 ~-9 ~ oak_planks
setblock ~8 ~-10 ~ oak_planks
setblock ~8 ~-11 ~ oak_planks
setblock ~9 ~-12 ~ oak_planks

setblock ~3 ~-2 ~ white_wool
setblock ~4 ~-3 ~ white_wool
fill ~5 ~-4 ~ ~5 ~-5 ~ white_wool
fill ~6 ~-5 ~ ~6 ~-6 ~ white_wool
fill ~7 ~-6 ~ ~7 ~-8 ~ white_wool
fill ~8 ~-7 ~ ~8 ~-9 ~ white_wool
fill ~9 ~-8 ~ ~9 ~-11 ~ white_wool
fill ~10 ~-9 ~ ~10 ~-12 ~ white_wool
fill ~11 ~-10 ~ ~11 ~-11 ~ white_wool
setblock ~12 ~-10 ~ white_wool

setblock ~4 ~-1 ~ air
setblock ~5 ~-2 ~ air
setblock ~6 ~-3 ~ air
fill ~7 ~-4 ~ ~8 ~-4 ~ air
fill ~8 ~-5 ~ ~10 ~-5 ~ air
fill ~9 ~-6 ~ ~11 ~-6 ~ air
fill ~10 ~-7 ~ ~11 ~-7 ~ air
setblock ~11 ~-8 ~ air

#Aile basse
setblock ~-2 ~-2 ~ oak_planks
setblock ~-3 ~-3 ~ oak_planks
setblock ~-4 ~-4 ~ oak_planks
setblock ~-5 ~-4 ~ oak_planks
setblock ~-6 ~-5 ~ oak_planks
setblock ~-7 ~-6 ~ oak_planks
setblock ~-8 ~-6 ~ oak_planks
setblock ~-9 ~-7 ~ oak_planks
setblock ~-10 ~-8 ~ oak_planks
setblock ~-11 ~-8 ~ oak_planks
setblock ~-12 ~-9 ~ oak_planks

setblock ~-2 ~-3 ~ white_wool
setblock ~-3 ~-4 ~ white_wool
fill ~-4 ~-5 ~ ~-5 ~-5 ~ white_wool
fill ~-5 ~-6 ~ ~-6 ~-6 ~ white_wool
fill ~-6 ~-7 ~ ~-8 ~-7 ~ white_wool
fill ~-7 ~-8 ~ ~-9 ~-8 ~ white_wool
fill ~-8 ~-9 ~ ~-11 ~-9 ~ white_wool
fill ~-9 ~-10 ~ ~-12 ~-10 ~ white_wool
fill ~-10 ~-11 ~ ~-11 ~-11 ~ white_wool
setblock ~-10 ~-12 ~ white_wool

setblock ~-1 ~-4 ~ air
setblock ~-2 ~-5 ~ air
setblock ~-3 ~-6 ~ air
fill ~-4 ~-7 ~ ~-4 ~-8 ~ air
fill ~-5 ~-8 ~ ~-5 ~-10 ~ air
fill ~-6 ~-9 ~ ~-6 ~-11 ~ air
fill ~-7 ~-10 ~ ~-7 ~-11 ~ air
setblock ~-8 ~-11 ~ air

#Aile gauche
setblock ~-2 ~2 ~ oak_planks
setblock ~-3 ~3 ~ oak_planks
setblock ~-4 ~4 ~ oak_planks
setblock ~-4 ~5 ~ oak_planks
setblock ~-5 ~6 ~ oak_planks
setblock ~-6 ~7 ~ oak_planks
setblock ~-6 ~8 ~ oak_planks
setblock ~-7 ~9 ~ oak_planks
setblock ~-8 ~10 ~ oak_planks
setblock ~-8 ~11 ~ oak_planks
setblock ~-9 ~12 ~ oak_planks

setblock ~-3 ~2 ~ white_wool
setblock ~-4 ~3 ~ white_wool
fill ~-5 ~4 ~ ~-5 ~5 ~ white_wool
fill ~-6 ~5 ~ ~-6 ~6 ~ white_wool
fill ~-7 ~6 ~ ~-7 ~8 ~ white_wool
fill ~-8 ~7 ~ ~-8 ~9 ~ white_wool
fill ~-9 ~8 ~ ~-9 ~11 ~ white_wool
fill ~-10 ~9 ~ ~-10 ~12 ~ white_wool
fill ~-11 ~10 ~ ~-11 ~11 ~ white_wool
setblock ~-12 ~10 ~ white_wool

setblock ~-4 ~1 ~ air
setblock ~-5 ~2 ~ air
setblock ~-6 ~3 ~ air
fill ~-7 ~4 ~ ~-8 ~4 ~ air
fill ~-8 ~5 ~ ~-10 ~5 ~ air
fill ~-9 ~6 ~ ~-11 ~6 ~ air
fill ~-10 ~7 ~ ~-11 ~7 ~ air
setblock ~-11 ~8 ~ air
setblock ~-11 ~8 ~ air
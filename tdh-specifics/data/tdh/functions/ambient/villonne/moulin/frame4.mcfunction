#Aile droite
fill ~2 ~0 ~ ~15 ~0 ~ oak_planks keep
fill ~2 ~1 ~ ~15 ~1 ~ white_wool keep
fill ~6 ~2 ~ ~15 ~2 ~ white_wool keep
fill ~10 ~3 ~ ~15 ~3 ~ white_wool keep
fill ~14 ~4 ~ ~15 ~4 ~ white_wool keep

#Aile basse
fill ~0 ~-2 ~ ~0 ~-15 ~ oak_planks keep
fill ~1 ~-2 ~ ~1 ~-15 ~ white_wool keep
fill ~2 ~-6 ~ ~2 ~-15 ~ white_wool keep
fill ~3 ~-10 ~ ~3 ~-15 ~ white_wool keep
fill ~4 ~-14 ~ ~4 ~-15 ~ white_wool keep

#Aile gauche
fill ~-2 ~-0 ~ ~-15 ~-0 ~ oak_planks keep
fill ~-2 ~-1 ~ ~-15 ~-1 ~ white_wool keep
fill ~-6 ~-2 ~ ~-15 ~-2 ~ white_wool keep
fill ~-10 ~-3 ~ ~-15 ~-3 ~ white_wool keep
fill ~-14 ~-4 ~ ~-15 ~-4 ~ white_wool keep

#Aile haute
fill ~-0 ~2 ~ ~-0 ~15 ~ oak_planks keep
fill ~-1 ~2 ~ ~-1 ~15 ~ white_wool keep
fill ~-2 ~6 ~ ~-2 ~15 ~ white_wool keep
fill ~-3 ~10 ~ ~-3 ~15 ~ white_wool keep
fill ~-4 ~14 ~ ~-4 ~15 ~ white_wool keep
#Aile droite
fill ~2 ~0 ~ ~15 ~0 ~ air replace oak_planks
fill ~2 ~1 ~ ~15 ~1 ~ air replace white_wool
fill ~6 ~2 ~ ~15 ~2 ~ air replace white_wool
fill ~10 ~3 ~ ~15 ~3 ~ air replace white_wool
fill ~14 ~4 ~ ~15 ~4 ~ air replace white_wool

#Aile basse
fill ~0 ~-2 ~ ~0 ~-15 ~ air replace oak_planks
fill ~1 ~-2 ~ ~1 ~-15 ~ air replace white_wool
fill ~2 ~-6 ~ ~2 ~-15 ~ air replace white_wool
fill ~3 ~-10 ~ ~3 ~-15 ~ air replace white_wool
fill ~4 ~-14 ~ ~4 ~-15 ~ air replace white_wool

#Aile gauche
fill ~-2 ~-0 ~ ~-15 ~-0 ~ air replace oak_planks
fill ~-2 ~-1 ~ ~-15 ~-1 ~ air replace white_wool
fill ~-6 ~-2 ~ ~-15 ~-2 ~ air replace white_wool
fill ~-10 ~-3 ~ ~-15 ~-3 ~ air replace white_wool
fill ~-14 ~-4 ~ ~-15 ~-4 ~ air replace white_wool

#Aile haute
fill ~-0 ~2 ~ ~-0 ~15 ~ air replace oak_planks
fill ~-1 ~2 ~ ~-1 ~15 ~ air replace white_wool
fill ~-2 ~6 ~ ~-2 ~15 ~ air replace white_wool
fill ~-3 ~10 ~ ~-3 ~15 ~ air replace white_wool
fill ~-4 ~14 ~ ~-4 ~15 ~ air replace white_wool
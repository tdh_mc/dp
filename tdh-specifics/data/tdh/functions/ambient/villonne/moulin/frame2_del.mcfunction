#Aile droite
execute if block ~2 ~0 ~ oak_planks run setblock ~2 ~0 ~ air
fill ~3 ~1 ~ ~4 ~1 ~ air replace oak_planks
fill ~5 ~2 ~ ~6 ~2 ~ air replace oak_planks
fill ~7 ~3 ~ ~8 ~3 ~ air replace oak_planks
fill ~9 ~4 ~ ~10 ~4 ~ air replace oak_planks
fill ~11 ~5 ~ ~12 ~5 ~ air replace oak_planks
fill ~13 ~6 ~ ~14 ~6 ~ air replace oak_planks

execute if block ~2 ~1 ~ white_wool run setblock ~2 ~1 ~ air
fill ~2 ~2 ~ ~4 ~2 ~ air replace white_wool
fill ~4 ~3 ~ ~6 ~3 ~ air replace white_wool
fill ~5 ~4 ~ ~8 ~4 ~ air replace white_wool
fill ~6 ~5 ~ ~10 ~5 ~ air replace white_wool
fill ~8 ~6 ~ ~12 ~6 ~ air replace white_wool
fill ~9 ~7 ~ ~14 ~7 ~ air replace white_wool
fill ~10 ~8 ~ ~14 ~8 ~ air replace white_wool
fill ~12 ~9 ~ ~13 ~9 ~ air replace white_wool
execute if block ~13 ~10 ~ white_wool run setblock ~13 ~10 ~ air

#Aile basse
execute if block ~0 ~-2 ~ oak_planks run setblock ~0 ~-2 ~ air
fill ~1 ~-3 ~ ~1 ~-4 ~ air replace oak_planks
fill ~2 ~-5 ~ ~2 ~-6 ~ air replace oak_planks
fill ~3 ~-7 ~ ~3 ~-8 ~ air replace oak_planks
fill ~4 ~-9 ~ ~4 ~-10 ~ air replace oak_planks
fill ~5 ~-11 ~ ~5 ~-12 ~ air replace oak_planks
fill ~6 ~-13 ~ ~6 ~-14 ~ air replace oak_planks

execute if block ~1 ~-2 ~ white_wool run setblock ~1 ~-2 ~ air
fill ~2 ~-2 ~ ~2 ~-4 ~ air replace white_wool
fill ~3 ~-4 ~ ~3 ~-6 ~ air replace white_wool
fill ~4 ~-5 ~ ~4 ~-8 ~ air replace white_wool
fill ~5 ~-6 ~ ~5 ~-10 ~ air replace white_wool
fill ~6 ~-8 ~ ~6 ~-12 ~ air replace white_wool
fill ~7 ~-9 ~ ~7 ~-14 ~ air replace white_wool
fill ~8 ~-10 ~ ~8 ~-14 ~ air replace white_wool
fill ~9 ~-12 ~ ~9 ~-13 ~ air replace white_wool
execute if block ~10 ~-13 ~ white_wool run setblock ~10 ~-13 ~ air

#Aile gauche
execute if block ~-2 ~-0 ~ oak_planks run setblock ~-2 ~-0 ~ air
fill ~-3 ~-1 ~ ~-4 ~-1 ~ air replace oak_planks
fill ~-5 ~-2 ~ ~-6 ~-2 ~ air replace oak_planks
fill ~-7 ~-3 ~ ~-8 ~-3 ~ air replace oak_planks
fill ~-9 ~-4 ~ ~-10 ~-4 ~ air replace oak_planks
fill ~-11 ~-5 ~ ~-12 ~-5 ~ air replace oak_planks
fill ~-13 ~-6 ~ ~-14 ~-6 ~ air replace oak_planks

execute if block ~-2 ~-1 ~ white_wool run setblock ~-2 ~-1 ~ air
fill ~-2 ~-2 ~ ~-4 ~-2 ~ air replace white_wool
fill ~-4 ~-3 ~ ~-6 ~-3 ~ air replace white_wool
fill ~-5 ~-4 ~ ~-8 ~-4 ~ air replace white_wool
fill ~-6 ~-5 ~ ~-10 ~-5 ~ air replace white_wool
fill ~-8 ~-6 ~ ~-12 ~-6 ~ air replace white_wool
fill ~-9 ~-7 ~ ~-14 ~-7 ~ air replace white_wool
fill ~-10 ~-8 ~ ~-14 ~-8 ~ air replace white_wool
fill ~-12 ~-9 ~ ~-13 ~-9 ~ air replace white_wool
execute if block ~-13 ~-10 ~ white_wool run setblock ~-13 ~-10 ~ air

#Aile haute
execute if block ~-0 ~2 ~ oak_planks run setblock ~-0 ~2 ~ air
fill ~-1 ~3 ~ ~-1 ~4 ~ air replace oak_planks
fill ~-2 ~5 ~ ~-2 ~6 ~ air replace oak_planks
fill ~-3 ~7 ~ ~-3 ~8 ~ air replace oak_planks
fill ~-4 ~9 ~ ~-4 ~10 ~ air replace oak_planks
fill ~-5 ~11 ~ ~-5 ~12 ~ air replace oak_planks
fill ~-6 ~13 ~ ~-6 ~14 ~ air replace oak_planks

execute if block ~-1 ~2 ~ white_wool run setblock ~-1 ~2 ~ air
fill ~-2 ~2 ~ ~-2 ~4 ~ air replace white_wool
fill ~-3 ~4 ~ ~-3 ~6 ~ air replace white_wool
fill ~-4 ~5 ~ ~-4 ~8 ~ air replace white_wool
fill ~-5 ~6 ~ ~-5 ~10 ~ air replace white_wool
fill ~-6 ~8 ~ ~-6 ~12 ~ air replace white_wool
fill ~-7 ~9 ~ ~-7 ~14 ~ air replace white_wool
fill ~-8 ~10 ~ ~-8 ~14 ~ air replace white_wool
fill ~-9 ~12 ~ ~-9 ~13 ~ air replace white_wool
execute if block ~-10 ~13 ~ white_wool run setblock ~-10 ~13 ~ air
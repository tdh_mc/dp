#Aile droite
setblock ~2 ~0 ~ oak_planks keep
fill ~3 ~1 ~ ~4 ~1 ~ oak_planks keep
fill ~5 ~2 ~ ~6 ~2 ~ oak_planks keep
fill ~7 ~3 ~ ~8 ~3 ~ oak_planks keep
fill ~9 ~4 ~ ~10 ~4 ~ oak_planks keep
fill ~11 ~5 ~ ~12 ~5 ~ oak_planks keep
fill ~13 ~6 ~ ~14 ~6 ~ oak_planks keep

setblock ~2 ~1 ~ white_wool keep
fill ~2 ~2 ~ ~4 ~2 ~ white_wool keep
fill ~4 ~3 ~ ~6 ~3 ~ white_wool keep
fill ~5 ~4 ~ ~8 ~4 ~ white_wool keep
fill ~6 ~5 ~ ~10 ~5 ~ white_wool keep
fill ~8 ~6 ~ ~12 ~6 ~ white_wool keep
fill ~9 ~7 ~ ~14 ~7 ~ white_wool keep
fill ~10 ~8 ~ ~14 ~8 ~ white_wool keep
fill ~12 ~9 ~ ~13 ~9 ~ white_wool keep
setblock ~13 ~10 ~ white_wool keep

#Aile basse
setblock ~0 ~-2 ~ oak_planks keep
fill ~1 ~-3 ~ ~1 ~-4 ~ oak_planks keep
fill ~2 ~-5 ~ ~2 ~-6 ~ oak_planks keep
fill ~3 ~-7 ~ ~3 ~-8 ~ oak_planks keep
fill ~4 ~-9 ~ ~4 ~-10 ~ oak_planks keep
fill ~5 ~-11 ~ ~5 ~-12 ~ oak_planks keep
fill ~6 ~-13 ~ ~6 ~-14 ~ oak_planks keep

setblock ~1 ~-2 ~ white_wool keep
fill ~2 ~-2 ~ ~2 ~-4 ~ white_wool keep
fill ~3 ~-4 ~ ~3 ~-6 ~ white_wool keep
fill ~4 ~-5 ~ ~4 ~-8 ~ white_wool keep
fill ~5 ~-6 ~ ~5 ~-10 ~ white_wool keep
fill ~6 ~-8 ~ ~6 ~-12 ~ white_wool keep
fill ~7 ~-9 ~ ~7 ~-14 ~ white_wool keep
fill ~8 ~-10 ~ ~8 ~-14 ~ white_wool keep
fill ~9 ~-12 ~ ~9 ~-13 ~ white_wool keep
setblock ~10 ~-13 ~ white_wool keep

#Aile gauche
setblock ~-2 ~-0 ~ oak_planks keep
fill ~-3 ~-1 ~ ~-4 ~-1 ~ oak_planks keep
fill ~-5 ~-2 ~ ~-6 ~-2 ~ oak_planks keep
fill ~-7 ~-3 ~ ~-8 ~-3 ~ oak_planks keep
fill ~-9 ~-4 ~ ~-10 ~-4 ~ oak_planks keep
fill ~-11 ~-5 ~ ~-12 ~-5 ~ oak_planks keep
fill ~-13 ~-6 ~ ~-14 ~-6 ~ oak_planks keep

setblock ~-2 ~-1 ~ white_wool keep
fill ~-2 ~-2 ~ ~-4 ~-2 ~ white_wool keep
fill ~-4 ~-3 ~ ~-6 ~-3 ~ white_wool keep
fill ~-5 ~-4 ~ ~-8 ~-4 ~ white_wool keep
fill ~-6 ~-5 ~ ~-10 ~-5 ~ white_wool keep
fill ~-8 ~-6 ~ ~-12 ~-6 ~ white_wool keep
fill ~-9 ~-7 ~ ~-14 ~-7 ~ white_wool keep
fill ~-10 ~-8 ~ ~-14 ~-8 ~ white_wool keep
fill ~-12 ~-9 ~ ~-13 ~-9 ~ white_wool keep
setblock ~-13 ~-10 ~ white_wool keep

#Aile haute
setblock ~-0 ~2 ~ oak_planks keep
fill ~-1 ~3 ~ ~-1 ~4 ~ oak_planks keep
fill ~-2 ~5 ~ ~-2 ~6 ~ oak_planks keep
fill ~-3 ~7 ~ ~-3 ~8 ~ oak_planks keep
fill ~-4 ~9 ~ ~-4 ~10 ~ oak_planks keep
fill ~-5 ~11 ~ ~-5 ~12 ~ oak_planks keep
fill ~-6 ~13 ~ ~-6 ~14 ~ oak_planks keep

setblock ~-1 ~2 ~ white_wool keep
fill ~-2 ~2 ~ ~-2 ~4 ~ white_wool keep
fill ~-3 ~4 ~ ~-3 ~6 ~ white_wool keep
fill ~-4 ~5 ~ ~-4 ~8 ~ white_wool keep
fill ~-5 ~6 ~ ~-5 ~10 ~ white_wool keep
fill ~-6 ~8 ~ ~-6 ~12 ~ white_wool keep
fill ~-7 ~9 ~ ~-7 ~14 ~ white_wool keep
fill ~-8 ~10 ~ ~-8 ~14 ~ white_wool keep
fill ~-9 ~12 ~ ~-9 ~13 ~ white_wool keep
setblock ~-10 ~13 ~ white_wool keep
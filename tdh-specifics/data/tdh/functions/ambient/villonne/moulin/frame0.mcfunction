#Aile haute
setblock ~1 ~2 ~ oak_planks keep
setblock ~2 ~3 ~ oak_planks keep
setblock ~3 ~4 ~ oak_planks keep
setblock ~4 ~5 ~ oak_planks keep
setblock ~5 ~6 ~ oak_planks keep
setblock ~6 ~7 ~ oak_planks keep
setblock ~7 ~8 ~ oak_planks keep
setblock ~8 ~9 ~ oak_planks keep
setblock ~9 ~10 ~ oak_planks keep
setblock ~10 ~11 ~ oak_planks keep

fill ~ ~2 ~ ~ ~3 ~ white_wool keep
fill ~1 ~3 ~ ~1 ~4 ~ white_wool keep
fill ~2 ~4 ~ ~2 ~5 ~ white_wool keep
fill ~3 ~5 ~ ~3 ~6 ~ white_wool keep
fill ~4 ~6 ~ ~4 ~8 ~ white_wool keep
fill ~5 ~7 ~ ~5 ~10 ~ white_wool keep
fill ~6 ~8 ~ ~6 ~11 ~ white_wool keep
fill ~7 ~9 ~ ~7 ~11 ~ white_wool keep
fill ~8 ~10 ~ ~8 ~11 ~ white_wool keep
setblock ~9 ~11 ~ white_wool keep

#Aile droite
setblock ~2 ~-1 ~ oak_planks keep
setblock ~3 ~-2 ~ oak_planks keep
setblock ~4 ~-3 ~ oak_planks keep
setblock ~5 ~-4 ~ oak_planks keep
setblock ~6 ~-5 ~ oak_planks keep
setblock ~7 ~-6 ~ oak_planks keep
setblock ~8 ~-7 ~ oak_planks keep
setblock ~9 ~-8 ~ oak_planks keep
setblock ~10 ~-9 ~ oak_planks keep
setblock ~11 ~-10 ~ oak_planks keep

fill ~2 ~ ~ ~3 ~ ~ white_wool keep
fill ~3 ~-1 ~ ~4 ~-1 ~ white_wool keep
fill ~4 ~-2 ~ ~5 ~-2 ~ white_wool keep
fill ~5 ~-3 ~ ~6 ~-3 ~ white_wool keep
fill ~6 ~-4 ~ ~8 ~-4 ~ white_wool keep
fill ~7 ~-5 ~ ~10 ~-5 ~ white_wool keep
fill ~8 ~-6 ~ ~11 ~-6 ~ white_wool keep
fill ~9 ~-7 ~ ~11 ~-7 ~ white_wool keep
fill ~10 ~-8 ~ ~11 ~-8 ~ white_wool keep
setblock ~11 ~-9 ~ white_wool keep

#Aile basse
setblock ~-1 ~-2 ~ oak_planks keep
setblock ~-2 ~-3 ~ oak_planks keep
setblock ~-3 ~-4 ~ oak_planks keep
setblock ~-4 ~-5 ~ oak_planks keep
setblock ~-5 ~-6 ~ oak_planks keep
setblock ~-6 ~-7 ~ oak_planks keep
setblock ~-7 ~-8 ~ oak_planks keep
setblock ~-8 ~-9 ~ oak_planks keep
setblock ~-9 ~-10 ~ oak_planks keep
setblock ~-10 ~-11 ~ oak_planks keep

fill ~ ~-2 ~ ~ ~-3 ~ white_wool keep
fill ~-1 ~-3 ~ ~-1 ~-4 ~ white_wool keep
fill ~-2 ~-4 ~ ~-2 ~-5 ~ white_wool keep
fill ~-3 ~-5 ~ ~-3 ~-6 ~ white_wool keep
fill ~-4 ~-6 ~ ~-4 ~-8 ~ white_wool keep
fill ~-5 ~-7 ~ ~-5 ~-10 ~ white_wool keep
fill ~-6 ~-8 ~ ~-6 ~-11 ~ white_wool keep
fill ~-7 ~-9 ~ ~-7 ~-11 ~ white_wool keep
fill ~-8 ~-10 ~ ~-8 ~-11 ~ white_wool keep
setblock ~-9 ~-11 ~ white_wool keep

#Aile gauche
setblock ~-2 ~1 ~ oak_planks keep
setblock ~-3 ~2 ~ oak_planks keep
setblock ~-4 ~3 ~ oak_planks keep
setblock ~-5 ~4 ~ oak_planks keep
setblock ~-6 ~5 ~ oak_planks keep
setblock ~-7 ~6 ~ oak_planks keep
setblock ~-8 ~7 ~ oak_planks keep
setblock ~-9 ~8 ~ oak_planks keep
setblock ~-10 ~9 ~ oak_planks keep
setblock ~-11 ~10 ~ oak_planks keep

fill ~-2 ~ ~ ~-3 ~ ~ white_wool keep
fill ~-3 ~1 ~ ~-4 ~1 ~ white_wool keep
fill ~-4 ~2 ~ ~-5 ~2 ~ white_wool keep
fill ~-5 ~3 ~ ~-6 ~3 ~ white_wool keep
fill ~-6 ~4 ~ ~-8 ~4 ~ white_wool keep
fill ~-7 ~5 ~ ~-10 ~5 ~ white_wool keep
fill ~-8 ~6 ~ ~-11 ~6 ~ white_wool keep
fill ~-9 ~7 ~ ~-11 ~7 ~ white_wool keep
fill ~-10 ~8 ~ ~-11 ~8 ~ white_wool keep
setblock ~-11 ~9 ~ white_wool keep
#Aile haute
fill ~1 ~2 ~ ~1 ~4 ~ oak_planks
fill ~2 ~5 ~ ~2 ~7 ~ oak_planks
fill ~3 ~8 ~ ~3 ~11 ~ oak_planks
fill ~4 ~12 ~ ~4 ~15 ~ oak_planks

fill ~0 ~2 ~ ~0 ~15 ~ white_wool
fill ~1 ~5 ~ ~1 ~15 ~ white_wool
fill ~2 ~8 ~ ~2 ~15 ~ white_wool
fill ~3 ~12 ~ ~3 ~15 ~ white_wool

fill ~2 ~1 ~ ~15 ~1 ~ air
fill ~6 ~2 ~ ~15 ~2 ~ air
fill ~10 ~3 ~ ~15 ~3 ~ air
fill ~14 ~4 ~ ~15 ~4 ~ air

#Aile droite
fill ~2 ~-1 ~ ~4 ~-1 ~ oak_planks
fill ~5 ~-2 ~ ~7 ~-2 ~ oak_planks
fill ~8 ~-3 ~ ~11 ~-3 ~ oak_planks
fill ~12 ~-4 ~ ~15 ~-4 ~ oak_planks

fill ~2 ~-0 ~ ~15 ~-0 ~ white_wool
fill ~5 ~-1 ~ ~15 ~-1 ~ white_wool
fill ~8 ~-2 ~ ~15 ~-2 ~ white_wool
fill ~12 ~-3 ~ ~15 ~-3 ~ white_wool

fill ~1 ~-2 ~ ~1 ~-15 ~ air
fill ~2 ~-6 ~ ~2 ~-15 ~ air
fill ~3 ~-10 ~ ~3 ~-15 ~ air
fill ~4 ~-14 ~ ~4 ~-15 ~ air

#Aile basse
fill ~-1 ~-2 ~ ~-1 ~-4 ~ oak_planks
fill ~-2 ~-5 ~ ~-2 ~-7 ~ oak_planks
fill ~-3 ~-8 ~ ~-3 ~-11 ~ oak_planks
fill ~-4 ~-12 ~ ~-4 ~-15 ~ oak_planks

fill ~-0 ~-2 ~ ~-0 ~-15 ~ white_wool
fill ~-1 ~-5 ~ ~-1 ~-15 ~ white_wool
fill ~-2 ~-8 ~ ~-2 ~-15 ~ white_wool
fill ~-3 ~-12 ~ ~-3 ~-15 ~ white_wool

fill ~-2 ~-1 ~ ~-15 ~-1 ~ air
fill ~-6 ~-2 ~ ~-15 ~-2 ~ air
fill ~-10 ~-3 ~ ~-15 ~-3 ~ air
fill ~-14 ~-4 ~ ~-15 ~-4 ~ air

#Aile gauche
fill ~-2 ~1 ~ ~-4 ~1 ~ oak_planks
fill ~-5 ~2 ~ ~-7 ~2 ~ oak_planks
fill ~-8 ~3 ~ ~-11 ~3 ~ oak_planks
fill ~-12 ~4 ~ ~-15 ~4 ~ oak_planks

fill ~-2 ~0 ~ ~-15 ~0 ~ white_wool
fill ~-5 ~1 ~ ~-15 ~1 ~ white_wool
fill ~-8 ~2 ~ ~-15 ~2 ~ white_wool
fill ~-12 ~3 ~ ~-15 ~3 ~ white_wool

fill ~-1 ~2 ~ ~-1 ~15 ~ air
fill ~-2 ~6 ~ ~-2 ~15 ~ air
fill ~-3 ~10 ~ ~-3 ~15 ~ air
fill ~-4 ~14 ~ ~-4 ~15 ~ air
#Aile haute
execute if block ~1 ~2 ~ oak_planks run setblock ~1 ~2 ~ air replace
execute if block ~2 ~3 ~ oak_planks run setblock ~2 ~3 ~ air replace
execute if block ~3 ~4 ~ oak_planks run setblock ~3 ~4 ~ air replace
execute if block ~4 ~5 ~ oak_planks run setblock ~4 ~5 ~ air replace
execute if block ~5 ~6 ~ oak_planks run setblock ~5 ~6 ~ air replace
execute if block ~6 ~7 ~ oak_planks run setblock ~6 ~7 ~ air replace
execute if block ~7 ~8 ~ oak_planks run setblock ~7 ~8 ~ air replace
execute if block ~8 ~9 ~ oak_planks run setblock ~8 ~9 ~ air replace
execute if block ~9 ~10 ~ oak_planks run setblock ~9 ~10 ~ air replace
execute if block ~10 ~11 ~ oak_planks run setblock ~10 ~11 ~ air replace

fill ~ ~2 ~ ~ ~3 ~ air replace white_wool
fill ~1 ~3 ~ ~1 ~4 ~ air replace white_wool
fill ~2 ~4 ~ ~2 ~5 ~ air replace white_wool
fill ~3 ~5 ~ ~3 ~6 ~ air replace white_wool
fill ~4 ~6 ~ ~4 ~8 ~ air replace white_wool
fill ~5 ~7 ~ ~5 ~10 ~ air replace white_wool
fill ~6 ~8 ~ ~6 ~11 ~ air replace white_wool
fill ~7 ~9 ~ ~7 ~11 ~ air replace white_wool
fill ~8 ~10 ~ ~8 ~11 ~ air replace white_wool
execute if block ~9 ~11 ~ white_wool run setblock ~9 ~11 ~ air replace

#Aile droite
execute if block ~2 ~-1 ~ oak_planks run setblock ~2 ~-1 ~ air replace
execute if block ~3 ~-2 ~ oak_planks run setblock ~3 ~-2 ~ air replace
execute if block ~4 ~-3 ~ oak_planks run setblock ~4 ~-3 ~ air replace
execute if block ~5 ~-4 ~ oak_planks run setblock ~5 ~-4 ~ air replace
execute if block ~6 ~-5 ~ oak_planks run setblock ~6 ~-5 ~ air replace
execute if block ~7 ~-6 ~ oak_planks run setblock ~7 ~-6 ~ air replace
execute if block ~8 ~-7 ~ oak_planks run setblock ~8 ~-7 ~ air replace
execute if block ~9 ~-8 ~ oak_planks run setblock ~9 ~-8 ~ air replace
execute if block ~10 ~-9 ~ oak_planks run setblock ~10 ~-9 ~ air replace
execute if block ~11 ~-10 ~ oak_planks run setblock ~11 ~-10 ~ air replace

fill ~2 ~ ~ ~3 ~ ~ air replace white_wool
fill ~3 ~-1 ~ ~4 ~-1 ~ air replace white_wool
fill ~4 ~-2 ~ ~5 ~-2 ~ air replace white_wool
fill ~5 ~-3 ~ ~6 ~-3 ~ air replace white_wool
fill ~6 ~-4 ~ ~8 ~-4 ~ air replace white_wool
fill ~7 ~-5 ~ ~10 ~-5 ~ air replace white_wool
fill ~8 ~-6 ~ ~11 ~-6 ~ air replace white_wool
fill ~9 ~-7 ~ ~11 ~-7 ~ air replace white_wool
fill ~10 ~-8 ~ ~11 ~-8 ~ air replace white_wool
execute if block ~11 ~-9 ~ white_wool run setblock ~11 ~-9 ~ air replace

#Aile basse
execute if block ~-1 ~-2 ~ oak_planks run setblock ~-1 ~-2 ~ air replace
execute if block ~-2 ~-3 ~ oak_planks run setblock ~-2 ~-3 ~ air replace
execute if block ~-3 ~-4 ~ oak_planks run setblock ~-3 ~-4 ~ air replace
execute if block ~-4 ~-5 ~ oak_planks run setblock ~-4 ~-5 ~ air replace
execute if block ~-5 ~-6 ~ oak_planks run setblock ~-5 ~-6 ~ air replace
execute if block ~-6 ~-7 ~ oak_planks run setblock ~-6 ~-7 ~ air replace
execute if block ~-7 ~-8 ~ oak_planks run setblock ~-7 ~-8 ~ air replace
execute if block ~-8 ~-9 ~ oak_planks run setblock ~-8 ~-9 ~ air replace
execute if block ~-9 ~-10 ~ oak_planks run setblock ~-9 ~-10 ~ air replace
execute if block ~-10 ~-11 ~ oak_planks run setblock ~-10 ~-11 ~ air replace

fill ~ ~-2 ~ ~ ~-3 ~ air replace white_wool
fill ~-1 ~-3 ~ ~-1 ~-4 ~ air replace white_wool
fill ~-2 ~-4 ~ ~-2 ~-5 ~ air replace white_wool
fill ~-3 ~-5 ~ ~-3 ~-6 ~ air replace white_wool
fill ~-4 ~-6 ~ ~-4 ~-8 ~ air replace white_wool
fill ~-5 ~-7 ~ ~-5 ~-10 ~ air replace white_wool
fill ~-6 ~-8 ~ ~-6 ~-11 ~ air replace white_wool
fill ~-7 ~-9 ~ ~-7 ~-11 ~ air replace white_wool
fill ~-8 ~-10 ~ ~-8 ~-11 ~ air replace white_wool
execute if block ~-9 ~-11 ~ white_wool run setblock ~-9 ~-11 ~ air replace

#Aile gauche
execute if block ~-2 ~1 ~ oak_planks run setblock ~-2 ~1 ~ air replace
execute if block ~-3 ~2 ~ oak_planks run setblock ~-3 ~2 ~ air replace
execute if block ~-4 ~3 ~ oak_planks run setblock ~-4 ~3 ~ air replace
execute if block ~-5 ~4 ~ oak_planks run setblock ~-5 ~4 ~ air replace
execute if block ~-6 ~5 ~ oak_planks run setblock ~-6 ~5 ~ air replace
execute if block ~-7 ~6 ~ oak_planks run setblock ~-7 ~6 ~ air replace
execute if block ~-8 ~7 ~ oak_planks run setblock ~-8 ~7 ~ air replace
execute if block ~-9 ~8 ~ oak_planks run setblock ~-9 ~8 ~ air replace
execute if block ~-10 ~9 ~ oak_planks run setblock ~-10 ~9 ~ air replace
execute if block ~-11 ~10 ~ oak_planks run setblock ~-11 ~10 ~ air replace

fill ~-2 ~ ~ ~-3 ~ ~ air replace white_wool
fill ~-3 ~1 ~ ~-4 ~1 ~ air replace white_wool
fill ~-4 ~2 ~ ~-5 ~2 ~ air replace white_wool
fill ~-5 ~3 ~ ~-6 ~3 ~ air replace white_wool
fill ~-6 ~4 ~ ~-8 ~4 ~ air replace white_wool
fill ~-7 ~5 ~ ~-10 ~5 ~ air replace white_wool
fill ~-8 ~6 ~ ~-11 ~6 ~ air replace white_wool
fill ~-9 ~7 ~ ~-11 ~7 ~ air replace white_wool
fill ~-10 ~8 ~ ~-11 ~8 ~ air replace white_wool
execute if block ~-11 ~9 ~ white_wool run setblock ~-11 ~9 ~ air replace
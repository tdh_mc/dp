#Aile haute
fill ~1 ~2 ~ ~1 ~3 ~ oak_planks keep
setblock ~2 ~4 ~ oak_planks keep
fill ~3 ~5 ~ ~3 ~6 ~ oak_planks keep
setblock ~4 ~7 ~ oak_planks keep
fill ~5 ~8 ~ ~5 ~9 ~ oak_planks keep
fill ~6 ~10 ~ ~6 ~11 ~ oak_planks keep
fill ~7 ~12 ~ ~7 ~13 ~ oak_planks keep

fill ~0 ~2 ~ ~0 ~4 ~ white_wool keep
fill ~1 ~4 ~ ~1 ~8 ~ white_wool keep
fill ~2 ~5 ~ ~2 ~12 ~ white_wool keep
fill ~3 ~7 ~ ~3 ~13 ~ white_wool keep
fill ~4 ~8 ~ ~4 ~13 ~ white_wool keep
fill ~5 ~10 ~ ~5 ~13 ~ white_wool keep
fill ~6 ~11 ~ ~6 ~13 ~ white_wool keep

#Aile droite
fill ~2 ~-1 ~ ~3 ~-1 ~ oak_planks keep
setblock ~4 ~-2 ~ oak_planks keep
fill ~5 ~-3 ~ ~6 ~-3 ~ oak_planks keep
setblock ~7 ~-4 ~ oak_planks keep
fill ~8 ~-5 ~ ~9 ~-5 ~ oak_planks keep
fill ~10 ~-6 ~ ~11 ~-6 ~ oak_planks keep
fill ~12 ~-7 ~ ~13 ~-7 ~ oak_planks keep

fill ~2 ~-0 ~ ~4 ~-0 ~ white_wool keep
fill ~4 ~-1 ~ ~8 ~-1 ~ white_wool keep
fill ~5 ~-2 ~ ~12 ~-2 ~ white_wool keep
fill ~7 ~-3 ~ ~13 ~-3 ~ white_wool keep
fill ~8 ~-4 ~ ~13 ~-4 ~ white_wool keep
fill ~10 ~-5 ~ ~13 ~-5 ~ white_wool keep
fill ~11 ~-6 ~ ~13 ~-6 ~ white_wool keep

#Aile basse
fill ~-1 ~-2 ~ ~-1 ~-3 ~ oak_planks keep
setblock ~-2 ~-4 ~ oak_planks keep
fill ~-3 ~-5 ~ ~-3 ~-6 ~ oak_planks keep
setblock ~-4 ~-7 ~ oak_planks keep
fill ~-5 ~-8 ~ ~-5 ~-9 ~ oak_planks keep
fill ~-6 ~-10 ~ ~-6 ~-11 ~ oak_planks keep
fill ~-7 ~-12 ~ ~-7 ~-13 ~ oak_planks keep

fill ~-0 ~-2 ~ ~-0 ~-4 ~ white_wool keep
fill ~-1 ~-4 ~ ~-1 ~-8 ~ white_wool keep
fill ~-2 ~-5 ~ ~-2 ~-12 ~ white_wool keep
fill ~-3 ~-7 ~ ~-3 ~-13 ~ white_wool keep
fill ~-4 ~-8 ~ ~-4 ~-13 ~ white_wool keep
fill ~-5 ~-10 ~ ~-5 ~-13 ~ white_wool keep
fill ~-6 ~-11 ~ ~-6 ~-13 ~ white_wool keep

#Aile gauche
fill ~-2 ~1 ~ ~-3 ~1 ~ oak_planks keep
setblock ~-4 ~2 ~ oak_planks keep
fill ~-5 ~3 ~ ~-6 ~3 ~ oak_planks keep
setblock ~-7 ~4 ~ oak_planks keep
fill ~-8 ~5 ~ ~-9 ~5 ~ oak_planks keep
fill ~-10 ~6 ~ ~-11 ~6 ~ oak_planks keep
fill ~-12 ~7 ~ ~-13 ~7 ~ oak_planks keep

fill ~-2 ~0 ~ ~-4 ~0 ~ white_wool keep
fill ~-4 ~1 ~ ~-8 ~1 ~ white_wool keep
fill ~-5 ~2 ~ ~-12 ~2 ~ white_wool keep
fill ~-7 ~3 ~ ~-13 ~3 ~ white_wool keep
fill ~-8 ~4 ~ ~-13 ~4 ~ white_wool keep
fill ~-10 ~5 ~ ~-13 ~5 ~ white_wool keep
fill ~-11 ~6 ~ ~-13 ~6 ~ white_wool keep
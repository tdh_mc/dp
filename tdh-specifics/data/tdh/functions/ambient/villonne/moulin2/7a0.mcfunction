#Aile droite
setblock ~ ~1 ~3 white_wool
fill ~ ~2 ~5 ~ ~2 ~7 white_wool
setblock ~ ~0 ~4 spruce_fence
fill ~ ~1 ~6 ~ ~1 ~7 spruce_fence

setblock ~ ~2 ~3 air
fill ~ ~3 ~5 ~ ~3 ~6 air

#Aile basse
setblock ~ ~-3 ~1 white_wool
fill ~ ~-5 ~2 ~ ~-7 ~2 white_wool
setblock ~ ~-4 ~0 spruce_fence
fill ~ ~-6 ~1 ~ ~-7 ~1 spruce_fence

setblock ~ ~-3 ~2 air
fill ~ ~-5 ~3 ~ ~-6 ~3 air

#Aile gauche
setblock ~ ~-1 ~-3 white_wool
fill ~ ~-2 ~-5 ~ ~-2 ~-7 white_wool
setblock ~ ~-0 ~-4 spruce_fence
fill ~ ~-1 ~-6 ~ ~-1 ~-7 spruce_fence

setblock ~ ~-2 ~-3 air
fill ~ ~-3 ~-5 ~ ~-3 ~-6 air

#Aile haute
setblock ~ ~3 ~-1 white_wool
fill ~ ~5 ~-2 ~ ~7 ~-2 white_wool
setblock ~ ~4 ~-0 spruce_fence
fill ~ ~6 ~-1 ~ ~7 ~-1 spruce_fence

setblock ~ ~3 ~-2 air
fill ~ ~5 ~-3 ~ ~6 ~-3 air
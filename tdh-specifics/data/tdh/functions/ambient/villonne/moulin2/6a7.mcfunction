#Aile droite
setblock ~ ~1 ~2 white_wool
setblock ~ ~2 ~4 white_wool
fill ~ ~3 ~5 ~ ~3 ~6 white_wool

setblock ~ ~0 ~3 spruce_fence
setblock ~ ~1 ~5 spruce_fence
fill ~ ~2 ~6 ~ ~2 ~7 spruce_fence

setblock ~ ~2 ~2 air
setblock ~ ~3 ~4 air
fill ~ ~4 ~5 ~ ~4 ~6 air

#Aile basse
setblock ~ ~-2 ~1 white_wool
setblock ~ ~-4 ~2 white_wool
fill ~ ~-5 ~3 ~ ~-6 ~3 white_wool

setblock ~ ~-3 ~0 spruce_fence
setblock ~ ~-5 ~1 spruce_fence
fill ~ ~-6 ~2 ~ ~-7 ~2 spruce_fence

setblock ~ ~-2 ~2 air
setblock ~ ~-4 ~3 air
fill ~ ~-5 ~4 ~ ~-6 ~4 air

#Aile gauche
setblock ~ ~-1 ~-2 white_wool
setblock ~ ~-2 ~-4 white_wool
fill ~ ~-3 ~-5 ~ ~-3 ~-6 white_wool

setblock ~ ~-0 ~-3 spruce_fence
setblock ~ ~-1 ~-5 spruce_fence
fill ~ ~-2 ~-6 ~ ~-2 ~-7 spruce_fence

setblock ~ ~-2 ~-2 air
setblock ~ ~-3 ~-4 air
fill ~ ~-4 ~-5 ~ ~-4 ~-6 air

#Aile haute
setblock ~ ~2 ~-1 white_wool
setblock ~ ~4 ~-2 white_wool
fill ~ ~5 ~-3 ~ ~6 ~-3 white_wool

setblock ~ ~3 ~-0 spruce_fence
setblock ~ ~5 ~-1 spruce_fence
fill ~ ~6 ~-2 ~ ~7 ~-2 spruce_fence

setblock ~ ~2 ~-2 air
setblock ~ ~4 ~-3 air
fill ~ ~5 ~-4 ~ ~6 ~-4 air
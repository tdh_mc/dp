#Aile haute
fill ~ ~1 ~0 ~ ~2 ~0 air
fill ~ ~3 ~1 ~ ~4 ~1 air
fill ~ ~5 ~2 ~ ~7 ~2 air
fill ~ ~1 ~1 ~ ~2 ~1 air
fill ~ ~2 ~2 ~ ~4 ~2 air
fill ~ ~4 ~3 ~ ~6 ~3 air

#Aile droite
fill ~ ~-0 ~1 ~ ~-0 ~2 air
fill ~ ~-1 ~3 ~ ~-1 ~4 air
fill ~ ~-2 ~5 ~ ~-2 ~7 air
fill ~ ~-1 ~1 ~ ~-1 ~2 air
fill ~ ~-2 ~2 ~ ~-2 ~4 air
fill ~ ~-3 ~4 ~ ~-3 ~6 air

#Aile basse
fill ~ ~-1 ~-0 ~ ~-2 ~-0 air
fill ~ ~-3 ~-1 ~ ~-4 ~-1 air
fill ~ ~-5 ~-2 ~ ~-7 ~-2 air
fill ~ ~-1 ~-1 ~ ~-2 ~-1 air
fill ~ ~-2 ~-2 ~ ~-4 ~-2 air
fill ~ ~-4 ~-3 ~ ~-6 ~-3 air

#Aile gauche
fill ~ ~0 ~-1 ~ ~0 ~-2 air
fill ~ ~1 ~-3 ~ ~1 ~-4 air
fill ~ ~2 ~-5 ~ ~2 ~-7 air
fill ~ ~1 ~-1 ~ ~1 ~-2 air
fill ~ ~2 ~-2 ~ ~2 ~-4 air
fill ~ ~3 ~-4 ~ ~3 ~-6 air
#Aile droite
fill ~ ~1 ~1 ~ ~1 ~3 air
fill ~ ~2 ~4 ~ ~2 ~7 air
fill ~ ~0 ~1 ~ ~0 ~4 air
fill ~ ~1 ~4 ~ ~1 ~7 air

#Aile basse
fill ~ ~-1 ~1 ~ ~-3 ~1 air
fill ~ ~-4 ~2 ~ ~-7 ~2 air
fill ~ ~-1 ~0 ~ ~-4 ~0 air
fill ~ ~-4 ~1 ~ ~-7 ~1 air

#Aile gauche
fill ~ ~-1 ~-1 ~ ~-1 ~-3 air
fill ~ ~-2 ~-4 ~ ~-2 ~-7 air
fill ~ ~-0 ~-1 ~ ~-0 ~-4 air
fill ~ ~-1 ~-4 ~ ~-1 ~-7 air

#Aile haute
fill ~ ~1 ~-1 ~ ~3 ~-1 air
fill ~ ~4 ~-2 ~ ~7 ~-2 air
fill ~ ~1 ~-0 ~ ~4 ~-0 air
fill ~ ~4 ~-1 ~ ~7 ~-1 air
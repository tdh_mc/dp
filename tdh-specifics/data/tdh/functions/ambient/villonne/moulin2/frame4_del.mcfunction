#Aile haute
fill ~ ~1 ~0 ~ ~2 ~0 air
setblock ~ ~3 ~1 air
setblock ~ ~4 ~2 air
setblock ~ ~5 ~3 air
setblock ~ ~6 ~4 air

fill ~ ~1 ~1 ~ ~2 ~1 air
fill ~ ~2 ~2 ~ ~3 ~2 air
fill ~ ~3 ~3 ~ ~4 ~3 air
fill ~ ~4 ~4 ~ ~5 ~4 air
setblock ~ ~5 ~5 air

#Aile droite
fill ~ ~-0 ~1 ~ ~-0 ~2 air
setblock ~ ~-1 ~3 air
setblock ~ ~-2 ~4 air
setblock ~ ~-3 ~5 air
setblock ~ ~-4 ~6 air

fill ~ ~-1 ~1 ~ ~-1 ~2 air
fill ~ ~-2 ~2 ~ ~-2 ~3 air
fill ~ ~-3 ~3 ~ ~-3 ~4 air
fill ~ ~-4 ~4 ~ ~-4 ~5 air
setblock ~ ~-5 ~5 air

#Aile basse
fill ~ ~-1 ~-0 ~ ~-2 ~-0 air
setblock ~ ~-3 ~-1 air
setblock ~ ~-4 ~-2 air
setblock ~ ~-5 ~-3 air
setblock ~ ~-6 ~-4 air

fill ~ ~-1 ~-1 ~ ~-2 ~-1 air
fill ~ ~-2 ~-2 ~ ~-3 ~-2 air
fill ~ ~-3 ~-3 ~ ~-4 ~-3 air
fill ~ ~-4 ~-4 ~ ~-5 ~-4 air
setblock ~ ~-5 ~-5 air

#Aile gauche
fill ~ ~0 ~-1 ~ ~0 ~-2 air
setblock ~ ~1 ~-3 air
setblock ~ ~2 ~-4 air
setblock ~ ~3 ~-5 air
setblock ~ ~4 ~-6 air

fill ~ ~1 ~-1 ~ ~1 ~-2 air
fill ~ ~2 ~-2 ~ ~2 ~-3 air
fill ~ ~3 ~-3 ~ ~3 ~-4 air
fill ~ ~4 ~-4 ~ ~4 ~-5 air
setblock ~ ~5 ~-5 air
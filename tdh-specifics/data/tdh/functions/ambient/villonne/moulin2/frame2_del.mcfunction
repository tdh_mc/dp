#Aile haute
fill ~ ~1 ~0 ~ ~4 ~0 air
fill ~ ~5 ~1 ~ ~7 ~1 air
fill ~ ~1 ~1 ~ ~4 ~1 air
fill ~ ~4 ~2 ~ ~7 ~2 air

#Aile droite
fill ~ ~-0 ~1 ~ ~-0 ~4 air
fill ~ ~-1 ~5 ~ ~-1 ~7 air
fill ~ ~-1 ~1 ~ ~-1 ~4 air
fill ~ ~-2 ~4 ~ ~-2 ~7 air

#Aile basse
fill ~ ~-1 ~-0 ~ ~-4 ~-0 air
fill ~ ~-5 ~-1 ~ ~-7 ~-1 air
fill ~ ~-1 ~-1 ~ ~-4 ~-1 air
fill ~ ~-4 ~-2 ~ ~-7 ~-2 air

#Aile gauche
fill ~ ~0 ~-1 ~ ~0 ~-4 air
fill ~ ~1 ~-5 ~ ~1 ~-7 air
fill ~ ~1 ~-1 ~ ~1 ~-4 air
fill ~ ~2 ~-4 ~ ~2 ~-7 air
#Aile droite
setblock ~ ~1 ~1 air
setblock ~ ~2 ~2 air
setblock ~ ~3 ~3 air
setblock ~ ~4 ~4 air
setblock ~ ~5 ~5 air
fill ~ ~0 ~1 ~ ~0 ~2 air
fill ~ ~1 ~2 ~ ~1 ~3 air
fill ~ ~2 ~3 ~ ~2 ~4 air
fill ~ ~3 ~4 ~ ~3 ~5 air
fill ~ ~4 ~5 ~ ~4 ~6 air

#Aile basse
setblock ~ ~-1 ~1 air
setblock ~ ~-2 ~2 air
setblock ~ ~-3 ~3 air
setblock ~ ~-4 ~4 air
setblock ~ ~-5 ~5 air
fill ~ ~-1 ~0 ~ ~-2 ~0 air
fill ~ ~-2 ~1 ~ ~-3 ~1 air
fill ~ ~-3 ~2 ~ ~-4 ~2 air
fill ~ ~-4 ~3 ~ ~-5 ~3 air
fill ~ ~-5 ~4 ~ ~-6 ~4 air

#Aile gauche
setblock ~ ~-1 ~-1 air
setblock ~ ~-2 ~-2 air
setblock ~ ~-3 ~-3 air
setblock ~ ~-4 ~-4 air
setblock ~ ~-5 ~-5 air
fill ~ ~-0 ~-1 ~ ~-0 ~-2 air
fill ~ ~-1 ~-2 ~ ~-1 ~-3 air
fill ~ ~-2 ~-3 ~ ~-2 ~-4 air
fill ~ ~-3 ~-4 ~ ~-3 ~-5 air
fill ~ ~-4 ~-5 ~ ~-4 ~-6 air

#Aile haute
setblock ~ ~1 ~-1 air
setblock ~ ~2 ~-2 air
setblock ~ ~3 ~-3 air
setblock ~ ~4 ~-4 air
setblock ~ ~5 ~-5 air
fill ~ ~1 ~-0 ~ ~2 ~-0 air
fill ~ ~2 ~-1 ~ ~3 ~-1 air
fill ~ ~3 ~-2 ~ ~4 ~-2 air
fill ~ ~4 ~-3 ~ ~5 ~-3 air
fill ~ ~5 ~-4 ~ ~6 ~-4 air
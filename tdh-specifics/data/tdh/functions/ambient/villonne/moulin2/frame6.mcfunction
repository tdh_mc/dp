#Aile droite
setblock ~ ~1 ~1 white_wool
fill ~ ~2 ~2 ~ ~2 ~3 white_wool
setblock ~ ~3 ~4 white_wool
fill ~ ~4 ~5 ~ ~4 ~6 white_wool

fill ~ ~0 ~1 ~ ~0 ~2 spruce_fence
fill ~ ~1 ~2 ~ ~1 ~4 spruce_fence
fill ~ ~2 ~4 ~ ~2 ~5 spruce_fence
fill ~ ~3 ~5 ~ ~3 ~6 spruce_fence

#Aile basse
setblock ~ ~-1 ~1 white_wool
fill ~ ~-2 ~2 ~ ~-3 ~2 white_wool
setblock ~ ~-4 ~3 white_wool
fill ~ ~-5 ~4 ~ ~-6 ~4 white_wool

fill ~ ~-1 ~0 ~ ~-2 ~0 spruce_fence
fill ~ ~-2 ~1 ~ ~-4 ~1 spruce_fence
fill ~ ~-4 ~2 ~ ~-5 ~2 spruce_fence
fill ~ ~-5 ~3 ~ ~-6 ~3 spruce_fence

#Aile gauche
setblock ~ ~-1 ~-1 white_wool
fill ~ ~-2 ~-2 ~ ~-2 ~-3 white_wool
setblock ~ ~-3 ~-4 white_wool
fill ~ ~-4 ~-5 ~ ~-4 ~-6 white_wool

fill ~ ~-0 ~-1 ~ ~-0 ~-2 spruce_fence
fill ~ ~-1 ~-2 ~ ~-1 ~-4 spruce_fence
fill ~ ~-2 ~-4 ~ ~-2 ~-5 spruce_fence
fill ~ ~-3 ~-5 ~ ~-3 ~-6 spruce_fence

#Aile haute
setblock ~ ~1 ~-1 white_wool
fill ~ ~2 ~-2 ~ ~3 ~-2 white_wool
setblock ~ ~4 ~-3 white_wool
fill ~ ~5 ~-4 ~ ~6 ~-4 white_wool

fill ~ ~1 ~-0 ~ ~2 ~-0 spruce_fence
fill ~ ~2 ~-1 ~ ~4 ~-1 spruce_fence
fill ~ ~4 ~-2 ~ ~5 ~-2 spruce_fence
fill ~ ~5 ~-3 ~ ~6 ~-3 spruce_fence
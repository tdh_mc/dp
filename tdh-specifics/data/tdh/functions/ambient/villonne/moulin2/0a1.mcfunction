#Aile droite
fill ~ ~0 ~5 ~ ~0 ~7 spruce_fence
fill ~ ~1 ~4 ~ ~1 ~7 white_wool
fill ~ ~2 ~4 ~ ~2 ~7 air

#Aile basse
fill ~ ~-5 ~0 ~ ~-7 ~0 spruce_fence
fill ~ ~-4 ~1 ~ ~-7 ~1 white_wool
fill ~ ~-4 ~2 ~ ~-7 ~2 air

#Aile gauche
fill ~ ~-0 ~-5 ~ ~-0 ~-7 spruce_fence
fill ~ ~-1 ~-4 ~ ~-1 ~-7 white_wool
fill ~ ~-2 ~-4 ~ ~-2 ~-7 air

#Aile haute
fill ~ ~5 ~-0 ~ ~7 ~-0 spruce_fence
fill ~ ~4 ~-1 ~ ~7 ~-1 white_wool
fill ~ ~4 ~-2 ~ ~7 ~-2 air
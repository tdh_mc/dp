#Aile haute
fill ~ ~1 ~0 ~ ~2 ~0 white_wool
fill ~ ~3 ~1 ~ ~4 ~1 white_wool
fill ~ ~5 ~2 ~ ~7 ~2 white_wool
fill ~ ~1 ~1 ~ ~2 ~1 spruce_fence
fill ~ ~2 ~2 ~ ~4 ~2 spruce_fence
fill ~ ~4 ~3 ~ ~6 ~3 spruce_fence

#Aile droite
fill ~ ~-0 ~1 ~ ~-0 ~2 white_wool
fill ~ ~-1 ~3 ~ ~-1 ~4 white_wool
fill ~ ~-2 ~5 ~ ~-2 ~7 white_wool
fill ~ ~-1 ~1 ~ ~-1 ~2 spruce_fence
fill ~ ~-2 ~2 ~ ~-2 ~4 spruce_fence
fill ~ ~-3 ~4 ~ ~-3 ~6 spruce_fence

#Aile basse
fill ~ ~-1 ~-0 ~ ~-2 ~-0 white_wool
fill ~ ~-3 ~-1 ~ ~-4 ~-1 white_wool
fill ~ ~-5 ~-2 ~ ~-7 ~-2 white_wool
fill ~ ~-1 ~-1 ~ ~-2 ~-1 spruce_fence
fill ~ ~-2 ~-2 ~ ~-4 ~-2 spruce_fence
fill ~ ~-4 ~-3 ~ ~-6 ~-3 spruce_fence

#Aile gauche
fill ~ ~0 ~-1 ~ ~0 ~-2 white_wool
fill ~ ~1 ~-3 ~ ~1 ~-4 white_wool
fill ~ ~2 ~-5 ~ ~2 ~-7 white_wool
fill ~ ~1 ~-1 ~ ~1 ~-2 spruce_fence
fill ~ ~2 ~-2 ~ ~2 ~-4 spruce_fence
fill ~ ~3 ~-4 ~ ~3 ~-6 spruce_fence
#Aile droite
setblock ~ ~1 ~1 white_wool
setblock ~ ~2 ~2 white_wool
setblock ~ ~3 ~3 white_wool
setblock ~ ~4 ~4 white_wool
setblock ~ ~5 ~5 white_wool
fill ~ ~0 ~1 ~ ~0 ~2 spruce_fence
fill ~ ~1 ~2 ~ ~1 ~3 spruce_fence
fill ~ ~2 ~3 ~ ~2 ~4 spruce_fence
fill ~ ~3 ~4 ~ ~3 ~5 spruce_fence
fill ~ ~4 ~5 ~ ~4 ~6 spruce_fence

fill ~ ~2 ~1 ~ ~3 ~1 air
fill ~ ~3 ~2 ~ ~4 ~2 air
fill ~ ~4 ~3 ~ ~5 ~3 air
fill ~ ~5 ~4 ~ ~6 ~4 air

#Aile basse
setblock ~ ~-1 ~1 white_wool
setblock ~ ~-2 ~2 white_wool
setblock ~ ~-3 ~3 white_wool
setblock ~ ~-4 ~4 white_wool
setblock ~ ~-5 ~5 white_wool
fill ~ ~-1 ~0 ~ ~-2 ~0 spruce_fence
fill ~ ~-2 ~1 ~ ~-3 ~1 spruce_fence
fill ~ ~-3 ~2 ~ ~-4 ~2 spruce_fence
fill ~ ~-4 ~3 ~ ~-5 ~3 spruce_fence
fill ~ ~-5 ~4 ~ ~-6 ~4 spruce_fence

fill ~ ~-1 ~2 ~ ~-1 ~3 air
fill ~ ~-2 ~3 ~ ~-2 ~4 air
fill ~ ~-3 ~4 ~ ~-3 ~5 air
fill ~ ~-4 ~5 ~ ~-4 ~6 air

#Aile gauche
setblock ~ ~-1 ~-1 white_wool
setblock ~ ~-2 ~-2 white_wool
setblock ~ ~-3 ~-3 white_wool
setblock ~ ~-4 ~-4 white_wool
setblock ~ ~-5 ~-5 white_wool
fill ~ ~-0 ~-1 ~ ~-0 ~-2 spruce_fence
fill ~ ~-1 ~-2 ~ ~-1 ~-3 spruce_fence
fill ~ ~-2 ~-3 ~ ~-2 ~-4 spruce_fence
fill ~ ~-3 ~-4 ~ ~-3 ~-5 spruce_fence
fill ~ ~-4 ~-5 ~ ~-4 ~-6 spruce_fence

fill ~ ~-2 ~-1 ~ ~-3 ~-1 air
fill ~ ~-3 ~-2 ~ ~-4 ~-2 air
fill ~ ~-4 ~-3 ~ ~-5 ~-3 air
fill ~ ~-5 ~-4 ~ ~-6 ~-4 air

#Aile haute
setblock ~ ~1 ~-1 white_wool
setblock ~ ~2 ~-2 white_wool
setblock ~ ~3 ~-3 white_wool
setblock ~ ~4 ~-4 white_wool
setblock ~ ~5 ~-5 white_wool
fill ~ ~1 ~-0 ~ ~2 ~-0 spruce_fence
fill ~ ~2 ~-1 ~ ~3 ~-1 spruce_fence
fill ~ ~3 ~-2 ~ ~4 ~-2 spruce_fence
fill ~ ~4 ~-3 ~ ~5 ~-3 spruce_fence
fill ~ ~5 ~-4 ~ ~6 ~-4 spruce_fence

fill ~ ~1 ~-2 ~ ~1 ~-3 air
fill ~ ~2 ~-3 ~ ~2 ~-4 air
fill ~ ~3 ~-4 ~ ~3 ~-5 air
fill ~ ~4 ~-5 ~ ~4 ~-6 air
#Aile droite
fill ~ ~1 ~1 ~ ~1 ~3 white_wool
fill ~ ~2 ~4 ~ ~2 ~7 white_wool
fill ~ ~0 ~1 ~ ~0 ~4 spruce_fence
fill ~ ~1 ~4 ~ ~1 ~7 spruce_fence

#Aile basse
fill ~ ~-1 ~1 ~ ~-3 ~1 white_wool
fill ~ ~-4 ~2 ~ ~-7 ~2 white_wool
fill ~ ~-1 ~0 ~ ~-4 ~0 spruce_fence
fill ~ ~-4 ~1 ~ ~-7 ~1 spruce_fence

#Aile gauche
fill ~ ~-1 ~-1 ~ ~-1 ~-3 white_wool
fill ~ ~-2 ~-4 ~ ~-2 ~-7 white_wool
fill ~ ~-0 ~-1 ~ ~-0 ~-4 spruce_fence
fill ~ ~-1 ~-4 ~ ~-1 ~-7 spruce_fence

#Aile haute
fill ~ ~1 ~-1 ~ ~3 ~-1 white_wool
fill ~ ~4 ~-2 ~ ~7 ~-2 white_wool
fill ~ ~1 ~-0 ~ ~4 ~-0 spruce_fence
fill ~ ~4 ~-1 ~ ~7 ~-1 spruce_fence
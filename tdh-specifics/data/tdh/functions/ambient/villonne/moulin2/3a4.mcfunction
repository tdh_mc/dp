#Aile haute
setblock ~ ~4 ~2 white_wool
setblock ~ ~5 ~3 white_wool
setblock ~ ~6 ~4 white_wool

setblock ~ ~3 ~3 spruce_fence
fill ~ ~4 ~4 ~ ~5 ~4 spruce_fence
setblock ~ ~5 ~5 spruce_fence

setblock ~ ~4 ~1 air
fill ~ ~5 ~2 ~ ~7 ~2 air
setblock ~ ~6 ~3 air

#Aile droite
setblock ~ ~-2 ~4 white_wool
setblock ~ ~-3 ~5 white_wool
setblock ~ ~-4 ~6 white_wool

setblock ~ ~-3 ~3 spruce_fence
fill ~ ~-4 ~4 ~ ~-4 ~5 spruce_fence
setblock ~ ~-5 ~5 spruce_fence

setblock ~ ~-1 ~4 air
fill ~ ~-2 ~5 ~ ~-2 ~7 air
setblock ~ ~-3 ~6 air

#Aile basse
setblock ~ ~-4 ~-2 white_wool
setblock ~ ~-5 ~-3 white_wool
setblock ~ ~-6 ~-4 white_wool

setblock ~ ~-3 ~-3 spruce_fence
fill ~ ~-4 ~-4 ~ ~-5 ~-4 spruce_fence
setblock ~ ~-5 ~-5 spruce_fence

setblock ~ ~-4 ~-1 air
fill ~ ~-5 ~-2 ~ ~-7 ~-2 air
setblock ~ ~-6 ~-3 air

#Aile gauche
setblock ~ ~2 ~-4 white_wool
setblock ~ ~3 ~-5 white_wool
setblock ~ ~4 ~-6 white_wool

setblock ~ ~3 ~-3 spruce_fence
fill ~ ~4 ~-4 ~ ~4 ~-5 spruce_fence
setblock ~ ~5 ~-5 spruce_fence

setblock ~ ~1 ~-4 air
fill ~ ~2 ~-5 ~ ~2 ~-7 air
setblock ~ ~3 ~-6 air
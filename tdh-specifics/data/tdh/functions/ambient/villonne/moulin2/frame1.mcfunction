#Aile droite
fill ~ ~0 ~1 ~ ~0 ~7 spruce_fence
fill ~ ~1 ~1 ~ ~1 ~7 white_wool

#Aile basse
fill ~ ~1 ~0 ~ ~7 ~0 spruce_fence
fill ~ ~1 ~-1 ~ ~7 ~-1 white_wool

#Aile gauche
fill ~ ~0 ~-1 ~ ~0 ~-7 spruce_fence
fill ~ ~-1 ~-1 ~ ~-1 ~-7 white_wool

#Aile haute
fill ~ ~-1 ~0 ~ ~-7 ~0 spruce_fence
fill ~ ~-1 ~1 ~ ~-7 ~1 white_wool
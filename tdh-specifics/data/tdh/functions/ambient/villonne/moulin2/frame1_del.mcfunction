#Aile droite
fill ~ ~0 ~1 ~ ~0 ~7 air
fill ~ ~1 ~1 ~ ~1 ~7 air

#Aile basse
fill ~ ~1 ~0 ~ ~7 ~0 air
fill ~ ~1 ~-1 ~ ~7 ~-1 air

#Aile gauche
fill ~ ~0 ~-1 ~ ~0 ~-7 air
fill ~ ~-1 ~-1 ~ ~-1 ~-7 air

#Aile haute
fill ~ ~-1 ~0 ~ ~-7 ~0 air
fill ~ ~-1 ~1 ~ ~-7 ~1 air
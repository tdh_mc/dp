#Aile droite
setblock ~ ~2 ~3 white_wool
setblock ~ ~3 ~4 white_wool
fill ~ ~4 ~5 ~ ~4 ~6 white_wool

setblock ~ ~1 ~4 spruce_fence
setblock ~ ~2 ~5 spruce_fence
setblock ~ ~3 ~6 spruce_fence

setblock ~ ~3 ~3 air
setblock ~ ~4 ~4 air
setblock ~ ~5 ~5 air

#Aile basse
setblock ~ ~-3 ~2 white_wool
setblock ~ ~-4 ~3 white_wool
fill ~ ~-5 ~4 ~ ~-6 ~4 white_wool

setblock ~ ~-4 ~1 spruce_fence
setblock ~ ~-5 ~2 spruce_fence
setblock ~ ~-6 ~3 spruce_fence

setblock ~ ~-3 ~3 air
setblock ~ ~-4 ~4 air
setblock ~ ~-5 ~5 air

#Aile gauche
setblock ~ ~-2 ~-3 white_wool
setblock ~ ~-3 ~-4 white_wool
fill ~ ~-4 ~-5 ~ ~-4 ~-6 white_wool

setblock ~ ~-1 ~-4 spruce_fence
setblock ~ ~-2 ~-5 spruce_fence
setblock ~ ~-3 ~-6 spruce_fence

setblock ~ ~-3 ~-3 air
setblock ~ ~-4 ~-4 air
setblock ~ ~-5 ~-5 air

#Aile haute
setblock ~ ~3 ~-2 white_wool
setblock ~ ~4 ~-3 white_wool
fill ~ ~5 ~-4 ~ ~6 ~-4 white_wool

setblock ~ ~4 ~-1 spruce_fence
setblock ~ ~5 ~-2 spruce_fence
setblock ~ ~6 ~-3 spruce_fence

setblock ~ ~3 ~-3 air
setblock ~ ~4 ~-4 air
setblock ~ ~5 ~-5 air
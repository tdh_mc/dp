#Aile droite
setblock ~ ~1 ~1 air
fill ~ ~2 ~2 ~ ~2 ~3 air
setblock ~ ~3 ~4 air
fill ~ ~4 ~5 ~ ~4 ~6 air

fill ~ ~0 ~1 ~ ~0 ~2 air
fill ~ ~1 ~2 ~ ~1 ~4 air
fill ~ ~2 ~4 ~ ~2 ~5 air
fill ~ ~3 ~5 ~ ~3 ~6 air

#Aile basse
setblock ~ ~-1 ~1 air
fill ~ ~-2 ~2 ~ ~-3 ~2 air
setblock ~ ~-4 ~3 air
fill ~ ~-5 ~4 ~ ~-6 ~4 air

fill ~ ~-1 ~0 ~ ~-2 ~0 air
fill ~ ~-2 ~1 ~ ~-4 ~1 air
fill ~ ~-4 ~2 ~ ~-5 ~2 air
fill ~ ~-5 ~3 ~ ~-6 ~3 air

#Aile gauche
setblock ~ ~-1 ~-1 air
fill ~ ~-2 ~-2 ~ ~-2 ~-3 air
setblock ~ ~-3 ~-4 air
fill ~ ~-4 ~-5 ~ ~-4 ~-6 air

fill ~ ~-0 ~-1 ~ ~-0 ~-2 air
fill ~ ~-1 ~-2 ~ ~-1 ~-4 air
fill ~ ~-2 ~-4 ~ ~-2 ~-5 air
fill ~ ~-3 ~-5 ~ ~-3 ~-6 air

#Aile haute
setblock ~ ~1 ~-1 air
fill ~ ~2 ~-2 ~ ~3 ~-2 air
setblock ~ ~4 ~-3 air
fill ~ ~5 ~-4 ~ ~6 ~-4 air

fill ~ ~1 ~-0 ~ ~2 ~-0 air
fill ~ ~2 ~-1 ~ ~4 ~-1 air
fill ~ ~4 ~-2 ~ ~5 ~-2 air
fill ~ ~5 ~-3 ~ ~6 ~-3 air
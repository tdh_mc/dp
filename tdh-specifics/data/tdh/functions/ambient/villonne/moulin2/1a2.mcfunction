#Aile haute
fill ~ ~1 ~0 ~ ~4 ~0 white_wool
fill ~ ~5 ~1 ~ ~7 ~1 white_wool
fill ~ ~1 ~1 ~ ~4 ~1 spruce_fence
fill ~ ~4 ~2 ~ ~7 ~2 spruce_fence

fill ~ ~5 ~0 ~ ~7 ~0 air
fill ~ ~1 ~-1 ~ ~7 ~-1 air

#Aile droite
fill ~ ~-0 ~1 ~ ~-0 ~4 white_wool
fill ~ ~-1 ~5 ~ ~-1 ~7 white_wool
fill ~ ~-1 ~1 ~ ~-1 ~4 spruce_fence
fill ~ ~-2 ~4 ~ ~-2 ~7 spruce_fence

fill ~ ~-0 ~5 ~ ~-0 ~7 air
fill ~ ~1 ~1 ~ ~1 ~7 air

#Aile basse
fill ~ ~-1 ~-0 ~ ~-4 ~-0 white_wool
fill ~ ~-5 ~-1 ~ ~-7 ~-1 white_wool
fill ~ ~-1 ~-1 ~ ~-4 ~-1 spruce_fence
fill ~ ~-4 ~-2 ~ ~-7 ~-2 spruce_fence

fill ~ ~-5 ~-0 ~ ~-7 ~-0 air
fill ~ ~-1 ~1 ~ ~-7 ~1 air

#Aile gauche
fill ~ ~0 ~-1 ~ ~0 ~-4 white_wool
fill ~ ~1 ~-5 ~ ~1 ~-7 white_wool
fill ~ ~1 ~-1 ~ ~1 ~-4 spruce_fence
fill ~ ~2 ~-4 ~ ~2 ~-7 spruce_fence

fill ~ ~0 ~-5 ~ ~0 ~-7 air
fill ~ ~-1 ~-1 ~ ~-1 ~-7 air
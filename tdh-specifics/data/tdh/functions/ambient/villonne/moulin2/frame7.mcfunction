#Aile droite
fill ~ ~1 ~1 ~ ~1 ~2 white_wool
fill ~ ~2 ~3 ~ ~2 ~4 white_wool
fill ~ ~3 ~5 ~ ~3 ~6 white_wool

fill ~ ~0 ~1 ~ ~0 ~3 spruce_fence
fill ~ ~1 ~3 ~ ~1 ~5 spruce_fence
fill ~ ~2 ~5 ~ ~2 ~7 spruce_fence

#Aile basse
fill ~ ~-1 ~1 ~ ~-2 ~1 white_wool
fill ~ ~-3 ~2 ~ ~-4 ~2 white_wool
fill ~ ~-5 ~3 ~ ~-6 ~3 white_wool

fill ~ ~-1 ~0 ~ ~-3 ~0 spruce_fence
fill ~ ~-3 ~1 ~ ~-5 ~1 spruce_fence
fill ~ ~-5 ~2 ~ ~-7 ~2 spruce_fence

#Aile gauche
fill ~ ~-1 ~-1 ~ ~-1 ~-2 white_wool
fill ~ ~-2 ~-3 ~ ~-2 ~-4 white_wool
fill ~ ~-3 ~-5 ~ ~-3 ~-6 white_wool

fill ~ ~-0 ~-1 ~ ~-0 ~-3 spruce_fence
fill ~ ~-1 ~-3 ~ ~-1 ~-5 spruce_fence
fill ~ ~-2 ~-5 ~ ~-2 ~-7 spruce_fence

#Aile haute
fill ~ ~1 ~-1 ~ ~2 ~-1 white_wool
fill ~ ~3 ~-2 ~ ~4 ~-2 white_wool
fill ~ ~5 ~-3 ~ ~6 ~-3 white_wool

fill ~ ~1 ~-0 ~ ~3 ~-0 spruce_fence
fill ~ ~3 ~-1 ~ ~5 ~-1 spruce_fence
fill ~ ~5 ~-2 ~ ~7 ~-2 spruce_fence
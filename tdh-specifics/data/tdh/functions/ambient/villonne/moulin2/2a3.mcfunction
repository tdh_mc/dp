#Aile haute
fill ~ ~3 ~1 ~ ~4 ~1 white_wool
fill ~ ~5 ~2 ~ ~7 ~2 white_wool
fill ~ ~2 ~2 ~ ~3 ~2 spruce_fence
fill ~ ~4 ~3 ~ ~6 ~3 spruce_fence

fill ~ ~3 ~0 ~ ~4 ~0 air
fill ~ ~5 ~1 ~ ~7 ~1 air

#Aile droite
fill ~ ~-1 ~3 ~ ~-1 ~4 white_wool
fill ~ ~-2 ~5 ~ ~-2 ~7 white_wool
fill ~ ~-2 ~2 ~ ~-2 ~3 spruce_fence
fill ~ ~-3 ~4 ~ ~-3 ~6 spruce_fence

fill ~ ~-0 ~3 ~ ~-0 ~4 air
fill ~ ~-1 ~5 ~ ~-1 ~7 air

#Aile basse
fill ~ ~-3 ~-1 ~ ~-4 ~-1 white_wool
fill ~ ~-5 ~-2 ~ ~-7 ~-2 white_wool
fill ~ ~-2 ~-2 ~ ~-3 ~-2 spruce_fence
fill ~ ~-4 ~-3 ~ ~-6 ~-3 spruce_fence

fill ~ ~-3 ~-0 ~ ~-4 ~-0 air
fill ~ ~-5 ~-1 ~ ~-7 ~-1 air

#Aile gauche
fill ~ ~1 ~-3 ~ ~1 ~-4 white_wool
fill ~ ~2 ~-5 ~ ~2 ~-7 white_wool
fill ~ ~2 ~-2 ~ ~2 ~-3 spruce_fence
fill ~ ~3 ~-4 ~ ~3 ~-6 spruce_fence

fill ~ ~0 ~-3 ~ ~0 ~-4 air
fill ~ ~1 ~-5 ~ ~1 ~-7 air
#Aile droite
fill ~ ~1 ~1 ~ ~1 ~2 air
fill ~ ~2 ~3 ~ ~2 ~4 air
fill ~ ~3 ~5 ~ ~3 ~6 air

fill ~ ~0 ~1 ~ ~0 ~3 air
fill ~ ~1 ~3 ~ ~1 ~5 air
fill ~ ~2 ~5 ~ ~2 ~7 air

#Aile basse
fill ~ ~-1 ~1 ~ ~-2 ~1 air
fill ~ ~-3 ~2 ~ ~-4 ~2 air
fill ~ ~-5 ~3 ~ ~-6 ~3 air

fill ~ ~-1 ~0 ~ ~-3 ~0 air
fill ~ ~-3 ~1 ~ ~-5 ~1 air
fill ~ ~-5 ~2 ~ ~-7 ~2 air

#Aile gauche
fill ~ ~-1 ~-1 ~ ~-1 ~-2 air
fill ~ ~-2 ~-3 ~ ~-2 ~-4 air
fill ~ ~-3 ~-5 ~ ~-3 ~-6 air

fill ~ ~-0 ~-1 ~ ~-0 ~-3 air
fill ~ ~-1 ~-3 ~ ~-1 ~-5 air
fill ~ ~-2 ~-5 ~ ~-2 ~-7 air

#Aile haute
fill ~ ~1 ~-1 ~ ~2 ~-1 air
fill ~ ~3 ~-2 ~ ~4 ~-2 air
fill ~ ~5 ~-3 ~ ~6 ~-3 air

fill ~ ~1 ~-0 ~ ~3 ~-0 air
fill ~ ~3 ~-1 ~ ~5 ~-1 air
fill ~ ~5 ~-2 ~ ~7 ~-2 air
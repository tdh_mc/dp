#Aile haute
fill ~ ~1 ~0 ~ ~2 ~0 white_wool
setblock ~ ~3 ~1 white_wool
setblock ~ ~4 ~2 white_wool
setblock ~ ~5 ~3 white_wool
setblock ~ ~6 ~4 white_wool

fill ~ ~1 ~1 ~ ~2 ~1 spruce_fence
fill ~ ~2 ~2 ~ ~3 ~2 spruce_fence
fill ~ ~3 ~3 ~ ~4 ~3 spruce_fence
fill ~ ~4 ~4 ~ ~5 ~4 spruce_fence
setblock ~ ~5 ~5 spruce_fence

#Aile droite
fill ~ ~-0 ~1 ~ ~-0 ~2 white_wool
setblock ~ ~-1 ~3 white_wool
setblock ~ ~-2 ~4 white_wool
setblock ~ ~-3 ~5 white_wool
setblock ~ ~-4 ~6 white_wool

fill ~ ~-1 ~1 ~ ~-1 ~2 spruce_fence
fill ~ ~-2 ~2 ~ ~-2 ~3 spruce_fence
fill ~ ~-3 ~3 ~ ~-3 ~4 spruce_fence
fill ~ ~-4 ~4 ~ ~-4 ~5 spruce_fence
setblock ~ ~-5 ~5 spruce_fence

#Aile basse
fill ~ ~-1 ~-0 ~ ~-2 ~-0 white_wool
setblock ~ ~-3 ~-1 white_wool
setblock ~ ~-4 ~-2 white_wool
setblock ~ ~-5 ~-3 white_wool
setblock ~ ~-6 ~-4 white_wool

fill ~ ~-1 ~-1 ~ ~-2 ~-1 spruce_fence
fill ~ ~-2 ~-2 ~ ~-3 ~-2 spruce_fence
fill ~ ~-3 ~-3 ~ ~-4 ~-3 spruce_fence
fill ~ ~-4 ~-4 ~ ~-5 ~-4 spruce_fence
setblock ~ ~-5 ~-5 spruce_fence

#Aile gauche
fill ~ ~0 ~-1 ~ ~0 ~-2 white_wool
setblock ~ ~1 ~-3 white_wool
setblock ~ ~2 ~-4 white_wool
setblock ~ ~3 ~-5 white_wool
setblock ~ ~4 ~-6 white_wool

fill ~ ~1 ~-1 ~ ~1 ~-2 spruce_fence
fill ~ ~2 ~-2 ~ ~2 ~-3 spruce_fence
fill ~ ~3 ~-3 ~ ~3 ~-4 spruce_fence
fill ~ ~4 ~-4 ~ ~4 ~-5 spruce_fence
setblock ~ ~5 ~-5 spruce_fence
scoreboard objectives add pharesActifs dummy
scoreboard objectives add fullCircleTicks dummy "Durée en ticks d'un tour complet"
scoreboard objectives add quartCircleTicks dummy "Durée en ticks d'un quart de tour"
scoreboard objectives add 8thCircleTicks dummy "Durée en ticks d'un huitième de tour"
scoreboard objectives add flashDistance dummy "Distance du signal lumineux"
scoreboard objectives add phareTrigger trigger "Variable de contrôle du phare"
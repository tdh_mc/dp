# Principalement une loop condition (>=80 => retour à 0)
execute if score @s currentTick matches ..-1 run scoreboard players set @s currentTick 0
execute if score @s currentTick >= @s fullCircleTicks run scoreboard players set @s currentTick 0

# Check de l'assignation de la variable quartCircleTicks
execute if score @s currentTick matches 0 run function tdh:ambient/phare/compute_quart_circle
execute unless score @s quartCircleTicks matches 1.. run function tdh:ambient/phare/compute_quart_circle

# Si on est revenus à 0 on remet la rotation à 0 également pour éviter les erreurs d'arrondi
execute if score @s currentTick matches 0 run tp @s ~ ~ ~ 0 0

# On calcule la position à laquelle les repeaters seront inactifs
scoreboard players operation @s temp = @s currentTick
scoreboard players operation @s temp %= @s quartCircleTicks
scoreboard players operation @s temp -= @s 8thCircleTicks

# On fait apparaître et disparaître les repeaters
execute if score @s temp matches -2 run function tdh:ambient/phare/large/off
execute if score @s temp matches 2 rotated ~45 ~ run function tdh:ambient/phare/large/spawn_repeaters

# Pour l'affichage du flash, on envoie juste le flash en face de nous
execute if score @s flashDistance matches ..124 run function tdh:ambient/phare/flash_100
execute if score @s flashDistance matches 125..174 run function tdh:ambient/phare/flash_150
execute if score @s flashDistance matches 175..224 run function tdh:ambient/phare/flash_200
execute if score @s flashDistance matches 225.. run function tdh:ambient/phare/flash_250

# Puis on tourne en fonction de la durée du cycle
execute if score @s fullCircleTicks matches ..37 run tp @s ~ ~ ~ ~12 ~
execute if score @s fullCircleTicks matches 38..52 run tp @s ~ ~ ~ ~8 ~
execute if score @s fullCircleTicks matches 53..74 run tp @s ~ ~ ~ ~6 ~
execute if score @s fullCircleTicks matches 75..104 run tp @s ~ ~ ~ ~4 ~
execute if score @s fullCircleTicks matches 105..149 run tp @s ~ ~ ~ ~3 ~
execute if score @s fullCircleTicks matches 150..224 run tp @s ~ ~ ~ ~2 ~
execute if score @s fullCircleTicks matches 225..314 run tp @s ~ ~ ~ ~1.5 ~
execute if score @s fullCircleTicks matches 315..449 run tp @s ~ ~ ~ ~1 ~
execute if score @s fullCircleTicks matches 450..629 run tp @s ~ ~ ~ ~0.66667 ~
execute if score @s fullCircleTicks matches 630..899 run tp @s ~ ~ ~ ~0.5 ~
execute if score @s fullCircleTicks matches 900.. run tp @s ~ ~ ~ ~0.33333 ~

# Enfin on incrémente currentTick de 1
scoreboard players add @s currentTick 1


# Principalement une loop condition (>=120 => retour à 0)
#execute unless score @s currentTick matches 0..119 run scoreboard players set @s currentTick 0

# Si on est revenus à 0 on remet la rotation à 0 également pour éviter les erreurs d'arrondi
#execute if score @s currentTick matches 0 run tp @s ~ ~ ~ 0 0

# On fait apparaître les repeaters à des ticks précis
#execute if score @s currentTick matches 0 run function tdh:ambient/phare/large/tick_repeaters_south
#execute if score @s currentTick matches 11 run function tdh:ambient/phare/large/off
#execute if score @s currentTick matches 15 rotated ~45 ~ run function tdh:ambient/phare/large/tick_repeaters_west
#execute if score @s currentTick matches 41 run function tdh:ambient/phare/large/off
#execute if score @s currentTick matches 45 rotated ~45 ~ run function tdh:ambient/phare/large/tick_repeaters_north
#execute if score @s currentTick matches 71 run function tdh:ambient/phare/large/off
#execute if score @s currentTick matches 75 rotated ~45 ~ run function tdh:ambient/phare/large/tick_repeaters_east
#execute if score @s currentTick matches 101 run function tdh:ambient/phare/large/off
#execute if score @s currentTick matches 105 rotated ~45 ~ run function tdh:ambient/phare/large/tick_repeaters_south

# En revanche pour l'affichage du flash, on envoie juste le flash en face de nous
#function tdh:ambient/phare/std/flash_150

# Puis on tourne de 3 degrés (1/120e de tour)
#tp @s ~ ~ ~ ~3 ~

# Enfin on incrémente currentTick de 1
#scoreboard players add @s currentTick 1
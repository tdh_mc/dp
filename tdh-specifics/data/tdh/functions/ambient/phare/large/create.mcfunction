summon armor_stand ~ ~ ~ {Invisible:1b,NoGravity:1b,CustomName:"\"Grand phare automatique\"",Tags:["GrandPhare","temptag"]}
scoreboard players set @e[tag=temptag,sort=nearest,limit=1] flashDistance 200
scoreboard players set @e[tag=temptag,sort=nearest,limit=1] fullCircleTicks 360
tag @e[tag=temptag,tag=GrandPhare] remove temptag
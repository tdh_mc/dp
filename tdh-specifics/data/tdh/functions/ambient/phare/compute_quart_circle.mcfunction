# On normalise les vitesses pour simplifier les calculs de rotation
execute if score @s fullCircleTicks matches ..37 run scoreboard players set @s fullCircleTicks 30
execute if score @s fullCircleTicks matches 38..52 run scoreboard players set @s fullCircleTicks 45
execute if score @s fullCircleTicks matches 53..74 run scoreboard players set @s fullCircleTicks 60
execute if score @s fullCircleTicks matches 75..104 run scoreboard players set @s fullCircleTicks 90
execute if score @s fullCircleTicks matches 105..149 run scoreboard players set @s fullCircleTicks 120
execute if score @s fullCircleTicks matches 150..224 run scoreboard players set @s fullCircleTicks 180
execute if score @s fullCircleTicks matches 225..314 run scoreboard players set @s fullCircleTicks 270
execute if score @s fullCircleTicks matches 315..449 run scoreboard players set @s fullCircleTicks 360
execute if score @s fullCircleTicks matches 450..629 run scoreboard players set @s fullCircleTicks 540
execute if score @s fullCircleTicks matches 630..899 run scoreboard players set @s fullCircleTicks 720
execute if score @s fullCircleTicks matches 900.. run scoreboard players set @s fullCircleTicks 1080

scoreboard players operation @s quartCircleTicks = @s fullCircleTicks
scoreboard players set @s temp 4
scoreboard players operation @s quartCircleTicks /= @s temp
scoreboard players operation @s 8thCircleTicks = @s quartCircleTicks
scoreboard players set @s temp 2
scoreboard players operation @s 8thCircleTicks /= @s temp
scoreboard players reset @s temp
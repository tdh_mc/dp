summon armor_stand ~ ~ ~ {Invisible:1b,NoGravity:1b,CustomName:"\"Phare automatique\"",Tags:["Phare","temptag"]}
scoreboard players set @e[tag=temptag,sort=nearest,limit=1] flashDistance 150
scoreboard players set @e[tag=temptag,sort=nearest,limit=1] fullCircleTicks 180
tag @e[tag=temptag,tag=Phare] remove temptag
# On trie en fonction du score
# Pour le paramètre 1 (période de rotation en ticks)
execute if score @p phareTrigger matches 1030 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 30
execute if score @p phareTrigger matches 1045 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 45
execute if score @p phareTrigger matches 1060 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 60
execute if score @p phareTrigger matches 1090 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 90
execute if score @p phareTrigger matches 1120 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 120
execute if score @p phareTrigger matches 1180 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 180
execute if score @p phareTrigger matches 1270 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 270
execute if score @p phareTrigger matches 1360 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 360
execute if score @p phareTrigger matches 1540 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 540
execute if score @p phareTrigger matches 1720 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 720
execute if score @p phareTrigger matches 11080 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] fullCircleTicks 1080

execute if score @p phareTrigger matches 1000..1999 run function tdh:ambient/phare/settings/phase2
execute if score @p phareTrigger matches 10000..19999 run function tdh:ambient/phare/settings/phase2

# Pour le paramètre 2 (portée du rayon lumineux)
execute if score @p phareTrigger matches 2100 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] flashDistance 100
execute if score @p phareTrigger matches 2150 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] flashDistance 150
execute if score @p phareTrigger matches 2200 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] flashDistance 200
execute if score @p phareTrigger matches 2250 run scoreboard players set @e[tag=SettingsManaged,distance=..75,sort=nearest,limit=1] flashDistance 250

execute if score @p phareTrigger matches 2000..2999 run function tdh:ambient/phare/settings/end
# On vérifie que chaque joueur ayant le tag ManagingSettings est toujours suffisamment proche d'un phare ; le cas échéant, on le supprime avant que le phare ne soit déchargé
execute at @a[tag=ManagingSettings] unless entity @e[distance=..66,tag=SettingsManaged] run function tdh:ambient/phare/settings/end

# On vérifie si un joueur ayant le tag ManagingSettings a modifié sa variable trigger
# Les valeurs 1 à 9 = les états initiaux de la fonction (1 par paramètre)
# Les valeurs définies par l'utilisateur sont préfixées par cet état initial (ex: 1180 pour la valeur 180 pour le paramètre 1)
execute at @a[tag=ManagingSettings,scores={phareTrigger=10..}] run function tdh:ambient/phare/settings/interpret

# Reschedule éventuel de la fonction
execute if entity @p[tag=ManagingSettings] run schedule function tdh:ambient/phare/settings/wait 5t
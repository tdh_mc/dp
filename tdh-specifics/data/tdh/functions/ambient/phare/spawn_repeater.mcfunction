scoreboard players set @s temp 0
# 1 = South, 2 = West, 3 = North, 4 = East
execute as @s[y_rotation=-44..45] run scoreboard players set @s temp 1
execute as @s[y_rotation=46..135] run scoreboard players set @s temp 2
execute unless entity @s[y_rotation=-135..136] run scoreboard players set @s temp 3
execute as @s[y_rotation=-134..-45] run scoreboard players set @s temp 4

# Selon la variable on spawn un repeater dans le sens correspondant
execute if score @s temp matches 1 run setblock ~ ~ ~ repeater[facing=north,powered=true,locked=true]
execute if score @s temp matches 2 run setblock ~ ~ ~ repeater[facing=east,powered=true,locked=true]
execute if score @s temp matches 3 run setblock ~ ~ ~ repeater[facing=south,powered=true,locked=true]
execute if score @s temp matches 4 run setblock ~ ~ ~ repeater[facing=west,powered=true,locked=true]

# On nettoie derrière nous
scoreboard players reset @s temp
# On essaie de trouver un phare ou un grand phare dans un rayon de 50, et on le tagge si on le trouve
execute as @e[tag=Phare,tag=!SettingsManaged,distance=..50,sort=nearest,limit=1] run tag @s add SettingsManaged
execute unless entity @e[tag=SettingsManaged,distance=..50] as @e[tag=GrandPhare,tag=!SettingsManaged,distance=..50,sort=nearest,limit=1] run tag @s add SettingsManaged

# Si on a trouvé un phare à gérer, on tagge le joueur ayant déclenché la commande
execute if entity @e[tag=SettingsManaged,distance=..50] run tag @s add ManagingSettings

# Si tout s'est bien passé, on initialise le joueur
execute as @s[tag=ManagingSettings] run function tdh:ambient/phare/settings/begin
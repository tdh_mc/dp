# On enregistre la valeur précédente du statut pour savoir si on vient de changer de mode
scoreboard players operation #phares temp = #phares status

# On calcule le nouveau statut (0 = pas de lumière, 1 = lumière)
scoreboard players set #phares status 0
execute if score #temps timeOfDay matches 2..4 run scoreboard players set #phares status 1
execute if score #meteo actuelle matches 2.. run scoreboard players set #phares status 1

# Selon le statut, on affiche ou non la lumière
execute if score #phares status matches 1 run function tdh:ambient/phare/tick_detail
execute if score #phares status matches 0 if score #phares temp matches 1 run function tdh:ambient/phare/tick_turn_down
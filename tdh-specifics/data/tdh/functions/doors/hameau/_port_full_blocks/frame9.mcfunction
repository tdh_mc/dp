# Point de référence global : [75 71 242] [coin le plus bas X/Z du rectangle de 4x12 piliers inclus]

# Effacement des blocs de la zone
fill ~1 ~0 ~0 ~10 ~-6 ~3 air


# Altitude = 0
setblock ~1 ~0 ~0 spruce_slab[type=bottom]
setblock ~1 ~0 ~3 spruce_slab[type=bottom]
setblock ~10 ~0 ~0 spruce_slab[type=bottom]
setblock ~10 ~0 ~3 spruce_slab[type=bottom]

setblock ~4 ~0 ~0 spruce_trapdoor[facing=east,half=bottom,open=true]
setblock ~4 ~0 ~3 spruce_trapdoor[facing=east,half=bottom,open=true]
setblock ~7 ~0 ~0 spruce_trapdoor[facing=west,half=bottom,open=true]
setblock ~7 ~0 ~3 spruce_trapdoor[facing=west,half=bottom,open=true]
fill ~4 ~0 ~1 ~4 ~0 ~2 oak_trapdoor[facing=east,half=bottom,open=true]
fill ~7 ~0 ~1 ~7 ~0 ~2 oak_trapdoor[facing=west,half=bottom,open=true]

setblock ~3 ~0 ~0 spruce_stairs[facing=east,half=bottom]
setblock ~3 ~0 ~3 spruce_stairs[facing=east,half=bottom]
setblock ~8 ~0 ~0 spruce_stairs[facing=west,half=bottom]
setblock ~8 ~0 ~3 spruce_stairs[facing=west,half=bottom]
fill ~3 ~0 ~1 ~3 ~0 ~2 oak_stairs[facing=east,half=bottom]
fill ~8 ~0 ~1 ~8 ~0 ~2 oak_stairs[facing=west,half=bottom]


# Altitude = -1
setblock ~1 ~-1 ~0 spruce_fence[west=true]
setblock ~1 ~-1 ~3 spruce_fence[west=true]
setblock ~10 ~-1 ~0 spruce_fence[east=true]
setblock ~10 ~-1 ~3 spruce_fence[east=true]

setblock ~4 ~-1 ~0 spruce_trapdoor[facing=east,half=bottom,open=true]
setblock ~4 ~-1 ~3 spruce_trapdoor[facing=east,half=bottom,open=true]
setblock ~7 ~-1 ~0 spruce_trapdoor[facing=west,half=bottom,open=true]
setblock ~7 ~-1 ~3 spruce_trapdoor[facing=west,half=bottom,open=true]
fill ~4 ~-1 ~1 ~4 ~-1 ~2 oak_trapdoor[facing=east,half=bottom,open=true]
fill ~7 ~-1 ~1 ~7 ~-1 ~2 oak_trapdoor[facing=west,half=bottom,open=true]

setblock ~3 ~-1 ~0 spruce_slab[type=double]
setblock ~3 ~-1 ~3 spruce_slab[type=double]
setblock ~8 ~-1 ~0 spruce_slab[type=double]
setblock ~8 ~-1 ~3 spruce_slab[type=double]
fill ~3 ~-1 ~1 ~3 ~-1 ~2 oak_slab[type=double]
fill ~8 ~-1 ~1 ~8 ~-1 ~2 oak_slab[type=double]


# Altitude = -2
setblock ~1 ~-2 ~0 lantern[hanging=true]
setblock ~1 ~-2 ~3 lantern[hanging=true]
setblock ~10 ~-2 ~0 lantern[hanging=true]
setblock ~10 ~-2 ~3 lantern[hanging=true]

setblock ~4 ~-2 ~0 spruce_trapdoor[facing=east,half=bottom,open=true]
setblock ~4 ~-2 ~3 spruce_trapdoor[facing=east,half=bottom,open=true]
setblock ~7 ~-2 ~0 spruce_trapdoor[facing=west,half=bottom,open=true]
setblock ~7 ~-2 ~3 spruce_trapdoor[facing=west,half=bottom,open=true]
fill ~4 ~-2 ~1 ~4 ~-2 ~2 oak_trapdoor[facing=east,half=bottom,open=true]
fill ~7 ~-2 ~1 ~7 ~-2 ~2 oak_trapdoor[facing=west,half=bottom,open=true]

setblock ~3 ~-2 ~0 spruce_slab[type=double]
setblock ~3 ~-2 ~3 spruce_slab[type=double]
setblock ~8 ~-2 ~0 spruce_slab[type=double]
setblock ~8 ~-2 ~3 spruce_slab[type=double]
fill ~3 ~-2 ~1 ~3 ~-2 ~2 oak_slab[type=double]
fill ~8 ~-2 ~1 ~8 ~-2 ~2 oak_slab[type=double]

setblock ~2 ~-2 ~0 spruce_stairs[facing=east,half=bottom]
setblock ~2 ~-2 ~3 spruce_stairs[facing=east,half=bottom]
setblock ~9 ~-2 ~0 spruce_stairs[facing=west,half=bottom]
setblock ~9 ~-2 ~3 spruce_stairs[facing=west,half=bottom]


# Altitude = -3
setblock ~1 ~-3 ~0 oak_fence[west=true]
setblock ~1 ~-3 ~3 oak_fence[west=true]
setblock ~10 ~-3 ~0 oak_fence[east=true]
setblock ~10 ~-3 ~3 oak_fence[east=true]

setblock ~3 ~-3 ~0 spruce_stairs[facing=west,half=top]
setblock ~3 ~-3 ~3 spruce_stairs[facing=west,half=top]
setblock ~8 ~-3 ~0 spruce_stairs[facing=east,half=top]
setblock ~8 ~-3 ~3 spruce_stairs[facing=east,half=top]
fill ~3 ~-3 ~1 ~3 ~-3 ~2 oak_stairs[facing=west,half=top]
fill ~8 ~-3 ~1 ~8 ~-3 ~2 oak_stairs[facing=east,half=top]

setblock ~2 ~-3 ~0 spruce_slab[type=double]
setblock ~2 ~-3 ~3 spruce_slab[type=double]
setblock ~9 ~-3 ~0 spruce_slab[type=double]
setblock ~9 ~-3 ~3 spruce_slab[type=double]


# Altitude = -4
setblock ~1 ~-4 ~0 oak_fence[west=true]
setblock ~1 ~-4 ~3 oak_fence[west=true]
setblock ~10 ~-4 ~0 oak_fence[east=true]
setblock ~10 ~-4 ~3 oak_fence[east=true]

setblock ~2 ~-4 ~0 spruce_slab[type=double]
setblock ~2 ~-4 ~3 spruce_slab[type=double]
setblock ~9 ~-4 ~0 spruce_slab[type=double]
setblock ~9 ~-4 ~3 spruce_slab[type=double]
fill ~2 ~-4 ~1 ~2 ~-4 ~2 oak_stairs[facing=east,half=bottom]
fill ~9 ~-4 ~1 ~9 ~-4 ~2 oak_stairs[facing=west,half=bottom]


# Altitude = -5
setblock ~2 ~-5 ~0 spruce_stairs[facing=west,half=top]
setblock ~2 ~-5 ~3 spruce_stairs[facing=west,half=top]
setblock ~9 ~-5 ~0 spruce_stairs[facing=east,half=top]
setblock ~9 ~-5 ~3 spruce_stairs[facing=east,half=top]
fill ~2 ~-5 ~1 ~2 ~-5 ~2 oak_trapdoor[facing=east,half=top,open=true]
fill ~9 ~-5 ~1 ~9 ~-5 ~2 oak_trapdoor[facing=west,half=top,open=true]

setblock ~1 ~-5 ~0 spruce_slab[type=double]
setblock ~1 ~-5 ~3 spruce_slab[type=double]
setblock ~10 ~-5 ~0 spruce_slab[type=double]
setblock ~10 ~-5 ~3 spruce_slab[type=double]
fill ~1 ~-5 ~1 ~1 ~-5 ~2 spruce_stairs[facing=east,half=bottom]
fill ~10 ~-5 ~1 ~10 ~-5 ~2 spruce_stairs[facing=west,half=bottom]


# Altitude = -6
setblock ~1 ~-6 ~0 spruce_stairs[facing=west,half=top]
setblock ~1 ~-6 ~3 spruce_stairs[facing=west,half=top]
setblock ~10 ~-6 ~0 spruce_stairs[facing=east,half=top]
setblock ~10 ~-6 ~3 spruce_stairs[facing=east,half=top]

fill ~1 ~-6 ~1 ~1 ~-6 ~2 oak_slab[type=double]
fill ~10 ~-6 ~1 ~10 ~-6 ~2 oak_slab[type=double]
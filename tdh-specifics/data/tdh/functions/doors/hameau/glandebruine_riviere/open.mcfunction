# Le Hameau - Porte glandebruine_riviere [Ouverture de la porte (sans vérification)]

#say @p Open

scoreboard players set fakeDoors doorHamGbRvOpen 1

schedule function tdh:doors/hameau/glandebruine_riviere/open_level1 0.15s
schedule function tdh:doors/hameau/glandebruine_riviere/open_level2 0.30s
schedule function tdh:doors/hameau/glandebruine_riviere/open_level3 0.45s
schedule function tdh:doors/hameau/glandebruine_riviere/open_level4 0.60s
schedule function tdh:doors/hameau/glandebruine_riviere/open_level5 0.75s
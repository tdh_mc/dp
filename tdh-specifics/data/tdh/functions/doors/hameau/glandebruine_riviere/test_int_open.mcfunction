# Le Hameau : Porte glandebruine_riviere [Ouverture depuis l'extérieur - Test de présence hostile]

function tdh:doors/hameau/glandebruine_riviere/test_hostiles

execute if score fakeDoors doorHamGbRvLock matches 1 run function tdh:doors/hameau/glandebruine_riviere/echec
execute if score fakeDoors doorHamGbRvLock matches 0 run function tdh:doors/hameau/glandebruine_riviere/int_open
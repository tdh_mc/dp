# Le Hameau : Porte glandebruine_riviere [Initialisation du scoreboard]
scoreboard objectives add doorHamGbRvOpen dummy "Le Hameau - Porte Glandebruine Rivière"
scoreboard objectives add doorHamGbRvLock dummy "Le Hameau - Porte Glandebruine Riviere [Verrou]"
scoreboard players set fakeDoors doorHamGbRvOpen 0
scoreboard players set fakeDoors doorHamGbRvLock 0
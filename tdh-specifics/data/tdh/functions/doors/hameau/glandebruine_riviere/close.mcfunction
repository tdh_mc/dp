# Le Hameau : Porte glandebruine_riviere [fermeture de la porte]

#say @p close

schedule function tdh:doors/hameau/glandebruine_riviere/close_level1 0.15s
schedule function tdh:doors/hameau/glandebruine_riviere/close_level2 0.30s
schedule function tdh:doors/hameau/glandebruine_riviere/close_level3 0.45s
schedule function tdh:doors/hameau/glandebruine_riviere/close_level4 0.60s
schedule function tdh:doors/hameau/glandebruine_riviere/close_level5 0.75s

scoreboard players set fakeDoors doorHamGbRvOpen 1
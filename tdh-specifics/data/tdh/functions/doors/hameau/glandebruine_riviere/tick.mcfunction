 # Le Hameau : Système de détection de la porte glandebruine_riviere [tests principaux]
 
 #say [[[[[[[[BEGIN TICK]]]]]]]]
 
 # Gestion des joueurs
 execute if entity @a[x=-32,y=64,z=85,distance=..20] run function tdh:doors/hameau/glandebruine_riviere/main
 
 # Fermeture de la porte [-37 60 80 -> -27 66 90]
 execute positioned -37 60 80 if score fakeDoors doorHamGbRvOpen matches 2 unless entity @a[dx=10,dy=6,dz=10] run function tdh:doors/hameau/glandebruine_riviere/close
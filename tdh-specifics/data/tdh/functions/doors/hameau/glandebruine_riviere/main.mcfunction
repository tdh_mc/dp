 # Le Hameau : Système de détection de la porte glandebruine_riviere [tests principaux]
 
 # say [[[[[[[[BEGIN MAIN]]]]]]]]
 
 # Extérieur S [-37 60 80 -> -33 66 90]
 execute positioned -37 60 80 at @a[dx=4,dy=6,dz=10,tag=!TDH_hameau_glandebruine_riviere_int,tag=!TDH_hameau_glandebruine_riviere_ext] run function tdh:doors/hameau/glandebruine_riviere/test_ext_open
 execute positioned -37 60 80 at @a[dx=4,dy=6,dz=10,tag=TDH_hameau_glandebruine_riviere_int,tag=!TDH_hameau_glandebruine_riviere_ext] run function tdh:doors/hameau/glandebruine_riviere/ext_greet
 
 # Intérieur S [-31 60 80 -> -27 66 90]
 execute positioned -31 60 80 at @a[dx=4,dy=6,dz=10,tag=!TDH_hameau_glandebruine_riviere_ext,tag=!TDH_hameau_glandebruine_riviere_int] run function tdh:doors/hameau/glandebruine_riviere/test_int_open
 execute positioned -31 60 80 at @a[dx=4,dy=6,dz=10,tag=TDH_hameau_glandebruine_riviere_ext,tag=!TDH_hameau_glandebruine_riviere_int] run function tdh:doors/hameau/glandebruine_riviere/int_greet
 
 # Extérieur R [-43 69 114 -> -43 79 132]
# execute positioned -43 69 114 at @a[dx=0,dy=10,dz=18] run function tdh:doors/hameau/glandebruine_riviere/reset
# execute positioned -36 71 116 at @a[dx=2,dy=10,dz=0] run function tdh:doors/hameau/glandebruine_riviere/reset
# execute positioned -36 71 128 at @a[dx=2,dy=10,dz=0] run function tdh:doors/hameau/glandebruine_riviere/reset
 # Intérieur R [-23 71 120 -> -23 79 130]
# execute positioned -23 71 120 at @a[dx=0,dy=8,dz=10] run function tdh:doors/hameau/glandebruine_riviere/reset

# Reset STD [distance à la porte > 10 car 10 > demi plus grande diagonale du carré ]
 execute positioned -32 63 85 at @a[tag=TDH_hameau_glandebruine_riviere_ext,distance=10..] run function tdh:doors/hameau/glandebruine_riviere/reset
 execute positioned -32 63 85 at @a[tag=TDH_hameau_glandebruine_riviere_int,distance=10..] run function tdh:doors/hameau/glandebruine_riviere/reset
# Le Hameau : Porte glandebruine_riviere [échec - impossible d'ouvrir la porte]

tellraw @p [{"text":"[Le Hameau - CGCAT] Ouverture impossible : créatures hostiles proches de la porte. (","color":"red"},{"selector":"@e[x=-32,y=63,z=85,distance=..20,type=#tdh:hostile]","color":"dark_red"},{"text":")","color":"red"}]
tag @p add TDH_hameau_glandebruine_riviere_ext
tag @p add TDH_hameau_glandebruine_riviere_int
playsound tdh:br.no record @a[distance=..16] -32 62 85 1
schedule function tdh:doors/hameau/glandebruine_riviere/playsound_access_denied 0.5s
# Le Hameau : Porte glandebruine_riviere [Ouverture depuis l'extérieur]

#say @p extOpen
execute if score fakeDoors doorHamGbRvOpen matches 0 run function tdh:doors/hameau/glandebruine_riviere/open
schedule function tdh:doors/hameau/glandebruine_riviere/playsound_acces_ok 0.3s

tag @p add TDH_hameau_glandebruine_riviere_ext
tellraw @p {"text":" [Le Hameau - CGCAT] Accès autorisé. Bienvenue au Hameau !","color":"green"}
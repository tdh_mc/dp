# Le Hameau : Porte glandebruine_riviere [Ouverture depuis l'intérieur]

#say @p intOpen
execute if score fakeDoors doorHamGbRvOpen matches 0 run function tdh:doors/hameau/glandebruine_riviere/open


tag @p add TDH_hameau_glandebruine_riviere_int
tellraw @p {"text":" [Le Hameau - CGCAT] Sortie autorisée. Nous espérons vous revoir bientôt !","color":"green"}
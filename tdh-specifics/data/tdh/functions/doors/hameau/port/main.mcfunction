 # Le Hameau : Système de détection du Pont Levant du Port [tests principaux]
 
 #say [[[[[[[[BEGIN MAIN]]]]]]]]
 
 # Extérieur S [76 63 246 -> 89 63 260]
 execute positioned 76 60 246 at @a[dx=13,dy=10,dz=14,tag=!TDH_hameau_port_int,tag=!TDH_hameau_port_ext] run function tdh:doors/hameau/port/test_ext_open
 execute positioned 76 60 246 at @a[dx=13,dy=10,dz=14,tag=TDH_hameau_port_int,tag=!TDH_hameau_port_ext] run function tdh:doors/hameau/port/ext_greet
 
 # Intérieur S [75 63 226 -> 89 63 241]
 execute positioned 75 60 226 at @a[dx=14,dy=10,dz=15,tag=!TDH_hameau_port_ext,tag=!TDH_hameau_port_int] run function tdh:doors/hameau/port/test_int_open
 execute positioned 75 60 226 at @a[dx=14,dy=10,dz=15,tag=TDH_hameau_port_ext,tag=!TDH_hameau_port_int] run function tdh:doors/hameau/port/int_greet
 
 # Intérieur R [69 63 222 -> 101 63 224]
 execute positioned 69 60 222 at @a[dx=32,dy=10,dz=2] run function tdh:doors/hameau/port/reset
 # Extérieur R [76 63 262 -> 100 63 264] + [72 63 250 -> 74 63 259]
 execute positioned 76 60 262 at @a[dx=24,dy=10,dz=2] run function tdh:doors/hameau/port/reset
 execute positioned 72 60 250 at @a[dx=2,dy=10,dz=9] run function tdh:doors/hameau/port/reset
# execute positioned -31 71 118 at @a[tag=TDH_hameau_port_ext,distance=8..] run function tdh:doors/hameau/port/reset
# execute positioned -31 71 118 at @a[tag=TDH_hameau_port_int,distance=8..] run function tdh:doors/hameau/port/reset
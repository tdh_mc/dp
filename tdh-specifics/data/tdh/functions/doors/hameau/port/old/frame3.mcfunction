# Point de référence global : [75 71 242] [coin le plus bas X/Z du rectangle de 4x12 piliers inclus]

# Effacement des blocs de la zone
fill ~1 ~0 ~0 ~10 ~-6 ~3 air


# Altitude = 0
setblock ~1 ~0 ~0 spruce_slab[type=bottom]
setblock ~1 ~0 ~3 spruce_slab[type=bottom]
setblock ~10 ~0 ~0 spruce_slab[type=bottom]
setblock ~10 ~0 ~3 spruce_slab[type=bottom]


# Altitude = -1
setblock ~1 ~-1 ~0 spruce_fence[west=true]
setblock ~1 ~-1 ~3 spruce_fence[west=true]
setblock ~10 ~-1 ~0 spruce_fence[east=true]
setblock ~10 ~-1 ~3 spruce_fence[east=true]


# Altitude = -2
setblock ~1 ~-2 ~0 lantern[hanging=true]
setblock ~1 ~-2 ~3 lantern[hanging=true]
setblock ~10 ~-2 ~0 lantern[hanging=true]
setblock ~10 ~-2 ~3 lantern[hanging=true]


# Altitude = -4
setblock ~1 ~-4 ~0 oak_fence[west=true]
setblock ~1 ~-4 ~3 oak_fence[west=true]
setblock ~10 ~-4 ~0 oak_fence[east=true]
setblock ~10 ~-4 ~3 oak_fence[east=true]

fill ~5 ~-4 ~0 ~6 ~-4 ~0 spruce_slab[type=bottom]
fill ~5 ~-4 ~3 ~6 ~-4 ~3 spruce_slab[type=bottom]


# Altitude = -5
setblock ~1 ~-5 ~0 oak_fence[west=true]
setblock ~1 ~-5 ~3 oak_fence[west=true]
setblock ~10 ~-5 ~0 oak_fence[east=true]
setblock ~10 ~-5 ~3 oak_fence[east=true]

fill ~5 ~-5 ~0 ~6 ~-5 ~0 spruce_slab[type=top]
fill ~5 ~-5 ~3 ~6 ~-5 ~3 spruce_slab[type=top]
fill ~5 ~-5 ~1 ~6 ~-5 ~2 oak_slab[type=top]

setblock ~4 ~-5 ~0 spruce_slab[type=double]
setblock ~4 ~-5 ~3 spruce_slab[type=double]
setblock ~7 ~-5 ~0 spruce_slab[type=double]
setblock ~7 ~-5 ~3 spruce_slab[type=double]
fill ~4 ~-5 ~1 ~4 ~-5 ~2 oak_slab[type=bottom]
fill ~7 ~-5 ~1 ~7 ~-5 ~2 oak_slab[type=bottom]

setblock ~3 ~-5 ~0 spruce_slab[type=bottom]
setblock ~3 ~-5 ~3 spruce_slab[type=bottom]
setblock ~8 ~-5 ~0 spruce_slab[type=bottom]
setblock ~8 ~-5 ~3 spruce_slab[type=bottom]
fill ~3 ~-5 ~1 ~3 ~-5 ~2 oak_trapdoor[facing=west,half=bottom]
fill ~8 ~-5 ~1 ~8 ~-5 ~2 oak_trapdoor[facing=east,half=bottom]

setblock ~3 ~-5 ~0 spruce_trapdoor[facing=south,half=bottom]
setblock ~3 ~-5 ~3 spruce_trapdoor[facing=north,half=bottom]
setblock ~8 ~-5 ~0 spruce_trapdoor[facing=south,half=bottom]
setblock ~8 ~-5 ~3 spruce_trapdoor[facing=north,half=bottom]


# Altitude = -6
setblock ~1 ~-6 ~0 spruce_stairs[facing=west,half=top]
setblock ~1 ~-6 ~3 spruce_stairs[facing=west,half=top]
setblock ~10 ~-6 ~0 spruce_stairs[facing=east,half=top]
setblock ~10 ~-6 ~3 spruce_stairs[facing=east,half=top]

setblock ~2 ~-6 ~0 spruce_slab[type=top]
setblock ~2 ~-6 ~3 spruce_slab[type=top]
setblock ~9 ~-6 ~0 spruce_slab[type=top]
setblock ~9 ~-6 ~3 spruce_slab[type=top]

setblock ~3 ~-6 ~0 spruce_trapdoor[facing=south,half=top]
setblock ~8 ~-6 ~0 spruce_trapdoor[facing=south,half=top]
setblock ~3 ~-6 ~3 spruce_trapdoor[facing=north,half=top]
setblock ~8 ~-6 ~3 spruce_trapdoor[facing=north,half=top]

fill ~2 ~-6 ~1 ~2 ~-6 ~2 oak_slab[type=top]
fill ~9 ~-6 ~1 ~9 ~-6 ~2 oak_slab[type=top]

fill ~3 ~-6 ~1 ~3 ~-6 ~2 spruce_trapdoor[facing=east,half=top]
fill ~8 ~-6 ~1 ~8 ~-6 ~2 spruce_trapdoor[facing=west,half=top]

fill ~1 ~-6 ~1 ~1 ~-6 ~2 oak_slab[type=bottom]
fill ~10 ~-6 ~1 ~10 ~-6 ~2 oak_slab[type=bottom]
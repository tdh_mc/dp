# Le Hameau : Pont Levant du Port [Initialisation du scoreboard]
scoreboard objectives add doorHamPortOpen dummy "Le Hameau - Pont Levant du Port"
scoreboard objectives add doorHamPortLock dummy "Le Hameau - Pont Levant du Port [Verrou]"
scoreboard players set fakeDoors doorHamPortOpen 0
scoreboard players set fakeDoors doorHamPortLock 0
# Le Hameau : Pont Levant du Port [Ouverture depuis l'intérieur]

#say @p intOpen
execute if score fakeDoors doorHamPortOpen matches 0 run function tdh:doors/hameau/port/open


tag @p add TDH_hameau_port_int
tellraw @p {"text":" [Le Hameau - CGCAT] Sortie autorisée. Nous espérons vous revoir bientôt !","color":"green"}
# Le Hameau : Pont Levant du Port [fermeture de la porte]

#say @p close

scoreboard players set fakeDoors doorHamPortOpen 1

schedule function tdh:doors/hameau/port/frame8_pos 1.65s
schedule function tdh:doors/hameau/port/frame7_pos 1.80s
schedule function tdh:doors/hameau/port/frame6_pos 1.95s
schedule function tdh:doors/hameau/port/frame5_pos 2.10s
schedule function tdh:doors/hameau/port/frame4_pos 2.25s
schedule function tdh:doors/hameau/port/frame3_pos 2.40s
schedule function tdh:doors/hameau/port/frame2_pos 2.55s
schedule function tdh:doors/hameau/port/frame1_pos 2.70s
schedule function tdh:doors/hameau/port/frame0_pos 2.85s

schedule function tdh:doors/hameau/port/close_level1 1.7s 
schedule function tdh:doors/hameau/port/close_level2 1.9s
schedule function tdh:doors/hameau/port/close_level3 2.1s
schedule function tdh:doors/hameau/port/close_level4 2.3s
schedule function tdh:doors/hameau/port/close_level5 2.5s
schedule function tdh:doors/hameau/port/close_level6 2.7s

execute positioned 80 63 245 run playsound tdh:br.door.big block @a[distance=..32] ~ ~ ~ 2 1.0
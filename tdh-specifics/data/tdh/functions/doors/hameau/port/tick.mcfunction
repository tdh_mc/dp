 # Le Hameau : Système de détection du Pont Levant du Port [tests principaux]
 
 #say [[[[[[[[BEGIN TICK]]]]]]]]
 
 # Gestion des joueurs
 execute if entity @a[x=80,y=63,z=245,distance=..35] run function tdh:doors/hameau/port/main
 
 # Fermeture de la porte [69 60 226 -> 101 70 264]
 execute positioned 69 60 226 if score fakeDoors doorHamPortOpen matches 2 unless entity @a[dx=32,dy=10,dz=38] run function tdh:doors/hameau/port/close
# Le Hameau : Pont Levant du Port [Ouverture depuis l'extérieur]

#say @p extOpen
execute if score fakeDoors doorHamPortOpen matches 0 run function tdh:doors/hameau/port/open
schedule function tdh:doors/hameau/port/playsound_acces_ok 0.3s

tag @p add TDH_hameau_port_ext
tellraw @p {"text":" [Le Hameau - CGCAT] Accès autorisé. Bienvenue au Hameau !","color":"green"}
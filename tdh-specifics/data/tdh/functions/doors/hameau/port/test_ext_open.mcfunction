# Le Hameau : Pont Levant du Port [Ouverture depuis l'extérieur - Test de présence hostile]

function tdh:doors/hameau/port/test_hostiles

execute if score fakeDoors doorHamPortLock matches 1 run function tdh:doors/hameau/port/echec
execute if score fakeDoors doorHamPortLock matches 0 run function tdh:doors/hameau/port/ext_open
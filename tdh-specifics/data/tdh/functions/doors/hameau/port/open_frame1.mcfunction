# Altitude = -5
fill ~3 ~-5 ~0 ~4 ~-5 ~0 air
fill ~3 ~-5 ~3 ~4 ~-5 ~3 air
fill ~7 ~-5 ~0 ~8 ~-5 ~0 air
fill ~7 ~-5 ~3 ~8 ~-5 ~3 air

fill ~5 ~-5 ~0 ~6 ~-5 ~0 air
fill ~5 ~-5 ~3 ~6 ~-5 ~3 air

fill ~5 ~-5 ~1 ~5 ~-5 ~2 air
fill ~6 ~-5 ~1 ~6 ~-5 ~2 air

fill ~2 ~-5 ~0 ~3 ~-5 ~0 spruce_trapdoor[facing=south,half=bottom]
fill ~8 ~-5 ~0 ~9 ~-5 ~0 spruce_trapdoor[facing=south,half=bottom]
fill ~2 ~-5 ~3 ~3 ~-5 ~3 spruce_trapdoor[facing=north,half=bottom]
fill ~8 ~-5 ~3 ~9 ~-5 ~3 spruce_trapdoor[facing=north,half=bottom]

fill ~4 ~-5 ~0 ~7 ~-5 ~0 spruce_slab[type=bottom]
fill ~4 ~-5 ~3 ~7 ~-5 ~3 spruce_slab[type=bottom]

fill ~4 ~-5 ~1 ~7 ~-5 ~1 oak_trapdoor[facing=south,half=bottom]
fill ~4 ~-5 ~2 ~7 ~-5 ~2 oak_trapdoor[facing=north,half=bottom]



# Altitude = -6
setblock ~4 ~-6 ~0 air
setblock ~4 ~-6 ~3 air
setblock ~7 ~-6 ~0 air
setblock ~7 ~-6 ~3 air

setblock ~3 ~-6 ~0 spruce_trapdoor[facing=south,half=top]
setblock ~8 ~-6 ~0 spruce_trapdoor[facing=south,half=top]
setblock ~3 ~-6 ~3 spruce_trapdoor[facing=north,half=top]
setblock ~8 ~-6 ~3 spruce_trapdoor[facing=north,half=top]

fill ~4 ~-6 ~1 ~4 ~-6 ~2 air
fill ~7 ~-6 ~1 ~7 ~-6 ~2 air

fill ~2 ~-6 ~1 ~2 ~-6 ~2 oak_slab[type=top]
fill ~9 ~-6 ~1 ~9 ~-6 ~2 oak_slab[type=top]

fill ~1 ~-6 ~1 ~2 ~-6 ~2 air
fill ~9 ~-6 ~1 ~10 ~-6 ~2 air




fill ~1 ~-6 ~1 ~1 ~-6 ~2 oak_slab[type=bottom]
fill ~10 ~-6 ~1 ~10 ~-6 ~2 oak_slab[type=bottom]
# Le Hameau : Pont Levant du Port [échec - impossible d'ouvrir la porte]

tellraw @p [{"text":"[Le Hameau - CGCAT] Ouverture impossible : créatures hostiles proches de la porte. (","color":"red"},{"selector":"@e[x=80,y=63,z=245,distance=..30,type=#tdh:hostile]","color":"dark_red"},{"text":")","color":"red"}]
tag @p add TDH_hameau_port_ext
tag @p add TDH_hameau_port_int
playsound tdh:br.no record @a[distance=..24] 80 63 245 2
schedule function tdh:doors/hameau/port/playsound_access_denied 0.5s
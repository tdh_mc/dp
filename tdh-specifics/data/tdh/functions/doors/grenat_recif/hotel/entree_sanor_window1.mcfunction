# Grenat Récif : Système d'ouverture/fermeture de la fenêtre EST de l'entrée de l'hôtel

execute if score systemMem windowClosed0001 matches 0 run function tdh:doors/grenat_recif/hotel/entree_sanor_window1_close
execute if score systemMem windowClosed0001 matches 1 run function tdh:doors/grenat_recif/hotel/entree_sanor_window1_open
execute if score systemMem windowClosed0001 matches 1 run scoreboard players set systemMem windowClosed0001 -1
execute if score systemMem windowClosed0001 matches 0 run scoreboard players set systemMem windowClosed0001 1
execute if score systemMem windowClosed0001 matches -1 run scoreboard players set systemMem windowClosed0001 0
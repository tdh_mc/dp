summon skeleton ~ ~ ~ {CustomName:"\"Archer squelette\"",Tags:["AntiSpawnDone","DonjonIllysia","SpawnTempTag"],DeathLootTable:"tdh:donjon/mob/squelette_archer_tier1",HandItems:[{id:"minecraft:bow",Count:1b},{}]}

execute store result score @e[tag=SpawnTempTag,sort=nearest,limit=1] temp run data get entity @e[sort=random,limit=1] Pos[0]
scoreboard players set @e[tag=SpawnTempTag,sort=nearest,limit=1] temp2 20
scoreboard players operation @e[tag=SpawnTempTag,sort=nearest,limit=1] temp %= @e[tag=SpawnTempTag,sort=nearest,limit=1] temp2

execute as @e[tag=SpawnTempTag,sort=nearest,limit=1] if score @s temp matches 0..2 run data merge entity @s {LeftHanded:1b}
execute as @e[tag=SpawnTempTag,sort=nearest,limit=1] if score @s temp matches 9.. run data merge entity @s {ArmorItems:[{},{},{},{Count:1b,id:"minecraft:leather_helmet"}]}

scoreboard players reset @e[tag=SpawnTempTag,sort=nearest,limit=1] temp
scoreboard players reset @e[tag=SpawnTempTag,sort=nearest,limit=1] temp2
tag @e[tag=SpawnTempTag,sort=nearest,limit=1] remove SpawnTempTag
summon skeleton ~ ~ ~ {CustomName:"\"Fantassin squelette\"",Tags:["AntiSpawnDone","DonjonIllysia","SpawnTempTag"],DeathLootTable:"tdh:donjon/mob/squelette_epeiste_tier1",HandItems:[{id:"minecraft:stone_sword",Count:1b},{}],ArmorItems:[{},{Count:1b,id:"minecraft:leather_leggings"},{Count:1b,id:"minecraft:leather_chestplate"},{}]}

execute store result score @e[tag=SpawnTempTag,sort=nearest,limit=1] temp run data get entity @e[sort=random,limit=1] Pos[0]
scoreboard players set @e[tag=SpawnTempTag,sort=nearest,limit=1] temp2 20
scoreboard players operation @e[tag=SpawnTempTag,sort=nearest,limit=1] temp %= @e[tag=SpawnTempTag,sort=nearest,limit=1] temp2

execute as @e[tag=SpawnTempTag,sort=nearest,limit=1] if score @s temp matches 16..18 run data merge entity @s {LeftHanded:1b}
execute as @e[tag=SpawnTempTag,sort=nearest,limit=1] if score @s temp matches 0..7 run data modify entity @s ArmorItems[0] set value {Count:1b,id:"minecraft:leather_boots"}
execute as @e[tag=SpawnTempTag,sort=nearest,limit=1] if score @s temp matches 18..19 run data modify entity @s HandItems[1] set value {Count:1b,id:"minecraft:shield"}

scoreboard players reset @e[tag=SpawnTempTag,sort=nearest,limit=1] temp
scoreboard players reset @e[tag=SpawnTempTag,sort=nearest,limit=1] temp2
tag @e[tag=SpawnTempTag,sort=nearest,limit=1] remove SpawnTempTag
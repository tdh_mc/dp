# Variables nécessaires pour les scripts d'entrée/sortie d'un donjon
scoreboard objectives add formDonjon trigger "Formulaire de non-responsabilité de donjon"


# Variables nécessaires pour les scripts de combat
scoreboard objectives add nombreDeces deathCount "Nombre de décès"
scoreboard objectives add nombreVictimes totalKillCount "Nombre de victimes"
scoreboard objectives add nombreVictimesH playerKillCount "Nombre de victimes humaines"

scoreboard objectives add nombreJoueurs dummy "Nombre de joueurs"
scoreboard objectives add nombreEnnemis dummy "Nombre d'ennemis"

scoreboard objectives add gameStatus dummy "Statut du jeu"

# Toujours pour les scripts de combat (boss bars)
bossbar add illysia:salle1_ennemis "Ennemis en vie"
bossbar set illysia:salle1_ennemis color red
bossbar set illysia:salle1_ennemis max 10
bossbar set illysia:salle1_ennemis style notched_10

bossbar add illysia:salle1_joueurs "Joueurs en vie"
bossbar set illysia:salle1_joueurs color green
bossbar set illysia:salle1_joueurs max 10
bossbar set illysia:salle1_joueurs style notched_10


# Variables nécessaires pour les plateformes
scoreboard objectives add dir dummy "Direction"
scoreboard objectives add blockType dummy "Type de bloc"
scoreboard objectives add xMin dummy
scoreboard objectives add xMax dummy
scoreboard objectives add yMin dummy
scoreboard objectives add yMax dummy
scoreboard objectives add zMin dummy
scoreboard objectives add zMax dummy
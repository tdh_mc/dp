# On vérifie si on est initialisé dans la bonne direction
execute if score @s dir matches 01..02 unless score @s posX matches 0.. unless score @s posX matches ..0 store result score @s posX run data get entity @s Pos[0]
execute if score @s dir matches 11..12 unless score @s posY matches 0.. unless score @s posY matches ..0 store result score @s posY run data get entity @s Pos[1]
execute if score @s dir matches 21..22 unless score @s posZ matches 0.. unless score @s posZ matches ..0 store result score @s posZ run data get entity @s Pos[2]

# Direction = axe X (0x)
# Sens : X pos -> X nég (01)
execute if score @s dir matches 01 run function tdh:donjon/plateforme/move_west
# Sens : X nég -> X pos (02)
execute if score @s dir matches 02 run function tdh:donjon/plateforme/move_east

# Direction = axe Y (1x)
# Sens : Y pos -> Y nég (11)
execute if score @s dir matches 11 run function tdh:donjon/plateforme/move_down
# Sens : Y nég -> Y pos (12)
execute if score @s dir matches 12 run function tdh:donjon/plateforme/move_up

# Direction = axe Z (2x)
# Sens : Z pos -> Z nég (21)
execute if score @s dir matches 21 run function tdh:donjon/plateforme/move_north
# Sens : Z nég -> Z pos (22)
execute if score @s dir matches 22 run function tdh:donjon/plateforme/move_south

# Inversion de la direction si le max/min est atteint
execute if score @s dir matches 01 if score @s posX <= @s xMin run scoreboard players set @s dir 02
execute if score @s dir matches 02 if score @s posX >= @s xMax run scoreboard players set @s dir 01

execute if score @s dir matches 11 if score @s posY <= @s yMin run scoreboard players set @s dir 12
execute if score @s dir matches 12 if score @s posY >= @s yMax run scoreboard players set @s dir 11

execute if score @s dir matches 21 if score @s posZ <= @s zMin run scoreboard players set @s dir 22
execute if score @s dir matches 22 if score @s posZ >= @s zMax run scoreboard players set @s dir 21

# On joue un son
playsound block.metal_pressure_plate.click_off block @a[distance=..15] ~ ~ ~ 1 1
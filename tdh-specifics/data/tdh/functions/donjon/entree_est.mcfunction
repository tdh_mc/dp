# Initialisation du trigger
scoreboard players enable @p[distance=..1] formDonjon
execute at @p[distance=..1] unless score @p formDonjon matches -1.. run scoreboard players set @p formDonjon 0

# Dans le cas où le score du joueur le plus proche est 0, cela signifie qu'il n'a pas accepté : on lui envoie donc le message
execute at @p[distance=..1,scores={formDonjon=0}] run function tdh:donjon/entree_text_greet

# Dans le cas où cela vaut 1, il a accepté : donc on lui affiche le message de bienvenue
execute at @p[distance=..1,scores={formDonjon=1}] run function tdh:donjon/entree_text_success

# On définit ensuite sa variable à 2 pour pouvoir le sélectionner de plus loin (après le tp) et on le tp
scoreboard players set @p[distance=..1,scores={formDonjon=1}] formDonjon 2
execute at @p[distance=..1,scores={formDonjon=2}] run tp @p ~2.5 ~ ~

# On redéfinit ensuite la variable à 0 pour la personne qu'on a tp
execute at @a[scores={formDonjon=2},distance=..10] run scoreboard players set @p formDonjon 0

# On schedule le reset des joueurs à -1 dans quelques ticks
schedule function tdh:donjon/entree_reset 20t
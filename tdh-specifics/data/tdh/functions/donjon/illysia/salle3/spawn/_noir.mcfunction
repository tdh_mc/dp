#On invoque les pions
execute positioned -701.5 47.00 142.5 run function tdh:donjon/illysia/salle3/spawn/noir/pion
execute positioned -704.5 47.00 142.5 run function tdh:donjon/illysia/salle3/spawn/noir/pion
execute positioned -707.5 47.00 142.5 run function tdh:donjon/illysia/salle3/spawn/noir/pion
execute positioned -710.5 47.00 142.5 run function tdh:donjon/illysia/salle3/spawn/noir/pion
execute positioned -713.5 47.00 142.5 run function tdh:donjon/illysia/salle3/spawn/noir/pion
execute positioned -716.5 47.00 142.5 run function tdh:donjon/illysia/salle3/spawn/noir/pion
execute positioned -719.5 47.00 142.5 run function tdh:donjon/illysia/salle3/spawn/noir/pion
execute positioned -722.5 47.00 142.5 run function tdh:donjon/illysia/salle3/spawn/noir/pion

#On invoque les pièces
execute positioned -701.5 47.00 145.5 run function tdh:donjon/illysia/salle3/spawn/noir/tour
execute positioned -704.5 47.00 145.5 run function tdh:donjon/illysia/salle3/spawn/noir/cavalier
execute positioned -707.5 47.00 145.5 run function tdh:donjon/illysia/salle3/spawn/noir/fou
execute positioned -710.5 47.00 145.5 run function tdh:donjon/illysia/salle3/spawn/noir/reine
execute positioned -713.5 47.00 145.5 run function tdh:donjon/illysia/salle3/spawn/noir/roi
execute positioned -716.5 47.00 145.5 run function tdh:donjon/illysia/salle3/spawn/noir/fou
execute positioned -719.5 47.00 145.5 run function tdh:donjon/illysia/salle3/spawn/noir/cavalier
execute positioned -722.5 47.00 145.5 run function tdh:donjon/illysia/salle3/spawn/noir/tour
#On invoque les pions
execute positioned -701.5 47.00 127.5 run function tdh:donjon/illysia/salle3/spawn/blanc/pion
execute positioned -704.5 47.00 127.5 run function tdh:donjon/illysia/salle3/spawn/blanc/pion
execute positioned -707.5 47.00 127.5 run function tdh:donjon/illysia/salle3/spawn/blanc/pion
execute positioned -710.5 47.00 127.5 run function tdh:donjon/illysia/salle3/spawn/blanc/pion
execute positioned -713.5 47.00 127.5 run function tdh:donjon/illysia/salle3/spawn/blanc/pion
execute positioned -716.5 47.00 127.5 run function tdh:donjon/illysia/salle3/spawn/blanc/pion
execute positioned -719.5 47.00 127.5 run function tdh:donjon/illysia/salle3/spawn/blanc/pion
execute positioned -722.5 47.00 127.5 run function tdh:donjon/illysia/salle3/spawn/blanc/pion

#On invoque les pièces
execute positioned -701.5 47.00 124.5 run function tdh:donjon/illysia/salle3/spawn/blanc/tour
execute positioned -704.5 47.00 124.5 run function tdh:donjon/illysia/salle3/spawn/blanc/cavalier
execute positioned -707.5 47.00 124.5 run function tdh:donjon/illysia/salle3/spawn/blanc/fou
execute positioned -710.5 47.00 124.5 run function tdh:donjon/illysia/salle3/spawn/blanc/reine
execute positioned -713.5 47.00 124.5 run function tdh:donjon/illysia/salle3/spawn/blanc/roi
execute positioned -716.5 47.00 124.5 run function tdh:donjon/illysia/salle3/spawn/blanc/fou
execute positioned -719.5 47.00 124.5 run function tdh:donjon/illysia/salle3/spawn/blanc/cavalier
execute positioned -722.5 47.00 124.5 run function tdh:donjon/illysia/salle3/spawn/blanc/tour
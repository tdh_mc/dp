# Invocation du squelette [TOUR NOIRE]
summon wither_skeleton ~ ~3 ~ {NoAI:1b,NoGravity:1b,HandItems:[{id:"bow",Count:1b},{}],ArmorItems:[{},{},{},{id:"iron_helmet",Count:1b}],Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Tour"],DeathLootTable:"minecraft:empty",Rotation:[180f,0f],HandDropChances:[0f,0f],ArmorDropChances:[0f,0f,0f,0f]}

# Invocation des 4 "créneaux" de la tour
summon falling_block ~ ~3 ~1 {NoGravity:1b,Time:-2147483648,BlockState:{Name:"dark_oak_trapdoor",Properties:{facing:"south",half:"bottom",open:"true"}},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}
summon falling_block ~ ~3 ~-1 {NoGravity:1b,Time:-2147483648,BlockState:{Name:"dark_oak_trapdoor",Properties:{facing:"north",half:"bottom",open:"true"}},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}
summon falling_block ~1 ~3 ~ {NoGravity:1b,Time:-2147483648,BlockState:{Name:"dark_oak_trapdoor",Properties:{facing:"east",half:"bottom",open:"true"}},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}
summon falling_block ~-1 ~3 ~ {NoGravity:1b,Time:-2147483648,BlockState:{Name:"dark_oak_trapdoor",Properties:{facing:"west",half:"bottom",open:"true"}},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}

# Invocation du corps de la tour
fill ~ ~ ~ ~ ~2 ~ barrier
summon falling_block ~ ~2 ~ {NoGravity:1b,Time:-2147483648,BlockState:{Name:"dark_oak_planks"},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}
summon falling_block ~ ~1 ~ {NoGravity:1b,Time:-2147483648,BlockState:{Name:"dark_oak_planks"},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}
summon falling_block ~ ~ ~ {NoGravity:1b,Time:-2147483648,BlockState:{Name:"dark_oak_slab",Properties:{type:"top"}},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}

# Invocation des roues
summon falling_block ~1 ~ ~ {NoGravity:1b,Time:-2147483648,BlockState:{Name:"oak_trapdoor",Properties:{facing:"east",half:"top",open:"true"}},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}
summon falling_block ~-1 ~ ~ {NoGravity:1b,Time:-2147483648,BlockState:{Name:"oak_trapdoor",Properties:{facing:"west",half:"top",open:"true"}},Tags:["AntiSpawnDone","DonjonIllysia","IllysiaSalle3","Noir","Technical"]}
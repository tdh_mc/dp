# On affiche un message à chaque joueur ayant déjà un élytra
execute positioned -689.5 57.0 168.0 at @a[dx=0,dy=2,dz=5] if entity @p[distance=..0.01,nbt={Inventory:[{id:"minecraft:elytra"}]}] run function tdh:donjon/illysia/salle2/give_elytra_fail

# On exécute la fonction qui give un elytra à chaque joueur n'ayant pas d'elytra sur lui
execute positioned -689.5 57.0 168.0 at @a[dx=0,dy=2,dz=5] unless entity @p[distance=..0.01,nbt={Inventory:[{id:"minecraft:elytra"}]}] run function tdh:donjon/illysia/salle2/give_elytra
# Si la variable d'état n'est pas initialisée, on la met à 0 ; sinon, on l'incrémente
execute if score #IllysiaSalle2 currentTick matches 0.. run scoreboard players add #IllysiaSalle2 currentTick 1
execute unless score #IllysiaSalle2 currentTick matches 0.. run scoreboard players set #IllysiaSalle2 currentTick 0

# Partie plateformes alternatives (une fois c'est les unes, une fois c'est les autres)
# On utilise le modulo de la variable d'état pour faire osciller, on calcule donc ce modulo
scoreboard players set #IllysiaSalle2 currentTickDiv 16
scoreboard players operation #IllysiaSalle2 currentTickMod = #IllysiaSalle2 currentTick
scoreboard players operation #IllysiaSalle2 currentTickMod %= #IllysiaSalle2 currentTickDiv

# En fonction du modulo, on affiche les bonnes plateformes
execute if score #IllysiaSalle2 currentTickMod matches 0 run function tdh:donjon/illysia/salle2/state0_on
execute if score #IllysiaSalle2 currentTickMod matches 1 run function tdh:donjon/illysia/salle2/state1_off
execute if score #IllysiaSalle2 currentTickMod matches 8 run function tdh:donjon/illysia/salle2/state1_on
execute if score #IllysiaSalle2 currentTickMod matches 9 run function tdh:donjon/illysia/salle2/state0_off

# On exécute les bons playsound également
execute if score #IllysiaSalle2 currentTickMod matches 0 positioned -703.5 36.0 165.5 run playsound block.metal_pressure_plate.click_on block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 1 positioned -703.5 36.0 165.5 run playsound block.metal_pressure_plate.click_off block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 3 positioned -703.5 36.0 165.5 run playsound block.ladder.place block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 5 positioned -703.5 36.0 165.5 run playsound block.ladder.place block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 7 positioned -703.5 36.0 165.5 run playsound block.ladder.place block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 8 positioned -703.5 36.0 165.5 run playsound block.metal_pressure_plate.click_on block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 9 positioned -703.5 36.0 165.5 run playsound block.metal_pressure_plate.click_off block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 11 positioned -703.5 36.0 165.5 run playsound block.ladder.place block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 13 positioned -703.5 36.0 165.5 run playsound block.ladder.place block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1
execute if score #IllysiaSalle2 currentTickMod matches 15 positioned -703.5 36.0 165.5 run playsound block.ladder.place block @a[dx=53,dy=24,dz=9] -681.5 53.5 170.5 1 1 0.1

# Pour les plateformes sur armor stands, on exécute le tick une fois par seconde (quand mod = 0/4/8/12)
execute if score #IllysiaSalle2 currentTickMod matches 0..12 unless score #IllysiaSalle2 currentTickMod matches 1..3 unless score #IllysiaSalle2 currentTickMod matches 5..7 unless score #IllysiaSalle2 currentTickMod matches 9..11 positioned -703.5 36.0 165.5 as @e[dx=53,dy=24,dz=9,tag=IllysiaSalle2,tag=Plateforme] at @s run function tdh:donjon/plateforme/tick

# On joue également un son 1 tick avant
execute if score #IllysiaSalle2 currentTickMod matches 3..15 unless score #IllysiaSalle2 currentTickMod matches 4..6 unless score #IllysiaSalle2 currentTickMod matches 8..10 unless score #IllysiaSalle2 currentTickMod matches 12..14 positioned -703.5 36.0 165.5 as @e[dx=53,dy=24,dz=9,tag=IllysiaSalle2,tag=Plateforme] at @s run function tdh:donjon/plateforme/pre_tick

# S'il y a toujours des joueurs dans la salle, on schedule un nouveau tick dans 5 ticks
execute positioned -716 0 160 if entity @a[dx=100,dy=70,dz=20] run schedule function tdh:donjon/illysia/salle2/loop 5t

# S'il n'y a plus de joueurs dans la salle, on vérifie à nouveau s'il y en a dans 100 ticks
execute positioned -716 0 160 if entity @a[dx=100,dy=70,dz=20] run schedule function tdh:donjon/illysia/salle2/try_loop 100t
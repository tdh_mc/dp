# S'il n'y a pas de joueurs dans la salle, on réessaie dans 100 ticks
execute positioned -716 0 160 unless entity @a[dx=100,dy=70,dz=20] run schedule function tdh:donjon/illysia/salle2/try_loop 100t

# S'il y en a, on recommence l'animation
execute positioned -716 0 160 if entity @a[dx=100,dy=70,dz=20] run function tdh:donjon/illysia/salle2/loop
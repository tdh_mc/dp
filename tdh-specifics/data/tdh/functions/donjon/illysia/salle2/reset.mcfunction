# On ouvre la porte de la salle
#setblock -707 47 170 redstone_torch

# On réinitialise les loot tables
data merge block -696 44 179 {LootTable:"tdh:donjon/chest/anti_lave"}

# On réinitialise l'état des autres objets
setblock -698 44 177 water_cauldron[level=2]

# On schedule la fonction qui fait se déplacer les plateformes
schedule function tdh:donjon/illysia/salle2/loop 5t
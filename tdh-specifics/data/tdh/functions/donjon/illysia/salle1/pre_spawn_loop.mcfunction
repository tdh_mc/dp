# On réduit le temps restant
scoreboard players remove #IllysiaSalle1 remainingTicks 1

# S'il reste 5 secondes ou moins, on l'affiche dans l'actionbar
execute if score #IllysiaSalle1 remainingTicks matches ..5 positioned -745.5 54.0 167.5 run title @a[dx=9,dy=4,dz=6] actionbar [{"text":"La porte se fermera dans ","color":"gold"},{"score":{"name":"#IllysiaSalle1","objective":"remainingTicks"},"color":"yellow","bold":"true"},{"text":" secondes.","color":"gold"}]

# S'il ne reste plus de ticks et qu'il y a quelqu'un, on démarre le jeu
execute if score #IllysiaSalle1 remainingTicks matches 0 positioned -738.5 54.0 169.5 if entity @a[dx=2,dy=2,dz=2,gamemode=!spectator] run function tdh:donjon/illysia/salle1/spawn

# S'il n'y a plus personne, on annule
execute positioned -738.5 54.0 169.5 unless entity @a[dx=2,dy=2,dz=2,gamemode=!spectator] run function tdh:donjon/illysia/salle1/pre_spawn_cancel

# On schedule la fonction à nouveau s'il reste des ticks
execute if score #IllysiaSalle1 remainingTicks matches 1.. run schedule function tdh:donjon/illysia/salle1/pre_spawn_loop 1s
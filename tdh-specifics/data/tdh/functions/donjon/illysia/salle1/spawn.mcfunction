

# Dans la petite nacelle centrale
execute positioned -724.5 56.5 170.0 run function tdh:donjon/spawn/squelette_archer_tier1
execute positioned -724.5 56.5 171.0 run function tdh:donjon/spawn/squelette_epeiste_tier1

# Dans la nacelle au fond à gauche
execute positioned -724.5 58.00 162.5 run function tdh:donjon/spawn/squelette_archer_tier1

# Au-dessus de la porte d'entrée
execute positioned -737.5 59.0 170.5 run function tdh:donjon/spawn/squelette_archer_tier2

# À gauche et à droite de l'escalier de sortie
execute positioned -719.5 54.0 165.5 run function tdh:donjon/spawn/squelette_epeiste_tier1
execute positioned -719.5 54.0 175.5 run function tdh:donjon/spawn/squelette_epeiste_tier1

# Dans la salle au fond à gauche
execute positioned -721.5 57.00 160.5 run function tdh:donjon/spawn/squelette_epeiste_tier2
execute positioned -719.5 57.00 160.5 run function tdh:donjon/spawn/squelette_epeiste_tier1

# Au niveau du muret, à droite
execute positioned -723.5 57.00 177.0 run function tdh:donjon/spawn/squelette_archer_tier1


# On ajoute à tous le tag IllysiaSalle1 (condition d'ouverture de la porte = plus aucune créature n'a ce tag / condition de défaite : plus aucun joueur n'a ce tag)
execute positioned -738.5 53.0 159.5 run tag @e[dx=22,dy=8,dz=20,tag=DonjonIllysia,type=skeleton] add IllysiaSalle1
execute positioned -738.5 53.0 159.5 run tag @a[dx=22,dy=8,dz=20,gamemode=!spectator] add IllysiaSalle1

# On ferme la porte
setblock -739 52 170 redstone_torch

# On ouvre la petite grille
fill -736 55 170 -736 54 170 air

# On enregistre le statut actuel du jeu (2=en cours)
scoreboard players set #IllysiaSalle1 gameStatus 2

# On enregistre les autres variables scoreboard (celles des joueurs)
execute at @a[tag=IllysiaSalle1] run scoreboard players set @p nombreDeces 0
execute at @a[tag=IllysiaSalle1] run scoreboard players set @p nombreVictimes 0
execute at @a[tag=IllysiaSalle1] run scoreboard players set @p nombreVictimesH 0

# On affiche les bossbars
bossbar set illysia:salle1_joueurs players @a[tag=IllysiaSalle1]
bossbar set illysia:salle1_ennemis players @a[tag=IllysiaSalle1]

# On démarre la boucle principale
schedule function tdh:donjon/illysia/salle1/loop 1s


# On ouvre la porte de sortie
setblock -717 47 170 stone

# On affiche le message de réussite à tous les joueurs
tellraw @a[tag=IllysiaSalle1] [{"text":"Bravo, ","color":"gold"},{"selector":"@a[tag=IllysiaSalle1]","color":"yellow"},{"text":" ! Vous avez triomphé. La porte au fond de la salle vient de s'ouvrir ; vous pouvez vous rassembler dans la salle suivante pour prendre un peu de repos.","color":"gold"}]

# On lance le loop de victoire (pour attendre que tout le monde soit sorti avant de rouvrir la salle à d'autres)
schedule function tdh:donjon/illysia/salle1/victoire_loop 1s

# On définit le statut du jeu (3=attente fin de partie)
scoreboard players set #IllysiaSalle1 gameStatus 3
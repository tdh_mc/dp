

# On ferme la porte de sortie
setblock -717 47 170 redstone_torch

# On ouvre la porte d'entrée
setblock -739 52 170 stone

# On ferme la petite grille
fill -736 55 170 -736 54 170 iron_bars

# On ferme la porte de la petite salle
setblock -720 57 164 spruce_door[facing=south,half=lower,hinge=left,open=false,powered=false]
setblock -720 58 164 spruce_door[facing=south,half=upper,hinge=left,open=false,powered=false]

# On tue tous les monstres ayant le tag IllysiaSalle1
kill @e[tag=IllysiaSalle1,type=!player]

# On masque les bossbars
bossbar set illysia:salle1_joueurs players
bossbar set illysia:salle1_ennemis players

# On réinitialise le statut du jeu
scoreboard players set #IllysiaSalle1 gameStatus 0

# On réinitialise les loot tables
data merge block -721 57 160 {LootTable:"tdh:donjon/chest/coffre_equipement_random"}
data merge block -723 58 163 {LootTable:"tdh:donjon/chest/tonneau_poisson"}
data merge block -723 57 163 {LootTable:"tdh:donjon/chest/tonneau_charbon"}
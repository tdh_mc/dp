

# On vérifie que tous les joueurs sont toujours dans la salle, sinon on leur retire le tag
execute positioned -738.5 49.0 159.5 run tag @a[tag=IllysiaSalle1,dx=22,dy=12,dz=20] add IllysiaSalle1Temp
tag @a[tag=IllysiaSalle1,tag=!IllysiaSalle1Temp] remove IllysiaSalle1
tag @a[tag=IllysiaSalle1Temp] remove IllysiaSalle1Temp

# On compte le nombre de joueurs
scoreboard players set #IllysiaSalle1 nombreJoueurs 0
execute at @a[tag=IllysiaSalle1] run scoreboard players add #IllysiaSalle1 nombreJoueurs 1

# S'il n'y a plus de joueurs dans la salle, on fait un reset
execute if score #IllysiaSalle1 nombreJoueurs matches 0 run function tdh:donjon/illysia/salle1/reset

# On schedule la fonction à nouveau s'il reste des joueurs
execute if score #IllysiaSalle1 nombreJoueurs matches 1.. run schedule function tdh:donjon/illysia/salle1/victoire_loop 1s
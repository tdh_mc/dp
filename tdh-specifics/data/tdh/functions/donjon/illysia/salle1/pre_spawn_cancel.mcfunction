# On affiche un message aux joueurs présents dans l'avant-salle
execute positioned -745.5 54.0 167.5 run tellraw @a[dx=6,dy=4,dz=6] [{"text":"Ahlala, quelle misère! Vous êtes vraiment des ","color":"gold"},{"text":"mauviettes","bold":"true","color":"yellow"},{"text":"...","color":"gold"}]

# On note dans le scoreboard que la partie est finie
scoreboard players set #IllysiaSalle1 remainingTicks 0
scoreboard players set #IllysiaSalle1 gameStatus 0
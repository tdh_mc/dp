
# On définit le temps restant avant de commencer (10 secondes)
execute unless score #IllysiaSalle1 gameStatus matches 1.. run scoreboard players set #IllysiaSalle1 remainingTicks 10

# On affiche un message aux joueurs présents dans l'avant-salle
execute unless score #IllysiaSalle1 gameStatus matches 1.. positioned -745.5 54.0 167.5 run tellraw @a[dx=6,dy=4,dz=6] [{"text":"Si vous souhaitez vous joindre à l'aventurier ","color":"gold"},{"selector":"@r[x=-737.5,y=55.0,z=170.5,distance=..2]","bold":"true","color":"yellow"},{"text":" et combattre à ses côtés, rejoignez-le dans le couloir.","color":"gold"},{"text":"\nLa porte se fermera dans ","color":"gray"},{"score":{"name":"#IllysiaSalle1","objective":"remainingTicks"},"color":"yellow","bold":"true"},{"text":" secondes.","color":"gray"}]

# On affiche un message aux joueurs présents dans le couloir
execute unless score #IllysiaSalle1 gameStatus matches 1.. positioned -738.5 54.0 169.5 run tellraw @a[dx=2,dy=2,dz=2] [{"text":"Les choses sérieuses commencent, ","color":"gold"},{"selector":"@a[dx=2,dy=2,dz=2]","color":"yellow"},{"text":". Vous avez des créatures à combattre.","color":"gold"},{"text":"\nSi vous restez dans ce couloir, la porte d'entrée se refermera dans ","color":"gray"},{"score":{"name":"#IllysiaSalle1","objective":"remainingTicks"},"color":"yellow","bold":"true"},{"text":" secondes.","color":"gray"}]

# On schedule la fonction qui indique le temps restant et décrémente
execute unless score #IllysiaSalle1 gameStatus matches 1.. run schedule function tdh:donjon/illysia/salle1/pre_spawn_loop 1s

# On note dans le scoreboard qu'une partie est en préparation (status=1)
execute unless score #IllysiaSalle1 gameStatus matches 1.. run scoreboard players set #IllysiaSalle1 gameStatus 1
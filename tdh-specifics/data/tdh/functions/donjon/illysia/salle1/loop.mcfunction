

# On vérifie que tous les joueurs sont toujours dans la salle, sinon on leur retire le tag
execute positioned -738.5 49.0 159.5 run tag @a[tag=IllysiaSalle1,dx=22,dy=12,dz=20] add IllysiaSalle1Temp
tag @a[tag=IllysiaSalle1,tag=!IllysiaSalle1Temp] remove IllysiaSalle1
tag @a[tag=IllysiaSalle1Temp] remove IllysiaSalle1Temp

# On compte le nombre de joueurs, et le nombre d'ennemis
scoreboard players set #IllysiaSalle1 nombreEnnemis 0
execute as @e[tag=IllysiaSalle1,type=!player] run scoreboard players add #IllysiaSalle1 nombreEnnemis 1
scoreboard players set #IllysiaSalle1 nombreJoueurs 0
execute at @a[tag=IllysiaSalle1] run scoreboard players add #IllysiaSalle1 nombreJoueurs 1

# On met à jour la visibilité des boss bars, ainsi que leur valeur
bossbar set illysia:salle1_joueurs players @a[tag=IllysiaSalle1]
bossbar set illysia:salle1_ennemis players @a[tag=IllysiaSalle1]
execute store result bossbar illysia:salle1_ennemis value run scoreboard players get #IllysiaSalle1 nombreEnnemis
execute store result bossbar illysia:salle1_joueurs value run scoreboard players get #IllysiaSalle1 nombreJoueurs

# On schedule la fonction à nouveau s'il reste des joueurs et des ennemis
execute if score #IllysiaSalle1 nombreEnnemis matches 1.. if score #IllysiaSalle1 nombreJoueurs matches 1.. run schedule function tdh:donjon/illysia/salle1/loop 1s

# S'il n'y a plus d'ennemis et qu'il reste des joueurs, c'est la victoire
execute if score #IllysiaSalle1 nombreEnnemis matches 0 if score #IllysiaSalle1 nombreJoueurs matches 1.. run function tdh:donjon/illysia/salle1/victoire

# S'il n'y a plus de joueurs, c'est la défaite
execute if score #IllysiaSalle1 nombreJoueurs matches 0 run function tdh:donjon/illysia/salle1/defaite
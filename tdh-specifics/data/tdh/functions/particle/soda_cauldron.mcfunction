# Cette fonction sera tellement plus simple avec un putain de /schedule [1.14]
# Tant qu'on n'y est pas, cette fonction n'est pas fonctionnelle si /timeflow est stop

# Calcul des modulos
#scoreboard players operation particleSystem cauldronCycle = #temps currentTick

# CauldronCycle est un modulo 400, c'est le temps que met un loop complet de l'animation
#scoreboard players operation particleSystem cauldronCycle %= fakePlayer number400

# On assigne la valeur du cycle entier à chacun des sous-ensembles, pour pouvoir timer par rapport au cycle et pas dans l'absolu
#scoreboard players operation particleSystem cauldronSmoke = particleSystem cauldronCycle
#scoreboard players operation particleSystem cauldronColours = particleSystem cauldronCycle

# CauldronSmoke est un modulo 45, c'est le temps qui passe entre 2 émanations de fumée
#scoreboard players operation particleSystem cauldronSmoke %= fakePlayer number45

# CauldronColours est un modulo 185, c'est le temps qui passe entre 2 explosions de couleurs
#scoreboard players operation particleSystem cauldronColours %= fakePlayer number185


# Fumée
execute if score currentTick modulo45 matches 10..30 run particle minecraft:large_smoke ~ ~2.5 ~ .01 .1 .01 .01 1 normal @a[distance=..15]

# Explosion de couleurs qui s'élèvent rapidement au-dessus du chaudron
execute if score currentTick modulo185 matches 55 run particle minecraft:entity_effect ~ ~2.8 ~ .01 .1 .01 1 50 normal @a[distance=..15]

# Poof, magic
execute if score currentTick modulo400 matches 0 run particle minecraft:poof ~ ~2.2 ~ .01 .4 .01 0.1 50 normal @a[distance=..15]
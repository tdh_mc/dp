execute store result score @s posX run data get entity @e[type=!item_frame,sort=random,limit=1] Pos[0]
scoreboard players operation @s posX %= fakePlayer number100

execute if score @s posX matches 0..9 run particle minecraft:campfire_signal_smoke ~0.19 ~0.5 ~0.17 0 .66 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 10..19 run particle minecraft:campfire_signal_smoke ~-0.13 ~0.5 ~0.41 0 .72 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 20..29 run particle minecraft:campfire_signal_smoke ~0.25 ~0.5 ~-0.05 0 .61 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 30..39 run particle minecraft:campfire_signal_smoke ~-0.21 ~0.5 ~-0.17 0 .63 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 40..49 run particle minecraft:campfire_signal_smoke ~-0.14 ~0.5 ~0.03 0 .80 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 50..59 run particle minecraft:campfire_signal_smoke ~0.19 ~0.5 ~0.24 0 .54 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 60..69 run particle minecraft:campfire_signal_smoke ~-0.15 ~0.5 ~0.12 0 .77 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 70..79 run particle minecraft:campfire_signal_smoke ~0.25 ~0.5 ~-0.25 0 .68 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 80..89 run particle minecraft:campfire_signal_smoke ~0.01 ~0.5 ~0.19 0 .73 0 0.05 0 normal @a[distance=..150]
execute if score @s posX matches 90..99 run particle minecraft:campfire_signal_smoke ~-0.11 ~0.5 ~0.22 0 0.59 0 0.05 0 normal @a[distance=..150]
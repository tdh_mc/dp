# Cette fonction sera tellement plus simple avec un putain de /schedule [1.14]
# Tant qu'on n'y est pas, cette fonction n'est pas fonctionnelle si /timeflow est stop


# Fumée
execute if score currentTick modulo45 matches 0..35 run particle minecraft:effect ~ ~3.9 ~ .1 .1 .1 .01 1 normal @a[distance=..30]
execute if score currentTick modulo45 matches 0..5 run particle minecraft:effect ~1 ~3.7 ~1 .01 .1 .01 .01 2 normal @a[distance=..30]
execute if score currentTick modulo45 matches 10..15 run particle minecraft:effect ~1 ~3.7 ~-1 .01 .1 .01 .01 2 normal @a[distance=..30]
execute if score currentTick modulo45 matches 25..30 run particle minecraft:effect ~-1 ~3.7 ~1 .02 .1 .01 .01 2 normal @a[distance=..30]
execute if score currentTick modulo45 matches 25..30 run particle minecraft:effect ~-1 ~3.7 ~-1 .01 .1 .02 .01 2 normal @a[distance=..30]

# Explosion de couleurs qui s'élèvent rapidement au-dessus du chaudron
execute if score currentTick modulo45 matches 0..45 unless score currentTick modulo45 matches 5..30 run particle minecraft:smoke ~ ~4 ~ .05 .1 .05 .1 4 normal @a[distance=..30]

# On applique l'effet Régénération aux entités proches
execute at @e[distance=..4.8] if score currentTick modulo45 matches 0 run effect give @e[sort=nearest,limit=1] minecraft:regeneration 5 1
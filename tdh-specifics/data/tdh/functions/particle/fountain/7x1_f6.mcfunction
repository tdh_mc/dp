execute at @e[tag=FontaineAnim7x1,tag=Sud] run particle rain ~ ~ ~2.5 0.3 3.25 0.3 0.5 540
execute at @e[tag=FontaineAnim7x1,tag=Nord] run particle rain ~ ~ ~-2.5 0.3 3.25 0.3 0.5 540
execute at @e[tag=FontaineAnim7x1,tag=Est] run particle rain ~2.5 ~ ~ 0.3 3.25 0.3 0.5 540
execute at @e[tag=FontaineAnim7x1,tag=Ouest] run particle rain ~-2.5 ~ ~ 0.3 3.25 0.3 0.5 540
schedule function tdh:particle/fountain/7x1_f7 3t
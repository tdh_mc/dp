execute at @e[tag=FontaineAnim7x1,tag=Sud] run particle rain ~ ~ ~1 0.2 2.5 0.2 0.5 300
execute at @e[tag=FontaineAnim7x1,tag=Nord] run particle rain ~ ~ ~-1 0.2 2.5 0.2 0.5 300
execute at @e[tag=FontaineAnim7x1,tag=Est] run particle rain ~1 ~ ~ 0.2 2.5 0.2 0.5 300
execute at @e[tag=FontaineAnim7x1,tag=Ouest] run particle rain ~-1 ~ ~ 0.2 2.5 0.2 0.5 300
schedule function tdh:particle/fountain/7x1_f4 3t
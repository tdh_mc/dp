execute at @e[tag=FontaineAnim7x1,tag=Sud] run particle rain ~ ~ ~5.5 0.15 1.75 0.15 0.5 450
execute at @e[tag=FontaineAnim7x1,tag=Nord] run particle rain ~ ~ ~-5.5 0.15 1.75 0.15 0.5 450
execute at @e[tag=FontaineAnim7x1,tag=Est] run particle rain ~5.5 ~ ~ 0.15 1.75 0.15 0.5 450
execute at @e[tag=FontaineAnim7x1,tag=Ouest] run particle rain ~-5.5 ~ ~ 0.15 1.75 0.15 0.5 450
schedule function tdh:particle/fountain/7x1_f13 3t
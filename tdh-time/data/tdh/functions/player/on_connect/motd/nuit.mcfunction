# On enregistre l'heure de lever du soleil
scoreboard players operation #store currentHour = #temps leverSoleil
function tdh:store/time

# On l'affiche ensuite
tellraw @p [{"text":"-","color":"gold"},{"text":" Il fait nuit ; le soleil se lèvera à "},{"storage":"tdh:store","nbt":"time","interpret":"true","color":"yellow"},{"text":"."}]
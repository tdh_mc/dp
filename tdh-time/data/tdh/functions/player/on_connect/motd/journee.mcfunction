# On enregistre l'heure de coucher du soleil
scoreboard players operation #store currentHour = #temps coucherSoleil
function tdh:store/time

# On l'affiche ensuite
tellraw @p [{"text":"-","color":"gold"},{"text":" Il fait jour ; le soleil se couchera à "},{"storage":"tdh:store","nbt":"time","interpret":"true","color":"yellow"},{"text":"."}]
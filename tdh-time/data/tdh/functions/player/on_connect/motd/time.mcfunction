# On donne la date et l'heure actuelle.
execute if score #temps currentMinute matches 10.. run tellraw @p [{"text":"-","color":"gold"},{"text":" Nous sommes le ","color":"gold"},{"storage":"tdh:datetime","nbt":"date.jour_semaine.min","color":"yellow"},{"text":" "},{"nbt":"date.jour","storage":"tdh:datetime","color":"yellow"},{"text":" "},{"storage":"tdh:datetime","nbt":"date.mois.min","color":"yellow"},{"text":" "},{"score":{"name":"#temps","objective":"dateAnnee"},"color":"yellow"},{"text":", et il est ","color":"gold"},{"score":{"name":"#temps","objective":"currentHour"},"color":"yellow"},{"text":"h","color":"yellow"},{"score":{"name":"#temps","objective":"currentMinute"},"color":"yellow"},{"text":" !","color":"gold"}]
execute if score #temps currentMinute matches ..9 run tellraw @p [{"text":"-","color":"gold"},{"text":" Nous sommes le ","color":"gold"},{"storage":"tdh:datetime","nbt":"date.jour_semaine.min","color":"yellow"},{"text":" "},{"nbt":"date.jour","storage":"tdh:datetime","color":"yellow"},{"text":" "},{"storage":"tdh:datetime","nbt":"date.mois.min","color":"yellow"},{"text":" "},{"score":{"name":"#temps","objective":"dateAnnee"},"color":"yellow"},{"text":", et il est ","color":"gold"},{"score":{"name":"#temps","objective":"currentHour"},"color":"yellow"},{"text":"h0","color":"yellow"},{"score":{"name":"#temps","objective":"currentMinute"},"color":"yellow"},{"text":" !"}]

# On donne l'heure de lever ou de coucher du soleil
scoreboard players operation #store currentHour = #temps leverSoleil
function tdh:store/time
execute if score #temps timeOfDay matches 1 run function tdh:player/on_connect/motd/journee
execute if score #temps timeOfDay matches 2 run function tdh:player/on_connect/motd/crepuscule
execute if score #temps timeOfDay matches 3 run function tdh:player/on_connect/motd/nuit
execute if score #temps timeOfDay matches 4 run function tdh:player/on_connect/motd/aube
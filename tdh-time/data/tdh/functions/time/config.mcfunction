# Configurateur du temps/calendrier
# Permet de choisir les paramètres d'écoulement du temps et des saisons

# On démarre la config et se donne donc le tag Config et ConfigLigne
tag @s add Config
tag @s add ConfigTime

# On active l'objectif de configuration à trigger (9999 = Choix d'un paramètre à modifier)
scoreboard players set @s configID 9999
scoreboard players set @s config 0
scoreboard players enable @s config

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-TIME","color":"gold"}]
tellraw @s [{"text":"Voici la configuration actuelle de ","color":"gray"},{"text":"tdh-time","bold":"true"},{"text":" :\n——————————————————————————"}]

# -- Options des journées --
# Écoulement du temps (15xxx)
execute unless score #temps on matches 1 run tellraw @s [{"text":"Le temps est ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour reprendre l'écoulement du temps."},"clickEvent":{"action":"run_command","value":"/trigger config set 15010"}},{"text":"en pause","color":"gold"},{"text":"."}]
execute if score #temps on matches 1 run tellraw @s [{"text":"Le temps ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour mettre l'écoulement du temps en pause."},"clickEvent":{"action":"run_command","value":"/trigger config set 15011"}},{"text":"s'écoule normalement","color":"gold"},{"text":"."}]

# Durée d'une journée (10000) - Par défaut 48'000 (40mn)
scoreboard players operation #store currentHour = #temps fullDayTicks
function tdh:store/realtime
tellraw @s [{"text":"Chaque ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée des journées."},"clickEvent":{"action":"run_command","value":"/trigger config set 10000"}},{"text":"journée","color":"yellow"},{"text":" dure "},{"score":{"name":"#temps","objective":"fullDayTicks"},"color":"gold"},{"text":"t","color":"gold"},{"text":" ("},{"storage":"tdh:store","nbt":"realtime","interpret":"true"},{"text":")."}]

# Nombre d'heures par journée (11000) - Par défaut 24
tellraw @s [{"text":"Chaque journée est divisée en ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le nombre d'heures par jour."},"clickEvent":{"action":"run_command","value":"/trigger config set 11000"}},{"score":{"name":"#temps","objective":"hoursPerDay"},"color":"gold"},{"text":" heures."}]

# Nombre de minutes par heure (12000) - Par défaut 60
tellraw @s [{"text":"Chaque heure est divisée en ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le nombre de minutes par heure."},"clickEvent":{"action":"run_command","value":"/trigger config set 12000"}},{"score":{"name":"#temps","objective":"minutesPerHour"},"color":"gold"},{"text":" minutes."}]

tellraw @s [{"text":"——————————————————————————","color":"gray"}]

# Répartition aux équinoxes (20xxx) - Par défaut 20'400/20'400/3'600/3'600
# Jour = 20000, Nuit = 20100, Crépuscules = 20200
scoreboard players operation #store currentHour = #equinoxe dayTicks
scoreboard players operation #store2 currentHour = #equinoxe nightTicks
function tdh:store/realtime
function tdh:store/realtime2
tellraw @s [{"text":"Aux ","color":"gray"},{"text":"équinoxes","color":"yellow"},{"text":", le jour dure "},{"score":{"name":"#equinoxe","objective":"dayTicks"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée du jour aux équinoxes."},"clickEvent":{"action":"run_command","value":"/trigger config set 20000"},"extra":[{"text":"t"},{"text":" (","color":"gray"},{"storage":"tdh:store","nbt":"realtime","interpret":"true","color":"gray"},{"text":")","color":"gray"}]},{"text":", et la nuit dure "},{"score":{"name":"#equinoxe","objective":"nightTicks"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée de la nuit aux équinoxes."},"clickEvent":{"action":"run_command","value":"/trigger config set 20100"},"extra":[{"text":"t"},{"text":" (","color":"gray"},{"storage":"tdh:store","nbt":"realtime2","interpret":"true","color":"gray"},{"text":")","color":"gray"}]},{"text":"."}]
scoreboard players operation #store currentHour = #equinoxe sunsetTicks
function tdh:store/realtime
tellraw @s [{"text":"Aux ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée du lever/coucher de soleil aux équinoxes."},"clickEvent":{"action":"run_command","value":"/trigger config set 20200"}},{"text":"équinoxes","color":"yellow"},{"text":", le lever/coucher du soleil dure "},{"score":{"name":"#equinoxe","objective":"sunsetTicks"},"color":"gold"},{"text":"t","color":"gold"},{"text":" ("},{"storage":"tdh:store","nbt":"realtime","interpret":"true"},{"text":")."}]

# Répartition au solstice d'été (21xxx) - Par défaut 32'000/12'000/2'000/2'000
# Jour = 21000, Nuit = 21100, Crépuscules = 21200
scoreboard players operation #store currentHour = #solsticeEte dayTicks
scoreboard players operation #store2 currentHour = #solsticeEte nightTicks
function tdh:store/realtime
function tdh:store/realtime2
tellraw @s [{"text":"Au ","color":"gray"},{"text":"solstice d'été","color":"yellow"},{"text":", le jour dure "},{"score":{"name":"#solsticeEte","objective":"dayTicks"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée du jour au solstice d'été."},"clickEvent":{"action":"run_command","value":"/trigger config set 21000"},"extra":[{"text":"t"},{"text":" (","color":"gray"},{"storage":"tdh:store","nbt":"realtime","interpret":"true","color":"gray"},{"text":")","color":"gray"}]},{"text":", et la nuit dure "},{"score":{"name":"#solsticeEte","objective":"nightTicks"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée de la nuit au solstice d'été."},"clickEvent":{"action":"run_command","value":"/trigger config set 21100"},"extra":[{"text":"t"},{"text":" (","color":"gray"},{"storage":"tdh:store","nbt":"realtime2","interpret":"true","color":"gray"},{"text":")","color":"gray"}]},{"text":"."}]
scoreboard players operation #store currentHour = #solsticeEte sunsetTicks
function tdh:store/realtime
tellraw @s [{"text":"Au ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée du lever/coucher de soleil au solstice d'été."},"clickEvent":{"action":"run_command","value":"/trigger config set 21200"}},{"text":"solstice d'été","color":"yellow"},{"text":", le lever/coucher du soleil dure "},{"score":{"name":"#solsticeEte","objective":"sunsetTicks"},"color":"gold"},{"text":"t","color":"gold"},{"text":" ("},{"storage":"tdh:store","nbt":"realtime","interpret":"true"},{"text":")."}]

# Répartition au solstice d'hiver (22xxx) - Par défaut 12'000/24'800/5'600/5'600
# Jour = 22000, Nuit = 22100, Crépuscules = 22200
scoreboard players operation #store currentHour = #solsticeHiver dayTicks
scoreboard players operation #store2 currentHour = #solsticeHiver nightTicks
function tdh:store/realtime
function tdh:store/realtime2
tellraw @s [{"text":"Au ","color":"gray"},{"text":"solstice d'hiver","color":"yellow"},{"text":", le jour dure "},{"score":{"name":"#solsticeHiver","objective":"dayTicks"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée du jour au solstice d'hiver."},"clickEvent":{"action":"run_command","value":"/trigger config set 22000"},"extra":[{"text":"t"},{"text":" (","color":"gray"},{"storage":"tdh:store","nbt":"realtime","interpret":"true","color":"gray"},{"text":")","color":"gray"}]},{"text":", et la nuit dure "},{"score":{"name":"#solsticeHiver","objective":"nightTicks"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée de la nuit au solstice d'hiver."},"clickEvent":{"action":"run_command","value":"/trigger config set 22100"},"extra":[{"text":"t"},{"text":" (","color":"gray"},{"storage":"tdh:store","nbt":"realtime2","interpret":"true","color":"gray"},{"text":")","color":"gray"}]},{"text":"."}]
scoreboard players operation #store currentHour = #solsticeHiver sunsetTicks
function tdh:store/realtime
tellraw @s [{"text":"Au ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier la durée du lever/coucher de soleil au solstice d'hiver."},"clickEvent":{"action":"run_command","value":"/trigger config set 22200"}},{"text":"solstice d'hiver","color":"yellow"},{"text":", le lever/coucher du soleil dure "},{"score":{"name":"#solsticeHiver","objective":"sunsetTicks"},"color":"gold"},{"text":"t","color":"gold"},{"text":" ("},{"storage":"tdh:store","nbt":"realtime","interpret":"true"},{"text":")."}]

tellraw @s [{"text":"——————————————————————————","color":"gray"}]

# Date de l'équinoxe de printemps (30xxx) - Par défaut 21-03
scoreboard players operation #store dateMois = #equinoxePrintemps dateMois
function tdh:store/mois
tellraw @s [{"text":"L'","color":"gray"},{"text":"équinoxe de printemps","color":"yellow"},{"text":" tombe le "},{"score":{"name":"#equinoxePrintemps","objective":"dateJour"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le jour de l'équinoxe de printemps."},"clickEvent":{"action":"run_command","value":"/trigger config set 30000"}},{"text":" "},{"storage":"tdh:store","nbt":"mois.min","color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le mois de l'équinoxe de printemps."},"clickEvent":{"action":"run_command","value":"/trigger config set 30100"}},{"text":" ("},{"score":{"name":"#equinoxePrintemps","objective":"dateJour"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le jour de l'équinoxe de printemps."},"clickEvent":{"action":"run_command","value":"/trigger config set 30000"}},{"text":"/"},{"score":{"name":"#equinoxePrintemps","objective":"dateMois"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le mois de l'équinoxe de printemps."},"clickEvent":{"action":"run_command","value":"/trigger config set 30100"}},{"text":")."}]
# Date du solstice d'été (31xxx) - Par défaut 21-06
scoreboard players operation #store dateMois = #solsticeEte dateMois
function tdh:store/mois
tellraw @s [{"text":"Le ","color":"gray"},{"text":"solstice d'été","color":"yellow"},{"text":" tombe le "},{"score":{"name":"#solsticeEte","objective":"dateJour"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le jour du solstice d'été."},"clickEvent":{"action":"run_command","value":"/trigger config set 31000"}},{"text":" "},{"storage":"tdh:store","nbt":"mois.min","color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le mois du solstice d'été."},"clickEvent":{"action":"run_command","value":"/trigger config set 31100"}},{"text":" ("},{"score":{"name":"#solsticeEte","objective":"dateJour"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le jour du solstice d'été."},"clickEvent":{"action":"run_command","value":"/trigger config set 31000"}},{"text":"/"},{"score":{"name":"#solsticeEte","objective":"dateMois"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le mois du solstice d'été."},"clickEvent":{"action":"run_command","value":"/trigger config set 31100"}},{"text":")."}]
# Date de l'équinoxe d'automne (32xxx) - Par défaut 21-09
scoreboard players operation #store dateMois = #equinoxeAutomne dateMois
function tdh:store/mois
tellraw @s [{"text":"L'","color":"gray"},{"text":"équinoxe d'automne","color":"yellow"},{"text":" tombe le "},{"score":{"name":"#equinoxeAutomne","objective":"dateJour"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le jour de l'équinoxe d'automne."},"clickEvent":{"action":"run_command","value":"/trigger config set 32000"}},{"text":" "},{"storage":"tdh:store","nbt":"mois.min","color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le mois de l'équinoxe d'automne."},"clickEvent":{"action":"run_command","value":"/trigger config set 32100"}},{"text":" ("},{"score":{"name":"#equinoxeAutomne","objective":"dateJour"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le jour de l'équinoxe d'automne."},"clickEvent":{"action":"run_command","value":"/trigger config set 32000"}},{"text":"/"},{"score":{"name":"#equinoxeAutomne","objective":"dateMois"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le mois de l'équinoxe d'automne."},"clickEvent":{"action":"run_command","value":"/trigger config set 32100"}},{"text":")."}]
# Date du solstice d'hiver (33xxx) - Par défaut 21-12
scoreboard players operation #store dateMois = #solsticeHiver dateMois
function tdh:store/mois
tellraw @s [{"text":"Le ","color":"gray"},{"text":"solstice d'hiver","color":"yellow"},{"text":" tombe le "},{"score":{"name":"#solsticeHiver","objective":"dateJour"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le jour du solstice d'hiver."},"clickEvent":{"action":"run_command","value":"/trigger config set 33000"}},{"text":" "},{"storage":"tdh:store","nbt":"mois.min","color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le mois du solstice d'hiver."},"clickEvent":{"action":"run_command","value":"/trigger config set 33100"}},{"text":" ("},{"score":{"name":"#solsticeHiver","objective":"dateJour"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le jour du solstice d'hiver."},"clickEvent":{"action":"run_command","value":"/trigger config set 33000"}},{"text":"/"},{"score":{"name":"#solsticeHiver","objective":"dateMois"},"color":"gold","hoverEvent":{"action":"show_text","contents":"Cliquer pour modifier le mois du solstice d'hiver."},"clickEvent":{"action":"run_command","value":"/trigger config set 33100"}},{"text":")."}]

tellraw @s [{"text":"——————————————————————————","color":"gray"}]

# Mois de l'année, durée de l'année (40xxx) (la config se passe dans un sous-menu)
tellraw @s [{"text":"Il y a ","color":"gray"},{"score":{"name":"#temps","objective":"dateMoisMax"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"[Avancé]","color":"red"},{"text":" Modification de la durée et du nom des mois.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 40000"}},{"text":" mois","color":"yellow"},{"text":" dans une année."}]
tellraw @s [{"text":"Une ","color":"gray"},{"text":"année non-bissextile","color":"yellow"},{"text":" dure "},{"score":{"name":"#temps","objective":"jourAnneeMax"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"[Avancé]","color":"red"},{"text":" Modification de la durée et du nom des mois.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 40000"}},{"text":" jours."}]
tellraw @s [{"text":"Une ","color":"gray"},{"text":"année bissextile","color":"yellow"},{"text":" dure "},{"score":{"name":"#anneeBissextile","objective":"jourAnneeMax"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"[Avancé]","color":"red"},{"text":" Modification de la durée et du nom des mois.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 40000"}},{"text":" jours."}]

tellraw @s [{"text":"——————————————————————————","color":"gray"}]

# Durée de la semaine (50xxx) (la config se passe dans un sous-menu)
tellraw @s [{"text":"Il y a ","color":"gray"},{"score":{"name":"#temps","objective":"jourSemaineMax"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"[Avancé]","color":"red"},{"text":" Modification de la durée de la semaine et du nom de ses jours.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 50000"}},{"text":" jours","color":"yellow"},{"text":" dans une semaine."}]

# Changer la date (60xxx)
tellraw @s [{"text":"Nous sommes le ","color":"gray"},{"nbt":"date.jour_semaine.maj","storage":"tdh:datetime","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Modification du jour de la semaine.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 60400"}},{"text":" "},{"nbt":"date.jour","storage":"tdh:datetime","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Modification du jour.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 60000"}},{"text":" "},{"nbt":"date.mois.min","storage":"tdh:datetime","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Modification du mois.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 60100"}},{"text":" "},{"score":{"name":"#temps","objective":"dateAnnee"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Modification de l'année.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 60200"}},{"text":"."}]
tellraw @s [{"text":"Vérifier l'intégrité de la date","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Cliquer ici si la date affichée ci-dessus semble incorrecte.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:time/config/date_check"}},{"text":" • ","color":"gray"},{"text":"Avancer d'une journée","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"La date sera avancée d'une journée, et l'heure sera définie au lever du soleil.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:time/config/prochaine_journee"}}]
tellraw @s [{"text":"Réinitialiser l'heure","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"L'heure sera définie au début de la journée (tick 0).","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:time/reset_heure"}},{"text":" • ","color":"gray"},{"text":"Actualiser l'affichage","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Affiche à nouveau ce menu.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:time/config"}}]


tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"- Quitter le configurateur","color":"gray","clickEvent":{"action":"run_command","value":"/trigger config set -1"}}]
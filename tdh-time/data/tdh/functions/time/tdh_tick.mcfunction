# On vérifie s'il y a des joueurs en ligne
# Pas besoin, car on l'a déjà fait dans tdh_test_tick au préalable

# On compte le nombre de joueurs qui dorment
scoreboard players set #temps playersSleeping 0
execute at @a[tag=IsSleeping] run scoreboard players add #temps playersSleeping 1

# On compte aussi les joueurs en spectateur
scoreboard players set #temps playersInSpec 0
execute at @a[gamemode=spectator] run scoreboard players add #temps playersInSpec 1

# On calcule le pourcentage d'accélération du temps en fonction du nombre de joueurs endormis
# speed=0 (temps en pause) // speed=1 (temps normal) // speed>=2 (temps accéléré)
scoreboard players operation #temps vitesse = #temps playersSleeping
scoreboard players operation #temps vitesse += #temps playersInSpec
scoreboard players set #temps temp 10
scoreboard players operation #temps vitesse *= #temps temp
scoreboard players operation #temps vitesse /= #temps playersOnline
scoreboard players operation #temps vitesse *= #temps vitesse
scoreboard players add #temps vitesse 1

# Si aucun joueur n'est endormi, la vitesse reste de 1
execute if score #temps playersSleeping matches 0 run scoreboard players set #temps vitesse 1

# On exécute ensuite la fonction correspondante
execute if score #temps vitesse matches 1 run function tdh:time/tdh_tick/0
execute if score #temps vitesse matches 2.. run function tdh:time/tdh_tick/sleep



# On stocke le nouveau tick Minecraft
execute store result score #temps currentTick run time query daytime
scoreboard players add #temps currentTick 6000
scoreboard players operation #temps currentTick %= #tempsOriginal fullDayTicks

# Pour chaque horaire disposant d'un tag associé, on appelle le tag correspondant
execute if score #temps currentTdhTick >= #2h currentTdhTick if score #temps previousTdhTick < #2h currentTdhTick run function #tdh:time/2h
execute if score #temps currentTdhTick >= #4h currentTdhTick if score #temps previousTdhTick < #4h currentTdhTick run function #tdh:time/4h
execute if score #temps currentTdhTick >= #6h currentTdhTick if score #temps previousTdhTick < #6h currentTdhTick run function #tdh:time/6h
execute if score #temps currentTdhTick >= #8h currentTdhTick if score #temps previousTdhTick < #8h currentTdhTick run function #tdh:time/8h
execute if score #temps currentTdhTick >= #10h currentTdhTick if score #temps previousTdhTick < #10h currentTdhTick run function #tdh:time/10h
execute if score #temps currentTdhTick >= #12h currentTdhTick if score #temps previousTdhTick < #12h currentTdhTick run function #tdh:time/12h
execute if score #temps currentTdhTick >= #14h currentTdhTick if score #temps previousTdhTick < #14h currentTdhTick run function #tdh:time/14h
execute if score #temps currentTdhTick >= #16h currentTdhTick if score #temps previousTdhTick < #16h currentTdhTick run function #tdh:time/16h
execute if score #temps currentTdhTick >= #18h currentTdhTick if score #temps previousTdhTick < #18h currentTdhTick run function #tdh:time/18h
execute if score #temps currentTdhTick >= #20h currentTdhTick if score #temps previousTdhTick < #20h currentTdhTick run function #tdh:time/20h
execute if score #temps currentTdhTick >= #22h currentTdhTick if score #temps previousTdhTick < #22h currentTdhTick run function #tdh:time/22h

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
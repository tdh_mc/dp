# On initialise les mois si on n'a pas déjà de config des mois
execute unless data storage tdh:datetime index.mois[0] run function tdh:time/init/subdivisions/mois

# On initialise les semaines si on n'a pas déjà de config des semaines
execute unless data storage tdh:datetime index.jour_semaine[0] run function tdh:time/init/subdivisions/semaine
# On définit la date à celle de l'équinoxe d'automne
# (début du calendrier astronomique dans le RP TDH)
scoreboard players set #temps dateAnnee 1
scoreboard players operation #temps dateMois = #equinoxeAutomne dateMois
scoreboard players operation #temps dateJour = #equinoxeAutomne dateJour

scoreboard players operation #tempsConv dateJour = #temps dateJour
scoreboard players operation #tempsConv dateMois = #temps dateMois
scoreboard players set #tempsConv anneeBissextile 0
function tdh:time/conv/jour_mois_vers_jourannee
scoreboard players operation #temps jourAnnee = #tempsConv jourAnnee
# Jsp pourquoi j'avais fait ce qui suit mais apriori ce n'est pas utile
#scoreboard players operation #equinoxeAutomne jourAnnee = #tempsConv jourAnnee
#scoreboard players operation #equinoxeAutomne dateAnnee = #temps dateAnnee

# On calcule les horaires de lever/coucher du soleil
function tdh:time/storage/ratios


# On définit ensuite l'heure : le début de la journée
time set 0
scoreboard players set #temps currentTick 6000
scoreboard players operation #temps currentTdhTick = #temps dayBegin

# On calcule l'heure actuelle
function tdh:time/tdh_tick/period
# Pour chaque fête, on initialise les valeurs seulement si elles n'existent pas encore
execute unless score #equinoxePrintemps dateMois matches 1.. run function tdh:time/init/fetes/equinoxe_printemps
execute unless score #solsticeEte dateMois matches 1.. run function tdh:time/init/fetes/solstice_ete
execute unless score #equinoxeAutomne dateMois matches 1.. run function tdh:time/init/fetes/equinoxe_automne
execute unless score #solsticeHiver dateMois matches 1.. run function tdh:time/init/fetes/solstice_hiver
# On calcule le nombre de mois à partir du storage
execute store result score #temps dateMoisMax run data get storage tdh:datetime index.mois

# On calcule le nombre total de jours dans une année
execute store result score #temps jourAnneeMax run data get storage tdh:datetime index.mois[0].jours

execute store result score #temps temp run data get storage tdh:datetime index.mois[1].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[2].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[3].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[4].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[5].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[6].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[7].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[8].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[9].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[10].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[11].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[12].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[13].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[14].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[15].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[16].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[17].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[18].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[19].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[20].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[21].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[22].jours
scoreboard players operation #temps jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[23].jours
scoreboard players operation #temps jourAnneeMax += #temps temp

# On calcule le nombre total de jours dans une année bissextile
execute store result score #anneeBissextile jourAnneeMax run data get storage tdh:datetime index.mois[0].joursBis

execute store result score #temps temp run data get storage tdh:datetime index.mois[1].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[2].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[3].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[4].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[5].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[6].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[7].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[8].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[9].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[10].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[11].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[12].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[13].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[14].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[15].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[16].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[17].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[18].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[19].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[20].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[21].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[22].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp
execute store result score #temps temp run data get storage tdh:datetime index.mois[23].joursBis
scoreboard players operation #anneeBissextile jourAnneeMax += #temps temp

scoreboard players reset #temps temp
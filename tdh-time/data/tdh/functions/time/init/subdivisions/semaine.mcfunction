# On définit les jours de la semaine par défaut
data modify storage tdh:datetime index.jour_semaine set value [{min:"lundi",maj:"Lundi",minS:"lun.",majS:"Lun."},{min:"mardi",maj:"Mardi",minS:"mar.",majS:"Mar."},{min:"mercredi",maj:"Mercredi",minS:"mer.",majS:"Mer."},{min:"jeudi",maj:"Jeudi",minS:"jeu.",majS:"Jeu."},{min:"vendredi",maj:"Vendredi",minS:"ven.",majS:"Ven."},{min:"samedi",maj:"Samedi",minS:"sam.",majS:"Sam."},{min:"dimanche",maj:"Dimanche",minS:"dim.",majS:"Dim."}]

# On calcule le nombre de jours dans une semaine
function tdh:time/init/subdivisions/calcul_semaine
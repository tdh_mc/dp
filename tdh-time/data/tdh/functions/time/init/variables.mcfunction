scoreboard objectives add playersOnline dummy "Joueurs en ligne"
scoreboard objectives add playersSleeping dummy "Joueurs dans un lit"
scoreboard objectives add playersInSpec dummy "Joueurs en spectateur"
scoreboard objectives add duree dummy "Durée"

scoreboard objectives add customTimeflowOn dummy
scoreboard objectives add speedGrassOn dummy
scoreboard objectives add timeWarpOn dummy

scoreboard objectives add currentTdhTick dummy
scoreboard objectives add previousTdhTick dummy

scoreboard objectives add dayBegin dummy
scoreboard objectives add nightBegin dummy
scoreboard objectives add sunsetBegin dummy
scoreboard objectives add sunriseBegin dummy

scoreboard objectives add fullDayTicks dummy
scoreboard objectives add dayTicks dummy
scoreboard objectives add nightTicks dummy
scoreboard objectives add sunsetTicks dummy
scoreboard objectives add sunriseTicks dummy

scoreboard objectives add leverSoleil dummy "Tick de lever du soleil"
scoreboard objectives add coucherSoleil dummy "Tick de coucher du soleil"

scoreboard objectives add idJour dummy "Identifiant du jour"

scoreboard objectives add dateJour dummy
scoreboard objectives add dateMois dummy
scoreboard objectives add dateAnnee dummy
scoreboard objectives add dateJourMax dummy
scoreboard objectives add dateMoisMax dummy
scoreboard objectives add anneeBissextile dummy

scoreboard objectives add jourSemaine dummy
scoreboard objectives add jourSemaineMax dummy
scoreboard objectives add saison dummy
scoreboard objectives add jourAnnee dummy
scoreboard objectives add jourAnneeMax dummy

execute unless score #temps on matches 0..1 run scoreboard players set #temps on 0
gamerule playersSleepingPercentage 150
scoreboard players set #tempsOriginal fullDayTicks 24000
scoreboard players set #tempsOriginal ticksPerHour 1000
scoreboard players set #tempsOriginal dayTicks 12000
scoreboard players set #tempsOriginal nightTicks 8400
scoreboard players set #tempsOriginal sunsetTicks 1800
scoreboard players set #tempsOriginal sunriseTicks 1800
scoreboard players set #tempsOriginal dayBegin 6000
scoreboard players set #tempsOriginal nightBegin 19800
scoreboard players set #tempsOriginal sunsetBegin 18000
scoreboard players set #tempsOriginal sunriseBegin 4200

# En-dehors des variables du temps original (non configurables puisque correspondant à Minecraft vanilla),
# toutes les variables ne sont initialisées que si elles sont en-dessous de leur maximum théorique
execute unless score #solsticeHiver dayTicks matches 12000.. run scoreboard players set #solsticeHiver dayTicks 12000
execute unless score #solsticeHiver nightTicks matches 8400.. run scoreboard players set #solsticeHiver nightTicks 24800
execute unless score #solsticeHiver sunsetTicks matches 1800.. run scoreboard players set #solsticeHiver sunsetTicks 5600
execute unless score #solsticeHiver sunriseTicks matches 1800.. run scoreboard players set #solsticeHiver sunriseTicks 5600

execute unless score #solsticeEte dayTicks matches 12000.. run scoreboard players set #solsticeEte dayTicks 32000
execute unless score #solsticeEte nightTicks matches 8400.. run scoreboard players set #solsticeEte nightTicks 12000
execute unless score #solsticeEte sunsetTicks matches 1800.. run scoreboard players set #solsticeEte sunsetTicks 2000
execute unless score #solsticeEte sunriseTicks matches 1800.. run scoreboard players set #solsticeEte sunriseTicks 2000

execute unless score #equinoxe dayTicks matches 12000.. run scoreboard players set #equinoxe dayTicks 20400
execute unless score #equinoxe nightTicks matches 8400.. run scoreboard players set #equinoxe nightTicks 20400
execute unless score #equinoxe sunsetTicks matches 1800.. run scoreboard players set #equinoxe sunsetTicks 3600
execute unless score #equinoxe sunriseTicks matches 1800.. run scoreboard players set #equinoxe sunriseTicks 3600

execute unless score #temps fullDayTicks matches 24000.. run scoreboard players set #temps fullDayTicks 48000
execute unless score #temps ticksPerHour matches 120.. run scoreboard players set #temps ticksPerHour 2000
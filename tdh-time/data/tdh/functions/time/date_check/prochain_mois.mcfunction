# On lance cette fonction si notre dateJour actuelle est supérieure au maximum du mois lors d'un date_check
# Elle est récursive, on ajoute un mois supplémentaire et on retire le nombre de jours correspondants à chaque itération

scoreboard players operation #temps dateJour -= #temps dateJourMax
scoreboard players add #temps dateMois 1
# Si on a dépassé le max, on passe à l'année suivante
execute if score #temps dateMois > #temps dateMoisMax run function tdh:time/date_check/prochaine_annee
# On récupère les données de ce mois
function tdh:time/storage/mois

# Si on a tjrs trop de jours pour ce nouveau mois, on relance la fonction
execute if score #temps dateJour > #temps dateJourMax run function tdh:time/date_check/prochain_mois
# Passage au jour suivant

# On incrémente jourSemaine, et on revient à Lundi (1) si on a dépassé Dimanche (7)
scoreboard players add #temps jourSemaine 1
execute if score #temps jourSemaine > #temps jourSemaineMax run scoreboard players operation #temps jourSemaine -= #temps jourSemaineMax

# On met à jour le jour de la semaine depuis le storage
# Les semaines jusqu'à 15 jours sont supportées (par défaut 7)
function tdh:time/storage/jour_semaine

# On incrémente l'identifiant du jour
scoreboard players add #temps idJour 1

# On incrémente dateJour et on décrémente l'heure de fullDayTicks
scoreboard players add #temps dateJour 1
scoreboard players add #temps jourAnnee 1
scoreboard players operation #temps currentTdhTick -= #temps fullDayTicks

# Si on a dépassé le maximum de jours dans ce mois, on passe au mois suivant
execute if score #temps dateJour > #temps dateJourMax run function tdh:time/tdh_tick/prochain_mois

# On calcule la durée de la journée et de la nuit pour aujourd'hui
function tdh:time/storage/ratios

# On met à jour le numéro du jour dans le storage
function tdh:time/storage/jour_mois

# On vérifie si on est ou non à une date spécifique
function tdh:time/storage/jour_special

# On fait tous les calculs supplémentaires devant être réalisés au changement de jour
function #tdh:time/0h
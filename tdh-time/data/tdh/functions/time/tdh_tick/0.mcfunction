# On incrémente le tick TDH (48000t/jour)
scoreboard players operation #temps previousTdhTick = #temps currentTdhTick
scoreboard players add #temps currentTdhTick 1

# On fait les calculs d'heure et de période
function tdh:time/tdh_tick/period

# En fonction de la période, on exécute la sous-fonction correspondante pour incrémenter le tick Minecraft
execute if score #temps timeOfDay matches 1 run function tdh:time/tdh_tick/1
execute if score #temps timeOfDay matches 2 run function tdh:time/tdh_tick/2
execute if score #temps timeOfDay matches 3 run function tdh:time/tdh_tick/3
execute if score #temps timeOfDay matches 4 run function tdh:time/tdh_tick/4
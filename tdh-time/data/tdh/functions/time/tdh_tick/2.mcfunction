# Condition pour l'incrémentation de currentTick :
# (nightBegin - previousTdhTick) / (defaultNightBegin - currentTick)
# <= (nightBegin - sunsetBegin) / (defaultNightBegin - defaultSunsetBegin)

# temp, temp2 = nightBegin
# temp3, temp4 = defaultnightBegin
scoreboard players operation #temps temp = #temps nightBegin
scoreboard players operation #temps temp2 = #temps nightBegin
scoreboard players operation #temps temp3 = #tempsOriginal nightBegin
scoreboard players operation #temps temp4 = #tempsOriginal nightBegin

# temp = (nightBegin - previousTdhTick)
# temp2 = (nightBegin - sunsetBegin)
# temp3 = (defaultNightBegin - currentTick)
# temp4 = (defaultNightBegin - defaultSunsetBegin)
scoreboard players operation #temps temp -= #temps previousTdhTick
scoreboard players operation #temps temp2 -= #temps sunsetBegin
scoreboard players operation #temps temp3 -= #temps currentTick
scoreboard players operation #temps temp4 -= #tempsOriginal sunsetBegin

# temp5 = 1'000
# temp = 1'000 * (nightBegin - previousTdhTick) / (defaultNightBegin - currentTick)
# temp2 = 1'000 * (nightBegin - sunsetBegin) / (defaultNightBegin - defaultSunsetBegin)
scoreboard players set #temps temp5 1000
scoreboard players operation #temps temp *= #temps temp5
scoreboard players operation #temps temp /= #temps temp3
scoreboard players operation #temps temp2 *= #temps temp5
scoreboard players operation #temps temp2 /= #temps temp4

# Si temp <= temp2, on incrémente currentTick
execute if score #temps temp <= #temps temp2 run time add 1t

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
scoreboard players reset #temps temp4
# On incrémente dateAnnee et on retire le nombre maximum de mois
scoreboard players add #temps dateAnnee 1
scoreboard players set #temps jourAnnee 1
scoreboard players operation #temps dateMois -= #temps dateMoisMax

# On calcule si l'année est ou non bissextile
function tdh:time/storage/annee
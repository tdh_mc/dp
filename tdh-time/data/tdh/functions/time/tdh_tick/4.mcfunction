# Condition pour l'incrémentation de currentTick :
# (dayBegin - previousTdhTick) / (defaultDayBegin - currentTick)
# <= (dayBegin - sunriseBegin) / (defaultDayBegin - defaultSunriseBegin)

# temp, temp2 = dayBegin
# temp3, temp4 = defaultdayBegin
scoreboard players operation #temps temp = #temps dayBegin
scoreboard players operation #temps temp2 = #temps dayBegin
scoreboard players operation #temps temp3 = #tempsOriginal dayBegin
scoreboard players operation #temps temp4 = #tempsOriginal dayBegin

# temp = (dayBegin - previousTdhTick)
# temp2 = (dayBegin - sunriseBegin)
# temp3 = (defaultDayBegin - currentTick)
# temp4 = (defaultDayBegin - defaultSunriseBegin)
scoreboard players operation #temps temp -= #temps previousTdhTick
scoreboard players operation #temps temp2 -= #temps sunriseBegin
scoreboard players operation #temps temp3 -= #temps currentTick
scoreboard players operation #temps temp4 -= #tempsOriginal sunriseBegin

# temp5 = 1'000
# temp = 1'000 * (dayBegin - previousTdhTick) / (defaultDayBegin - currentTick)
# temp2 = 1'000 * (dayBegin - sunriseBegin) / (defaultDayBegin - defaultSunriseBegin)
scoreboard players set #temps temp5 1000
scoreboard players operation #temps temp *= #temps temp5
scoreboard players operation #temps temp /= #temps temp3
scoreboard players operation #temps temp2 *= #temps temp5
scoreboard players operation #temps temp2 /= #temps temp4

# Si temp <= temp2, on incrémente le temps
execute if score #temps temp <= #temps temp2 run time add 1t

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
scoreboard players reset #temps temp4
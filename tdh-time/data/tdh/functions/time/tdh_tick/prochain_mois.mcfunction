# Passage au mois suivant

# On incrémente le mois et reset le jour
scoreboard players add #temps dateMois 1
scoreboard players operation #temps dateJour -= #temps dateJourMax

# Si on a dépassé le nombre maximum de mois dans l'année, on passe à l'année suivante
execute if score #temps dateMois > #temps dateMoisMax run function tdh:time/tdh_tick/prochaine_annee

# On copie le storage du bon mois dans le mois actuel
function tdh:time/storage/mois
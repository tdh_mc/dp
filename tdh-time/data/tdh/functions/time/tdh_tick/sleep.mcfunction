# On incrémente le tick TDH (48000t/jour)
scoreboard players operation #temps previousTdhTick = #temps currentTdhTick
scoreboard players add #temps currentTdhTick 100

# On fait les calculs d'heure et de période
function tdh:time/tdh_tick/period

# En fonction de la période, on exécute la fonction correspondante
execute if score #temps timeOfDay matches 1 run function tdh:time/tdh_tick/sleep/1
execute if score #temps timeOfDay matches 2 run function tdh:time/tdh_tick/sleep/2
execute if score #temps timeOfDay matches 3 run function tdh:time/tdh_tick/sleep/3
execute if score #temps timeOfDay matches 4 run function tdh:time/tdh_tick/sleep/4

# Si on est en fin de nuit (et qu'il n'y a pas d'orage), on avance au matin
#execute if score #temps currentTick matches 5000..5999 unless score fakeWeather CurrentWeather matches 2.. run function tdh:time/day
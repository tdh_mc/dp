# Condition pour l'incrémentation de currentTick :
# (sunriseBegin - previousTdhTick (+fullDayTicks?)) / (defaultSunriseBegin - currentTick (+defaultFullDayTicks?))
# <= (sunriseBegin - nightBegin + fullDayTicks) / (defaultSunriseBegin - defaultNightBegin + defaultFullDayTicks)

# temp, temp2 = sunriseBegin
# temp3, temp4 = defaultSunriseBegin
scoreboard players operation #temps temp = #temps sunriseBegin
scoreboard players operation #temps temp2 = #temps sunriseBegin
scoreboard players operation #temps temp3 = #tempsOriginal sunriseBegin
scoreboard players operation #temps temp4 = #tempsOriginal sunriseBegin

# temp = (sunriseBegin - previousTdhTick) (+fullDayTicks?)
# temp2 = (sunriseBegin - nightBegin + fullDayTicks)
# temp3 = (defaultSunriseBegin - currentTick) (+defaultFullDayTicks?)
# temp4 = (defaultSunriseBegin - defaultNightBegin + defaultFullDayTicks)
scoreboard players operation #temps temp -= #temps previousTdhTick
execute if score #temps temp matches ..-1 run scoreboard players operation #temps temp += #temps fullDayTicks
scoreboard players operation #temps temp2 -= #temps nightBegin
scoreboard players operation #temps temp2 += #temps fullDayTicks
scoreboard players operation #temps temp3 -= #temps currentTick
execute if score #temps temp3 matches ..-1 run scoreboard players operation #temps temp3 += #tempsOriginal fullDayTicks
scoreboard players operation #temps temp4 -= #tempsOriginal nightBegin
scoreboard players operation #temps temp4 += #tempsOriginal fullDayTicks

# temp5 = 1'000
# temp = 1'000 * (sunriseBegin - previousTdhTick) / (defaultSunriseBegin - currentTick)
# temp2 = 1'000 * (sunriseBegin - nightBegin) / (defaultSunriseBegin - defaultNightBegin)
scoreboard players set #temps temp5 1000
scoreboard players operation #temps temp *= #temps temp5
scoreboard players operation #temps temp /= #temps temp3
scoreboard players operation #temps temp2 *= #temps temp5
scoreboard players operation #temps temp2 /= #temps temp4

# Si temp <= temp2, on incrémente currentTick
execute if score #temps temp <= #temps temp2 run time add 1t

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
scoreboard players reset #temps temp4
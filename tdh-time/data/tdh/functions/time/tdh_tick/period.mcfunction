# Si on a changé de jour (= le tick TDH est supérieur ou égal à 48000), on exécute la fonction changement de jour
execute if score #temps currentTdhTick >= #temps fullDayTicks run function tdh:time/tdh_tick/prochain_jour

# On calcule l'heure précise à partir du tick TDH
function tdh:time/storage/heure

# On calcule la période de la journée à partir du tick TDH
function tdh:time/storage/periode
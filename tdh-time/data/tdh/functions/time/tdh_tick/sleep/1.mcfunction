# On augmentera currentTick d'une valeur de :
# 100 * (defaultSunsetBegin - currentTick) / (sunsetBegin - previousTdhTick)

# temp = sunsetBegin
# temp2 = defaultSunsetBegin
scoreboard players operation #temps temp = #temps sunsetBegin
scoreboard players operation #temps temp2 = #tempsOriginal sunsetBegin

# temp = (sunsetBegin - previousTdhTick)
# temp2 = (defaultSunsetBegin - currentTick)
scoreboard players operation #temps temp -= #temps previousTdhTick
scoreboard players operation #temps temp2 -= #temps currentTick

# temp2 = 100 * (defaultSunsetBegin - currentTick) / (sunsetBegin - previousTdhTick)
scoreboard players set #temps temp3 100
scoreboard players operation #temps temp2 *= #temps temp3
scoreboard players operation #temps temp2 /= #temps temp

# On incrémente currentTick temp2 fois
function tdh:time/tdh_tick/sleep/increment

# On affiche à tous les joueurs l'heure actuelle
function tdh:time/tdh_tick/sleep/display

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
# On augmentera currentTick d'une valeur de :
# 100 * (defaultNightBegin - currentTick) / (nightBegin - previousTdhTick)

# temp = nightBegin
# temp2 = defaultNightBegin
scoreboard players operation #temps temp = #temps nightBegin
scoreboard players operation #temps temp2 = #tempsOriginal nightBegin

# temp = (nightBegin - previousTdhTick)
# temp2 = (defaultNightBegin - currentTick)
scoreboard players operation #temps temp -= #temps previousTdhTick
scoreboard players operation #temps temp2 -= #temps currentTick

# temp2 = 100 * (defaultNightBegin - currentTick) / (nightBegin - previousTdhTick)
scoreboard players set #temps temp3 100
scoreboard players operation #temps temp2 *= #temps temp3
scoreboard players operation #temps temp2 /= #temps temp

# On incrémente currentTick temp2 fois
function tdh:time/tdh_tick/sleep/increment

# On affiche à tous les joueurs l'heure actuelle
function tdh:time/tdh_tick/sleep/display

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
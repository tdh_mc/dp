execute if score #temps temp2 matches 100.. run function tdh:time/tdh_tick/sleep/increment/100
execute if score #temps temp2 matches 50.. run function tdh:time/tdh_tick/sleep/increment/50
execute if score #temps temp2 matches 20.. run function tdh:time/tdh_tick/sleep/increment/20
execute if score #temps temp2 matches 10.. run function tdh:time/tdh_tick/sleep/increment/10
execute if score #temps temp2 matches 5.. run function tdh:time/tdh_tick/sleep/increment/5
execute if score #temps temp2 matches 1.. run function tdh:time/tdh_tick/sleep/increment/1

execute if score #temps temp2 matches 1.. run function tdh:time/tdh_tick/sleep/increment
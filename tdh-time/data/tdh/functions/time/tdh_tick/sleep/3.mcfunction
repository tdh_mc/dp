# On augmentera currentTick d'une valeur de :
# 100 * (defaultSunriseBegin - currentTick) / (sunriseBegin - previousTdhTick)

# temp = sunriseBegin
# temp2 = defaultSunriseBegin
scoreboard players operation #temps temp = #temps sunriseBegin
scoreboard players operation #temps temp2 = #tempsOriginal sunriseBegin

# temp = (sunriseBegin - previousTdhTick)
# temp2 = (defaultSunriseBegin - currentTick)
scoreboard players operation #temps temp -= #temps previousTdhTick
execute if score #temps temp matches ..-1 run scoreboard players operation #temps temp += #temps fullDayTicks
scoreboard players operation #temps temp2 -= #temps currentTick
execute if score #temps temp2 matches ..-1 run scoreboard players operation #temps temp2 += #tempsOriginal fullDayTicks

# temp2 = 200 * (defaultSunriseBegin - currentTick) / (sunriseBegin - previousTdhTick)
scoreboard players set #temps temp3 100
scoreboard players operation #temps temp2 *= #temps temp3
scoreboard players operation #temps temp2 /= #temps temp

# On incrémente currentTick temp2 fois
function tdh:time/tdh_tick/sleep/increment

# On affiche à tous les joueurs l'heure actuelle
function tdh:time/tdh_tick/sleep/display

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
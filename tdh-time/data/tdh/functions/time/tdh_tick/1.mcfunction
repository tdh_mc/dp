# Condition pour l'incrémentation de currentTick :
# (sunsetBegin - previousTdhTick) / (defaultSunsetBegin - currentTick)
# <= (sunsetBegin - dayBegin) / (defaultSunsetBegin - defaultDayBegin)

# temp, temp2 = sunsetBegin
# temp3, temp4 = defaultSunsetBegin
scoreboard players operation #temps temp = #temps sunsetBegin
scoreboard players operation #temps temp2 = #temps sunsetBegin
scoreboard players operation #temps temp3 = #tempsOriginal sunsetBegin
scoreboard players operation #temps temp4 = #tempsOriginal sunsetBegin

# temp = (sunsetBegin - previousTdhTick)
# temp2 = (sunsetBegin - dayBegin)
# temp3 = (defaultSunsetBegin - currentTick)
# temp4 = (defaultSunsetBegin - defaultDayBegin)
scoreboard players operation #temps temp -= #temps previousTdhTick
scoreboard players operation #temps temp2 -= #temps dayBegin
scoreboard players operation #temps temp3 -= #temps currentTick
scoreboard players operation #temps temp4 -= #tempsOriginal dayBegin

# temp5 = 1'000
# temp = 1'000 * (sunsetBegin - previousTdhTick) / (defaultSunsetBegin - currentTick)
# temp2 = 1'000 * (sunsetBegin - dayBegin) / (defaultSunsetBegin - defaultDayBegin)
scoreboard players set #temps temp5 1000
scoreboard players operation #temps temp *= #temps temp5
scoreboard players operation #temps temp /= #temps temp3
scoreboard players operation #temps temp2 *= #temps temp5
scoreboard players operation #temps temp2 /= #temps temp4

# Si temp <= temp2, on incrémente currentTick
execute if score #temps temp <= #temps temp2 run time add 1t

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
scoreboard players reset #temps temp4
## SÉLECTION DU MOIS DE LA DATE DE L'ÉQUINOXE D'AUTOMNE

# On stocke la date actuelle (jour & mois)
scoreboard players operation #store dateJour = #equinoxeAutomne dateJour
scoreboard players operation #store dateMois = #equinoxeAutomne dateMois
function tdh:store/mois

# On stocke le nom du paramètre actuel
data modify storage tdh:config param set value {nom:"équinoxe d'automne",deDu:"de l'",alAu:"à l'"}

# On lance la fonction générique
function tdh:time/config/date_mois_select
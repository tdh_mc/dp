## SÉLECTION DU JOUR DE LA DATE DE L'ÉQUINOXE D'AUTOMNE

# On stocke la date actuelle (jour & mois)
function tdh:store/mois/equinoxe_automne

# On stocke le nom du paramètre actuel
data modify storage tdh:config param set value {nom:"équinoxe d'automne",deDu:"de l'",alAu:"à l'"}

# On lance la fonction générique
function tdh:time/config/date_jour_select
# On tick régulièrement pour lancer les sous-fonctions de prise en compte de la configuration
# Le joueur a une variable configID qui sert à stocker la variable qu'il config, et une variable config qu'il trigger pour communiquer la valeur désirée

# Cette fonction est à destination des administrateurs, donc on peut utiliser execute AS au lieu de AT

# Si on est à l'écran d'accueil, on lance une des fonctions select selon notre choix
execute as @s[scores={configID=9999}] unless score @s config matches 0 run function tdh:time/config/select

# Selon la variable qu'on cherche à config
# - Dans tous les cas (10000+), si on veut revenir au menu (config -120047):
#	(pas possible avec Mois (40000) et Semaine (50000) car on doit spécifier si on conserve ou annule nos modifications)
execute as @s[scores={configID=10000..,config=-120047}] unless score @s configID matches 40000..59999 run function tdh:time/config


# Options des journées :
# - Durée d'une journée (10000)
execute as @s[scores={configID=10000}] unless score @s config matches ..0 run function tdh:time/config/duree_journee_set
# - Répartition aux équinoxes (20000)
execute as @s[scores={configID=20000}] unless score @s config matches ..0 run function tdh:time/config/equinoxe/duree_jour_set
execute as @s[scores={configID=20100}] unless score @s config matches ..0 run function tdh:time/config/equinoxe/duree_nuit_set
execute as @s[scores={configID=20200}] unless score @s config matches ..0 run function tdh:time/config/equinoxe/duree_crepuscule_set
# - Répartition au solstice d'été (21000)
execute as @s[scores={configID=21000}] unless score @s config matches ..0 run function tdh:time/config/solstice_ete/duree_jour_set
execute as @s[scores={configID=21100}] unless score @s config matches ..0 run function tdh:time/config/solstice_ete/duree_nuit_set
execute as @s[scores={configID=21200}] unless score @s config matches ..0 run function tdh:time/config/solstice_ete/duree_crepuscule_set
# - Répartition au solstice d'hiver (22000)
execute as @s[scores={configID=22000}] unless score @s config matches ..0 run function tdh:time/config/solstice_hiver/duree_jour_set
execute as @s[scores={configID=22100}] unless score @s config matches ..0 run function tdh:time/config/solstice_hiver/duree_nuit_set
execute as @s[scores={configID=22200}] unless score @s config matches ..0 run function tdh:time/config/solstice_hiver/duree_crepuscule_set

# Options de l'année :
# - Date de l'équinoxe de printemps (30000)
execute as @s[scores={configID=30000}] unless score @s config matches ..0 run function tdh:time/config/equinoxe_printemps/date_jour_set
execute as @s[scores={configID=30100}] unless score @s config matches ..0 run function tdh:time/config/equinoxe_printemps/date_mois_set
# - Date du solstice d'été (31000)
execute as @s[scores={configID=31000}] unless score @s config matches ..0 run function tdh:time/config/solstice_ete/date_jour_set
execute as @s[scores={configID=31100}] unless score @s config matches ..0 run function tdh:time/config/solstice_ete/date_mois_set
# - Date de l'équinoxe d'automne (32000)
execute as @s[scores={configID=32000}] unless score @s config matches ..0 run function tdh:time/config/equinoxe_automne/date_jour_set
execute as @s[scores={configID=32100}] unless score @s config matches ..0 run function tdh:time/config/equinoxe_automne/date_mois_set
# - Date du solstice d'hiver (33000)
execute as @s[scores={configID=33000}] unless score @s config matches ..0 run function tdh:time/config/solstice_hiver/date_jour_set
execute as @s[scores={configID=33100}] unless score @s config matches ..0 run function tdh:time/config/solstice_hiver/date_mois_set

# Options des mois :
execute as @s[scores={configID=40000}] unless score @s config matches ..0 run function tdh:time/config/mois/tick

# Options des semaines :
execute as @s[scores={configID=50000}] unless score @s config matches ..0 run function tdh:time/config/semaine/tick

# Modification de la date :
execute as @s[scores={configID=60000}] unless score @s config matches ..0 run function tdh:time/config/date_actuelle/jour_set
execute as @s[scores={configID=60100}] unless score @s config matches ..0 run function tdh:time/config/date_actuelle/mois_set
execute as @s[scores={configID=60200}] unless score @s config matches ..0 run function tdh:time/config/date_actuelle/annee_set
execute as @s[scores={configID=60400}] unless score @s config matches ..0 run function tdh:time/config/date_actuelle/jour_semaine_set

# Tous les joueurs qui viennent de choisir une config retournent au menu principal
execute as @s[scores={configID=10000..}] unless score @s config matches 0 run function tdh:time/config
# On appelle cette fonction lorsqu'on modifie la durée de la journée,
# pour modifier le nombre de ticks des différentes périodes de la journée tout en respectant leurs rapports

# Arguments de cette fonction :
# - #temps fullDayTicks = durée actuelle de la journée (supposé être tjrs défini)
# - #dureeJourneeConversion temp = durée précédente de la journée

# On enregistre une variable de scaling
# (on ne peut pas multiplier avant de diviser pour éviter l'overflow, donc on divise après avoir multiplié par un facteur 1000 qu'on retirera à la fin du calcul)
scoreboard players set #dureeJourneeConversion temp2 1000

# On met à jour toutes les valeurs
# On multiplie d'abord tout par la variable de scaling définie ci-dessus
scoreboard players operation #equinoxe sunriseTicks *= #dureeJourneeConversion temp2
scoreboard players operation #equinoxe dayTicks *= #dureeJourneeConversion temp2
scoreboard players operation #equinoxe sunsetTicks *= #dureeJourneeConversion temp2
scoreboard players operation #equinoxe nightTicks *= #dureeJourneeConversion temp2

scoreboard players operation #solsticeEte sunriseTicks *= #dureeJourneeConversion temp2
scoreboard players operation #solsticeEte dayTicks *= #dureeJourneeConversion temp2
scoreboard players operation #solsticeEte sunsetTicks *= #dureeJourneeConversion temp2
scoreboard players operation #solsticeEte nightTicks *= #dureeJourneeConversion temp2

scoreboard players operation #solsticeHiver sunriseTicks *= #dureeJourneeConversion temp2
scoreboard players operation #solsticeHiver dayTicks *= #dureeJourneeConversion temp2
scoreboard players operation #solsticeHiver sunsetTicks *= #dureeJourneeConversion temp2
scoreboard players operation #solsticeHiver nightTicks *= #dureeJourneeConversion temp2

# Puis on divise par la durée précédente de la journée
scoreboard players operation #equinoxe sunriseTicks /= #dureeJourneeConversion temp
scoreboard players operation #equinoxe dayTicks /= #dureeJourneeConversion temp
scoreboard players operation #equinoxe sunsetTicks /= #dureeJourneeConversion temp
scoreboard players operation #equinoxe nightTicks /= #dureeJourneeConversion temp

scoreboard players operation #solsticeEte sunriseTicks /= #dureeJourneeConversion temp
scoreboard players operation #solsticeEte dayTicks /= #dureeJourneeConversion temp
scoreboard players operation #solsticeEte sunsetTicks /= #dureeJourneeConversion temp
scoreboard players operation #solsticeEte nightTicks /= #dureeJourneeConversion temp

scoreboard players operation #solsticeHiver sunriseTicks /= #dureeJourneeConversion temp
scoreboard players operation #solsticeHiver dayTicks /= #dureeJourneeConversion temp
scoreboard players operation #solsticeHiver sunsetTicks /= #dureeJourneeConversion temp
scoreboard players operation #solsticeHiver nightTicks /= #dureeJourneeConversion temp

# On multiplie ensuite par la nouvelle durée de la journée
scoreboard players operation #equinoxe sunriseTicks *= #temps fullDayTicks
scoreboard players operation #equinoxe dayTicks *= #temps fullDayTicks
scoreboard players operation #equinoxe sunsetTicks *= #temps fullDayTicks
scoreboard players operation #equinoxe nightTicks *= #temps fullDayTicks

scoreboard players operation #solsticeEte sunriseTicks *= #temps fullDayTicks
scoreboard players operation #solsticeEte dayTicks *= #temps fullDayTicks
scoreboard players operation #solsticeEte sunsetTicks *= #temps fullDayTicks
scoreboard players operation #solsticeEte nightTicks *= #temps fullDayTicks

scoreboard players operation #solsticeHiver sunriseTicks *= #temps fullDayTicks
scoreboard players operation #solsticeHiver dayTicks *= #temps fullDayTicks
scoreboard players operation #solsticeHiver sunsetTicks *= #temps fullDayTicks
scoreboard players operation #solsticeHiver nightTicks *= #temps fullDayTicks

# Enfin, on divise par la variable de scaling
scoreboard players operation #equinoxe sunriseTicks /= #dureeJourneeConversion temp2
scoreboard players operation #equinoxe dayTicks /= #dureeJourneeConversion temp2
scoreboard players operation #equinoxe sunsetTicks /= #dureeJourneeConversion temp2
scoreboard players operation #equinoxe nightTicks /= #dureeJourneeConversion temp2

scoreboard players operation #solsticeEte sunriseTicks /= #dureeJourneeConversion temp2
scoreboard players operation #solsticeEte dayTicks /= #dureeJourneeConversion temp2
scoreboard players operation #solsticeEte sunsetTicks /= #dureeJourneeConversion temp2
scoreboard players operation #solsticeEte nightTicks /= #dureeJourneeConversion temp2

scoreboard players operation #solsticeHiver sunriseTicks /= #dureeJourneeConversion temp2
scoreboard players operation #solsticeHiver dayTicks /= #dureeJourneeConversion temp2
scoreboard players operation #solsticeHiver sunsetTicks /= #dureeJourneeConversion temp2
scoreboard players operation #solsticeHiver nightTicks /= #dureeJourneeConversion temp2


# On vérifie l'intégrité des résultats
function tdh:time/config/equinoxe/verifier_duree
function tdh:time/config/solstice_ete/verifier_duree
function tdh:time/config/solstice_hiver/verifier_duree


scoreboard players reset #dureeJourneeConversion temp2
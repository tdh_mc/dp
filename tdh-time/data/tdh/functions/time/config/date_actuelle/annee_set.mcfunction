## ENREGISTREMENT DE L'ANNÉE ACTUELLE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (min: 1, pas de maximum)
execute if score @s config matches 1.. run scoreboard players operation #temps dateAnnee = @s config

# On recalcule l'ensemble des données de la date
function tdh:time/date_check

# On affiche un message de confirmation
function tdh:time/display/date
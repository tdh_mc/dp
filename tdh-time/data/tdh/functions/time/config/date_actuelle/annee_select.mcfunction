## SÉLECTION DE L'ANNÉE ACTUELLE

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config 0
scoreboard players enable @s config


tellraw @s [{"text":"CONFIGURATEUR TDH-TIME","color":"gold"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez l'année actuelle :","color":"gray"}]
tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","hoverEvent":{"action":"show_text","value":"Entrer une année (forcément positive)."},"clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"score":{"name":"#temps","objective":"dateAnnee"}},{"text":")"}]
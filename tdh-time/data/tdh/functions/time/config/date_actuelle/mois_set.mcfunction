## ENREGISTREMENT DU MOIS ACTUEL
# Il est indiqué par la variable config

# On définit la valeur de la variable (min: 1, max:dateMoisMax)
execute if score @s config matches 1.. unless score @s config > #temps dateMoisMax run scoreboard players operation #temps dateMois = @s config
execute if score @s config > #temps dateMoisMax run scoreboard players operation #temps dateMois = #temps dateMoisMax

# On recalcule l'ensemble des données de la date
function tdh:time/date_check

# On affiche un message de confirmation
function tdh:time/display/date
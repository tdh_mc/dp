## ENREGISTREMENT DU JOUR DE LA SEMAINE ACTUEL
# Il est indiqué par la variable config

# On définit la valeur de la variable (min: 1, max:jourSemaineMax)
execute if score @s config matches 1.. unless score @s config > #temps jourSemaineMax run scoreboard players operation #temps jourSemaine = @s config
execute if score @s config > #temps jourSemaineMax run scoreboard players operation #temps jourSemaine = #temps jourSemaineMax

# On recalcule l'ensemble des données de la date
function tdh:time/date_check

# On affiche un message de confirmation
function tdh:time/display/date
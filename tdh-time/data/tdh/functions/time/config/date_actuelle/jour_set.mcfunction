## ENREGISTREMENT DU JOUR ACTUEL
# Il est indiqué par la variable config

# On définit la valeur de la variable (min: 1, max:dateJourMax)
execute if score @s config matches 1.. unless score @s config > #temps dateJourMax run scoreboard players operation #temps dateJour = @s config
execute if score @s config > #temps dateJourMax run scoreboard players operation #temps dateJour = #temps dateJourMax

# On recalcule l'ensemble des données de la date
function tdh:time/date_check

# On affiche un message de confirmation
function tdh:time/display/date
# On se place juste après minuit
scoreboard players operation #temps currentTdhTick = #temps fullDayTicks
# On exécute la fonction de changement de jour
function tdh:time/tdh_tick/prochain_jour
# On définit l'heure au lever du soleil
function tdh:time/reset_heure
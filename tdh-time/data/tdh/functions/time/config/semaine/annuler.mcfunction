# On restaure le storage de sauvegarde de l'état original de la config des mois
data modify storage tdh:datetime index.jour_semaine set from storage tdh:datetime index_backup.jour_semaine
data remove storage tdh:datetime index_backup.jour_semaine

# On recalcule la durée des semaines
function tdh:time/init/subdivisions/calcul_semaine

# On retourne au menu principal
function tdh:time/config
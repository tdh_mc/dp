# On supprime le storage de sauvegarde de l'état original de la config des mois
data remove storage tdh:datetime index_backup.jour_semaine

# On recalcule la durée des semaines
function tdh:time/init/subdivisions/calcul_semaine

# On retourne au menu principal
function tdh:time/config
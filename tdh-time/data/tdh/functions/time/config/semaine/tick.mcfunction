# Cette fonction est exécutée régulièrement par un admin quand son ID de config est 50000 (= config des semaines)
# On vérifie sa variable "config" pour savoir quoi faire ensuite

# - Actualisation des informations (1)
execute as @s[scores={config=1}] run function tdh:time/config/semaine
# - Sauvegarde de la configuration (2)
execute as @s[scores={config=2}] run function tdh:time/config/semaine/sauvegarder
# - Restauration de la configuration précédente (3)
execute as @s[scores={config=3..}] run function tdh:time/config/semaine/annuler
## ENREGISTREMENT DE LA DURÉE DE LA JOURNÉE
# Elle est indiquée par la variable config

# On enregistre la valeur précédente
scoreboard players operation #dureeJourneeConversion temp = #temps fullDayTicks
# On définit la valeur de la variable (minimum 30mn (36'000t), maximum 24h (1'728'000t)
execute if score @s config matches 36000..1728000 run scoreboard players operation #temps fullDayTicks = @s config
execute if score @s config matches 0..35999 run scoreboard players set #temps fullDayTicks 36000
execute if score @s config matches 1728001.. run scoreboard players set #temps fullDayTicks 1728000
# On s'assure que la nouvelle valeur est bien multiple de notre nombre d'heures par jour
scoreboard players operation #temps fullDayTicks /= #temps hoursPerDay
scoreboard players operation #temps fullDayTicks *= #temps hoursPerDay

# On scale les valeurs actuelles des durées des périodes de la journée
# (en les multipliant par le nouveau nombre de ticks et en divisant par l'ancien)
function tdh:time/config/duree_journee_set/conversion_durees

# On recalcule la durée d'une heure en ticks
function tdh:time/storage/ticks_par_heure


# On stocke la nouvelle durée de la journée
scoreboard players operation #store currentHour = #temps fullDayTicks
function tdh:store/realtime

# On affiche un message de confirmation
tellraw @s [{"text":"Une journée dure désormais ","color":"gray"},{"nbt":"time","storage":"tdh:store","color":"yellow"},{"text":". Les durées individuelles des périodes de la journée ont été modifiées en conséquence."}]
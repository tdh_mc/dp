## SÉLECTION DU MOIS DE LA DATE DE L'ÉQUINOXE DE PRINTEMPS

# On stocke la date actuelle (jour & mois)
scoreboard players operation #store dateJour = #equinoxePrintemps dateJour
scoreboard players operation #store dateMois = #equinoxePrintemps dateMois
function tdh:store/mois

# On stocke le nom du paramètre actuel
data modify storage tdh:config param set value {nom:"équinoxe de printemps",deDu:"de l'",alAu:"à l'"}

# On lance la fonction générique
function tdh:time/config/date_mois_select
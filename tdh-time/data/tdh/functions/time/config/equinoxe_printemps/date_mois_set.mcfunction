## ENREGISTREMENT DU MOIS DE LA DATE DE L'ÉQUINOXE DE PRINTEMPS
# Elle est indiquée par la variable config

# On définit la valeur de la variable
execute unless score @s config matches ..0 run scoreboard players operation #equinoxePrintemps dateMois = @s config

# On recalcule la saison dans laquelle on se trouve
function tdh:time/storage/saison

# On stocke la nouvelle date pour l'afficher ci-après
function tdh:store/mois/equinoxe_printemps

# On affiche un message de confirmation
tellraw @s [{"text":"L'équinoxe de printemps se produira désormais le ","color":"gray"},{"score":{"name":"#store","objective":"dateJour"},"color":"yellow"},{"text":" "},{"nbt":"mois.min","storage":"tdh:store","color":"yellow"},{"text":"."}]
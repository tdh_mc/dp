## SÉLECTION DE LA DURÉE DU JOUR À L'ÉQUINOXE

# On calcule la valeur actuelle
scoreboard players operation #store currentHour = #equinoxe dayTicks
function tdh:store/realtime

# On stocke le nom du paramètre actuel
data modify storage tdh:config param set value {nom:"équinoxe",deDu:"de l'",alAu:"à l'"}

# On lance la fonction générique
function tdh:time/config/duree_jour_select
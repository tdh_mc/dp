# On additionne les durées respectives des différentes périodes de la journée
scoreboard players set #temps temp 0
scoreboard players operation #temps temp += #equinoxe sunsetTicks
scoreboard players operation #temps temp += #equinoxe sunriseTicks
scoreboard players operation #temps temp += #equinoxe dayTicks
scoreboard players operation #temps temp += #equinoxe nightTicks

# On calcule la différence entre cette somme et la durée totale d'une journée
scoreboard players operation #temps temp -= #temps fullDayTicks

# Si cette différence ne vaut pas 0, on modifie la durée du jour pour corriger la durée totale
execute unless score #temps temp matches 0 run scoreboard players operation #equinoxe dayTicks -= #temps temp

scoreboard players reset #temps temp
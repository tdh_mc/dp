## ENREGISTREMENT DE LA DURÉE DU CRÉPUSCULE À L'ÉQUINOXE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (min: 50s (1000t), max: durée d'une journée / 2 - 11000t)
# On calcule d'abord le maximum autorisé (qu'on stocke dans #temps temp)
scoreboard players operation #temps temp = #temps fullDayTicks
scoreboard players set #temps temp2 2
scoreboard players operation #temps temp /= #temps temp2
scoreboard players remove #temps temp 11000
scoreboard players reset #temps temp2
# On définit ensuite la valeur configurée, clampée aux minimum et maximum calculés ci-dessus
execute if score @s config matches 1000.. if score @s config <= #temps temp run scoreboard players operation #equinoxe sunsetTicks = @s config
execute if score @s config matches 0..999 run scoreboard players set #equinoxe sunsetTicks 1000
execute if score @s config > #temps temp run scoreboard players operation #equinoxe sunsetTicks = #temps temp

# Les 2 crépuscules ayant la même durée, on définit le second à la valeur du premier
scoreboard players operation #equinoxe sunriseTicks = #equinoxe sunsetTicks


# On garde le même rapport entre le jour et la nuit, mais on les scale accordément :
# On calcule la part actuelle du jour par rapport à l'ensemble jour + nuit (crépuscules non inclus)
scoreboard players set #temps temp2 1000
scoreboard players operation #temps temp3 = #equinoxe dayTicks
scoreboard players operation #temps temp3 += #equinoxe nightTicks
# À partir d'ici, temp3 = (ticksJour + ticksNuit)
scoreboard players operation #temps temp = #equinoxe dayTicks
scoreboard players operation #temps temp *= #temps temp2
# À partir d'ici, temp = (ticksJour * 1000)
scoreboard players operation #temps temp /= #temps temp3
# " temp = 1000 * (ticksJour / (ticksJour + ticksNuit))

# On calcule le temps disponible pour jour+nuit avec la nouvelle durée des crépuscules
scoreboard players operation #temps temp3 = #temps fullDayTicks
scoreboard players operation #temps temp3 -= #equinoxe sunriseTicks
scoreboard players operation #temps temp3 -= #equinoxe sunsetTicks
# temp3 = ticksJourneeEntiere - ticksCrépuscule1 - ticksCrépuscule2
#		= ticksJour + ticksNuit
scoreboard players operation #equinoxe dayTicks = #temps temp3
scoreboard players operation #equinoxe dayTicks *= #temps temp
# ticksJour = 1000 * (ticksJour + ticksNuit) * (ticksJour / (ticksJour + ticksNuit))
#			= 1000 * ticksJour
scoreboard players operation #equinoxe dayTicks /= #temps temp2
#			= ticksJour

# On calcule ensuite ticksNuit en retranchant ticksJour au temps total trouvé ci-dessus (temp3)
scoreboard players operation #equinoxe nightTicks = #temps temp3
scoreboard players operation #equinoxe nightTicks -= #equinoxe dayTicks


# On stocke la nouvelle valeur pour l'afficher ci-après
scoreboard players operation #store currentHour = #equinoxe sunsetTicks
function tdh:store/realtime
# On calcule le pourcentage d'une journée que représente désormais le crépuscule
#scoreboard players set #temps temp2 1000 (déjà fait précédemment)
scoreboard players operation #temps temp = #equinoxe sunsetTicks
scoreboard players operation #temps temp *= #temps temp2
scoreboard players operation #temps temp /= #temps fullDayTicks
# temp = 1000 * sunsetTicks / fullDayTicks (donc en pour-mille du temps)
scoreboard players set #temps temp2 10
scoreboard players operation #temps temp3 = #temps temp
scoreboard players operation #temps temp3 %= #temps temp2
# temp3 = le nombre après la virgule (le "3" dans "17,3 %")
scoreboard players operation #temps temp /= #temps temp2
# temp = le nombre avant la virgule (le "17" ci-dessus)

# On affiche un message de confirmation
tellraw @s [{"text":"À l'équinoxe, un crépuscule dure désormais ","color":"gray"},{"nbt":"realtime","storage":"tdh:store","color":"yellow"},{"text":" ("},{"score":{"name":"#temps","objective":"temp"},"color":"red"},{"text":".","color":"red"},{"score":{"name":"#temps","objective":"temp3"},"color":"red"},{"text":"%","color":"red"},{"text":" d'une journée)."}]

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
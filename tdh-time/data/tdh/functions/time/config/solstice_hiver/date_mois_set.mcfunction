## ENREGISTREMENT DU MOIS DE LA DATE DU SOLSTICE D'HIVER
# Elle est indiquée par la variable config

# On définit la valeur de la variable
execute unless score @s config matches ..0 run scoreboard players operation #solsticeHiver dateMois = @s config

# On recalcule la saison dans laquelle on se trouve
function tdh:time/storage/saison

# On stocke la nouvelle date pour l'afficher ci-après
function tdh:store/mois/solstice_hiver

# On affiche un message de confirmation
tellraw @s [{"text":"Le solstice d'hiver se produira désormais le ","color":"gray"},{"score":{"name":"#store","objective":"dateJour"},"color":"yellow"},{"text":" "},{"nbt":"mois.min","storage":"tdh:store","color":"yellow"},{"text":"."}]
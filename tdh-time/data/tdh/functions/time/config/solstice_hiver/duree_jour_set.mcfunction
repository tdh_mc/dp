## ENREGISTREMENT DE LA DURÉE DU JOUR AU SOLSTICE D'HIVER
# Elle est indiquée par la variable config

# On définit la valeur de la variable (min: 10mn (12000t), max: durée d'une journée - 10000t - 2x durée du crépuscule)
# On calcule d'abord le maximum autorisé (qu'on stocke dans #temps temp)
scoreboard players operation #temps temp = #temps fullDayTicks
scoreboard players remove #temps temp 10000
scoreboard players operation #temps temp -= #solsticeHiver sunsetTicks
scoreboard players operation #temps temp -= #solsticeHiver sunriseTicks
# On définit ensuite la valeur configurée, clampée aux minimum et maximum calculés ci-dessus
execute if score @s config matches 12000.. if score @s config <= #temps temp run scoreboard players operation #solsticeHiver dayTicks = @s config
execute if score @s config matches 0..11999 run scoreboard players set #solsticeHiver dayTicks 12000
execute if score @s config > #temps temp run scoreboard players operation #solsticeHiver dayTicks = #temps temp

# On ne modifie pas la durée des crépuscules ; on recalcule simplement celle de la nuit à partir des 3 autres
scoreboard players operation #solsticeHiver nightTicks = #temps fullDayTicks
scoreboard players operation #solsticeHiver nightTicks -= #solsticeHiver dayTicks
scoreboard players operation #solsticeHiver nightTicks -= #solsticeHiver sunriseTicks
scoreboard players operation #solsticeHiver nightTicks -= #solsticeHiver sunsetTicks


# On stocke la nouvelle valeur pour l'afficher ci-après
scoreboard players operation #store currentHour = #solsticeHiver dayTicks
function tdh:store/realtime
# On calcule le pourcentage d'une journée que représente désormais le jour
scoreboard players set #temps temp2 1000
scoreboard players operation #temps temp = #solsticeHiver dayTicks
scoreboard players operation #temps temp *= #temps temp2
scoreboard players operation #temps temp /= #temps fullDayTicks
# temp = 1000 * dayTicks / fullDayTicks (donc en pour-mille du temps)
scoreboard players set #temps temp2 10
scoreboard players operation #temps temp3 = #temps temp
scoreboard players operation #temps temp3 %= #temps temp2
# temp3 = le nombre après la virgule (le "3" dans "17,3 %")
scoreboard players operation #temps temp /= #temps temp2
# temp = le nombre avant la virgule (le "17" ci-dessus)

# On affiche un message de confirmation
tellraw @s [{"text":"Au solstice d'hiver, le jour dure désormais ","color":"gray"},{"nbt":"realtime","storage":"tdh:store","color":"yellow"},{"text":" ("},{"score":{"name":"#temps","objective":"temp"},"color":"red"},{"text":".","color":"red"},{"score":{"name":"#temps","objective":"temp3"},"color":"red"},{"text":"%","color":"red"},{"text":" d'une journée)."}]

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp3
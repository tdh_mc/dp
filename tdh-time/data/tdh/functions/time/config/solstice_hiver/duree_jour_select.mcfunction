## SÉLECTION DE LA DURÉE DU JOUR AU SOLSTICE D'HIVER

# On calcule la valeur actuelle
scoreboard players operation #store currentHour = #solsticeHiver dayTicks
function tdh:store/realtime

# On stocke le nom du paramètre actuel
data modify storage tdh:config param set value {nom:"solstice d'hiver",deDu:"du ",alAu:"au "}

# On lance la fonction générique
function tdh:time/config/duree_jour_select
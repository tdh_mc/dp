# On additionne les durées respectives des différentes périodes de la journée
scoreboard players set #temps temp 0
scoreboard players operation #temps temp += #solsticeHiver sunsetTicks
scoreboard players operation #temps temp += #solsticeHiver sunriseTicks
scoreboard players operation #temps temp += #solsticeHiver dayTicks
scoreboard players operation #temps temp += #solsticeHiver nightTicks

# On calcule la différence entre cette somme et la durée totale d'une journée
scoreboard players operation #temps temp -= #temps fullDayTicks

# Si cette différence ne vaut pas 0, on modifie la durée du jour pour corriger la durée totale
execute unless score #temps temp matches 0 run scoreboard players operation #solsticeHiver dayTicks -= #temps temp

scoreboard players reset #temps temp
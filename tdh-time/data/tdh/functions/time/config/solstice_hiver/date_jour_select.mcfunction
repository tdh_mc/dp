## SÉLECTION DU JOUR DE LA DATE DU SOLSTICE D'HIVER

# On stocke la date actuelle (jour & mois)
scoreboard players operation #store dateJour = #solsticeHiver dateJour
scoreboard players operation #store dateMois = #solsticeHiver dateMois
function tdh:store/mois

# On stocke le nom du paramètre actuel
data modify storage tdh:config param set value {nom:"solstice d'hiver",deDu:"du ",alAu:"au "}

# On lance la fonction générique
function tdh:time/config/date_jour_select
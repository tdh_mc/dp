## ENREGISTREMENT DU NOMBRE DE MINUTES PAR HEURE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (minimum 10, maximum 10000)
execute if score @s config matches 10..10000 run scoreboard players operation #temps minutesPerHour = @s config
execute if score @s config matches 0..9 run scoreboard players set #temps minutesPerHour 10
execute if score @s config matches 10001.. run scoreboard players set #temps minutesPerHour 10000

# On affiche un message de confirmation
tellraw @s [{"text":"Une heure est désormais découpée en ","color":"gray"},{"score":{"name":"#temps","objective":"minutesPerHour"},"color":"yellow"},{"text":" minutes."}]
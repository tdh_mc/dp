## ENREGISTREMENT DU NOMBRE D'HEURES PAR JOURNÉE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (minimum 2, maximum 100)
execute if score @s config matches 2..100 run scoreboard players operation #temps hoursPerDay = @s config
execute if score @s config matches 0..1 run scoreboard players set #temps hoursPerDay 2
execute if score @s config matches 101.. run scoreboard players set #temps hoursPerDay 100
# On s'assure que le nombre de ticks par jour est bien un multiple de ce nouveau nombre
scoreboard players operation #temps temp = #temps fullDayTicks
scoreboard players operation #temps temp /= #temps hoursPerDay
scoreboard players operation #temps temp *= #temps hoursPerDay
execute unless score #temps temp = #temps fullDayTicks run scoreboard players operation #temps fullDayTicks = #temps temp

# On recalcule la durée d'une heure en ticks
function tdh:time/storage/ticks_par_heure

# On affiche un message de confirmation
tellraw @s [{"text":"Une journée est désormais découpée en ","color":"gray"},{"score":{"name":"#temps","objective":"hoursPerDay"},"color":"yellow"},{"text":" heures."}]
# Cette fonction est exécutée par un administrateur
# avec configID = 9999 (menu principal) et config = le menu qu'il veut afficher

# Selon le menu qu'on veut afficher

# Options des journées :
# - Durée d'une journée (10000)
execute as @s[scores={config=10000}] run function tdh:time/config/duree_journee_select
# - Heures par journée (11000)
execute as @s[scores={config=11000}] run function tdh:time/config/heures_par_journee_select
# - Minutes par heure (12000)
execute as @s[scores={config=12000}] run function tdh:time/config/minutes_par_heure_select
# - Démarrer/arrêter l'écoulement du temps (15010/15011)
execute as @s[scores={config=15010}] run function tdh:time/config/timeflow/start
execute as @s[scores={config=15011}] run function tdh:time/config/timeflow/stop
# - Répartition aux équinoxes (20000)
execute as @s[scores={config=20000}] run function tdh:time/config/equinoxe/duree_jour_select
execute as @s[scores={config=20100}] run function tdh:time/config/equinoxe/duree_nuit_select
execute as @s[scores={config=20200}] run function tdh:time/config/equinoxe/duree_crepuscule_select
# - Répartition au solstice d'été (21000)
execute as @s[scores={config=21000}] run function tdh:time/config/solstice_ete/duree_jour_select
execute as @s[scores={config=21100}] run function tdh:time/config/solstice_ete/duree_nuit_select
execute as @s[scores={config=21200}] run function tdh:time/config/solstice_ete/duree_crepuscule_select
# - Répartition au solstice d'hiver (22000)
execute as @s[scores={config=22000}] run function tdh:time/config/solstice_hiver/duree_jour_select
execute as @s[scores={config=22100}] run function tdh:time/config/solstice_hiver/duree_nuit_select
execute as @s[scores={config=22200}] run function tdh:time/config/solstice_hiver/duree_crepuscule_select

# Options de l'année :
# - Date de l'équinoxe de printemps (30000)
execute as @s[scores={config=30000}] run function tdh:time/config/equinoxe_printemps/date_jour_select
execute as @s[scores={config=30100}] run function tdh:time/config/equinoxe_printemps/date_mois_select
# - Date du solstice d'été (31000)
execute as @s[scores={config=31000}] run function tdh:time/config/solstice_ete/date_jour_select
execute as @s[scores={config=31100}] run function tdh:time/config/solstice_ete/date_mois_select
# - Date de l'équinoxe d'automne (32000)
execute as @s[scores={config=32000}] run function tdh:time/config/equinoxe_automne/date_jour_select
execute as @s[scores={config=32100}] run function tdh:time/config/equinoxe_automne/date_mois_select
# - Date du solstice d'hiver (33000)
execute as @s[scores={config=33000}] run function tdh:time/config/solstice_hiver/date_jour_select
execute as @s[scores={config=33100}] run function tdh:time/config/solstice_hiver/date_mois_select

# Options des mois :
execute as @s[scores={config=40000}] run function tdh:time/config/mois

# Options des semaines :
execute as @s[scores={config=50000}] run function tdh:time/config/semaine

# Modification de la date :
execute as @s[scores={config=60000}] run function tdh:time/config/date_actuelle/jour_select
execute as @s[scores={config=60100}] run function tdh:time/config/date_actuelle/mois_select
execute as @s[scores={config=60200}] run function tdh:time/config/date_actuelle/annee_select
execute as @s[scores={config=60400}] run function tdh:time/config/date_actuelle/jour_semaine_select

# - Quitter le configurateur (-1)
execute as @s[scores={config=-1}] run function tdh:config/end
## SÉLECTION DU JOUR DE LA DATE DU SOLSTICE D'ÉTÉ

# On stocke la date actuelle (jour & mois)
scoreboard players operation #store dateJour = #solsticeEte dateJour
scoreboard players operation #store dateMois = #solsticeEte dateMois
function tdh:store/mois

# On stocke le nom du paramètre actuel
data modify storage tdh:config param set value {nom:"solstice d'été",deDu:"du ",alAu:"au "}

# On lance la fonction générique
function tdh:time/config/date_jour_select
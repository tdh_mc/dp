## SÉLECTION DE LA DURÉE DE LA NUIT AU SOLSTICE D'ÉTÉ

# On calcule la valeur actuelle
scoreboard players operation #store currentHour = #solsticeEte nightTicks
function tdh:store/realtime

# On stocke le nom du paramètre actuel
data modify storage tdh:config param set value {nom:"solstice d'été",deDu:"du ",alAu:"au "}

# On lance la fonction générique
function tdh:time/config/duree_nuit_select
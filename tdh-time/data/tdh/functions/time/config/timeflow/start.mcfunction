## ACTIVATION DU TIMEFLOW

# On active l'écoulement du temps
scoreboard players set #temps on 1

# On affiche un message de confirmation
tellraw @s [{"text":"L'écoulement du temps a été ","color":"gray"},{"text":"relancé","color":"green"},{"text":"."}]

# On réaffiche le menu principal
function tdh:time/config
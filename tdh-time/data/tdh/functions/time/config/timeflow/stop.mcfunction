## DÉSACTIVATION DU TIMEFLOW

# On désactive l'écoulement du temps
scoreboard players set #temps on 0

# On affiche un message de confirmation
tellraw @s [{"text":"L'écoulement du temps a été ","color":"gray"},{"text":"mis en pause","color":"red"},{"text":"."}]

# On réaffiche le menu principal
function tdh:time/config
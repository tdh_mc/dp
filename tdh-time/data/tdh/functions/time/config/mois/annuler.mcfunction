# On restaure le storage de sauvegarde de l'état original de la config des mois
data modify storage tdh:datetime index.mois set from storage tdh:datetime index_backup.mois
data remove storage tdh:datetime index_backup.mois

# On recalcule la durée des mois
function tdh:time/init/subdivisions/calcul_mois

# On retourne au menu principal
function tdh:time/config
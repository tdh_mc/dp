# Cette fonction est exécutée régulièrement par un admin quand son ID de config est 40000 (= config des mois)
# On vérifie sa variable "config" pour savoir quoi faire ensuite

# - Actualisation des informations (1)
execute as @s[scores={config=1}] run function tdh:time/config/mois
# - Sauvegarde de la configuration (2)
execute as @s[scores={config=2}] run function tdh:time/config/mois/sauvegarder
# - Restauration de la configuration précédente (3)
execute as @s[scores={config=3..}] run function tdh:time/config/mois/annuler
# On affiche la date
tellraw @s [{"text":"Nous sommes le ","color":"gold"},{"nbt":"date.jour_semaine.majS","storage":"tdh:datetime","color":"yellow"},{"text":" "},{"nbt":"date.jour","storage":"tdh:datetime","color":"yellow"},{"text":" "},{"nbt":"date.mois.min","storage":"tdh:datetime","color":"yellow"},{"text":" "},{"score":{"objective":"dateAnnee","name":"#temps"},"color":"yellow"},{"text":" ! ","color":"gold"},{"text":"(jour n°","color":"gray"},{"score":{"name":"#temps","objective":"idJour"},"color":"gray"},{"text":")","color":"gray"}]

# S'il y a une date spécifique aujourd'hui, on l'affiche
execute if data storage tdh:datetime date.special.formate run tellraw @s [{"nbt":"date.special.formate","storage":"tdh:datetime","interpret":"true"}]


# On affiche également l'heure
function tdh:time/display/time
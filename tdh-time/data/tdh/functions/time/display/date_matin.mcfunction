# Message générique
tellraw @a [{"text":"~~~~~~~~~~~~~~~~~~\n","color":"gray"},{"text":"Une nouvelle journée commence !","color":"gold"}]


# On affiche le message
tellraw @a [{"text":"Nous sommes désormais le ","color":"gray"},{"nbt":"date.jour_semaine.min","storage":"tdh:datetime","color":"yellow"},{"text":" "},{"nbt":"date.jour","storage":"tdh:datetime","color":"yellow"},{"text":" "},{"nbt":"date.mois.min","storage":"tdh:datetime","color":"yellow"},{"text":" "},{"score":{"objective":"dateAnnee","name":"#temps"},"color":"yellow"},{"text":" !","color":"gray"}]

# S'il y a une date spécifique aujourd'hui, on l'affiche
execute if data storage tdh:datetime date.special.formate run tellraw @a [{"nbt":"date.special.formate","storage":"tdh:datetime","interpret":"true"}]



# Fin de message générique
tellraw @a [{"text":"~~~~~~~~~~~~~~~~~~","color":"gray"}]
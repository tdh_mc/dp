# On stocke la bonne écriture du temps dans le storage
scoreboard players operation #store currentHour = #temps currentTdhTick
function tdh:store/time

# On affiche le message à tous les joueurs présents à cette position (l'appelant et les éventuels spectateurs)
tellraw @a[distance=0] [{"text":"Il est ","color":"gold"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":" (","color":"gold"},{"score":{"name":"#temps","objective":"currentTdhTick"},"color":"red"},{"text":" ticks TDH, ","color":"gold"},{"score":{"name":"#temps","objective":"currentTick"},"color":"red"},{"text":" ticks CGC).","color":"gold"}]
# On calcule si l'année est ou non bissextile
scoreboard players operation #tempsConv dateAnnee = #temps dateAnnee
function tdh:time/conv/is_bissextile
scoreboard players operation #temps anneeBissextile = #tempsConv anneeBissextile
# On définit une variable temporaire qui sera set à 0 si on définit les durées jour/nuit pour aujourd'hui
scoreboard players set #temps temp 1

execute if score #temps dateMois = #equinoxePrintemps dateMois if score #temps dateJour = #equinoxePrintemps dateJour run function tdh:time/storage/ratios/equinoxe_printemps
execute if score #temps dateMois = #solsticeEte dateMois if score #temps dateJour = #solsticeEte dateJour run function tdh:time/storage/ratios/solstice_ete
execute if score #temps dateMois = #equinoxeAutomne dateMois if score #temps dateJour = #equinoxeAutomne dateJour run function tdh:time/storage/ratios/equinoxe_automne
execute if score #temps dateMois = #solsticeHiver dateMois if score #temps dateJour = #solsticeHiver dateJour run function tdh:time/storage/ratios/solstice_hiver

# Si la variable est toujours à 1, c'est qu'on n'est pas à un solstice/équinoxe et qu'on peut moduler les ratios
execute if score #temps temp matches 1 run function tdh:time/storage/ratios/saison


# On modifie légèrement la durée du jour si le total des 4 périodes ne donne pas la durée en ticks de la journée
scoreboard players operation #temps temp = #temps fullDayTicks
scoreboard players operation #temps temp -= #temps dayTicks
scoreboard players operation #temps temp -= #temps nightTicks
scoreboard players operation #temps temp -= #temps sunsetTicks
scoreboard players operation #temps temp -= #temps sunriseTicks
execute unless score #temps temp matches 0 run scoreboard players operation #temps dayTicks += #temps temp


# On calcule le début de chaque période de la journée
scoreboard players set #temps temp2 2

scoreboard players operation #temps dayBegin = #temps fullDayTicks
scoreboard players operation #temps dayBegin /= #temps temp2
scoreboard players operation #temps temp = #temps dayTicks
scoreboard players operation #temps temp /= #temps temp2
scoreboard players operation #temps dayBegin -= #temps temp

scoreboard players operation #temps nightBegin = #temps fullDayTicks
scoreboard players operation #temps temp = #temps nightTicks
scoreboard players operation #temps temp /= #temps temp2
scoreboard players operation #temps nightBegin -= #temps temp

scoreboard players operation #temps sunsetBegin = #temps nightBegin
scoreboard players operation #temps sunsetBegin -= #temps sunsetTicks

scoreboard players operation #temps sunriseBegin = #temps dayBegin
scoreboard players operation #temps sunriseBegin -= #temps sunriseTicks

# On calcule l'horaire de lever du soleil (1/2 du temps de lever du soleil après le début du lever de soleil)
scoreboard players operation #store currentHour = #temps sunriseBegin
scoreboard players operation #temps temp9 = #temps sunriseTicks
scoreboard players operation #temps temp9 /= #temps temp2
scoreboard players operation #store currentHour += #temps temp9
# Avant de calculer l'heure et la minute, on enregistre le tick
scoreboard players operation #temps leverSoleil = #store currentHour
function tdh:store/time

# On calcule l'horaire de coucher du soleil (1/2 du temps de coucher du soleil avant le début du coucher de soleil)
scoreboard players operation #store2 currentHour = #temps nightBegin
scoreboard players operation #temps temp9 = #temps sunsetTicks
scoreboard players operation #temps temp9 /= #temps temp2
scoreboard players operation #store2 currentHour -= #temps temp9
# Avant de calculer l'heure et la minute, on enregistre le tick
scoreboard players operation #temps coucherSoleil = #store2 currentHour
function tdh:store/time2

# On affiche un message de debug
tellraw @a[tag=TimeDebugLog] [{"score":{"name":"#temps","objective":"dateJour"},"color":"gray"},{"text":"/","color":"gray"},{"score":{"name":"#temps","objective":"dateMois"},"color":"gray"},{"text":"/","color":"gray"},{"score":{"name":"#temps","objective":"dateAnnee"},"color":"gray"},{"text":" — Lever du soleil : ","color":"gray"},{"storage":"tdh:store","nbt":"time","interpret":"true","color":"yellow"},{"text":".","color":"gray"},{"text":"\nCoucher du soleil : ","color":"gray"},{"storage":"tdh:store","nbt":"time2","interpret":"true","color":"yellow"},{"text":".","color":"gray"}]

# On réinitialise les variables temporaires
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
scoreboard players reset #temps temp9
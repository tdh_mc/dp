# On réinitialise la saison pour savoir ci-dessous si on a déjà fait un calcul
scoreboard players set #temps saison 0

# On a 4 sous-fonctions selon la 1ère saison du calendrier
execute if score #equinoxePrintemps dateMois < #solsticeEte dateMois if score #equinoxePrintemps dateMois < #equinoxeAutomne dateMois if score #equinoxePrintemps dateMois < #solsticeHiver dateMois run function tdh:time/storage/saison/debut_printemps
execute if score #temps saison matches 0 if score #solsticeEte dateMois < #equinoxePrintemps dateMois if score #solsticeEte dateMois < #equinoxeAutomne dateMois if score #solsticeEte dateMois < #solsticeHiver dateMois run function tdh:time/storage/saison/debut_ete
execute if score #temps saison matches 0 if score #equinoxeAutomne dateMois < #equinoxePrintemps dateMois if score #equinoxeAutomne dateMois < #solsticeEte dateMois if score #equinoxeAutomne dateMois < #solsticeHiver dateMois run function tdh:time/storage/saison/debut_automne
execute if score #temps saison matches 0 if score #solsticeHiver dateMois < #equinoxePrintemps dateMois if score #solsticeHiver dateMois < #solsticeEte dateMois if score #solsticeHiver dateMois < #equinoxeAutomne dateMois run function tdh:time/storage/saison/debut_hiver

# On fait des tests plus poussés (dans le cas où plusieurs saisons démarrent le même mois...)
execute if score #temps saison matches 0 run function tdh:time/storage/saison/test_pousse
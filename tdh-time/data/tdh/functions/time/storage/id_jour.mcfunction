# Cette fonction convertit le jour/mois/annee en idJour
# pour être certain que la valeur est correcte
scoreboard players operation #tempsConv dateJour = #temps dateJour
scoreboard players operation #tempsConv dateMois = #temps dateMois
scoreboard players operation #tempsConv dateAnnee = #temps dateAnnee
function tdh:time/conv/jour_mois_annee_vers_idjour
scoreboard players operation #temps idJour = #tempsConv idJour
# Cette fonction convertit le jour/mois en jourAnnee
# pour être certain que la valeur est correcte
scoreboard players operation #tempsConv dateJour = #temps dateJour
scoreboard players operation #tempsConv dateMois = #temps dateMois
function tdh:time/conv/jour_mois_vers_jourannee
scoreboard players operation #temps jourAnnee = #tempsConv jourAnnee
# On fait des tests plus poussés (dans le cas où plusieurs saisons démarrent le même mois...)

scoreboard players set #saison temp 999999
scoreboard players set #saison temp2 999999
scoreboard players operation #saison temp < #equinoxePrintemps dateMois
scoreboard players operation #saison temp < #solsticeEte dateMois
scoreboard players operation #saison temp < #equinoxeAutomne dateMois
scoreboard players operation #saison temp < #solsticeHiver dateMois

execute if score #equinoxePrintemps dateMois = #saison temp if score #equinoxePrintemps dateJour < #saison temp2 run scoreboard players operation #saison temp2 = #equinoxePrintemps dateJour
execute if score #solsticeEte dateMois = #saison temp if score #solsticeEte dateJour < #saison temp2 run scoreboard players operation #saison temp2 = #solsticeEte dateJour
execute if score #equinoxeAutomne dateMois = #saison temp if score #equinoxeAutomne dateJour < #saison temp2 run scoreboard players operation #saison temp2 = #equinoxeAutomne dateJour
execute if score #solsticeHiver dateMois = #saison temp if score #solsticeHiver dateJour < #saison temp2 run scoreboard players operation #saison temp2 = #solsticeHiver dateJour

execute if score #equinoxePrintemps dateMois = #saison temp if score #equinoxePrintemps dateJour = #saison temp2 run function tdh:time/storage/saison/debut_printemps
execute if score #solsticeEte dateMois = #saison temp if score #solsticeEte dateJour = #saison temp2 run function tdh:time/storage/saison/debut_ete
execute if score #equinoxeAutomne dateMois = #saison temp if score #equinoxeAutomne dateJour = #saison temp2 run function tdh:time/storage/saison/debut_automne
execute if score #solsticeHiver dateMois = #saison temp if score #solsticeHiver dateJour = #saison temp2 run function tdh:time/storage/saison/debut_hiver

scoreboard players reset #saison temp
scoreboard players reset #saison temp2
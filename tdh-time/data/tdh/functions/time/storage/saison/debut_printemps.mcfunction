# Version où le premier début de saison de l'année est le printemps

# Par défaut on est au début de l'année, pendant l'hiver (avant l'équinoxe de printemps)
scoreboard players set #temps saison 4

# Si on est après l'équinoxe de printemps, on est au printemps
execute if score #temps dateMois = #equinoxePrintemps dateMois if score #temps dateJour >= #equinoxePrintemps dateJour run scoreboard players set #temps saison 1
execute if score #temps dateMois > #equinoxePrintemps dateMois run scoreboard players set #temps saison 1

# Si on est après le solstice d'été, on est en été
execute if score #temps dateMois = #solsticeEte dateMois if score #temps dateJour >= #solsticeEte dateJour run scoreboard players set #temps saison 2
execute if score #temps dateMois > #solsticeEte dateMois run scoreboard players set #temps saison 2

# Si on est après l'équinoxe d'automne, on est en automne
execute if score #temps dateMois = #equinoxeAutomne dateMois if score #temps dateJour >= #equinoxeAutomne dateJour run scoreboard players set #temps saison 3
execute if score #temps dateMois > #equinoxeAutomne dateMois run scoreboard players set #temps saison 3

# Si on est après le solstice d'hiver, on est en hiver
execute if score #temps dateMois = #solsticeHiver dateMois if score #temps dateJour >= #solsticeHiver dateJour run scoreboard players set #temps saison 4
execute if score #temps dateMois > #solsticeHiver dateMois run scoreboard players set #temps saison 4
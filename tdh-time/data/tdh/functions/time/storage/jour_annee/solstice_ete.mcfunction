# On recalcule la date du solstice d'été
scoreboard players operation #tempsConv dateJour = #solsticeEte dateJour
scoreboard players operation #tempsConv dateMois = #solsticeEte dateMois

# On vérifie si le solstice sera l'an prochain
scoreboard players operation #tempsConv dateAnnee = #temps dateAnnee
execute unless score #temps saison matches 2 if score #solsticeEte dateMois < #temps dateMois run scoreboard players add #tempsConv dateAnnee 1
execute unless score #temps saison matches 2 if score #solsticeEte dateMois = #temps dateMois if score #solsticeEte dateJour < #temps dateJour run scoreboard players add #tempsConv dateAnnee 1

# Si le solstice est l'an prochain, on calcule le statut d'année bissextile de l'an prochain
execute unless score #tempsConv dateAnnee = #temps dateAnnee run function tdh:time/conv/is_bissextile
# Dans le cas contraire, on stocke simplement la valeur actuelle
execute if score #tempsConv dateAnnee = #temps dateAnnee run scoreboard players operation #tempsConv anneeBissextile = #temps anneeBissextile

# On lance enfin le calcul du jourAnnee correspondant au solstice, et on le stocke
function tdh:time/conv/jour_mois_vers_jourannee
scoreboard players operation #solsticeEte jourAnnee = #tempsConv jourAnnee
scoreboard players operation #solsticeEte dateAnnee = #tempsConv dateAnnee
# On recalcule la date du solstice d'hiver
scoreboard players operation #tempsConv dateJour = #solsticeHiver dateJour
scoreboard players operation #tempsConv dateMois = #solsticeHiver dateMois

# On vérifie si le solstice sera l'an prochain
scoreboard players operation #tempsConv dateAnnee = #temps dateAnnee
execute unless score #temps saison matches 4 if score #solsticeHiver dateMois < #temps dateMois run scoreboard players add #tempsConv dateAnnee 1
execute unless score #temps saison matches 4 if score #solsticeHiver dateMois = #temps dateMois if score #solsticeHiver dateJour < #temps dateJour run scoreboard players add #tempsConv dateAnnee 1

# Si le solstice est l'an prochain, on calcule le statut d'année bissextile de l'an prochain
execute unless score #tempsConv dateAnnee = #temps dateAnnee run function tdh:time/conv/is_bissextile
# Dans le cas contraire, on stocke simplement la valeur actuelle
execute if score #tempsConv dateAnnee = #temps dateAnnee run scoreboard players operation #tempsConv anneeBissextile = #temps anneeBissextile

# On lance enfin le calcul du jourAnnee correspondant au solstice, et on le stocke
function tdh:time/conv/jour_mois_vers_jourannee
scoreboard players operation #solsticeHiver jourAnnee = #tempsConv jourAnnee
scoreboard players operation #solsticeHiver dateAnnee = #tempsConv dateAnnee
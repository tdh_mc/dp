# On recalcule la date de l'équinoxe de printemps
scoreboard players operation #tempsConv dateJour = #equinoxePrintemps dateJour
scoreboard players operation #tempsConv dateMois = #equinoxePrintemps dateMois

# On vérifie si l'équinoxe sera l'an prochain
scoreboard players operation #tempsConv dateAnnee = #temps dateAnnee
execute unless score #temps saison matches 1 if score #equinoxePrintemps dateMois < #temps dateMois run scoreboard players add #tempsConv dateAnnee 1
execute unless score #temps saison matches 1 if score #equinoxePrintemps dateMois = #temps dateMois if score #equinoxePrintemps dateJour < #temps dateJour run scoreboard players add #tempsConv dateAnnee 1

# Si l'équinoxe est l'an prochain, on calcule le statut d'année bissextile de l'an prochain
execute unless score #tempsConv dateAnnee = #temps dateAnnee run function tdh:time/conv/is_bissextile
# Dans le cas contraire, on stocke simplement la valeur actuelle
execute if score #tempsConv dateAnnee = #temps dateAnnee run scoreboard players operation #tempsConv anneeBissextile = #temps anneeBissextile

# On lance enfin le calcul du jourAnnee correspondant à l'équinoxe, et on le stocke
function tdh:time/conv/jour_mois_vers_jourannee
scoreboard players operation #equinoxePrintemps jourAnnee = #tempsConv jourAnnee
scoreboard players operation #equinoxePrintemps dateAnnee = #tempsConv dateAnnee
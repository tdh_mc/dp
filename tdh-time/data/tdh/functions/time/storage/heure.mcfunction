# On calcule l'heure précise à partir du tick TDH
scoreboard players operation #temps currentHour = #temps currentTdhTick
scoreboard players operation #temps currentHour /= #temps ticksPerHour
scoreboard players operation #temps currentMinute = #temps currentTdhTick
scoreboard players operation #temps currentMinute %= #temps ticksPerHour

# Pour conserver la précision la plus haute possible, on multiplie d'abord par 60
# avant de diviser par ticksPerHour ;
# cela correspond précisément à une division par ticksPerMinute :
#		ticksPerMinute = ticksPerHour / 60, donc
#	1 / ticksPerMinute = 60 / ticksPerHour
scoreboard players operation #temps currentMinute *= #temps minutesPerHour
scoreboard players operation #temps currentMinute /= #temps ticksPerHour
# On enregistre le fait qu'on a modifié le temps
scoreboard players reset #temps temp

# Selon la saison, on a une sous-fonction différente
execute if score #temps saison matches 1 run function tdh:time/storage/ratios/saison/printemps
execute if score #temps saison matches 2 run function tdh:time/storage/ratios/saison/ete
execute if score #temps saison matches 3 run function tdh:time/storage/ratios/saison/automne
execute if score #temps saison matches 4 run function tdh:time/storage/ratios/saison/hiver
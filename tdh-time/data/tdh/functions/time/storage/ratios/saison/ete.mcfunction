# L'été est la saison n°2 et correspond à (pi/2, pi) sur la courbe sin

# On enregistre comme valeur par défaut celles de l'équinoxe suivant (puisqu'on va aller de 1 à 0, voir ci-dessous)
scoreboard players operation #temps dayTicks = #equinoxe dayTicks
scoreboard players operation #temps nightTicks = #equinoxe nightTicks
scoreboard players operation #temps sunsetTicks = #equinoxe sunsetTicks
scoreboard players operation #temps sunriseTicks = #equinoxe sunriseTicks
# On enregistre dans #tempsTemp la différence entre le solstice précédent et l'équinoxe suivant
# On inverse les 2 car on utilise le sin(x+pi/2) qui va de 1 à 0 au lieu de 0 à 1
scoreboard players operation #tempsTemp dayTicks = #solsticeEte dayTicks
scoreboard players operation #tempsTemp dayTicks -= #equinoxe dayTicks
scoreboard players operation #tempsTemp nightTicks = #solsticeEte nightTicks
scoreboard players operation #tempsTemp nightTicks -= #equinoxe nightTicks
scoreboard players operation #tempsTemp sunsetTicks = #solsticeEte sunsetTicks
scoreboard players operation #tempsTemp sunsetTicks -= #equinoxe sunsetTicks
scoreboard players operation #tempsTemp sunriseTicks = #solsticeEte sunriseTicks
scoreboard players operation #tempsTemp sunriseTicks -= #equinoxe sunriseTicks

# On calcule à présent la proportion de la saison déjà écoulée
# On part de x=500 et on va jusqu'à x=1000 (qui correspond à pi avec notre niveau de précision)
# Donc on multiplie 500 par le nombre de jours écoulés, on divise par le nombre total de jours cette saison, et on ajoute 500

# On calcule le nombre de jours écoulés cette saison
scoreboard players operation #tempsTemp jourAnnee = #temps jourAnnee
# On retranche la date de début de l'été, (seulement si on est la même année que ledit début)
#			(en pratique on ne fait pas le test car on fera la même opération dans le cas contraire)
# Dans ce cas : jourAnnee = jourAnnee - debutSaison
scoreboard players operation #tempsTemp jourAnnee -= #solsticeEte jourAnnee
# Dans le cas contraire, on AJOUTE la différence entre le début de saison et la fin de l'année
# Par ex l'hiver commence le 21 décembre, si on est le 10 janvier, on aura 20j (31-21 + 10)
# Dans ce cas : jourAnnee = (jourAnnee-debutSaison) + jourAnneeMax
#						  = jourAnnee + (jourAnneeMax - debutSaison)
execute unless score #solsticeEte dateAnnee = #temps dateAnnee run scoreboard players operation #tempsTemp jourAnnee += #temps jourAnneeMax
# À partir d'ici jourAnnee = jours écoulés cette saison

# On effectue exactement la même opération pour le nombre total de jours cette saison :
# on prend simplement la date du solstice d'été
scoreboard players operation #tempsTemp jourAnneeMax = #equinoxeAutomne jourAnnee
scoreboard players operation #tempsTemp jourAnneeMax -= #solsticeEte jourAnnee
execute unless score #solsticeEte dateAnnee = #equinoxeAutomne dateAnnee run scoreboard players operation #tempsTemp jourAnneeMax += #temps jourAnneeMax
# À partir d'ici jourAnneeMax = jours totaux cette saison

# jourAnnee = 500 * (joursEcoules / joursTotaux) + 500
scoreboard players set #tempsTemp temp 500
scoreboard players operation #tempsTemp jourAnnee *= #tempsTemp temp
scoreboard players operation #tempsTemp jourAnnee /= #tempsTemp jourAnneeMax
scoreboard players operation #tempsTemp jourAnnee += #tempsTemp temp
# #sin temp = 1000 * sin(jourAnnee)
#			= 1000 * sin((joursEcoules / joursTotaux) * pi/2)
scoreboard players operation #sin temp = #tempsTemp jourAnnee
function tdh:math/sin/1000

# Maintenant qu'on a notre proportion, on multiplie chaque timing calculé par cette valeur
scoreboard players operation #tempsTemp dayTicks *= #sin temp
scoreboard players operation #tempsTemp nightTicks *= #sin temp
scoreboard players operation #tempsTemp sunsetTicks *= #sin temp
scoreboard players operation #tempsTemp sunriseTicks *= #sin temp
scoreboard players set #tempsTemp temp 1000
scoreboard players operation #tempsTemp dayTicks /= #tempsTemp temp
scoreboard players operation #tempsTemp nightTicks /= #tempsTemp temp
scoreboard players operation #tempsTemp sunsetTicks /= #tempsTemp temp
scoreboard players operation #tempsTemp sunriseTicks /= #tempsTemp temp
# On peut désormais ajouter ces valeurs à celles de le solstice pour obtenir celles de cette journée
scoreboard players operation #temps dayTicks += #tempsTemp dayTicks
scoreboard players operation #temps nightTicks += #tempsTemp nightTicks
scoreboard players operation #temps sunsetTicks += #tempsTemp sunsetTicks
scoreboard players operation #temps sunriseTicks += #tempsTemp sunriseTicks


# On réinitialise les variables de travail
scoreboard players reset #tempsTemp
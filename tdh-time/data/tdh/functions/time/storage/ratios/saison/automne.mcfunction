# L'automne est la saison n°3 et correspond à (pi, 3*pi/2) sur la courbe sin

# On enregistre comme valeur par défaut celles de l'équinoxe précédent
scoreboard players operation #temps dayTicks = #equinoxe dayTicks
scoreboard players operation #temps nightTicks = #equinoxe nightTicks
scoreboard players operation #temps sunsetTicks = #equinoxe sunsetTicks
scoreboard players operation #temps sunriseTicks = #equinoxe sunriseTicks
# On enregistre dans #tempsTemp la différence entre l'équinoxe précédent et le solstice suivant
scoreboard players operation #tempsTemp dayTicks = #solsticeHiver dayTicks
scoreboard players operation #tempsTemp dayTicks -= #equinoxe dayTicks
scoreboard players operation #tempsTemp nightTicks = #solsticeHiver nightTicks
scoreboard players operation #tempsTemp nightTicks -= #equinoxe nightTicks
scoreboard players operation #tempsTemp sunsetTicks = #solsticeHiver sunsetTicks
scoreboard players operation #tempsTemp sunsetTicks -= #equinoxe sunsetTicks
scoreboard players operation #tempsTemp sunriseTicks = #solsticeHiver sunriseTicks
scoreboard players operation #tempsTemp sunriseTicks -= #equinoxe sunriseTicks

# On calcule à présent la proportion de la saison déjà écoulée
# On part de x=0 et on va jusqu'à x=500 (qui correspond à pi/2 avec notre niveau de précision)
# Donc on multiplie 500 par le nombre de jours écoulés, et on divise par le nombre total de jours cette saison

# On calcule le nombre de jours écoulés cette saison
scoreboard players operation #tempsTemp jourAnnee = #temps jourAnnee
# On retranche la date de début de l'automne, (seulement si on est la même année que ledit début)
#			(en pratique on ne fait pas le test car on fera la même opération dans le cas contraire)
# Dans ce cas : jourAnnee = jourAnnee - debutSaison
scoreboard players operation #tempsTemp jourAnnee -= #equinoxeAutomne jourAnnee
# Dans le cas contraire, on AJOUTE la différence entre le début de saison et la fin de l'année
# Par ex l'hiver commence le 21 décembre, si on est le 10 janvier, on aura 20j (31-21 + 10)
# Dans ce cas : jourAnnee = (jourAnnee-debutSaison) + jourAnneeMax
#						  = jourAnnee + (jourAnneeMax - debutSaison)
execute unless score #equinoxeAutomne dateAnnee = #temps dateAnnee run scoreboard players operation #tempsTemp jourAnnee += #temps jourAnneeMax
# À partir d'ici jourAnnee = jours écoulés cette saison

# On effectue exactement la même opération pour le nombre total de jours cette saison :
# on prend simplement la date du solstice d'hiver
scoreboard players operation #tempsTemp jourAnneeMax = #solsticeHiver jourAnnee
scoreboard players operation #tempsTemp jourAnneeMax -= #equinoxeAutomne jourAnnee
execute unless score #equinoxeAutomne dateAnnee = #solsticeHiver dateAnnee run scoreboard players operation #tempsTemp jourAnneeMax += #temps jourAnneeMax
# À partir d'ici jourAnneeMax = jours totaux cette saison

# jourAnnee = 500 * (joursEcoules / joursTotaux)
scoreboard players set #tempsTemp temp 500
scoreboard players operation #tempsTemp jourAnnee *= #tempsTemp temp
scoreboard players operation #tempsTemp jourAnnee /= #tempsTemp jourAnneeMax
# #sin temp = 1000 * sin(jourAnnee)
#			= 1000 * sin((joursEcoules / joursTotaux) * pi/2)
scoreboard players operation #sin temp = #tempsTemp jourAnnee
function tdh:math/sin/1000

# Maintenant qu'on a notre proportion, on multiplie chaque timing calculé par cette valeur
scoreboard players operation #tempsTemp dayTicks *= #sin temp
scoreboard players operation #tempsTemp nightTicks *= #sin temp
scoreboard players operation #tempsTemp sunsetTicks *= #sin temp
scoreboard players operation #tempsTemp sunriseTicks *= #sin temp
scoreboard players set #tempsTemp temp 1000
scoreboard players operation #tempsTemp dayTicks /= #tempsTemp temp
scoreboard players operation #tempsTemp nightTicks /= #tempsTemp temp
scoreboard players operation #tempsTemp sunsetTicks /= #tempsTemp temp
scoreboard players operation #tempsTemp sunriseTicks /= #tempsTemp temp
# On peut désormais ajouter ces valeurs à celles de l'équinoxe pour obtenir celles de cette journée
scoreboard players operation #temps dayTicks += #tempsTemp dayTicks
scoreboard players operation #temps nightTicks += #tempsTemp nightTicks
scoreboard players operation #temps sunsetTicks += #tempsTemp sunsetTicks
scoreboard players operation #temps sunriseTicks += #tempsTemp sunriseTicks


# On réinitialise les variables de travail
scoreboard players reset #tempsTemp
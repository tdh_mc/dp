# On enregistre la durée du jour et de la nuit
scoreboard players operation #temps dayTicks = #solsticeEte dayTicks
scoreboard players operation #temps nightTicks = #solsticeEte nightTicks
scoreboard players operation #temps sunsetTicks = #solsticeEte sunsetTicks
scoreboard players operation #temps sunriseTicks = #solsticeEte sunriseTicks

# On enregistre la nouvelle saison
scoreboard players set #temps saison 2

# On recalcule la date de l'équinoxe d'automne
function tdh:time/storage/jour_annee/equinoxe_automne

# On enregistre le fait qu'on a calculé les durées jour/nuit
scoreboard players set #temps temp 0
# On enregistre la durée du jour et de la nuit
scoreboard players operation #temps dayTicks = #equinoxe dayTicks
scoreboard players operation #temps nightTicks = #equinoxe nightTicks
scoreboard players operation #temps sunsetTicks = #equinoxe sunsetTicks
scoreboard players operation #temps sunriseTicks = #equinoxe sunriseTicks

# On enregistre la nouvelle saison
scoreboard players set #temps saison 1

# On recalcule la date du solstice d'été
function tdh:time/storage/jour_annee/solstice_ete


# On enregistre le fait qu'on a calculé les durées jour/nuit
scoreboard players set #temps temp 0
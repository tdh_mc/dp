# On enregistre la durée du jour et de la nuit
scoreboard players operation #temps dayTicks = #solsticeHiver dayTicks
scoreboard players operation #temps nightTicks = #solsticeHiver nightTicks
scoreboard players operation #temps sunsetTicks = #solsticeHiver sunsetTicks
scoreboard players operation #temps sunriseTicks = #solsticeHiver sunriseTicks

# On enregistre la nouvelle saison
scoreboard players set #temps saison 4

# On recalcule la date de l'équinoxe de printemps
function tdh:time/storage/jour_annee/equinoxe_printemps

# On enregistre le fait qu'on a calculé les durées jour/nuit
scoreboard players set #temps temp 0
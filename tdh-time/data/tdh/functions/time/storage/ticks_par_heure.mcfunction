# On recalcule le nombre de ticks par heure
scoreboard players operation #temps ticksPerHour = #temps fullDayTicks
scoreboard players operation #temps ticksPerHour /= #temps hoursPerDay


# On recalcule les horaires des différents triggers de la journée
# 2h = le nombre de ticks par jour divisé par 12 (on prend "2h" de manière symbolique comme 1/12e de la journée, et pas précisément comme "2h", puisqu'on peut paramétrer le nombre d'heures par jour)
scoreboard players set #2h temp 12
scoreboard players operation #2h currentTdhTick = #temps fullDayTicks
scoreboard players operation #2h currentTdhTick /= #2h temp
scoreboard players reset #2h temp

# 4h, 6h, 8h... sont tous des multiples de la valeur précédente
scoreboard players operation #4h currentTdhTick = #2h currentTdhTick
scoreboard players operation #4h currentTdhTick += #2h currentTdhTick

scoreboard players operation #6h currentTdhTick = #4h currentTdhTick
scoreboard players operation #6h currentTdhTick += #2h currentTdhTick

scoreboard players operation #8h currentTdhTick = #6h currentTdhTick
scoreboard players operation #8h currentTdhTick += #2h currentTdhTick

scoreboard players operation #10h currentTdhTick = #8h currentTdhTick
scoreboard players operation #10h currentTdhTick += #2h currentTdhTick

scoreboard players operation #12h currentTdhTick = #10h currentTdhTick
scoreboard players operation #12h currentTdhTick += #2h currentTdhTick

scoreboard players operation #14h currentTdhTick = #12h currentTdhTick
scoreboard players operation #14h currentTdhTick += #2h currentTdhTick

scoreboard players operation #16h currentTdhTick = #14h currentTdhTick
scoreboard players operation #16h currentTdhTick += #2h currentTdhTick

scoreboard players operation #18h currentTdhTick = #16h currentTdhTick
scoreboard players operation #18h currentTdhTick += #2h currentTdhTick

scoreboard players operation #20h currentTdhTick = #18h currentTdhTick
scoreboard players operation #20h currentTdhTick += #2h currentTdhTick

scoreboard players operation #22h currentTdhTick = #20h currentTdhTick
scoreboard players operation #22h currentTdhTick += #2h currentTdhTick
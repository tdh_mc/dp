# Par défaut, il n'y a pas de date spéciale aujourd'hui
data modify storage tdh:datetime date.special set value {}

# Bonne année !
execute if score #temps dateJour matches 1 if score #temps dateMois matches 1 run function tdh:time/storage/jour_special/nouvel_an

# Solstices / Equinoxes
execute if score #temps dateJour = #equinoxePrintemps dateJour if score #temps dateMois = #equinoxePrintemps dateMois run function tdh:time/storage/jour_special/equinoxe_printemps
execute if score #temps dateJour = #solsticeEte dateJour if score #temps dateMois = #solsticeEte dateMois run function tdh:time/storage/jour_special/solstice_ete
execute if score #temps dateJour = #equinoxeAutomne dateJour if score #temps dateMois = #equinoxeAutomne dateMois run function tdh:time/storage/jour_special/equinoxe_automne
execute if score #temps dateJour = #solsticeHiver dateJour if score #temps dateMois = #solsticeHiver dateMois run function tdh:time/storage/jour_special/solstice_hiver

# Fêtes supplémentaires (ajoutées par d'autres DP)
function #tdh:time/jour_special_test
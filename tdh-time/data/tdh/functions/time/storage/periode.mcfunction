# On calcule la période de la journée à partir du tick TDH
scoreboard players set #temps timeOfDay 3
execute if score #temps currentTdhTick >= #temps sunriseBegin run scoreboard players set #temps timeOfDay 4
execute if score #temps currentTdhTick >= #temps dayBegin run scoreboard players set #temps timeOfDay 1
execute if score #temps currentTdhTick >= #temps sunsetBegin run scoreboard players set #temps timeOfDay 2
execute if score #temps currentTdhTick >= #temps nightBegin run scoreboard players set #temps timeOfDay 3
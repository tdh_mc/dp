# On compte le nombre de joueurs en ligne
execute store result score #temps playersOnline if entity @a

# Le temps n'avance que s'il y a au moins un joueur en ligne et que le temps n'est pas désactivé
execute if score #temps playersOnline matches 1.. if score #temps on matches 1 run function tdh:time/tdh_tick
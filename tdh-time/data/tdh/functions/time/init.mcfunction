function tdh:time/init/variables
function tdh:time/init/duree_jour_nuit
function tdh:time/init/subdivisions
function tdh:time/init/fetes

# Si on n'a pas encore de date, on définit le calendrier au 1er solstice d'automne
execute unless score #temps dateJour matches 1.. run function tdh:time/init/date

# On réinitialise l'heure au lever du soleil si on n'en avait pas encore
execute unless score #temps currentTdhTick matches 0.. run function tdh:time/reset_heure

# On vérifie que la date actuelle est consistante
function tdh:time/date_check
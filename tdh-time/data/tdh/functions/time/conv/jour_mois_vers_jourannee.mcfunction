# Paramètres de la fonction :
#  - #tempsConv dateJour
#  - #tempsConv dateMois
#  - #tempsConv anneeBissextile (1 ou 0)
# Résultat de la fonction :
#  - #tempsConv jourAnnee, l'ID de jour correspondant à (dateJour, dateMois) pour une année (bissextile ou non selon le 3ème paramètre)

# Par exemple pour jour=15 et mois=2 on aura comme résultat 31+15=46

scoreboard players operation #tempsConv jourAnnee = #tempsConv dateJour
execute if score #tempsConv dateMois matches 2.. run function tdh:time/conv/jour_mois_vers_jourannee/2
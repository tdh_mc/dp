# Cette fonction convertit jour/mois/annee en idJour
# Arguments : #tempsConv dateJour / dateMois / dateAnnee
# Résultat : #tempsConv idJour

# On calcule le jourAnnee à partir du jour et du mois
function tdh:time/conv/is_bissextile
function tdh:time/conv/jour_mois_vers_jourannee

# On calcule l'idJour par récursion, tant qu'il reste des années dans temp
scoreboard players operation #tempsConv temp = #tempsConv dateAnnee
scoreboard players operation #tempsConv idJour = #tempsConv jourAnnee
execute if score #tempsConv temp matches 2.. run function tdh:time/conv/jour_mois_annee_vers_idjour/next
# On retranche 100 ans de temp, et on ajoute 76 ans + 24 bissextiles
scoreboard players remove #tempsConv temp 100

scoreboard players operation #tempsConv temp2 = #temps jourAnneeMax
scoreboard players set #tempsConv temp3 76
scoreboard players operation #tempsConv temp2 *= #tempsConv temp3
scoreboard players operation #tempsConv idJour += #tempsConv temp2

scoreboard players operation #tempsConv temp2 = #anneeBissextile jourAnneeMax
scoreboard players set #tempsConv temp3 24
scoreboard players operation #tempsConv temp2 *= #tempsConv temp3
scoreboard players operation #tempsConv idJour += #tempsConv temp2
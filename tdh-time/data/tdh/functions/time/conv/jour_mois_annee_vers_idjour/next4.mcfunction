# On retranche 4 ans de temp, et on ajoute 3 années et 1 année bissextile
scoreboard players remove #tempsConv temp 4

scoreboard players operation #tempsConv temp2 = #temps jourAnneeMax
scoreboard players set #tempsConv temp3 3
scoreboard players operation #tempsConv temp2 *= #tempsConv temp3
scoreboard players operation #tempsConv idJour += #tempsConv temp2

scoreboard players operation #tempsConv idJour += #anneeBissextile jourAnneeMax
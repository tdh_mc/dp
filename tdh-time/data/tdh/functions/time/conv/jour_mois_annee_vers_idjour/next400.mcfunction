# On retranche 400 ans de temp, et on ajoute 303 ans + 97 bissextiles
scoreboard players remove #tempsConv temp 400

scoreboard players operation #tempsConv temp2 = #temps jourAnneeMax
scoreboard players set #tempsConv temp3 303
scoreboard players operation #tempsConv temp2 *= #tempsConv temp3
scoreboard players operation #tempsConv idJour += #tempsConv temp2

scoreboard players operation #tempsConv temp2 = #anneeBissextile jourAnneeMax
scoreboard players set #tempsConv temp3 97
scoreboard players operation #tempsConv temp2 *= #tempsConv temp3
scoreboard players operation #tempsConv idJour += #tempsConv temp2
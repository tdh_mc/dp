# Il reste moins de 4 années dans la variable temp
# On ajoute donc simplement le nombre d'années restant, qui ne peuvent pas être bissextiles
scoreboard players operation #tempsConv temp8 = #temps jourAnneeMax
scoreboard players remove #tempsConv temp 1
scoreboard players operation #tempsConv temp8 *= #tempsConv temp
scoreboard players operation #tempsConv idJour += #tempsConv temp8
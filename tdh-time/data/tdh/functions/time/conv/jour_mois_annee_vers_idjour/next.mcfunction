# Cette fonction prend un argument, temp, qui représente un nombre d'années,
# et calcule le nombre de jours contenus dans toutes les années écoulées depuis l'an 1.
# Le résultat se trouvera dans #tempsConv idJour

# Requiert qu'on ait déjà initialisé #tempsConv idJour avec une valeur initiale

# S'il y a moins de 100, mais plus de 4 années dans la variable, on ajoute 4 ans et 1 année bissextile
execute if score #tempsConv temp matches 5..100 run function tdh:time/conv/jour_mois_annee_vers_idjour/next4
# S'il y a moins de 400, mais plus de 100 années dans la variable, on ajoute 100 ans et 24 années bissextiles
execute if score #tempsConv temp matches 101..400 run function tdh:time/conv/jour_mois_annee_vers_idjour/next100
# S'il y a plus de 400 années dans la variable, on ajoute 400 ans et 97 années bissextiles
execute if score #tempsConv temp matches 401.. run function tdh:time/conv/jour_mois_annee_vers_idjour/next400

# S'il y a moins de 4 années dans la variable, on ajoute ce qui reste et on s'arrête là
execute if score #tempsConv temp matches ..4 run function tdh:time/conv/jour_mois_annee_vers_idjour/end
# S'il reste au moins 4 années, on continue à récurser
execute if score #tempsConv temp matches 5.. run function tdh:time/conv/jour_mois_annee_vers_idjour/next

# TODO :
# Tu as besoin de tout ça pour faire une fonction de RECALCUL de l'idJour qui le recalculera automatiquement (depuis date_check) et permettra d'avoir un idJour consistant quand on change la date actuelle depuis la config.
# Détermine si une année est bissextile ou non
# L'année est passée dans la variable #tempsConv dateAnnee
# La variable #tempsConv anneeBissextile vaudra 1 ssi l'année est bissextile
scoreboard players set #tempsConv anneeBissextile 0

# Mod4
scoreboard players operation #tempsConv temp2 = #tempsConv dateAnnee
scoreboard players set #tempsConv temp 4
scoreboard players operation #tempsConv temp2 %= #tempsConv temp
# Si l'année est divisible par 4, elle est bissextile
execute if score #tempsConv temp2 matches 0 run scoreboard players set #tempsConv anneeBissextile 1

# Mod100
execute if score #tempsConv anneeBissextile matches 1 run scoreboard players operation #tempsConv temp2 = #tempsConv dateAnnee
execute if score #tempsConv anneeBissextile matches 1 run scoreboard players set #tempsConv temp 100
execute if score #tempsConv anneeBissextile matches 1 run scoreboard players operation #tempsConv temp2 %= #tempsConv temp
# Si l'année bissextile est divisible par 100, elle n'est pas bissextile
# Mais on la passe à 2 au lieu de 0 pour pouvoir faire l'itération suivante
execute if score #tempsConv anneeBissextile matches 1 if score #tempsConv temp2 matches 0 run scoreboard players set #tempsConv anneeBissextile 2

# Mod400
execute if score #tempsConv anneeBissextile matches 2 run scoreboard players operation #tempsConv temp2 = #tempsConv dateAnnee
execute if score #tempsConv anneeBissextile matches 2 run scoreboard players set #tempsConv temp 400
execute if score #tempsConv anneeBissextile matches 2 run scoreboard players operation #tempsConv temp2 %= #tempsConv temp
# Si l'année précédemment considérée comme bissextile, puis non bissextile est divisible par 400, elle est bien bissextile
execute if score #tempsConv anneeBissextile matches 2 if score #tempsConv temp2 matches 0 run scoreboard players set #tempsConv anneeBissextile 1

# Si elle n'est pas divisible par 400, elle est bien non bissextile, et on passe la variable de 2 à 0
execute if score #tempsConv anneeBissextile matches 2 run scoreboard players set #tempsConv anneeBissextile 0
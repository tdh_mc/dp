execute unless score #tempsConv anneeBissextile matches 1.. store result score #tempsConv temp run data get storage tdh:datetime index.mois[10].jours
execute if score #tempsConv anneeBissextile matches 1.. store result score #tempsConv temp run data get storage tdh:datetime index.mois[10].joursBis
scoreboard players operation #tempsConv jourAnnee += #tempsConv temp

execute if score #tempsConv dateMois matches 13.. run function tdh:time/conv/jour_mois_vers_jourannee/13
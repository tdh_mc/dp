execute unless score #tempsConv anneeBissextile matches 1.. store result score #tempsConv temp run data get storage tdh:datetime index.mois[5].jours
execute if score #tempsConv anneeBissextile matches 1.. store result score #tempsConv temp run data get storage tdh:datetime index.mois[5].joursBis
scoreboard players operation #tempsConv jourAnnee += #tempsConv temp

execute if score #tempsConv dateMois matches 8.. run function tdh:time/conv/jour_mois_vers_jourannee/8
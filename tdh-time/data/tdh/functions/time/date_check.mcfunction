# On vérifie que la date actuelle est possible, et on la corrige le cas échéant

# On prend le modulo du jour de la semaine pour être sûr qu'il est possible
scoreboard players operation #temps jourSemaine %= #temps jourSemaineMax
# Si le jour de la semaine vaut 0, on le définit au max
execute if score #temps jourSemaine matches 0 run scoreboard players operation #temps jourSemaine = #temps jourSemaineMax
# On force le re-storage du jour de la semaine
function tdh:time/storage/jour_semaine

# On recalcule le statut bissextile de l'année
function tdh:time/storage/annee

# On calcule si le mois actuel est possible
scoreboard players operation #temps temp = #temps dateMois
scoreboard players remove #temps temp 1
scoreboard players operation #temps temp2 = #temps temp
scoreboard players operation #temps temp /= #temps dateMoisMax
execute if score #temps temp matches 1.. run scoreboard players operation #temps temp2 %= #temps dateMoisMax
execute if score #temps temp matches 1.. run scoreboard players add #temps temp2 1
# À partir d'ici, temp = le nombre d'années à ajouter et
#				  temp2 = le nouveau mois
execute if score #temps temp matches 1.. run scoreboard players operation #temps dateAnnee += #temps temp
execute if score #temps temp matches 1.. run scoreboard players operation #temps dateMois = #temps temp2
# Si on modifie l'année, on recalcule aussi son statut bissextile ou non
execute if score #temps temp matches 1.. run function tdh:time/storage/annee
scoreboard players reset #temps temp
scoreboard players reset #temps temp2
# On force le re-storage du mois
function tdh:time/storage/mois

# On calcule si le jour du mois est possible, et on le décale si nécessaire
execute if score #temps dateJour > #temps dateJourMax run function tdh:time/date_check/prochain_mois

# On force le re-storage du jour du mois (1er/autre)
function tdh:time/storage/jour_mois

# On recalcule le jourAnnee
function tdh:time/storage/jour_annee

# On recalcule l'ID jour
function tdh:time/storage/id_jour

# On recalcule la saison
function tdh:time/storage/saison

# On recalcule les jourAnnee de début de saison
function tdh:time/storage/jour_annee/equinoxe_printemps
function tdh:time/storage/jour_annee/solstice_ete
function tdh:time/storage/jour_annee/equinoxe_automne
function tdh:time/storage/jour_annee/solstice_hiver

# On vérifie si on est un jour spécial ou non
function tdh:time/storage/jour_special


# On recalcule le nombre de ticks par heure
function tdh:time/storage/ticks_par_heure
# On recalcule l'heure
function tdh:time/storage/heure
# On recalcule la période de la journée
function tdh:time/storage/periode


# On vérifie les ratios jour/nuit
function tdh:time/storage/ratios
# On appelle cette fonction après avoir défini l'objectif #store idJour
# Elle décompose ce résultat en jour/mois/année (dans #store dateJour/Mois/Annee)
# Le résultat sera stocké sous forme de NBT dans le storage "tdh:store day"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, puisque les scores correspondants changeront lors du prochain appel à cette fonction

# On calcule d'abord l'année
function tdh:store/day/annee
# On récupère un nombre d'années (dans dateAnnee) et la bissextilité ou non de l'année actuelle
# Le nombre de jours restants est indiqué dans la variable #store dateMois
function tdh:store/day/mois
# On a à la sortie les variables #dateAnnee, #dateMois, #dateJour

# On enregistre dans le storage le nom du mois calculé
function tdh:store/mois

# On enregistre l'affichage correct dans le storage
data modify storage tdh:store day set value '[{"score":{"name":"#store","objective":"dateJour"}},{"text":" "},{"nbt":"mois.min","storage":"tdh:store"},{"text":" "},{"score":{"name":"#store","objective":"dateAnnee"}}]'



# On réinitialise toutes les variables temporaires
scoreboard players reset #store temp
scoreboard players reset #store temp2
scoreboard players reset #store temp3
# On appelle cette fonction après avoir défini l'objectif #store idJour
# Elle décompose ce résultat en jour/mois (dans #store dateJour/Mois)
# Le résultat sera stocké sous forme de NBT dans le storage "tdh:store day"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, puisque les scores correspondants changeront lors du prochain appel à cette fonction
tellraw @a[tag=StoreDebugLog] [{"text":"","color":"gray"},{"text":"--- Conversion d'un idJour en date ---\n","bold":"true"},{"text":"idJour : "},{"score":{"name":"#store","objective":"idJour"},"color":"gold"}]

# On calcule d'abord l'année
# (on ne l'affiche pas, mais on en a tout de même besoin pour savoir quel jour on est)
function tdh:store/day/annee
# On récupère un nombre d'années (dans dateAnnee) et la bissextilité ou non de l'année actuelle
# Le nombre de jours restants est indiqué dans la variable #store dateMois
function tdh:store/day/mois
# On a à la sortie les variables #dateAnnee, #dateMois, #dateJour


# On enregistre l'affichage correct dans le storage
data modify storage tdh:store day set value '[{"score":{"name":"#store","objective":"dateJour"}},{"text":"/"},{"score":{"name":"#store","objective":"dateMois"}}]'



# On réinitialise toutes les variables temporaires
scoreboard players reset #store temp
scoreboard players reset #store temp2
scoreboard players reset #store temp3
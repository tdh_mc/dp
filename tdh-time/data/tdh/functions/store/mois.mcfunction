# On appelle cette fonction après avoir défini l'objectif #store dateMois
# Les données du mois en question seront stockées sous forme de NBT dans le storage "tdh:store mois"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, et en principe jusqu'au prochain appel à cette fonction

# On vide le storage
data remove storage tdh:store mois

# On enregistre l'affichage correct dans le storage, selon le paramètre dateMois (seulement si le mois correspondant existe)
execute if score #store dateMois matches 1 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[0]
execute if score #store dateMois matches 2 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[1]
execute if score #store dateMois matches 3 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[2]
execute if score #store dateMois matches 4 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[3]
execute if score #store dateMois matches 5 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[4]
execute if score #store dateMois matches 6 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[5]
execute if score #store dateMois matches 7 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[6]
execute if score #store dateMois matches 8 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[7]
execute if score #store dateMois matches 9 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[8]
execute if score #store dateMois matches 10 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[9]
execute if score #store dateMois matches 11 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[10]
execute if score #store dateMois matches 12 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[11]
execute if score #store dateMois matches 13 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[12]
execute if score #store dateMois matches 14 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[13]
execute if score #store dateMois matches 15 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[14]
execute if score #store dateMois matches 16 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[15]
execute if score #store dateMois matches 17 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[16]
execute if score #store dateMois matches 18 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[17]
execute if score #store dateMois matches 19 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[18]
execute if score #store dateMois matches 20 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[19]
execute if score #store dateMois matches 21 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[20]
execute if score #store dateMois matches 22 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[21]
execute if score #store dateMois matches 23 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[22]
execute if score #store dateMois matches 24 run data modify storage tdh:store mois set from storage tdh:datetime index.mois[23]

# Si on n'a pas trouvé de mois correct, on l'indique expressément
execute unless data storage tdh:store mois run data modify storage tdh:store mois set value {deDu:"de ",min:"<ERREUR>",maj:"<ERREUR>",minS:"<ERR>",majS:"<ERR>",jours:-1,joursBis:-1}
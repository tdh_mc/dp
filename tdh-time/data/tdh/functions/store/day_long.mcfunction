# On appelle cette fonction après avoir défini l'objectif #store idJour
# Elle décompose ce résultat en jour/mois/année (dans #store dateJour/Mois/Annee)
# Le résultat sera stocké sous forme de NBT dans le storage "tdh:store day"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, puisque les scores correspondants changeront lors du prochain appel à cette fonction

# On calcule d'abord l'année
function tdh:store/day/annee
# On récupère un nombre d'années (dans dateAnnee) et la bissextilité ou non de l'année actuelle
# Le nombre de jours restants est indiqué dans la variable #store dateMois
function tdh:store/day/mois
# On a à la sortie les variables #dateAnnee, #dateMois, #dateJour


# On enregistre l'affichage correct dans le storage
execute if score #store dateJour matches ..9 if score #store dateMois matches ..9 run data modify storage tdh:store day set value '[{"text":"0"},{"score":{"name":"#store","objective":"dateJour"}},{"text":"/0"},{"score":{"name":"#store","objective":"dateMois"}},{"text":"/"},{"score":{"name":"#store","objective":"dateAnnee"}}]'
execute if score #store dateJour matches ..9 if score #store dateMois matches 10.. run data modify storage tdh:store day set value '[{"text":"0"},{"score":{"name":"#store","objective":"dateJour"}},{"text":"/"},{"score":{"name":"#store","objective":"dateMois"}},{"text":"/"},{"score":{"name":"#store","objective":"dateAnnee"}}]'
execute if score #store dateJour matches 10.. if score #store dateMois matches ..9 run data modify storage tdh:store day set value '[{"score":{"name":"#store","objective":"dateJour"}},{"text":"/0"},{"score":{"name":"#store","objective":"dateMois"}},{"text":"/"},{"score":{"name":"#store","objective":"dateAnnee"}}]'
execute if score #store dateJour matches 10.. if score #store dateMois matches 10.. run data modify storage tdh:store day set value '[{"score":{"name":"#store","objective":"dateJour"}},{"text":"/"},{"score":{"name":"#store","objective":"dateMois"}},{"text":"/"},{"score":{"name":"#store","objective":"dateAnnee"}}]'



# On réinitialise toutes les variables temporaires
scoreboard players reset #store temp
scoreboard players reset #store temp2
scoreboard players reset #store temp3
# Cette fonction prend 2 arguments:
# - dateMois qui contient un idJour <= 366 correspondant à un jour et un mois,
# - anneeBissextile qui détermine si l'année est bissextile ou non
# Elle calcule en sortie l'id du mois (dateMois) et du jour (dateJour)

scoreboard players operation #store temp = #store dateMois
scoreboard players set #store dateJour 0
scoreboard players set #store dateMois 1
function tdh:store/day/mois/debut
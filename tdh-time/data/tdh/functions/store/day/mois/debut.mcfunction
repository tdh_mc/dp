# Cette fonction prend 2 arguments, temp et anneeBissextile,
# et calcule le nombre de mois à partir du 1er janvier contenus dans temp ainsi que le reste (jour du mois)
# respectivement dans #store dateMois (initialisé à 1) et #store dateJour (initialisé à 0)

# On récupère dans des variables temporaires les données du mois actuel (indiqué par #store dateMois)
execute if score #store dateMois matches 1 run function tdh:store/day/mois/1
execute if score #store dateMois matches 2 run function tdh:store/day/mois/2
execute if score #store dateMois matches 3 run function tdh:store/day/mois/3
execute if score #store dateMois matches 4 run function tdh:store/day/mois/4
execute if score #store dateMois matches 5 run function tdh:store/day/mois/5
execute if score #store dateMois matches 6 run function tdh:store/day/mois/6
execute if score #store dateMois matches 7 run function tdh:store/day/mois/7
execute if score #store dateMois matches 8 run function tdh:store/day/mois/8
execute if score #store dateMois matches 9 run function tdh:store/day/mois/9
execute if score #store dateMois matches 10 run function tdh:store/day/mois/10
execute if score #store dateMois matches 11 run function tdh:store/day/mois/11
execute if score #store dateMois matches 12 run function tdh:store/day/mois/12
execute if score #store dateMois matches 13 run function tdh:store/day/mois/13
execute if score #store dateMois matches 14 run function tdh:store/day/mois/14
execute if score #store dateMois matches 15 run function tdh:store/day/mois/15
execute if score #store dateMois matches 16 run function tdh:store/day/mois/16
execute if score #store dateMois matches 17 run function tdh:store/day/mois/17
execute if score #store dateMois matches 18 run function tdh:store/day/mois/18
execute if score #store dateMois matches 19 run function tdh:store/day/mois/19
execute if score #store dateMois matches 20 run function tdh:store/day/mois/20
execute if score #store dateMois matches 21 run function tdh:store/day/mois/21
execute if score #store dateMois matches 22 run function tdh:store/day/mois/22
execute if score #store dateMois matches 23 run function tdh:store/day/mois/23
execute if score #store dateMois matches 24 run function tdh:store/day/mois/24

# À partir d'ici on a #store dateJourMax = jour maximum de ce mois-ci (selon le statut anneeBissextile ou non)
# Si le nombre de jours restants (temp) est inférieur ou égal à ce maximum, on termine le calcul
execute if score #store temp <= #store dateJourMax run function tdh:store/day/mois/end
# Si le nombre de jours restants (temp) est supérieur à ce maximum, on passe au mois suivant
execute if score #store temp > #store dateJourMax run function tdh:store/day/mois/next

tellraw @a[tag=StoreDebugLog] [{"text":"-- Calcul du jour --\n","color":"gray"},{"text":"dateJour = "},{"score":{"name":"#store","objective":"dateJour"},"color":"gold"},{"text":" ; dateMois = "},{"score":{"name":"#store","objective":"dateMois"},"color":"gold"},{"text":" ; dateAnnee = "},{"score":{"name":"#store","objective":"dateAnnee"},"color":"yellow"}]
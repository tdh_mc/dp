# On incrémente le numéro de mois et on décrémente le nombre de jours restant à calculer (temp)
scoreboard players add #store dateMois 1
scoreboard players operation #store temp -= #store dateJourMax

# On continue le calcul sur le reste du mois
function tdh:store/day/mois/debut
# Il reste moins de 4 années dans la variable temp
# On calcule la durée de 3 années non bissextiles
scoreboard players operation #store temp8 = #temps jourAnneeMax
scoreboard players set #store temp9 3
scoreboard players operation #store temp8 *= #store temp9
# On vérifie si la 4ème année est l'actuelle ; si oui, elle est peut-être bissextile et on vérifiera plus loin
execute if score #store temp > #store temp8 run scoreboard players set #store anneeBissextile 1

# On applique les années
scoreboard players operation #store temp7 = #store temp
scoreboard players remove #store temp7 1
scoreboard players operation #store temp7 /= #temps jourAnneeMax
scoreboard players operation #store dateAnnee += #store temp7

# temp7 contient encore à ce stade le nombre d'années :
# on l'utilise pour retirer les années complètes de temp, et garder uniquement les jours/mois
scoreboard players operation #store temp7 *= #temps jourAnneeMax
scoreboard players operation #store temp -= #store temp7

# Si l'année est un multiple de 100, on annule la bissextilité
execute if score #store anneeBissextile matches 1 run function tdh:store/day/annee/end_check

# Une fois tout ceci calculé, on enregistre le nombre de jours restants dans dateMois
scoreboard players operation #store dateMois = #store temp


tellraw @a[tag=StoreDebugLog] [{"text":"","color":"gray"},{"text":"-- Fin du calcul de l'année --\n","color":"green","bold":"true"},{"text":"idJour = "},{"score":{"name":"#store","objective":"temp"},"color":"gold"},{"text":" ; dateAnnee = "},{"score":{"name":"#store","objective":"dateAnnee"},"color":"yellow"}]
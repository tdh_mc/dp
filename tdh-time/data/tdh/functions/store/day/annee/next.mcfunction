# Cette fonction prend un argument, temp,
# et calcule le nombre d'années contenues dans temp ainsi que le nombre d'années bissextiles
# respectivement dans #store dateAnnee et #store anneeBissextile, toutes 2 déjà initialisées à 0

# 3 paramètres sont initialisés par la fonction parente avant le début de la récursion :
# #store temp2 = la durée en jours d'une "unité" (4 années dont 1 bissextile)
# #store temp3 = la durée en jours de 25 "unités" (100 années dont 24 bissextiles)
# #store temp4 = la durée en jours de 100 "unités" (400 années dont 97 bissextiles)

tellraw @a[tag=StoreDebugLog] [{"text":"-- Itération de l'année --\n","color":"gray"},{"text":"idJour = "},{"score":{"name":"#store","objective":"temp"},"color":"gold"},{"text":" ; dateAnnee = "},{"score":{"name":"#store","objective":"dateAnnee"},"color":"yellow"}]

# S'il y a moins de 100, mais plus de 4 années dans la variable, on ajoute 4 ans et 1 année bissextile
execute if score #store temp > #store temp2 if score #store temp <= #store temp3 run function tdh:store/day/annee/next4
# S'il y a moins de 400, mais plus de 100 années dans la variable, on ajoute 100 ans et 24 années bissextiles
execute if score #store temp > #store temp3 if score #store temp <= #store temp4 run function tdh:store/day/annee/next100
# S'il y a plus de 400 années dans la variable, on ajoute 400 ans et 97 années bissextiles
execute if score #store temp > #store temp4 run function tdh:store/day/annee/next400

# S'il y a moins de 4 années dans la variable, on ajoute ce qui reste et on s'arrête là
execute if score #store temp <= #store temp2 run function tdh:store/day/annee/end
# S'il reste au moins 4 années, on continue à récurser
execute if score #store temp > #store temp2 run function tdh:store/day/annee/next
# Cette fonction prend un argument, idJour,
# et calcule le nombre d'années contenues dans idJour ainsi que la bissextilité de l'année en cours
# respectivement dans #store dateAnnee et #store anneeBissextile

# On initialise les paramètres de la fonction
scoreboard players operation #store temp = #store idJour
scoreboard players set #store dateAnnee 1
scoreboard players set #store anneeBissextile 0

# On enregistre les thresholds des différents paliers de la fonction
# temp2 = Durée de 4 années (3 normales + 1 bissextile)
scoreboard players operation #store temp2 = #temps jourAnneeMax
scoreboard players set #store temp9 3
scoreboard players operation #store temp2 *= #store temp9
scoreboard players operation #store temp2 += #anneeBissextile jourAnneeMax

# temp3 = Durée de 100 années (76 normales + 24 bissextiles)
scoreboard players operation #store temp3 = #temps jourAnneeMax
scoreboard players set #store temp9 76
scoreboard players operation #store temp3 *= #store temp9
scoreboard players operation #store temp4 = #anneeBissextile jourAnneeMax
scoreboard players set #store temp9 24
scoreboard players operation #store temp4 *= #store temp9
scoreboard players operation #store temp3 += #store temp4

# temp4 = Durée de 400 années (303 normales + 97 bissextiles)
scoreboard players operation #store temp4 = #temps jourAnneeMax
scoreboard players set #store temp9 303
scoreboard players operation #store temp4 *= #store temp9
scoreboard players operation #store temp5 = #anneeBissextile jourAnneeMax
scoreboard players set #store temp9 97
scoreboard players operation #store temp5 *= #store temp9
scoreboard players operation #store temp4 += #store temp5

# On exécute la première itération de la récursion
function tdh:store/day/annee/next
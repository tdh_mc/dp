# On invoque une armor stand invisible pour placer aléatoirement le joueur aux alentours
summon armor_stand ~ ~ ~ {Tags:["TempPlayerPosition"],Invisible:1b,Marker:1b,NoGravity:1b}
spreadplayers ~ ~ 1 50 false @e[tag=TempPlayerPosition]

# On définit le point de spawn du joueur à la position de cette armor stand
tellraw @p[tag=pendingDeath] [{"text":"Vous allez réapparaître près de votre ","color":"gold"},{"text":"point de décès","color":"red"},{"text":".","color":"gold"}]
execute at @e[tag=TempPlayerPosition,limit=1] run spawnpoint @p[tag=pendingDeath] ~ ~ ~

# On tue l'armor stand invisible
kill @e[tag=TempPlayerPosition]

# On supprime le tag pendingDeath pour enregistrer le fait qu'on a bien défini un point de respawn
tag @p[tag=pendingDeath] remove pendingDeath
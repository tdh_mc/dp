# On sélectionne un joueur au hasard, que l'on téléporte à une position aléatoire
execute at @r run function tdh:arena/wild/begin/random_location

# On assigne les équipes
function tdh:arena/wild/begin/random_teams
tellraw @a [{"text":"Équipe Procyenne : ","color":"red"},{"selector":"@a[team=Procyens]"},{"text":"\nÉquipe Hamélienne : ","color":"gold"},{"selector":"@a[team=Hameliens]"}]

# On téléporte tous les joueurs à la position du point de référence
function tdh:arena/wild/begin/before_combat
# Cette fonction est exécutée par l'armor stand WildArena à sa propre position

# On supprime d'abord les coffres alentour
execute at @e[tag=WildArenaChest] run setblock ~ ~ ~ air
kill @e[tag=WildArenaChest]

# On décharge ensuite les chunks et on s'auto-tue
forceload remove ~ ~
tag @s remove WildArena
kill @s
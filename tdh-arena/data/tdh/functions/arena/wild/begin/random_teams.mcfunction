# On classe tous les joueurs en 2 équipes Procyens/Haméliens
team join Hameliens @r[team=!Procyens,team=!Hameliens]
team join Procyens @r[team=!Procyens,team=!Hameliens]

# S'il reste des joueurs à classer, on répète la fonction
execute if entity @p[team=!Procyens,team=!Hameliens] run function tdh:arena/wild/begin/random_teams
# On remplace le point de référence "joueur" par une armor stand
execute at @p[tag=RefPoint] run summon armor_stand ~ ~ ~ {Tags:["WildArena"],Invisible:1b,Marker:1b,NoGravity:1b,Invulnerable:1b}
tag @a[tag=RefPoint] remove RefPoint

# On forceload le chunk dans lequel se trouve le point de référence
execute at @e[tag=WildArena,limit=1] run forceload add ~ ~

# On déplace tous les joueurs à 20m au dessus du sol, regardant vers le bas
execute at @e[tag=WildArena,limit=1] run tp @a ~-10 ~15 ~-10 315 60

# On place tous les joueurs en spectateur
gamemode spectator @a

# On initialise les joueurs
scoreboard players set @a arenaDeaths 0
tag @a add InArena

# On affiche un message dans le chat
tellraw @a [{"text":"Dans ","color":"yellow"},{"text":"10","color":"red"},{"text":" secondes, chaque équipe sera téléportée dans une zone aléatoire des alentours."}]

# On schedule le début du combat
schedule function tdh:arena/wild/begin/begin_combat 10s
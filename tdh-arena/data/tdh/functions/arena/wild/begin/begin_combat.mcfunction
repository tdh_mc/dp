# On affiche le nombre de décès sur le côté de l'écran
scoreboard objectives setdisplay sidebar arenaDeaths

# On efface l'inventaire de tous les joueurs
clear @a

# On déplace tous les joueurs à une position aléatoire autour du point de référence
execute at @e[tag=WildArena,limit=1] run spreadplayers ~ ~ 100 100 true @a

# On place tous les joueurs en mode aventure, et on les heal
gamemode adventure @a
effect give @a instant_health 1 8 true
effect give @a saturation 1 8 true

# On leur donne le stuff de départ
loot give @a loot tdh:arena/kit/begin

# On génère des coffres de loot supplémentaires autour de chaque équipe
execute at @r[team=Procyens] run function tdh:arena/wild/begin/gen_bonus_chest
execute at @r[team=Hameliens] run function tdh:arena/wild/begin/gen_bonus_chest

# On initialise la bordure
execute at @e[tag=WildArena,limit=1] run worldborder center ~ ~
worldborder set 360
worldborder set 200 300
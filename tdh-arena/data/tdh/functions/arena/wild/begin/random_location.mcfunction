# On récupère dans temp un nombre aléatoire entre 0 et 4
scoreboard players set #random min 0
scoreboard players set #random max 5
function tdh:random

# On tag le joueur aléatoire à la position duquel cette fonction a été exécutée
tag @p add RefPoint

# Selon le résultat, on sélectionne une des régions de la carte
# 0 : Hameau Original Est (entre Kraken et Quatre Chemins en X, entre Dorlinor et Duerrom en Z)
execute if score #random temp matches ..0 run spreadplayers 1472 448 1 1024 true @p[tag=RefPoint]
execute if score #random temp matches ..0 run tellraw @a [{"text":"Le lieu du combat est en cours de sélection, quelque part dans l'","color":"gold"},{"text":"est des Terres du Hameau","color":"red"},{"text":" !"}]
# 1 : Hameau Original Ouest (entre l'ouest de Pieuze et Kraken en X, entre le nord des Sablons et Grenat–Récif en Z)
execute if score #random temp matches 1 run spreadplayers -576 448 1 1024 true @p[tag=RefPoint]
execute if score #random temp matches 1 run tellraw @a [{"text":"Le lieu du combat est en cours de sélection, quelque part dans l'","color":"gold"},{"text":"ouest des Terres du Hameau","color":"red"},{"text":" !"}]
# 2 : Département de la Béollonie (entre Béothas et Litoréa en X, entre Duerrom et Thalrion en Z)
execute if score #random temp matches 2 run spreadplayers 1472 2496 1 1024 true @p[tag=RefPoint]
execute if score #random temp matches 2 run tellraw @a [{"text":"Le lieu du combat est en cours de sélection, quelque part dans le ","color":"gold"},{"text":"département de la Béollonie","color":"red"},{"text":" !"}]
# 3 : Balchaïs (entre Argençon et Quécol en X, entre Évrocq et Argençon en Z)
execute if score #random temp matches 3 run spreadplayers 1472 5568 1 1024 true @p[tag=RefPoint]
execute if score #random temp matches 3 run tellraw @a [{"text":"Le lieu du combat est en cours de sélection, quelque part dans les ","color":"gold"},{"text":"Balchaïs","color":"red"},{"text":" !"}]
# 4 : Procyon (entre le nord de Cyséal et Aulnoy en X, entre Oréa et Aulnoy en Z)
execute if score #random temp matches 4.. run spreadplayers -576 5312 1 1024 true @p[tag=RefPoint]
execute if score #random temp matches 4.. run tellraw @a [{"text":"Le lieu du combat est en cours de sélection, quelque part dans la ","color":"gold"},{"text":"Contrée de Valient","color":"red"},{"text":" !"}]
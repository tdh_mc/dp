# On retire les joueurs des équipes
team empty Procyens
team empty Hameliens

# On retire les tags d'arène
tag @a[tag=InArena] remove InArena

# On réinitialise le nombre de décès
scoreboard players reset * arenaDeaths

# On retire les scores affichés
scoreboard objectives setdisplay sidebar

# On supprime l'armor stand
execute as @e[tag=WildArena] at @s run function tdh:arena/wild/end/delete_arena

# On réinitialise la bordure
worldborder center 0 0
worldborder set 59999968
# Initialisation des équipes
team add Procyens {"text":"Combattants Procyens","color":"red"}
team modify Procyens prefix {"text":"[P]","color":"dark_red"}
team modify Procyens color red
team modify Procyens friendlyFire true
team modify Procyens seeFriendlyInvisibles true
team modify Procyens collisionRule always
team modify Procyens nametagVisibility hideForOtherTeams

team add Hameliens {"text":"Combattants Haméliens","color":"gold"}
team modify Hameliens prefix {"text":"[H]","color":"gold"}
team modify Hameliens color yellow
team modify Hameliens friendlyFire true
team modify Hameliens seeFriendlyInvisibles true
team modify Hameliens collisionRule always
team modify Hameliens nametagVisibility hideForOtherTeams

# Initialisation des variables
scoreboard objectives add arenaDeaths deathCount "Décès"
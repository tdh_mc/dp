# Génération d'un ID de dialogue unique
# Cette fonction est exécutée par le PNJ lançant le dialogue,
# à la position du joueur participant au dialogue.

# On choisit un ID de dialogue aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 1000000
function tdh:random
scoreboard players operation #dialogue dialogueID = #random temp

# On supprime le score temporaire éventuellement distribué à l'itération précédente
scoreboard players reset #dialogue temp

# Si l'ID de dialogue existe déjà, on réexécute cette fonction
execute as @e[type=villager,scores={dialogueID=1..}] if score @s dialogueID = #dialogue dialogueID run scoreboard players set #dialogue temp 1
execute as @a[scores={dialogueID=1..}] if score @s dialogueID = #dialogue dialogueID run scoreboard players set #dialogue temp 1
execute if score #dialogue temp matches 1 run function tdh:dialogue/new_id

# À la fin de la fonction, on nettoie à nouveau le score
scoreboard players reset #dialogue temp
# On vérifie si la réponse contient des éléments cliquables ou non
execute if data storage tdh:dialogue reponse.cliquable run tag @s add ReponseCliquable

# On affiche la réponse du PNJ, en blanc pour tout le monde
execute as @s[tag=ReponseCliquable] run tellraw @p[tag=NotreJoueur] [{"text":""},{"selector":"@s"},{"text":" : "},{"storage":"tdh:dialogue","nbt":"reponse.cliquable","interpret":true}]
execute as @s[tag=!ReponseCliquable] run tellraw @p[tag=NotreJoueur] [{"text":""},{"selector":"@s"},{"text":" : "},{"storage":"tdh:dialogue","nbt":"reponse","interpret":true}]

# On dit la phrase destinée aux autres joueurs à la position du PNJ,
# pour que les gens autour de lui l'entendent quand même s'il est un peu éloigné du joueur
execute at @s[tag=ReponseCliquable] run tellraw @a[distance=..12,tag=!NotreJoueur] [{"text":""},{"selector":"@s"},{"text":" : "},{"storage":"tdh:dialogue","nbt":"reponse.basique","interpret":true}]
execute at @s[tag=!ReponseCliquable] run tellraw @a[distance=..12,tag=!NotreJoueur] [{"text":""},{"selector":"@s"},{"text":" : "},{"storage":"tdh:dialogue","nbt":"reponse","interpret":true}]

# On supprime les tags temporaires
tag @s remove ReponseCliquable
# Selon le statut, on affiche la bonne phrase
execute if score @s temp matches 0 run function tdh:dialogue/affichage/phrase_entite/0
execute if score @s temp matches 1 run function tdh:dialogue/affichage/phrase_entite/1
execute if score @s temp matches 2 run function tdh:dialogue/affichage/phrase_entite/2
execute if score @s temp matches 3 run function tdh:dialogue/affichage/phrase_entite/3
execute if score @s temp matches 4 run function tdh:dialogue/affichage/phrase_entite/4
execute if score @s temp matches 5 run function tdh:dialogue/affichage/phrase_entite/5

# On supprime les tags temporaires
tag @s remove EntiteParlante
data remove entity @s Item.tag.dialogue.extra
scoreboard players reset @s temp
scoreboard players reset @s temp2
scoreboard players reset @s temp3
scoreboard players reset @s temp4
scoreboard players reset @s temp5
scoreboard players reset @s temp6
scoreboard players reset @s temp7
scoreboard players reset @s temp8
scoreboard players reset @s temp9
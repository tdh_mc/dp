# Une entité importante est utilisée dans une réplique de dialogue (par exemple dans un sélecteur),
# et possède le tag EntiteImportante et (généralement) des scores temp à temp9 pour enregistrer des infos

# On a déjà affiché la réplique, on peut donc nettoyer les tags et scores
tag @s remove EntiteImportante
scoreboard players reset @s temp
scoreboard players reset @s temp2
scoreboard players reset @s temp3
scoreboard players reset @s temp4
scoreboard players reset @s temp5
scoreboard players reset @s temp6
scoreboard players reset @s temp7
scoreboard players reset @s temp8
scoreboard players reset @s temp9
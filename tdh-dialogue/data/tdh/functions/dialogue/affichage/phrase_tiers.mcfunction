# On affiche la réponse du PNJ, en blanc pour tout le monde mais avec un retour à la ligne supplémentaire pour le joueur
# On exécute à la position du PNJ pour que les gens autour de lui l'entendent quand même s'il est un peu éloigné du joueur
tellraw @p[tag=NotreJoueur] [{"text":""},{"selector":"@s"},{"text":" : "},{"storage":"tdh:dialogue","nbt":"phrase_tiers","interpret":true},{"storage":"tdh:dialogue","nbt":"phrase_tiers.cliquable","interpret":true},{"text":"\n"}]

execute positioned as @s run tellraw @a[distance=..12,tag=!NotreJoueur] [{"text":""},{"selector":"@s"},{"text":" : "},{"storage":"tdh:dialogue","nbt":"phrase_tiers","interpret":true},{"storage":"tdh:dialogue","nbt":"phrase_tiers.basique","interpret":true}]

# On supprime les tags temporaires
tag @s remove TiersParlant
scoreboard players reset @s temp
scoreboard players reset @s temp2
scoreboard players reset @s temp3
scoreboard players reset @s temp4
scoreboard players reset @s temp5
scoreboard players reset @s temp6
scoreboard players reset @s temp7
scoreboard players reset @s temp8
scoreboard players reset @s temp9
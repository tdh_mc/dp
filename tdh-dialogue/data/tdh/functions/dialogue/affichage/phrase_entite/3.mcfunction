# On se donne un tag temporaire si la réponse à afficher comporte des éléments cliquables
execute if data storage tdh:dialogue reponse.cliquable run tag @s add ReponseCliquable

# On affiche la réponse du PNJ, en blanc pour tout le monde et sans retour à la ligne supplémentaire pour le joueur
execute as @s[tag=!ReponseCliquable] run tellraw @p[tag=NotreJoueur] [{"text":""},{"storage":"tdh:dialogue","nbt":"phrase_entite[3]","interpret":true}]
execute as @s[tag=ReponseCliquable] run tellraw @p[tag=NotreJoueur] [{"text":""},{"storage":"tdh:dialogue","nbt":"phrase_entite[3].cliquable","interpret":true}]

execute positioned as @s[tag=!ReponseCliquable] run tellraw @a[distance=..12,tag=!NotreJoueur] [{"text":""},{"storage":"tdh:dialogue","nbt":"phrase_entite[3]","interpret":true}]
execute positioned as @s[tag=ReponseCliquable] run tellraw @a[distance=..12,tag=!NotreJoueur] [{"text":""},{"storage":"tdh:dialogue","nbt":"phrase_entite[3].basique","interpret":true}]

# On supprime le tag temporaire
tag @s remove ReponseCliquable
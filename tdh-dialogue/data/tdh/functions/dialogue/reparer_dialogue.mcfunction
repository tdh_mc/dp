# Cette fonction est exécutée par un PNJ pouvant dialoguer,
# et s'assure que le commerce sera bien désactivé avec ce villageois
# (Minecraft re-génère des offres régulièrement si aucune n'existe, même sans point de travail accessible)

# On supprime les offres
data modify entity @s Offers.Recipes set value []

# On se donne un tag pour enregistrer cette suppression
tag @s add DialogueRepare
# Variables utilisées pour détecter les actions des joueurs
scoreboard objectives add talkedToVillager minecraft.custom:minecraft.talked_to_villager

# Variables utilisées pour détecter ce dont le joueur parle
scoreboard objectives add topic trigger "Sujet"
scoreboard objectives add answer trigger "Réponse"
scoreboard objectives add dialogueID dummy "Identifiant de dialogue"

# Variables utilisées pour les transactions
scoreboard objectives add or dummy
scoreboard objectives add fer dummy
scoreboard objectives add diamant dummy
scoreboard objectives add kubs dummy

# Variables utilisées par les PNJ pour stocker diverses informations sur les dialogues
scoreboard objectives add likesHeat dummy "Appréciation de la chaleur"
scoreboard objectives add likesRain dummy "Appréciation des précipitations"
scoreboard objectives add knowsTCH dummy "Connaissance de TCH"
scoreboard objectives add likesTCH dummy "Appréciation de TCH"
scoreboard objectives add likesDrugs dummy "Appréciation des stupéfiants"
scoreboard objectives add knowsDrugs dummy "Connaissance des stupéfiants"
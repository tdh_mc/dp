# Cette fonction enregistre dans le storage si on va dire bonjour ou bonsoir,
# avec des conditions spécifiques qui font qu'on ne dira pas toujours bonjour ou bonsoir aux mêmes horaires exacts

# On track tout avec une variable de contrôle avant de modifier le storage
# Par défaut on a 1(bonjour) s'il fait jour ou aube et 2(bonsoir) s'il fait nuit ou crépuscule
execute if score #temps timeOfDay matches 2..3 run scoreboard players set #dialogue temp 2
execute unless score #temps timeOfDay matches 2..3 run scoreboard players set #dialogue temp 1

# S'il est plus tard que 19h, quelle que soit le moment de la journée, on dit bonsoir
execute if score #temps currentTdhTick matches 38000.. run scoreboard players set #dialogue temp 2
# S'il est plus tôt que 16h, on dit toujours bonjour
execute if score #temps currentTdhTick matches 24000..31999 run scoreboard players set #dialogue temp 1

# On a "bon matin" s'il fait jour (on dit bonjour) et qu'il est moins de midi
execute if score #dialogue temp matches 1 if score #temps currentTdhTick matches ..11999 run scoreboard players set #dialogue temp 3


# On assigne ensuite la bonne valeur dans le storage
execute if score #dialogue temp matches 1 run data modify storage tdh:dialogue extra.bonjour set value {min:"bonjour",maj:"Bonjour"}
execute if score #dialogue temp matches 2 run data modify storage tdh:dialogue extra.bonjour set value {min:"bonsoir",maj:"Bonsoir"}
execute if score #dialogue temp matches 3 run data modify storage tdh:dialogue extra.bonjour set value {min:"bon matin",maj:"Bon matin"}

# On réinitialise la variable temporaire
scoreboard players reset #dialogue temp
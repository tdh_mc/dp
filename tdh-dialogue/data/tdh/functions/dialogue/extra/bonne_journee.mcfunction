# Cette fonction enregistre dans le storage si on va dire 
# avec des conditions spécifiques qui font qu'on ne dira pas toujours bonjour ou bonsoir aux mêmes horaires exacts

# On track tout avec une variable de contrôle avant de modifier le storage
# Par défaut on a 1(bonne journée) s'il fait jour ou aube et 2(bonne soirée) s'il fait nuit ou crépuscule
execute if score #temps timeOfDay matches 2..3 run scoreboard players set #dialogue temp 2
execute unless score #temps timeOfDay matches 2..3 run scoreboard players set #dialogue temp 1

# S'il est plus tard que 18h, quelle que soit le moment de la journée, on dit bonne soirée
execute if score #temps currentTdhTick matches 36000.. run scoreboard players set #dialogue temp 2
# S'il est plus tard que 13h et qu'on dit bonne journée, on dit bonne après-midi
execute if score #dialogue temp matches 1 if score #temps currentTdhTick matches 26000.. run scoreboard players set #dialogue temp 3
# Si on dit bonne soirée mais qu'il est plus de minuit, on dit bonne nuit
execute if score #dialogue temp matches 2 if score #temps currentTdhTick matches ..23999 run scoreboard players set #dialogue temp 4


# On assigne ensuite la bonne valeur dans le storage
execute if score #dialogue temp matches 1 run data modify storage tdh:dialogue extra.bonne_journee set value {min:"bonne journée",maj:"Bonne journée"}
execute if score #dialogue temp matches 2 run data modify storage tdh:dialogue extra.bonne_journee set value {min:"bonne soirée",maj:"Bonne soirée"}
execute if score #dialogue temp matches 3 run data modify storage tdh:dialogue extra.bonne_journee set value {min:"bonne après-midi",maj:"Bonne après-midi"}
execute if score #dialogue temp matches 4 run data modify storage tdh:dialogue extra.bonne_journee set value {min:"bonne nuit",maj:"Bonne nuit"}

# On réinitialise la variable temporaire
scoreboard players reset #dialogue temp
# On se retrouve dans cette fonction si on a détecté que topic != 0

# On active la variable triggerable
scoreboard players enable @p[tag=SpeakingToNPC] topic


### ENREGISTREMENT DES PHRASES À AFFICHER
### ET CALCUL DE LA LOGIQUE DU DIALOGUE
### (dans des sous-fonctions selon le topic)

# Liste de toutes les variables topic existantes
# On détecte si le topic correspond ET si le joueur a débloqué le topic.

# <999 : Mots-clés génériques
# -4 : Écran de sélection des lieux
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux=true}] topic matches -4 run function tdh:dialogue/topics/choose_lieu
# -2 : Choisir un sujet
execute if score @p[distance=..1,tag=SpeakingToNPC] topic matches -2 run function tdh:dialogue/topics/choose_topic
# -1 : Au revoir
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={au_revoir=true}}] topic matches -1 run function tdh:dialogue/topics/au_revoir
# 1 : Greetings
# 2 : Autre sujet
# (topic inutilisé dans le gameplay, mais servent en interne pour enregistrer des sujets pour les answers)
# 10 : Météo
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={meteo=true}}] topic matches 10 run function tdh:dialogue/topics/meteo
# 100 : Rumeurs
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={rumeurs=true}}] topic matches 100 run function tdh:dialogue/topics/rumeurs
# 200 : Travail
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={travail=true}}] topic matches 200 run function tdh:dialogue/topics/travail
# 300 : Métier
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={metier=true}}] topic matches 300 run function tdh:dialogue/topics/metier
# - 380 : Manger
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={manger=true}}] topic matches 380 run function tdh:dialogue/topics/manger
# - 381 : Boire
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={manger=true}}] topic matches 381 run function tdh:dialogue/topics/boire
# 382 : Chambre
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={chambre=true}}] topic matches 382 run function tdh:dialogue/topics/chambre
# - 383 : Torche
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={torche=true}}] topic matches 383 run function tdh:dialogue/topics/torche
# 384 : Réserver une chambre
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/auberge={reserver=true}}] topic matches 384 run function tdh:dialogue/topics/reserver_chambre
# 399 : Ville
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/base_topics={ville=true}}] topic matches 399 run function tdh:dialogue/topics/ville

# 400-599 : Lieux (tous les lieux ayant été débloqués)
# 400-499 : Lieux (Terres du Hameau)

# 400 : Terres du Hameau
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/root_hameau=true}] topic matches 400 run function tdh:dialogue/topics/lieux/terres_du_hameau
# - 401 : Le Hameau
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/hameau=true}] topic matches 401 run function tdh:dialogue/topics/lieux/le_hameau
# - 402 : Mines du Totem
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/hameau_totem=true}] topic matches 402 run function tdh:dialogue/topics/lieux/mines_totem
# - 403 : Camps Miniers du Hameau
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/hameau_campsminiers=true}] topic matches 403 run function tdh:dialogue/topics/lieux/hameau_camps_miniers
# - 404 : Maisonnet de Belzébuth
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/maisonnet_belzebuth=true}] topic matches 404 run function tdh:dialogue/topics/lieux/maisonnet_belzebuth
# - 405 : Verchamps
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/verchamps=true}] topic matches 405 run function tdh:dialogue/topics/lieux/verchamps
# - 406 : Fort du Val
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/fortduval=true}] topic matches 406 run function tdh:dialogue/topics/lieux/fort_du_val

# - 410 : Grenat
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenat=true}] topic matches 410 run function tdh:dialogue/topics/lieux/grenat
# - 411 : Récif de Grenat
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenatrecif=true}] topic matches 411 run function tdh:dialogue/topics/lieux/grenat_recif
# - 412 : Crique de Sanor
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenatrecif_sanor=true}] topic matches 412 run function tdh:dialogue/topics/lieux/crique_sanor
# - 413 : Lac de Preskrik
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenatrecif_preskrik=true}] topic matches 413 run function tdh:dialogue/topics/lieux/lac_preskrik

# - 420 : Villonne
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/villonne=true}] topic matches 420 run function tdh:dialogue/topics/lieux/villonne
# - 421 : Tolbrok
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/tolbrok=true}] topic matches 421 run function tdh:dialogue/topics/lieux/tolbrok
# - 422 : Duerrom
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/duerrom=true}] topic matches 422 run function tdh:dialogue/topics/lieux/duerrom
# - 423 : Béothas
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/beothas=true}] topic matches 423 run function tdh:dialogue/topics/lieux/beothas
# - 424 : Base nautique de Jëej
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/jeej=true}] topic matches 424 run function tdh:dialogue/topics/lieux/jeej

# - 430 : Dorlinor
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/dorlinor=true}] topic matches 430 run function tdh:dialogue/topics/lieux/dorlinor
# - 431 : Ygriak
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/ygriak=true}] topic matches 431 run function tdh:dialogue/topics/lieux/ygriak
# - 432 : Evenis Eclesta
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/eveniseclesta=true}] topic matches 432 run function tdh:dialogue/topics/lieux/evenis_eclesta
# - 433 : Volcan d'Eclesta
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/volcaneclesta=true}] topic matches 433 run function tdh:dialogue/topics/lieux/volcan_eclesta
# - 434 : Citadelle de Gzor
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/gzor=true}] topic matches 434 run function tdh:dialogue/topics/lieux/citadelle_gzor
# - 435 : Château Onyx
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/onyx=true}] topic matches 435 run function tdh:dialogue/topics/lieux/chateau_onyx

# - 440 : Les Sablons
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/sablons=true}] topic matches 440 run function tdh:dialogue/topics/lieux/les_sablons
# - 441 : Château d'Eremos
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/sablons_eremos=true}] topic matches 441 run function tdh:dialogue/topics/lieux/chateau_eremos
# - 442 : Château d'Oklam
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/sablons_oklam=true}] topic matches 442 run function tdh:dialogue/topics/lieux/chateau_oklam
# - 443 : Temple de Chizân
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/chizan=true}] topic matches 443 run function tdh:dialogue/topics/lieux/temple_chizan

# - 450 : Pieuze-en-Sulûm
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/pieuze=true}] topic matches 450 run function tdh:dialogue/topics/lieux/pieuze_en_sulum
# - 451 : Fultèz-en-Sulûm
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/fultez=true}] topic matches 451 run function tdh:dialogue/topics/lieux/fultez_en_sulum
# - 452 : Château d'Illysia
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/illysia=true}] topic matches 452 run function tdh:dialogue/topics/lieux/chateau_illysia

# - 460 : Hadès
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/hades=true}] topic matches 460 run function tdh:dialogue/topics/lieux/hades
# - 461 : Athès
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/athes=true}] topic matches 461 run function tdh:dialogue/topics/lieux/athes
# - 462 : Midès
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/mides=true}] topic matches 462 run function tdh:dialogue/topics/lieux/mides

# - 470 : Néronil-le-Pont
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/neronil=true}] topic matches 470 run function tdh:dialogue/topics/lieux/neronil_le_pont
# - 471 : Litoréa
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/litorea=true}] topic matches 471 run function tdh:dialogue/topics/lieux/litorea
# - 472 : Moronia
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/moronia=true}] topic matches 472 run function tdh:dialogue/topics/lieux/moronia
# - 473 : Eïdin-la-Vallée
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/eidin=true}] topic matches 473 run function tdh:dialogue/topics/lieux/eidin_la_vallee

# 500-599 : Lieux (Contrée de Valient)

# 500 : Contrée de Valient
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/root_procyon=true}] topic matches 500 run function tdh:dialogue/topics/lieux/contree_de_valient
# - 501 : Procyon
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/procyon=true}] topic matches 501 run function tdh:dialogue/topics/lieux/procyon
# - 502 : Donjon des Ténèbres
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/procyon_tenebres=true}] topic matches 502 run function tdh:dialogue/topics/lieux/donjon_tenebres
# - 503 : Aulnoy
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/aulnoy=true}] topic matches 503 run function tdh:dialogue/topics/lieux/aulnoy
# - 504 : Argençon
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/argencon=true}] topic matches 504 run function tdh:dialogue/topics/lieux/argencon

# - 510 : Cyséal
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/cyseal=true}] topic matches 510 run function tdh:dialogue/topics/lieux/cyseal
# - 511 : Vernoglie les Mines
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/vernoglie=true}] topic matches 511 run function tdh:dialogue/topics/lieux/vernoglie_les_mines

# - 520 : Frambourg
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/frambourg=true}] topic matches 520 run function tdh:dialogue/topics/lieux/frambourg
# - 521 : Ternelieu
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/ternelieu=true}] topic matches 521 run function tdh:dialogue/topics/lieux/ternelieu
# - 522 : Gare de Frambourg–Ternelieu
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/frambourgternelieu=true}] topic matches 522 run function tdh:dialogue/topics/lieux/frambourg_ternelieu
# - 523 : Géorlie
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/georlie=true}] topic matches 523 run function tdh:dialogue/topics/lieux/georlie
# - 524 : Bussy-Nigel
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/bussynigel=true}] topic matches 524 run function tdh:dialogue/topics/lieux/bussy_nigel
# - 525 : Crestali
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/crestali=true}] topic matches 525 run function tdh:dialogue/topics/lieux/crestali
# - 526 : Milloreau-les-Pics
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/milloreau=true}] topic matches 526 run function tdh:dialogue/topics/lieux/milloreau_les_pics
# - 527 : Calmeflot-les-Deux-Berges
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/calmeflot=true}] topic matches 527 run function tdh:dialogue/topics/lieux/calmeflot_les_deux_berges
# - 528 : La Dodène
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/ladodene=true}] topic matches 528 run function tdh:dialogue/topics/lieux/la_dodene

# - 540 : Evrocq
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/evrocq=true}] topic matches 540 run function tdh:dialogue/topics/lieux/evrocq
# - 541 : Quécol-en-Drie
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/quecol=true}] topic matches 541 run function tdh:dialogue/topics/lieux/quecol_en_drie
# - 542 : Le Relais
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/lerelais=true}] topic matches 542 run function tdh:dialogue/topics/lieux/le_relais
# - 543 : Plaine des Barbares
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/plaine_barbares=true}] topic matches 543 run function tdh:dialogue/topics/lieux/plaine_barbares
# - 544 : Île du Singe
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/ile_du_singe=true}] topic matches 544 run function tdh:dialogue/topics/lieux/ile_du_singe

# - 550 : Chassy-sur-Flumine
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/chassy=true}] topic matches 550 run function tdh:dialogue/topics/lieux/chassy_sur_flumine
# - 551 : Thalrion
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/thalrion=true}] topic matches 551 run function tdh:dialogue/topics/lieux/thalrion
# - 552 : Le Roustiflet
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/le_roustiflet=true}] topic matches 552 run function tdh:dialogue/topics/lieux/le_roustiflet

# - 560 : Grenat Méridional
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenat_meridional=true}] topic matches 560 run function tdh:dialogue/topics/lieux/grenat_meridional
# - 561 : Oréa-sur-Mer
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/orea=true}] topic matches 561 run function tdh:dialogue/topics/lieux/orea_sur_mer
# - 562 : Fénicia
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/fenicia=true}] topic matches 562 run function tdh:dialogue/topics/lieux/fenicia


# 1000-1999 : TCH et sujets relatifs à TCH
# - 1000 : TCH
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={tch=true}}] topic matches 1000 run function tdh:dialogue/topics/tch
# - 1100 : Acheter tickets
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={ticket=true}}] topic matches 1100 run function tdh:dialogue/topics/tch/ticket
# - 1110 : Tarif des tickets
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/tarifs={tarifs=true}}] topic matches 1110 run function tdh:dialogue/topics/tch/tarifs
# - 1120 : Vendeurs à la sauvette
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/tarifs={sauvette=true}}] topic matches 1120 run function tdh:dialogue/topics/tch/sauvette
# - 1150 : Fraude
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/tarifs={fraude=true}}] topic matches 1150 run function tdh:dialogue/topics/tch/fraude
# 1160 : Contrôleur
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/tarifs={controleur=true}}] topic matches 1160 run function tdh:dialogue/topics/tch/ticket/controleur
# - 1200 : Itinéraire
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={itineraire=true}}] topic matches 1200 run function tdh:dialogue/topics/tch/itineraire
# - 1201 : Résultat d'itinéraire (topic "caché", sans condition, qui correspond à l'obtention des résultats d'un itinéraire)
execute if score @p[distance=0,tag=SpeakingToNPC] topic matches 1201 run function tdh:dialogue/topics/tch/itineraire/agent_tch/autre_chose
# - 1300 : Lignes
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={lignes=true}}] topic matches 1300 run function tdh:dialogue/topics/tch/lignes
# TODO : 13XX = Lignes de métro (1301=M1, 1302=M2, 1303=M3, etc)
# - 1400 : Horaires
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={horaires=true}}] topic matches 1400 run function tdh:dialogue/topics/tch/horaires
# - 1500 : Abonnement
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={abonnement=true}}] topic matches 1500 run function tdh:dialogue/topics/tch/abonnement
# - 1510 : Carte GOLD
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/abonnement={carte_gold=true}}] topic matches 1510 run function tdh:dialogue/topics/tch/abonnement/carte_gold
# - 1520 : Recharger
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/abonnement={recharger=true}}] topic matches 1520 run function tdh:dialogue/topics/tch/abonnement/recharger
# 1521 : Recharger au guichet
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/abonnement={recharger_au_guichet=true}}] topic matches 1521 run function tdh:dialogue/topics/tch/abonnement/recharger_au_guichet
# 1530 : Aides (pour payer l'abonnement)
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/abonnement={aides=true}}] topic matches 1530 run function tdh:dialogue/topics/tch/abonnement/aides

# 2000-2999 : Ressources naturelles
# 2000-2199 : Ressources minières
# 2000 : Mine
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={mine=true}}] topic matches 2000 run function tdh:dialogue/topics/ressource/mine
# 2010 : Charbon
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={charbon=true}}] topic matches 2010 run function tdh:dialogue/topics/ressource/charbon
# 2020 : Fer
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={fer=true}}] topic matches 2020 run function tdh:dialogue/topics/ressource/fer
# 2030 : Or
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={or=true}}] topic matches 2030 run function tdh:dialogue/topics/ressource/or
# 2040 : Redstone
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={redstone=true}}] topic matches 2040 run function tdh:dialogue/topics/ressource/redstone
# 2050 : Lapis
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={lapis_lazuli=true}}] topic matches 2050 run function tdh:dialogue/topics/ressource/lapis_lazuli
# 2060 : Diamant
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={diamant=true}}] topic matches 2060 run function tdh:dialogue/topics/ressource/diamant
# 2070 : Emeraude
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={emeraude=true}}] topic matches 2070 run function tdh:dialogue/topics/ressource/emeraude
# 2080 : Quartz
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={charbon=true}}] topic matches 2080 run function tdh:dialogue/topics/ressource/quartz

# 2200-2399 : Ressources végétales
# 2200 : Drogue
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={drogue=true}}] topic matches 2200 run function tdh:dialogue/topics/ressource/drogue
# 2210 : Tabac
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={tabac=true}}] topic matches 2210 run function tdh:dialogue/topics/ressource/tabac
# 2220 : Herbe
execute if score @p[distance=..1,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={herbe=true}}] topic matches 2220 run function tdh:dialogue/topics/ressource/herbe

### AFFICHAGE DES LIGNES DE DIALOGUE
# (uniquement si on n'a pas accédé à un écran de sélection)
execute unless score @p[tag=SpeakingToNPC] topic matches ..-2 run function tdh:dialogue/affichage
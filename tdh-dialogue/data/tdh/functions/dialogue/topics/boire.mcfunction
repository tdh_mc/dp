# Ce sujet fonctionne différemment selon la personne qu'on interroge.

# On exécute la bonne fonction selon le tag
# Une fois qu'on est passés dans une fonction, elle nous donnera le tag BoissonDonnee, pour éviter les checks redondants
execute as @s[tag=Tavernier] run function tdh:dialogue/topics/boire/tavernier
execute as @s[tag=Aubergiste,tag=!BoissonDonnee] run function tdh:dialogue/topics/boire/aubergiste
execute as @s[tag=Agriculteur,tag=!BoissonDonnee] run function tdh:dialogue/topics/boire/agriculteur
execute as @s[tag=Eleveur,tag=!BoissonDonnee] run function tdh:dialogue/topics/boire/eleveur
execute as @s[tag=!BoissonDonnee] run function tdh:dialogue/topics/boire/generic

# On supprime le tag évitant les checks redondants
tag @s remove BoissonDonnee
# On choisit une phrase pour la "question"
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Au revoir !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Je dois y aller. Au revoir !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Je n\'ai pas le temps de discuter plus longtemps."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Il faut que j\'y aille."'

# On choisit une phrase pour la réponse
# On se donnera le tag AuRevoir dans chaque sous fonction pour éviter les checks redondants
execute as @s[tag=AgentTCH] run function tdh:dialogue/topics/au_revoir/agent_tch
execute as @s[tag=!AuRevoir] run function tdh:dialogue/topics/au_revoir/generic
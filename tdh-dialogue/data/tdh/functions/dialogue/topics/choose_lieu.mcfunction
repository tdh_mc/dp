# Tous les lieux possibles sont proposés ici (uniquement s'ils ont été débloqués)

# On autorise le joueur à toucher à son score au cas où ça ne serait pas le cas
scoreboard players enable @p[distance=0,tag=SpeakingToNPC] topic

# On initialise une liste vide dans le storage
data modify storage tdh:dialogue lieux set value []


# Pour chaque topic, on stocke son nom dans la liste si le joueur l'a débloqué

# 400-499 : Régions, villes et lieux-dits
# 400 : Terres du Hameau
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/root_hameau=true}] run data modify storage tdh:dialogue lieux append value '[{"text":""},{"text":"Terres du Hameau","color":"red","clickEvent":{"action":"run_command","value":"/trigger topic set 400"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des Terres du Hameau."}]}},{"text":" :\\n","color":"gray"}]'
# 401 : Le Hameau
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/hameau=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Le Hameau","clickEvent":{"action":"run_command","value":"/trigger topic set 401"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du Hameau."}]}}]'
# 402 : Mines du Totem
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/hameau_totem=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Mines du Totem","clickEvent":{"action":"run_command","value":"/trigger topic set 402"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des mines du Totem."}]}}]'
# 403 : Camps Miniers
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/hameau_campsminiers=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Camps miniers du Hameau","clickEvent":{"action":"run_command","value":"/trigger topic set 403"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des camps miniers du Hameau."}]}}]'
# 404 : Maisonnet de Belzébuth
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/maisonnet_belzebuth=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Maisonnet de Belzébuth","clickEvent":{"action":"run_command","value":"/trigger topic set 404"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du maisonnet de Belzébuth."}]}}]'
# 405 : Verchamps
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/verchamps=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Verchamps","clickEvent":{"action":"run_command","value":"/trigger topic set 405"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Verchamps."}]}}]'
# 406 : Fort du Val
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/fortduval=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Fort du Val","clickEvent":{"action":"run_command","value":"/trigger topic set 406"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Fort du Val."}]}}]'

# 410 : Grenat
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenat=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Grenat","clickEvent":{"action":"run_command","value":"/trigger topic set 410"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Grenat."}]}}]'
# 411 : Récif de Grenat
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenatrecif=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Récif de Grenat","clickEvent":{"action":"run_command","value":"/trigger topic set 411"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du récif de Grenat."}]}}]'
# 412 : Crique de Sanor
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenatrecif_sanor=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Crique de Sanor","clickEvent":{"action":"run_command","value":"/trigger topic set 412"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la crique de Sanor."}]}}]'
# 413 : Lac de Preskrik
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenatrecif_preskrik=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Lac de Preskrik","clickEvent":{"action":"run_command","value":"/trigger topic set 413"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du lac de Preskrik."}]}}]'

# 420 : Villonne
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/villonne=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Villonne","clickEvent":{"action":"run_command","value":"/trigger topic set 420"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Villonne."}]}}]'
# 421 : Tolbrok
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/tolbrok=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Tolbrok","clickEvent":{"action":"run_command","value":"/trigger topic set 421"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Tolbrok."}]}}]'
# 422 : Duerrom
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/duerrom=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Duerrom","clickEvent":{"action":"run_command","value":"/trigger topic set 422"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Duerrom."}]}}]'
# 423 : Béothas
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/beothas=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Béothas","clickEvent":{"action":"run_command","value":"/trigger topic set 423"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Béothas."}]}}]'
# 424 : Office de Jëej du tourisme
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/jeej=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Office du tourisme de Jëej","clickEvent":{"action":"run_command","value":"/trigger topic set 424"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de l\'office du tourisme de Jëej."}]}}]'

# 430 : Dorlinor
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/dorlinor=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Dorlinor","clickEvent":{"action":"run_command","value":"/trigger topic set 430"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Dorlinor."}]}}]'
# 431 : Ygriak
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/ygriak=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Ygriak","clickEvent":{"action":"run_command","value":"/trigger topic set 431"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler d\'Ygriak."}]}}]'
# 432 : Evenis Eclesta
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/eveniseclesta=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Base nautique d\'Évenis–Éclesta","clickEvent":{"action":"run_command","value":"/trigger topic set 432"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la base nautique d\'Évenis–Éclesta."}]}}]'
# 433 : Volcan d'Éclesta
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/volcaneclesta=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Volcan d\'Éclesta","clickEvent":{"action":"run_command","value":"/trigger topic set 433"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du volcan d\'Éclesta."}]}}]'
# 434 : Citadelle de Gzor
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/gzor=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Citadelle de Gzor","clickEvent":{"action":"run_command","value":"/trigger topic set 434"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la citadelle de Gzor."}]}}]'
# 435 : Château Onyx
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/onyx=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Château Onyx","clickEvent":{"action":"run_command","value":"/trigger topic set 435"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du château Onyx."}]}}]'

# 440 : Les Sablons
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/sablons=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Les Sablons","clickEvent":{"action":"run_command","value":"/trigger topic set 440"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des Sablons."}]}}]'
# 441 : Château d'Erêmos
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/sablons_eremos=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Château d\'Erêmos","clickEvent":{"action":"run_command","value":"/trigger topic set 441"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du château d\'Erêmos."}]}}]'
# 442 : Chateau d'Oklam
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/sablons_oklam=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Château d\'Oklam","clickEvent":{"action":"run_command","value":"/trigger topic set 442"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du château d\'Oklam."}]}}]'
# 443 : Temple de Chizân
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/chizan=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Temple de Chizân","clickEvent":{"action":"run_command","value":"/trigger topic set 443"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du temple de Chizân."}]}}]'

# 450 : Pieuze-en-Sulûm
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/pieuze=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Pieuze-en-Sulûm","clickEvent":{"action":"run_command","value":"/trigger topic set 450"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Pieuze-en-Sulûm."}]}}]'
# 451 : Fultèz-en-Sulûm
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/fultez=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Fultèz-en-Sulûm","clickEvent":{"action":"run_command","value":"/trigger topic set 451"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Fultèz-en-Sulûm."}]}}]'
# 452 : Château d'Illysia
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/illysia=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Château d\'Illysia","clickEvent":{"action":"run_command","value":"/trigger topic set 452"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du château d\'Illysia."}]}}]'

# 460 : Hadès
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/hades=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Hadès","clickEvent":{"action":"run_command","value":"/trigger topic set 460"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Hadès."}]}}]'
# 461 : Athès
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/athes=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Athès","clickEvent":{"action":"run_command","value":"/trigger topic set 461"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler d\'Athès."}]}}]'
# 462 : Midès
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/mides=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Midès","clickEvent":{"action":"run_command","value":"/trigger topic set 462"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Midès."}]}}]'

# 470 : Néronil-le-Pont
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/neronil=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Néronil-le-Pont","clickEvent":{"action":"run_command","value":"/trigger topic set 470"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Néronil-le-Pont."}]}}]'
# 471 : Litoréa
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/litorea=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Litoréa","clickEvent":{"action":"run_command","value":"/trigger topic set 471"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Litoréa."}]}}]'
# 472 : Moronia
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/moronia=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Moronia","clickEvent":{"action":"run_command","value":"/trigger topic set 472"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Moronia."}]}}]'
# 473 : Eïdin-la-Vallée
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/eidin=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Eïdin-la-Vallée","clickEvent":{"action":"run_command","value":"/trigger topic set 473"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler d\'Eïdin-la-Vallée."}]}}]'

# 500 : Contrée de Valient
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/root_procyon=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"\\n"},{"text":"Contrée de Valient","color":"red","clickEvent":{"action":"run_command","value":"/trigger topic set 500"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la Contrée de Valient."}]}},{"text":" :\\n","color":"gray"}]'
# 501 : Procyon
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/procyon=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Procyon","clickEvent":{"action":"run_command","value":"/trigger topic set 501"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Procyon."}]}}]'
# 502 : Donjon des Ténèbres
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/procyon_tenebres=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Donjon des Ténèbres","clickEvent":{"action":"run_command","value":"/trigger topic set 502"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du donjon des Ténèbres."}]}}]'
# 503 : Aulnoy
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/aulnoy=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Aulnoy","clickEvent":{"action":"run_command","value":"/trigger topic set 503"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler d\'Aulnoy."}]}}]'
# 504 : Argençon
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/argencon=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Argençon","clickEvent":{"action":"run_command","value":"/trigger topic set 504"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler d\'Argençon."}]}}]'

# 510 : Cyséal
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/cyseal=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Cyséal","clickEvent":{"action":"run_command","value":"/trigger topic set 510"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Cyséal."}]}}]'
# 511 : Vernoglie-les-Mines
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/vernoglie=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Vernoglie-les-Mines","clickEvent":{"action":"run_command","value":"/trigger topic set 511"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Vernoglie-les-Mines."}]}}]'

# 520 : Frambourg
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/frambourg=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Frambourg","clickEvent":{"action":"run_command","value":"/trigger topic set 520"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Frambourg."}]}}]'
# 521 : Ternelieu
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/ternelieu=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Ternelieu","clickEvent":{"action":"run_command","value":"/trigger topic set 521"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Ternelieu."}]}}]'
# 522 : Gare de Frambourg-Ternelieu
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/frambourgternelieu=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Gare de Frambourg-Ternelieu","clickEvent":{"action":"run_command","value":"/trigger topic set 522"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la gare de Frambourg-Ternelieu."}]}}]'
# 523 : Géorlie
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/georlie=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Géorlie","clickEvent":{"action":"run_command","value":"/trigger topic set 523"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Géorlie."}]}}]'
# 524 : Bussy-Nigel
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/bussynigel=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Bussy-Nigel","clickEvent":{"action":"run_command","value":"/trigger topic set 524"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Bussy-Nigel."}]}}]'
# 525 : Crestali
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/crestali=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Crestali","clickEvent":{"action":"run_command","value":"/trigger topic set 525"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Crestali."}]}}]'
# 526 : Milloreau-les-Pics
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/milloreau=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Milloreau-les-Pics","clickEvent":{"action":"run_command","value":"/trigger topic set 526"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Milloreau-les-Pics."}]}}]'
# 527 : Calmeflot-les-Deux-Berges
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/calmeflot=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Calmeflot-les-Deux-Berges","clickEvent":{"action":"run_command","value":"/trigger topic set 527"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Calmeflot-les-Deux-Berges."}]}}]'
# 528 : La Dodène
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/ladodene=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"La Dodène","clickEvent":{"action":"run_command","value":"/trigger topic set 528"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de La Dodène."}]}}]'

# 540 : Évrocq
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/evrocq=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Évrocq","clickEvent":{"action":"run_command","value":"/trigger topic set 540"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler d\'Évrocq."}]}}]'
# 541 : Quécol-en-Drie
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/quecol=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Quécol-en-Drie","clickEvent":{"action":"run_command","value":"/trigger topic set 541"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Quécol-en-Drie."}]}}]'
# 542 : Le Relais
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/lerelais=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Le Relais","clickEvent":{"action":"run_command","value":"/trigger topic set 542"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du Relais."}]}}]'
# 543 : Plaine des Barbares
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/plaine_barbares=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Plaine des Barbares","clickEvent":{"action":"run_command","value":"/trigger topic set 543"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la plaine des Barbares."}]}}]'
# 544 : Île du Singe
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/ile_du_singe=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Île du Singe","clickEvent":{"action":"run_command","value":"/trigger topic set 544"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de l\'île du Singe."}]}}]'

# 550 : Chassy-sur-Flumine
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/chassy=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Chassy-sur-Flumine","clickEvent":{"action":"run_command","value":"/trigger topic set 550"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Chassy-sur-Flumine."}]}}]'
# 551 : Thalrion
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/thalrion=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Thalrion","clickEvent":{"action":"run_command","value":"/trigger topic set 551"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Thalrion."}]}}]'
# 552 : Le Roustiflet
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/leroustiflet=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Le Roustiflet","clickEvent":{"action":"run_command","value":"/trigger topic set 552"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du Roustiflet."}]}}]'

# 560 : Grenat Méridional
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/grenat_meridional=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Grenat Méridional","clickEvent":{"action":"run_command","value":"/trigger topic set 560"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler du Grenat Méridional."}]}}]'
# 561 : Oréa-sur-Mer
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/orea=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Oréa-sur-Mer","clickEvent":{"action":"run_command","value":"/trigger topic set 561"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler d\'Oréa-sur-Mer."}]}}]'
# 562 : Fénicia
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/lieux/fenicia=true}] run data modify storage tdh:dialogue lieux append value '[{"text":"Fénicia","clickEvent":{"action":"run_command","value":"/trigger topic set 562"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Fénicia."}]}}]'


# On affiche le tableau complet
tellraw @p[distance=0,tag=SpeakingToNPC] [{"nbt":"lieux[]","storage":"tdh:dialogue","interpret":true,"color":"yellow","separator":{"text":" ● ","color":"gray"}}]


# On supprime ensuite les données du storage
data remove storage tdh:dialogue lieux
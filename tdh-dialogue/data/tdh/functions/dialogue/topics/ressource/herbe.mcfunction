# Le résultat sera différent en fonction du type de PNJ qu'on interroge
# Une fois qu'on est passés dans une fonction, elle nous donnera le tag HerbeDonnee, pour éviter les checks redondants

# Si c'est un agent TCH, il nous rappellera qu'il est interdit de fumer, ekcetera
execute as @s[tag=AgentTCH] run function tdh:dialogue/topics/ressource/herbe/agent_tch
# Si c'est un dealer, ben ouais évidemment tu veux quoi ?
execute as @s[tag=Dealer] run function tdh:dialogue/topics/ressource/herbe/dealer
# Si c'est un buraliste ou un tavernier, il va y avoir un check supplémentaire pour vérifier si on est bien seuls
execute as @s[tag=Buraliste] run function tdh:dialogue/topics/ressource/herbe/buraliste
execute as @s[tag=Tavernier] run function tdh:dialogue/topics/ressource/herbe/tavernier
# Sinon, on a une réponse générique
execute as @s[tag=!HerbeDonnee] run function tdh:dialogue/topics/ressource/herbe/generic

# On supprime le tag évitant les checks redondants
tag @s remove HerbeDonnee
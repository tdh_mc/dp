# Le résultat sera différent en fonction du type de PNJ qu'on interroge
# Une fois qu'on est passés dans une fonction, elle nous donnera le tag TabacDonne, pour éviter les checks redondants

# Si c'est un agent TCH, il nous rappellera qu'il est interdit de fumer, ekcetera
execute as @s[tag=AgentTCH] run function tdh:dialogue/topics/ressource/tabac/agent_tch
# Si c'est un buraliste, il nous demandera ce qu'on souhaite
execute as @s[tag=Buraliste] run function tdh:dialogue/topics/ressource/tabac/buraliste
# Si c'est un tavernier, il pourra probablement nous dépanner, mais pour plus cher
execute as @s[tag=Tavernier] run function tdh:dialogue/topics/ressource/tabac/tavernier
# Sinon, on a une réponse générique (qui cherchera aux alentours s'il trouve un bureau de tabac ou une taverne)
execute as @s[tag=!TabacDonne] run function tdh:dialogue/topics/ressource/tabac/generic

# On supprime le tag évitant les checks redondants
tag @s remove TabacDonne
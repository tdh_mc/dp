# On choisit une phrase pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Je ne comprends pas un traître mot de ce que vous me racontez. Vous avez bu ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas de quoi vous me parlez. Il vous faut certainement un peu de repos."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Hein ? Je n\'ai aucune idée de ce que vous me voulez. Arrêtez donc de m\'enquiquiner !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Vous devez vous tromper de personne, je ne vois que ça..."'

# Fin de conversation
tag @s add AuRevoir
# On enregistre dans le storage les données de la taverne
data modify storage tdh:dialogue extra.lieu.phrase set value '"l\'entrée du métro"'

# On se donne un tag pour savoir qu'on a trouvé un bâtiment proche
tag @s add LieuTrouve
# On enregistre dans le storage les données de l'auberge
data modify storage tdh:dialogue extra.lieu set from entity @e[type=item_frame,tag=Auberge,sort=nearest,limit=1] Item.tag.auberge
data modify storage tdh:dialogue extra.lieu.phrase set value '[{"text":"l\'auberge "},{"storage":"tdh:dialogue","nbt":"extra.lieu.prefixe"},{"storage":"tdh:dialogue","nbt":"extra.lieu.nom"}]'

# On se donne un tag pour savoir qu'on a trouvé un bâtiment proche
tag @s add LieuTrouve
# On tag un dealer aléatoirement
tag @e[type=villager,tag=Dealer,distance=..200,sort=random,limit=1] add EntiteImportante

# On récupère la position du dealer
execute store result score @s deltaX run data get entity @e[type=villager,tag=Dealer,tag=EntiteImportante,sort=nearest,limit=1] Pos[0]
execute store result score @s deltaZ run data get entity @e[type=villager,tag=Dealer,tag=EntiteImportante,sort=nearest,limit=1] Pos[2]
execute store result score @s posX run data get entity @s Pos[0]
execute store result score @s posZ run data get entity @s Pos[2]
# On fait le calcul de la distance
function tdh:dialogue/calcul/distance

# On génère une réponse aléatoire du PNJ (la phrase générique qui introduit le dealer)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Je connais un plan. Il s\'appelle "},{"selector":"@e[type=villager,tag=Dealer,tag=EntiteImportante,sort=nearest,limit=1]"},{"text":", et il est bien achalandé. "},{"storage":"tdh:dialogue","nbt":"extra.indication","interpret":true}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Vous êtes bien tombé ! Par ici, le plus fiable, c\'est "},{"selector":"@e[type=villager,tag=Dealer,tag=EntiteImportante,sort=nearest,limit=1]"},{"text":" : il a sûrement tout ce qu\'il vous faut. "},{"storage":"tdh:dialogue","nbt":"extra.indication","interpret":true}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Vous avez frappé à la bonne porte ! Tout ce que vous avez besoin de faire, c\'est rendre visite à "},{"selector":"@e[type=villager,tag=Dealer,tag=EntiteImportante,sort=nearest,limit=1]"},{"text":" pour lui acheter les produits que vous voulez, et ce absolument à prix d\'or. "},{"storage":"tdh:dialogue","nbt":"extra.indication","interpret":true}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Vous avez de la chance, je connais justement quelqu\'un. C\'est le dénommé "},{"selector":"@e[type=villager,tag=Dealer,tag=EntiteImportante,sort=nearest,limit=1]"},{"text":"..."},{"storage":"tdh:dialogue","nbt":"extra.indication","interpret":true},{"text":" Je suis certain qu\'il a ce que vous cherchez."}]'


# Selon la position du dealer, on enregistre des données à afficher (et on se donne le tag LieuTrouve quand on trouve)
execute at @e[type=villager,tag=Dealer,tag=EntiteImportante,limit=1] at @e[type=item_frame,tag=Hotel,distance=..25,limit=1] run function tdh:dialogue/topics/ressource/herbe/generic/succes/dealer/hotel
execute as @s[tag=!LieuTrouve] at @e[type=villager,tag=Dealer,tag=EntiteImportante,limit=1] at @e[type=item_frame,tag=Auberge,distance=..25,limit=1] run function tdh:dialogue/topics/ressource/herbe/generic/succes/dealer/auberge
execute as @s[tag=!LieuTrouve] at @e[type=villager,tag=Dealer,tag=EntiteImportante,limit=1] at @e[type=item_frame,tag=Taverne,distance=..25,limit=1] run function tdh:dialogue/topics/ressource/herbe/generic/succes/dealer/taverne
execute as @s[tag=!LieuTrouve] at @e[type=villager,tag=Dealer,tag=EntiteImportante,limit=1] at @e[type=item_frame,tag=Tabagie,distance=..25,limit=1] run function tdh:dialogue/topics/ressource/herbe/generic/succes/dealer/tabagie
execute as @s[tag=!LieuTrouve] at @e[type=villager,tag=Dealer,tag=EntiteImportante,limit=1] at @e[type=item_frame,tag=tchEntrance,distance=..15,limit=1] run function tdh:dialogue/topics/ressource/herbe/generic/succes/dealer/bouche_metro

# On génère d'abord, selon l'heure de la journée, le bon verbe ("il traîne souvent" vs "il habite à" si c'est la nuit)
execute if score #temps timeOfDay matches 1 run data modify storage tdh:dialogue extra.verbe set value "Il traîne souvent "
execute unless score #temps timeOfDay matches 1 run data modify storage tdh:dialogue extra.verbe set value "Il habite "

# On génère ensuite la phrase complète aléatoirement, avec des versions différentes si on a trouvé un lieu et si on n'en a pas trouvé
function tdh:random

# Si on a trouvé un lieu
execute if score #random temp matches 0..49 as @s[tag=LieuTrouve] run data modify storage tdh:dialogue extra.indication set value '[{"storage":"tdh:dialogue","nbt":"extra.verbe"},{"text":"près de "},{"storage":"tdh:dialogue","nbt":"extra.lieu.phrase","interpret":true},{"text":", "},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":" d\'ici."}]'
execute if score #random temp matches 50..99 as @s[tag=LieuTrouve] run data modify storage tdh:dialogue extra.indication set value '[{"storage":"tdh:dialogue","nbt":"extra.verbe"},{"text":"vers "},{"storage":"tdh:dialogue","nbt":"extra.lieu.phrase","interpret":true},{"text":", en direction "},{"storage":"tdh:locate","nbt":"resultat.direction.deDu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":"."}]'

# Si on a pas trouvé de lieu
execute if score #random temp matches 0..99 as @s[tag=!LieuTrouve] run data modify storage tdh:dialogue extra.indication set value '[{"storage":"tdh:dialogue","nbt":"extra.verbe"},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":" d\'ici."}]'

# On supprime le tag temporaire
tag @s remove LieuTrouve
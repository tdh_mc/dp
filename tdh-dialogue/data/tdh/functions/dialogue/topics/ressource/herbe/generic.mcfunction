# On enregistre le fait qu'on a répondu
tag @s add HerbeDonnee

# On génère pour le PNJ un avis sur les stupéfiants s'il n'en possède pas encore
execute unless score @s likesDrugs matches 0.. run function tdh:dialogue/calcul/avis_stupefiants

# On vérifie s'il y a des dealers/taverne/tabagie à proximité
# Sinon, le score restera à 0
scoreboard players set #dialogue temp 0
# On vérifie s'il y a une taverne ou une tabagie
execute if entity @e[type=item_frame,tag=Taverne,distance=..200] run scoreboard players set #dialogue temp 1
execute if entity @e[type=item_frame,tag=Tabagie,distance=..200] run scoreboard players set #dialogue temp 2
# On vérifie s'il y a des dealers
execute if entity @e[type=villager,tag=Dealer,distance=..150] run scoreboard players add #dialogue temp 10
# L'info est encodée de la manière suivante :
# 0 : rien / 1 : taverne / 2 : tabagie (et éventuellement taverne, mais osef car la tabagie est prioritaire)
# 10 : dealer et c tout / 11 : dealer et taverne / 12 : dealer et tabagie


# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous ne sauriez pas où je peux trouver de l\'herbe, dans le coin ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous savez où je peux trouver de l\'herbe, par ici ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"J\'ai besoin de fumer un peu d\'herbe... il y a des vendeurs, dans le coin ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Il faut que je fume de l\'herbe, c\'est capital. Vous savez où je peux en trouver ?"'


# Selon l'avis du PNJ sur l'herbe, on a une sous fonction différente
# Par défaut, 50=aucune appréciation et 75=appréciation totale

# <40 : il déteste vraiment ça et va nous envoyer chier
execute unless score @s likesDrugs matches 40.. run function tdh:dialogue/topics/ressource/herbe/generic/echec/deteste
# <25 et faible appréciation de l'herbe : le type n'y connaît vraiment rien et du coup ne peut rien indiquer du tout
execute if score @s knowsDrugs matches ..24 if score @s likesDrugs matches 40..55 run function tdh:dialogue/topics/ressource/herbe/generic/echec/fume_pas
# <25 et appréciation de l'herbe : le type n'y connaît vraiment rien non plus et n'indique rien non plus
execute if score @s knowsDrugs matches ..24 if score @s likesDrugs matches 56.. run function tdh:dialogue/topics/ressource/herbe/generic/echec/connait_pas
# 25-50 : il ne connaît probablement pas de dealers mais pourra indiquer une éventuelle tabagie/taverne
execute if score @s knowsDrugs matches 25..49 if score @s likesDrugs matches 40.. run function tdh:dialogue/topics/ressource/herbe/generic/novice
# 50-75 : il connaît probablement un dealer s'il y en a un
# (même proportion de chances si on a +75 mais qu'on ne fume pas)
execute if score @s knowsDrugs matches 50..74 if score @s likesDrugs matches 40.. run function tdh:dialogue/topics/ressource/herbe/generic/apprenti
execute if score @s knowsDrugs matches 75.. if score @s likesDrugs matches 40..54 run function tdh:dialogue/topics/ressource/herbe/generic/apprenti
# >75 et fumeur : il connaît avec certitude le dealer s'il y en a un
execute if score @s knowsDrugs matches 75.. if score @s likesDrugs matches 55.. run function tdh:dialogue/topics/ressource/herbe/generic/expert
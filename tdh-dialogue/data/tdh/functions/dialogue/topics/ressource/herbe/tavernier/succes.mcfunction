# On choisit une phrase pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"N\'allez surtout pas raconter ça à tout le monde, mais j\'ai quelques joints pour les bons clients... Je vous en mets un ou deux ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"S\'il n\'y a que vous, ça ne me dérange pas de vous en vendre, mais n\'allez pas crier ça sur les toits, hein !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Venez pas me demander ça quand il y a d\'autres clients, d\'accord ? Vous vouliez quoi ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"J\'en vends un peu pour arrondir les recettes... mais de grâce, n\'en pipez mot !"'

# On active le mode réponse chez le tavernier (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 2220

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les différentes options possibles
tag @s add Options
# Le joint et l'herbe sont toujours disponibles
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Joint ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 101"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x2","clickEvent":{"action":"run_command","value":"/trigger answer set 102"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"4 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x3","clickEvent":{"action":"run_command","value":"/trigger answer set 103"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 or","color":"gold","bold":"true"},{"text":"."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Herbe à pipe ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 201"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x2","clickEvent":{"action":"run_command","value":"/trigger answer set 202"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"4 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x5","clickEvent":{"action":"run_command","value":"/trigger answer set 205"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"8 or","color":"gold","bold":"true"},{"text":"."}]}}]'

# On a aussi la réponse générique "Non, je ne veux rien"
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"En fait, je ne veux rien.","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Changer d\'avis."}]}}]'
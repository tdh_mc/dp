# On enregistre le fait qu'on a répondu
tag @s add HerbeDonnee

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Dites... vous ne sauriez pas où je peux trouver de l\'herbe, dans le coin ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous n\'auriez pas un plan fiable pour de l\'herbe à pipe, par hasard ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Il y a des vendeurs d\'herbe, dans le coin ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"On peut trouver de l\'herbe à pipe, dans la région ?"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Non mais ça ne va pas ? Je vais faire comme si je n\'avais rien entendu, d\'accord ? Gardez bien ça en tête : il est totalement interdit d\'en utiliser sur l\'ensemble du réseau TCH... y compris sur les lignes aériennes !"',cliquable:'[{"text":"Non mais ça ne va pas ? Je vais faire comme si je n\'avais rien entendu, d\'accord ? Gardez bien ça en tête : il est totalement interdit d\'en utiliser sur l\'ensemble du réseau "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de TCH."}]}},{"text":"... y compris sur les lignes aériennes !"}]'}
execute if score #random temp matches 0..49 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch tch

execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"Franchement, vous me laissez sans voix. Permettez-moi simplement de vous rappeler que la fumée est proscrite sur l\'ensemble du réseau, et en produire peut vous exposer à une amende de la part d\'un contrôleur !"',cliquable:'[{"text":"Franchement, vous me laissez sans voix. Permettez-moi simplement de vous rappeler que la fumée est proscrite sur l\'ensemble du réseau, et en produire peut vous exposer à une amende de la part d\'un "},{"text":"contrôleur","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1160"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des contrôleurs TCH."}]}},{"text":" !"}]'}
execute if score #random temp matches 50..99 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch/tarifs controleur
# On génère une réponse négative aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Je ne fume pas, désolé."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je n\'y connais vraiment pas grand chose... Bon courage dans vos recherches !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas trop. Il doit y en avoir, c\'est sûr, mais ça n\'est pas mon domaine !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je ne suis vraiment pas renseigné à ce sujet, sachez-le."'

# On enregistre le fait qu'on quitte la conversation
tag @s add AuRevoir
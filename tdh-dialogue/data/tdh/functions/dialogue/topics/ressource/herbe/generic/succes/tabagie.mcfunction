# On récupère la position de la tabagie
execute store result score @s deltaX run data get entity @e[type=item_frame,tag=Tabagie,sort=nearest,limit=1] Pos[0]
execute store result score @s deltaZ run data get entity @e[type=item_frame,tag=Tabagie,sort=nearest,limit=1] Pos[2]
execute store result score @s posX run data get entity @s Pos[0]
execute store result score @s posZ run data get entity @s Pos[2]
# On fait le calcul de la distance
function tdh:dialogue/calcul/distance

# On tag un buraliste aléatoirement
execute at @e[type=item_frame,tag=Tabagie,sort=nearest,limit=1] run tag @e[type=villager,tag=Buraliste,distance=..20,sort=random,limit=1] add EntiteImportante

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Vous pouvez essayer à la tabagie "},{"nbt":"Item.tag.tabagie.prefixe","entity":"@e[type=item_frame,tag=Tabagie,sort=nearest,limit=1]"},{"nbt":"Item.tag.tabagie.nom","entity":"@e[type=item_frame,tag=Tabagie,sort=nearest,limit=1]"},{"text":", "},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":". Mais attention, ils ne sont pas "},{"text":"supposés","italic":"true"},{"text":" en vendre, si vous voyez ce que je veux dire... Alors s\'il y a du monde, patientez le temps qu\'il s\'en aille !"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Demandez donc ça à "},{"selector":"@e[type=villager,tag=Buraliste,tag=EntiteImportante,limit=1]"},{"text":", "},{"nbt":"Item.tag.tabagie.alAu","entity":"@e[type=item_frame,tag=Tabagie,sort=nearest,limit=1]"},{"nbt":"Item.tag.tabagie.nom","entity":"@e[type=item_frame,tag=Tabagie,sort=nearest,limit=1]"},{"text":" ! C\'est "},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":" d\'ici, mais soyez malin, hein ! N\'allez pas lui demander ça quand il y a du monde dans sa boutique. Ça risquerait de créer des problèmes..."}]'
# On génère une réponse négative aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Dans le coin, je ne sais pas, désolé. Ce n\'est pas moi qui l\'achète, d\'habitude..."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je ne connais pas de gens qui en vendent, désolé... Bon courage dans vos recherches !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas trop. Il y en a, c\'est sûr, mais il ne faut pas compter sur moi pour en trouver !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Ça m\'arrive d\'en prendre, mais je ne sais pas qui fournit. J\'ai juste des amis qui m\'en prêtent de temps en temps..."'

# On enregistre le fait qu'on quitte la conversation
tag @s add AuRevoir
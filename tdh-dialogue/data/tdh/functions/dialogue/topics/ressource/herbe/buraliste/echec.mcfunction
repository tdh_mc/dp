# On choisit une phrase pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'"Non mais ça ne va pas ? C\'est un établissement respectable ici, enfin ! Vous ne voulez pas aussi un peu de cocaïne pendant que vous y êtes ? Je vous jure..."',cliquable:'[{"text":"Non mais ça ne va pas ? C\'est un établissement respectable ici, enfin ! Vous ne voulez pas aussi un peu de "},{"text":"cocaïne","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2230"},"hoverEvent":{"action":"show_text","value":{"text":"Demander où trouver de la cocaïne"}}},{"text":" pendant que vous y êtes ? Je vous jure..."}]'}
execute if score #random temp matches 0..24 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/ressources cocaine

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Vous êtes cinglé ? Vous voulez acheter de la drogue dans mon humble tabagie ? Vous n\'êtes vraiment pas net !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Mais vous n\'êtes pas bien ? J\'ai assez de problèmes comme ça, moi, démerdez-vous avec les vôtres !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Mais absolument pas, espèce de malade ! Comment pouvez-vous imaginer une chose pareille ?"'

# Fin de conversation
tag @s add AuRevoir
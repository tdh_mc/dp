# On a une chance sur 10 de trouver le dealer s'il existe
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
# donc 90% de chances de changer un score "dealer trouvé" (10, 11 ou 12)
# en score "dealer non trouvé" (0/1/2)
execute if score #random temp matches 10.. if score #dialogue temp matches 10.. run scoreboard players remove #dialogue temp 10

# Si notre score est de 1 ou 2 (taverne/tabagie) on a 25% de chances supplémentaires de ne pas donner de conseil du tout,
# au lieu de conseiller d'aller à la taverne ou la tabagie
function tdh:random
execute if score #random temp matches 75.. if score #dialogue temp matches 1..2 run scoreboard players set #dialogue temp 0

# Selon le résultat, on exécute le succès ou l'échec correspondant
execute if score #dialogue temp matches 0 run function tdh:dialogue/topics/ressource/herbe/generic/echec/connait_pas
execute if score #dialogue temp matches 1 run function tdh:dialogue/topics/ressource/herbe/generic/succes/taverne
execute if score #dialogue temp matches 2 run function tdh:dialogue/topics/ressource/herbe/generic/succes/tabagie
execute if score #dialogue temp matches 10.. run function tdh:dialogue/topics/ressource/herbe/generic/succes/dealer
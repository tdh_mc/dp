# On enregistre le fait qu'on a répondu
tag @s add HerbeDonnee

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..39 run data modify storage tdh:dialogue question set value '"Vous n\'auriez pas de l\'herbe, par hasard ?"'
execute if score #random temp matches 40..69 run data modify storage tdh:dialogue question set value '"Il me faudrait un peu d\'herbe ! Vous avez ça en stock ?"'
execute if score #random temp matches 70..99 run data modify storage tdh:dialogue question set value '"Vous avez de l\'herbe, aussi ?"'


# On vérifie s'il y a quelqu'un aux alentours ou si on est seuls
execute at @s positioned ~-12 ~-2 ~-12 if entity @e[type=villager,dx=24,dy=4,dz=24,tag=!Buraliste] run tag @s add EchecCheck

# On n'accepte de vendre de l'herbe que si on est seuls
execute as @s[tag=EchecCheck] run function tdh:dialogue/topics/ressource/herbe/buraliste/echec
execute as @s[tag=!EchecCheck] run function tdh:dialogue/topics/ressource/herbe/buraliste/succes

# On supprime le tag temporaire
tag @s remove EchecCheck
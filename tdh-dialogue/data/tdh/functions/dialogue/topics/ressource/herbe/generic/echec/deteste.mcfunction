# On génère une réponse négative aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Va chier, sale drogué !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Certainement pas ici en tout cas. Allez, dégage."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Ne m\'approche plus avec tes sales pattes de drogué ! Va la chercher ailleurs, ta merde !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Il n\'y a pas de gens louches, par ici ! Allez, du vent !"'

# On enregistre le fait qu'on quitte la conversation
tag @s add AuRevoir
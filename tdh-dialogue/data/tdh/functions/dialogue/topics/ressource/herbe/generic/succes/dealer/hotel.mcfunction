# On enregistre dans le storage les données de l'hôtel
data modify storage tdh:dialogue extra.lieu set from entity @e[type=item_frame,tag=Hotel,sort=nearest,limit=1] Item.tag.hotel
data modify storage tdh:dialogue extra.lieu.phrase set value '[{"text":"l\'hôtel "},{"storage":"tdh:dialogue","nbt":"extra.lieu.prefixe"},{"storage":"tdh:dialogue","nbt":"extra.lieu.nom"}]'

# On se donne un tag pour savoir qu'on a trouvé un bâtiment proche
tag @s add LieuTrouve
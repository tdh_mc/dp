# On choisit une phrase pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Allez, on fait ça vite."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Tu veux combien ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Il te faut quoi ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Parle pas trop fort !"'

# On active le mode réponse chez le dealer (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 2220

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les différentes options possibles
tag @s add Options
# Le joint et l'herbe sont toujours disponibles
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Joint ","color":"yellow"},{"text":"–"},{"text":"x5","clickEvent":{"action":"run_command","value":"/trigger answer set 105"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x10","clickEvent":{"action":"run_command","value":"/trigger answer set 110"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"4 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x25","clickEvent":{"action":"run_command","value":"/trigger answer set 125"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"8 or","color":"gold","bold":"true"},{"text":"."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Herbe à pipe ","color":"yellow"},{"text":"x5","clickEvent":{"action":"run_command","value":"/trigger answer set 205"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"3 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x10","clickEvent":{"action":"run_command","value":"/trigger answer set 210"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x25","clickEvent":{"action":"run_command","value":"/trigger answer set 225"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"10 or","color":"gold","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x50","clickEvent":{"action":"run_command","value":"/trigger answer set 250"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"15 or","color":"gold","bold":"true"},{"text":"."}]}}]'

# On a aussi la réponse générique "Non, je ne veux rien"
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"En fait, je ne veux rien.","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Changer d\'avis."}]}}]'
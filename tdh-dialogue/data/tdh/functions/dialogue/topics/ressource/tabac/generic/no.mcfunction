# On génère une réponse négative aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Désolé, je ne fume pas."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je ne crois pas que vous en trouverez aisément, par ici..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas du tout, désolé."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Pas la moindre idée. Bon courage à vous !"'

# On enregistre le fait qu'on quitte la conversation
tag @s add AuRevoir
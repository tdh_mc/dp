# On enregistre le fait qu'on a répondu
tag @s add TabacDonne

# On récupère la position de la taverne
execute store result score @s deltaX run data get entity @e[type=item_frame,tag=Taverne,sort=nearest,limit=1] Pos[0]
execute store result score @s deltaZ run data get entity @e[type=item_frame,tag=Taverne,sort=nearest,limit=1] Pos[2]
execute store result score @s posX run data get entity @s Pos[0]
execute store result score @s posZ run data get entity @s Pos[2]
# On fait le calcul de la distance
function tdh:dialogue/calcul/distance

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Il n\'y a pas de tabagie, dans le coin... Si vous n\'avez vraiment plus rien, essayez donc à la taverne "},{"nbt":"Item.tag.taverne.prefixe","entity":"@e[type=item_frame,tag=Taverne,sort=nearest,limit=1]"},{"nbt":"Item.tag.taverne.nom","entity":"@e[type=item_frame,tag=Taverne,sort=nearest,limit=1]"},{"text":", "},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":". Mais bon, ça risque de vous coûter un bras !"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Proche d\'ici, je ne vois pas trop... Peut-être en ont-ils un peu à la taverne "},{"nbt":"Item.tag.taverne.prefixe","entity":"@e[type=item_frame,tag=Taverne,sort=nearest,limit=1]"},{"nbt":"Item.tag.taverne.nom","entity":"@e[type=item_frame,tag=Taverne,sort=nearest,limit=1]"},{"text":" ? C\'est "},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":" d\'ici, à deux pas à peine."}]'
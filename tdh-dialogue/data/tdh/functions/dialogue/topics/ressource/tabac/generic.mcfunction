# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous ne sauriez pas où je peux trouver du tabac, dans le coin ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous savez où je peux trouver du tabac, par ici ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Ma tabatière est vide, savez-vous où je pourrais la remplir ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Quelqu\'un vend-il du tabac, dans la région ?"'

# Si on trouve une tabagie ou taverne aux alentours, on lance une fonction spécifique
execute if entity @e[type=item_frame,tag=Tabagie,distance=..200] run function tdh:dialogue/topics/ressource/tabac/generic/tabagie
execute as @s[tag=!TabacDonne] if entity @e[type=item_frame,tag=Taverne,distance=..200] run function tdh:dialogue/topics/ressource/tabac/generic/taverne
execute as @s[tag=!TabacDonne] if entity @e[type=item_frame,tag=Ferme,distance=..200] run function tdh:dialogue/topics/ressource/tabac/generic/ferme

# Sinon, on lance la fonction générique "JSP"
execute as @s[tag=!TabacDonne] run function tdh:dialogue/topics/ressource/tabac/generic/no
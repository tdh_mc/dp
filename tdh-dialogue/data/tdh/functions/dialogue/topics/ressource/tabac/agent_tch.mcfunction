# On enregistre le fait qu'on a répondu
tag @s add TabacDonne

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Dites, vous ne sauriez pas où je peux trouver du tabac, dans le coin ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous ne savez pas où je pourrais m\'acheter du tabac ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Vous savez s\'il y a un bureau de tabac, par ici ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Il y aurait un bureau de tabac, dans la région ?"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Je ne sais pas si j\'ai le droit de vous dire ça. Je dois simplement vous rappeler que fumer est nocif pour votre santé, et qu\'il est totalement interdit d\'en utiliser sur l\'ensemble du réseau TCH... y compris sur le réseau aérien !"',cliquable:'[{"text":"Je ne sais pas si j\'ai le droit de vous dire ça. Je dois simplement vous rappeler que fumer est nocif pour votre santé, et qu\'il est totalement interdit d\'en utiliser sur l\'ensemble du réseau "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de TCH."}]}},{"text":"... y compris sur le réseau aérien !"}]'}
execute if score #random temp matches 0..49 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch tch

execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"Franchement, je ne saurais pas vous dire. Permettez-moi simplement de vous rappeler que l\'utilisation de tabac est proscrite sur l\'ensemble du réseau, et peut vous exposer à une amende de la part d\'un contrôleur !"',cliquable:'[{"text":"Franchement, je ne saurais pas vous dire. Permettez-moi simplement de vous rappeler que l\'utilisation de tabac est proscrite sur l\'ensemble du réseau, et peut vous exposer à une amende de la part d\'un "},{"text":"contrôleur","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1160"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des contrôleurs TCH."}]}},{"text":" !"}]'}
execute if score #random temp matches 50..99 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch/tarifs controleur
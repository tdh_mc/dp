# On enregistre le fait qu'on a répondu
tag @s add TabacDonne

# On choisit une phrase pour la question et une autre pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous auriez du tabac à me dépanner ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Dites, je sais que ce n\'est pas l\'endroit, mais j\'aurais vraiment besoin de tabac..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Je viens de me rendre compte que je n\'ai plus de tabac. Vous auriez un petit quelque chose à me vendre ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Vous auriez un peu de tabac pour moi ?"'

function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Je connais la vie, ne vous inquiétez pas. J\'ai toujours quelques cigarettes en stock pour les négligents..."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Ce n\'est pas mon fonds de commerce, mais j\'en garde toujours un peu au cas où."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je n\'ai que des cigarettes, mais c\'est déjà mieux que rien."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"J\'ai juste quelques cigarettes. Bon, je peux bien vous en vendre une ou deux..."'

# On active le mode réponse chez le tavernier (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 2210

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les différentes options possibles
tag @s add Options
# Les cigarettes sont toujours disponibles à des prix ne défiant pas, à ma connaissance, la moindre concurrence
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Cigarette ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 101"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x2","clickEvent":{"action":"run_command","value":"/trigger answer set 102"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"4 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# On a aussi la réponse générique "Non, je ne veux rien"
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Je n\'en ai pas tant besoin que ça, finalement.","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Changer d\'avis."}]}}]'
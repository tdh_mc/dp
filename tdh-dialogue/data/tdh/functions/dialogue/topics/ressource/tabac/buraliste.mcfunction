# On enregistre le fait qu'on a répondu
tag @s add TabacDonne

# On choisit une phrase pour la question et une autre pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..39 run data modify storage tdh:dialogue question set value '"Vous auriez du tabac à me vendre ?"'
execute if score #random temp matches 40..69 run data modify storage tdh:dialogue question set value '"Il me faudrait de quoi fumer ! Vous avez encore du stock ?"'
execute if score #random temp matches 70..99 run data modify storage tdh:dialogue question set value '"Vous auriez un peu de tabac pour moi ?"'

function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Bien sûr ! Qu\'est-ce qu\'il vous fallait ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Assurément ! Voyez plutôt."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Vous avez besoin de cigarettes, ou de tabac pour votre pipe ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"J\'ai tout ce qu\'il vous faut : cigarettes au détail, ou tabac à pipe !"'

# On active le mode réponse chez le buraliste (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 2210

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les différentes options possibles
tag @s add Options
# Les cigarettes et le tabac à pipe sont toujours disponibles
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Cigarette ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 101"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x5","clickEvent":{"action":"run_command","value":"/trigger answer set 105"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"4 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x10","clickEvent":{"action":"run_command","value":"/trigger answer set 110"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"7 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x20","clickEvent":{"action":"run_command","value":"/trigger answer set 120"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"10 fer","color":"gray","bold":"true"},{"text":"."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Tabac à pipe ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 201"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x5","clickEvent":{"action":"run_command","value":"/trigger answer set 205"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"3 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x10","clickEvent":{"action":"run_command","value":"/trigger answer set 210"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x20","clickEvent":{"action":"run_command","value":"/trigger answer set 220"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"8 fer","color":"gray","bold":"true"},{"text":"."}]}}]'
# Le briquet et la pipe aussi
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Briquet ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 501"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Pipe ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 601"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# On a aussi la réponse générique "Non, je ne veux rien"
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"En fait, je ne veux rien.","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Changer d\'avis."}]}}]'
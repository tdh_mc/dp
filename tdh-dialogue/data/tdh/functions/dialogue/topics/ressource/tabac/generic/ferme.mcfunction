# On enregistre le fait qu'on a répondu
tag @s add TabacDonne

# On récupère la position de la ferme
execute store result score @s deltaX run data get entity @e[type=item_frame,tag=Ferme,sort=nearest,limit=1] Pos[0]
execute store result score @s deltaZ run data get entity @e[type=item_frame,tag=Ferme,sort=nearest,limit=1] Pos[2]
execute store result score @s posX run data get entity @s Pos[0]
execute store result score @s posZ run data get entity @s Pos[2]
# On fait le calcul de la distance
function tdh:dialogue/calcul/distance

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Je ne sais pas si vous trouverez grand chose, par ici... Essayez donc à la ferme "},{"nbt":"Item.tag.ferme.prefixe","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"nbt":"Item.tag.ferme.nom","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"text":", "},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":" d\'ici. Je ne sais plus s\'ils en font pousser, mais c\'est au moins possible !"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Eh bien, je ne sais pas trop... Essayez donc voir à la ferme "},{"nbt":"Item.tag.ferme.prefixe","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"nbt":"Item.tag.ferme.nom","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"text":". C\'est "},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":" d\'ici, vous ne devriez pas la louper. Je ne sais pas s\'ils font du tabac, mais ça vaut peut-être bien le coup d\'essayer... C\'est à vous de voir."}]'
# On choisit une phrase pour la réponse
function tdh:random

# Phrases où le maître d'hôtel ne propose pas de réserver pour plus tard
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Je vous présente mes excuses, mais il n\'y a plus le moindre lit de libre."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Toutes mes excuses... il n\'y a pas de chambre libre pour l\'instant."'
# Phrases où il propose de réserver pour plus tard
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value {basique:'"Je suis vraiment navré, c\'est complet pour la nuit. Si vous prévoyez de revenir par ici, vous pouvez réserver votre prochain séjour !"',cliquable:'[{"text":"Je suis vraiment navré, c\'est complet pour la nuit. Si vous prévoyez de revenir par ici, vous pouvez "},{"text":"réserver","color":"gold","hoverEvent":{"action":"show_text","contents":"Réserver une nuit d\'hôtel"},"clickEvent":{"action":"run_command","value":"/trigger topic set 384"}},{"text":" votre prochain séjour !"}]'}
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value {basique:'"Je suis vraiment désolé, toutes les places sont prises. N\'hésitez pas à réserver la prochaine fois !"',cliquable:'[{"text":"Je suis vraiment désolé, toutes les places sont prises. N\'hésitez pas à "},{"text":"réserver","color":"gold","hoverEvent":{"action":"show_text","contents":"Réserver une nuit d\'hôtel"},"clickEvent":{"action":"run_command","value":"/trigger topic set 384"}},{"text":" la prochaine fois !"}]'}
# On débloque le sujet "réserver" si la réponse en parlait
execute if score #random temp matches 50..99 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/auberge reserver
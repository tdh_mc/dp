# On enregistre le fait qu'on a répondu
tag @s add ChambreDonnee

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 if score #temps currentTick matches 5500..16500 run data modify storage tdh:dialogue question set value '"Vous auriez une chambre de libre ?"'
execute if score #random temp matches 0..49 unless score #temps currentTick matches 5500..16500 run data modify storage tdh:dialogue question set value '"Vous auriez une chambre pour cette nuit ?"'
execute if score #random temp matches 50..69 run data modify storage tdh:dialogue question set value '"Si vous avez un lit de libre, j\'aimerais bien vous le prendre !"'
execute if score #random temp matches 70..99 run data modify storage tdh:dialogue question set value '"Tous vos lits sont occupés, ou il reste de la place ?"'


# On vérifie s'il y a une chambre libre à proximité

# Pour l'instant il n'y a jamais de chambre libre car il n'y a pas de chambre. TODO
function tdh:dialogue/topics/chambre/hotelier/pas_de_place
# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Vous ne sauriez pas où je peux trouver un lit, dans le coin ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Il y a un hôtel dans les environs ? Ou éventuellement une auberge ?"'


# Si on trouve un hôtel ou auberge aux alentours, on lance une fonction spécifique
execute if entity @e[type=item_frame,tag=Hotel,distance=..200] run function tdh:dialogue/topics/chambre/generic/hotel
execute as @s[tag=!ChambreDonnee] if entity @e[type=item_frame,tag=Auberge,distance=..200] run function tdh:dialogue/topics/chambre/generic/auberge
execute as @s[tag=!ChambreDonnee] if entity @e[type=item_frame,tag=Ferme,distance=..200] run function tdh:dialogue/topics/chambre/generic/ferme

# Sinon, on lance la fonction générique "JSP"
execute as @s[tag=!ChambreDonnee] run function tdh:dialogue/topics/chambre/generic/no
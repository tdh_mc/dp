# On génère une réponse négative aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Désolé, je ne suis pas guide touristique !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je ne crois pas qu\'on ait ça dans le coin..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas du tout. Les nuits à la belle étoile, ça forge la jeunesse !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Pas la moindre idée. Bon courage à vous !"'

# On enregistre le fait qu'on quitte la conversation
tag @s add AuRevoir
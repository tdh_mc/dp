# On enregistre le fait qu'on a répondu
tag @s add ChambreDonnee

# On récupère la position de la ferme
execute store result score @s deltaX run data get entity @e[type=item_frame,tag=Ferme,sort=nearest,limit=1] Pos[0]
execute store result score @s deltaZ run data get entity @e[type=item_frame,tag=Ferme,sort=nearest,limit=1] Pos[2]
execute store result score @s posX run data get entity @s Pos[0]
execute store result score @s posZ run data get entity @s Pos[2]
# On fait le calcul de la distance
function tdh:dialogue/calcul/distance

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Pas que je sache. Il y a bien la ferme "},{"nbt":"Item.tag.ferme.prefixe","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"nbt":"Item.tag.ferme.nom","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"text":", "},{"storage":"tdh:locate","nbt":"resultat.direction.alAu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":". Vu leur surface habitable, ça m\'étonnerait qu\'ils n\'aient pas un lit à vous prêter !"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Si vous demandez gentiment à la ferme "},{"nbt":"Item.tag.ferme.prefixe","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"nbt":"Item.tag.ferme.nom","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"text":", ils vous prêteront sans doute un lit pour la nuit. C\'est en direction "},{"storage":"tdh:locate","nbt":"resultat.direction.deDu"},{"storage":"tdh:locate","nbt":"resultat.direction.nom"},{"text":", vous y serez vite."}]'
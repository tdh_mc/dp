# On enregistre le fait qu'on a répondu
tag @s add ChambreDonnee

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# Phrases s'il fait jour
execute if score #random temp matches 0..99 if score #temps currentTick matches 6000..16500 run data modify storage tdh:dialogue question set value '"Vous auriez une chambre de libre ?"'
# Phrases s'il fait nuit
execute if score #random temp matches 0..49 unless score #temps currentTick matches 6000..16500 run data modify storage tdh:dialogue question set value '"Vous auriez une chambre pour la nuit ?"'
execute if score #random temp matches 50..79 unless score #temps currentTick matches 6000..16500 run data modify storage tdh:dialogue question set value '[{"text":"J\'espère qu\'on dort "},{"entity":"@e[type=item_frame,tag=Auberge,sort=nearest,limit=1]","nbt":"Item.tag.auberge.alAu"},{"entity":"@e[type=item_frame,tag=Auberge,sort=nearest,limit=1]","nbt":"Item.tag.auberge.nom"},{"text":" aussi bien qu\'on y mange ! Si vous avez la place de m\'accueillir, bien sûr."}]'
execute if score #random temp matches 80..99 unless score #temps currentTick matches 6000..16500 run data modify storage tdh:dialogue question set value '"Eh bien, ce n\'est pas tout ça, mais il se fait sommeil ! Il vous reste de la place ?"'


# On vérifie s'il y a une chambre libre à proximité

# Pour l'instant il n'y a jamais de chambre libre car il n'y a pas de chambre. TODO
function tdh:dialogue/topics/chambre/aubergiste/pas_de_place
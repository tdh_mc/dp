# Ce sujet fonctionne différemment selon la personne qu'on interroge.

# On exécute la bonne fonction selon le tag
# Une fois qu'on est passés dans une fonction, elle nous donnera le tag RepasDonne, pour éviter les checks redondants
execute as @s[tag=Aubergiste] run function tdh:dialogue/topics/manger/aubergiste
execute as @s[tag=Tavernier,tag=!RepasDonne] run function tdh:dialogue/topics/manger/tavernier
execute as @s[tag=Pecheur,tag=!RepasDonne] run function tdh:dialogue/topics/manger/pecheur
execute as @s[tag=Agriculteur,tag=!RepasDonne] run function tdh:dialogue/topics/manger/agriculteur
execute as @s[tag=Eleveur,tag=!RepasDonne] run function tdh:dialogue/topics/manger/eleveur
execute as @s[tag=!RepasDonne] run function tdh:dialogue/topics/manger/generic

# On supprime le tag évitant les checks redondants
tag @s remove RepasDonne
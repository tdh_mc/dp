# On affiche un "refus" générique du NPC
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..19 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas de quoi vous me parlez."'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue reponse set value '"Je n\'ai rien compris de ce que vous me racontez."'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue reponse set value '"Pourriez-vous aller importuner quelqu\'un d\'autre avec votre charabia ?"'
execute if score #random temp matches 60..89 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas pourquoi vous me parlez de ça."'
execute if score #random temp matches 90..99 run data modify storage tdh:dialogue reponse set value '"On se connaît ?"'

# Fin de conversation
tag @s add AuRevoir
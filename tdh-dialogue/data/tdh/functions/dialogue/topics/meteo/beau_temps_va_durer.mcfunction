# On génère une phrase selon l'appréciation des précipitations
execute if score @s likesRain matches ..19 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui ! Ça devrait durer toute la semaine... au moins ! J\'adore quand le soleil brille, pas vous ?"}]'

execute if score @s likesRain matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Et normalement, le temps va rester au beau fixe ! Je croise les doigts."}]'

execute if score @s likesRain matches 40..59 unless score #temps dateMois matches 3..11 run data modify storage tdh:dialogue reponse set value '[{"text":"Ça devrait rester au beau fixe cette semaine. En principe..."}]'
execute if score @s likesRain matches 40..59 if score #temps dateMois matches 3..4 run data modify storage tdh:dialogue reponse set value '[{"text":"Ça devrait rester au beau fixe cette semaine. Mais en cette saison, je ne fais confiance à aucune prévision..."}]'
execute if score @s likesRain matches 40..59 if score #temps dateMois matches 5..8 run data modify storage tdh:dialogue reponse set value '[{"text":"C\'est la moindre des choses, en cette saison !"}]'
execute if score @s likesRain matches 40..59 if score #temps dateMois matches 9..11 run data modify storage tdh:dialogue reponse set value '[{"text":"Pourvu que ça dure ! Même si nous sommes en "},{"storage":"tdh:datetime","nbt":"date.mois.min"},{"text":", le ciel est censé rester dégagé toute la semaine."}]'

execute if score @s likesRain matches 60..79 run data modify storage tdh:dialogue reponse set value '[{"text":"Et ça risque bien de durer... C\'est déprimant."}]'

execute if score @s likesRain matches 80.. run data modify storage tdh:dialogue reponse set value '[{"text":"Du soleil... Du soleil "},{"text":"toute","bold":"true"},{"text":" la semaine ! Ça n\'est pas humain..."}]'
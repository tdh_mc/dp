# On affiche une phrase selon l'appréciation des précipitations
execute if score #temps dateMois matches 3..5 if score @s likesRain matches ..44 run data modify storage tdh:dialogue reponse set value '[{"text":"Vous savez comment sont les mois "},{"storage":"tdh:datetime","nbt":"date.mois.deDu"},{"storage":"tdh:datetime","nbt":"date.mois.min"},{"text":"... La pluie va et vient, s\'arrête, reprend... J\'ai hâte que l\'été arrive !"}]'
execute if score #temps dateMois matches 3..5 if score @s likesRain matches 45.. run data modify storage tdh:dialogue reponse set value '[{"text":"Eh bien, nous sommes en "},{"storage":"tdh:datetime","nbt":"date.mois.min"},{"text":", et cette semaine nous avons de belles giboulées. Profitez-en tant qu\'elles sont là ! Vous les regretterez quand l\'été viendra."}]'

execute unless score #temps dateMois matches 3..5 if score @s likesRain matches ..19 run data modify storage tdh:dialogue reponse set value '[{"text":"Franchement, je ne sais pas vous dire... À votre place, je resterais à l\'abri !"}]'

execute unless score #temps dateMois matches 3..5 if score @s likesRain matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Ça m\'étonnerait... Ou si ça s\'arrête, ça m\'étonnerait que ça ne revienne pas. Restez plutôt au chaud !"}]'

execute unless score #temps dateMois matches 3..5 if score @s likesRain matches 40..59 run data modify storage tdh:dialogue reponse set value '[{"text":"Peut-être que oui, peut-être que non. Dans le doute, couvrez-vous."}]'

execute unless score #temps dateMois matches 3..5 if score @s likesRain matches 60..79 if entity @e[type=villager,distance=10..99] run data modify storage tdh:dialogue reponse set value '[{"text":"Eh bien, "},{"selector":"@e[type=villager,distance=10..99,sort=random,limit=1]"},{"text":" m\'a dit que oui, mais je n\'en suis pas si sûr. J\'espère que ça va durer !"}]'
execute unless score #temps dateMois matches 3..5 if score @s likesRain matches 60..79 unless entity @e[type=villager,distance=10..99] run data modify storage tdh:dialogue reponse set value '[{"text":"Je n\'en suis pas si sûr. J\'espère vraiment que ça va durer !"}]'

execute unless score #temps dateMois matches 3..5 if score @s likesRain matches 80.. run data modify storage tdh:dialogue reponse set value '[{"text":"En tout cas, je souhaite que ça continue !"}]'
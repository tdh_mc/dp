# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Belle journée, pas vrai ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Beau temps, vous ne trouvez pas ?"'

# Si le CGC a prévu du beau temps cette semaine, on affiche une réponse joviale
execute if score #meteo_calme probaChgmt matches ..500 run function tdh:dialogue/topics/meteo/beau_temps_va_durer
# Sinon, on affiche une réponse avec la météo prévue
execute if score #meteo_calme probaChgmt matches 501.. run function tdh:dialogue/topics/meteo/beau_temps_va_pas_durer
# On affiche une phrase selon l'appréciation des précipitations
execute if score @s likesRain matches ..19 run data modify storage tdh:dialogue reponse set value '[{"text":"Heureusement, ça ne devrait pas durer !"}]'

execute if score @s likesRain matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Heureusement, il va s\'arrêter. On a de la chance cette fois-ci..."}]'

execute if score @s likesRain matches 40..59 run data modify storage tdh:dialogue reponse set value '[{"text":"Au moins, ça devrait s\'arrêter bientôt."}]'

execute if score @s likesRain matches 60..79 run data modify storage tdh:dialogue reponse set value '[{"text":"Malheureusement, c\'est bientôt fini..."}]'

execute if score @s likesRain matches 80.. run data modify storage tdh:dialogue reponse set value '[{"text":"Il va bientôt s\'arrêter, c\'est dommage. J\'adore regarder les éclairs..."}]'
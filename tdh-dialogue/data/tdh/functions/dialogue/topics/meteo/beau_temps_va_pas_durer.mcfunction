# On génère une phrase selon l'appréciation des précipitations
execute if score @s likesRain matches ..19 if score #temps dateMois matches 9..11 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui... mais ça ne va pas durer. En même temps, en plein mois "},{"storage":"tdh:datetime","nbt":"date.mois.deDu"},{"storage":"tdh:datetime","nbt":"date.mois.min"},{"text":", on pouvait s\'y attendre..."}]'
execute if score @s likesRain matches ..19 if score #temps dateMois matches 6..8 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui... mais ça ne va pas durer. Il va pleuvoir bientôt ! Il n\'y a plus de saisons !"}]'
execute if score @s likesRain matches ..19 unless score #temps dateMois matches 6..11 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui... mais l\'éclaircie va être de courte durée. Quand donc reverrai-je des journées plus ensoleillées ?"}]'

execute if score @s likesRain matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Pour l\'instant... mais méfiez-vous, la pluie est en chemin !"}]'

execute if score @s likesRain matches 40..59 unless score #temps dateMois matches 4..11 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui, mais il va pleuvoir. Enfin, c\'est ce qu\'il me semble..."}]'
execute if score @s likesRain matches 40..59 if score #temps dateMois matches 4 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui, mais il va pleuvoir. En avril, ne vous découvrez pas d\'un fil !"}]'
execute if score @s likesRain matches 40..59 if score #temps dateMois matches 5..8 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui, mais il va pleuvoir, méfiance..."}]'
execute if score @s likesRain matches 40..59 if score #temps dateMois matches 9..11 run data modify storage tdh:dialogue reponse set value '[{"text":"Ce n\'est qu\'une accalmie. J\'entends déjà la pluie qui vient..."}]'

execute if score @s likesRain matches 60..79 run data modify storage tdh:dialogue reponse set value '[{"text":"Ce n\'est pas ça que j\'appelle une \\"belle journée\\". Il y a bien trop de soleil ! Mais la pluie va arriver, encore un peu de patience..."}]'

execute if score @s likesRain matches 80.. run data modify storage tdh:dialogue reponse set value '[{"text":"Vous rigolez ? Il va pleuvoir. Je n\'attends que ça."}]'
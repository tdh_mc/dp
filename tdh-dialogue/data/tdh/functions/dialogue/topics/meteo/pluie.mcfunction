# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Cette pluie, ça va durer longtemps, vous croyez ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Sacrée pluie qu\'il y a là dehors !"'

# Selon les chances que la pluie s'arrête, on a une réponse différente
execute if score #meteo_pluie probaCalme matches 2000.. if score #meteo_calme probaPluie matches ..1999 run function tdh:dialogue/topics/meteo/pluie_va_pas_durer

execute if score #meteo_pluie probaCalme matches 2000.. if score #meteo_calme probaPluie matches 2000.. run function tdh:dialogue/topics/meteo/pluie_va_alterner

execute if score #meteo_pluie probaCalme matches ..1999 if score #meteo intensite < #meteo intensiteMax run function tdh:dialogue/topics/meteo/pluie_va_empirer
execute if score #meteo_pluie probaCalme matches ..1999 if score #meteo_pluie probaOrage matches 1000.. run function tdh:dialogue/topics/meteo/pluie_va_empirer

execute if score #meteo_pluie probaCalme matches ..1999 if score #meteo_pluie probaOrage matches ..999 if score #meteo intensite >= #meteo intensiteMax run function tdh:dialogue/topics/meteo/pluie_va_durer
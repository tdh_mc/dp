# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Bon sang, quelle tempête !"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"C\'est un sacré orage, pas vrai ?"'

# Selon les chances que la pluie s'arrête, on a une réponse différente
execute if score #meteo_orage probaChgmt matches 2000.. if score #meteo_calme probaChgmt matches ..1999 run function tdh:dialogue/topics/meteo/orage_va_pas_durer

execute if score #meteo_pluie probaCalme matches ..1999 if score #meteo intensite < #meteo intensiteMax run function tdh:dialogue/topics/meteo/orage_va_empirer

execute if score #meteo_pluie probaCalme matches ..1999 if score #meteo intensite >= #meteo intensiteMax run function tdh:dialogue/topics/meteo/orage_va_durer
# On affiche une phrase selon l'appréciation des précipitations
execute if score @s likesRain matches ..19 run data modify storage tdh:dialogue reponse set value '[{"text":"Heureusement, non, ça ne va pas durer !"}]'

execute if score @s likesRain matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Non, on devrait avoir de la chance cette fois-ci..."}]'

execute if score @s likesRain matches 40..59 run data modify storage tdh:dialogue reponse set value '[{"text":"Non, elle devrait cesser bientôt."}]'

execute if score @s likesRain matches 60..79 run data modify storage tdh:dialogue reponse set value '[{"text":"Non, malheureusement, c\'est bientôt fini..."}]'

execute if score @s likesRain matches 80.. run data modify storage tdh:dialogue reponse set value '[{"text":"Eh non, pas cette fois. Mais je ne désespère pas d\'avoir une vraie semaine de pluie, un de ces jours..."}]'
# On affiche une phrase selon l'appréciation des précipitations
execute if score @s likesRain matches ..19 run data modify storage tdh:dialogue reponse set value '[{"text":"Oh si, ça va durer, croyez-moi... J\'en suis le premier désolé."}]'

execute if score @s likesRain matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Si, ça risque de durer un moment... Et moi qui déteste être trempé !"}]'

execute if score @s likesRain matches 40..59 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui, ça ne s\'arrêtera pas de sitôt. Il va falloir être patient..."}]'

execute if score @s likesRain matches 60..79 unless score #temps dateMois matches 5..8 run data modify storage tdh:dialogue reponse set value '[{"text":"Si, ça va continuer un bon moment. Vous n\'aimez pas la pluie ? Ça n\'a pourtant rien d\'anormal en "},{"nbt":"date.mois.min","storage":"tdh:datetime"},{"text":"..."}]'
execute if score @s likesRain matches 60..79 if score #temps dateMois matches 5..8 run data modify storage tdh:dialogue reponse set value '[{"text":"Si, ça va continuer un bon moment. Enfin, moi, ça ne me dérange pas..."}]'

execute if score @s likesRain matches 80.. run data modify storage tdh:dialogue reponse set value '[{"text":"Bien sûr que si, ça va durer. Vous n\'aimez pas la pluie ? Ça n\'a pourtant rien d\'anormal en "},{"nbt":"date.mois.min","storage":"tdh:datetime"},{"text":"..."}]'
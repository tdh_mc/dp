# On affiche une phrase selon l'appréciation des précipitations
execute if score @s likesRain matches ..19 run data modify storage tdh:dialogue reponse set value '[{"text":"Ça va même empirer ! Restez à l\'intérieur si vous le pouvez."}]'

execute if score @s likesRain matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Oh que si, ça risque même de s\'intensifier. Si vous n\'avez rien d\'important à faire cette semaine, abritez-vous..."}]'

execute if score @s likesRain matches 40..69 run data modify storage tdh:dialogue reponse set value '[{"text":"Si, et ça risque même d\'empirer. Restez prudent."}]'

execute if score @s likesRain matches 70.. if score #meteo intensiteMax matches ..3 run data modify storage tdh:dialogue reponse set value '[{"text":"Si, ça va continuer un bon moment. Vous n\'aimez pas la pluie ?"}]'
execute if score @s likesRain matches 70.. if score #meteo intensiteMax matches 4 run data modify storage tdh:dialogue reponse set value '[{"text":"Si, ça va continuer un bon moment. Faites attention si vous sortez ; je suis le premier à aimer quand il pleut, mais là ça risque d\'être violent."}]'
# On affiche une phrase selon l'appréciation des précipitations
execute if score @s likesRain matches ..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Et ça va encore empirer ! Restez à l\'abri, coûte que coûte."}]'

execute if score @s likesRain matches 40..69 run data modify storage tdh:dialogue reponse set value '[{"text":"Et ça risque encore d\'empirer ! Restez très prudent."}]'

execute if score @s likesRain matches 70.. if score #meteo intensiteMax matches ..3 run data modify storage tdh:dialogue reponse set value '[{"text":"Et ça va continuer un bon moment. Vous n\'aimez pas le tonnerre ?"}]'
execute if score @s likesRain matches 70.. if score #meteo intensiteMax matches 4 run data modify storage tdh:dialogue reponse set value '[{"text":"Et ça va continuer un bon moment. Faites attention si vous sortez ; je suis le premier à aimer quand il pleut, mais là ça risque d\'être violent."}]'
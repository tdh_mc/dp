# On affiche une phrase selon l'appréciation des précipitations
execute if score @s likesRain matches ..19 run data modify storage tdh:dialogue reponse set value '[{"text":"Ça va durer un moment ! Restez à l\'intérieur si vous le pouvez."}]'

execute if score @s likesRain matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Ça va durer encore un moment. Si vous n\'avez rien d\'important à faire cette semaine, abritez-vous..."}]'

execute if score @s likesRain matches 40..69 run data modify storage tdh:dialogue reponse set value '[{"text":"Et ça risque bien de durer. Restez prudent."}]'

execute if score @s likesRain matches 70.. if score #meteo intensiteMax matches ..3 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui, ça va continuer un bon moment. Vous aimez l\'orage, vous aussi ?"}]'
execute if score @s likesRain matches 70.. if score #meteo intensiteMax matches 4 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui, ça va continuer un bon moment. Faites attention si vous sortez ; je suis le premier à aimer l\'orage, mais là ça risque d\'être violent."}]'
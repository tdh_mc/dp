# On choisit une phrase pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Bon voyage sur les lignes TCH !"'
execute if score #random temp matches 25..49 if score #temps timeOfDay matches 1 run data modify storage tdh:dialogue reponse set value '"Je vous souhaite une excellente journée !"'
execute if score #random temp matches 25..49 if score #temps timeOfDay matches 4 run data modify storage tdh:dialogue reponse set value '"Je vous souhaite une excellente matinée !"'
execute if score #random temp matches 25..49 if score #temps timeOfDay matches 3 run data modify storage tdh:dialogue reponse set value '"Je vous souhaite une excellente nuit !"'
execute if score #random temp matches 25..49 if score #temps timeOfDay matches 2 run data modify storage tdh:dialogue reponse set value '"Je vous souhaite une excellente soirée !"'
execute if score #random temp matches 50..74 if score #temps timeOfDay matches 1 run data modify storage tdh:dialogue reponse set value '"Bonne journée, et à bientôt !"'
execute if score #random temp matches 50..74 if score #temps timeOfDay matches 2 run data modify storage tdh:dialogue reponse set value '"Bonne soirée, et à bientôt !"'
execute if score #random temp matches 50..74 if score #temps timeOfDay matches 3 run data modify storage tdh:dialogue reponse set value '"Bonne nuit, et à bientôt !"'
execute if score #random temp matches 50..74 if score #temps timeOfDay matches 4 run data modify storage tdh:dialogue reponse set value '"Bon matin, et à bientôt !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"C\'est toujours un plaisir de vous accueillir sur les lignes TCH !"'

# On enregistre le fait qu'on s'en va
tag @s add AuRevoir
# On choisit une phrase pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Au revoir ! À bientôt !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"À la prochaine !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Bonjour chez vous !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Au plaisir de vous recroiser !"'

# On enregistre le fait qu'on s'en va
tag @s add AuRevoir
# On enregistre le fait qu'on a répondu
tag @s add BoissonDonnee

# On choisit une phrase pour la question et une autre pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..39 if score #temps currentTick matches 4500..17500 run data modify storage tdh:dialogue question set value '"Bonjour, aubergiste ! Vous auriez quelque chose à boire ?"'
execute if score #random temp matches 0..39 unless score #temps currentTick matches 4500..17500 run data modify storage tdh:dialogue question set value '"Bonsoir, aubergiste ! Vous auriez quelque chose à boire ?"'
execute if score #random temp matches 40..69 run data modify storage tdh:dialogue question set value '"Holà, aubergiste ! J\'ai bien envie de boire un coup !"'
execute if score #random temp matches 70..99 run data modify storage tdh:dialogue question set value '"J\'ai soif, patron ! Il me faudrait un bon verre !"'

function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Bien sûr ! Qu\'est-ce que je vous sers ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"C\'est pour ça que je suis là ! Qu\'est-ce qui vous ferait plaisir ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Qu\'est-ce qui vous ferait plaisir ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"On va vous trouver ça ! Qu\'est-ce qu\'il vous faut ?"'

# On active le mode réponse chez l'aubergiste (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 381

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les différentes options possibles
tag @s add Options
# La bière est toujours disponible, quel que soit l'établissement
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Bière ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 101"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x2","clickEvent":{"action":"run_command","value":"/trigger answer set 102"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x4","clickEvent":{"action":"run_command","value":"/trigger answer set 104"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"3 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x8","clickEvent":{"action":"run_command","value":"/trigger answer set 108"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Si la taverne a le tag "Pomme", on a le droit à du cidre
execute as @e[tag=Auberge,sort=nearest,limit=1,distance=..50] if entity @s[tag=Pomme] run data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Cidre doux ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 121"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x2","clickEvent":{"action":"run_command","value":"/trigger answer set 122"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x4","clickEvent":{"action":"run_command","value":"/trigger answer set 124"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"3 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x8","clickEvent":{"action":"run_command","value":"/trigger answer set 128"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 fer","color":"gray","bold":"true"},{"text":"."}]}}]'
execute as @e[tag=Auberge,sort=nearest,limit=1,distance=..50] if entity @s[tag=Pomme] run data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Cidre brut ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 131"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x2","clickEvent":{"action":"run_command","value":"/trigger answer set 132"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x4","clickEvent":{"action":"run_command","value":"/trigger answer set 134"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"3 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x8","clickEvent":{"action":"run_command","value":"/trigger answer set 138"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Si la taverne a le tag "Pasteque", on a le droit à du vin de melon
execute as @e[tag=Auberge,sort=nearest,limit=1,distance=..50] if entity @s[tag=Pasteque] run data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Vin de pastèque ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 201"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x2","clickEvent":{"action":"run_command","value":"/trigger answer set 202"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x4","clickEvent":{"action":"run_command","value":"/trigger answer set 204"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"3 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x8","clickEvent":{"action":"run_command","value":"/trigger answer set 208"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Si la taverne a le tag "Miel", on a le droit à de l'hydromel
execute as @e[tag=Auberge,sort=nearest,limit=1,distance=..50] if entity @s[tag=Miel] run data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Hydromel ","color":"yellow"},{"text":"x1","clickEvent":{"action":"run_command","value":"/trigger answer set 211"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x2","clickEvent":{"action":"run_command","value":"/trigger answer set 212"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x4","clickEvent":{"action":"run_command","value":"/trigger answer set 214"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"3 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x8","clickEvent":{"action":"run_command","value":"/trigger answer set 218"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"5 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Dans tous les cas, on peut aussi implorer de la bouffe (TODO)

# On a aussi la réponse générique "Non, je ne veux rien"
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"En fait, je ne veux rien.","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Changer d\'avis."}]}}]'
# On enregistre le fait qu'on a répondu
tag @s add BoissonDonnee

# On récupère la position de la ferme
execute store result score @s deltaX run data get entity @e[type=item_frame,tag=Ferme,sort=nearest,limit=1] Pos[0]
execute store result score @s deltaZ run data get entity @e[type=item_frame,tag=Ferme,sort=nearest,limit=1] Pos[2]
execute store result score @s posX run data get entity @s Pos[0]
execute store result score @s posZ run data get entity @s Pos[2]
# On fait le calcul de la distance
function tdh:dialogue/calcul/distance

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Eh bien, je ne vois pas de taverne à vous conseiller par ici... Il y a bien la ferme "},{"nbt":"Item.tag.ferme.prefixe","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"nbt":"Item.tag.ferme.nom","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"text":", "},{"nbt":"resultat.direction.alAu","storage":"tdh:locate"},{"nbt":"resultat.direction.nom","storage":"tdh:locate"},{"text":". Vous n\'avez qu\'à frapper là-bas et voir s\'ils ont de quoi vous hydrater !"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Dans le coin, vous aurez du mal à trouver une taverne. Le mieux, c\'est que vous alliez frapper "},{"nbt":"Item.tag.ferme.alAu","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"nbt":"Item.tag.ferme.nom","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1]"},{"text":" ! C\'est "},{"nbt":"resultat.direction.alAu","storage":"tdh:locate"},{"nbt":"resultat.direction.nom","storage":"tdh:locate"},{"text":" d\'ici. Vu la taille de leurs champs, ça m\'étonnerait qu\'ils manquent de boissons..."}]'
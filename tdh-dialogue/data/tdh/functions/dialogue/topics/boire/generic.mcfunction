# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous ne sauriez pas où je peux trouver à boire, dans le coin ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Ne trouve-t-on donc aucune taverne dans les environs ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Où puis-je me rafraîchir le gosier, par ici ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Cet endroit est charmant, mais il y fait bien soif. Vous savez où je pourrais trouver une taverne ?"'


# Si on trouve une taverne ou auberge aux alentours, on lance une fonction spécifique
execute if entity @e[type=item_frame,tag=Taverne,distance=..200] run function tdh:dialogue/topics/boire/generic/taverne
execute as @s[tag=!BoissonDonnee] if entity @e[type=item_frame,tag=Auberge,distance=..200] run function tdh:dialogue/topics/boire/generic/auberge
execute as @s[tag=!BoissonDonnee] if entity @e[type=item_frame,tag=Ferme,distance=..200] run function tdh:dialogue/topics/boire/generic/ferme

# Sinon, on lance la fonction générique "JSP"
execute as @s[tag=!BoissonDonnee] run function tdh:dialogue/topics/boire/generic/no
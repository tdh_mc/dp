# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Je ne me promène pas avec des torches en rab, désolé. Vous imaginez la place que ça prendrait ? Je n\'ai pas onze bras !"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Si je me promenais avec tout le fatras dont tous les inconnus qui croisent ma route ont besoin, il me faudrait une carriole..."'

# On termine la conversation
tag @s add AuRevoir
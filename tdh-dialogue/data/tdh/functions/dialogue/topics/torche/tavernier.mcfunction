# On enregistre le fait qu'on a répondu
tag @s add TorcheDonnee

# Selon la météo, on a des branches différentes du dialogue
# S'il fait beau, on va dire qu'on a des torches et demander si on part à l'aventure
execute if score #meteo actuelle matches 1 run function tdh:dialogue/topics/torche/tavernier/beau
# S'il fait mauvais, on va proposer de rester au moins boire un verre en attendant que ça s'arrête
execute if score #meteo actuelle matches 2.. run function tdh:dialogue/topics/torche/tavernier/mauvais
# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Bien sûr, on doit pouvoir vous trouver ça. Il part à l\'aventure ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"On doit bien avoir ça sous une étagère... Il est aventurier ?"'

# On active le mode réponse chez le tavernier (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 383

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer


# Les différents choix possibles sont affichés à la personne qui dialogue
# On exagère sur notre statut d'aventurier
data modify storage tdh:dialogue reponse set value '[{"text":"Exactement. J\'ai de nombreuses quêtes héroïques à accomplir.","clickEvent":{"action":"run_command","value":"/trigger answer set 1000"},"hoverEvent":{"action":"show_text","value":[{"text":"Vanter vos futurs exploits."}]}}]'
# On minimise notre statut d'aventurier
data modify storage tdh:dialogue reponse set value '[{"text":"Oh, rien d\'incroyable, vous savez...","clickEvent":{"action":"run_command","value":"/trigger answer set 1050"},"hoverEvent":{"action":"show_text","value":[{"text":"Minimiser vos compétences."}]}}]'
# On est froid et on demande juste notre torche
data modify storage tdh:dialogue reponse set value '[{"text":"Ça vous regarde ?","clickEvent":{"action":"run_command","value":"/trigger answer set 1100"},"hoverEvent":{"action":"show_text","value":[{"text":"Vous n\'avez pas le temps de bavarder avec des manants."}]}}]'
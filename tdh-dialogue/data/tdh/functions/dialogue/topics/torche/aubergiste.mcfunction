# On enregistre le fait qu'on a répondu
tag @s add TorcheDonnee

# Selon l'heure de la journée et la météo, on a des branches différentes du dialogue
# S'il fait nuit et beau, on va insister pour qu'on dorme ici plutôt que de s'aventurer dehors
execute if score #temps timeOfDay matches 2..3 if score #meteo actuelle matches 1 run function tdh:dialogue/topics/torche/aubergiste/nuit_beau
# S'il fait nuit et mauvais, on va s'inquiéter qu'on veuille sortir de nuit par ce temps
execute if score #temps timeOfDay matches 2..3 if score #meteo actuelle matches 2.. run function tdh:dialogue/topics/torche/aubergiste/nuit_mauvais
# S'il fait jour et beau, on va dire qu'on a des torches et demander si on part à l'aventure
execute unless score #temps timeOfDay matches 2..3 if score #meteo actuelle matches 1 run function tdh:dialogue/topics/torche/aubergiste/jour_beau
# S'il fait jour et mauvais, on va proposer de rester au moins boire un verre en attendant que ça s'arrête
execute unless score #temps timeOfDay matches 2..3 if score #meteo actuelle matches 2.. run function tdh:dialogue/topics/torche/aubergiste/jour_mauvais
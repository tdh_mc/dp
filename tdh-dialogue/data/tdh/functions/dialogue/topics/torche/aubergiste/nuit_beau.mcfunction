# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Vous ne voulez pas plutôt une chambre ? Les nuits sont fraîches en cette saison..."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Vous ne voulez pas dormir ici ? Vous allez vous rendre malade."'

# On active le mode réponse chez l'aubergiste (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 383

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer


# Les différents choix possibles sont affichés à la personne qui dialogue
# On accepte la demande de l'aubergiste et on demande à prendre une chambre
data modify storage tdh:dialogue reponse set value '[{"text":"Vous avez sans doute raison... Combien ça me coûterait ?","clickEvent":{"action":"run_command","value":"/trigger answer set 382"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander une chambre pour la nuit."}]}}]'

# On tente d'amadouer l'aubergiste
data modify storage tdh:dialogue reponse set value '[{"text":"Votre auberge a l\'air superbe, mais je suis très pressé.","clickEvent":{"action":"run_command","value":"/trigger answer set 10"},"hoverEvent":{"action":"show_text","value":[{"text":"Tenter d\'amadouer "},{"selector":"@s"},{"text":" en le flattant."}]}}]'
# On tente de convaincre l'aubergiste
data modify storage tdh:dialogue reponse set value '[{"text":"Je n\'ai vraiment pas le temps. Votre prix sera le mien !","clickEvent":{"action":"run_command","value":"/trigger answer set 20"},"hoverEvent":{"action":"show_text","value":[{"text":"Tenter de convaincre "},{"selector":"@s"},{"text":" en faisant appel à son sens des affaires."}]}}]'
# On tente de menacer l'aubergiste
data modify storage tdh:dialogue reponse set value '[{"text":"Ce n\'est pas un aubergiste qui se mettra en travers de mon chemin !","clickEvent":{"action":"run_command","value":"/trigger answer set 30"},"hoverEvent":{"action":"show_text","value":[{"text":"Tenter de faire peur à "},{"selector":"@s"},{"text":" pour qu\'il se montre plus docile."}]}}]'

# On a aussi la réponse générique "Non, je ne veux rien"
data modify storage tdh:dialogue reponse set value '[{"text":"Je me débrouillerai sans vous, alors.","color":"gold","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Laisser tomber."}]}}]'
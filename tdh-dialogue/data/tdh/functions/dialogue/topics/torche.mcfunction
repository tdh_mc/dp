# Ce sujet fonctionne différemment selon la personne qu'on interroge.
# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Vous n\'auriez pas une torche à me donner ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Vous n\'auriez pas une torche pour moi ?"'

# On exécute la bonne fonction selon le tag
# Une fois qu'on est passés dans une fonction, elle nous donnera le tag TorcheDonnee, pour éviter les checks redondants
execute as @s[tag=Aubergiste] run function tdh:dialogue/topics/torche/aubergiste
execute as @s[tag=Tavernier,tag=!TorcheDonnee] run function tdh:dialogue/topics/torche/tavernier
execute as @s[tag=!TorcheDonnee] run function tdh:dialogue/topics/torche/generic

# On supprime le tag évitant les checks redondants
tag @s remove TorcheDonnee
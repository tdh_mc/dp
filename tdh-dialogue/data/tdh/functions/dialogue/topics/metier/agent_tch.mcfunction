# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Je suis agent TCH. Les gens s\'en rendent compte d\'habitude !"',cliquable:'[{"text":"Je suis agent "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de TCH"}}},{"text":". Les gens s\'en rendent compte d\'habitude !"}]'}
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"En tant qu\'agent TCH, c\'est moi qui fais fonctionner la station "},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]"},{"text":" : j\'assiste les clients, je répare les distributeurs cassés, je fais le ménage..."}]',cliquable:'[{"text":"En tant qu\'agent "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de TCH"}}},{"text":", c\'est moi qui fais fonctionner la station "},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]"},{"text":" : j\'assiste les clients, je répare les distributeurs cassés, je fais le ménage..."}]'}
# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Je suis employé par la banque nationale du Hameau. C\'est une institution très importante, des biens et des services de toutes les Terres du Hameau s\'y arrêtent pour se reproduire !"',cliquable:'[{"text":"Je suis employé par la banque nationale du "},{"text":"Hameau","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 401"},"hoverEvent":{"action":"show_text","value":{"text":"Parler du Hameau"}}},{"text":". C\'est une institution très importante, des biens et des services de toutes les "},{"text":"Terres du Hameau","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 400"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des Terres du Hameau"}}},{"text":" s\'y arrêtent pour se reproduire !"}]'}
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"J\'aime dire à mes amis que je suis banquier, même si ce n\'est pas l\'intitulé exact du poste. « Guichetier du Hameau, » c\'est quand même moins élégant."',cliquable:'[{"text":"J\'aime dire à mes amis que je suis banquier, même si ce n\'est pas l\'intitulé exact du poste. « Guichetier du "},{"text":"Hameau","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 401"},"hoverEvent":{"action":"show_text","value":{"text":"Parler du Hameau"}}},{"text":", » c\'est quand même moins élégant."}]'}
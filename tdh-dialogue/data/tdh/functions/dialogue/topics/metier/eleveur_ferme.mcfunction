# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# Enregistrement des données :
# /data merge entity XXX {Item:{tag:{ferme:{nom:"Nom de la ferme",deDu:"du ",alAu:"au ",prefixe:"du "}}}}

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"J\'élève mes bêtes ici, à la ferme "},{"nbt":"Item.tag.ferme.prefixe","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1,distance=..60]"},{"nbt":"Item.tag.ferme.nom","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1,distance=..60]"},{"text":". N\'hésitez pas à rester ici un moment, nous sommes de bonne compagnie – et puis, c\'est un bel endroit !"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Eh bien, je suis éleveur ; avec ma famille, on s\'occupe "},{"nbt":"Item.tag.ferme.deDu","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1,distance=..60]"},{"nbt":"Item.tag.ferme.nom","entity":"@e[type=item_frame,tag=Ferme,sort=nearest,limit=1,distance=..60]"},{"text":", on fournit les marchés et accessoirement les voyageurs de passage. N\'hésitez pas à rester un petit moment, on adore avoir de la visite !"}]'
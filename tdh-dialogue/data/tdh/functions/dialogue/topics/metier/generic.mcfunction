# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..19 run data modify storage tdh:dialogue reponse set value '"Je vous en pose, des questions, moi ?"'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue reponse set value '"Si on te le demande, tu diras que t\'en sais rien, hein ?"'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue reponse set value '"Je profite de la vie. Pouvez-vous en dire autant ?"'
execute if score #random temp matches 60..89 run data modify storage tdh:dialogue reponse set value '"C\'est pas vos oignons."'
execute if score #random temp matches 90..99 run data modify storage tdh:dialogue reponse set value '"Le dernier qui m\'a demandé ça n\'a pas eu de réponse non plus."'


# On quitte la conversation
tag @s add AuRevoir
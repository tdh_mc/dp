# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Je suis éleveur. Ces citadins de "},{"selector":"@e[tag=NomVille,distance=..200]"},{"text":" ont bien de la chance que quelqu\'un décapite les porcs à leur place !"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Je suis le meilleur éleveur de "},{"selector":"@e[tag=NomVille,distance=..200]"},{"text":". En même temps, tous ces citadins sont des mauviettes. Ils m\'ont déjà collé un procès parce que mon coq chantait ! Ces nigauds !"}]'
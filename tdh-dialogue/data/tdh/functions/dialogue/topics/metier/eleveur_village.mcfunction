# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Je suis éleveur. Je fournis les habitants de "},{"selector":"@e[tag=NomVillage,distance=..200]"},{"text":" en viande et en cuir ; mais s\'il vous en faut, j\'ai toujours un peu de surplus."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Je suis le meilleur éleveur de "},{"selector":"@e[tag=NomVillage,distance=..200]"},{"text":" ; mes bêtes sont toujours très compliantes lors de leur mise en box."}]'
# On a 3 situations possibles
# Soit on est à côté d'une ferme nommée
execute if entity @e[tag=Ferme,distance=..50] run function tdh:dialogue/topics/metier/eleveur_ferme
# Soit on est à côté d'une ville ou village
execute unless entity @e[tag=Ferme,distance=..50] if entity @e[tag=NomVille,distance=..300] run function tdh:dialogue/topics/metier/eleveur_ville
execute unless entity @e[tag=Ferme,distance=..50] if entity @e[tag=NomVillage,distance=..200] run function tdh:dialogue/topics/metier/eleveur_village
# Soit on a une version générique
execute as @s[tag=!MetierDonne] run function tdh:dialogue/topics/metier/eleveur_generic
# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Je suis pêcheur. Je pêche des saumons, des truites, des carpes... Vous devriez voir ça !"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Vous ne trouverez pas pêcheur plus expérimenté dans le coin. Je connais tous les coins à poisson, les rochers à coquillages, et 94 types de nœuds différents."'
# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Eh bien, je suis aubergiste, et tel que vous me voyez, je suis dans mon auberge ! Vous prenez quelque chose à manger ?"',cliquable:'[{"text":"Eh bien, je suis aubergiste, et tel que vous me voyez, je suis dans mon auberge ! Vous prenez quelque chose à "},{"text":"manger","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 380"},"hoverEvent":{"action":"show_text","value":{"text":"Demander de la nourriture"}}},{"text":" ?"}]'}
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"Je pensais pas ça possible de pas remarquer que je suis aubergiste ! Il faut de tout pour faire un monde... Vous consommez ?"',cliquable:'[{"text":"Je pensais pas ça possible de pas remarquer que je suis aubergiste ! Il faut de tout pour faire un monde... Vous "},{"text":"consommez","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 380"},"hoverEvent":{"action":"show_text","value":{"text":"Demander de la nourriture"}}},{"text":" ?"}]'}
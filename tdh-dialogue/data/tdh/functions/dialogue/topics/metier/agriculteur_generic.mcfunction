# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Je travaille à la ferme. N\'hésitez pas à nous rendre visite quand vous êtes en voyage, nous sommes de bonne compagnie – et puis, c\'est un bel endroit !"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Eh bien, je suis agriculteur ; avec ma famille, on travaille les champs, on fournit les marchés et accessoirement les voyageurs de passage."'
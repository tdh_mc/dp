# On enregistre le fait qu'on a répondu
tag @s add MetierDonne

# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Je suis agriculteur. Les habitants de "},{"selector":"@e[tag=NomVille,sort=nearest,limit=1]"},{"text":" ont bien de la chance qu\'un type comme moi ramasse leur grain !"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Eh bien, je suis agriculteur ; avec ma famille, on s\'assure que tous les habitants de "},{"selector":"@e[tag=NomVille,sort=nearest,limit=1]"},{"text":" aient de quoi manger. Tout repose sur moi mais vous ne les entendrez pas trop chanter mes louanges..."}]'
# Ce sujet fonctionne différemment selon la personne qu'on interroge.

# On exécute la bonne fonction selon le tag
# Une fois qu'on est passés dans une fonction, elle nous donnera le tag ChambreDonnee, pour éviter les checks redondants
execute as @s[tag=Hotelier] run function tdh:dialogue/topics/chambre/hotelier
execute as @s[tag=Aubergiste,tag=!ChambreDonnee] run function tdh:dialogue/topics/chambre/aubergiste
execute as @s[tag=Agriculteur,tag=!ChambreDonnee] run function tdh:dialogue/topics/chambre/agriculteur
execute as @s[tag=Eleveur,tag=!ChambreDonnee] run function tdh:dialogue/topics/chambre/eleveur
execute as @s[tag=!ChambreDonnee] run function tdh:dialogue/topics/chambre/generic

# On supprime le tag évitant les checks redondants
tag @s remove ChambreDonnee
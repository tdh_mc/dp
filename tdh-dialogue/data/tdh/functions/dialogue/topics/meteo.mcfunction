# Ce sujet fonctionne différemment selon la personne qu'on interroge.

# Si on n'a pas encore de score d'appréciation de la météo, on en génère un
execute unless score @s likesHeat matches 0..100 run function tdh:dialogue/calcul/preferences_meteo



# On exécute une sous-fonction en fonction du temps qu'il fait actuellement
execute if score #meteo actuelle = #meteo_calme actuelle run function tdh:dialogue/topics/meteo/beau_temps
execute if score #meteo actuelle = #meteo_pluie actuelle run function tdh:dialogue/topics/meteo/pluie
execute if score #meteo actuelle = #meteo_orage actuelle run function tdh:dialogue/topics/meteo/orage
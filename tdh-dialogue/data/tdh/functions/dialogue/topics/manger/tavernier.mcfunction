# On enregistre le fait qu'on a répondu
tag @s add RepasDonne

# On choisit une phrase pour la question et une autre pour la réponse
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..39 run data modify storage tdh:dialogue question set value '"Vous auriez quelque chose à manger ?"'
execute if score #random temp matches 40..69 run data modify storage tdh:dialogue question set value '"Je sais que ce n\'est pas trop l\'endroit, mais vous auriez quelque chose à grignoter ?"'
execute if score #random temp matches 70..99 run data modify storage tdh:dialogue question set value '"J\'ai grand faim, patron ! Je sais que nous ne sommes pas à l\'auberge, mais si vous avez un petit quelque chose à manger, je vous le prends !"'

function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Eh bien, ce n\'est pas le grand luxe, mais personne ne mourra de faim "},{"entity":"@e[type=item_frame,tag=Taverne,sort=nearest,limit=1]","nbt":"Item.tag.taverne.alAu"},{"entity":"@e[type=item_frame,tag=Taverne,sort=nearest,limit=1]","nbt":"Item.tag.taverne.nom"},{"text":" ! Qu\'est-ce que je vous sers ?"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"On n\'a pas beaucoup de choix, mais y\'a quand même un minimum !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"C\'est du basique, mais ça tient au corps ! Qu\'est-ce qu\'il prendra ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"On va bien vous trouver quelque chose de consistant ! Combien il en veut ?"'

# On active le mode réponse chez le tavernier (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 380

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer


# On enregistre les différentes options possibles
tag @s add Options
# Le pain est un aliment toujours disponible, quel que soit l'établissement
data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Pain ","color":"yellow"},{"text":"x16","clickEvent":{"action":"run_command","value":"/trigger answer set 1016"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x32","clickEvent":{"action":"run_command","value":"/trigger answer set 1032"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Si la taverne a le tag "Patate", on a le droit à des frites
execute as @e[tag=Taverne,sort=nearest,limit=1,distance=..50] if entity @s[tag=Patate] run data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Frites ","color":"yellow"},{"text":"x24","clickEvent":{"action":"run_command","value":"/trigger answer set 2024"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}},{"text":"–"},{"text":"x64","clickEvent":{"action":"run_command","value":"/trigger answer set 2064"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Si la taverne a le tag "Pasteque", on a le droit à des pastèques
execute as @e[tag=Taverne,sort=nearest,limit=1,distance=..50] if entity @s[tag=Pasteque] run data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Pastèque ","color":"yellow"},{"text":"x48","clickEvent":{"action":"run_command","value":"/trigger answer set 2148"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Si la taverne a le tag "Pomme", on a le droit à des pommes
execute as @e[tag=Taverne,sort=nearest,limit=1,distance=..50] if entity @s[tag=Pomme] run data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Pommes ","color":"yellow"},{"text":"x48","clickEvent":{"action":"run_command","value":"/trigger answer set 2248"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Si la taverne a le tag "Baies", on a le droit à des baies
execute as @e[tag=Taverne,sort=nearest,limit=1,distance=..50] if entity @s[tag=Baies] run data modify storage tdh:dialogue options append value '[{"text":""},{"text":"Baies sucrées ","color":"yellow"},{"text":"x48","clickEvent":{"action":"run_command","value":"/trigger answer set 2348"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}}]'

# Dans tous les cas, on peut aussi implorer de la bouffe (TODO)

# On a aussi la réponse générique "Non, je ne veux rien"
data modify storage tdh:dialogue options append value '[{"text":"En fait, je ne veux rien.","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Changer d\'avis."}]}}]'
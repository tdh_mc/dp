# Ce sujet fonctionne différemment selon la personne qu'on interroge.
# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Vous faites quoi dans la vie ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"C\'est quoi, votre métier ?"'

# On exécute la bonne fonction selon le tag
# Une fois qu'on est passés dans une fonction, elle nous donnera le tag MetierDonne, pour éviter les checks redondants
execute as @s[tag=AgentTCH] run function tdh:dialogue/topics/metier/agent_tch
execute as @s[tag=GuichetBNH,tag=!MetierDonne] run function tdh:dialogue/topics/metier/guichet_bnh
execute as @s[tag=Aubergiste,tag=!MetierDonne] run function tdh:dialogue/topics/metier/aubergiste
execute as @s[tag=Tavernier,tag=!MetierDonne] run function tdh:dialogue/topics/metier/tavernier
execute as @s[tag=Pecheur,tag=!MetierDonne] run function tdh:dialogue/topics/metier/pecheur
execute as @s[tag=Agriculteur,tag=!MetierDonne] run function tdh:dialogue/topics/metier/agriculteur
execute as @s[tag=Eleveur,tag=!MetierDonne] run function tdh:dialogue/topics/metier/eleveur
execute as @s[tag=!MetierDonne] run function tdh:dialogue/topics/metier/generic

# On supprime le tag évitant les checks redondants
tag @s remove MetierDonne
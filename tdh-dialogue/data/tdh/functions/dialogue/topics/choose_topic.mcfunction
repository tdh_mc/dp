# Tous les topics possibles sont proposés ici, hormis certains topics spéciaux

# On autorise le joueur à toucher à son score au cas où ça ne serait pas le cas
scoreboard players enable @p[distance=0,tag=SpeakingToNPC] topic

# On initialise une liste vide dans un storage
data modify storage tdh:dialogue topics set value []

# < 999 : Mots-clés génériques
data modify storage tdh:dialogue topics append value '[{"text":"rumeurs","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger topic set 100"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander ce qui se dit dans le coin..."}]}},{"text":" ● ","color":"gray"},{"text":"ville","clickEvent":{"action":"run_command","value":"/trigger topic set 399"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la ville."}]}},{"text":" ("},{"text":"lieux","clickEvent":{"action":"run_command","value":"/trigger topic set -4"},"hoverEvent":{"action":"show_text","value":[{"text":"Afficher l\'écran de sélection des lieux débloqués."}]}},{"text":") ● "},{"text":"métier","clickEvent":{"action":"run_command","value":"/trigger topic set 300"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander ce qu\'il fait dans la vie."}]}},{"text":" ● ","color":"gray"},{"text":"travail","clickEvent":{"action":"run_command","value":"/trigger topic set 200"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander s\'il a du travail pour vous."}]}},{"text":" ● ","color":"gray"},{"text":"météo","clickEvent":{"action":"run_command","value":"/trigger topic set 10"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la pluie et du beau temps."}]}},{"text":" ● ","color":"gray"},{"text":"manger","clickEvent":{"action":"run_command","value":"/trigger topic set 380"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander de quoi manger."}]}},{"text":" ● ","color":"gray"},{"text":"boire","clickEvent":{"action":"run_command","value":"/trigger topic set 381"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander de quoi boire."}]}},{"text":" ● ","color":"gray"},{"text":"chambre","clickEvent":{"action":"run_command","value":"/trigger topic set 382"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander un lit."}]}},{"text":" ● ","color":"gray"},{"text":"torche","clickEvent":{"action":"run_command","value":"/trigger topic set 383"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander de quoi éclairer."}]}}]'

# Pour chaque topic, on stocke son nom et son event dans la liste si le joueur l'a débloqué

# 1000-1999 : TCH
# 1000 : TCH
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={tch=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"\\n"},{"text":"TCH","color":"red","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des transports en commun du Hameau."}]}}]'
# 1100 : Tickets
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={ticket=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"ticket","clickEvent":{"action":"run_command","value":"/trigger topic set 1100"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des tickets TCH."}]}}]'
# 1110 : Tarif des tickets
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/tarifs={tarifs=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"tarifs","clickEvent":{"action":"run_command","value":"/trigger topic set 1110"},"hoverEvent":{"action":"show_text","value":[{"text":"Critiquer le tarif des tickets."}]}}]'
# 1120 : Vendeur à la sauvette
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/tarifs={sauvette=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"sauvette","clickEvent":{"action":"run_command","value":"/trigger topic set 1120"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des vendeurs à la sauvette."}]}}]'
# 1150 : Fraude
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/tarifs={fraude=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"fraude","clickEvent":{"action":"run_command","value":"/trigger topic set 1150"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de la fraude."}]}}]'
# 1160 : Contrôleur
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/tarifs={controleur=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"contrôleur","clickEvent":{"action":"run_command","value":"/trigger topic set 1160"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des contrôleurs TCH."}]}}]'
# 1200 : Itinéraire
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={itineraire=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"itinéraire","clickEvent":{"action":"run_command","value":"/trigger topic set 1200"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander son chemin."}]}}]'
# 1300 : Lignes
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={lignes=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"lignes","clickEvent":{"action":"run_command","value":"/trigger topic set 1300"},"hoverEvent":{"action":"show_text","value":[{"text":"Se renseigner sur les lignes TCH."}]}}]'
# 1400 : Horaires
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={horaires=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"horaires","clickEvent":{"action":"run_command","value":"/trigger topic set 1400"},"hoverEvent":{"action":"show_text","value":[{"text":"Se renseigner sur les horaires."}]}}]'
# 1500 : Abonnement
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch={abonnement=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"abonnement","clickEvent":{"action":"run_command","value":"/trigger topic set 1500"},"hoverEvent":{"action":"show_text","value":[{"text":"Se renseigner sur l\'abonnement."}]}}]'
# 1510 : Carte GOLD
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/abonnement={carte_gold=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"carte Gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1510"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander à prendre une carte Gold."}]}}]'
# 1520 : Recharger
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/abonnement={recharger=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"recharger","clickEvent":{"action":"run_command","value":"/trigger topic set 1520"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander comment recharger sa carte Gold."}]}}]'
# 1521 : Recharger au guichet
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/abonnement={recharger_au_guichet=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"recharger au guichet","clickEvent":{"action":"run_command","value":"/trigger topic set 1521"},"hoverEvent":{"action":"show_text","value":[{"text":"Recharger sa carte Gold."}]}}]'
# 1530 : Aides (pour payer l'abonnement)
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/tch/abonnement={carte_gold=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"aides","clickEvent":{"action":"run_command","value":"/trigger topic set 1530"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des aides au paiement de l\'abonnement."}]}}]'

# 2000-2999 : Ressources naturelles
# 2000-2199 : Ressources minières
# 2000 : Mine
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={mine=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"\\n"},{"text":"Mine","color":"red","clickEvent":{"action":"run_command","value":"/trigger topic set 2000"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver une mine."}]}}]'
# 2010 : Charbon
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={charbon=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"charbon","clickEvent":{"action":"run_command","value":"/trigger topic set 2010"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver du charbon."}]}}]'
# 2020 : Fer
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={fer=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"fer","clickEvent":{"action":"run_command","value":"/trigger topic set 2020"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver du fer."}]}}]'
# 2030 : Or
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={or=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"or","clickEvent":{"action":"run_command","value":"/trigger topic set 2030"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver de l\'or."}]}}]'
# 2040 : Redstone
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={redstone=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"redstone","clickEvent":{"action":"run_command","value":"/trigger topic set 2040"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver de la redstone."}]}}]'
# 2050 : Lapis
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={lapis_lazuli=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"lapis_lazuli","clickEvent":{"action":"run_command","value":"/trigger topic set 2050"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver du lapis-lazuli."}]}}]'
# 2060 : Diamant
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={diamant=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"diamant","clickEvent":{"action":"run_command","value":"/trigger topic set 2060"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver des diamants."}]}}]'
# 2070 : Emeraude
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={emeraude=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"emeraude","clickEvent":{"action":"run_command","value":"/trigger topic set 2070"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver des émeraudes."}]}}]'
# 2080 : Quartz
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={quartz=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"quartz","clickEvent":{"action":"run_command","value":"/trigger topic set 2080"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver du quartz"}]}}]'

# 2200-2399 : Ressources stupéfiantes
# 2200 : Drogue
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={drogue=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"\\n"},{"text":"Drogue","color":"red","clickEvent":{"action":"run_command","value":"/trigger topic set 2200"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver de la drogue."}]}}]'
# 2210 : Tabac
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={tabac=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"tabac","clickEvent":{"action":"run_command","value":"/trigger topic set 2210"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver du tabac."}]}}]'
# 2220 : Herbe
execute if entity @p[distance=0,tag=SpeakingToNPC,advancements={tdh:dialogue/ressources={herbe=true}}] run data modify storage tdh:dialogue topics append value '[{"text":"herbe","clickEvent":{"action":"run_command","value":"/trigger topic set 2220"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander où trouver de l\'herbe."}]}}]'

# On affiche l’ensemble du tableau généré

tellraw @p[distance=0,tag=SpeakingToNPC] [{"nbt":"topics[]","storage":"tdh:dialogue","interpret":true,"color":"yellow","separator":{"text":" ● ","color":"gray"}}]


# On supprime ensuite les données temporaires
data remove storage tdh:dialogue topics
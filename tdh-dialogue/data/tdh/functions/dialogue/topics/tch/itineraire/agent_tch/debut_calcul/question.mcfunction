# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"J\'aimerais me rendre "},{"storage":"tdh:dialogue","nbt":"extra.destination.alAu"},{"storage":"tdh:dialogue","nbt":"extra.destination.nom"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"C\'est possible d\'aller "},{"storage":"tdh:dialogue","nbt":"extra.destination.alAu"},{"storage":"tdh:dialogue","nbt":"extra.destination.nom"},{"text":", d\'ici ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"Il faudrait que j\'aille "},{"storage":"tdh:dialogue","nbt":"extra.destination.alAu"},{"storage":"tdh:dialogue","nbt":"extra.destination.nom"},{"text":"."}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"On m\'attend "},{"storage":"tdh:dialogue","nbt":"extra.destination.alAu"},{"storage":"tdh:dialogue","nbt":"extra.destination.nom"},{"text":". C\'est possible d\'y aller d\'ici ?"}]'
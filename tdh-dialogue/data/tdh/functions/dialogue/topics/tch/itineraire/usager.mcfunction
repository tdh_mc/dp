# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous pourriez m\'aider ? Je suis complètement perdu, je ne sais pas où je dois aller..."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Je suis un peu perdu... Vous pouvez m\'aider à trouver mon chemin ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"J\'aurais besoin de savoir où je suis et comment en partir... si vous avez une minute."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Je suis perdu, j\'aurais bien besoin d\'un peu d\'aide..."'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Eh bien, ça dépend... Vous devez aller où ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je vais voir ce que je peux faire. C\'est quoi, votre destination ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je ne vous garantis rien, mais dites toujours."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je ne suis pas cartographe, mais je connais le coin. Vous voulez aller où ?"'

# On affiche l'écran de sélection des lieux
tag @s add ChoixLieu
# On enregistre le fait qu'on a demandé un itinéraire (pour que les topics relatifs aux différentes villes activent le menu pour demander un itinéraire au lieu de simplement parler de la ville)
tag @s add Itineraire
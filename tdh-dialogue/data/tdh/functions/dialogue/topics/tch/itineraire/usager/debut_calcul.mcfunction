# L'endroit où on veut se rendre est stocké dans #tchItineraire stationArrivee
# On enregistre le fait qu'on est plus en recherche d'itinéraire (puisqu'on vient de le rechercher)
tag @s remove Itineraire

# On génère notre question
function tdh:dialogue/topics/tch/itineraire/usager/debut_calcul/question

# On lance le calcul
function tch:itineraire/_distance

# On calcule ensuite le résultat (réponse du PNJ)
# Pour l'instant on donne toujours une direction exacte et on connaît toujours l'endroit dont parle le joueur
# TODO rendre ça conditionnel et permettre de donner parfois de mauvaises indications
function tdh:dialogue/topics/tch/itineraire/usager/debut_calcul/reponse/succes
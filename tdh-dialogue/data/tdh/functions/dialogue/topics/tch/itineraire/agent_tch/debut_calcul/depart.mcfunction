# S'il y a une station, on vérifie qu'elle a bien son ID station assigné ; sinon, on le calcule
execute as @e[tag=NomStation,distance=..150] unless score @s idStation matches 1.. run function tch:auto/get_id_station

# Ensuite, on définit la variable station de départ à l'ID de la station la plus proche
execute as @e[tag=NomStation,distance=..150,sort=nearest,limit=1] if score @s idStation matches 1.. run scoreboard players operation #tchItineraire stationDepart = @s idStation
# On enregistre enfin le nom détaillé de la station dans le storage (avec deDu et alAu notamment)
execute as @e[type=item_frame,tag=tchItinNode,tag=!tchQuaiPC,tag=!tchSortiePC] if score @s idStation = #tchItineraire stationDepart run data modify storage tdh:dialogue extra.depart set from entity @s Item.tag.display.station
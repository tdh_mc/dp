# On réinitialise l'ID station de départ pour être sûr qu'on a pas juste gardé l'ancien
scoreboard players reset #tchItineraire stationDepart

# On enregistre les noms des stations de départ et d'arrivée
function tdh:dialogue/topics/tch/itineraire/agent_tch/debut_calcul/depart
function tdh:dialogue/topics/tch/itineraire/agent_tch/debut_calcul/arrivee

# On génère notre question
function tdh:dialogue/topics/tch/itineraire/agent_tch/debut_calcul/question

# On lance le calcul
function tch:itineraire/_calcul

# Selon si le calcul est bien en cours ou a directement échoué, on génère des réponses différentes
# Si le calcul a échoué ou n'a pas encore commencé, on dit qu'on va rechercher
execute if score #tchItineraire status matches 1 run function tdh:dialogue/topics/tch/itineraire/agent_tch/debut_calcul/reponse/en_cours
execute unless score #tchItineraire status matches 1 unless score #tchItineraire dialogueID = @s dialogueID run function tdh:dialogue/topics/tch/itineraire/agent_tch/debut_calcul/reponse/en_cours

# Ce n'est un échec automatique que si #tchItineraire a notre dialogueID : sinon, on est simplement en train de faire un autre calcul
execute unless score #tchItineraire status matches 1 if score #tchItineraire dialogueID = @s dialogueID run function tdh:dialogue/topics/tch/itineraire/agent_tch/debut_calcul/reponse/echec
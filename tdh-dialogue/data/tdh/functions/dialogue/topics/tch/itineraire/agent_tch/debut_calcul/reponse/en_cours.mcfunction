# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Alors, pour aller "},{"storage":"tdh:dialogue","nbt":"extra.depart.deDu"},{"storage":"tdh:dialogue","nbt":"extra.depart.nom"},{"text":" "},{"storage":"tdh:dialogue","nbt":"extra.destination.alAu"},{"storage":"tdh:dialogue","nbt":"extra.destination.nom"},{"text":"... Ne bougez pas, je fais une recherche !"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Donc, vous voulez aller "},{"storage":"tdh:dialogue","nbt":"extra.destination.alAu"},{"storage":"tdh:dialogue","nbt":"extra.destination.nom"},{"text":" ? Je fais une recherche tout de suite !"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Je fais la recherche tout de suite ! Alors, destination "},{"storage":"tdh:dialogue","nbt":"extra.destination.prefixe"},{"storage":"tdh:dialogue","nbt":"extra.destination.nom"},{"text":"... depuis "},{"storage":"tdh:dialogue","nbt":"extra.depart.prefixe"},{"storage":"tdh:dialogue","nbt":"extra.depart.nom"},{"text":"..."}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Un tout petit instant, je rentre ça dans mon ordinateur... Alors, on part "},{"storage":"tdh:dialogue","nbt":"extra.depart.deDu"},{"storage":"tdh:dialogue","nbt":"extra.depart.nom"},{"text":"... et on se rend "},{"storage":"tdh:dialogue","nbt":"extra.destination.alAu"},{"storage":"tdh:dialogue","nbt":"extra.destination.nom"},{"text":"..."}]'

# On enregistre le fait qu'on ne peut pas proposer de sujet,
# et qu'on est en train de faire une recherche (on doit donc actualiser régulièrement et sortir de nouvelles phrases)
tag @s add EnAttente
# On met un timer de ~8s avant de produire une nouvelle phrase
scoreboard players set @s remainingTicks 50
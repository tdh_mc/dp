# On exécute cette fonction en tant qu'un PNJ après avoir obtenu la réponse à notre recherche d'itinéraire
# et on va poser la "question" finale "Vous avez besoin d'autre chose ?"


# On génère une phrase aléatoire du joueur
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Merci beaucoup."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Merci !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Eh bien, merci, j\'imagine."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Merci à vous."'

# On génère une phrase aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Vous fallait-il autre chose ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Aviez-vous besoin d\'autres informations ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Est-ce tout ce que je pouvais faire pour vous ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"C\'est tout ce dont vous aviez besoin ?"'


# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On affiche les options
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"Oui, en fait, j\'avais autre chose à vous demander...","clickEvent":{"action":"run_command","value":"/trigger answer set -2"},"hoverEvent":{"action":"show_text","value":[{"text":"Afficher la liste des sujets disponibles"}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Merci, ça sera tout.","clickEvent":{"action":"run_command","value":"/trigger answer set -1"},"hoverEvent":{"action":"show_text","value":[{"text":"Quitter la conversation"}]}}]'
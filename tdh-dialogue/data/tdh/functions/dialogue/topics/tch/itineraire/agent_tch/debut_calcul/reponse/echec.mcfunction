# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"J\'aimerais beaucoup vous aider, mais il est "},{"storage":"tch:itineraire","nbt":"dernier_resultat.erreur"},{"text":". "},{"storage":"tdh:dialogue","nbt":"extra.conseil","interpret":true}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Ç\'aurait été avec plaisir ; malheureusement, "},{"storage":"tch:itineraire","nbt":"dernier_resultat.erreur"},{"text":"... "},{"storage":"tdh:dialogue","nbt":"extra.conseil","interpret":true}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Je croyais pouvoir vous être utile, mais il est "},{"storage":"tch:itineraire","nbt":"dernier_resultat.erreur"},{"text":". Désolé ! "},{"storage":"tdh:dialogue","nbt":"extra.conseil","interpret":true}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Toutes mes excuses, mais il est "},{"storage":"tch:itineraire","nbt":"dernier_resultat.erreur"},{"text":". "},{"storage":"tdh:dialogue","nbt":"extra.conseil","interpret":true}]'

# S'il fait nuit, on conseille d'attendre demain
execute if score #temps timeOfDay matches 3 unless score #temps currentTdhTick matches 4000..24000 run data modify storage tdh:dialogue extra.conseil set value '"Je vous conseille de revenir demain matin, quand le service aura repris !"'
execute if score #temps timeOfDay matches 3 if score #temps currentTdhTick matches 4000..24000 run data modify storage tdh:dialogue extra.conseil set value '"Je vous conseille de revenir en début de matinée, quand davantage de lignes seront ouvertes !"'
execute unless score #temps timeOfDay matches 3 run data modify storage tdh:dialogue extra.conseil set value '""'

# On enregistre le fait qu'on est plus en recherche d'itinéraire
tag @s remove Itineraire
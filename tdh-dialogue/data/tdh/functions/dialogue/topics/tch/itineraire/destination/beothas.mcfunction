# On enregistre l'ID de la station d'arrivée
scoreboard players set #tchItineraire stationArrivee 1160

# On fait le reste des calculs
execute as @s[tag=AgentTCH] run function tdh:dialogue/topics/tch/itineraire/agent_tch/debut_calcul
execute as @s[tag=!AgentTCH] run function tdh:dialogue/topics/tch/itineraire/usager/debut_calcul
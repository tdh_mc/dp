# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"Il faudrait que j\'aille "},{"storage":"tch:itineraire","nbt":"distance.destination.alAu"},{"storage":"tch:itineraire","nbt":"distance.destination.nom"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"Vous savez aller "},{"storage":"tch:itineraire","nbt":"distance.destination.alAu"},{"storage":"tch:itineraire","nbt":"distance.destination.nom"},{"text":", d\'ici ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"Je suis sur le bon chemin, pour aller "},{"storage":"tch:itineraire","nbt":"distance.destination.alAu"},{"storage":"tch:itineraire","nbt":"distance.destination.nom"},{"text":" ?"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"C\'est par où, la route "},{"storage":"tch:itineraire","nbt":"distance.destination.deDu"},{"storage":"tch:itineraire","nbt":"distance.destination.nom"},{"text":" ?"}]'
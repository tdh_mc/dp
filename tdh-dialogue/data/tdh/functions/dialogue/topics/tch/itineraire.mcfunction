# L'agent TCH fera une recherche d'itinéraire TCH complète
# Les autres PNJ pourront éventuellement donner une indication
execute as @s[tag=AgentTCH] run function tdh:dialogue/topics/tch/itineraire/agent_tch
execute as @s[tag=!AgentTCH] run function tdh:dialogue/topics/tch/itineraire/usager
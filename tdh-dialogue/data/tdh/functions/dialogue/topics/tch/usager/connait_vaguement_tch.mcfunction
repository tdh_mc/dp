# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Ils s\'occupent du métro, je crois ? Je n\'en sais pas beaucoup plus. C\'est un peu hors de mes moyens..."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"J\'en ai déjà entendu parler, mais je n\'ai jamais utilisé leur réseau. Vous trouverez sans doute quelqu\'un qui vous en parlera mieux que moi..."'
# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Vous n\'imaginez pas à quel point c\'était horrible de se déplacer avant l\'arrivée du métro ! Je suis très heureux d\'utiliser TCH."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je les adore ! J\'ai été leur premier client à l\'ouverture d\'au moins 3 lignes."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Sans eux, ma vie serait si morne ! Comment ferais-je pour partir en vacances à l\'autre bout du monde sans leur métro, je me le demande bien..."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Quand je me lève le matin, après avoir mangé mon pain et avant de prendre mon bain, je ferme les yeux et je me dis à moi-même « Qu\'est-ce que tu vois ? ». Alors je réponds « Je ne vois rien », avant de rétorquer « Ça serait ta vie sans TCH, gros. »"'
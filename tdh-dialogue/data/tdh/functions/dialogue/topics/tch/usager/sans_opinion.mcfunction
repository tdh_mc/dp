# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Leur service, ce n\'est pas la panacée, mais je connais tellement pire que je ne leur en veux pas trop."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas trop quoi vous dire. Ils n\'ont pas de place spéciale dans mon cœur, mais leurs employés sont gentils et quand j\'ai besoin d\'eux, j\'arrive généralement à destination."'
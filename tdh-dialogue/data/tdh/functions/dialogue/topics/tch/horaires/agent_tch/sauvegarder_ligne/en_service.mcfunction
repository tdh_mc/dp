# On exécute cette fonction en tant qu'une item frame tchPC,
# si on est en service

# On se donne le tag qui permettra l'affichage du temps d'attente
tag @s add EntiteParlante

# On enregistre les 2 directions sauf si c'est un terminus
tag @e[type=armor_stand,tag=Direction,tag=!Terminus,distance=..3,limit=1] add DirTemp
execute as @e[type=armor_stand,tag=Direction,tag=!Terminus,tag=DirTemp,distance=..3,limit=1] at @s at @e[type=item_frame,tag=SpawnMetro,distance=..50] if score @s idLine = @e[type=item_frame,tag=SpawnMetro,distance=0,limit=1] idLine run tag @e[type=item_frame,tag=SpawnMetro,distance=0,limit=1] add Direction1
execute as @e[type=armor_stand,tag=Direction,tag=!Terminus,tag=!DirTemp,distance=..3,limit=1] at @s at @e[type=item_frame,tag=SpawnMetro,distance=..50] if score @s idLine = @e[type=item_frame,tag=SpawnMetro,distance=0,limit=1] idLine run tag @e[type=item_frame,tag=SpawnMetro,distance=0,limit=1] add Direction2

execute at @e[type=item_frame,tag=Direction1,limit=1] run function tdh:dialogue/topics/tch/horaires/agent_tch/sauvegarder_ligne/direction1
execute at @e[type=item_frame,tag=Direction2,limit=1] run function tdh:dialogue/topics/tch/horaires/agent_tch/sauvegarder_ligne/direction2

# On définit le statut de l'item frame
scoreboard players set @s temp 4
# S'il n'y a qu'une seule direction (=terminus), on se donne le statut 3
execute unless entity @e[type=armor_stand,tag=Direction,tag=!Terminus,tag=!DirTemp,distance=..3] run scoreboard players set @s temp 3

# On enregistre le nom de chaque direction
data modify entity @s Item.tag.dialogue.extra.direction1 set from entity @e[type=armor_stand,tag=Direction,tag=!Terminus,tag=DirTemp,distance=..3,limit=1] CustomName
data modify entity @s Item.tag.dialogue.extra.direction2 set from entity @e[type=armor_stand,tag=Direction,tag=!Terminus,tag=!DirTemp,distance=..3,limit=1] CustomName

# On supprime le tag temporaire
tag @e[type=armor_stand,tag=Direction,tag=DirTemp,distance=..3,limit=1] remove DirTemp
tag @e[type=item_frame,tag=Direction1] remove Direction1
tag @e[type=item_frame,tag=Direction2] remove Direction2
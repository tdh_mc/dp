# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Où diable avez-vous vu la moindre ligne par ici ? Vous êtes ivre ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Vous avez vu une gare, vous, dans le coin ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je ne comprends pas la question. TCH dessert cet endroit, maintenant ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je pense que vous rêvez. Il n\'y a pas de lignes par ici."'

# Fin de conversation
tag @s add AuRevoir
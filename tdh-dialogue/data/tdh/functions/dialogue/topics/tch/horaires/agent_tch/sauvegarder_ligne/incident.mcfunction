# On exécute cette fonction en tant qu'une item frame tchPC,
# si on est en incident

# On se donne le tag qui permettra l'affichage du temps d'attente
tag @s add EntiteParlante

# On définit le statut de l'item frame
scoreboard players set @s temp 0
# Si on connaît déjà l'heure de reprise, on passe en statut 1
execute if score @s status matches 2.. run scoreboard players set @s temp 1

# Si on a une heure de reprise, on calcule l'heure et la minute (HH:MM = temp2:temp3)
scoreboard players operation #store currentHour = @s finIncident
function tdh:store/time
scoreboard players operation @s temp2 = #store currentHour
scoreboard players operation @s temp3 = #store currentMinute

# S'il y a moins de 10mn, on passe en statut 2 (version du message avec 0 devant les minutes)
execute if score @s temp3 matches ..9 run scoreboard players set @s temp 2
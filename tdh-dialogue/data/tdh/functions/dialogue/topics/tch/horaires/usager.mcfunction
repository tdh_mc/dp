# Si on n'a pas encore de score d'appréciation de TCH, on en génère un
execute unless score @s likesTCH matches 0..100 run function tdh:dialogue/calcul/type_usager

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Vous pourriez me donner les horaires des lignes ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Quels sont les horaires des lignes qui passent ici ?"'

# Si on connait un minimum TCH et qu'une ligne passe ici, on va répondre d'aller bien se faire voir
execute as @s[scores={knowsTCH=30..}] if entity @e[type=armor_stand,tag=NumLigne,distance=..150] run function tdh:dialogue/topics/tch/horaires/usager/no
# Si on connait un minimum TCH et qu'aucune ligne ne passe ici, on donne la même réponse en précisant qu'il n'y a pas de ligne ici
execute as @s[scores={knowsTCH=30..}] unless entity @e[type=armor_stand,tag=NumLigne,distance=..150] run function tdh:dialogue/topics/tch/horaires/usager/no_line
# Si on ne connaît pas bien TCH, on donne un non générique
execute as @s[scores={knowsTCH=..29}] run function tdh:dialogue/topics/no
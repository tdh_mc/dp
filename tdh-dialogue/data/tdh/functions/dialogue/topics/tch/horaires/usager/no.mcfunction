# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Il y a des panneaux d\'information et des annonces sonores, ici. Vous êtes sourd "},{"text":"et","bold":"true"},{"text":" aveugle ?"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Vous savez que TCH paie des gens exprès pour répondre à ce genre de questions ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Vous ne faites franchement aucun effort. Allez donc importuner un agent, au moins il est payé pour ça !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je pense qu\'un agent saura vous en dire plus que moi."'

# Fin de conversation
tag @s add AuRevoir
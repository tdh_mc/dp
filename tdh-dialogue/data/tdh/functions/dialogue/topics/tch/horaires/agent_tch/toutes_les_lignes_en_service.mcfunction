# On génère une réponse du PNJ en fonction du nombre de lignes

# On affiche un message générique qui dit que certaines lignes sont en service
# 1 ligne en service
execute if score @s temp3 matches 0 if score @s temp4 matches 1 run data modify storage tdh:dialogue reponse set value '[{"text":"Le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" est encore en service."}]'
execute if score @s temp3 matches 1 if score @s temp4 matches 0 run data modify storage tdh:dialogue reponse set value '[{"text":"Le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" est encore en service, mais il subit actuellement des perturbations."}]'
# 2+ lignes en service
execute if score @s temp3 matches 1.. if score @s temp4 matches 1.. run data modify storage tdh:dialogue reponse set value '[{"text":"Les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" sont encore en service."}]'
execute if score @s temp3 matches 2.. if score @s temp4 matches 0 run data modify storage tdh:dialogue reponse set value '[{"text":"Les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" sont encore en service, mais subissent actuellement des perturbations."}]'
execute if score @s temp3 matches 0 if score @s temp4 matches 2.. run data modify storage tdh:dialogue reponse set value '[{"text":"Les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" sont encore en service."}]'

# On enregistre le fait qu'on doit afficher des informations en tant que des item frames (les lignes ouvertes)
tag @s add EntiteParle
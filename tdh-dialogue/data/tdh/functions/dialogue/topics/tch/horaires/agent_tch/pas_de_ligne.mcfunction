# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Aucune ligne ne passe dans cette station pour l\'instant. Toutes mes excuses !"}]',cliquable:'[{"text":"Aucune "},{"text":"ligne","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1300"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des lignes"}}},{"text":" ne passe dans cette station pour l\'instant. Toutes mes excuses !"}]'}
execute if score #random temp matches 0..49 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch lignes

execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"Le mauvais point, c\'est qu\'à ce jour, aucune ligne ne dessert cette station. Le bon, c\'est que TCH me paie expressément pour être ici, ce qui me permet de vous en informer !"',cliquable:'"Le mauvais point, c\'est qu\'à ce jour, aucune "},{"text":"ligne","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1300"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des lignes"}}},{"text":" ne dessert cette station. Le bon, c\'est que "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de TCH"}}},{"text":" me paie expressément pour être ici, ce qui me permet de vous en informer !"'}
execute if score #random temp matches 50..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch lignes
execute if score #random temp matches 50..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch tch
# On exécute cette fonction en tant qu'une ligne qui passe ici, pour s'ajouter dans le storage

# On récupère notre PC
function tch:tag/pc

# Selon l'état de notre PC, on définit une variable temporaire à l'état de la ligne
# 1 = Hors service, 2 = Fin de service, 3 = Incident détecté et en cours, 4 = En service et roule normalement
execute if entity @e[type=item_frame,tag=ourPC,tag=HorsService] run scoreboard players set #dialogue temp 1
execute if entity @e[type=item_frame,tag=ourPC,tag=EnService,tag=FinDeService] run scoreboard players set #dialogue temp 2
execute if entity @e[type=item_frame,tag=ourPC,tag=EnService,tag=!FinDeService] run scoreboard players set #dialogue temp 4
execute if entity @e[type=item_frame,tag=ourPC,tag=EnService,tag=IncidentEnCours,scores={status=1..3}] run scoreboard players set #dialogue temp 3

# On enregistre la ligne dans le bon storage
execute if score #dialogue temp matches 1 run data modify storage tdh:dialogue extra.lignes.hors_service append from entity @s CustomName
execute if score #dialogue temp matches 2 run data modify storage tdh:dialogue extra.lignes.fin_service append from entity @s CustomName
# On ajoute toutes les lignes en incident dans la liste des lignes en service (car elles sont bel et bien en service)
execute if score #dialogue temp matches 3 run data modify storage tdh:dialogue extra.lignes.en_service append from entity @s CustomName
execute if score #dialogue temp matches 4 run data modify storage tdh:dialogue extra.lignes.en_service append from entity @s CustomName

# On incrémente la variable temporaire correspondante
execute if score #dialogue temp matches 1 run scoreboard players add #ligne temp 1
execute if score #dialogue temp matches 2 run scoreboard players add #ligne temp2 1
execute if score #dialogue temp matches 3 run scoreboard players add #ligne temp3 1
execute if score #dialogue temp matches 4 run scoreboard players add #ligne temp4 1

# Si on est en service, on récupère les infos détaillées sur nos items frames
execute if score #dialogue temp matches 3 as @e[type=item_frame,tag=ourPC,limit=1] run function tdh:dialogue/topics/tch/horaires/agent_tch/sauvegarder_ligne/incident
execute if score #dialogue temp matches 4 as @e[type=item_frame,tag=ourPC,limit=1] run function tdh:dialogue/topics/tch/horaires/agent_tch/sauvegarder_ligne/en_service

# On supprime le tag du PC
scoreboard players reset #dialogue temp
tag @e[type=item_frame,tag=ourPC] remove ourPC
# On génère une réponse du PNJ en fonction du nombre de lignes

# On affiche un message générique qui dit que certaines lignes sont en service
# 1 ligne en service
execute if score @s temp3 matches 0 if score @s temp4 matches 1 run data modify storage tdh:dialogue reponse set value '[{"text":"Le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" est encore en service.\\n"},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_text","interpret":true}]'
execute if score @s temp3 matches 1 if score @s temp4 matches 0 run data modify storage tdh:dialogue reponse set value '[{"text":"Le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" est encore en service, mais il subit actuellement des perturbations.\\n"},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_text","interpret":true}]'
# 2+ lignes en service
execute if score @s temp3 matches 1.. if score @s temp4 matches 1.. run data modify storage tdh:dialogue reponse set value '[{"text":"Les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" sont encore en service.\\n"},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_text","interpret":true}]'
execute if score @s temp3 matches 2.. if score @s temp4 matches 0 run data modify storage tdh:dialogue reponse set value '[{"text":"Les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" sont encore en service, mais subissent actuellement des perturbations.\\n"},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_text","interpret":true}]'
execute if score @s temp3 matches 0 if score @s temp4 matches 2.. run data modify storage tdh:dialogue reponse set value '[{"text":"Les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.en_service_format","interpret":true},{"text":" sont encore en service.\\n"},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_text","interpret":true}]'

# EXTRA : Les lignes n'étant plus en service
# 1 ligne FDS et 0 fermée
execute if score @s temp matches 0 if score @s temp2 matches 1 run data modify storage tdh:dialogue extra.lignes.hors_service_text set value '[{"text":"En revanche, le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fin_service_format","interpret":true},{"text":" a terminé son service."}]'
# 2+ lignes FDS et 0 fermée
execute if score @s temp matches 0 if score @s temp2 matches 2.. run data modify storage tdh:dialogue extra.lignes.hors_service_text set value '[{"text":"En revanche, les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fin_service_format","interpret":true},{"text":" ont terminé leur service. Toutes mes excuses !"}]'
# 0 ligne FDS et 1 fermée
execute if score @s temp matches 1 if score @s temp2 matches 0 run data modify storage tdh:dialogue extra.lignes.hors_service_text set value '[{"text":"En revanche, le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_format","interpret":true},{"text":" n\'a pas encore ouvert. Toutes mes excuses !"}]'
# 1 ligne FDS et 1 fermée
execute if score @s temp matches 1 if score @s temp2 matches 1 run data modify storage tdh:dialogue extra.lignes.hors_service_text set value '[{"text":"En revanche, le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fin_service_format","interpret":true},{"text":" a terminé son service. Quant au "},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_format","interpret":true},{"text":", il n\'a pas encore ouvert. Toutes mes excuses !"}]'
# 2+ lignes FDS et 1 fermée
execute if score @s temp matches 1 if score @s temp2 matches 2.. run data modify storage tdh:dialogue extra.lignes.hors_service_text set value '[{"text":"En revanche, les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fin_service_format","interpret":true},{"text":" ont terminé leur service. Quant au "},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_format","interpret":true},{"text":", il n\'a pas encore ouvert. Toutes mes excuses !"}]'
# 0 ligne FDS et 2+ fermées
execute if score @s temp matches 2.. if score @s temp2 matches 0 run data modify storage tdh:dialogue extra.lignes.hors_service_text set value '[{"text":"En revanche, le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fin_service_format","interpret":true},{"text":" a terminé son service. Toutes mes excuses !"}]'
# 1 ligne FDS et 2+ fermées
execute if score @s temp matches 2.. if score @s temp2 matches 1 run data modify storage tdh:dialogue extra.lignes.hors_service_text set value '[{"text":"En revanche, le "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fin_service_format","interpret":true},{"text":" a terminé son service. Quant aux "},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_format","interpret":true},{"text":", ils n\'ont pas encore ouvert. Toutes mes excuses !"}]'
# 2+ lignes FDS et 2+ fermées
execute if score @s temp matches 2.. if score @s temp2 matches 2.. run data modify storage tdh:dialogue extra.lignes.hors_service_text set value '[{"text":"En revanche, les "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fin_service_format","interpret":true},{"text":" ont terminé leur service. Quant aux "},{"storage":"tdh:dialogue","nbt":"extra.lignes.hors_service_format","interpret":true},{"text":", ils n\'ont pas encore ouvert. Toutes mes excuses !"}]'

# On enregistre le fait qu'on doit afficher des informations en tant que des item frames (les lignes ouvertes)
tag @s add EntiteParle
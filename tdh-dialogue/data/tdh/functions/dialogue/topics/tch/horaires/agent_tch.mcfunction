# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous pourriez me donner les horaires des lignes ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Quels sont les horaires des lignes qui passent ici ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Vous savez si des trains passent bientôt ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"C\'est quand, les prochains passages ?"'


# On calcule la réponse de l'agent TCH.
# On initialise 4 variables temporaires pour le nombre de lignes : fermées, en fin de service, en incident, en service.
scoreboard players set #ligne temp 0
scoreboard players set #ligne temp2 0
scoreboard players set #ligne temp3 0
scoreboard players set #ligne temp4 0
# On ajoute chaque ligne qui passe ici dans le storage tout en comptant le nombre de lignes
execute at @e[tag=NomStation,distance=..150,sort=nearest,limit=1] as @e[type=armor_stand,tag=NumLigne,distance=..150] at @s run function tdh:dialogue/topics/tch/horaires/agent_tch/sauvegarder_ligne

# On formate les lignes pour l'affichage
function tdh:dialogue/topics/tch/horaires/agent_tch/formater_lignes

# On récupère les variables temporaires
scoreboard players operation @s temp = #ligne temp
scoreboard players operation @s temp2 = #ligne temp2
scoreboard players operation @s temp3 = #ligne temp3
scoreboard players operation @s temp4 = #ligne temp4
scoreboard players reset #ligne temp
scoreboard players reset #ligne temp2
scoreboard players reset #ligne temp3
scoreboard players reset #ligne temp4

# On compte le nombre total de lignes
scoreboard players operation @s temp9 = @s temp
scoreboard players operation @s temp9 += @s temp2
scoreboard players operation @s temp9 += @s temp3
scoreboard players operation @s temp9 += @s temp4

# S'il n'y a aucune ligne qui passe ici
execute if score @s temp9 matches 0 run function tdh:dialogue/topics/tch/horaires/agent_tch/pas_de_ligne
# S'il y a des lignes mais qu'aucune n'a encore ouvert (= il y a autant de lignes que de lignes fermées)
execute if score @s temp9 matches 1.. if score @s temp = @s temp9 run function tdh:dialogue/topics/tch/horaires/agent_tch/pas_de_ligne_ouverte
# S'il y a des lignes mais qu'elles sont toutes en fin de service (= il n'y a ni ligne en incident ni ligne en service)
execute if score @s temp9 matches 1.. if score @s temp3 matches 0 if score @s temp4 matches 0 run function tdh:dialogue/topics/tch/horaires/agent_tch/pas_de_ligne_en_service
# S'il y a des lignes dont certaines en service (= il y a au moins une ligne en service mais moins que de lignes en tout)
execute if score @s temp9 matches 1.. if score @s temp4 matches 1.. if score @s temp4 < @s temp9 run function tdh:dialogue/topics/tch/horaires/agent_tch/certaines_lignes_en_service
execute if score @s temp9 matches 1.. if score @s temp3 matches 1.. if score @s temp3 < @s temp9 run function tdh:dialogue/topics/tch/horaires/agent_tch/certaines_lignes_en_service
# Si toutes les lignes sont en service (= il n'y a ni ligne fermée ni ligne en fin de service)
execute if score @s temp9 matches 1.. if score @s temp matches 0 if score @s temp2 matches 0 run function tdh:dialogue/topics/tch/horaires/agent_tch/toutes_les_lignes_en_service


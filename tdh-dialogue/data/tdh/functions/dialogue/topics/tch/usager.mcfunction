# Si on n'a pas encore de score d'appréciation de TCH, on en génère un
execute unless score @s likesTCH matches 0..100 run function tdh:dialogue/calcul/type_usager

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Qu\'est-ce que vous pouvez me dire sur TCH ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous pensez quoi de TCH ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Vous avez quelque chose à me dire sur TCH ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Vous avez entendu parler de TCH ?"'

# On sélectionne une sous fonction pour la réponse selon notre appréciation de TCH
execute as @s[scores={knowsTCH=30..,likesTCH=85..}] run function tdh:dialogue/topics/tch/usager/adore_tch
execute as @s[scores={knowsTCH=30..,likesTCH=55..84}] run function tdh:dialogue/topics/tch/usager/aime_tch
execute as @s[scores={knowsTCH=30..,likesTCH=35..54}] run function tdh:dialogue/topics/tch/usager/sans_opinion
execute as @s[scores={knowsTCH=30..,likesTCH=..34}] run function tdh:dialogue/topics/tch/usager/deteste_tch
execute as @s[scores={knowsTCH=15..29}] run function tdh:dialogue/topics/tch/usager/connait_vaguement_tch
execute as @s[scores={knowsTCH=..14}] run function tdh:dialogue/topics/no
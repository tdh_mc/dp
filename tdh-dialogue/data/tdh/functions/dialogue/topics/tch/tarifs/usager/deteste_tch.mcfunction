# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'"C\'est ce que disent tous ceux qui achètent encore leurs tickets au guichet comme des débutants ! Il y a des vendeurs à la sauvette qui les vendent moins cher, et s\'il n\'y en a pas... il suffit de se baisser et de ramasser un ticket usagé !"',cliquable:'[{"text":"C\'est ce que disent tous ceux qui achètent encore leurs tickets au guichet comme des débutants ! Il y a des vendeurs à la "},{"text":"sauvette","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1120"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des vendeurs à la sauvette"}}},{"text":" qui les vendent moins cher, et s\'il n\'y en a pas... il suffit de se baisser et de ramasser un ticket usagé !"}]'}
execute if score #random temp matches 0..24 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/tarifs sauvette

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value {basique:'"Ça, c\'est parce que vous n\'avez jamais appris à frauder ! C\'est dans les vieux pots qu\'on fait les meilleures soupes..."',cliquable:'[{"text":"Ça, c\'est parce que vous n\'avez jamais appris à "},{"text":"frauder","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1150"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de la fraude"}}},{"text":" ! C\'est dans les vieux pots qu\'on fait les meilleures soupes..."}]'}
execute if score #random temp matches 25..49 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/tarifs fraude

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Ce n\'est pas possible de sortir encore une phrase pareille en "},{"score":{"name":"#temps","objective":"dateAnnee"}},{"text":" ! Entre la fraude et les vendeurs à la sauvette, ce n\'est vraiment pas difficile d\'économiser..."}]',cliquable:'[{"text":"Ce n\'est pas possible de sortir encore une phrase pareille en "},{"score":{"name":"#temps","objective":"dateAnnee"}},{"text":" ! Entre la "},{"text":"fraude","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1150"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de la fraude"}}},{"text":" et les vendeurs à la "},{"text":"sauvette","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1120"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des vendeurs à la sauvette"}}},{"text":", ce n\'est vraiment pas difficile d\'économiser..."}]'}
execute if score #random temp matches 50..74 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/tarifs fraude
execute if score #random temp matches 50..74 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/tarifs sauvette

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value {basique:'"Je m\'en fiche un peu ; je fraude. Je me fais prendre de temps à autre, mais ça reste plus rentable que leur fichu abonnement !"',cliquable:'[{"text":"Je m\'en fiche un peu ; je "},{"text":"fraude","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1150"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de la fraude"}}},{"text":". Je me fais prendre de temps à autre, mais ça reste plus rentable que leur fichu "},{"text":"abonnement","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1500"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de l\'abonnement"}}},{"text":" !"}]'}
execute if score #random temp matches 75..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch abonnement
execute if score #random temp matches 75..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/tarifs fraude


# Si un agent TCH est à proximité, il entend la conversation
execute at @s as @e[type=villager,tag=AgentTCH,tag=!SpeakingToPlayer,distance=..10,sort=nearest,limit=1] run tag @s add TiersParlant
execute if entity @e[type=villager,tag=TiersParlant,distance=..20] run tag @s add TiersParle
execute as @s[tag=TiersParle] run data modify storage tdh:dialogue phrase_tiers set value '"Vous vous rendez compte que je vous entends ?"'
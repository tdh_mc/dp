# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'"Ben... c\'est vrai que c\'est un peu cher, mais je me suis fait contrôler à chaque fois que j\'ai essayé de voyager sans ticket, alors..."',cliquable:'[{"text":"Ben... c\'est vrai que c\'est un peu cher, mais je me suis fait "},{"text":"contrôler","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1160"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des contrôleurs"}}},{"text":" à chaque fois que j\'ai essayé de "},{"text":"voyager sans ticket","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1150"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de la fraude"}}},{"text":", alors..."}]'}
execute if score #random temp matches 0..24 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/tarifs fraude
execute if score #random temp matches 0..24 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/tarifs controleur

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Vous n\'avez pas tort, mais c\'est leur réseau. Même si le prix du ticket était de 5 diamants, ça ne serait pas mes oignons."'

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value {basique:'"Oui, j\'ai toujours un peu de mal à payer quand mon abonnement expire... Mais bon, on a déjà de la chance d\'avoir un réseau aussi développé, il faut bien que ça leur rapporte quelque chose."',cliquable:'[{"text":"Oui, j\'ai toujours un peu de mal à payer quand mon "},{"text":"abonnement","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1500"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de l\'abonnement"}}},{"text":" expire... Mais bon, on a déjà de la chance d\'avoir un réseau aussi développé, il faut bien que ça leur rapporte quelque chose."}]'}
execute if score #random temp matches 50..74 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch abonnement

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value {basique:'"C\'est vrai que c\'est cher. Mais bon, qu\'est-ce que vous voulez y faire ? Ce sont leurs trains et leurs lignes, après tout."',cliquable:'[{"text":"C\'est vrai que c\'est cher. Mais bon, qu\'est-ce que vous voulez y faire ? Ce sont leurs trains et leurs "},{"text":"lignes","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1300"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des lignes"}}},{"text":", après tout."}]'}
execute if score #random temp matches 75..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch lignes
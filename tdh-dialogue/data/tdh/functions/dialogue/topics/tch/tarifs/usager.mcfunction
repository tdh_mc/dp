# Si on n'a pas encore de score d'appréciation de TCH, on en génère un
execute unless score @s likesTCH matches 0..100 run function tdh:dialogue/calcul/type_usager

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous ne trouvez pas que les tarifs de TCH sont quand même un peu chers ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Loin de moi l\'idée de critiquer, mais... Vous ne trouvez pas que les tickets TCH sont carrément hors de prix ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Ce n\'est pas payé par les impôts, TCH ? Pourquoi il faut en plus qu\'on achète les tickets aussi cher ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Entre nous... voyager sur le réseau TCH, ce n\'est pas donné, vous ne trouvez pas ?"'

# Selon l'appréciation et la connaissance de TCH, on génère une réponse
execute as @s[scores={knowsTCH=40..,likesTCH=80..}] run function tdh:dialogue/topics/tch/tarifs/usager/adore_tch
execute as @s[scores={knowsTCH=40..,likesTCH=55..79}] run function tdh:dialogue/topics/tch/tarifs/usager/aime_tch
execute as @s[scores={knowsTCH=40..,likesTCH=35..54}] run function tdh:dialogue/topics/tch/tarifs/usager/sans_opinion
execute as @s[scores={knowsTCH=40..,likesTCH=..34}] run function tdh:dialogue/topics/tch/tarifs/usager/deteste_tch
execute as @s[scores={knowsTCH=20..39}] run function tdh:dialogue/topics/tch/tarifs/usager/connait_vaguement_tch
execute as @s[scores={knowsTCH=..19}] run function tdh:dialogue/topics/no
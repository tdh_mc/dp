# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'"Il y en a qui achètent leur ticket à des vendeurs à la sauvette, mais il paraît que ça n\'est pas très fiable. Si vous voyagez beaucoup, prenez plutôt un abonnement !"',cliquable:'[{"text":"Il y en a qui achètent leur ticket à des vendeurs à la "},{"text":"sauvette","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1120"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des vendeurs à la sauvette"}}},{"text":", mais il paraît que ça n\'est pas très fiable. Si vous voyagez beaucoup, prenez plutôt un "},{"text":"abonnement","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1500"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de l\'abonnement"}}},{"text":" !"}]'}
execute if score #random temp matches 0..24 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch abonnement
execute if score #random temp matches 0..24 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/tarifs sauvette

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"C\'est un budget, c\'est sûr, mais je trouve que ça en vaut la peine."'

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Pas tant que ça. Je pense qu\'en tant qu\'usagers, on n\'imagine pas trop combien coûte l\'entretien de ce genre de réseau."'

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value {basique:'"Moi, je bénéficie d\'aides de mon patron pour payer l\'abonnement TCH ! Si vous travaillez, demandez à votre ville, ils proposent peut-être quelque chose de semblable."',cliquable:'[{"text":"Moi, je bénéficie d\'"},{"text":"aides","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1530"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des aides à l\'abonnement"}}},{"text":" de mon patron pour payer l\'"},{"text":"abonnement","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1500"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de l\'abonnement"}}},{"text":" ! Si vous travaillez, demandez à votre ville, ils proposent peut-être quelque chose de semblable."}]'}
execute if score #random temp matches 75..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch abonnement
execute if score #random temp matches 75..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/abonnement aides
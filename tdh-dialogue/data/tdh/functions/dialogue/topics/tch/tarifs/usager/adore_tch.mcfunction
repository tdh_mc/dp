# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'"Franchement, je ne trouve pas. Si vous voyagez beaucoup, il vaut mieux que vous preniez un abonnement !"',cliquable:'[{"text":"Franchement, je ne trouve pas. Si vous voyagez beaucoup, il vaut mieux que vous preniez un "},{"text":"abonnement","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1500"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de l\'abonnement"}}},{"text":" !"}]'}
execute if score #random temp matches 0..24 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch abonnement

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value {basique:'"Pas du tout, je dirais même que c\'est vraiment donné vu le nombre de destinations desservies ! Vous imaginez combien il était difficile de voyager avant que TCH n\'existe ?"',cliquable:'[{"text":"Pas du tout, je dirais même que c\'est vraiment donné vu le nombre de destinations desservies ! Vous imaginez combien il était difficile de voyager avant que "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de TCH"}}},{"text":" n\'existe ?"}]'}
execute if score #random temp matches 25..49 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch tch

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Ben, pas vraiment. Je trouve même qu\'ils pourraient se permettre de faire payer un peu plus, vu la taille de leur réseau."'

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value {basique:'"Moi, je bénéficie d\'aides de mon patron pour payer l\'abonnement TCH ! Si vous travaillez, demandez à votre ville, ils proposent peut-être quelque chose de semblable."',cliquable:'[{"text":"Moi, je bénéficie d\'"},{"text":"aides","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1530"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des aides à l\'abonnement"}}},{"text":" de mon patron pour payer l\'"},{"text":"abonnement","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1500"},"hoverEvent":{"action":"show_text","value":{"text":"Parler de l\'abonnement"}}},{"text":" ! Si vous travaillez, demandez à votre ville, ils proposent peut-être quelque chose de semblable."}]'}
execute if score #random temp matches 75..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch abonnement
execute if score #random temp matches 75..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/abonnement aides
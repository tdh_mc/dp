# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Je n\'ai pas trouvé, non. Mais bon, mon dernier trajet, c\'était il y a quelques années, ça a peut-être changé depuis."'

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Bof... je ne voyage pas souvent, donc ça ne m\'importe pas tant que ça. Et le jour où je n\'ai pas d\'argent, je fais un peu de marche à pied."'

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas trop. J\'utilise peu les transports, donc ça ne me concerne pas vraiment..."'

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value {basique:'"Je n\'utilise pas trop les transports, mais je crois qu\'il existe des aides pour les payer dans certaines villes ? Il faudrait que vous demandiez à quelqu\'un de plus calé que moi."',cliquable:'[{"text":"Je n\'utilise pas trop les transports, mais je crois qu\'il existe des "},{"text":"aides","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1530"},"hoverEvent":{"action":"show_text","value":{"text":"Parler des aides à l\'abonnement"}}},{"text":" pour les payer dans certaines villes ? Il faudrait que vous demandiez à quelqu\'un de plus calé que moi."}]'}
execute if score #random temp matches 75..99 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch/abonnement aides
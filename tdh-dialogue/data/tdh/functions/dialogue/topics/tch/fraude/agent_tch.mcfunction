# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Vous voyez beaucoup de fraudeurs, ces temps-ci ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Il y a de la fraude, sur le réseau TCH ?"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Tant qu\'il y en a, c\'est déjà trop ! Soyez sympa, si vous voyez quelqu\'un en train de frauder, dénoncez-le immédiatement au membre du personnel TCH le plus proche."',cliquable:'[{"text":"Tant qu\'il y en a, c\'est déjà trop ! Soyez sympa, si vous voyez quelqu\'un en train de frauder, dénoncez-le immédiatement au membre du personnel "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de TCH."}]}},{"text":" le plus proche."}]'}
execute if score #random temp matches 0..49 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch tch

execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"Si j\'en vois un seul frauder dans ma station, je demande des renforts de contrôleurs. Il n\'y a que ça qu\'ils comprennent !"',cliquable:'[{"text":"Si j\'en vois un seul frauder dans ma station, je demande des renforts de "},{"text":"contrôleurs","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1160"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des contrôleurs TCH."}]}},{"text":". Il n\'y a que ça qu\'ils comprennent !"}]'}
execute if score #random temp matches 50..99 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch/tarifs controleur
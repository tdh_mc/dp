# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'"Eh bien... Ils ne sont pas très agréables pour les usagers comme moi, mais ils ne représentent pas le problème immense que TCH dépeint. Au fond, ils ne font de mal à personne..."',cliquable:'[{"text":"Eh bien... Ils ne sont pas très agréables pour les usagers comme moi, mais ils ne représentent pas le problème immense que "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de TCH."}]}},{"text":" dépeint. Au fond, ils ne font de mal à personne..."}]'}
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je crois qu\'il n\'y en a pas trop. Oh, j\'en vois bien un de temps à autre qui saute les portiques, mais ça reste plutôt marginal."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Oh, je ne vais pas dire que je ne vois aucun problème, mais il y a tout de même plus grave... Quand mon train en retard de 4 minutes, par exemple."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas trop quoi vous en dire. Il y en a, c\'est sûr, mais ils sont discrets."'

# Fin de conversation
tag @s add AuRevoir
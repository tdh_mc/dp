# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Ils me font peur. J\'espère que vous n\'en êtes pas un."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Moi, je n\'en ai jamais vu, mais d\'après les innombrables pubs TCH intrusives que j\'ai vues à ce sujet, il y en a des millions..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Il faut être dingue pour faire ce genre de choses ! Ces gens-là risquent la mort, voire pire ; une amende !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"J\'essaie de ne pas y faire trop attention. Je fais confiance à TCH pour les attraper et les jeter dans des oubliettes."'

# Fin de conversation
tag @s add AuRevoir
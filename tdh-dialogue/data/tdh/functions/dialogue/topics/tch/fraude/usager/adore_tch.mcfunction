# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Et vous, qu\'est-ce que vous pensez des cambrioleurs et des tueurs à gages ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Pour moi, il faudrait tirer à balles réelles sur les gens qui sautent les portiques. Ils ne respectent rien, ça justifie donc tout !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je n\'en ai jamais vu, mais comme je lis le journal, je peux vous le dire : il y en a beaucoup trop !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Ces fumiers méritent d\'être séquestrés dans une cave humide pendant quinze semaines pour apprendre à respecter la valeur travail ! Si j\'en vois un qui essaie de passer derrière moi aux valideurs, je vous préviens : je hurle !"'

# Fin de conversation
tag @s add AuRevoir
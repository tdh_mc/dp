# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Boh, je n\'utilise pas trop les transports, mais à mon avis ce n\'est pas le plus grave des problèmes..."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Il y a quand même des problèmes plus importants, non ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas trop pourquoi vous me demandez ça. La dernière fois que j\'ai pris les transports, c\'était l\'an dernier..."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Sans doute quelqu\'un qui connaît bien TCH saura-t-il vous en dire plus que moi..."'

# Fin de conversation
tag @s add AuRevoir
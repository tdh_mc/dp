# On débloque les sujets correspondants chez le joueur
#advancement grant @p[tag=SpeakingToNPC] only tdh:dialogue/tch tch

# Plusieurs cas de figure : soit on est dans une station et l'usager ne veut pas en parler. Soit on est en-dehors d'une station et il aborde le sujet (meilleurs conseils si son niveau de connaissance de TCH est élevé).
execute if entity @p[tag=SpeakingToNPC,distance=..1,tag=tchInside] run function tdh:dialogue/topics/tch/fraude/usager/deteste_tch/en_station

# On est hors d'une station ; on va filtrer à nouveau selon son niveau de connaissance de TCH
execute unless entity @p[tag=SpeakingToNPC,distance=..1,tag=tchInside] run function tdh:dialogue/topics/tch/fraude/usager/deteste_tch/hors_station
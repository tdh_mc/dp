# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"J\'aimerais acheter des tickets."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Il me faudrait quelques tickets !"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Bien sûr ! Comme sur l\'ensemble du réseau, les tarifs sont les suivants : 1 ticket pour 1 lingot de fer, 5 tickets pour 1 lingot d\'or, et 20 tickets pour 1 diamant !"',cliquable:'[{"text":"Bien sûr ! Comme sur l\'ensemble du réseau, les "},{"text":"tarifs","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1110"},"hoverEvent":{"action":"show_text","value":{"text":"Se plaindre des tarifs"}}},{"text":" sont les suivants :"}]'}
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"C\'est comme si c\'était fait ! Je vous laisse regarder les tarifs : combien vous en faut-il ?"',cliquable:'[{"text":"C\'est comme si c\'était fait ! Je vous laisse regarder les "},{"text":"tarifs","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1110"},"hoverEvent":{"action":"show_text","value":{"text":"Se plaindre des tarifs"}}},{"text":" : combien vous en faut-il ?"}]'}

# On débloque les sujets correspondants chez le joueur
advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch/tarifs tarifs


# On active le mode réponse chez l'agent TCH (on enregistre le fait qu'il a posé une question en rapport avec les tickets)
scoreboard players set @s topic 1100

# On active le mode réponse chez le joueur
# Mais on ne désactive PAS la possibilité de proposer un sujet (car on veut pouvoir cliquer notamment sur Tarifs)
#scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les choix de réponse
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"Je vais vous prendre un seul ticket.","clickEvent":{"action":"run_command","value":"/trigger answer set 100"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 fer","color":"gray","bold":"true"},{"text":"."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je vais vous prendre 5 tickets.","clickEvent":{"action":"run_command","value":"/trigger answer set 500"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 or","color":"gold","bold":"true"},{"text":"."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je vais vous prendre 20 tickets.","clickEvent":{"action":"run_command","value":"/trigger answer set 2000"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"1 diamant","color":"gray","bold":"true"},{"text":"."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je vais y réfléchir...","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"La marche à pied, au moins, c\'est gratuit."}]}}]'
# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas trop quoi en penser. Je les ignore et eux aussi, donc on va dire que ce sont des relations cordiales."'

execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"J\'en vois passer de temps à autre. Moi je suis neutre, je ne leur parle pas trop mais ils mettent souvent de l\'ambiance."'
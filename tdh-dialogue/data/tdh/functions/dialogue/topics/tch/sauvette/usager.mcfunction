# Si on n'a pas encore de score d'appréciation de TCH, on en génère un
execute unless score @s likesTCH matches 0..100 run function tdh:dialogue/calcul/type_usager

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous avez déjà vu des vendeurs à la sauvette sur le réseau TCH ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Il y a des gens qui revendent des tickets, sur le réseau TCH ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Ça utilise des tickets frauduleux, dans la région, ou quoi ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Il y a vraiment des gens qui revendent des titres de transport ?"'

# Selon l'appréciation et la connaissance de TCH, on génère une réponse
execute as @s[scores={knowsTCH=40..,likesTCH=80..}] run function tdh:dialogue/topics/tch/sauvette/usager/adore_tch
execute as @s[scores={knowsTCH=40..,likesTCH=50..79}] run function tdh:dialogue/topics/tch/sauvette/usager/aime_tch
execute as @s[scores={knowsTCH=40..,likesTCH=35..49}] run function tdh:dialogue/topics/tch/sauvette/usager/sans_opinion
execute as @s[scores={knowsTCH=40..,likesTCH=..34}] run function tdh:dialogue/topics/tch/sauvette/usager/deteste_tch
execute as @s[scores={knowsTCH=20..39}] run function tdh:dialogue/topics/tch/sauvette/usager/connait_vaguement_tch
execute as @s[scores={knowsTCH=..19}] run function tdh:dialogue/topics/no
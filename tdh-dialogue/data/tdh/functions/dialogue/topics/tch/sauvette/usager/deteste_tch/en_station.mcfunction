# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Je suis obligé de répondre ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Bof !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Vous êtes de la sécurité ? Sympa, votre costume."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je ne vois pas trop où vous voulez en venir avec ce genre de questions."'

# Fin de conversation
tag @s add AuRevoir
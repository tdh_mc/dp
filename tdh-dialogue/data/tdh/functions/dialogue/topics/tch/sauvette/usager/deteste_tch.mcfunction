# On calcule la réponse de l'usager
# Il ne donne des infos que si on est pas dans une station

execute if entity @p[tag=SpeakingToNPC,distance=..1,tag=tchInside] run function tdh:dialogue/topics/tch/sauvette/usager/deteste_tch/en_station
execute unless entity @p[tag=SpeakingToNPC,distance=..1,tag=tchInside] run function tdh:dialogue/topics/tch/sauvette/usager/deteste_tch/hors_station
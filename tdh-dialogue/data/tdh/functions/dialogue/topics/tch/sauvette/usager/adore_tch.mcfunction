# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Méfiez-vous-en ! Si TCH dépense de l\'argent pour avertir les voyageurs, ce n\'est pas pour rien... Ces vauriens vendent des tickets usagés au prix du neuf, quand ce ne sont pas tout simplement des faux. Utilisez des titres officiels ou marchez à pied."'

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je ne comprends pas pourquoi TCH n\'agit pas plus fermement contre ce genre d\'individus ! Ce ne sont ni plus ni moins que des trafiquants de papier recyclé, je ne sais pas si vous vous rendez compte ?"'

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Restez à bonne distance d\'eux, et ne leur parlez pas. Ces gens-là cherchent à vous attirer dans un coin discret — prétextant de vous vendre un ticket 10% moins cher, ou je ne sais quel autre mensonge alléchant — avant de vous détrousser et de vous laisser agonisant sur le bas-côté. Évidemment que j\'en suis sûr, je l\'ai lu sur une page Frambook !"'

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Ils s\'attendent vraiment à ce qu\'on tombe dans le panneau ? Des tickets moins chers que les vrais mais qui fonctionneraient quand même... Qu\'ils me permettent d\'en douter sérieusement !"'

# Fin de conversation
tag @s add AuRevoir
# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'"C\'était donc ça qu\'ils me voulaient ! Des années et des années que je les prenais pour des sous-traitants de TCH. Il faut avouer, à bien y repenser, qu\'ils n\'avaient jamais l\'air de rentrer dans les stations..."',cliquable:'[{"text":"C\'était donc ça qu\'ils me voulaient ! Des années et des années que je les prenais pour des sous-traitants de "},{"text":"TCH","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1000"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de TCH."}]}},{"text":". Il faut avouer, à bien y repenser, qu\'ils n\'avaient jamais l\'air de rentrer dans les stations..."}]'}
execute if score #random temp matches 0..24 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/tch tch

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value {basique:'"Je crois que j\'en ai déjà vu, une fois ! À Ygriak ou à Hadès... ou peut-être à Grenat ? Je ne sais plus, je ne sais vraiment plus..."',cliquable:'[{"text":"Je crois que j\'en ai déjà vu, une fois ! À "},{"text":"Ygriak","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 431"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler d\'Ygriak."}]}},{"text":" ou à "},{"text":"Hadès","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 460"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Hadès."}]}},{"text":"... ou peut-être à "},{"text":"Grenat","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 410"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler de Grenat."}]}},{"text":" ? Je ne sais plus, je ne sais vraiment plus..."}]'}
execute if score #random temp matches 25..49 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/lieux/ygriak
execute if score #random temp matches 25..49 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/lieux/hades
execute if score #random temp matches 25..49 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/lieux/grenat

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"J\'en ai rencontré un, une fois, il s\'appelait... Krantor ? Gronkor ? Prompyl ? Quelque chose comme ça... Enfin, j\'y connais rien, moi, hein... Je dis ça pour aider."'

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"J\'en ai déjà rencontré, à l\'occasion ! La dernière fois, il y en a un, il avait cinq types de la sécurité assis sur son torse. Mais bon, ils avaient l\'air hyper pro, alors je ne me suis pas inquiété."'
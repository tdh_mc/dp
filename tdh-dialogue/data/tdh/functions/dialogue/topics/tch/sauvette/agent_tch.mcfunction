# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Vous voyez souvent des vendeurs à la sauvette ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Qu\'est-ce que vous pouvez me dire sur les vendeurs à la sauvette ?"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Si quelqu\'un d\'autre qu\'un agent TCH vous propose des tickets, ne lui faites pas confiance. En général, leurs tickets sont défectueux et de mauvaise qualité. Et je vous garantis qu\'un contrôleur ne s\'y laissera pas prendre..."',cliquable:'[{"text":"Si quelqu\'un d\'autre qu\'un agent TCH vous propose des tickets, ne lui faites pas confiance. En général, leurs tickets sont défectueux et de mauvaise qualité. Et je vous garantis qu\'un "},{"text":"contrôleur","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1160"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des contrôleurs TCH."}]}},{"text":" ne s\'y laissera pas prendre..."}]'}
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"N\'ayons pas peur des mots : ces personnes sont... vilaines ! Je vous déconseille d\'acheter le moindre titre de transport à quiconque d\'autre qu\'un agent TCH au sein d\'une station du réseau. Vous risquez de ne pas pouvoir le valider, et les contrôleurs sont généralement plus futés que les machines..."',cliquable:'[{"text":"N\'ayons pas peur des mots : ces personnes sont... vilaines ! Je vous déconseille d\'acheter le moindre titre de transport à quiconque d\'autre qu\'un agent TCH au sein d\'une station du réseau. Vous risquez de ne pas pouvoir le valider, et les "},{"text":"contrôleurs","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1160"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des contrôleurs TCH."}]}},{"text":" sont généralement plus futés que les machines..."}]'}

# On débloque les sujets correspondants chez le joueur
advancement grant @p[tag=SpeakingToNPC] only tdh:dialogue/tch/tarifs controleur
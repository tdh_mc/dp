# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Évidemment, il y en a plein. Souvent, ce sont des arnaqueurs, mais il faut simplement savoir reconnaître les bons..."'

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Il y en a pas mal, ça dépend des stations. Si ça vous intéresse, j\'ai quelques bonnes adresses..."'

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Malheureusement, certains d\'entre eux vendent vraiment tout ce qui leur tombe sous la main, sans se préoccuper de savoir si les tickets fonctionnent ou non. Mais il y a des exceptions ; j\'en connais deux ou trois qui ne m\'ont jamais déçu !"'

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Si vous voulez avoir des tickets bien moins chers, les vendeurs à la sauvette sont vos amis ! Je peux vous en dire un peu plus, si ça vous intéresse..."'

# On active le mode réponse chez le PNJ (on enregistre le fait qu'il a posé une question en rapport avec la sauvette)
scoreboard players set @s topic 1120

# On active le mode réponse chez le joueur
# Mais on ne désactive PAS la possibilité de proposer un sujet (car on veut pouvoir cliquer notamment sur Tarifs)
#scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les choix de réponse
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"Vous pourriez me dire quels vendeurs sont fiables ?","clickEvent":{"action":"run_command","value":"/trigger answer set 300"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander comment trouver un bon revendeur de tickets."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Si vous le dites...","clickEvent":{"action":"run_command","value":"/trigger answer set 100"},"hoverEvent":{"action":"show_text","value":[{"text":"Tourner les talons."}]}}]'
# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Dites, je ne veux pas paraître malpoli, mais je n\'ai pas vraiment le temps de parler chiffons..."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Vous savez que TCH paie des gens exprès pour répondre à ce genre de questions ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Désolé, mais j\'ai autre chose à faire."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je pense qu\'un agent saura vous en dire plus que moi."'

# Fin de conversation
tag @s add AuRevoir
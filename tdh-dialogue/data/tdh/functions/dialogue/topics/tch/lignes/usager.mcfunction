# Si on n'a pas encore de score d'appréciation de TCH, on en génère un
execute unless score @s likesTCH matches 0..100 run function tdh:dialogue/calcul/type_usager

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous pouvez me parler des lignes TCH ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous avez quelque chose à me dire sur les lignes TCH ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Elles sont comment, les lignes TCH ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Vous pensez quoi des lignes de TCH ?"'

# Selon l'appréciation et la connaissance de TCH, on génère une réponse
execute as @s[scores={knowsTCH=40..,likesTCH=80..}] run function tdh:dialogue/topics/tch/lignes/usager/adore_tch
execute as @s[scores={knowsTCH=40..,likesTCH=65..79}] run function tdh:dialogue/topics/tch/lignes/usager/aime_tch
execute as @s[scores={knowsTCH=40..,likesTCH=..64}] run function tdh:dialogue/topics/tch/lignes/usager/no
execute as @s[scores={knowsTCH=20..39}] run function tdh:dialogue/topics/tch/lignes/usager/no
execute as @s[scores={knowsTCH=..19}] run function tdh:dialogue/topics/no
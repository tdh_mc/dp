# On exécute cette fonction en tant que le PC de notre ligne préférée

# On sauvegarde le nom de la ligne, et le type de véhicule utilisé (pour pouvoir l'afficher dans les phrases et qu'on ne se marre pas en lisant "J'attendais tous les soirs le dernier métro sur la ligne Câble B ^^^^^^^")
data modify storage tdh:dialogue extra.ligne set from entity @s Item.tag.display.line

# On choisit 2 stations aléatoires de cette ligne
scoreboard players operation #dialogue idLine = @s idLine
execute as @e[type=item_frame,tag=tchQuaiPC] if score @s idLine = #dialogue idLine run tag @s add StationPossible
tag @e[type=item_frame,tag=tchQuaiPC,tag=StationPossible,sort=random,limit=1] add StationPossible1
tag @e[type=item_frame,tag=tchQuaiPC,tag=StationPossible,tag=!StationPossible1,sort=random,limit=1] add StationPossible2

# On enregistre le nom de ces 2 stations
data modify storage tdh:dialogue extra.station1 set from entity @e[type=item_frame,tag=tchQuaiPC,tag=StationPossible1,limit=1] Item.tag.display.station
data modify storage tdh:dialogue extra.station2 set from entity @e[type=item_frame,tag=tchQuaiPC,tag=StationPossible2,limit=1] Item.tag.display.station

# On réinitialise tous les scores temporaires
scoreboard players reset #dialogue idLine
tag @e[type=item_frame,tag=tchQuaiPC,tag=StationPossible] remove StationPossible
tag @e[type=item_frame,tag=tchQuaiPC,tag=StationPossible1] remove StationPossible1
tag @e[type=item_frame,tag=tchQuaiPC,tag=StationPossible2] remove StationPossible2
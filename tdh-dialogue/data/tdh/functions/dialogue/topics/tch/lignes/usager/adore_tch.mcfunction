# Si on n'a pas encore de ligne préférée, on en génère une
execute unless score @s idLine matches 1.. run function tdh:dialogue/calcul/ligne_preferee

# Selon si notre ligne préférée est ouverte ou fermée, on a une phrase différente
# On se donne un tag si notre ligne est ouverte
function tch:tag/pc
execute if entity @e[type=item_frame,tag=tchPC,tag=ourPC,tag=!HorsService] run tag @s add LigneOuverte
# On enregistre les extras nécessaires
execute as @e[type=item_frame,tag=tchPC,tag=ourPC,limit=1] run function tdh:dialogue/topics/tch/lignes/usager/sauvegarder_ligne

# On exécute la bonne fonction
execute as @s[tag=LigneOuverte] run function tdh:dialogue/topics/tch/lignes/usager/adore_tch/ligne_ouverte
execute as @s[tag=!LigneOuverte] run function tdh:dialogue/topics/tch/lignes/usager/adore_tch/ligne_fermee

# On supprime le tag temporaire
tag @s remove LigneOuverte
tag @e[type=item_frame,tag=tchPC,tag=ourPC] remove ourPC
# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous pouvez me parler des lignes TCH ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Il y a combien de lignes, sur le réseau TCH ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Il y a beaucoup de lignes en service, sur le réseau TCH ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Toutes les lignes que je vois sur le plan, elles sont ouvertes ?"'

# On débloque les sujets correspondants chez le joueur
# TODO : Débloquer les sujets spécifiques à chaque ligne
#advancement grant @p[tag=SpeakingToNPC] only tdh:dialogue/tch/xxx yyy

# On va enregistrer dans le storage de dialogue toutes les lignes ouvertes, dans l'ordre de leur ID
function tdh:dialogue/calcul/lignes_ouvertes_fermees

# On calcule ensuite la réponse de l'agent TCH.
# La réponse dépend de la question, donc on ne génère pas de nouvelle variable aléatoire :
#function tdh:random

execute if score #random temp matches 0..24 if score #dialogue temp2 matches 1.. run data modify storage tdh:dialogue reponse set value '[{"text":"Bien sûr ! Il y a actuellement "},{"storage":"tdh:dialogue","nbt":"extra.lignes.n_ouvertes"},{"text":" lignes ouvertes sur le réseau : il s\'agit des "},{"storage":"tdh:dialogue","nbt":"extra.lignes.ouvertes","interpret":true},{"text":".\\n"},{"storage":"tdh:dialogue","nbt":"extra.lignes.n_fermees"},{"text":" autres sont planifiées à ce jour, les lignes "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fermees","interpret":true},{"text":"."}]'
execute if score #random temp matches 0..24 if score #dialogue temp2 matches ..0 run data modify storage tdh:dialogue reponse set value '[{"text":"Bien sûr ! Il y a actuellement "},{"storage":"tdh:dialogue","nbt":"extra.lignes.n"},{"text":" lignes ouvertes sur le réseau : il s\'agit des "},{"storage":"tdh:dialogue","nbt":"extra.lignes.ouvertes","interpret":true},{"text":"."}]'

execute if score #random temp matches 25..74 if score #dialogue temp2 matches 1.. run data modify storage tdh:dialogue reponse set value '[{"text":"Pour l\'instant, il y a "},{"storage":"tdh:dialogue","nbt":"extra.lignes.n"},{"text":" lignes, dont "},{"storage":"tdh:dialogue","nbt":"extra.lignes.n_ouvertes"},{"text":" sont ouvertes : "},{"storage":"tdh:dialogue","nbt":"extra.lignes.ouvertes","interpret":true},{"text":".\\n"},{"storage":"tdh:dialogue","nbt":"extra.lignes.n_fermees"},{"text":" autres sont planifiées à ce jour, les lignes "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fermees","interpret":true},{"text":"."}]'
execute if score #random temp matches 25..74 if score #dialogue temp2 matches ..0 run data modify storage tdh:dialogue reponse set value '[{"text":"Pour l\'instant, l\'intégralité des "},{"storage":"tdh:dialogue","nbt":"extra.lignes.n"},{"text":" lignes du réseau sont ouvertes : il s\'agit des "},{"storage":"tdh:dialogue","nbt":"extra.lignes.ouvertes","interpret":true},{"text":"."}]'

execute if score #random temp matches 75..99 if score #dialogue temp2 matches 1.. run data modify storage tdh:dialogue reponse set value '[{"text":"Pour l\'instant, il y a "},{"storage":"tdh:dialogue","nbt":"extra.lignes.n_ouvertes"},{"text":" lignes ouvertes : "},{"storage":"tdh:dialogue","nbt":"extra.lignes.ouvertes","interpret":true},{"text":".\\n"},{"storage":"tdh:dialogue","nbt":"extra.lignes.n_fermees"},{"text":" autres sont fermées pour l\'instant, les lignes "},{"storage":"tdh:dialogue","nbt":"extra.lignes.fermees","interpret":true},{"text":"."}]'
execute if score #random temp matches 75..99 if score #dialogue temp2 matches ..0 run data modify storage tdh:dialogue reponse set value '[{"text":"Oui, l\'intégralité des "},{"storage":"tdh:dialogue","nbt":"extra.lignes.n"},{"text":" lignes du réseau sont ouvertes à l\'heure actuelle : il s\'agit des "},{"storage":"tdh:dialogue","nbt":"extra.lignes.ouvertes","interpret":true},{"text":"."}]'

# Les variables temporaires sont réinitialisées à la fin de la conversation
# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Ça ne sera pas long ! Vous devez déjà savoir qu\'elle coûte 2 lingots de fer, après quoi vous pourrez charger des jours d\'abonnement directement auprès d\'une borne automatique. Alors, je vous en initialise une ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Vous ne le regretterez pas ! Comme vous devez le savoir, la carte Gold coûte 2 lingots de fer ; une fois achetée, il vous suffira de la recharger à une borne de vente pour pouvoir l\'utiliser. Alors, ça vous tente ?"'

# On active le mode réponse chez l'agent TCH (on enregistre le fait qu'il a posé une question en rapport avec la carte GOLD)
scoreboard players set @s topic 1510

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les choix de réponse
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"Voici ce que je vous dois.","clickEvent":{"action":"run_command","value":"/trigger answer set 1789"},"hoverEvent":{"action":"show_text","value":[{"text":"Payer ","color":"yellow"},{"text":"2 fer","color":"gray","bold":"true"},{"text":"."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je vais y réfléchir...","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Changer d\'avis."}]}}]'
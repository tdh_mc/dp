# Si on n'a pas encore de score d'appréciation de TCH, on en génère un
execute unless score @s likesTCH matches 0..100 run function tdh:dialogue/calcul/type_usager

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Vous ne sauriez pas où je peux recharger ma carte Gold, par hasard ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Vous avez une idée d\'où je pourrais recharger une carte Gold, par ici ?"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 if score @s knowsTCH matches 30.. run data modify storage tdh:dialogue reponse set value '"Vous devriez chercher un distributeur : un ordinateur antédéluvien surmonté d\'un panneau vert. Bon courage !"'
execute if score #random temp matches 50..99 if score @s knowsTCH matches 30.. run data modify storage tdh:dialogue reponse set value '"Cherchez donc un distributeur ! Il y en a dans presque toutes les stations."'

execute if score #random temp matches 0..49 if score @s knowsTCH matches ..29 run data modify storage tdh:dialogue reponse set value '"Je n\'en ai pas la moindre idée. Allez donc leur demander !"'
execute if score #random temp matches 50..99 if score @s knowsTCH matches ..29 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas trop. Désolé de ne pas pouvoir vous aider !"'

# Dans un cas très spécifique (très bonne connaissance de TCH et appréciation assez médiocre),
# on débloque le sujet qui permet de recharger auprès d'un agent TCH
execute if score #random temp matches 35..76 if score @s knowsTCH matches 91.. if score @s likesTCH matches ..49 run data modify storage tdh:dialogue reponse set value '"Aucun agent TCH ne vous l\'avouera, mais il est tout à fait possible de recharger votre abonnement au guichet ! Insistez un peu, faites valoir vos droits : si vous êtes au courant, ils n\'auront pas d\'autre choix que d\'accepter !"'
execute if score #random temp matches 35..76 if score @s knowsTCH matches 91.. if score @s likesTCH matches ..49 run advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch/abonnement recharger_au_guichet

# Fin de conversation
tag @s add AuRevoir
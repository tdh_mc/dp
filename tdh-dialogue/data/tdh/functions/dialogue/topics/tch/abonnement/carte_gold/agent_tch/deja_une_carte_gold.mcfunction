# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Excusez-moi, mais la machine me dit que vous avez déjà une carte Gold. Si vous souhaitez la recharger, c\'est vers une borne de vente qu\'il faut vous diriger !"',cliquable:'[{"text":"Excusez-moi, mais la machine me dit que vous avez déjà une carte Gold. Si vous souhaitez la "},{"text":"recharger","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1520"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander comment recharger une carte Gold."}]}},{"text":", c\'est vers une borne de vente qu\'il faut vous diriger!"}]'}
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'"Si j\'en crois le capteur magnétique sur lequel vous marchez en ce moment même, vous avez déjà une carte Gold sur vous. Si vous souhaitez la recharger, c\'est à une borne automatique que ça se passe !"',cliquable:'[{"text":"Si j\'en crois le capteur magnétique sur lequel vous marchez en ce moment même, vous avez déjà une carte Gold sur vous. Si vous souhaitez la "},{"text":"recharger","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1520"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander comment recharger une carte Gold."}]}},{"text":", c\'est à une borne automatique que ça se passe !"}]'}

# On débloque les sujets correspondants chez le joueur
advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch/abonnement recharger
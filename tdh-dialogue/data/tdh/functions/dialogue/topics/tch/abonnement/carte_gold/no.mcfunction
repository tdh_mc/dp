# Si on n'a pas encore de score d'appréciation de TCH, on en génère un
execute unless score @s likesTCH matches 0..100 run function tdh:dialogue/calcul/type_usager

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"J\'aimerais prendre une carte Gold. Comment je peux faire ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Vous sauriez où je peux trouver une carte Gold ?"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 if score @s knowsTCH matches 30.. run data modify storage tdh:dialogue reponse set value '"C\'est à un agent TCH qu\'il faut demander ça. Je ne vous serai pas d\'un grand secours..."'
execute if score #random temp matches 50..99 if score @s knowsTCH matches 30.. run data modify storage tdh:dialogue reponse set value '"Je ne crois pas que je puisse vous aider. Allez donc demander à un agent, il sera certainement ravi de perdre deux heures à vous faire remplir le contrat !"'

execute if score #random temp matches 0..99 if score @s knowsTCH matches ..29 run data modify storage tdh:dialogue reponse set value '"Vous avez dû vous tromper de personne, désolé."'

# Fin de conversation
tag @s add AuRevoir
# Si on n'a pas encore de score d'appréciation de TCH, on en génère un
execute unless score @s likesTCH matches 0..100 run function tdh:dialogue/calcul/type_usager

# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Comment je peux faire pour m\'abonner au réseau TCH ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Vous savez comment je pourrais prendre un abonnement TCH ?"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 if score @s knowsTCH matches 30.. run data modify storage tdh:dialogue reponse set value '"Allez donc voir un agent, il vous expliquera ça mieux que moi !"'
execute if score #random temp matches 50..99 if score @s knowsTCH matches 30.. run data modify storage tdh:dialogue reponse set value '"Ça ne vous paraîtrait pas plus pertinent de demander ça à un de leurs employés ? Ils ne me paient pas pour vous répondre, moi !"'

execute if score #random temp matches 0..99 if score @s knowsTCH matches 15..29 run data modify storage tdh:dialogue reponse set value '"Je n\'en sais rien du tout. Allez donc leur demander à eux !"'
execute if score #random temp matches 0..99 if score @s knowsTCH matches ..14 run data modify storage tdh:dialogue reponse set value '"C\'est quoi TCH ? Ça se mange ?"'

# Fin de conversation
tag @s add AuRevoir
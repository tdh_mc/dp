# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Comment recharger ma carte Gold ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Ça se passe comment, pour recharger ma carte ?"'

# On génère une réponse aléatoire du PNJ
function tdh:random
# On se tag s'il y a un distributeur à proximité
execute if entity @e[type=item_frame,tag=Distributeur,distance=..75,limit=1] run tag @s add DistributeurPresent

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'"Si vous souhaitez recharger votre carte Gold, c\'est vers une borne de vente qu\'il faut vous diriger ! Une semaine d\'abonnement vous coûtera 3 lingots de fer, un mois d\'abonnement 3 lingots d\'or, et quatre mois d\'abonnement 3 diamants. Si vous n\'avez qu\'un seul lingot d\'or ou diamant, vous pourrez tout de même payer respectivement une semaine et un mois d\'abonnement."',cliquable:'[{"text":"Si vous souhaitez recharger votre "},{"text":"carte Gold","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1510"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander à prendre une carte Gold."}]}},{"text":", c\'est vers une borne de vente qu\'il faut vous diriger !\\nUne semaine d\'abonnement vous coûtera "},{"text":"3 lingots de fer","bold":"true"},{"text":", un mois d\'abonnement "},{"text":"3 lingots d\'or","bold":"true"},{"text":", et quatre mois d\'abonnement "},{"text":"3 diamants","bold":"true"},{"text":".\\nSi vous n\'avez qu\'un seul "},{"text":"lingot d\'or","bold":"true"},{"text":" ou "},{"text":"diamant","bold":"true"},{"text":", vous pourrez tout de même payer respectivement une semaine et un mois d\'abonnement."}]'}
execute if score #random temp matches 50..99 as @s[tag=DistributeurPresent] run data modify storage tdh:dialogue reponse set value {basique:'"La carte Gold ne se recharge qu\'aux bornes de vente ! Vous devriez trouver facilement, il y en a plusieurs dans cette station. Cherchez les panneaux verts !"',cliquable:'[{"text":"La "},{"text":"carte Gold","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1510"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander à prendre une carte Gold."}]}},{"text":" ne se recharge qu\'aux bornes de vente ! Vous devriez trouver facilement, il y en a plusieurs dans cette station. Cherchez les panneaux verts !"}]'}
execute if score #random temp matches 50..99 as @s[tag=!DistributeurPresent] run data modify storage tdh:dialogue reponse set value {basique:'"La carte Gold ne se recharge malheureusement qu\'aux bornes de vente ! Il n\'y en a pas dans cette station, mais la plupart des autres sont mieux équipées. Bon courage à vous !"',cliquable:'[{"text":"La "},{"text":"carte Gold","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1510"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander à prendre une carte Gold."}]}},{"text":" ne se recharge malheureusement qu\'aux bornes de vente ! Il n\'y en a pas dans cette station, mais la plupart des autres sont mieux équipées. Bon courage à vous !"}]'}

# On débloque les sujets correspondants chez le joueur
advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/tch/abonnement carte_gold

# On supprime les tags temporaires
tag @s remove DistributeurPresent
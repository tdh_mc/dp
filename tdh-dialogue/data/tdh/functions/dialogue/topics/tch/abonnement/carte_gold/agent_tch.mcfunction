# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"J\'aimerais vous prendre une carte Gold."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Ça serait possible de me faire une carte Gold ?"'

# On vérifie si le joueur a déjà une carte gold sur lui
execute store result score #dialogue temp8 run clear @p[tag=SpeakingToNPC] paper{tch:{type:2b}} 0

# On exécute la bonne fonction en fonction du résultat du test
# Si on a déjà une carte gold sur nous
execute if score #dialogue temp8 matches 1.. run function tdh:dialogue/topics/tch/abonnement/carte_gold/agent_tch/deja_une_carte_gold

# Si on n'a pas de carte gold sur nous
execute if score #dialogue temp8 matches 0 run function tdh:dialogue/topics/tch/abonnement/carte_gold/agent_tch/acheter_carte_gold

# On réinitialise la variable temporaire
scoreboard players reset #dialogue temp8
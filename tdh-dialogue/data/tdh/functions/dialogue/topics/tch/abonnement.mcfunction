# Ce sujet ne fonctionne qu'avec les agents TCH, donc toute autre personne n'aura qu'une réponse négative générique.
execute as @s[tag=AgentTCH] run function tdh:dialogue/topics/tch/abonnement/agent_tch
execute as @s[tag=!AgentTCH] run function tdh:dialogue/topics/tch/abonnement/no
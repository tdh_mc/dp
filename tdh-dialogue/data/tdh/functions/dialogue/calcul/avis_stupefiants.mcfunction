# On génère aléatoirement un avis sur les stupéfiants avec en gros :
# 00 (déteste absolument les stupéfiants et tout ce qui s'y rapporte)
# 25 (déteste les stupéfiants mais apprécie l'alcool)
# 50 (déteste les stupéfiants mais apprécie l'alcool et le tabac)
# 75 (déteste les drogues dures mais apprécie l'alcool, le tabac et l'herbe)
# 100 (apprécie absolument tous types de stupéfiants)

# Les scores intermédiaires faisant varier l'appréciation du précédent type de produit

# L'avis n'est pas pas vraiment équilibré, avec beaucoup plus de gens vers le début du spectre que vers la fin,
# mais quand même davantage de gens qui apprécient l'alcool que de gens qui ne l'apprécient pas ;
# avec moins d'appréciation chez les citadins que chez les campagnards
# et davantage d'appréciation si on est buraliste ou tavernier
# et une appréciation lockée entre 80 et 99 pour les vendeurs desdits stupéfiants

# On génère par défaut un avis allant de -100 à 900
scoreboard players set #random min -100
scoreboard players set #random max 900
function tdh:random
scoreboard players operation #dialogue temp = #random temp

# On module cet avis selon les paramètres spécifiés ci-dessus
# Les citadins ont un malus de 20%
execute if entity @e[type=armor_stand,tag=NomVille,distance=..150] run scoreboard players remove #dialogue temp 200
# Si on est dans une auberge, on a un bonus de 10%
execute if entity @e[type=item_frame,tag=Auberge,distance=..20] run scoreboard players add #dialogue temp 100
# Si on est dans une taverne, on a un bonus de 20%
execute if entity @e[type=item_frame,tag=Taverne,distance=..20] run scoreboard players add #dialogue temp 200
# Si on est buraliste, tavernier ou aubergiste, on a une appréciation supérieure
execute as @s[tag=Buraliste] run scoreboard players add #dialogue temp 450
execute as @s[tag=Tavernier] run scoreboard players add #dialogue temp 300
execute as @s[tag=Aubergiste] run scoreboard players add #dialogue temp 200
# Si on est dealer, on a une appréciation colossalement supérieure
execute as @s[tag=Dealer] run scoreboard players add #dialogue temp 500
# On a moins de chances d'apprécier les stupéfiants si on aime TCH,
# et plus de chances de les apprécier si on n'aime pas TCH (et encore davantage si on en a une bonne connaissance)
execute as @s[tag=!AgentTCH] run function tdh:dialogue/calcul/type_usager
execute if score @s likesTCH matches 50.. run scoreboard players remove #dialogue temp 75
execute if score @s likesTCH matches 75.. run scoreboard players remove #dialogue temp 150
execute if score @s likesTCH matches ..35 run scoreboard players add #dialogue temp 75
execute if score @s likesTCH matches ..35 if score @s knowsTCH matches 65.. run scoreboard players add #dialogue temp 150
# Si on est à Ygriak, Fénicia, Oréa-sur-Mer, Moronia ou Eïdin-la-Vallée, on a beaucoup plus de chances de les apprécier
execute if entity @e[type=armor_stand,name="Ygriak",distance=..150] run scoreboard players add #dialogue temp 500
execute if entity @e[type=armor_stand,name="Fénicia",distance=..150] run scoreboard players add #dialogue temp 350
execute if entity @e[type=armor_stand,name="Oréa-sur-Mer",distance=..150] run scoreboard players add #dialogue temp 350
execute if entity @e[type=armor_stand,name="Moronia",distance=..150] run scoreboard players add #dialogue temp 500
execute if entity @e[type=armor_stand,name="Eïdin-la-Vallée",distance=..150] run scoreboard players add #dialogue temp 300
# Si on est à Duerrom, Tolbrok ou Hadès, on a davantage de chances de les apprécier
execute if entity @e[type=armor_stand,name="Duerrom",distance=..150] run scoreboard players add #dialogue temp 200
execute if entity @e[type=armor_stand,name="Tolbrok",distance=..150] run scoreboard players add #dialogue temp 150
execute if entity @e[type=armor_stand,name="Hadès",distance=..150] run scoreboard players add #dialogue temp 200
# Si on est en région elfique, on a moins de chances de les apprécier
execute if entity @e[type=armor_stand,name="Dorlinor",distance=..150] run scoreboard players remove #dialogue temp 200
execute if entity @e[type=armor_stand,name="Athès",distance=..150] run scoreboard players remove #dialogue temp 150
execute if entity @e[type=armor_stand,name="Midès",distance=..150] run scoreboard players remove #dialogue temp 150

# On vérifie si on dépasse de la limite de 0-1000
execute if score #dialogue temp matches ..-1 run scoreboard players set #dialogue temp 0
execute if score #dialogue temp matches 1001.. run scoreboard players set #dialogue temp 1000

# Ensuite, on calcule l'avis final
# Si on est entre 0 et 500, ça va de "je déteste tout" à "j'apprécie l'alcool" (on scale 0-500 à 0-25)
execute if score #dialogue temp matches 0..500 run function tdh:dialogue/calcul/avis_stupefiants/alcool
# Si on est entre 500 et 750, on apprécie l'alcool et de plus en plus le tabac (on scale 501-750 à 26-50)
execute if score #dialogue temp matches 501..750 run function tdh:dialogue/calcul/avis_stupefiants/tabac
# Si on est entre 750 et 900, on apprécie l'alcool, le tabac et de +en+ l'herbe (on scale 751-900 à 51-75)
execute if score #dialogue temp matches 751..900 run function tdh:dialogue/calcul/avis_stupefiants/herbe
# Si on est entre 900 et 1000, on apprécie de "tout sauf ddures" à tout sans exception (on scale 900-1000 à 76-100)
execute if score #dialogue temp matches 901..1000 run function tdh:dialogue/calcul/avis_stupefiants/tout


# Une fois qu'on a calculé l'avis, si on est à la campagne et qu'on a un score inférieur à 15 (apprécie l'alcool à moitié), on remonte le score
execute if score @s likesDrugs matches ..14 unless entity @e[type=armor_stand,tag=NomVille,distance=..200] unless entity @e[type=armor_stand,tag=NomStation,distance=..75] run scoreboard players set @s likesDrugs 15


# Après avoir calculé l'avis, on calcule également la connaissance des stupéfiants
function tdh:dialogue/calcul/avis_stupefiants/connaissance_stupefiants
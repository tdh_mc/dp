# Personne qui aime l'alcool à 100%, le tabac à 100%, l'herbe à 100% et tout le reste entre 0 et 100%
# On scale 901-1000 (dans #dialogue temp) à 76-100 (dans @s likesDrugs)

tellraw @s [{"score":{"name":"#dialogue","objective":"temp"},"color":"white"}]

scoreboard players operation @s likesDrugs = #dialogue temp
scoreboard players remove @s likesDrugs 901
scoreboard players set #dialogue temp2 4
scoreboard players operation @s likesDrugs /= #dialogue temp2
scoreboard players add @s likesDrugs 76

# On réinitialise la variable temporaire
scoreboard players reset #dialogue temp2

tellraw @s [{"score":{"name":"@s","objective":"likesDrugs"},"color":"white"}]
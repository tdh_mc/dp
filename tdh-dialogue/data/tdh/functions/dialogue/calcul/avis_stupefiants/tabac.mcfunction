# Personne qui aime l'alcool à 100%, le tabac (entre 0 et 100%) et aucun autre produit
# On scale 501-750 (dans #dialogue temp) à 26-50 (dans @s likesDrugs)

tellraw @s [{"score":{"name":"#dialogue","objective":"temp"},"color":"gold"}]

scoreboard players operation @s likesDrugs = #dialogue temp
scoreboard players remove @s likesDrugs 501
scoreboard players set #dialogue temp2 10
scoreboard players operation @s likesDrugs /= #dialogue temp2
scoreboard players add @s likesDrugs 26

# On réinitialise la variable temporaire
scoreboard players reset #dialogue temp2

tellraw @s [{"score":{"name":"@s","objective":"likesDrugs"},"color":"gold"}]
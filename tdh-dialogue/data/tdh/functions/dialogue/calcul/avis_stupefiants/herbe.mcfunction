# Personne qui aime l'alcool à 100%, le tabac à 100%, l'herbe (entre 0 et 100%) et aucun autre produit
# On scale 751-900 (dans #dialogue temp) à 51-75 (dans @s likesDrugs)

tellraw @s [{"score":{"name":"#dialogue","objective":"temp"},"color":"green"}]

scoreboard players operation @s likesDrugs = #dialogue temp
scoreboard players remove @s likesDrugs 751
scoreboard players set #dialogue temp2 6
scoreboard players operation @s likesDrugs /= #dialogue temp2
scoreboard players add @s likesDrugs 51

# On réinitialise la variable temporaire
scoreboard players reset #dialogue temp2

tellraw @s [{"score":{"name":"@s","objective":"likesDrugs"},"color":"green"}]
# Personne qui n'aime que l'alcool (entre 0 et 100%) et aucun autre produit
# On scale 0-500 (dans #dialogue temp) à 0-25 (dans @s likesDrugs)

tellraw @s [{"score":{"name":"#dialogue","objective":"temp"},"color":"red"}]

scoreboard players operation @s likesDrugs = #dialogue temp
scoreboard players set #dialogue temp2 20
scoreboard players operation @s likesDrugs /= #dialogue temp2

# On réinitialise la variable temporaire
scoreboard players reset #dialogue temp2

tellraw @s [{"score":{"name":"@s","objective":"likesDrugs"},"color":"red"}]
# On génère aléatoirement une connaissance des stupéfiants selon l'avis qu'on en a
# Déterminera notamment si le PNJ connaît des dealers et des astuces

# Il y a quand même globalement beaucoup plus de gens qui ne s'y connaissent pas,
# mais il y a aussi bcp plus de consommateurs qui s'y connaissent

# On génère par défaut un avis allant de -200 à 50 (sera capé à la fin à 0-100)
scoreboard players set #random min -200
scoreboard players set #random max 50
function tdh:random
scoreboard players operation #dialogue temp = #random temp

# On module cet avis selon les paramètres spécifiés ci-dessus
# Les citadins ont un bonus léger
execute if entity @e[type=armor_stand,tag=NomVille,distance=..150] run scoreboard players add #dialogue temp 25
# Si on est dans une taverne, on a un léger bonus
execute if entity @e[type=item_frame,tag=Taverne,distance=..20] run scoreboard players add #dialogue temp 10
# Si on est buraliste, tavernier ou aubergiste, on a une connaissance supérieure
execute as @s[tag=Buraliste] run scoreboard players add #dialogue temp 30
execute as @s[tag=Tavernier] run scoreboard players add #dialogue temp 20
execute as @s[tag=Aubergiste] run scoreboard players add #dialogue temp 10
# Si on est dealer, on a une connaissance colossalement supérieure
execute as @s[tag=Dealer] run scoreboard players add #dialogue temp 125
# La connaissance est corrélée à l'appréciation des stupéfiants au dessus de 50
scoreboard players operation #dialogue temp2 = @s likesDrugs
scoreboard players remove #dialogue temp2 50
execute if score #dialogue temp2 matches ..-1 run scoreboard players set #dialogue temp2 0
scoreboard players operation #dialogue temp += #dialogue temp2
scoreboard players reset #dialogue temp2
# Si on est à Grenat, Oréa-sur-Mer, Le Hameau ou Procyon, on a une bien meilleure connaissance
execute if entity @e[type=armor_stand,name="Le Hameau",distance=..150] run scoreboard players add #dialogue temp 30
execute if entity @e[type=armor_stand,name="Grenat",distance=..150] run scoreboard players add #dialogue temp 35
execute if entity @e[type=armor_stand,name="Oréa-sur-Mer",distance=..150] run scoreboard players add #dialogue temp 30
execute if entity @e[type=armor_stand,name="Procyon",distance=..150] run scoreboard players add #dialogue temp 25
# Si on est à Fénicia, Eïdin-la-Vallée, Duerrom, Les Sablons, Dorlinor ou Hadès, on a davantage de chances d'avoir une bonne connaissance
execute if entity @e[type=armor_stand,name="Fénicia",distance=..150] run scoreboard players add #dialogue temp 15
execute if entity @e[type=armor_stand,name="Eïdin-la-Vallée",distance=..150] run scoreboard players add #dialogue temp 15
execute if entity @e[type=armor_stand,name="Duerrom",distance=..150] run scoreboard players add #dialogue temp 20
execute if entity @e[type=armor_stand,name="Les Sablons",distance=..150] run scoreboard players add #dialogue temp 20
execute if entity @e[type=armor_stand,name="Dorlinor",distance=..150] run scoreboard players add #dialogue temp 20
execute if entity @e[type=armor_stand,name="Hadès",distance=..150] run scoreboard players add #dialogue temp 15

# On vérifie si on dépasse de la limite de 0-100
execute if score #dialogue temp matches ..-1 run scoreboard players set #dialogue temp 0
execute if score #dialogue temp matches 101.. run scoreboard players set #dialogue temp 100

# On assigne enfin la valeur de la variable au PNJ
scoreboard players operation @s knowsDrugs = #dialogue temp
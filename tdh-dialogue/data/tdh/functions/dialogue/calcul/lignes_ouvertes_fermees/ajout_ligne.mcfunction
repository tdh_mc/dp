# On ajoute la ligne qui exécute cette fonction au bon storage (lignes ouvertes ou lignes fermées)
execute as @s[tag=EnService] run function tdh:dialogue/calcul/lignes_ouvertes_fermees/ajout_ligne_ouverte
execute as @s[tag=!EnService] run function tdh:dialogue/calcul/lignes_ouvertes_fermees/ajout_ligne_fermee

# On se retire le tag temporaire
tag @s remove tchPCtemp
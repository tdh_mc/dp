# On retranche 1 au nombre de lignes ouvertes restant à lister
scoreboard players remove #dialogue temp 1

# On exécute cette fonction à la position d'une item frame TexteTemporaire et donc d'un panneau
data modify block ~ ~ ~ Text1 set from entity @s Item.tag.display.line.nom
execute if score #dialogue temp matches 1.. run data modify block ~ ~ ~ Text2 set value '[{"storage":"tdh:dialogue","nbt":"extra.lignes.ouvertes","interpret":true},{"block":"~ ~ ~","nbt":"Text1","interpret":true},{"text":", "}]'
execute if score #dialogue temp matches ..0 run data modify block ~ ~ ~ Text2 set value '[{"storage":"tdh:dialogue","nbt":"extra.lignes.ouvertes","interpret":true},{"text":"et "},{"block":"~ ~ ~","nbt":"Text1","interpret":true}]'
data modify storage tdh:dialogue extra.lignes.ouvertes set from block ~ ~ ~ Text2
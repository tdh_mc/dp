# On choisit un des PC de ligne ayant le tag tchPCtemp,
# en commençant par celui ayant l'ID de ligne le plus bas

# On calcule le plus petit ID de ligne
scoreboard players set #dialogue idLine 99999999
execute as @e[type=item_frame,tag=tchPCtemp] run scoreboard players operation #dialogue idLine < @s idLine

# On ajoute au bon storage la ligne ayant l'ID le plus bas, et on retire son tag
execute at @e[type=item_frame,tag=TexteTemporaire,limit=1] as @e[type=item_frame,tag=tchPCtemp] if score @s idLine = #dialogue idLine run function tdh:dialogue/calcul/lignes_ouvertes_fermees/ajout_ligne

# S'il reste des lignes ayant le tag temporaire, on recommence cette fonction
execute if entity @e[type=item_frame,tag=tchPCtemp,limit=1] run function tdh:dialogue/calcul/lignes_ouvertes_fermees/choix_ligne
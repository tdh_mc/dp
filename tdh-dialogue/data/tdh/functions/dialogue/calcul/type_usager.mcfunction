# On génère d'abord une connaissance de TCH :
scoreboard players set @s knowsTCH 0

# On a différentes conditions pour augmenter la connaissance de TCH (0-100) :
# (+30) : On est dans une station
execute if entity @p[distance=0,tag=tchInside] run scoreboard players add @s knowsTCH 30
# (+10..20) : On est à proximité d'une station
execute if entity @e[tag=NomStation,distance=..80] run scoreboard players add @s knowsTCH 10
execute if entity @e[tag=NomStation,distance=..160] run scoreboard players add @s knowsTCH 20
# (+30) : On est en ville
execute if entity @e[tag=NomVille,distance=..200] run scoreboard players add @s knowsTCH 30
# (+10) : On est dans un village
execute if entity @e[tag=NomVillage,distance=..100] run scoreboard players add @s knowsTCH 10

# On prend un nombre aléatoire pour moduler la connaissance de TCH
scoreboard players set #random min 50
scoreboard players set #random max 150
function tdh:random
# À ce stade, on a temp = nombre aléatoire entre 50 et 150
# On multiplie donc par temp, puis on divise par 100,
# pour moduler la connaissance finale de TCH entre 50% et 150% de sa valeur initiale.
scoreboard players operation @s knowsTCH *= #random temp
scoreboard players operation @s knowsTCH /= #random max


# Si on ne connaît pas ou peu TCH, on en a une appréciation neutre
execute as @s[scores={knowsTCH=..29}] run scoreboard players set @s likesTCH 47
# Sinon, on génère aléatoirement une valeur d'appréciation de TCH
execute as @s[scores={knowsTCH=30..}] run function tdh:random
execute as @s[scores={knowsTCH=30..}] run scoreboard players operation @s likesTCH = #random temp
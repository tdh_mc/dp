### CALCUL DE LA LISTE DES LIGNES OUVERTES ET FERMÉES
### Renverra des données dans extra de la forme:
### {
###     lignes: {
###         n: <int> (le nombre de lignes),
###         n_fermees: <int> (le nombre de lignes fermées),
###         n_ouvertes: <int> (le nombre de lignes ouvertes),
###         fermees: <texte JSON> (la liste formatée des lignes fermées),
###         ouvertes: <texte JSON> (la liste formatée des lignes ouvertes)
###     }
### }

# On compte dans une variable temporaire le nombre de lignes en service (temp) et hors service (temp2)
# On stocke également le nombre total de lignes (temp3)
scoreboard players set #dialogue temp 0
scoreboard players set #dialogue temp2 0
execute as @e[type=item_frame,tag=tchPC,tag=EnService] run scoreboard players add #dialogue temp 1
execute as @e[type=item_frame,tag=tchPC,tag=HorsService] run scoreboard players add #dialogue temp2 1
scoreboard players operation #dialogue temp3 = #dialogue temp
scoreboard players operation #dialogue temp3 += #dialogue temp2
# On stocke ces valeurs dans le storage, car on va les modifier ci-après pour savoir quand mettre des virgules et quand mettre des "et"
execute store result storage tdh:dialogue extra.lignes.n int 1 run scoreboard players get #dialogue temp3
execute store result storage tdh:dialogue extra.lignes.n_fermees int 1 run scoreboard players get #dialogue temp2
execute store result storage tdh:dialogue extra.lignes.n_ouvertes int 1 run scoreboard players get #dialogue temp

# On initialise d'abord le texte à afficher
data modify storage tdh:dialogue extra.lignes.ouvertes set value '[{"text":""}]'
data modify storage tdh:dialogue extra.lignes.fermees set value '[{"text":""}]'
# On donne un tag temporaire à toutes les lignes TCH
tag @e[type=item_frame,tag=tchPC] add tchPCtemp
# On ajoute chacune de ces lignes au storage approprié, dans l'ordre de leurs ID de ligne
function tdh:dialogue/calcul/lignes_ouvertes_fermees/choix_ligne

scoreboard players reset #dialogue temp
# #dialogue temp2 est une valeur de retour : elle contient le nombre de lignes fermées
execute store result score #dialogue temp2 run data get storage tdh:dialogue extra.lignes.n_fermees
#scoreboard players reset #dialogue temp2
scoreboard players reset #dialogue temp3
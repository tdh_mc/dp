# Cette fonction est exécutée par un villageois en cours de dialogue,
# avec ces 4 variables assignées :
# PosX et PosZ à la position du villageois
# DeltaX et DeltaZ à la position de la cible à localiser

# On retranche PosX et PosZ à DeltaX et DeltaZ pour stocker dans DeltaX/Z la différence entre les 2 coordonnées
scoreboard players operation @s deltaX -= @s posX
scoreboard players operation @s deltaZ -= @s posZ

# On calcule le rapport entre les deux directions
# posX = abs(deltaX) * 10
scoreboard players set @s posX 0
execute if score @s deltaX matches ..-1 run scoreboard players operation @s posX -= @s deltaX
execute if score @s deltaX matches 0.. run scoreboard players operation @s posX = @s deltaX
scoreboard players set @s posZ 10
scoreboard players operation @s deltaX *= @s posZ

# posZ = abs(deltaZ)
scoreboard players set @s posZ 0
execute if score @s deltaZ matches ..-1 run scoreboard players operation @s posZ -= @s deltaZ
execute if score @s deltaZ matches 0.. run scoreboard players operation @s posZ = @s deltaZ

# posX = 10 * abs(deltaX/deltaZ)
scoreboard players operation @s posX /= @s posZ


# On stocke dans le storage la direction de la position
# posX > 20 donc on est majoritairement sur l'axe nord/sud
execute if score @s posX matches 20.. if score @s deltaZ matches 0.. run data modify storage tdh:locate resultat.direction set value {alAu:"au ",deDu:"du ",prefixe:"le ",nom:"sud"}
execute if score @s posX matches 20.. if score @s deltaZ matches ..-1 run data modify storage tdh:locate resultat.direction set value {alAu:"au ",deDu:"du ",prefixe:"le ",nom:"nord"}

# posX < 5 donc on est majoritairement sur l'axe est/ouest
execute if score @s posX matches ..5 if score @s deltaX matches 0.. run data modify storage tdh:locate resultat.direction set value {alAu:"à l'",deDu:"de l'",prefixe:"l'",nom:"est"}
execute if score @s posX matches ..5 if score @s deltaX matches ..-1 run data modify storage tdh:locate resultat.direction set value {alAu:"à l'",deDu:"de l'",prefixe:"l'",nom:"ouest"}

# Sinon on est sur un axe diagonal
execute if score @s posX matches 6..19 if score @s deltaX matches 0.. if score @s deltaZ matches 0.. run data modify storage tdh:locate resultat.direction set value {alAu:"au ",deDu:"du ",prefixe:"le ",nom:"sud-est"}
execute if score @s posX matches 6..19 if score @s deltaX matches ..-1 if score @s deltaZ matches 0.. run data modify storage tdh:locate resultat.direction set value {alAu:"au ",deDu:"du ",prefixe:"le ",nom:"sud-ouest"}
execute if score @s posX matches 6..19 if score @s deltaX matches 0.. if score @s deltaZ matches ..-1 run data modify storage tdh:locate resultat.direction set value {alAu:"au ",deDu:"du ",prefixe:"le ",nom:"nord-est"}
execute if score @s posX matches 6..19 if score @s deltaX matches ..-1 if score @s deltaZ matches ..-1 run data modify storage tdh:locate resultat.direction set value {alAu:"au ",deDu:"du ",prefixe:"le ",nom:"nord-ouest"}
# On choisit la ligne préférée d'un usager
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# Une des lignes principales (70% au total)
# 15% de chances pour M1 et M8 (30% au total)
execute if score #random temp matches ..14 run scoreboard players set @s idLine 11
execute if score #random temp matches 15..29 run scoreboard players set @s idLine 81
# 10% de chances pour M5a et M5b (20% total)
execute if score #random temp matches 30..39 run scoreboard players set @s idLine 51
execute if score #random temp matches 40..49 run scoreboard players set @s idLine 55
# 7% de chances pour le M6 et M7 (14% total)
execute if score #random temp matches 50..56 run scoreboard players set @s idLine 61
execute if score #random temp matches 57..63 run scoreboard players set @s idLine 71
# 4% de chances pour le M3
execute if score #random temp matches 64..67 run scoreboard players set @s idLine 31
# 2% de chances pour le M2
execute if score #random temp matches 68..69 run scoreboard players set @s idLine 21
# 0% de chances pour le M4


# 10% de chances pour le RER A
execute if score #random temp matches 70..79 run scoreboard players set @s idLine 2101
# Rien pour le RER B car il n'y a même pas encore de tracé prévu

# Une des lignes de câble (15% total)
execute if score #random temp matches 80..87 run scoreboard players set @s idLine 4011
execute if score #random temp matches 88..94 run scoreboard players set @s idLine 4021

# Une des lignes de métro mineures (5% total)
execute if score #random temp matches 95..96 run scoreboard players set @s idLine 73
execute if score #random temp matches 97..99 run scoreboard players set @s idLine 83
# Cette fonction sert à générer des préférences météorologiques
# Chaque PNJ ayant ses propres goûts en matière de temps

# On initialise les scores entre 30 et 70%
scoreboard players set #random min 30
scoreboard players set #random max 70
function tdh:random
scoreboard players operation @s likesHeat = #random temp
function tdh:random
scoreboard players operation @s likesRain = #random temp

# On a ensuite différentes conditions qui font pencher la balance dans un sens ou dans l'autre :
# (+25 pluie): On n'est ni dans une ville, ni dans un village
execute unless entity @e[type=armor_stand,tag=NomVille,distance=..200] unless entity @e[type=armor_stand,tag=NomVillage,distance=..100] run scoreboard players add @s likesRain 25
# (+5 pluie): On est dans un village
execute if entity @e[type=armor_stand,tag=NomVillage,distance=..100] run scoreboard players add @s likesRain 5
# (-25 pluie): On est en ville
execute if entity @e[type=armor_stand,tag=NomVille,distance=..200] run scoreboard players remove @s likesRain 25
# (-5 pluie par 10 altitude > 80)
# et (-2 chaleur par 10 altitude > 60)
execute store result score @s temp run data get entity @s Pos[1]
scoreboard players operation @s temp2 = @s temp
scoreboard players remove @s temp 80
scoreboard players remove @s temp2 60
scoreboard players set @s temp3 2
scoreboard players operation @s temp /= @s temp3
scoreboard players set @s temp3 5
scoreboard players operation @s temp2 /= @s temp3

scoreboard players operation @s likesRain -= @s temp
scoreboard players operation @s likesHeat -= @s temp2
scoreboard players reset @s temp
scoreboard players reset @s temp2
scoreboard players reset @s temp3
# (-15 à 25 chaleur): On est dans un biome froid
execute if predicate tdh:biome/neige run scoreboard players remove @s likesHeat 25
execute if predicate tdh:biome/neige_plus_haut run scoreboard players remove @s likesHeat 15
# (+15 à 25 chaleur): On est dans un biome chaud
execute if predicate tdh:biome/desert run scoreboard players add @s likesHeat 25
execute if predicate tdh:biome/jungle run scoreboard players add @s likesHeat 15
# (+15 à 25 pluie): On est dans un biome humide
# (pas de check pour marais car c'est inclus dans les forêts)
execute if predicate tdh:biome/foret/humide run scoreboard players add @s likesRain 15
execute if predicate tdh:biome/foret run scoreboard players add @s likesRain 10
# (-50 pluie): On est dans l'océan
execute if predicate tdh:biome/ocean run scoreboard players remove @s likesRain 50
execute if predicate tdh:biome/riviere run scoreboard players remove @s likesRain 25

# On normalise les résultats obtenus
execute if score @s likesRain matches ..-1 run scoreboard players set @s likesRain 0
execute if score @s likesHeat matches ..-1 run scoreboard players set @s likesHeat 0
execute if score @s likesRain matches 101.. run scoreboard players set @s likesRain 100
execute if score @s likesHeat matches 101.. run scoreboard players set @s likesHeat 100
# On exécute cette fonction en tant qu'un PNJ en attente du résultat d'une recherche d'itinéraire,
# à la position du joueur en conversation, si les résultats de la recherche qu'on a demandés sont arrivés

# On récupère les résultats de la recherche
execute store result score #dialogue temp5 run data get storage tch:itineraire dernier_resultat.succes
# Selon les résultats, on exécute la bonne fonction
execute if score #dialogue temp5 matches 0 run function tdh:dialogue/en_attente/itineraire/resultat/echec
execute if score #dialogue temp5 matches 1 run function tdh:dialogue/en_attente/itineraire/resultat/succes
scoreboard players reset #dialogue temp5

# On termine l'attente
function tdh:dialogue/en_attente/itineraire/resultat/fin
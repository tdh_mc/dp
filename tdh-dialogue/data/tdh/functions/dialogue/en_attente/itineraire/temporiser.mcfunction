# Le joueur doit s'impatienter un peu, on lui lâche une phrase pour qu'il ne croie pas que c'est tout cassé

# On réinitialise le temps d'attente
scoreboard players set @s remainingTicks 40

# On génère une phrase aléatoirement
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..9 run data modify storage tdh:dialogue reponse set value '"Ça ne devrait plus prendre longtemps... Enfin, j\'espère."'
execute if score #random temp matches 10..19 run data modify storage tdh:dialogue reponse set value '"Désolé, elle est parfois un peu lente, cette bécane."'
execute if score #random temp matches 20..29 run data modify storage tdh:dialogue reponse set value '"Il faudra vraiment que je dise à ma responsable de faire remplacer cet ordinateur..."'
execute if score #random temp matches 30..39 run data modify storage tdh:dialogue reponse set value '"Bon, normalement, il devrait bientôt vous avoir trouvé ça..."'
execute if score #random temp matches 40..49 run data modify storage tdh:dialogue reponse set value '"Ça, c\'est mon ordi... Il y en a beaucoup comme ça, mais lui c\'est le mien."'
execute if score #random temp matches 50..59 run data modify storage tdh:dialogue reponse set value '"Putain ! L\'attente est énorme, non mais regardez-moi ça ?"'
execute if score #random temp matches 60..69 run data modify storage tdh:dialogue reponse set value '"Voilà, ça c\'est l\'ordi que j\'ai utilisé. Je prends Microsoft Edge le matin, et Firefox le soir."'
execute if score #random temp matches 70..79 run data modify storage tdh:dialogue reponse set value '"Eh bien, j\'espère qu\'il a bientôt fini..."'
execute if score #random temp matches 80..89 run data modify storage tdh:dialogue reponse set value '"Ne vous inquiétez pas, il fait ça souvent. Vous aurez bientôt la réponse."'
execute if score #random temp matches 90..99 run data modify storage tdh:dialogue reponse set value '"C\'est sûr que ça serait plus simple si je connaissais tout par cœur..."'

# On affiche ensuite la phrase, puis on la supprime
function tdh:dialogue/affichage/reponse
data remove storage tdh:dialogue reponse
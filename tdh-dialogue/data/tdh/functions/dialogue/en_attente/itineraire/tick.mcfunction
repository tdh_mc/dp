# On exécute cette fonction en tant qu'un PNJ en attente du résultat d'une recherche d'itinéraire,
# à la position du joueur en conversation

# On décrémente le timer (quand on arrive à 0, on écrit une phrase dans le chat)
scoreboard players remove @s remainingTicks 1

# Si on est arrivés à 0, on écrit une phrase qui dit que ça calcule toujours
execute if score @s remainingTicks matches ..0 run function tdh:dialogue/en_attente/itineraire/temporiser


# On vérifie si le calcul est terminé
# Lorsque ce sera le cas, le statut de #tchItineraire sera à 2 (= calcul terminé) pendant 0.5s pour nous laisser le temps de récupérer le résultat
execute if score #tchItineraire status matches 2 if score #tchItineraire dialogueID = @s dialogueID run function tdh:dialogue/en_attente/itineraire/resultat
# On calcule le temps de trajet dans un format humainement lisible
execute store result score #store currentHour run data get storage tch:itineraire dernier_resultat.itineraire.cumulTemps
function tdh:store/time_delay

# On calcule la distance dans un format humainement lisible
execute store result score #store distance run data get storage tch:itineraire dernier_resultat.itineraire.cumulDistance
# On multiplie la distance par le facteur de temps TDH pour obtenir un kilométrage cohérent vis-à-vis du temps de trajet
scoreboard players set #tchItinDialogue temp 72
scoreboard players operation #tchItinDialogue temp *= #tempsOriginal fullDayTicks
scoreboard players operation #tchItinDialogue temp /= #temps fullDayTicks
scoreboard players operation #store distance *= #tchItinDialogue temp
scoreboard players reset #tchItinDialogue temp
function tdh:store/distance_format

# On affiche le temps total de trajet et la distance totale du trajet
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"En tout, le trajet "},{"storage":"tch:itineraire","nbt":"dernier_resultat.depart.deDu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.depart.nom"},{"text":" "},{"storage":"tch:itineraire","nbt":"dernier_resultat.arrivee.alAu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.arrivee.nom"},{"text":" vous prendra "},{"storage":"tdh:store","nbt":"time","interpret":true},{"text":", pour une distance de "},{"storage":"tdh:store","nbt":"distance","interpret":true},{"text":"."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Au total, vous mettrez "},{"storage":"tdh:store","nbt":"time","interpret":true},{"text":" à parcourir les "},{"storage":"tdh:store","nbt":"distance","interpret":true},{"text":" qui vous séparent "},{"storage":"tch:itineraire","nbt":"dernier_resultat.arrivee.deDu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.arrivee.nom"},{"text":"."}]'

function tdh:dialogue/affichage/reponse
# On enregistre d'abord la phrase
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Malheureusement, l\'itinéraire "},{"storage":"tch:itineraire","nbt":"dernier_resultat.depart.deDu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.depart.nom"},{"text":" "},{"storage":"tch:itineraire","nbt":"dernier_resultat.arrivee.alAu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.arrivee.nom"},{"text":" n\'est pas possible en ce moment sur le réseau TCH. "},{"storage":"tdh:dialogue","nbt":"extra.conseil","interpret":true}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Je suis désolé, mais il est impossible d\'aller "},{"storage":"tch:itineraire","nbt":"dernier_resultat.arrivee.alAu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.arrivee.nom"},{"text":" depuis "},{"storage":"tch:itineraire","nbt":"dernier_resultat.depart.prefixe"},{"storage":"tch:itineraire","nbt":"dernier_resultat.depart.nom"},{"text":" avec TCH pour l\'instant. "},{"storage":"tdh:dialogue","nbt":"extra.conseil","interpret":true}]'

# S'il fait nuit, on conseille d'attendre demain
execute if score #temps timeOfDay matches 3 unless score #temps currentTdhTick matches 4000..24000 run data modify storage tdh:dialogue extra.conseil set value '"Je vous conseille de revenir demain matin, quand le service aura repris !"'
execute if score #temps timeOfDay matches 3 if score #temps currentTdhTick matches 4000..24000 run data modify storage tdh:dialogue extra.conseil set value '"Je vous conseille de revenir en début de matinée, quand davantage de lignes seront ouvertes !"'
execute unless score #temps timeOfDay matches 3 run data modify storage tdh:dialogue extra.conseil set value '""'

# On affiche ensuite la phrase
function tdh:dialogue/affichage/reponse

# On nettoie le storage
data remove storage tdh:dialogue reponse
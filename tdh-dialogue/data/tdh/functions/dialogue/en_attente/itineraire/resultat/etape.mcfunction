# On exécute cette fonction en tant qu'un PNJ en attente du résultat d'une recherche d'itinéraire,
# à la position du joueur en conversation, si les résultats de la recherche qu'on a demandés sont arrivés

# On va générer une des phrases indicatives de l'agent TCH, à partir de l'élément [0] des résultats
# (le tableau d'origine est trié du dernier mode emprunté vers le premier, donc on ajoute nos données avec prepend)

# On se donne des tags selon ce que contient l'étape actuelle
execute if data storage tch:itineraire dernier_resultat.itineraire.chemin[-1].line run tag @s add LigneEmpruntee
execute if data storage tch:itineraire dernier_resultat.itineraire.chemin[-1].sortie run tag @s add SortieEmpruntee
execute as @s[tag=!LigneEmpruntee,tag=!SortieEmpruntee] if data storage tch:itineraire dernier_resultat.itineraire.chemin[-1].station run tag @s add StationEmpruntee

# On enregistre le fait qu'on a traité une étape
scoreboard players add @s temp 1
# Si on emprunte une ligne, on incrémente la variable correspondante
scoreboard players add @s[tag=LigneEmpruntee] temp2 1

# On génère une phrase différente selon le tag qu'on a
execute as @s[tag=LigneEmpruntee] run function tdh:dialogue/en_attente/itineraire/resultat/etape/ligne
execute as @s[tag=StationEmpruntee,scores={temp2=0}] run function tdh:dialogue/en_attente/itineraire/resultat/etape/depart
#execute as @s[tag=StationEmpruntee,scores={temp2=1..}] run function tdh:dialogue/en_attente/itineraire/resultat/etape/arrivee
execute as @s[tag=SortieEmpruntee,scores={temp2=0}] run function tdh:dialogue/en_attente/itineraire/resultat/etape/entree
execute as @s[tag=SortieEmpruntee,scores={temp2=1..}] run function tdh:dialogue/en_attente/itineraire/resultat/etape/sortie
# Sous certaines conditions, on désactive l'affichage de la phrase :
# Station empruntée et temp2 > 1 (signifie qu'on passe par un hub après le départ)
execute as @s[tag=StationEmpruntee,scores={temp2=1..}] run tag @s add NoOutput

# On affiche ensuite la phrase
# (sauf si on a une des conditions désactivées ci-dessus)
execute as @s[tag=!NoOutput] run function tdh:dialogue/affichage/reponse

# On supprime les tags
tag @s remove NoOutput
tag @s remove LigneEmpruntee
tag @s remove SortieEmpruntee
tag @s remove StationEmpruntee

# On supprime le dernier élément du tableau
data remove storage tch:itineraire dernier_resultat.itineraire.chemin[-1]
# S'il reste un élément au tableau, on continue l'affichage
execute if data storage tch:itineraire dernier_resultat.itineraire.chemin[0] run function tdh:dialogue/en_attente/itineraire/resultat/etape
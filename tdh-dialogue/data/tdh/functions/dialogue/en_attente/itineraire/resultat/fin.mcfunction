# On enregistre le fait qu'on n'est plus ni en attente, ni en recherche d'itinéraire
tag @s remove EnAttente
tag @s remove Itineraire

# On donne au joueur le topic correspondant
scoreboard players set @p[distance=0,tag=SpeakingToNPC] topic 1201
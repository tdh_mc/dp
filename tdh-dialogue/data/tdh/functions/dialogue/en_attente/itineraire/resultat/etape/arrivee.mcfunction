# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà ! Vous êtes arrivé "},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].station.details.alAu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].station.details.nom"},{"text":"."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Et cela conclut votre voyage, puisque vous arrivez "},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].station.details.alAu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].station.details.nom"},{"text":" !"}]'
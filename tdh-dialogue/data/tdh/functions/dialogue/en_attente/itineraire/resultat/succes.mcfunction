# On affiche une ligne en début de texte
function tdh:dialogue/affichage/debut_texte

# On initialise des variables pour compter :
# - le nombre d'étapes traitées
scoreboard players set @s temp 0
# - le nombre de lignes empruntées
scoreboard players set @s temp2 0

# On génère puis affiche une phrase (groupe verbal + num ligne / station d'arrivée) pour chaque étape du trajet
# On génèrera les phrases suivantes directement dans etape, qui est récursive tant qu'il y a des éléments à afficher
function tdh:dialogue/en_attente/itineraire/resultat/etape

# Après l'affichage de toutes les étapes, on affiche le temps total de trajet
function tdh:dialogue/en_attente/itineraire/resultat/mesures

# On réinitialise les variables temporaires
scoreboard players reset @s temp
scoreboard players reset @s temp2
# On nettoie le storage
data remove storage tdh:dialogue reponse
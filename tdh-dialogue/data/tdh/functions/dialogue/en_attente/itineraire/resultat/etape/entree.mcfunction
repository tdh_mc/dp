# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Entrez dans la station en emprutant l\'accès n°"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.num"},{"text":", situé vers "},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.prefixe"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.nom"},{"text":"."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"D\'abord, entrez dans la station par l\'accès n°"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.num"},{"text":", qui se situe au niveau "},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.deDu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.nom"},{"text":"."}]'
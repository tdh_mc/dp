# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Vous pouvez alors quitter la station en empruntant la sortie n°"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.num"},{"text":", qui débouche "},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.alAu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.nom"},{"text":"."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Enfin, sortez de la station, en empruntant la sortie n°"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.num"},{"text":". Vous arriverez alors au niveau "},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.deDu"},{"storage":"tch:itineraire","nbt":"dernier_resultat.itineraire.chemin[-1].sortie.details.nom"},{"text":"."}]'
# On exécute cette fonction en tant qu'un PNJ en attente d'une phrase, à la position du joueur en conversation

# Selon le type d'information qu'on attend, on a une sous-fonction spécifique :
execute as @s[tag=Itineraire] run function tdh:dialogue/en_attente/itineraire/tick

# Si on n'a plus aucun des tags qu'on recherche, on termine le mode en attente
execute as @s[tag=!Itineraire] run function tdh:dialogue/en_attente/fin
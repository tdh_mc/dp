# On exécute cette fonction en tant qu'un PNJ en attente d'une phrase, à la position du joueur en conversation

# On empêche le joueur d'activer les variables triggerables en attendant la fin de la réplique du PNJ
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players reset @p[tag=SpeakingToNPC] answer
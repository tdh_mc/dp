# On exécute cette fonction en tant qu'un PNJ en attente d'une phrase, à la position du joueur en conversation,
# après avoir prononcé la dernière phrase (on veut donc retirer le mode en attente et autoriser à nouveau le joueur à parler)

# On retire notre tag
tag @s remove EnAttente

# On nettoie le timer
scoreboard players reset @s remainingTicks

# On autorise le joueur à activer les variables triggerables
scoreboard players enable @p[tag=SpeakingToNPC] topic
#scoreboard players reset @p[tag=SpeakingToNPC] answer
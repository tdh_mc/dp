# Début de conversation générique
# Cette fonction est exécutée par le PNJ lançant le dialogue,
# à la position du joueur participant au dialogue.

# On supprime les offres du PNJ pour éviter d'ouvrir l'interface de commerce
data modify entity @s Offers.Recipes set value []

# On met le joueur en mode dialogue (immobilisé)
tag @p[distance=..1,scores={talkedToVillager=1..}] add SpeakingToNPC
effect give @p[tag=SpeakingToNPC] slowness 999999 10 true
effect give @p[tag=SpeakingToNPC] resistance 999999 10 true
effect give @p[tag=SpeakingToNPC] jump_boost 999999 250 true

# On fait de même pour le PNJ
# On vérifie si le PNJ est déjà en mode NoAI : si c'est le cas, on lui donne le tag NoAI
execute store result score #dialogue temp7 run data get entity @s NoAI
execute if score #dialogue temp7 matches 1 run tag @s add NoAI
scoreboard players reset #dialogue temp7
# On met ensuite le PNJ en mode dialogue
tag @s add SpeakingToPlayer
execute at @s facing entity @p[tag=SpeakingToNPC] feet run tp @s ~ ~ ~ ~ ~
data merge entity @s[tag=!NoAI] {NoAI:1b}

# On définit les scores du joueur :
# Tous les triggers à 0 (position de repos, ne déclenche rien)
scoreboard players set @p[tag=SpeakingToNPC] answer 0
scoreboard players set @p[tag=SpeakingToNPC] topic 0
# Le sneak time à 0 (pour éviter d'insta-quitter le dialogue)
scoreboard players set @p[tag=SpeakingToNPC] sneakTime 0

# On choisit un identifiant de dialogue unique
scoreboard players set #dialogue dialogueID 0
function tdh:dialogue/new_id

# Une fois l'identifiant généré, on le donne au PNJ et au joueur, puis on réinitialise l'ID temporaire
scoreboard players operation @s dialogueID = #dialogue dialogueID
scoreboard players operation @p[scores={talkedToVillager=1..},distance=..1] dialogueID = #dialogue dialogueID
scoreboard players reset #dialogue dialogueID

# On affiche des particules au-dessus du PNJ
execute at @s run particle happy_villager ~ ~2 ~ .3 .2 .3 1 10

# On tourne le joueur dans la direction du dialogue
execute at @p[tag=SpeakingToNPC] facing entity @s feet run tp @p[tag=SpeakingToNPC] ~ ~ ~ ~ ~

# On affiche un titre au joueur
title @p[tag=SpeakingToNPC] times 10 80 10
title @p[tag=SpeakingToNPC] subtitle [{"text":"Pressez ","color":"gold"},{"keybind":"key.chat","color":"red","bold":"true"},{"text":" pour dialoguer ; "},{"keybind":"key.sneak","color":"red","bold":"true"},{"text":" pour quitter."}]
title @p[tag=SpeakingToNPC] title [{"text":"Conversation","color":"red"}]

# On active pour le joueur la variable permettant de choisir un sujet
scoreboard players enable @p[tag=SpeakingToNPC] topic

# On choisit une phrase pour dire bonjour
function tdh:dialogue/greet/joueur

# On exécute les sous-fonctions de greetings correspondantes selon qui est le PNJ
# Pour éviter de faire d'énormes checks avec de multiples tags, on se donne simplement un tag "GreetingsDone" quand on est passé dans une des conditions
# On génère dans chaque sous-fonction une phrase du PNJ
execute as @s[tag=AgentTCH] run function tdh:dialogue/greet/agent_tch
execute as @s[tag=!GreetingsDone,tag=Aubergiste] run function tdh:dialogue/greet/aubergiste
execute as @s[tag=!GreetingsDone,tag=Tavernier] run function tdh:dialogue/greet/tavernier
execute as @s[tag=!GreetingsDone,tag=Buraliste] run function tdh:dialogue/greet/buraliste
execute as @s[tag=!GreetingsDone,tag=Dealer] run function tdh:dialogue/greet/dealer
execute as @s[tag=!GreetingsDone] run function tdh:dialogue/greet/generic

# Une fois tous les tests passés, on se supprime le tag
tag @s remove GreetingsDone

# On affiche ensuite ce qu'on a généré
execute at @p[tag=SpeakingToNPC] run function tdh:dialogue/affichage/debut_conversation
execute at @p[tag=SpeakingToNPC] run function tdh:dialogue/affichage
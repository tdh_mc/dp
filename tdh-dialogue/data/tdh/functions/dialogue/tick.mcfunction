# Le check principal, qui lance la conversation avec les villageois lorsqu'un joueur interagit avec l'un d'eux
execute at @a[gamemode=!spectator,tag=!SpeakingToNPC,scores={talkedToVillager=1..}] rotated as @p positioned ^ ^ ^2.25 as @e[type=villager,sort=nearest,limit=1,distance=..2.25] unless entity @s[tag=SpeakingToPlayer] unless entity @s[tag=NoDialogue] unless data entity @s SleepingX positioned ^ ^ ^-2 run function tdh:dialogue/greet
scoreboard players set @a[scores={talkedToVillager=1..}] talkedToVillager 0

# Pour les joueurs qui sont déjà en cours de conversation, on fait un check détaillé de leur score
execute at @a[tag=SpeakingToNPC] run function tdh:dialogue/tick_detail

# Pour les joueurs en cours de conversation, s'ils sont en train de sneak ou en spectateur, on les sort de la conversation
scoreboard players set @a[tag=SpeakingToNPC,tag=!LockedInDialogue,scores={sneakTime=1..}] topic -1
execute at @a[tag=SpeakingToNPC,gamemode=spectator] run function tdh:dialogue/end

# On ne sort pas de la conversation si c'est une conversation importante (flag "LockedInDialogue"),
# mais on réinitialise tout de même le score
scoreboard players set @a[tag=SpeakingToNPC,tag=LockedInDialogue,scores={sneakTime=1..}] sneakTime 0

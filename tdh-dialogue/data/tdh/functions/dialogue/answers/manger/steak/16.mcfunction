# On enregistre les données dans le storage
function tdh:dialogue/answers/manger/steak
data modify storage tdh:dialogue transaction.objet set value {nombre:"seize ",unite:"steaks"}
data modify storage tdh:dialogue transaction.item.Count set value 16b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 2

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
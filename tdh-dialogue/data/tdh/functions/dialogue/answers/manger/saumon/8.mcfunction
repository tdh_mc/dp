# On enregistre les données dans le storage
function tdh:dialogue/answers/manger/saumon
data modify storage tdh:dialogue transaction.objet set value {nombre:"huit ",unite:"saumons"}
data modify storage tdh:dialogue transaction.item.Count set value 8b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 1

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
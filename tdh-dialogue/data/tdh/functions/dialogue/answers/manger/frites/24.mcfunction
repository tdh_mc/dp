# On enregistre les données dans le storage
function tdh:dialogue/answers/manger/frites
data modify storage tdh:dialogue transaction.objet set value {nombre:"un petit ",unite:"cornet de frites"}
data modify storage tdh:dialogue transaction.item.Count set value 24b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 1

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
# On enregistre les données dans le storage
function tdh:dialogue/answers/manger/ragout_mouton
data modify storage tdh:dialogue transaction.objet set value {nombre:"12 ",unite:"ragoûts de mouton"}
data modify storage tdh:dialogue transaction.item.Count set value 12b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 1

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"Je vais vous prendre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"Vous auriez "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"Je voudrais bien "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" !"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"Ça sera "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", pour moi, s\'il vous plaît."}]'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Bon appétit ! C\'est tout ce qu\'il lui fallait ?"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Voici "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" pour vous ! Bon appétit à vous ! C\'est tout ce qu\'on pouvait faire pour lui ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà ! Profitez-en bien ! Il avait besoin d\'autre chose ?"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Si vous avez besoin de plus de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", vous saurez où me trouver ! Il lui fallait autre chose ?"}]'

# On enregistre une tournure aléatoire pour le refus qui sera utilisée SI on a un refus de transaction
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Je crois que vous n\'avez pas assez de "},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Il va me falloir ces "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":" si vous voulez "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Vous devez sous-estimer le prix de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Vous devez me payer "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":". C\'est non négociable !"}]'


# Si on a répondu qu'on voulait bien acheter, on appelle une sous fonction pour faire différentes choses:
# - Enregistrer le prix (dans #transaction fer/or/diamant/kubs)
# - Enregistrer dans le storage l'item à donner au joueur (tdh:dialogue transaction.objet)
# Pour le pain
execute if score @p[tag=SpeakingToNPC] answer matches 1016 run function tdh:dialogue/answers/manger/pain/16
execute if score @p[tag=SpeakingToNPC] answer matches 1032 run function tdh:dialogue/answers/manger/pain/32

# Pour les patates
execute if score @p[tag=SpeakingToNPC] answer matches 2024 run function tdh:dialogue/answers/manger/frites/24
execute if score @p[tag=SpeakingToNPC] answer matches 2064 run function tdh:dialogue/answers/manger/frites/64

# Pour les pastèques
execute if score @p[tag=SpeakingToNPC] answer matches 2148 run function tdh:dialogue/answers/manger/pasteque/48

# Pour les pommes
execute if score @p[tag=SpeakingToNPC] answer matches 2248 run function tdh:dialogue/answers/manger/pomme/48

# Pour les baies
execute if score @p[tag=SpeakingToNPC] answer matches 2348 run function tdh:dialogue/answers/manger/baies/48

# Si on n'a pas pu récupérer de valeur, on dit qu'on ne veut rien finalement
execute as @s[tag=!Transaction] run function tdh:dialogue/answers/manger/no

# Sinon, on affiche les options
execute as @s[tag=Transaction] run function tdh:dialogue/answers/manger/autre_chose
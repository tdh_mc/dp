# On enregistre les données dans le storage
function tdh:dialogue/answers/manger/travers_porc
data modify storage tdh:dialogue transaction.objet set value {nombre:"32 ",unite:"travers de porc"}
data modify storage tdh:dialogue transaction.item.Count set value 32b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 3

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
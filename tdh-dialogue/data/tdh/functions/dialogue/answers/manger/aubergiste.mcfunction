# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"Je vais vous prendre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"Vous auriez "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"Je voudrais bien "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" !"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"Ça sera "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", pour moi, s\'il vous plaît."}]'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Bon appétit ! C\'est tout ce que vous désiriez ?"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Voici "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" pour vous ! Bon appétit à vous ! C\'est tout ce que je pouvais faire pour vous ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà ! Profitez-en bien ! Vous aviez besoin d\'autre chose ?"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Si vous avez besoin de plus de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", vous saurez où me trouver ! Il vous fallait autre chose ?"}]'

# On enregistre une tournure aléatoire pour le refus qui sera utilisée SI on a un refus de transaction
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Je crois que vous n\'avez pas assez de "},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Il va me falloir ces "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":" si vous voulez "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Vous devez sous-estimer le prix de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Vous devez me payer "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":". C\'est non négociable !"}]'


# Si on a répondu qu'on voulait bien acheter, on appelle une sous fonction pour faire différentes choses:
# - Enregistrer le prix (dans #transaction fer/or/diamant/kubs)
# - Enregistrer dans le storage l'item à donner au joueur (tdh:dialogue transaction.objet)
# Pour le pain
execute if score @p[tag=SpeakingToNPC] answer matches 1016 run function tdh:dialogue/answers/manger/pain/16
execute if score @p[tag=SpeakingToNPC] answer matches 1032 run function tdh:dialogue/answers/manger/pain/32
execute if score @p[tag=SpeakingToNPC] answer matches 1064 run function tdh:dialogue/answers/manger/pain/64

# Pour les sandwichs
execute if score @p[tag=SpeakingToNPC] answer matches 1112 run function tdh:dialogue/answers/manger/sandwich_poulet/12
execute if score @p[tag=SpeakingToNPC] answer matches 1124 run function tdh:dialogue/answers/manger/sandwich_poulet/24
execute if score @p[tag=SpeakingToNPC] answer matches 1148 run function tdh:dialogue/answers/manger/sandwich_poulet/48

execute if score @p[tag=SpeakingToNPC] answer matches 1212 run function tdh:dialogue/answers/manger/sandwich_lapin/12
execute if score @p[tag=SpeakingToNPC] answer matches 1224 run function tdh:dialogue/answers/manger/sandwich_lapin/24
execute if score @p[tag=SpeakingToNPC] answer matches 1248 run function tdh:dialogue/answers/manger/sandwich_lapin/48

# Pour le cabillaud
execute if score @p[tag=SpeakingToNPC] answer matches 1312 run function tdh:dialogue/answers/manger/cabillaud/12
execute if score @p[tag=SpeakingToNPC] answer matches 1324 run function tdh:dialogue/answers/manger/cabillaud/24
execute if score @p[tag=SpeakingToNPC] answer matches 1348 run function tdh:dialogue/answers/manger/cabillaud/48

# Pour le saumon
execute if score @p[tag=SpeakingToNPC] answer matches 1408 run function tdh:dialogue/answers/manger/saumon/8
execute if score @p[tag=SpeakingToNPC] answer matches 1416 run function tdh:dialogue/answers/manger/saumon/16
execute if score @p[tag=SpeakingToNPC] answer matches 1432 run function tdh:dialogue/answers/manger/saumon/32
execute if score @p[tag=SpeakingToNPC] answer matches 1464 run function tdh:dialogue/answers/manger/saumon/64

# Pour la charcuterie
execute if score @p[tag=SpeakingToNPC] answer matches 1508 run function tdh:dialogue/answers/manger/travers_porc/8
execute if score @p[tag=SpeakingToNPC] answer matches 1516 run function tdh:dialogue/answers/manger/travers_porc/16
execute if score @p[tag=SpeakingToNPC] answer matches 1532 run function tdh:dialogue/answers/manger/travers_porc/32
execute if score @p[tag=SpeakingToNPC] answer matches 1564 run function tdh:dialogue/answers/manger/travers_porc/64

execute if score @p[tag=SpeakingToNPC] answer matches 1608 run function tdh:dialogue/answers/manger/boudin/8
execute if score @p[tag=SpeakingToNPC] answer matches 1616 run function tdh:dialogue/answers/manger/boudin/16
execute if score @p[tag=SpeakingToNPC] answer matches 1632 run function tdh:dialogue/answers/manger/boudin/32
execute if score @p[tag=SpeakingToNPC] answer matches 1664 run function tdh:dialogue/answers/manger/boudin/64

execute if score @p[tag=SpeakingToNPC] answer matches 1708 run function tdh:dialogue/answers/manger/jambon/8
execute if score @p[tag=SpeakingToNPC] answer matches 1716 run function tdh:dialogue/answers/manger/jambon/16
execute if score @p[tag=SpeakingToNPC] answer matches 1732 run function tdh:dialogue/answers/manger/jambon/32
execute if score @p[tag=SpeakingToNPC] answer matches 1764 run function tdh:dialogue/answers/manger/jambon/64

# Pour le boeuf
execute if score @p[tag=SpeakingToNPC] answer matches 1808 run function tdh:dialogue/answers/manger/steak/8
execute if score @p[tag=SpeakingToNPC] answer matches 1816 run function tdh:dialogue/answers/manger/steak/16
execute if score @p[tag=SpeakingToNPC] answer matches 1832 run function tdh:dialogue/answers/manger/steak/32
execute if score @p[tag=SpeakingToNPC] answer matches 1864 run function tdh:dialogue/answers/manger/steak/64

# Pour le mouton
execute if score @p[tag=SpeakingToNPC] answer matches 1912 run function tdh:dialogue/answers/manger/ragout_mouton/12
execute if score @p[tag=SpeakingToNPC] answer matches 1924 run function tdh:dialogue/answers/manger/ragout_mouton/24
execute if score @p[tag=SpeakingToNPC] answer matches 1948 run function tdh:dialogue/answers/manger/ragout_mouton/48

# Pour les patates
execute if score @p[tag=SpeakingToNPC] answer matches 2024 run function tdh:dialogue/answers/manger/frites/24
execute if score @p[tag=SpeakingToNPC] answer matches 2064 run function tdh:dialogue/answers/manger/frites/64

# Si on n'a pas pu récupérer de valeur, on dit qu'on ne veut rien finalement
execute as @s[tag=!Transaction] run function tdh:dialogue/answers/manger/no

# Sinon, on affiche les options
execute as @s[tag=Transaction] run function tdh:dialogue/answers/manger/autre_chose
# On enregistre les données dans le storage
function tdh:dialogue/answers/manger/cabillaud
data modify storage tdh:dialogue transaction.objet set value {nombre:"douze ",unite:"cabillauds"}
data modify storage tdh:dialogue transaction.item.Count set value 12b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 1

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
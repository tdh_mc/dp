# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"C\'est hors de mes moyens, là..."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Je comprends mieux pourquoi personne ne vous en prend..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"J\'ai déjà vu des prix gonflés, mais là, c\'est un record !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Je reviendrai vous prendre deux ou trois cigarettes quand je gagnerai à DuerroMillion, hein..."'

# On enregistre une tournure aléatoire pour la réponse du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Ça n\'est pas mon problème, hein ! Si vous trouvez moins cher ailleurs, n\'hésitez pas à leur rendre visite..."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"C\'est une taverne, ici ! Pintez-vous donc la gueule comme tout le monde au lieu de nous enfumer !"'

# Fin de conversation
tag @s add AuRevoir
# On enregistre les données dans le storage
function tdh:dialogue/answers/ressource/tabac/cigarette
data modify storage tdh:dialogue transaction.objet set value {nombre:"deux ",unite:"cigarettes"}
data modify storage tdh:dialogue transaction.item.Count set value 2b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 4

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
# On enregistre les données dans le storage
function tdh:dialogue/answers/ressource/tabac/cigarette
data modify storage tdh:dialogue transaction.objet set value {nombre:"vingt ",unite:"cigarettes"}
data modify storage tdh:dialogue transaction.item.Count set value 20b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 10

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
# On enregistre les données dans le storage
function tdh:dialogue/answers/ressource/tabac/tabac
data modify storage tdh:dialogue transaction.objet set value {nombre:"dix doses de ",unite:"tabac à pipe"}
data modify storage tdh:dialogue transaction.item.Count set value 10b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 5

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
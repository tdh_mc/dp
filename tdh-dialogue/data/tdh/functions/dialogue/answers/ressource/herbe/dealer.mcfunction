# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"Allez, mets-moi "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"On dit "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", c\'est bon ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"Il me faudrait "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" !"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"Ça sera "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", si possible."}]'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Tiens, "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Il te fallait autre chose ?"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Pas de souci, c\'est parti pour "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":". Ça sera tout ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà, éclate-toi bien... Il te fallait quelque chose d\'autre ?"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"N\'hésite pas à revenir, j\'ai toujours de nouveaux arrivages de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"... Pour aujourd\'hui, c\'est tout bon ?"}]'

# On enregistre une tournure aléatoire pour le refus qui sera utilisée SI on a un refus de transaction
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Tu me files les "},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":" d\'abord, hein, pas l\'inverse."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Donne d\'abord "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":", et on verra pour tes "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'


# Si on a répondu qu'on voulait bien acheter, on appelle une sous fonction pour faire différentes choses:
# - Enregistrer le prix (dans #transaction fer/or/diamant/kubs)
# - Enregistrer dans le storage l'item à donner au joueur (tdh:dialogue transaction.objet)
# Pour les joints
execute if score @p[tag=SpeakingToNPC] answer matches 105 run function tdh:dialogue/answers/ressource/herbe/dealer/joint/5
execute if score @p[tag=SpeakingToNPC] answer matches 110 run function tdh:dialogue/answers/ressource/herbe/dealer/joint/10
execute if score @p[tag=SpeakingToNPC] answer matches 125 run function tdh:dialogue/answers/ressource/herbe/dealer/joint/25

# Pour l'herbe
execute if score @p[tag=SpeakingToNPC] answer matches 205 run function tdh:dialogue/answers/ressource/herbe/dealer/herbe/5
execute if score @p[tag=SpeakingToNPC] answer matches 210 run function tdh:dialogue/answers/ressource/herbe/dealer/herbe/10
execute if score @p[tag=SpeakingToNPC] answer matches 225 run function tdh:dialogue/answers/ressource/herbe/dealer/herbe/25
execute if score @p[tag=SpeakingToNPC] answer matches 250 run function tdh:dialogue/answers/ressource/herbe/dealer/herbe/50


# Si on n'a pas pu récupérer de valeur, on dit qu'on ne veut rien finalement
execute as @s[tag=!Transaction] run function tdh:dialogue/answers/ressource/herbe/dealer/no

# Sinon, on affiche les options
execute as @s[tag=Transaction] run function tdh:dialogue/answers/ressource/herbe/dealer/autre_chose
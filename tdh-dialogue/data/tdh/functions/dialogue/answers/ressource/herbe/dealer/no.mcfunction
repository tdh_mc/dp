# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Aïe, je ne vais pas avoir assez. Je reviendrai."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Je vais manquer d\'argent, désolé..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Oh, je ne m\'attendais pas à ce que ça coûte si cher ! Je reviendrai, là je n\'ai pas assez..."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Ah oui, c\'est carrément pas donné. Il faut que je voie si j\'ai assez de monnaie..."'

# On enregistre une tournure aléatoire pour la réponse du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"C\'est con !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Eh, j\'suis plutôt donné, pourtant, par rapport aux autres !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Reviens quand tu pourras payer, je suis là souvent."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Promène-toi un peu, va voir la concurrence, tu verras que je suis pas si cher que ça !"'

# Fin de conversation
tag @s add AuRevoir
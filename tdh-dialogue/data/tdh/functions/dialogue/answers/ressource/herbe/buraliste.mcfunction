# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"Allez, mettez-moi "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"On peut partir sur "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"Je voudrais bien "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" !"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"On va dire "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", si possible."}]'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Il vous fallait autre chose ?"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Voici "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" pour vous ! Vous aviez besoin d\'autre chose ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà ! Profitez bien ! Vous aviez besoin d\'autre chose ?"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Si vous avez besoin de plus de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", n\'hésitez pas à revenir... mais en restant discret ! Il vous fallait autre chose ?"}]'

# On enregistre une tournure aléatoire pour le refus qui sera utilisée SI on a un refus de transaction
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Désolé, mais ça coûte "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Il va me falloir ces "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":" si vous voulez "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'


# Si on a répondu qu'on voulait bien acheter, on appelle une sous fonction pour faire différentes choses:
# - Enregistrer le prix (dans #transaction fer/or/diamant/kubs)
# - Enregistrer dans le storage l'item à donner au joueur (tdh:dialogue transaction.objet)
# Pour les pétards
execute if score @p[tag=SpeakingToNPC] answer matches 101 run function tdh:dialogue/answers/ressource/herbe/buraliste/joint/1
execute if score @p[tag=SpeakingToNPC] answer matches 105 run function tdh:dialogue/answers/ressource/herbe/buraliste/joint/5
execute if score @p[tag=SpeakingToNPC] answer matches 110 run function tdh:dialogue/answers/ressource/herbe/buraliste/joint/10

# Pour l'herbe
execute if score @p[tag=SpeakingToNPC] answer matches 201 run function tdh:dialogue/answers/ressource/herbe/buraliste/herbe/1
execute if score @p[tag=SpeakingToNPC] answer matches 205 run function tdh:dialogue/answers/ressource/herbe/buraliste/herbe/5
execute if score @p[tag=SpeakingToNPC] answer matches 210 run function tdh:dialogue/answers/ressource/herbe/buraliste/herbe/10

# Pour le briquet
execute if score @p[tag=SpeakingToNPC] answer matches 501 run function tdh:dialogue/answers/ressource/herbe/buraliste/briquet/1

# Pour la pipe
execute if score @p[tag=SpeakingToNPC] answer matches 601 run function tdh:dialogue/answers/ressource/herbe/buraliste/pipe/1


# Si on n'a pas pu récupérer de valeur, on dit qu'on ne veut rien finalement
execute as @s[tag=!Transaction] run function tdh:dialogue/answers/ressource/herbe/buraliste/no

# Sinon, on affiche les options
execute as @s[tag=Transaction] run function tdh:dialogue/answers/ressource/herbe/buraliste/autre_chose
# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Finalement, je manque de monnaie."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Oh, je crois que je n\'ai pas les moyens, en fait..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"En fait, je n\'ai pas d\'argent."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Désolé, j\'ai changé d\'avis."'

# On enregistre une tournure aléatoire pour la réponse du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Eh bien, c\'est vous qui voyez !"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Revenez quand vous en aurez !"'

# Fin de conversation
tag @s add AuRevoir
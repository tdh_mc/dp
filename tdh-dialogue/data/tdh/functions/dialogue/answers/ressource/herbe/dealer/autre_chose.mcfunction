# On enregistre le topic chez le PNJ ("autre sujet", topic 2)
scoreboard players set @s topic 2

# On active le mode réponse chez le joueur
# Mais on ne désactive PAS la possibilité de proposer un sujet
#scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les choix de réponse
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"Il m\'en faudrait un peu plus...","clickEvent":{"action":"run_command","value":"/trigger answer set 2220"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander davantage d\'herbe."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je voulais aussi parler d\'autre chose...","clickEvent":{"action":"run_command","value":"/trigger answer set -2"},"hoverEvent":{"action":"show_text","value":[{"text":"Aborder un autre sujet."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Ça sera tout.","clickEvent":{"action":"run_command","value":"/trigger answer set -1"},"hoverEvent":{"action":"show_text","value":[{"text":"Dire au revoir."}]}}]'
# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"C\'est hors de mes moyens, là..."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Je vais manquer d\'argent, désolé..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Ah oui, c\'est un peu cher ! Il va falloir que j\'y réfléchisse..."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Ah oui, c\'est carrément hors de prix. Je vais voir si je peux retirer suffisamment..."'

# On enregistre une tournure aléatoire pour la réponse du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Ça n\'est pas mon problème, hein ! Si vous trouvez moins cher ailleurs, n\'hésitez pas à leur rendre visite..."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Revenez quand vous en aurez !"'

# Fin de conversation
tag @s add AuRevoir
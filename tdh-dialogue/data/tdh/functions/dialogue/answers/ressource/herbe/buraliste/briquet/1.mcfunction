# On enregistre les données dans le storage
function tdh:dialogue/answers/ressource/herbe/briquet
data modify storage tdh:dialogue transaction.objet set value {nombre:"un ",unite:"briquet"}
data modify storage tdh:dialogue transaction.item.Count set value 1b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 2

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
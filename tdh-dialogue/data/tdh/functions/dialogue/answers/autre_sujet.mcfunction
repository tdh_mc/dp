# Si on a un answer positif (= pas un truc générique style Au revoir / Liste des sujets),
# On convertit notre answer en topic
scoreboard players operation @p[tag=SpeakingToNPC] topic = @p[tag=SpeakingToNPC] answer
scoreboard players reset @p[tag=SpeakingToNPC] answer
scoreboard players set @p[tag=SpeakingToNPC] answer 0
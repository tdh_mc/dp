# Selon notre réponse, on lance la sous-fonction correspondante
# Si on essaie de persuader le tavernier de nous filer une torche
execute if score @p[tag=SpeakingToNPC] answer matches 10 run function tdh:dialogue/answers/torche/tavernier/amadouer
execute if score @p[tag=SpeakingToNPC] answer matches 20 run function tdh:dialogue/answers/torche/tavernier/convaincre
execute if score @p[tag=SpeakingToNPC] answer matches 30 run function tdh:dialogue/answers/torche/tavernier/menacer

# Si on accepte de payer 1 fer pour avoir notre torche
execute if score @p[tag=SpeakingToNPC] answer matches 100 run function tdh:dialogue/answers/torche/tavernier/payer

# Si on accepte de prendre un verre à la place
execute if score @p[tag=SpeakingToNPC] answer matches 381 run function tdh:dialogue/answers/torche/tavernier/prendre_verre

# Si on engage la conversation sur l'aventure en se vantant
execute if score @p[tag=SpeakingToNPC] answer matches 1000 run function tdh:dialogue/answers/torche/tavernier/bavarder/vantard
# Si on engage la conversation sur l'aventure modestement
execute if score @p[tag=SpeakingToNPC] answer matches 1050 run function tdh:dialogue/answers/torche/tavernier/bavarder/modeste
# Si on insiste simplement pour avoir notre torche
execute if score @p[tag=SpeakingToNPC] answer matches 1100 run function tdh:dialogue/answers/torche/tavernier/bavarder/froid

# Sinon, fin de conversation
execute if score @p[tag=SpeakingToNPC] answer matches ..0 run function tdh:dialogue/answers/torche/tavernier/no
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase ridicule dans 75% des cas, une passable dans 25% des cas
execute if score #random temp2 matches ..74 run function tdh:dialogue/answers/torche/aubergiste/menacer/question/ridicule
execute if score #random temp2 matches 75.. run function tdh:dialogue/answers/torche/aubergiste/menacer/question/passable

# On a une petite chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une phrase passable
execute if score #random temp2 matches 85..87 run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes_critique
# On a un succès normal sinon si la phrase est passable
execute unless score #random temp2 matches 85..87 if score #random temp2 matches 54.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes
# Sinon, si on est ridicule, c'est l'échec
execute unless score #random temp2 matches 54.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 20
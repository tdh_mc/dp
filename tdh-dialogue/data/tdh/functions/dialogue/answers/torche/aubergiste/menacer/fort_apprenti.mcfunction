# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase ridicule dans 25% des cas, une passable dans 35% des cas, une bonne le reste du temps
execute if score #random temp2 matches ..24 run function tdh:dialogue/answers/torche/aubergiste/menacer/question/ridicule
execute if score #random temp2 matches 25..59 run function tdh:dialogue/answers/torche/aubergiste/menacer/question/passable
execute if score #random temp2 matches 60.. run function tdh:dialogue/answers/torche/aubergiste/menacer/question/bonne

# On a une grande chance d'avoir un succès critique
execute if score #random temp2 matches 24..97 run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes_critique
# On a un succès normal sinon si la phrase est passable
execute unless score #random temp2 matches 24..97 if score #random temp2 matches 10.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes
# Sinon, si on est ridicule, c'est l'échec
execute unless score #random temp2 matches 10.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 22
# On génère une manière passable de menacer l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '[{"text":"Vous ne savez pas à qui vous tenez tête, "},{"selector":"@s"},{"text":". Et si vous le saviez, laissez-moi vous dire que vous tiendriez un autre discours... Il est toujours temps de vous rattraper."}]'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"J\'ai vraiment l\'air de me laisser marcher sur les pieds ? Vous avez dix-sept secondes pour aller me chercher une torche avant que je ne me fâche réellement. Un... deux..."'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Il va falloir que je raconte à tous mes amis ce jour où j\'étais dans le besoin et où PERSONNE ici ne m\'a donné de torche. Je suis sûr qu\'ils seront ravis de venir vous jeter des légumes gâtés."'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Vous avez deux options : soit vous me donnez cette fichue torche, soit je m\'éclaire avec le feu que je mettrai dans votre établissement. Alors ?"'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Bâtard de tes morts ! Tu n\'as pas la moindre légitimité à me donner des conseils. Reste à ta place d\'aubergiste, et en échange je ne t\'arracherai pas les côtes."'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 20
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase banale dans 40% des cas, une bonne dans 60% des cas
execute if score #random temp2 matches ..39 run function tdh:dialogue/answers/torche/aubergiste/convaincre/question/basique
execute if score #random temp2 matches 40.. run function tdh:dialogue/answers/torche/aubergiste/convaincre/question/bonne

# On a une petite chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une bonne phrase
execute if score #random temp2 matches 89..95 run function tdh:dialogue/answers/torche/aubergiste/convaincre/reponse/succes_critique
# On a un succès normal sinon
execute unless score #random temp2 matches 89..95 if score #random temp2 matches 1.. run function tdh:dialogue/answers/torche/aubergiste/convaincre/reponse/succes
# Rarement, c'est l'échec
execute unless score #random temp2 matches 1.. run function tdh:dialogue/answers/torche/aubergiste/convaincre/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 5
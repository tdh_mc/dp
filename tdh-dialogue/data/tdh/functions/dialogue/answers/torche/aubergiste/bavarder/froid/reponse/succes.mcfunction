# On génère une réponse neutre de l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Ne le prenez pas mal... On voit beaucoup d\'aventuriers, ces temps-ci. Vous avez un lingot de fer, pour votre torche ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Comme vous voudrez... Ça vous fera un lingot de fer, pour la torche."'

# On enregistre le succès
tag @s add Succes
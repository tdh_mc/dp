# Selon la valeur d'éloquence du joueur, on va générer une bonne ou une mauvaise phrase
# On stocke la valeur d'éloquence
scoreboard players operation #dialogue eloquence = @p[tag=SpeakingToNPC] eloquence
# On initialise le bonus d'éloquence qu'on aura éventuellement
scoreboard players set #joueur eloquenceXp 0

# Si on a moins de 20 d'éloquence, on sortira une mauvaise phrase à chaque fois
execute if score #dialogue eloquence matches ..19 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/debutant
# Si on a 20 à 40 d'éloquence, on se vantera correctement
execute if score #dialogue eloquence matches 20..39 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/apprenti
# Si on a 40 à 75 d'éloquence, on se vantera normalement
execute if score #dialogue eloquence matches 36..50 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/adepte
# Si on a 75 à 100 d'éloquence, on se vantera très bien
execute if score #dialogue eloquence matches 51..75 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/expert
# Si on a 100+ d'éloquence, on se vantera merveilleusement à tous les coups
execute if score #dialogue eloquence matches 101.. run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/maitre

# On génère la phrase du PNJ
execute as @s[tag=Succes,tag=SuccesCritique] run function tdh:dialogue/answers/torche/aubergiste/succes_critique
execute as @s[tag=Succes,tag=!SuccesCritique] run function tdh:dialogue/answers/torche/aubergiste/succes

# On enregistre les éventuels points d'éloquence ajoutés par l'interaction
function tdh:player/xp/eloquence/gain_xp

# On supprime les tags temporaires
scoreboard players reset #dialogue eloquence
tag @s remove Succes
tag @s remove SuccesCritique
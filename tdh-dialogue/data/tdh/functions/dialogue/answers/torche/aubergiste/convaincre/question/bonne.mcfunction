# On génère une bonne manière de convaincre l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '"Écoutez... J\'ai de l\'argent, et vous avez très probablement une torche à me fournir. Tout ce que je vous demande, c\'est de me donner un prix : je suis enclin à vous payer, et mes affaires ne peuvent pas se permettre d\'attendre."'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"Ce n\'est pas possible... Soit je vous paie cette torche, soit je m\'en vais en acheter une ailleurs et vous n\'aurez pas mon argent. Ce n\'est tout de même pas ça qui va vous mettre en faillite ?"'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '[{"text":"Mais... Laissez-moi donc vous payer une torche plutôt que de vous inquiéter pour moi, "},{"selector":"@s"},{"text":" ! Je ne demande que ça !"}]'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Votre sollicitude est appréciable, mais j\'ai grand besoin de vous acheter cette torche et de ne pas m\'éterniser ici. Vous n\'allez quand même pas m\'obliger à acheter chez vos voisins..."'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Je ne vais quand même pas attendre qu\'un marchand ambulant débarque sur mon chemin pour lui acheter une torche ? Vous avez les matériaux, j\'ai de l\'argent ; acceptez donc que l\'on s\'entende..."'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 15
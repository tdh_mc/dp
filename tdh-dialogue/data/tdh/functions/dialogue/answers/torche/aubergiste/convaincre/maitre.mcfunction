# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase banale dans 20% des cas, une bonne dans 80% des cas
execute if score #random temp2 matches ..19 run function tdh:dialogue/answers/torche/aubergiste/convaincre/question/basique
execute if score #random temp2 matches 20.. run function tdh:dialogue/answers/torche/aubergiste/convaincre/question/bonne

# On a une petite chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une bonne phrase
execute if score #random temp2 matches 85..95 run function tdh:dialogue/answers/torche/aubergiste/convaincre/reponse/succes_critique
# On a un succès normal sinon
execute unless score #random temp2 matches 85..95 run function tdh:dialogue/answers/torche/aubergiste/convaincre/reponse/succes
# On génère une réponse neutre de l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Eh bien, allez donc vous faire voir, tiens ! Je vous fais la conversation, et vous m\'envoyez paître ? Mes clients habituels n\'ont pas été élevés chez les porcs !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Puisque c\'est comme ça, vous allez vous la mettre où je pense, votre torche ! Allez donc manquer de respect quelqu\'un d\'autre !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Et aimable, par-dessus le marché... Va la trouver ailleurs, ta torche de merde, clampin !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Casse-toi, casse-toi enfoiré. Tu m\'as assez cassé les pieds."'
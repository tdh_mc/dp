# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase moyenne dans 20% des cas, et une bonne sinon
execute if score #random temp2 matches ..19 run function tdh:dialogue/answers/torche/aubergiste/amadouer/question/moyenne
execute if score #random temp2 matches 20.. run function tdh:dialogue/answers/torche/aubergiste/amadouer/question/bonne

# On a une grande chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une bonne phrase et/ou une moyenne (rarement)
execute if score #random temp2 matches 15..98 run function tdh:dialogue/answers/torche/aubergiste/amadouer/reponse/succes_critique
# On a un succès normal lorsqu'on a fait une moyenne phrase
execute unless score #random temp2 matches 15..98 run function tdh:dialogue/answers/torche/aubergiste/amadouer/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 20
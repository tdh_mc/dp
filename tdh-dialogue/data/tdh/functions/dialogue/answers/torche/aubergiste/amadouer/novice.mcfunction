# On sortira une mauvaise phrase à tous les coups et on aura une réponse défavorable PRESQUE à tous les coups
function tdh:dialogue/answers/torche/aubergiste/amadouer/question/mauvaise

# 1 chance sur 10 d'avoir une réponse favorable (EN PAYANT!)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp
execute if score #random temp2 matches 11.. run function tdh:dialogue/answers/torche/aubergiste/amadouer/reponse/echec
execute if score #random temp2 matches 0..10 run function tdh:dialogue/answers/torche/aubergiste/amadouer/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 30
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase ridicule dans 40% des cas, une passable dans 40% des cas, et une bonne dans 20%
execute if score #random temp2 matches ..39 run function tdh:dialogue/answers/torche/aubergiste/menacer/question/ridicule
execute if score #random temp2 matches 40..79 run function tdh:dialogue/answers/torche/aubergiste/menacer/question/passable
execute if score #random temp2 matches 80.. run function tdh:dialogue/answers/torche/aubergiste/menacer/question/bonne

# On a une petite chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une phrase passable ou bonne
execute if score #random temp2 matches 76..97 run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes_critique
# On a un succès normal sinon si la phrase est passable
execute unless score #random temp2 matches 76..97 if score #random temp2 matches 34.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes
# Sinon, si on est ridicule, c'est l'échec
execute unless score #random temp2 matches 34.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 17
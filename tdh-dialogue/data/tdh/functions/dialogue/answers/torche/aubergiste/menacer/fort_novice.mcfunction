# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase ridicule dans 50% des cas, une passable dans 50% des cas
execute if score #random temp2 matches ..49 run function tdh:dialogue/answers/torche/aubergiste/menacer/question/ridicule
execute if score #random temp2 matches 50.. run function tdh:dialogue/answers/torche/aubergiste/menacer/question/passable

# On a une bonne chance d'avoir un succès critique (torche gratuite)
execute if score #random temp2 matches 30..90 run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes_critique
# On a un succès normal sinon si la phrase est passable
execute unless score #random temp2 matches 30..90 if score #random temp2 matches 15.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes
# Sinon, si on est ridicule, c'est l'échec
execute unless score #random temp2 matches 15.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 28
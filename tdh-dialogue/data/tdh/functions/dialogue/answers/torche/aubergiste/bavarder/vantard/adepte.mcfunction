# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase mauvaise dans 30% des cas, une moyenne dans 50% des cas, et une bonne sinon
execute if score #random temp2 matches ..29 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/question/mauvaise
execute if score #random temp2 matches 30..79 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/question/moyenne
execute if score #random temp2 matches 80.. run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/question/bonne

# On a une petite chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une bonne phrase
execute if score #random temp2 matches 79..94 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/reponse/succes_critique
# On a un succès normal sinon
execute unless score #random temp2 matches 79..94 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 25
# On sortira une phrase désagréable à tous les coups
function tdh:dialogue/answers/torche/aubergiste/bavarder/froid/question

# 9 chances sur 10 d'avoir une réponse favorable (EN PAYANT!)
# 1 chance sur 10 qu'on nous dise d'aller nous faire foutre
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp
execute if score #random temp2 matches 10 run function tdh:dialogue/answers/torche/aubergiste/bavarder/froid/reponse/echec
execute if score #random temp2 matches ..9 run function tdh:dialogue/answers/torche/aubergiste/bavarder/froid/reponse/succes

# On réinitialise les variables temporaires
scoreboard players reset #random temp2
scoreboard players reset #dialogue temp3
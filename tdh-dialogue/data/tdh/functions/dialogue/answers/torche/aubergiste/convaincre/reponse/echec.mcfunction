# On génère une réponse négative de l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Ce n\'est pas d\'argent donc j\'ai besoin. C\'est d\'un peu de compagnie... Si vous partez maintenant, je vais me faire un sang d\'encre."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Ne croyez pas que je ne vive que pour l\'argent ! Je ne suis pas vendeur de torches ; trouvez-en un et vous lui verserez tous les tombereaux d\'or que vous voudrez !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je suis au regret de refuser. Si je vous donne des pierres précieuses contre quinze charrettes de fange, vous irez aussi nettoyer mes latrines ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Ce n\'est pas gentil de me parler comme si j\'étais un dragon assoiffé d\'argent ! Si j\'étais si avare que vous le sous-entendez, cette auberge serait bien moins accueillante... et probablement moins éclairée !"'

# On gagne plus de points d'éloquence si on foire
scoreboard players add #joueur eloquenceXp 22
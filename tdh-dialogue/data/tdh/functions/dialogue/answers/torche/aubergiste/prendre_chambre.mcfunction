# On fait comme si on venait de demander une chambre
function tdh:dialogue/topics/chambre

# On remplace la phrase qu'on prononce par qqch de plus contextuel
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous avez sans doute raison... Combien ça me coûterait ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Je crois que vous avez raison... Ça me coûterait combien, exactement ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Vous devez avoir raison. Vous me la faites à combien, la chambre ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Vous n\'avez pas tort, il faut dire... C\'est combien, pour la nuit ?"'
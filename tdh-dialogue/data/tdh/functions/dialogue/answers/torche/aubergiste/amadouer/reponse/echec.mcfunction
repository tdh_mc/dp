# On génère une réponse négative de l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Faites-moi confiance, prenez plutôt une chambre, ça vous remettra les idées en place."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Eh bien, je n\'ai pas de torche à vous prêter. Désolé. Vous croyez peut-être que ça pousse sur les arbres ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Ne le prenez pas mal, mais ça ne va pas être possible. Je suis aubergiste, pas torcheron."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Vous confondez mon auberge avec un magasin général... Mon fonds de commerce, ça n\'est pas les torches... Imaginez un peu si tout le monde m\'embarquait une torche en partant d\'ici, « Bonjour monsieur "},{"selector":"@s"},{"text":", je vous pique une torche hein, allez bonne soirée ! », ça ne serait pas vivable."}]'

# On gagne plus de points d'éloquence si on foire
scoreboard players add #joueur eloquenceXp 35
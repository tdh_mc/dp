# On génère une réponse positive de l'aubergiste (pour VENDRE une torche)
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Et une torche, une ! Ça vous fera un lingot de fer, s\'il vous plaît."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Eh bien, je ne vais quand même pas refuser... Pour un lingot de fer, je descends vous en chercher une, de torche."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Vous avez raison ; je vais vous chercher votre torche. Un lingot de fer, ça vous ira, comme prix ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je vais vous en chercher une. Un lingot de fer, et elle est à vous !"'

# On enregistre le succès
tag @s add Succes
# On génère une réponse positive de l'aubergiste (pour DONNER une torche)
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"De grâce, n\'en faites rien. Voici votre torche, monseigneur. Je m\'en voudrais d\'avoir eu l\'air de vous manquer de respect."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"N\'en dites pas plus ! Je vous l\'offre gracieusement, votre torche. S\'il vous plaît, restons-en là..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Pitié, ne cassez rien. Voici votre torche. Je ne vous fais rien payer. Bon voyage !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Désolé, désolé, je ne sais plus ce que je dis. Pour vous, bien sûr que j\'ai une torche à ven... à donner ! À vous donner ! Je vais la chercher tout de suite."'

# On gagne quelques points d'éloquence pour un succès critique
scoreboard players add #joueur eloquenceXp 21

# On enregistre le succès
tag @s add Succes
tag @s add SuccesCritique
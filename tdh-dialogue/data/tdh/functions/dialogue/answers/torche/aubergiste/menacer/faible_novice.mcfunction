# On sera ridicule à tous les coups et ça échouera souvent
function tdh:dialogue/answers/torche/aubergiste/menacer/question/ridicule

# 2 chance sur 10 d'avoir une réponse favorable (EN PAYANT!)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp
execute if score #random temp2 matches 2.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/echec
execute if score #random temp2 matches ..1 run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 15
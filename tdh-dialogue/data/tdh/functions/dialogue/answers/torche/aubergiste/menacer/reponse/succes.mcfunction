# On génère une réponse positive de l'aubergiste (pour VENDRE une torche)
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Vous ne me faites pas peur, mais vous me faites un peu pitié. Je vais vous en vendre une, de torche ; un lingot de fer et elle est à vous."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Je ne sais pas ce que le monde vous a fait, mon cher, mais il faut vous calmer. Un lingot de fer pour votre torche, ça vous ira ?"'

# On enregistre le succès
tag @s add Succes
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une mauvaise phrase dans 10% des cas, une moyenne dans 50% des cas, et une bonne sinon
execute if score #random temp2 matches ..9 run function tdh:dialogue/answers/torche/aubergiste/amadouer/question/mauvaise
execute if score #random temp2 matches 10..59 run function tdh:dialogue/answers/torche/aubergiste/amadouer/question/moyenne
execute if score #random temp2 matches 60.. run function tdh:dialogue/answers/torche/aubergiste/amadouer/question/bonne

# On a une bonne chance d'avoir un succès critique lorsqu'on a fait une bonne phrase et/ou une moyenne (rarement)
execute if score #random temp2 matches 58..95 run function tdh:dialogue/answers/torche/aubergiste/amadouer/reponse/succes_critique
# On a un succès normal lorsqu'on a fait une moyenne phrase et (rarement) une mauvaise
execute unless score #random temp2 matches 58..95 if score #random temp2 matches 8.. run function tdh:dialogue/answers/torche/aubergiste/amadouer/reponse/succes
# Sinon, c'est l'échec
execute unless score #random temp2 matches 8.. run function tdh:dialogue/answers/torche/aubergiste/amadouer/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 40
# On génère une phrase désagréable pour répondre à l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '"Ça vous regarde ?"'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"Si on vous le demande, vous direz que vous en savez rien, d\'accord ?"'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Occupez-vous donc de vos affaires, et donnez-moi cette torche."'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Vous tenez vraiment à le savoir, ou c\'est juste pour rallonger notre charmante interaction ?"'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"C\'est pas vos oignons. Raboulez la torche, qu\'on en finisse."'
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase passable dans 25% des cas, une bonne sinon
execute if score #random temp2 matches ..24 run function tdh:dialogue/answers/torche/aubergiste/menacer/question/passable
execute if score #random temp2 matches 25.. run function tdh:dialogue/answers/torche/aubergiste/menacer/question/bonne

# On a une grande chance d'avoir un succès critique (torche gratuite)
execute if score #random temp2 matches 15.. run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes_critique
# On a un succès normal sinon
execute if score #random temp2 matches ..14 run function tdh:dialogue/answers/torche/aubergiste/menacer/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 16
# On sortira une phrase banale à tous les coups et on aura une réponse favorable PRESQUE à tous les coups
function tdh:dialogue/answers/torche/aubergiste/convaincre/question/basique

# 9 chances sur 10 d'avoir une réponse favorable (EN PAYANT!)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp
execute if score #random temp2 matches 90..99 run function tdh:dialogue/answers/torche/aubergiste/convaincre/reponse/echec
execute if score #random temp2 matches ..89 run function tdh:dialogue/answers/torche/aubergiste/convaincre/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 5
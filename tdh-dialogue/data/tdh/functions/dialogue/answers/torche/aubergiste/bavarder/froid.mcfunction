# On abrège la conversation
function tdh:dialogue/answers/torche/aubergiste/bavarder/froid/debutant

# On génère la phrase du PNJ
execute as @s[tag=Succes] run function tdh:dialogue/answers/torche/aubergiste/succes
execute as @s[tag=!Succes] run function tdh:dialogue/answers/torche/aubergiste/echec

# On supprime les tags temporaires
tag @s remove Succes
tag @s remove SuccesCritique
# On génère une mauvaise manière de se vanter avec l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '"Bien sûr ! Je n\'arrête pas d\'accomplir de nombreux actes héroïques. Comme la dernière fois, j\'ai aidé quelqu\'un et... il m\'a dit merci !"'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"C\'est ça, je pars à l\'aventure ! Je suis un aventurier et je parcours de nombreuses aventures. C\'est trop l\'éclate !"'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"L\'aventure, c\'est ma vie ! Je bois de l\'aventure, je mange de l\'aventure, je fais même caca de l\'aventure. C\'est trop chouette l\'aventure."'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Oui, le mois dernier j\'ai même participé à une aventure compétitive avec mes amis. Je suis vraiment un héros, croyez-moi."'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Je suis le plus grand aventurier de tout mon village natal ! Je me demande ce que certains feraient sans moi et d\'autres."'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 20
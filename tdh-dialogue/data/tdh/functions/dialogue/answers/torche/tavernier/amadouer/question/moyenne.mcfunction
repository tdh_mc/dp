# On génère une manière moyenne d'amadouer l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 unless score #temps idFete matches 1.. run data modify storage tdh:dialogue question set value '"On m\'attend à des lieues d\'ici, malheureusement... Mais je parlerai de votre taverne à mes amis, croyez-moi !"'
# TODO afficher le nom d'une ville
execute if score #random temp matches 20..39 unless score #temps idFete matches 1.. run data modify storage tdh:dialogue question set value '"Vous n\'allez pas me croire... ma mère m\'a justement invité dans une autre taverne à l\'autre bout du monde. Je n\'ai pas arrêté de lui dire que l\'accueil était plus chaleureux ici, mais elle n\'a rien voulu entendre. Si je peux la rejoindre aujourd\'hui, je vous la ramènerai bientôt pour qu\'elle voie la différence..."'
# TODO afficher le nom de la fête
execute if score #random temp matches 0..39 if score #temps idFete matches 1.. run data modify storage tdh:dialogue question set value '[{"text":"J\'aurais préféré... J\'adore votre taverne, mais mes amis m\'attendent en forêt pour les festivités d\'aujourd\'hui. Et si je n\'ai pas de lumière, je ne pourrai pas les rejoindre !"}]'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Votre taverne a l\'air superbe, mais je suis vraiment très pressé. Je ne manquerai pas de revenir dès que j\'aurai du temps... si je ne me fais pas manger par les loups sur la route, donc il me faudrait vraiment une torche !"'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Ce n\'est pas la dernière fois que je viens, croyez-moi ! Votre taverne est toujours un havre de paix et d\'amitié, mais aujourd\'hui je ne peux pas rester..."'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Vous êtes vraiment adorable ! Mais je ne peux pas me permettre d\'accepter, j\'ai trop à faire en ce moment."'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 10
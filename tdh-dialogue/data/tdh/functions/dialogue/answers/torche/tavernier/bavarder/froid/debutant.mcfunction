# On sortira une phrase désagréable à tous les coups
function tdh:dialogue/answers/torche/tavernier/bavarder/froid/question

# 9 chances sur 10 d'avoir une réponse favorable (EN PAYANT!)
# 1 chance sur 10 qu'on nous dise d'aller nous faire foutre
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp
execute if score #random temp2 matches 90..99 run function tdh:dialogue/answers/torche/tavernier/bavarder/froid/reponse/echec
execute if score #random temp2 matches ..89 run function tdh:dialogue/answers/torche/tavernier/bavarder/froid/reponse/succes
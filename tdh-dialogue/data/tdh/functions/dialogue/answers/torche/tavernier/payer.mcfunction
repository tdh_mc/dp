# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"Tenez, voici votre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"Voilà, "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", comme demandé."}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"Et voilà, "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", tout pile !"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"Ça me va. Voilà votre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà votre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Il vous fallait autre chose ?"}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Voici "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" pour vous ! C\'est tout ce que je pouvais faire pour vous ?"}]'

# On enregistre une tournure aléatoire pour le refus qui sera utilisée SI on a un refus de transaction
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Je crois que vous n\'avez pas assez de "},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Vous devez me payer "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":". C\'est non négociable !"}]'

# On enregistre les données dans le storage
data modify storage tdh:dialogue transaction.item set value {id:"minecraft:torch",Count:1b,tag:{tdhTorche:1b,CustomModelData:100,dynamique:100}}
data modify storage tdh:dialogue transaction.objet set value {nombre:"une ",unite:"torche"}

# On enregistre le prix dans le score
scoreboard players set #transaction fer 1

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
# On abrège la conversation
function tdh:dialogue/answers/torche/tavernier/bavarder/froid/debutant

# Selon le résultat généré, on affiche la suite des événéments
execute as @s[tag=Succes] run function tdh:dialogue/answers/torche/tavernier/succes
execute as @s[tag=!Succes] run function tdh:dialogue/answers/torche/tavernier/echec

# On supprime les tags temporaires
tag @s remove Succes
tag @s remove SuccesCritique
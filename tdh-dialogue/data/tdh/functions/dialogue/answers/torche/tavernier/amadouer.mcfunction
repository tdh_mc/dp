# Selon la valeur d'éloquence du joueur, on va générer une bonne ou une mauvaise phrase
# On stocke la valeur d'éloquence
scoreboard players operation #dialogue eloquence = @p[tag=SpeakingToNPC] eloquence
# On initialise le bonus d'éloquence qu'on aura éventuellement
scoreboard players set #joueur eloquenceXp 0

# Si on a moins de 10 d'éloquence, on sortira une mauvaise phrase à chaque fois
execute if score #dialogue eloquence matches ..9 run function tdh:dialogue/answers/torche/tavernier/amadouer/novice
# Si on a 10 à 20 d'éloquence, on sortira une mauvaise phrase assez souvent
execute if score #dialogue eloquence matches 10..20 run function tdh:dialogue/answers/torche/tavernier/amadouer/debutant
# Si on a 21 à 35 d'éloquence, on sortira une mauvaise phrase moyennement souvent
execute if score #dialogue eloquence matches 21..35 run function tdh:dialogue/answers/torche/tavernier/amadouer/apprenti
# Si on a 36 à 50 d'éloquence, on sortira rarement une mauvaise phrase
execute if score #dialogue eloquence matches 36..50 run function tdh:dialogue/answers/torche/tavernier/amadouer/confirme
# Si on a 51 à 75 d'éloquence, on sortira rarement une phrase moyenne
execute if score #dialogue eloquence matches 51..75 run function tdh:dialogue/answers/torche/tavernier/amadouer/adepte
# Si on a 76 à 100 d'éloquence, on sortira une très bonne phrase
execute if score #dialogue eloquence matches 76..100 run function tdh:dialogue/answers/torche/tavernier/amadouer/expert
# Si on a 101+ d'éloquence, on convaincra à tous les coups
execute if score #dialogue eloquence matches 101.. run function tdh:dialogue/answers/torche/tavernier/amadouer/maitre

# Selon le résultat généré, on affiche la suite des événéments
execute as @s[tag=Succes,tag=SuccesCritique] run function tdh:dialogue/answers/torche/tavernier/succes_critique
execute as @s[tag=Succes,tag=!SuccesCritique] run function tdh:dialogue/answers/torche/tavernier/succes
execute as @s[tag=!Succes] run function tdh:dialogue/answers/torche/tavernier/echec

# On enregistre les éventuels points d'éloquence ajoutés par l'interaction
function tdh:player/xp/eloquence/gain_xp

# On supprime les tags temporaires
scoreboard players reset #dialogue eloquence
tag @s remove Succes
tag @s remove SuccesCritique
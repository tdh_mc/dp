# On génère une réponse positive de l'aubergiste (pour DONNER une torche)
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Le moins qu\'on puisse dire, c\'est que vous savez causer ! La voilà, votre torche ; et bon courage !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Eh bien, je ne sais même pas quoi dire... Prenez toutes les torches qu\'il vous faudra, et que mes voeux vous accompagnent !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Vous m\'êtes sympathique, alors je vais faire une exception. La voilà, votre torche ; cadeau de la maison !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je ne vais pas simplement vous donner une torche : je vais prier pour vous ce soir ! À bientôt, j\'espère !"'

# On gagne quelques points d'éloquence pour un succès critique
scoreboard players add #joueur eloquenceXp 15

# On enregistre le succès
tag @s add Succes
tag @s add SuccesCritique
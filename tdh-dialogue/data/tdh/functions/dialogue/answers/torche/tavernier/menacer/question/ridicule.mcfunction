# On génère une manière ridicule de menacer l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '[{"text":"Ne te mets pas en travers de ma route, "},{"selector":"@s"},{"text":" ! J\'ai besoin de cette torche, ce n\'est pas gentil !"}]'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"Je suis très docile pour l\'instant, mais vous savez, je peux devenir féroce. Je peux avoir des dents de 50 centimètres moi. Je suis un fou. Donnez-moi cette torche."'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Faites attention, quand je n\'ai pas eu ma torche le soir avant de m\'endormir, il arrive que je devienne méchant."'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Sachez que les aigles ne volent pas avec les colibris ! Je domine les cieux tandis que vous zinzinulez piteusement ! Allez-vous vous mettre en travers du chemin de celui qui vous dépasse ?"'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Nique bien tes ancêtres. Fils de chien malade. Je vais aller faire mes besoins dans tes champs de carottes si tu ne me donnes pas de torche !"'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 25
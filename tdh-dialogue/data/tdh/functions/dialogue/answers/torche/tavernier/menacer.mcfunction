# Menacer est plus facile si on est mauvais en éloquence, mais ne sera pas très efficace si on est faible

# Selon la valeur d'éloquence et de force du joueur, on va générer une bonne ou une mauvaise phrase
# On stocke la valeur d'éloquence et de force
scoreboard players operation #dialogue eloquence = @p[tag=SpeakingToNPC] eloquence
scoreboard players operation #dialogue force = @p[tag=SpeakingToNPC] force
# On initialise le bonus d'éloquence qu'on aura éventuellement
scoreboard players set #joueur eloquenceXp 0

# Si on a moins de 10 d'éloquence et moins de 20 de force, on sera mauvais à chaque fois
execute if score #dialogue eloquence matches ..9 if score #dialogue force matches ..19 run function tdh:dialogue/answers/torche/tavernier/menacer/faible_novice
# Si on a moins de 50 d'éloquence et moins de 20 de force, on sera mauvais à chaque fois mais on a des chances de convaincre
execute if score #dialogue eloquence matches 10..49 if score #dialogue force matches ..19 run function tdh:dialogue/answers/torche/tavernier/menacer/faible_apprenti
# Si on a plus de 50 d'éloquence et moins de 20 de force, on sera tout aussi mauvais mais aura plus de chances de convaincre
execute if score #dialogue eloquence matches 50.. if score #dialogue force matches ..19 run function tdh:dialogue/answers/torche/tavernier/menacer/faible_expert

# Si on a moins de 10 d'éloquence et 20-50 de force, on sera un peu menaçant à chaque fois
execute if score #dialogue eloquence matches ..9 if score #dialogue force matches 20..49 run function tdh:dialogue/answers/torche/tavernier/menacer/moyen_novice
# Si on a moins de 50 d'éloquence et 20-50 de force, on aura plus de chances de convaincre
execute if score #dialogue eloquence matches 10..49 if score #dialogue force matches 20..49 run function tdh:dialogue/answers/torche/tavernier/menacer/moyen_apprenti
# Si on a plus de 50 d'éloquence et 20-50 de force, on aura plus de chances de convaincre
execute if score #dialogue eloquence matches 50.. if score #dialogue force matches 20..49 run function tdh:dialogue/answers/torche/tavernier/menacer/moyen_expert

# Si on a moins de 10 d'éloquence et 50+ de force, on sera un peu menaçant à chaque fois
execute if score #dialogue eloquence matches ..9 if score #dialogue force matches 50.. run function tdh:dialogue/answers/torche/tavernier/menacer/fort_novice
# Si on a moins de 50 d'éloquence et 50+ de force, on aura plus de chances de convaincre
execute if score #dialogue eloquence matches 10..49 if score #dialogue force matches 50.. run function tdh:dialogue/answers/torche/tavernier/menacer/fort_apprenti
# Si on a plus de 50 d'éloquence et 50+ de force, on aura plus de chances de convaincre
execute if score #dialogue eloquence matches 50.. if score #dialogue force matches 50.. run function tdh:dialogue/answers/torche/tavernier/menacer/fort_expert

# Selon le résultat généré, on affiche la suite des événéments
execute as @s[tag=Succes,tag=SuccesCritique] run function tdh:dialogue/answers/torche/tavernier/succes_critique
execute as @s[tag=Succes,tag=!SuccesCritique] run function tdh:dialogue/answers/torche/tavernier/succes
execute as @s[tag=!Succes] run function tdh:dialogue/answers/torche/tavernier/echec

# On enregistre les éventuels points d'éloquence ajoutés par l'interaction
function tdh:player/xp/eloquence/gain_xp

# On supprime les tags temporaires
scoreboard players reset #dialogue eloquence
scoreboard players reset #dialogue force
tag @s remove Succes
tag @s remove SuccesCritique
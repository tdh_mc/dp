# On génère une mauvaise manière d'amadouer l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '"Je suis très très très pressé, mais votre taverne a l\'air très très... sympathique... Oui, vraiment."'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"Oh, non, ce n\'est... ce n\'est pas la peine. Ce n\'est pas contre vous ! Ni contre la clientèle charmante de cette charmante euh... taverne..."'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Écoutez... Vous m\'êtes vraiment très sympathique et... Je pensais... que je pourrais revenir, par exemple, plus tard, par exemple pour me murger la figure, et si vous êtes d\'accord... Mais pas aujourd\'hui, malheureusement..."'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Votre taverne a l\'air vraiment géniale, top délire... Mais je ne peux vraiment pas rester..."'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Mais je n\'en ai pas envie... même si c\'est adorable..."'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 20
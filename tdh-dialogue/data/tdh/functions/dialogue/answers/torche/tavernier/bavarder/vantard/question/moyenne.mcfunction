# On génère une manière moyenne de se vanter avec l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '"L\'aventure, c\'est toute ma vie ! Siroter une bière à la taverne avant de partir fracasser des brigands, quelle meilleure occupation que celle-ci ?"'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"Évidemment que je suis un aventurier ! Les autres activités de ce monde ne sont pas assez enrichissantes."'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Il m\'arrive de me mettre en quatre pour aider mon prochain et de mettre mes muscles au service des faibles, si tel est le sens de votre question."'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Il n\'y a pas plus aventurier que moi, dans le coin ! Et une mission urgente m\'attend justement à des lieues d\'ici."'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"L\'appel du devoir est plus fort que tout – même plus fort que vos breuvages !"'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 12
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une mauvaise phrase dans 80% des cas, et une moyenne sinon
execute if score #random temp2 matches ..79 run function tdh:dialogue/answers/torche/tavernier/amadouer/question/mauvaise
execute if score #random temp2 matches 80.. run function tdh:dialogue/answers/torche/tavernier/amadouer/question/moyenne

# On a une réponse favorable lorsqu'on sort une phrase moyenne (sauf exception de 1/20) et seulement quelques chances si elle est mauvaise
execute if score #random temp2 matches 74..98 run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/succes
execute unless score #random temp2 matches 74..98 run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 45
# On génère une phrase modeste pour répondre à l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '"Eh bien, pas vraiment... J\'ai simplement besoin de rentrer chez moi. Vous devez croiser bien plus d\'aventuriers que moi !"'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"Il faut le dire vite ! Je pars au travail, et j\'aurai besoin d\'y voir."'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"J\'essaie déjà de m\'occuper de mes propres problèmes avant de régler ceux des autres."'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Ma vie est bien moins trépidante que vous ne semblez l\'imaginer... Je dois juste rentrer chez moi, rien de plus."'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Si je partais à l\'aventure, j\'aurais de quoi me fabriquer une torche, vous ne croyez pas ?"'
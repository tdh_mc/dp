# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase ridicule dans 50% des cas, une passable dans 50% des cas
execute if score #random temp2 matches ..49 run function tdh:dialogue/answers/torche/tavernier/menacer/question/ridicule
execute if score #random temp2 matches 50.. run function tdh:dialogue/answers/torche/tavernier/menacer/question/passable

# On a une petite chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une phrase passable
execute if score #random temp2 matches 85..87 run function tdh:dialogue/answers/torche/tavernier/menacer/reponse/succes_critique
# On a un succès normal sinon si la phrase est passable
execute unless score #random temp2 matches 85..87 if score #random temp2 matches 44.. run function tdh:dialogue/answers/torche/tavernier/menacer/reponse/succes
# Sinon, si on est ridicule, c'est l'échec
execute unless score #random temp2 matches 44.. run function tdh:dialogue/answers/torche/tavernier/menacer/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 10
# On active le mode réponse chez l'aubergiste (on enregistre le fait qu'il a posé une question)
scoreboard players set @s topic 383

# On active le mode réponse chez le joueur (on désactive la possibilité de proposer un sujet, et on active les réponses à la place)
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer


# Les différents choix possibles sont affichés à la personne qui dialogue
tag @s add Options
# On demande plutôt à prendre une chambre, finalement
data modify storage tdh:dialogue options append value '[{"text":"Eh bien, à ce prix... Je vais plutôt vous prendre un verre !","clickEvent":{"action":"run_command","value":"/trigger answer set 381"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander à boire."}]}}]'

# On accepte de payer un lingot de fer
data modify storage tdh:dialogue options append value '[{"text":"Je marche. Voici votre fer.","clickEvent":{"action":"run_command","value":"/trigger answer set 100"},"hoverEvent":{"action":"show_text","value":[{"text":"Donner un lingot de fer."}]}}]'

# On refuse
data modify storage tdh:dialogue options append value '[{"text":"Je vais plutôt me débrouiller tout seul.","clickEvent":{"action":"run_command","value":"/trigger answer set 1"},"hoverEvent":{"action":"show_text","value":[{"text":"Refuser l\'offre du tavernier."}]}}]'
# Convaincre est plus facile qu'amadouer, mais il y a aussi moins de chances d'avoir un succès critique

# Selon la valeur d'éloquence du joueur, on va générer une bonne ou une mauvaise phrase
# On stocke la valeur d'éloquence
scoreboard players operation #dialogue eloquence = @p[tag=SpeakingToNPC] eloquence
# On initialise le bonus d'éloquence qu'on aura éventuellement
scoreboard players set #joueur eloquenceXp 0

# Si on a moins de 25 d'éloquence, on sortira une phrase basique à chaque fois
execute if score #dialogue eloquence matches ..24 run function tdh:dialogue/answers/torche/tavernier/convaincre/debutant
# Si on a 25 à 50 d'éloquence, on sortira une phrase basique assez souvent
execute if score #dialogue eloquence matches 25..50 run function tdh:dialogue/answers/torche/tavernier/convaincre/confirme
# Si on a 50 à 75 d'éloquence, on sortira une bonne phrase souvent
execute if score #dialogue eloquence matches 51..75 run function tdh:dialogue/answers/torche/tavernier/convaincre/adepte
# Si on a 75 à 100 d'éloquence, on sortira rarement une phrase basique
execute if score #dialogue eloquence matches 76..100 run function tdh:dialogue/answers/torche/tavernier/convaincre/expert
# Si on a 101+ d'éloquence, on convaincra à tous les coups
execute if score #dialogue eloquence matches 101.. run function tdh:dialogue/answers/torche/tavernier/convaincre/maitre

# Selon le résultat généré, on affiche la suite des événéments
execute as @s[tag=Succes,tag=SuccesCritique] run function tdh:dialogue/answers/torche/tavernier/succes_critique
execute as @s[tag=Succes,tag=!SuccesCritique] run function tdh:dialogue/answers/torche/tavernier/succes
execute as @s[tag=!Succes] run function tdh:dialogue/answers/torche/tavernier/echec

# On enregistre les éventuels points d'éloquence ajoutés par l'interaction
function tdh:player/xp/eloquence/gain_xp

# On supprime les tags temporaires
scoreboard players reset #dialogue eloquence
tag @s remove Succes
tag @s remove SuccesCritique
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase mauvaise dans 70% des cas, une bonne dans 30% des cas
execute if score #random temp2 matches ..69 run function tdh:dialogue/answers/torche/tavernier/bavarder/vantard/question/mauvaise
execute if score #random temp2 matches 70.. run function tdh:dialogue/answers/torche/tavernier/bavarder/vantard/question/moyenne

# On a une petite chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une bonne phrase
execute if score #random temp2 matches 90..94 run function tdh:dialogue/answers/torche/tavernier/bavarder/vantard/reponse/succes_critique
# On a un succès normal sinon
execute unless score #random temp2 matches 90..94 run function tdh:dialogue/answers/torche/tavernier/bavarder/vantard/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 20
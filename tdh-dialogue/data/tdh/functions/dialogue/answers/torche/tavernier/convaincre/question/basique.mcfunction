# On génère une manière moyenne de convaincre l'aubergiste
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '"Vous n\'allez pas refuser de me vendre de l\'équipement, quand même ? C\'est aussi dans votre intérêt !"'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"Je croyais que l\'argent adoucissait les moeurs... Vous n\'aimez pas l\'argent, dites ?"'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Je suis vraiment pressé. Vous ne voulez pas que je vous paie ?"'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Je n\'ai vraiment pas le temps, malheureusement. Je n\'aurais pas cru que quelqu\'un d\'honnête comme vous rejetterait une occasion de gagner quelques pièces !"'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Ce n\'est pas une question de volonté, mais de devoir. Combien faut-il donner pour vous convaincre ?"'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 10
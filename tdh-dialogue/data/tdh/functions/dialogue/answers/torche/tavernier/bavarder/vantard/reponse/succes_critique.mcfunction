# On génère une réponse enjouée de l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Eh bien, ce fut un honneur de vous recevoir ici ! La voici, votre torche ; la bonne fortune vous accompagne !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Je souhaite que votre périple soit couronné de succès ! Et entre nous, la torche... je ne vais pas vous la faire payer. Cadeau de la maison ! Faites-en bon usage."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Incroyable ! Votre quête m\'a l\'air bien grande et grosse. Je vous la donne, la torche ; vous en ferez plus pieux usage que nous !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Eh bien, votre torche vous est gracieusement offerte par l\'auberge "},{"entity":"@e[type=item_frame,tag=Auberge,sort=nearest,limit=1]","nbt":"Item.tag.auberge.deDu"},{"entity":"@e[type=item_frame,tag=Auberge,sort=nearest,limit=1]","nbt":"Item.tag.auberge.nom"},{"text":" ! Faites bonne route, "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" !"}]'

# On enregistre le succès
tag @s add Succes
tag @s add SuccesCritique
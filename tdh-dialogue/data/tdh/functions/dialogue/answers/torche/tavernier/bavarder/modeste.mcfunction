# On génère à tous les coups une phrase de modeste
function tdh:dialogue/answers/torche/tavernier/bavarder/modeste/debutant

# On affiche la suite des événéments (le tavernier veut bien nous vendre la torche)
function tdh:dialogue/answers/torche/tavernier/succes

# On supprime les tags temporaires
tag @s remove Succes
tag @s remove SuccesCritique
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une mauvaise phrase dans 40% des cas, une moyenne dans 50% des cas, et une bonne sinon
execute if score #random temp2 matches ..39 run function tdh:dialogue/answers/torche/tavernier/amadouer/question/mauvaise
execute if score #random temp2 matches 40..89 run function tdh:dialogue/answers/torche/tavernier/amadouer/question/moyenne
execute if score #random temp2 matches 90.. run function tdh:dialogue/answers/torche/tavernier/amadouer/question/bonne

# On a une petite chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une bonne phrase et/ou une moyenne (rarement)
execute if score #random temp2 matches 87..95 run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/succes_critique
# On a un succès normal lorsqu'on a fait une bonne phrase et (rarement) une mauvaise
execute unless score #random temp2 matches 87..95 if score #random temp2 matches 35.. run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/succes
# Sinon, c'est l'échec
execute unless score #random temp2 matches 35.. run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/echec

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 60
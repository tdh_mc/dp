# On génère une réponse neutre de l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Ne le prenez pas mal ! Pour la torche, un lingot de fer, ça vous ira ?"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"C\'est tout aussi honorable. Votre torche, je vous la fais pour un lingot de fer, c\'est bon ?"'
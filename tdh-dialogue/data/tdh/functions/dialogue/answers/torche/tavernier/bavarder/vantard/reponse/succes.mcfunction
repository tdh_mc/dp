# On génère une réponse neutre de l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Eh bien, c\'est... passionnant ! Bon, pour votre torche, un lingot de fer, ça vous ira ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Fascinant, vraiment... fascinant. Votre torche, je vous la fais pour un lingot de fer, c\'est bon ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Hmm. Bref, pour votre torche, vous auriez un lingot de fer ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"En tout cas, ça fait plaisir. Je vous fais la torche pour un lingot de fer, d\'accord ?"'

# On enregistre le succès
tag @s add Succes
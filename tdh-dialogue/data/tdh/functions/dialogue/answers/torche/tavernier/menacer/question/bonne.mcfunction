# On génère une bonne manière de menacer l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '[{"text":"Bien sûr, "},{"selector":"@s"},{"text":". Voyez-vous, j\'ai toujours énormément apprécié votre bicoque. Je serais vraiment... TRÈS déçu de ne plus pouvoir m\'y arrêter, si vous voyez ce que je veux dire. « Pourtant, je n\'avais pas arrêté de lui expliquer – dirai-je aux enquêteurs. À chaque visite, je lui disais que le bois, c\'était peut-être très joli, mais surtout très inflammable... » Les accidents, ça arrive, n\'est-ce-pas ? Et on a souvent du mal à en déterminer l\'origine... Je me fais comprendre ?"}]'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"Je ne crois vraiment pas vous avoir laissé le choix, et si j\'ai mal été compris, permettez-moi de me corriger... Vous allez me fournir une torche. Tout de suite. Sans quoi je risquerais de me fâcher tout rouge..."'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Ha ha ha ! Excusez-moi, vous allez me trouver bête, mais pendant l\'ombre d\'un instant j\'ai presque cru que vous veniez de me refuser cette torche que je vous ai demandée... SI GENTIMENT. Vous êtes bien sûr de ne pas vous êtes mal exprimé ?"'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Ton grand-père le cadavre, tavernier, je vais te le soulever ! Tu as exactement une minute pour me ramener cette foutue torche avant que je te passe par le fil de mon épée !"'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Sale fils d\'unijambiste, détritus tutélaire de ces landes désolées ! Si tu ne me donnes pas une torche immédiatement, ton futur deviendra aussi vide que l\'éther luminifère qui remplace tes trois neurones morts !"'

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 10
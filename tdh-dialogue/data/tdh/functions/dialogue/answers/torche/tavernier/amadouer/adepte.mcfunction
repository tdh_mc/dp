# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira une phrase moyenne dans 40% des cas, et une bonne sinon
execute if score #random temp2 matches ..39 run function tdh:dialogue/answers/torche/tavernier/amadouer/question/moyenne
execute if score #random temp2 matches 40.. run function tdh:dialogue/answers/torche/tavernier/amadouer/question/bonne

# On a une grande chance d'avoir un succès critique (torche gratuite) lorsqu'on a fait une bonne phrase et/ou une moyenne (rarement)
execute if score #random temp2 matches 35..98 run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/succes_critique
# On a un succès normal lorsqu'on a fait une moyenne phrase
execute unless score #random temp2 matches 35..98 run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 30
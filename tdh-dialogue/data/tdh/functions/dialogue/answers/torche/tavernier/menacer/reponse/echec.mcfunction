# On génère une réponse négative de l'tavernier
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Ce n\'est pas une petite frappe dans ton genre qui va me faire peur ! Dégage de ma taverne, l\'ahuri !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Tu veux qu\'on se tape, c\'est ça ? Fais gaffe, parce que tu risques de passer pour un gros nul devant toute la taverne au lieu de partir gentiment sans faire d\'histoire et de simplement passer pour un bouffon."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Mon gars, il y a erreur, je crois que tu as confondu ma charmante taverne avec la maison de ta daronne ! Va donc voir chez elle s\'il y a des torches à vendre, et profites-en pour aller bien te faire foutre !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Tu crois me faire peur avec tes menaces d\'écolier ? Sors un peu dans la rue, trouve-toi un boulot, et surtout apprends à fabriquer des torches toi-même, gros boloss !"'

# On gagne plus de points d'éloquence si on foire
scoreboard players add #joueur eloquenceXp 13
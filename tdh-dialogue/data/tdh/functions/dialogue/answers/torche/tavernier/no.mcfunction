# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Je me débrouillerai sans vous, alors."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous êtes vachement vache !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Ça n\'est pas gentil."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Eh bien, si je m\'attendais..."'

# On enregistre une tournure aléatoire pour la réponse du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"J\'essaie simplement de vous aider !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Si vous voulez tout de même à boire, demandez-moi."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Je prie pour le salut de votre âme..."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Vous savez, j\'en ai vu d\'autres."'

# Fin de conversation
tag @s add AuRevoir
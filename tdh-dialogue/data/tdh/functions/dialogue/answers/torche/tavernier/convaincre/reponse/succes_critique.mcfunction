# On génère une réponse positive de l'tavernier (pour DONNER une torche)
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Très franchement, vous m\'êtes sympathique. La voici, votre torche ; et bon voyage à vous !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Écoutez, je ne suis pas un rapiat, et je sais reconnaître lorsque quelqu\'un est dans le besoin. Prenez-la donc, votre torche !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Pour vous, ça ne coûtera pas cher, ne vous inquiétez pas. La voilà, votre torche ; cadeau de la maison !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Avoir le sens des affaires, c\'est aussi savoir faire des cadeaux ! Je ne vais quand même pas vous faire payer pour une torche. À bientôt, j\'espère !"'

# On gagne quelques points d'éloquence pour un succès critique
scoreboard players add #joueur eloquenceXp 13

# On enregistre le succès
tag @s add Succes
tag @s add SuccesCritique
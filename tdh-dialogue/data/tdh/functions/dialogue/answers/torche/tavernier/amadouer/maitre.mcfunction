# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp

# On sortira toujours une bonne phrase
function tdh:dialogue/answers/torche/tavernier/amadouer/question/bonne

# On a une grande chance d'avoir un succès critique
execute if score #random temp2 matches 5.. run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/succes_critique
execute unless score #random temp2 matches 5.. run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 10
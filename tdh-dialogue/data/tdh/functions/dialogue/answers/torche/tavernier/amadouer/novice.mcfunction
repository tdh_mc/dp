# On sortira une mauvaise phrase à tous les coups et on aura une réponse défavorable PRESQUE à tous les coups
function tdh:dialogue/answers/torche/tavernier/amadouer/question/mauvaise

# 1 chance sur 10 d'avoir une réponse favorable (EN PAYANT!)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp
execute if score #random temp2 matches 1.. run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/echec
execute if score #random temp2 matches 0 run function tdh:dialogue/answers/torche/tavernier/amadouer/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 30
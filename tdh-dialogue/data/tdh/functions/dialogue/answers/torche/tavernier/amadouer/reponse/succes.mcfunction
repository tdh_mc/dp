# On génère une réponse positive de l'aubergiste (pour VENDRE une torche)
# On stocke une variable aléatoire entre 0 et 99
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On stocke une phrase différente selon la valeur de cette variable
execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Pas de problème pour une torche, alors ! Un lingot de fer, ça vous ira ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Eh bien, je ne vois pas comment je pourrais refuser... Pour un lingot de fer, je descends vous en chercher une, de torche."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"On doit bien avoir quelques torches qui traînent à la cave ! Tout comme un lingot de fer doit bien traîner dans votre poche..."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Je vais vous en chercher une. Vous me laissez un petit lingot de fer, pour mes bonnes oeuvres ?"'

# On enregistre le succès
tag @s add Succes
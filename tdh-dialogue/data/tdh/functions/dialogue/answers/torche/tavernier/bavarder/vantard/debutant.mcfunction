# On sortira une vantardise éclatée à tous les coups
function tdh:dialogue/answers/torche/tavernier/bavarder/vantard/question/mauvaise

# 9 chances sur 10 d'avoir une réponse favorable (EN PAYANT!) et 1 chance sur 10 sans payer
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #random temp2 = #random temp
execute if score #random temp2 matches 90..99 run function tdh:dialogue/answers/torche/tavernier/bavarder/vantard/reponse/succes_critique
execute if score #random temp2 matches ..89 run function tdh:dialogue/answers/torche/tavernier/bavarder/vantard/reponse/succes

# On gagne des points d'éloquence
scoreboard players add #joueur eloquenceXp 10
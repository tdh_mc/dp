# On fait comme si on venait de demander à boire
function tdh:dialogue/topics/boire

# On remplace la phrase qu'on prononce par qqch de plus contextuel
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous avez sans doute raison... Vous avez quoi en réserve ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Je crois que vous avez raison... Qu\'est-ce que vous me proposez ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Vous devez avoir raison. Qu\'est-ce que vous avez à boire ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Vous n\'avez pas tort, et puis j\'ai une petite soif... Qu\'est-ce que vous avez à me proposer ?"'
# Selon notre réponse, on lance la sous-fonction correspondante
# Si on essaie de persuader l'aubergiste de nous filer une torche
execute if score @p[tag=SpeakingToNPC] answer matches 10 run function tdh:dialogue/answers/torche/aubergiste/amadouer
execute if score @p[tag=SpeakingToNPC] answer matches 20 run function tdh:dialogue/answers/torche/aubergiste/convaincre
execute if score @p[tag=SpeakingToNPC] answer matches 30 run function tdh:dialogue/answers/torche/aubergiste/menacer

# Si on accepte de payer 1 fer pour avoir notre torche
execute if score @p[tag=SpeakingToNPC] answer matches 100 run function tdh:dialogue/answers/torche/aubergiste/payer

# Si on accepte de prendre une chambre/un verre à la place
execute if score @p[tag=SpeakingToNPC] answer matches 381 run function tdh:dialogue/answers/torche/aubergiste/prendre_verre
execute if score @p[tag=SpeakingToNPC] answer matches 382 run function tdh:dialogue/answers/torche/aubergiste/prendre_chambre

# Si on engage la conversation sur l'aventure en se vantant
execute if score @p[tag=SpeakingToNPC] answer matches 1000 run function tdh:dialogue/answers/torche/aubergiste/bavarder/vantard
# Si on engage la conversation sur l'aventure modestement
execute if score @p[tag=SpeakingToNPC] answer matches 1050 run function tdh:dialogue/answers/torche/aubergiste/bavarder/modeste
# Si on insiste simplement pour avoir notre torche
execute if score @p[tag=SpeakingToNPC] answer matches 1100 run function tdh:dialogue/answers/torche/aubergiste/bavarder/froid

# Sinon, fin de conversation
execute if score @p[tag=SpeakingToNPC] answer matches ..0 run function tdh:dialogue/answers/torche/aubergiste/no
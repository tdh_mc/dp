# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"Je vais vous prendre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"Vous auriez "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"Je voudrais bien "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" !"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"Ça sera "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", pour moi, s\'il vous plaît."}]'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Bonne dégustation ! C\'est tout ce que vous désiriez ?"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Voici "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" pour vous ! Bonne dégustation ! C\'est tout ce que je pouvais faire pour vous ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà ! Profitez-en bien ! Vous aviez besoin d\'autre chose ?"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Si vous avez besoin de plus de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", vous saurez où me trouver ! Il vous fallait autre chose ?"}]'

# On enregistre une tournure aléatoire pour le refus qui sera utilisée SI on a un refus de transaction
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Je crois que vous n\'avez pas assez de "},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Il va me falloir ces "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":" si vous voulez "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Vous devez sous-estimer le prix de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Vous devez me payer "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":". C\'est non négociable !"}]'


# Si on a répondu qu'on voulait bien acheter, on appelle une sous fonction pour faire différentes choses:
# - Enregistrer le prix (dans #transaction fer/or/diamant/kubs)
# - Enregistrer dans le storage l'item à donner au joueur (tdh:dialogue transaction.objet)
execute if score @p[tag=SpeakingToNPC] answer matches 101 run function tdh:dialogue/answers/boire/biere/1
execute if score @p[tag=SpeakingToNPC] answer matches 102 run function tdh:dialogue/answers/boire/biere/2
execute if score @p[tag=SpeakingToNPC] answer matches 104 run function tdh:dialogue/answers/boire/biere/4
execute if score @p[tag=SpeakingToNPC] answer matches 108 run function tdh:dialogue/answers/boire/biere/8

# Pour le cidre
execute if score @p[tag=SpeakingToNPC] answer matches 121 run function tdh:dialogue/answers/boire/cidre_doux/1
execute if score @p[tag=SpeakingToNPC] answer matches 122 run function tdh:dialogue/answers/boire/cidre_doux/2
execute if score @p[tag=SpeakingToNPC] answer matches 124 run function tdh:dialogue/answers/boire/cidre_doux/4
execute if score @p[tag=SpeakingToNPC] answer matches 128 run function tdh:dialogue/answers/boire/cidre_doux/8
execute if score @p[tag=SpeakingToNPC] answer matches 131 run function tdh:dialogue/answers/boire/cidre_brut/1
execute if score @p[tag=SpeakingToNPC] answer matches 132 run function tdh:dialogue/answers/boire/cidre_brut/2
execute if score @p[tag=SpeakingToNPC] answer matches 134 run function tdh:dialogue/answers/boire/cidre_brut/4
execute if score @p[tag=SpeakingToNPC] answer matches 138 run function tdh:dialogue/answers/boire/cidre_brut/8

# Pour le vin de melon
execute if score @p[tag=SpeakingToNPC] answer matches 201 run function tdh:dialogue/answers/boire/vin_melon/1
execute if score @p[tag=SpeakingToNPC] answer matches 202 run function tdh:dialogue/answers/boire/vin_melon/2
execute if score @p[tag=SpeakingToNPC] answer matches 204 run function tdh:dialogue/answers/boire/vin_melon/4
execute if score @p[tag=SpeakingToNPC] answer matches 208 run function tdh:dialogue/answers/boire/vin_melon/8

# Pour l'hydromel
execute if score @p[tag=SpeakingToNPC] answer matches 211 run function tdh:dialogue/answers/boire/hydromel/1
execute if score @p[tag=SpeakingToNPC] answer matches 212 run function tdh:dialogue/answers/boire/hydromel/2
execute if score @p[tag=SpeakingToNPC] answer matches 214 run function tdh:dialogue/answers/boire/hydromel/4
execute if score @p[tag=SpeakingToNPC] answer matches 218 run function tdh:dialogue/answers/boire/hydromel/8

# Si on n'a pas pu récupérer de valeur, on dit qu'on ne veut rien finalement
execute as @s[tag=!Transaction] run function tdh:dialogue/answers/boire/no

# Sinon, on affiche les options
execute as @s[tag=Transaction] run function tdh:dialogue/answers/boire/autre_chose
# On enregistre les données dans le storage
function tdh:dialogue/answers/boire/cidre_doux
data modify storage tdh:dialogue transaction.objet set value {nombre:"quatre ",unite:"cidres doux"}
data modify storage tdh:dialogue transaction.item.Count set value 4b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 3

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
# On enregistre le topic chez le PNJ ("autre sujet", topic 2)
scoreboard players set @s topic 2

# On active le mode réponse chez le joueur
# Mais on ne désactive PAS la possibilité de proposer un sujet
#scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les choix de réponse
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"Oui, il me faudrait davantage à boire...","clickEvent":{"action":"run_command","value":"/trigger answer set 381"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander une autre boisson."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"J\'aimerais aussi manger, si c\'est possible.","clickEvent":{"action":"run_command","value":"/trigger answer set 380"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander s\'il y a quelque chose à manger."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Vous auriez une chambre pour la nuit ?","clickEvent":{"action":"run_command","value":"/trigger answer set 382"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander s\'il y a des chambres disponibles."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je voulais aussi parler d\'autre chose...","clickEvent":{"action":"run_command","value":"/trigger answer set -2"},"hoverEvent":{"action":"show_text","value":[{"text":"Aborder un autre sujet."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Non, ça sera tout.","clickEvent":{"action":"run_command","value":"/trigger answer set -1"},"hoverEvent":{"action":"show_text","value":[{"text":"Dire au revoir."}]}}]'
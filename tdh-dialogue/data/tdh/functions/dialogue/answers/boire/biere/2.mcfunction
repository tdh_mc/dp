# On enregistre les données dans le storage
function tdh:dialogue/answers/boire/biere
data modify storage tdh:dialogue transaction.objet set value {nombre:"deux ",unite:"bières"}
data modify storage tdh:dialogue transaction.item.Count set value 2b

# On enregistre le prix dans le score
scoreboard players set #transaction fer 2

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
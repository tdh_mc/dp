# Pour le cas de l'usager qui déteste TCH et qui a des conseils sur les vendeurs à la sauvette
# Si on a demandé des conseils
execute if score @p[tag=SpeakingToNPC] answer matches 300 run function tdh:dialogue/answers/tch/sauvette/usager/deteste_tch/demander_conseils
# Si on a dit au revoir en remerciant l'usager
execute if score @p[tag=SpeakingToNPC] answer matches 399 run function tdh:dialogue/answers/tch/sauvette/usager/deteste_tch/merci_au_revoir

# Si on a répondu que c'était honteux et qu'on allait partir
execute if score @p[tag=SpeakingToNPC] answer matches 100 run function tdh:dialogue/answers/tch/sauvette/usager/deteste_tch/contredire
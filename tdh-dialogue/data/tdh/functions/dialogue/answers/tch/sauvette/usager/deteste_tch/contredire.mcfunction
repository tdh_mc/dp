# On choisit une phrase pour la "question"
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Si vous le dites..."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Je ne m\'attendais pas à ce que quelqu\'un puisse apprécier ces vauriens ! Je suppose qu\'il faut de tout pour faire un monde..."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Ça ne m\'intéresse plus du tout. Je pensais simplement que vous alliez en dire du mal, comme tout le monde."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Ces gens-là feraient mieux de se trouver un vrai travail ! Et vous aussi, tant qu\'à faire, parce qu\'avocat du diable ça ne paie plus très bien !"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"C\'est ça, ouais."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Allez, barre-toi, putain de capitaliste !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Tu veux un cookie, c\'est ça ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Ben alors casse-toi, pauvre con."'

# Fin de conversation
tag @s add AuRevoir
# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vous pouvez me dire quels vendeurs sont fiables ?"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous connaissez des revendeurs de confiance, vous ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Ça a l\'air sympa de pouvoir payer ses tickets moins cher. Mais comment je peux savoir à qui faire confiance ?"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Vous devez bien avoir une ou deux bonnes adresses pour acheter des tickets qui marchent ?"'

# Selon la connaissance de TCH du PNJ, on génère un conseil de sauvette plus ou moins efficace
execute if score @s knowsTCH matches ..54 run function tdh:dialogue/answers/tch/sauvette/usager/deteste_tch/demander_conseils/reponse/mauvais
execute if score @s knowsTCH matches 55..79 run function tdh:dialogue/answers/tch/sauvette/usager/deteste_tch/demander_conseils/reponse/moyen
execute if score @s knowsTCH matches 80.. run function tdh:dialogue/answers/tch/sauvette/usager/deteste_tch/demander_conseils/reponse/bon


# On active le mode réponse chez le joueur
# Mais on ne désactive PAS la possibilité de proposer un sujet
#scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les choix de réponse
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"Je voulais aussi parler d\'autre chose...","clickEvent":{"action":"run_command","value":"/trigger answer set -2"},"hoverEvent":{"action":"show_text","value":[{"text":"Aborder un autre sujet."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Merci beaucoup, pour tout. À bientôt !","clickEvent":{"action":"run_command","value":"/trigger answer set 399"},"hoverEvent":{"action":"show_text","value":[{"text":"Dire au revoir."}]}}]'
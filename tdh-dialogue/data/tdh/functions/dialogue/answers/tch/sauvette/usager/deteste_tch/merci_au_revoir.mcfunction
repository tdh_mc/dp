# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '"Merci beaucoup pour ces conseils. Au revoir !"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '"Merci infiniment pour ces paroles. À bientôt !"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"N\'en parlez à personne !"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Il n\'y a pas de quoi !"'

# Fin de conversation
tag @s add AuRevoir
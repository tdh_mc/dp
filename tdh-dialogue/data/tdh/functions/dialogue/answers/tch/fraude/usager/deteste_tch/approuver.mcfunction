# On choisit une phrase pour la "question"
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Je suis bien d\'accord avec vous ! J\'ai toujours pensé qu\'il y avait des choses louches dans les coulisses de TCH."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous avez bien raison ! Il faudrait que plus de gens osent dire ce genre de choses."'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Merci pour vos paroles. Ça fait du bien d\'entendre autre chose que l\'habituel blabla pro-patrons."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Eh bien, vous n\'y allez pas de main morte ! Mais je ne peux pas vous donner tort, TCH semble avoir quelques casseroles..."'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Eh bien, en tout cas, ça fait plaisir ! Si vous avez besoin d\'astuces, n\'hésitez pas à venir me rendre visite."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Ben dis donc, pour une fois que j\'entends ça ! Tu es plus malin que la moyenne, camarade. Tu permets que je t\'appelle camarade ?"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Si tu as envie de te renseigner plus en détails sur les sales agissements de TCH, n\'hésite pas à lire le Grenard Enchaîné. C\'est un journal grenois un peu underground, genre dans les caves et tout. Ils ont des yeux et des oreilles partout, tout le gratin politique Hamélien a une trouille pas possible d\'être pris la main dans le sac et affiché à la Une..."'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"On n\'imagine pas à quel point tous ceux qui traînent dans les hautes sphères du pouvoir ont les mains sales. C\'est à se demander si c\'est possible de les atteindre sans être corrompu jusqu\'à l\'os."'

# On active le mode réponse chez le joueur
# Mais on ne désactive PAS la possibilité de proposer un sujet
#scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players enable @p[tag=SpeakingToNPC] answer

# On enregistre les choix de réponse
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"À ce propos, il y aurait moyen d\'avoir des conseils pour frauder ?","clickEvent":{"action":"run_command","value":"/trigger answer set 300"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander des conseils en matière de fraude."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je voulais aussi parler d\'autre chose...","clickEvent":{"action":"run_command","value":"/trigger answer set -2"},"hoverEvent":{"action":"show_text","value":[{"text":"Aborder un autre sujet."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Merci beaucoup, pour tout. À bientôt !","clickEvent":{"action":"run_command","value":"/trigger answer set 399"},"hoverEvent":{"action":"show_text","value":[{"text":"Dire au revoir."}]}}]'
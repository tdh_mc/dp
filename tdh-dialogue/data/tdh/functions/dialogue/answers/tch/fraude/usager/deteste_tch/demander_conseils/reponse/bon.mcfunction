# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Il y a plus sûr que de se fier à sa bonne étoile : se lier d\'amitié avec un contrôleur, ça n\'a l\'air de rien, mais ça change la vie. Selon tes talents de baratineur, tu peux espérer glaner des informations sur les patrouilles et l\'organisation des contrôles, voire subtiliser un uniforme et un badge pour te déplacer gratuitement et sans entrave pour le reste de tes jours..."'
# TODO créer et lancer la quête Se lier d'amitié avec un contrôleur

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Si tu as un peu de cran, j\'ai peut-être un plan en or. Mon pote gestionnaire de communautés, de l\'agence commerciale de Knosos, m\'a raconté la dernière fois que c\'est là qu\'ils stockent les uniformes et les badges de contrôleur. Il y a une entrée discrète pour le personnel, et ça devrait être possible d\'en crocheter la serrure..."'
# TODO créer et lancer la quête Voler un uniforme de contrôleur

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Il paraît que le siège de TCH abrite le fichier automatisé des abonnements. C\'est pas à la portée du premier venu, mais il y a certainement un moyen de s\'y introduire et de falsifier un abonnement ou deux sur l\'ordinateur... Tu te prends une carte Gold, puis tu entres en douce dans le siège... et tu te débrouilles pour accéder au fichier. Ils doivent bien garder les plans d\'accès quelque part. C\'est tout ce que je sais..."'
# TODO créer et lancer la quête S'introduire au PCC

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"J\'ai entendu dire qu\'un forgeron légendaire avait créé une paire d\'ailes qui permettent de voler comme un oiseau. Le vieux qui m\'a dit ça était à moitié fou et en train de clamser, mais depuis le temps j\'ai fait mes recherches et je suis convaincu qu\'il avait raison.\\nJe ne crois pas que le créateur de ces ailes ait fait ça spécifiquement pour faciliter la fraude, mais si mes informations sont exactes, ça permettrait de contourner les valideurs de n\'importe quelle station... Si ça t\'intéresse, je peux te faire une copie des notes que je possède. Elles ne sont pas très précises, mais un aventurier motivé devrait venir à bout de leurs mystères..."'
# TODO créer et lancer la quête Trouver l'élytre
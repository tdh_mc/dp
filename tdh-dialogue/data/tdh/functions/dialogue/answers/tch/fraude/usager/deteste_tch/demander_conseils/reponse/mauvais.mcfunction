# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Il y a des lignes et des stations où c\'est nettement plus facile. Le tout, c\'est de les trouver... Moins il y a d\'agents et de valideurs, et plus les équipements ont l\'air miteux, et moins il y a de chances de se faire avoir... En principe !"'

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Ce n\'est pas très courtois, mais le plus simple reste encore de tabasser un voyageur et de lui voler ses tickets. Il va sans dire qu\'il vaut mieux faire ça en-dehors d\'une station... mais au moins, vous n\'aurez pas de problème si vous croisez des contrôleurs !"'

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Les voyageurs jettent souvent leur ticket par terre après avoir fini leur voyage, même s\'il est encore valide. TCH essaie de régler le problème en installant des poubelles et en incitant les gens à y jeter leurs tickets, mais c\'est dur de changer les habitudes de milliers de gros porcs à la fois ! L\'avantage, c\'est que dans les grosses stations, il n\'y a parfois qu\'à se baisser pour pouvoir voyager en toute sérénité..."'

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Avec un bon sens du timing, vous pouvez toujours sauter sur un train en marche. Je le faisais souvent, quand j\'étais gamin : c\'est un peu dangereux, mais tellement meilleur marché !"'
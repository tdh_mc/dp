# On choisit une phrase pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Merci beaucoup pour ces conseils. Au revoir !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Merci infiniment pour ces paroles. À bientôt !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Encore une fois, ça fait plaisir. Bon vent, et que la fortune vous sourie !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Je ne sais pas comment te remercier ! Je dois partir maintenant, à la revoyure !"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Au revoir."'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"À bientôt !"'

# Fin de conversation
tag @s add AuRevoir
# On génère une réponse aléatoire du PNJ
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'"Une bonne méthode, c\'est de commencer son trajet sur une ligne de métro aérien. À Ygriak, par exemple, c\'est facile de rentrer en douce sur la ligne 6. Bon, il ne faut pas avoir peur de se blesser..."',cliquable:'[{"text":"Une bonne méthode, c\'est de commencer son trajet sur une ligne de métro aérien. À "},{"text":"Ygriak","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 431"},"hoverEvent":{"action":"show_text","value":{"text":"Parler d\'Ygriak"}}},{"text":", par exemple, c\'est facile de rentrer en douce sur la ligne 6. Bon, il ne faut pas avoir peur de se blesser..."}]'}
execute if score #random temp matches 0..24 run advancement grant @a[distance=..12,gamemode=!spectator] only tdh:dialogue/lieux/ygriak

execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Une bonne méthode pour éviter de payer, c\'est de ramper au-dessus de ces satanées portes. Parfois, il suffit même de s\'accroupir ! Essaie de faire ça aux heures creuses, voire même avant l\'ouverture des stations : plus c\'est tôt le matin, mieux c\'est !"'

execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"J\'ai entendu dire qu\'il y avait des puits d\'aération le long des tunnels de métro. Parfois même des sorties de secours... Si quelqu\'un voulait découvrir quelque chose, il lui suffirait de suivre cette piste."'

execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Ce n\'est pas tout de rentrer en douce, il faut encore ne pas se faire prendre. Quand tu marches dans les couloirs, assure-toi de voir les contrôleurs avant qu\'ils te voient, et de t\'entraîner à la course s\'ils te voient quand même et cherchent à te choper. S\'ils sont au bout du couloir, change de couloir. Et n\'aie pas peur de prendre les sens interdits, seuls les moutons se laissent guider."'
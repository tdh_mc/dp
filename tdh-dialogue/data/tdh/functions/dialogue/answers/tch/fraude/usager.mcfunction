# Pour le cas de l'usager qui déteste TCH et qui soutient les fraudeurs
# Si on a approuvé les critiques faites par l'usager à TCH
execute if score @p[tag=SpeakingToNPC] answer matches 200 run function tdh:dialogue/answers/tch/fraude/usager/deteste_tch/approuver
# Si on a demandé des conseils pour frauder
execute if score @p[tag=SpeakingToNPC] answer matches 300 run function tdh:dialogue/answers/tch/fraude/usager/deteste_tch/demander_conseils
# Si on a dit au revoir en remerciant l'usager
execute if score @p[tag=SpeakingToNPC] answer matches 399 run function tdh:dialogue/answers/tch/fraude/usager/deteste_tch/merci_au_revoir

# Si on a répondu que c'était honteux et qu'on allait partir
execute if score @p[tag=SpeakingToNPC] answer matches 100 run function tdh:dialogue/answers/tch/fraude/usager/deteste_tch/contredire
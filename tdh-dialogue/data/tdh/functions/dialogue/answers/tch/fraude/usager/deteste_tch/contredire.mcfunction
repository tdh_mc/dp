# On choisit une phrase pour la "question"
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '"Vos propos sont honteux ! Les gens comme vous n\'aiment vraiment pas la réussite !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Vous avez une dent contre TCH et ça se voit. Ça n\'en rend pas moins vos propos révoltants, séditieux et profondément orientés !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '"Vous êtes la honte de la société moderne ! Trouvez-vous un vrai travail au lieu de vociférer ce genre d\'ignominies !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '"Vous auriez dû mieux écouter à l\'école, si tant est que vous y soyez allé. Votre liberté s\'arrête là où commence celle des riches, et vu ce que vous en faites, ça ne me dérange pas du tout !"'

# On génère une réponse aléatoire du PNJ
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '"Va donc, eh, bourgeois."'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '"Allez, barre-toi, putain de capitaliste !"'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '"Quand ils auront fini avec les resquilleurs, c\'est les connards comme toi qu\'ils viendront chercher !"'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '"Ben alors casse-toi, pauvre con."'

# Fin de conversation
tag @s add AuRevoir
# On enregistre les données dans le storage
function tdh:dialogue/answers/tch/ticket/agent_tch/ticket_journee
data modify storage tdh:dialogue transaction.objet set value {nombre:"un ",unite:"carnet de tickets TCH"}
data modify storage tdh:dialogue transaction.item.Count set value 20b

# On enregistre le prix dans le score
scoreboard players set #transaction diamant 1

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue question set value '[{"text":"Je vais vous prendre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '[{"text":"Il me faudrait "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue question set value '[{"text":"On va partir sur "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" !"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue question set value '[{"text":"Juste "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", pour moi, s\'il vous plaît."}]'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Je peux faire autre chose pour vous ?"}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value '[{"text":"Voici "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" pour vous ! Y avait-il autre chose ?"}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value '[{"text":"Et voilà ! Vous faut-il autre chose ?"}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Si vous avez besoin de plus de "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":", vous savez où me trouver ! Vous aviez besoin d\'autre chose ?"}]'

# On enregistre une tournure aléatoire pour le refus qui sera utilisée SI on a un refus de transaction
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Je suis désolé, mais je crois que vous n\'avez pas assez de "},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"."}]'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Il va me falloir "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":" si vous voulez "},{"storage":"tdh:dialogue","nbt":"transaction.objet.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Je pense qu\'il vous manque "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"..."}]'
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Vous devez me payer "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":". Je suis navré, mais ce n\'est pas moi qui décide !"}]'

# Si on a répondu qu'on voulait bien acheter, on lance la sous-fonction correspondante
execute if score @p[tag=SpeakingToNPC] answer matches 100 run function tdh:dialogue/answers/tch/ticket/agent_tch/ticket_journee/1
execute if score @p[tag=SpeakingToNPC] answer matches 500 run function tdh:dialogue/answers/tch/ticket/agent_tch/ticket_journee/5
execute if score @p[tag=SpeakingToNPC] answer matches 2000 run function tdh:dialogue/answers/tch/ticket/agent_tch/ticket_journee/20

# Si on n'a pas pu récupérer de valeur, on dit qu'on ne veut rien finalement
execute as @s[tag=!Transaction] run function tdh:dialogue/answers/tch/ticket/agent_tch/no

# Sinon, on affiche les options
execute as @s[tag=Transaction] run function tdh:dialogue/answers/tch/ticket/agent_tch/autre_chose
# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue question set value '[{"text":"Et voilà "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue question set value '[{"text":"Voici "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":", comme prévu."}]'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Et voilà votre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Il vous suffit désormais de la recharger à une borne. Vous aviez besoin d\'autre chose ?"}]',cliquable:'[{"text":"Et voilà votre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" ! Il vous suffit désormais de la "},{"text":"recharger","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1520"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander comment recharger une carte Gold."}]}},{"text":" à une borne. Vous aviez besoin d\'autre chose ?"}]'}
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Vous n\'avez plus qu\'à recharger votre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" à une borne pour pouvoir voyager en toute sérénité ! Vous fallait-il autre chose ?"}]',cliquable:'[{"text":"Vous n\'avez plus qu\'à "},{"text":"recharger","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 1520"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander comment recharger une carte Gold."}]}},{"text":" votre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":" à une borne pour pouvoir voyager en toute sérénité ! Vous fallait-il autre chose ?"}]'}

# On enregistre une tournure aléatoire pour le refus qui sera utilisée SI on a un refus de transaction
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Il va me falloir ces "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":" si vous voulez votre "},{"storage":"tdh:dialogue","nbt":"transaction.objet.unite"},{"text":"."}]'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue transaction.echec set value '[{"text":"Je crois que vous oubliez les "},{"storage":"tdh:dialogue","nbt":"transaction.prix.nombre"},{"storage":"tdh:dialogue","nbt":"transaction.prix.unite"},{"text":"..."}]'

# On tag le joueur pour lequel on s'apprête à créer un abonnement
tag @p[tag=SpeakingToNPC] add tchAbonnementJoueur
# On génère un nouvel abonnement
function tch:abonnement/nouveau

# On enregistre les données de transaction dans le storage
data modify storage tdh:dialogue transaction.item set value {id:"minecraft:paper",Count:1,tag:{CustomModelData:110,display:{Name:'[{"text":"Carte ","color":"yellow"},{"text":"Gold","color":"gold"},{"text":" TCH"}]',Lore:["{\"text\":\"Rechargez cette carte d'abonnement\",\"color\":\"gray\"}","{\"text\":\"aux bornes de vente TCH.\",\"color\":\"gray\"}"]},tch:{type:2b,abonnement:0}}}
data modify storage tdh:dialogue transaction.item.tag.tch.abonnement set from storage tch:abonnement resultat.id
data modify storage tdh:dialogue transaction.objet set value {nombre:"une ",unite:"carte Gold"}

# On enregistre le prix dans le score
scoreboard players set #transaction fer 2

# On enregistre le fait qu'on va faire une transaction
tag @s add Transaction
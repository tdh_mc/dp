# On enregistre d'abord une tournure aléatoire pour la question
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..19 run data modify storage tdh:dialogue question set value '"Je vais y réfléchir..."'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue question set value '"En fait, je ferai ça plus tard."'
execute if score #random temp matches 40..59 run data modify storage tdh:dialogue question set value '"Finalement, je n\'en ai pas besoin pour l\'instant."'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue question set value '"Je crois que je viens de changer d\'avis."'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue question set value '"Peut-être une autre fois."'

# On enregistre une tournure aléatoire pour l'acceptation qui est le truc par défaut
function tdh:random

execute if score #random temp matches 0..49 run data modify storage tdh:dialogue reponse set value '"Pas de problème. N\'hésitez pas, si vous changez encore d\'avis !"'
execute if score #random temp matches 50..99 run data modify storage tdh:dialogue reponse set value '"Vous savez où me trouver s\'il vous en faut une."'

# On enregistre le fait qu'on va quitter le dialogue
tag @s add AuRevoir
# Si on a répondu qu'on voulait bien acheter, on lance la sous-fonction pour vérifier qu'on a les moyens
execute if score @p[tag=SpeakingToNPC] answer matches 1789 run function tdh:dialogue/answers/tch/abonnement/carte_gold/agent_tch/achat

# Sinon, fin de conversation
execute unless score @p[tag=SpeakingToNPC] answer matches 1789 run function tdh:dialogue/answers/tch/abonnement/carte_gold/agent_tch/no
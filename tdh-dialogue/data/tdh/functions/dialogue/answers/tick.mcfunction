# On se retrouve dans cette fonction si on a détecté que answer != 0

# En fonction du type de réponse attendue (le topic enregistré par le PNJ), on appelle une sous fonction

# 2 - Autre sujet (pour aborder un sujet spécifique mais depuis les answers)
execute if score @s topic matches 2 if score @p[tag=SpeakingToNPC] answer matches 1.. run function tdh:dialogue/answers/autre_sujet

# 380 - Manger (choisir un aliment à acheter)
execute if score @s[tag=Aubergiste] topic matches 380 run function tdh:dialogue/answers/manger/aubergiste
execute if score @s[tag=Tavernier] topic matches 380 run function tdh:dialogue/answers/manger/tavernier

# 381 - Boire (choisir un aliment à acheter)
execute if score @s[tag=Aubergiste] topic matches 381 run function tdh:dialogue/answers/boire/aubergiste
execute if score @s[tag=Tavernier] topic matches 381 run function tdh:dialogue/answers/boire/tavernier

# 383 - Torche (acheter la torche ou non // ou s'il essaie de nous convaincre de rester dormir ou boire à la place)
execute if score @s[tag=Aubergiste] topic matches 383 run function tdh:dialogue/answers/torche/aubergiste
execute if score @s[tag=Tavernier] topic matches 383 run function tdh:dialogue/answers/torche/tavernier

# 1100 - Achat de tickets (sélection du nombre de tickets à acheter)
execute if score @s[tag=AgentTCH] topic matches 1100 run function tdh:dialogue/answers/tch/ticket/agent_tch

# 1120 - Sauvette (si on parle avec un usager)
execute if score @s[tag=!AgentTCH] topic matches 1120 run function tdh:dialogue/answers/tch/sauvette/usager
# 1150 - Fraude (si on parle avec un usager)
execute if score @s[tag=!AgentTCH] topic matches 1150 run function tdh:dialogue/answers/tch/fraude/usager

# 1510 - Carte Gold (acheter / ne pas acheter la carte gold ?)
execute if score @s[tag=AgentTCH] topic matches 1510 run function tdh:dialogue/answers/tch/abonnement/carte_gold/agent_tch

# 2210 - Tabac (choisir un produit à acheter)
execute if score @s[tag=Buraliste] topic matches 2210 run function tdh:dialogue/answers/ressource/tabac/buraliste
execute if score @s[tag=Tavernier] topic matches 2210 run function tdh:dialogue/answers/ressource/tabac/tavernier

# 2220 - Herbe (choisir un produit à acheter)
execute if score @s[tag=Buraliste] topic matches 2220 run function tdh:dialogue/answers/ressource/herbe/buraliste
execute if score @s[tag=Tavernier] topic matches 2220 run function tdh:dialogue/answers/ressource/herbe/tavernier
execute if score @s[tag=Dealer] topic matches 2220 run function tdh:dialogue/answers/ressource/herbe/dealer


### AFFICHAGE DES LIGNES DE DIALOGUE
# Dans le cas où answer = -1, on quitte la conversation
execute if score @p[tag=SpeakingToNPC] answer matches -1 run function tdh:dialogue/topics/au_revoir
# On affiche les lignes de dialogue uniquement si on n'a pas une réponse générique
execute if score @p[tag=SpeakingToNPC] answer matches -1.. unless score @p[tag=SpeakingToNPC] answer matches 0 run function tdh:dialogue/affichage
# Dans le cas où answer = -4, on affiche l'écran de choix des lieux
execute if score @p[tag=SpeakingToNPC] answer matches -4 run function tdh:dialogue/topics/choose_lieu
# Dans le cas où answer = -2, on affiche l'écran de choix des topics
execute if score @p[tag=SpeakingToNPC] answer matches -2 run function tdh:dialogue/topics/choose_topic
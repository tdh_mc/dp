### AFFICHAGE DES LIGNES DE DIALOGUE
## Tags que peut avoir reçu le PNJ :
# - PasDeQuestion si le joueur ne dit rien
# - AuRevoir s'il termine la conversation
# - Options s'il faut afficher des options après le texte
# - ChoixLieu s'il faut afficher l'écran de sélection des lieux après le texte
# - TiersParle si une tierce personne (autre que le joueur et le PNJ principal de la conversation) parle
# - EntiteParle si une item frame doit sortir une phrase
# - EnAttente si le PNJ attend de dire la suite de ses phrases, et qu'on ne peut pas proposer de sujet
# - Transaction si le PNJ donne un objet au joueur (en utilisant un givespot de préférence)
#	=> Ne pas oublier de définir les scores #transaction fer/or/diamant/kubs selon les prix demandés

## Tags que peut posséder le joueur :
# - DialogueDebugLog s'il faut lui afficher le dump complet des données de storage.

## Tags que peut avoir reçu un autre PNJ :
# - TiersParlant pour la tierce personne qui dit la réplique additionnelle
# - EntiteImportante pour une personne dont un PNJ parle dans son discours. Utilisé éventuellement en combinaison avec un score @s temp-temp9 pour définir quelle personne correspond à quel emplacement du dialogue.

## Tags que peut avoir reçu une item frame :
# - EntiteParlante pour l'item frame qui sort des infos additionnelles

## Structure du storage des dialogues :
# {
#	question:'"La phrase prononcée par le joueur ?"',
#
#	reponse:{
#		basique:'"La phrase répondue par l'aubergiste (sans liens cliquables)"'
#		cliquable:'[{"text":"La phrase répondue par l'aubergiste (avec liens cliquables)"}]'
#	},
#	OU
#	reponse:'"La phrase répondue par l'aubergiste."' (si rien n'est cliquable)
#
# PEUVENT ÊTRE ABSENTS :
#
#	extra:?,	(des données supplémentaires requises pour l'affichage d'une phrase)
#
#	phrase_tiers:'"La phrase prononcée par le tiers."' (si rien n\'est cliquable)
#	OU
#	phrase_tiers:{
#		basique:'"La phrase prononcée par le tiers."' (sans liens cliquables)
#		cliquable:'[{"text":"La phrase prononcée par le tiers."}]' (avec liens cliquables)
#	},
#
#	phrase_entite:['"La phrase prononcée par l\'entité."'] (si rien n\'est cliquable)
#	OU
#	phrase_entite:[
#		{
#			basique:'"La phrase prononcée par l\'entité."' (sans liens cliquables)
#			cliquable:'[{"text":"La phrase prononcée par l\'entité."}]' (avec liens cliquables)
#		}
#	],
#
#	transaction:{
#		prix: {nombre:3, unite:"lingots de fer"}, (affiché en toutes lettres pour les messages tellraw)
#		item: {id:"minecraft:stone",Count:1b,tag:{<etc>}}, (l'objet devant être donné au joueur)
#		objet: {nombre:1, unite:"bloc de pierre"}, (affiché en toutes lettres pour les messages tellraw)
#		echec: '"La réplique à faire dire au PNJ si la transaction échoue."'
#	},
#	
#	options:[
#		<option 1>,
#		<option 2>,
#		...
#		<option N>
#	]
# }

# Si on n'a pas trouvé de topic cohérent (= pas de réponse générée pour le PNJ), on quitte la conversation
execute unless data storage tdh:dialogue reponse run function tdh:dialogue/topics/au_revoir

# On tag notre joueur pour afficher les lignes en jaune/cliquables uniquement à lui,
# et pas à tous les autres joueurs en dialogue
tag @p[tag=SpeakingToNPC,distance=0] add NotreJoueur

# On fait les calculs de notre transaction si besoin
execute as @s[tag=Transaction] run function tdh:dialogue/transaction/paiement

# On affiche le début du bloc de texte
function tdh:dialogue/affichage/debut_texte

# On affiche la question du joueur
execute as @s[tag=!PasDeQuestion] run function tdh:dialogue/affichage/question

# On affiche la réponse du PNJ
function tdh:dialogue/affichage/reponse

# On affiche la phrase additionnelle dite par un autre PNJ, s'il y en a une
execute as @s[tag=EntiteParle] as @e[tag=EntiteParlante] run function tdh:dialogue/affichage/phrase_entite 
execute as @s[tag=TiersParle] as @e[type=villager,tag=TiersParlant,distance=..20] run function tdh:dialogue/affichage/phrase_tiers

# On affiche les options s'il y en a
execute as @s[tag=Options,tag=!AuRevoir] run function tdh:dialogue/affichage/options

# On affiche la fin du bloc de texte
execute as @s[tag=!EnAttente] run function tdh:dialogue/affichage/fin_texte

# On affiche la sélection des lieux s'il y a lieu (^^^^^^)
execute as @s[tag=ChoixLieu,tag=!AuRevoir] run function tdh:dialogue/affichage/choix_lieu

# On affiche la sélection d'un autre sujet si on ne va pas quitter la conversation et qu'on n'a pas déjà d'autres options affichées (dans certains cas, il est quand même possible de cliquer sur des liens dans le message malgré la présence d'options, mais on n'affiche tout de même pas le champ "Choisir un autre sujet...")
execute as @s[tag=!AuRevoir,tag=!Options,tag=!EnAttente,tag=!ChoixLieu] run function tdh:dialogue/affichage/autre_sujet
# Si on doit terminer la conversation, on le fait
execute as @s[tag=AuRevoir] run function tdh:dialogue/end

# On supprime les tags des entités importantes
execute as @e[tag=EntiteImportante] run function tdh:dialogue/affichage/reset_entite_importante

# On supprime les tags temporaires
tag @s remove PasDeQuestion
tag @s remove Transaction
tag @s remove Options
tag @s remove ChoixLieu
tag @s remove TiersParle
tag @s remove EntiteParle
# On ne supprime pas EnAttente, qui le sera dès que les phrases supplémentaires auront été prononcées
# Cependant, si on est en attente, on bloque les deux variables triggerables du joueur
execute as @s[tag=EnAttente] run function tdh:dialogue/attente/debut

# On affiche le dump au joueur si c'est nécessaire
execute if entity @p[distance=0,tag=DialogueDebugLog] run function tdh:dialogue/affichage/debug

# On supprime le tag de notre joueur
tag @p[tag=NotreJoueur,distance=0] remove NotreJoueur

# On réinitialise le storage
data remove storage tdh:dialogue question
data remove storage tdh:dialogue reponse
data remove storage tdh:dialogue extra
data remove storage tdh:dialogue phrase_tiers
data remove storage tdh:dialogue phrase_entite
data remove storage tdh:dialogue options
data remove storage tdh:dialogue transaction
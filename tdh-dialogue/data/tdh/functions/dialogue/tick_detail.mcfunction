# Exécuté à la position de chaque joueur en cours de conversation avec un PNJ

# On donne un tag temporaire au PNJ avec lequel on est en train de parler
execute as @e[type=villager,tag=SpeakingToPlayer,distance=..20] if score @s dialogueID = @p[tag=SpeakingToNPC] dialogueID run tag @s add OurNPC

# On détecte simplement si une des deux variables de contrôle (topic & answer) est différente de 0
# Si c'est le cas, on lance la fonction de détection
# Il est important que answer soit calculé avant topics, car parfois answers redirige vers topics
execute unless score @p[tag=SpeakingToNPC] answer matches 0 as @e[type=villager,tag=OurNPC,tag=!EnAttente,sort=nearest,limit=1] run function tdh:dialogue/answers/tick
execute if score @p[tag=SpeakingToNPC] answer matches 0 unless score @p[tag=SpeakingToNPC] topic matches 0 as @e[type=villager,tag=OurNPC,tag=!EnAttente,sort=nearest,limit=1] run function tdh:dialogue/topics/tick

# Ensuite, on réinitialise les 2 variables de contrôle à 0
scoreboard players set @p[tag=SpeakingToNPC] topic 0
scoreboard players set @p[tag=SpeakingToNPC] answer 0

# Si on est en attente de la suite d'une phrase, on lance la fonction appropriée
# Il est important que ça s'exécute après les 2 assignations ci-dessus, puisqu'en fin de dialogue on pourra assigner de force un topic/answer au joueur pour revenir dans la boucle principale
execute as @e[type=villager,tag=OurNPC,tag=EnAttente,limit=1] run function tdh:dialogue/en_attente/tick

# Enfin, on supprime le tag temporaire du PNJ
tag @e[type=villager,tag=OurNPC] remove OurNPC
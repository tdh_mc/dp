# On réinitialise le scoreboard
scoreboard players reset @s dialogueID
scoreboard players reset @s topic
scoreboard players reset @s answer

# On réactive le PNJ et on lui retire ses tags
data merge entity @s[tag=!NoAI] {NoAI:0b}
tag @s remove SpeakingToPlayer
tag @s remove AuRevoir
tag @s remove EnAttente
tag @s remove Itineraire
tag @s remove NoAI

# On supprime les éventuelles variables temporaires sauvegardées par le PNJ
scoreboard players reset @s temp
scoreboard players reset @s temp2
scoreboard players reset @s temp3
scoreboard players reset @s temp4
scoreboard players reset @s temp5
scoreboard players reset @s temp6
scoreboard players reset @s temp7
scoreboard players reset @s temp8
scoreboard players reset @s temp9
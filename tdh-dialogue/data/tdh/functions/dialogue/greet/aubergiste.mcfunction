execute as @e[tag=Auberge,distance=..60,sort=nearest,limit=1] as @s[tag=NoChambre] run tag @p[tag=SpeakingToNPC] add NoChambreHere

execute if entity @e[tag=Auberge,distance=..60] unless entity @p[tag=NoChambreHere] run function tdh:dialogue/greet/aubergiste/special
execute if entity @e[tag=Auberge,distance=..60] if entity @p[tag=NoChambreHere] run function tdh:dialogue/greet/aubergiste/special_sans_chambre
execute as @s[tag=!GreetingsDone] run function tdh:dialogue/greet/aubergiste/generic

tag @a[tag=NoChambreHere] remove NoChambreHere
# Cette fonction est exécutée par le dealer lançant le dialogue,
# à la position du joueur participant au dialogue.

# On débloque les sujets correspondants (herbe)
advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/ressources drogue
advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/ressources herbe

# On enregistre le fait qu'on a émis un greeting
tag @s add GreetingsDone

# La phrase d'accroche
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Eh, tu fumes la beuh, un peu ?"}]',cliquable:'[{"text":"Eh, tu fumes la "},{"text":"beuh","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2220"},"hoverEvent":{"action":"show_text","value":{"text":"Demander de l\'herbe."}}},{"text":", un peu ?"}]'}
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Eh, ça te dirait un peu d\'herbe ?"}]',cliquable:'[{"text":"Eh, ça te dirait un peu d\'"},{"text":"herbe","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2220"},"hoverEvent":{"action":"show_text","value":{"text":"Demander de l\'herbe."}}},{"text":" ?"}]'}
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Eh, il te faudrait pas de l\'herbe, par hasard ?"}]',cliquable:'[{"text":"Eh, il te faudrait pas de l\'"},{"text":"herbe","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2220"},"hoverEvent":{"action":"show_text","value":{"text":"Demander de l\'herbe."}}},{"text":", par hasard ?"}]'}
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Eh, tu m\'as l\'air d\'avoir bien envie d\'un peu d\'herbe, toi."}]',cliquable:'[{"text":"Eh, tu m\'as l\'air d\'avoir bien envie d\'un peu d\'"},{"text":"herbe","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2220"},"hoverEvent":{"action":"show_text","value":{"text":"Demander de l\'herbe."}}},{"text":", toi."}]'}
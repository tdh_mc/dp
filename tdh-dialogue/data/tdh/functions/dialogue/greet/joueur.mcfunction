# On choisit une phrase pour la "question"
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 unless score #temps timeOfDay matches 2..3 run data modify storage tdh:dialogue question set value '"Bonjour !"'
execute if score #random temp matches 0..24 if score #temps timeOfDay matches 2..3 run data modify storage tdh:dialogue question set value '"Bonsoir !"'
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue question set value '"Salut !"'

# 1 chance sur 2 que ça soit l'autre qui engage la conversation
execute if score #random temp matches 50..99 run tag @s add PasDeQuestion
# Cette fonction est exécutée par le PNJ lançant le dialogue,
# à la position du joueur participant au dialogue.

# On enregistre le fait qu'on a émis un greeting
tag @s add GreetingsDone

# On enregistre les données d'affichage de notre station
execute as @e[tag=NomStation,sort=nearest,limit=1] run function tch:tag/station_pc
data modify storage tdh:dialogue extra.station set from entity @e[type=item_frame,tag=ourStationPC,limit=1] Item.tag.display.station
tag @e[type=item_frame,tag=ourStationPC] remove ourStationPC

# On débloque les sujets correspondants chez le joueur
# Cette commande débloque le mot-clé "tch" et tous les mots-clés de base de tch (acheter tickets, horaires, etc)
advancement grant @p[tag=SpeakingToNPC] only tdh:dialogue/tch

# On choisit une phrase au hasard pour le PNJ
# Si on n'a jamais parlé à un agent avant, on obtient tjrs une phrase de bienvenue spécifique (TODO)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..19 run data modify storage tdh:dialogue reponse set value '[{"text":"Bienvenue dans la station TCH "},{"storage":"tdh:dialogue","nbt":"extra.station.deDu"},{"storage":"tdh:dialogue","nbt":"extra.station.nom"},{"text":", "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" ! Puis-je vous renseigner ?"}]'
execute if score #random temp matches 20..39 run data modify storage tdh:dialogue reponse set value '[{"text":"Soyez le bienvenu "},{"storage":"tdh:dialogue","nbt":"extra.station.alAu"},{"storage":"tdh:dialogue","nbt":"extra.station.nom"},{"text":", "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" ! Vous faut-il un renseignement ?"}]'
execute if score #random temp matches 40..59 if score #temps timeOfDay matches 2..3 run data modify storage tdh:dialogue reponse set value '[{"text":"Bonsoir et bienvenue, "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" ! Toute l\'équipe "},{"storage":"tdh:dialogue","nbt":"extra.station.deDu"},{"storage":"tdh:dialogue","nbt":"extra.station.nom"},{"text":" est heureuse de vous accueillir. Vous avez besoin de quelque chose ?"}]'
execute if score #random temp matches 40..59 unless score #temps timeOfDay matches 2..3 run data modify storage tdh:dialogue reponse set value '[{"text":"Bonjour et bienvenue, "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" ! Toute l\'équipe "},{"storage":"tdh:dialogue","nbt":"extra.station.deDu"},{"storage":"tdh:dialogue","nbt":"extra.station.nom"},{"text":" est heureuse de vous accueillir. Vous avez besoin d\'un renseignement ?"}]'
execute if score #random temp matches 60..79 run data modify storage tdh:dialogue reponse set value '[{"text":"Je vous souhaite la bienvenue "},{"storage":"tdh:dialogue","nbt":"extra.station.alAu"},{"storage":"tdh:dialogue","nbt":"extra.station.nom"},{"text":", "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" ! Vous fallait-il un renseignement ?"}]'
execute if score #random temp matches 80..99 run data modify storage tdh:dialogue reponse set value '[{"text":"Bienvenue "},{"storage":"tdh:dialogue","nbt":"extra.station.alAu"},{"storage":"tdh:dialogue","nbt":"extra.station.nom"},{"text":", "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" ! Je peux vous renseigner ?"}]'


# On enregistre les différentes options
tag @s add Options
data modify storage tdh:dialogue options append value '[{"text":"J\'aurais besoin de tickets.","clickEvent":{"action":"run_command","value":"/trigger topic set 1100"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander à acheter des tickets."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Comment on fait, pour s\'abonner ?","clickEvent":{"action":"run_command","value":"/trigger topic set 1500"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander à prendre un abonnement."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Pourriez-vous me donner un itinéraire ?","clickEvent":{"action":"run_command","value":"/trigger topic set 1200"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander son chemin."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Quels sont les horaires des prochains trains ?","clickEvent":{"action":"run_command","value":"/trigger topic set 1400"},"hoverEvent":{"action":"show_text","value":[{"text":"Demander les prochains passages."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Vous pouvez me parler du réseau TCH ?","clickEvent":{"action":"run_command","value":"/trigger topic set 1300"},"hoverEvent":{"action":"show_text","value":[{"text":"Parler des lignes."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je voulais parler d\'autre chose...","clickEvent":{"action":"run_command","value":"/trigger topic set -2"},"hoverEvent":{"action":"show_text","value":[{"text":"Choisir un sujet débloqué de votre choix."}]}}]'
data modify storage tdh:dialogue options append value '[{"text":"Je n\'ai besoin de rien, merci.","clickEvent":{"action":"run_command","value":"/trigger topic set -1"},"hoverEvent":{"action":"show_text","value":[{"text":"Quitter la conversation."}]}}]'
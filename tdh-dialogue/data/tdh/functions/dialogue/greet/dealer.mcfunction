# S'il n'y a personne aux alentours, on propose de la drogue directement dans les greetings
execute positioned ~-12 ~-2 ~-12 if entity @e[type=villager,tag=!Dealer,dx=24,dy=4,dz=24,limit=1] run tag @s add GensPresents
execute as @s[tag=!GensPresents] run function tdh:dialogue/greet/dealer/generic
tag @s remove GensPresents

# Sinon, on exécute la fonction greetings générique car on se sent observé
# (donc on ne dit rien ici, on exécutera la fonction tout à l'heure)
# Fonction de greetings générique
# Cette fonction est exécutée par le PNJ lançant le dialogue,
# à la position du joueur participant au dialogue.

# On enregistre le fait qu'on a émis un greeting
tag @s add GreetingsDone

# Phrase d'accroche générique
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..9 run data modify storage tdh:dialogue reponse set value '"Que me voulez-vous ?"'
execute if score #random temp matches 10..19 run data modify storage tdh:dialogue reponse set value '"On se connaît ?"'
execute if score #random temp matches 20..29 run data modify storage tdh:dialogue reponse set value '"Il y a un problème ?"'
execute if score #random temp matches 30..39 run data modify storage tdh:dialogue reponse set value '"Qu\'est-ce que vous me voulez ?"'
execute if score #random temp matches 40..49 run data modify storage tdh:dialogue reponse set value '"Qu\'est-ce qui se passe ?"'
execute if score #random temp matches 50..59 run data modify storage tdh:dialogue reponse set value '"Bonjour ?"'
execute if score #random temp matches 60..69 run data modify storage tdh:dialogue reponse set value '"Comment allez-vous ?"'
execute if score #random temp matches 70..79 run data modify storage tdh:dialogue reponse set value '"J\'ai l\'impression de vous avoir déjà croisé..."'
execute if score #random temp matches 80..89 run data modify storage tdh:dialogue reponse set value '"Vous venez d\'arriver ?"'
execute if score #random temp matches 90..99 run data modify storage tdh:dialogue reponse set value '"Vous êtes qui, vous ?"'
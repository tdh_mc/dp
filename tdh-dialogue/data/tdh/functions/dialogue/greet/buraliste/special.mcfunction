# Cette fonction est exécutée par le buraliste lançant le dialogue,
# à la position du joueur participant au dialogue.

# On enregistre le fait qu'on a émis un greeting
tag @s add GreetingsDone

# On vérifie si on doit dire bonjour ou bonsoir
function tdh:dialogue/extra/bonjour
# On enregistre la seconde phrase (Proposer du tabac)
function tdh:dialogue/greet/buraliste/extra/tabac

# La phrase d'accroche
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue reponse set value {basique:'[{"storage":"tdh:dialogue","nbt":"extra.bonjour.maj"},{"text":" et bienvenue dans la tabagie "},{"nbt":"Item.tag.tabagie.prefixe","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"nbt":"Item.tag.tabagie.nom","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"text":", "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" ! "},{"storage":"tdh:dialogue","nbt":"extra.tabac.basique","interpret":true}]',cliquable:'[{"storage":"tdh:dialogue","nbt":"extra.bonjour.maj"},{"text":" et bienvenue dans la tabagie "},{"nbt":"Item.tag.tabagie.prefixe","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"nbt":"Item.tag.tabagie.nom","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"text":", "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":" ! "},{"storage":"tdh:dialogue","nbt":"extra.tabac.cliquable","interpret":true}]'}
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Je vous souhaite le "},{"storage":"tdh:dialogue","nbt":"extra.bonjour.min"},{"text":" ! "},{"storage":"tdh:dialogue","nbt":"extra.tabac.basique","interpret":true}]',cliquable:'[{"text":"Je vous souhaite le "},{"storage":"tdh:dialogue","nbt":"extra.bonjour.min"},{"text":" ! "},{"storage":"tdh:dialogue","nbt":"extra.tabac.cliquable","interpret":true}]'}
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Soyez le bienvenu dans l\'humble tabagie "},{"nbt":"Item.tag.tabagie.prefixe","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"nbt":"Item.tag.tabagie.nom","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"text":" ! "},{"storage":"tdh:dialogue","nbt":"extra.tabac.basique","interpret":true}]',cliquable:'[{"text":"Soyez le bienvenu dans l\'humble tabagie "},{"nbt":"Item.tag.tabagie.prefixe","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"nbt":"Item.tag.tabagie.nom","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"text":" ! "},{"storage":"tdh:dialogue","nbt":"extra.tabac.cliquable","interpret":true}]'}
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue reponse set value {basique:'[{"text":"Allez, approchez, ne soyez pas timides ! L\'on fait du bon tabac à la tabagie "},{"nbt":"Item.tag.tabagie.prefixe","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"nbt":"Item.tag.tabagie.nom","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"text":"... Oh, "},{"storage":"tdh:dialogue","nbt":"extra.bonjour.min"},{"text":", "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":", et bienvenue dans ma tabagie ! "},{"storage":"tdh:dialogue","nbt":"extra.tabac.basique","interpret":true}]',cliquable:'[{"text":"Allez, approchez, ne soyez pas timides ! L\'on fait du bon tabac à la tabagie "},{"nbt":"Item.tag.tabagie.prefixe","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"nbt":"Item.tag.tabagie.nom","entity":"@e[tag=Tabagie,sort=nearest,limit=1]"},{"text":"... Oh, "},{"storage":"tdh:dialogue","nbt":"extra.bonjour.min"},{"text":", "},{"selector":"@p[tag=SpeakingToNPC]"},{"text":", et bienvenue dans ma tabagie ! "},{"storage":"tdh:dialogue","nbt":"extra.tabac.cliquable","interpret":true}]'}
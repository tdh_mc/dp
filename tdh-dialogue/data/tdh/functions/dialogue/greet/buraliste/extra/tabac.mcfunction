# On débloque les sujets correspondants (tabac)
advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/ressources drogue
advancement grant @a[gamemode=!spectator,distance=..12] only tdh:dialogue/ressources tabac

# On enregistre la seconde phrase (Proposer du tabac)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

execute if score #random temp matches 0..24 run data modify storage tdh:dialogue extra.tabac set value {basique:'"Il vous faut du tabac ?"',cliquable:'[{"text":"Il vous faut du "},{"text":"tabac","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2210"},"hoverEvent":{"action":"show_text","value":{"text":"Demander du tabac."}}},{"text":" ?"}]'}
execute if score #random temp matches 25..49 run data modify storage tdh:dialogue extra.tabac set value {basique:'"Vous avez besoin de cigarettes ?"',cliquable:'[{"text":"Vous avez besoin de "},{"text":"cigarettes","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2210"},"hoverEvent":{"action":"show_text","value":{"text":"Demander du tabac."}}},{"text":" ?"}]'}
execute if score #random temp matches 50..74 run data modify storage tdh:dialogue extra.tabac set value {basique:'"Vous m\'avez l\'air d\'avoir bien besoin de quelques clopes !"',cliquable:'[{"text":"Vous m\'avez l\'air d\'avoir bien besoin de quelques "},{"text":"clopes","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2210"},"hoverEvent":{"action":"show_text","value":{"text":"Demander du tabac."}}},{"text":" !"}]'}
execute if score #random temp matches 75..99 run data modify storage tdh:dialogue extra.tabac set value {basique:'"Vous désirez fumer quelque chose ?"',cliquable:'[{"text":"Vous désirez "},{"text":"fumer","color":"gold","clickEvent":{"action":"run_command","value":"/trigger topic set 2210"},"hoverEvent":{"action":"show_text","value":{"text":"Demander du tabac."}}},{"text":" quelque chose ?"}]'}
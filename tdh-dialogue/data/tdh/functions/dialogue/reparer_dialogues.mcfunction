# ---- CORRECTION DU BUG DES DIALOGUES ----
# Si on ne réinitialise pas les offres des PNJ régulièrement, ils en génèrent de nouvelles quelles que soient les conditions
execute positioned as @a as @e[type=villager,distance=..20,tag=!NoDialogue,tag=!DialogueRepare] run function tdh:dialogue/reparer_dialogue
execute as @e[type=villager,tag=DialogueRepare] positioned as @s unless entity @p[distance=..40] run tag @s remove DialogueRepare
# Fin de conversation, exécutée à la position du joueur terminant la conversation
# On termine la conversation pour le PNJ
execute as @e[tag=SpeakingToPlayer] if score @s dialogueID = @p[tag=SpeakingToNPC] dialogueID run function tdh:dialogue/end_npc

# On affiche un message "fin de conversation"
function tdh:dialogue/affichage/fin_conversation

# On efface le titre éventuellement affiché au joueur
title @p[tag=SpeakingToNPC] times 0 0 0

# On nettoie les effets qui immobilisent le joueur
effect clear @p[tag=SpeakingToNPC] slowness
effect clear @p[tag=SpeakingToNPC] jump_boost
effect clear @p[tag=SpeakingToNPC] resistance

# On nettoie les variables scoreboard
scoreboard players reset @p[tag=SpeakingToNPC] topic
scoreboard players reset @p[tag=SpeakingToNPC] answer
scoreboard players reset @p[tag=SpeakingToNPC] dialogueID

# On supprime le tag du joueur
tag @p[tag=SpeakingToNPC] remove SpeakingToNPC
# On a déjà stocké le prix de la transaction dans #transaction fer/or/kubs/diamant

# On a une sous-fonction pour chacun des moyens de paiement non nuls qu'on aie
execute if score #transaction fer matches 1.. run function tdh:dialogue/transaction/paiement/fer
execute if score #transaction or matches 1.. run function tdh:dialogue/transaction/paiement/or
execute if score #transaction diamant matches 1.. run function tdh:dialogue/transaction/paiement/diamant
execute if score #transaction kubs matches 1.. run function tdh:dialogue/transaction/paiement/kubs

# Si on peut payer, on retire l'argent au joueur
execute as @s[tag=!PaiementRefuse] run function tdh:dialogue/transaction/paiement/succes
# Sinon, on génère une nouvelle réponse à la place de la réponse prévue
execute as @s[tag=PaiementRefuse] run function tdh:dialogue/transaction/paiement/echec

# On réinitialise les variables
scoreboard players reset #transaction fer
scoreboard players reset #transaction or
scoreboard players reset #transaction diamant
scoreboard players reset #transaction kubs
tag @s remove PaiementRefuse
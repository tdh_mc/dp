# On a déjà stocké le prix de la transaction dans #transaction fer/or/kubs/diamant

# On a une sous-fonction pour chacun des moyens de paiement non nuls qu'on aie
execute if score #transaction fer matches 1.. run function tdh:dialogue/transaction/paiement/succes/fer
execute if score #transaction or matches 1.. run function tdh:dialogue/transaction/paiement/succes/or
execute if score #transaction diamant matches 1.. run function tdh:dialogue/transaction/paiement/succes/diamant
execute if score #transaction kubs matches 1.. run function tdh:dialogue/transaction/paiement/succes/kubs

# S'il y a une item frame libre, on pose l'item dessus ; sinon, on fait à la position du joueur
execute if entity @e[tag=GiveSpot,distance=..4] run function tdh:dialogue/transaction/paiement/succes/don/give_spot
execute unless entity @e[tag=GiveSpot,distance=..4] run function tdh:dialogue/transaction/paiement/succes/don/no_give_spot
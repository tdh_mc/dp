# On retire 1 moyen de paiement au joueur et à la variable
clear @p[tag=SpeakingToNPC] gold_ingot 1
scoreboard players remove #transaction or 1

# S'il reste de l'or à payer, on continue
execute if score #transaction or matches 1.. run function tdh:dialogue/transaction/paiement/succes/or
# On retire 1 moyen de paiement au joueur et à la variable
clear @p[tag=SpeakingToNPC] diamond 1
scoreboard players remove #transaction diamant 1

# S'il reste du diamant à payer, on continue
execute if score #transaction diamant matches 1.. run function tdh:dialogue/transaction/paiement/succes/diamant
# On retire 1 moyen de paiement au joueur et à la variable
clear @p[tag=SpeakingToNPC] paper{kubs:1b} 1
scoreboard players remove #transaction kubs 1

# S'il reste du kub à payer, on continue
execute if score #transaction kubs matches 1.. run function tdh:dialogue/transaction/paiement/succes/kubs
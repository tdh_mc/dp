# On retire 1 moyen de paiement au joueur et à la variable
clear @p[tag=SpeakingToNPC] iron_ingot 1
scoreboard players remove #transaction fer 1

# S'il reste du fer à payer, on continue
execute if score #transaction fer matches 1.. run function tdh:dialogue/transaction/paiement/succes/fer
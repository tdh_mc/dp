# On empêche le PNJ de ramasser les items
function tdh:dialogue/fill_inventory

# On invoque l'item
execute at @e[tag=GiveSpot,distance=..4,sort=nearest,limit=1] run summon item ~ ~0.5 ~ {Tags:["ItemTransaction"],PickupDelay:25s,Item:{id:"minecraft:stone",Count:1b}}
# On fait en sorte que seul le joueur ayant fait la transaction puisse le ramasser
data modify entity @e[type=item,tag=ItemTransaction,distance=..8,limit=1] Owner set from entity @p[tag=SpeakingToNPC] UUID
# On récupère le bon item (type, nombre et tags spécifiques)
data modify entity @e[type=item,tag=ItemTransaction,distance=..8,limit=1] Item set from storage tdh:dialogue transaction.item

# On supprime le tag temporaire de l'item
tag @e[type=item,tag=ItemTransaction,distance=..8] remove ItemTransaction
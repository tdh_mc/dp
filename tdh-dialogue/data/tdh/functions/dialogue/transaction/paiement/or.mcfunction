# On enregistre le texte du moyen de paiement
execute if score #transaction or matches 1 run data modify storage tdh:dialogue transaction.prix set value {nombre:"un ",unite:"lingot d'or"}
execute if score #transaction or matches 2.. run data modify storage tdh:dialogue transaction.prix.unite set value "lingots d'or"
execute if score #transaction or matches 2 run data modify storage tdh:dialogue transaction.prix.nombre set value "deux "
execute if score #transaction or matches 3 run data modify storage tdh:dialogue transaction.prix.nombre set value "trois "
execute if score #transaction or matches 4 run data modify storage tdh:dialogue transaction.prix.nombre set value "quatre "
execute if score #transaction or matches 5 run data modify storage tdh:dialogue transaction.prix.nombre set value "cinq "
execute if score #transaction or matches 6 run data modify storage tdh:dialogue transaction.prix.nombre set value "six "
execute if score #transaction or matches 7 run data modify storage tdh:dialogue transaction.prix.nombre set value "sept "
execute if score #transaction or matches 8 run data modify storage tdh:dialogue transaction.prix.nombre set value "huit "
execute if score #transaction or matches 9 run data modify storage tdh:dialogue transaction.prix.nombre set value "neuf "
execute if score #transaction or matches 10 run data modify storage tdh:dialogue transaction.prix.nombre set value "dix "
execute if score #transaction or matches 11.. store result storage tdh:dialogue transaction.prix.nombre int 1 run scoreboard players get #transaction or

# On stocke la quantité de moyens de paiement du joueur
execute store result score #transaction temp run clear @p[tag=SpeakingToNPC] gold_ingot 0

# Si on ne peut pas payer, on se donne un tag
execute if score #transaction temp < #transaction or run tag @s add PaiementRefuse

# On réinitialise la variable temporaire
scoreboard players reset #transaction temp
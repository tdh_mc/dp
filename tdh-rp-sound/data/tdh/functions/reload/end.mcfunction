# Message broadcasté à tout le monde
execute at @r unless entity @p[distance=1..] run tellraw @a [{"text":"[SERVER] : Merci pour ta patience, "},{"selector":"@p"},{"text":" !"}]
execute at @r if entity @p[distance=1..] run tellraw @a [{"text":"[SERVER] : Merci pour votre patience, "},{"selector":"@a"},{"text":" !"}]

# On termine le son, et on joue le son de fin
stopsound @a master tdh:br.reload
playsound tdh:br.pause_end master @a
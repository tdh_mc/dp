# Message broadcasté à tout le monde
tellraw @a [{"text":"["},{"selector":"@r"},{"text":"] : Un p'tit "},{"text":"reload","bold":"true","color":"#ce3535"},{"text":", hein ?"}]

# Son joué lors du reload du serveur
playsound tdh:br.reload master @a ~ ~ ~ 999999 1 1

# Schedule pour arrêter tout ça dans quelques ticks (mais une fois le reload fini, du coup)
schedule function tdh:reload/end 3t


# Enfin, on reload le tout
reload
# Tick des sons d'oiseaux dans une forêt en automne (+ hiver non enneigé et parfois printemps)
# Selon l'heure de la journée
execute if score #temps currentTick matches 5000..8499 run function tdh:sound/oiseau/foret/automne/matin
execute if score #temps currentTick matches 8500..17499 run function tdh:sound/oiseau/foret/automne/jour
execute if score #temps currentTick matches 17500..19500 run function tdh:sound/oiseau/foret/automne/soir
execute unless score #temps currentTick matches 5000..19500 run function tdh:sound/oiseau/foret/automne/nuit
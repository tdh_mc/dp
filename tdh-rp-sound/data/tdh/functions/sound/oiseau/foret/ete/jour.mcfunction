# Tick des sons d'oiseaux en forêt en été, en journée

# On choisit un emplacement pour jouer le son
function tdh:sound/oiseau/invoquer_emetteur

# On joue le son au-dessus de l'emplacement indiqué
execute at @e[type=marker,tag=SonOiseauEmetteur,distance=..60,sort=nearest,limit=1] run playsound tdh:fx.birds.forest.spruce.day ambient @a[distance=..30] ~ ~4.5 ~ 1.5 1

# On détruit l'émetteur
kill @e[type=marker,tag=SonOiseauEmetteur,distance=..60]

# On change le temps d'attente
scoreboard players set @a[distance=..0] sonOiseauTicks -40
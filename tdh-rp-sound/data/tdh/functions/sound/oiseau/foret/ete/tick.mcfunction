# Tick des sons d'oiseaux dans une forêt en été (parfois un peu printemps)
# Selon l'heure de la journée
execute if score #temps currentTick matches 5000..8499 run function tdh:sound/oiseau/foret/ete/matin
execute if score #temps currentTick matches 8500..17499 run function tdh:sound/oiseau/foret/ete/jour
execute if score #temps currentTick matches 17500..19500 run function tdh:sound/oiseau/foret/ete/soir
execute unless score #temps currentTick matches 5000..19500 run function tdh:sound/oiseau/foret/ete/nuit
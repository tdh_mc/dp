# Tick des sons d'oiseaux dans une forêt en hiver enneigé
# Selon l'heure de la journée
execute if score #temps currentTick matches 5000..8499 run function tdh:sound/oiseau/foret/hiver/matin
execute if score #temps currentTick matches 8500..17499 run function tdh:sound/oiseau/foret/hiver/jour
execute if score #temps currentTick matches 17500..19000 run function tdh:sound/oiseau/foret/hiver/soir
execute unless score #temps currentTick matches 5000..19000 run function tdh:sound/oiseau/foret/hiver/nuit
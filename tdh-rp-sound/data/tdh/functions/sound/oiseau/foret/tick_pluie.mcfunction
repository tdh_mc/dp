# Tick des oiseaux dans la forêt

# Si on est au printemps ou en été
execute if score #temps dateMois matches 4..9 run function tdh:sound/oiseau/foret/ete/tick

# Si on est en automne ou en hiver, on utilise les sons automne
execute unless score #temps dateMois matches 4..9 run function tdh:sound/oiseau/foret/automne/tick
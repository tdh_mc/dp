# Tick des oiseaux dans la forêt enneigée

# Si on est en été
execute if score #temps dateMois matches 7..9 run function tdh:sound/oiseau/foret/ete/tick

# Si on est en automne ou au printemps
execute if score #temps dateMois matches 4..11 unless score #temps dateMois matches 7..9 run function tdh:sound/oiseau/foret/automne/tick

# Si on est en hiver
execute unless score #temps dateMois matches 4..11 run function tdh:sound/oiseau/foret/hiver/tick
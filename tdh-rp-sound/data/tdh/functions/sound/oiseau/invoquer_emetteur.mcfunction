# Pour éviter la commande spreadplayers (en raison du bug https://bugs.mojang.com/browse/MC-186963),
# on va plutôt prendre une variable aléatoire pour chaque axe (X, Y et Z) et l'additionner à la pos du joueur
# En premier lieu, on récupère la position du joueur dans des variables
data modify storage tdh:sound oiseau.position set from entity @p[distance=0] Pos
execute store result score #oiseau_centre posX run data get storage tdh:sound oiseau.position[0] 10
execute store result score #oiseau_centre posY run data get storage tdh:sound oiseau.position[1] 10
execute store result score #oiseau_centre posZ run data get storage tdh:sound oiseau.position[2] 10

# On invoque un objet pour figurer l'oiseau
summon marker ~ ~ ~ {Tags:["SonOiseauEmetteur","EmetteurTemp"]}
tellraw @a[tag=InsecteDebugLog] [{"text":"[Oiseau]","color":"yellow"},{"text":" Invocation d'un nouvel émetteur de son d'oiseau, ","color":"gray"},{"selector":"@e[type=marker,tag=EmetteurTemp,limit=1]"},{"text":".","color":"gray"}]

# On calcule une position aléatoire :
# - entre -10 et +10 blocs sur les deux axes horizontaux
# - entre -1 et +10 blocs sur l'axe vertical
# La position de référence est stockée dans #oiseau_centre posX/Y/Z **et multipliée par 10**
# On remplace au fur et à mesure la position dans le storage
scoreboard players operation #oiseau posX = #oiseau_centre posX
scoreboard players operation #oiseau posY = #oiseau_centre posY
scoreboard players operation #oiseau posZ = #oiseau_centre posZ
scoreboard players set #random min -100
scoreboard players set #random max 100
function tdh:random
execute store result storage tdh:sound oiseau.position[0] double 0.1 run scoreboard players operation #oiseau posX += #random temp
function tdh:random
execute store result storage tdh:sound oiseau.position[2] double 0.1 run scoreboard players operation #oiseau posZ += #random temp
scoreboard players set #random min -10
function tdh:random
execute store result storage tdh:sound oiseau.position[1] double 0.1 run scoreboard players operation #oiseau posY += #random temp

# On communique sa position au marqueur
data modify entity @e[type=marker,tag=EmetteurTemp,limit=1] Pos set from storage tdh:sound oiseau.position

# Si le bloc choisi est invalide, on supprime le marqueur
execute as @e[type=marker,tag=EmetteurTemp] at @s unless block ~ ~-1 ~ #tdh:base_oiseau unless block ~ ~ ~ #tdh:base_oiseau run kill @s

# Si on s'est tués, on affiche un message d'erreur ;
execute unless entity @e[type=marker,tag=EmetteurTemp,limit=1] run tellraw @a[tag=InsecteDebugLog] [{"text":"[Oiseau]","color":"red"},{"text":" L'émetteur a été supprimé.","color":"gray"}]
# Sinon, on affiche la position définie
execute if entity @e[type=marker,tag=EmetteurTemp,limit=1] run tellraw @a[tag=InsecteDebugLog] [{"text":"[Oiseau]","color":"yellow"},{"text":" Nouvelle position de l'émetteur : ","color":"gray"},{"storage":"tdh:sound","nbt":"oiseau.position"}]

# Sinon, on lui retire son tag temporaire
tag @e[type=marker,tag=EmetteurTemp] remove EmetteurTemp
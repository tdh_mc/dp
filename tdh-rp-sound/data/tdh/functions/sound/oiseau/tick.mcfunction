# Tick des sons d'oiseaux (exécuté une fois toutes les 5 secondes, plus lentement selon le biome)
# On réinitialise le timer des sons d'oiseaux
scoreboard players set @a[distance=..0] sonOiseauTicks -100


# Pour les biomes classiques (plaine...) s'il pleut
execute if predicate tdh:biome/plaine_sans_neige run function tdh:sound/oiseau/plaine/tick

# Pour les biomes forêt s'il pleut
execute if predicate tdh:biome/foret unless score @p biomeType matches 1 run function tdh:sound/oiseau/foret/tick_pluie

# Pour les biomes marais
execute if predicate tdh:biome/marais run function tdh:sound/oiseau/marais/tick

# Pour les biomes enneigés
execute if score @p biomeType matches 1 run function tdh:sound/oiseau/neige/tick

# Pour les biomes forêt s'il neige
execute if predicate tdh:biome/foret if score @p biomeType matches 1 run function tdh:sound/oiseau/foret/tick_neige


# On multiplie le temps d'attente par 3 s'il pleut, 10 s'il y a de l'orage
scoreboard players set #oiseau temp5 1
execute if score #meteo actuelle matches 2 run scoreboard players set #oiseau temp5 3
execute if score #meteo actuelle matches 3 run scoreboard players set #oiseau temp5 10
scoreboard players operation @a[distance=..0] sonOiseauTicks *= #oiseau temp5
scoreboard players reset #oiseau temp5
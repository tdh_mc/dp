# Tick des sons d'oiseaux en plaine
# Selon l'heure de la journée
execute if score #temps currentTick matches 5500..8999 run function tdh:sound/oiseau/plaine/matin
execute if score #temps currentTick matches 9000..17499 run function tdh:sound/oiseau/plaine/jour
execute if score #temps currentTick matches 17500..19500 run function tdh:sound/oiseau/plaine/soir
execute unless score #temps currentTick matches 5500..19500 run function tdh:sound/oiseau/plaine/nuit
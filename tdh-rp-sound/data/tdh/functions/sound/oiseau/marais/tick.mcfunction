# Tick des sons d'oiseaux dans un marais
# Selon l'heure de la journée
execute if score #temps currentTick matches 5000..8499 run function tdh:sound/oiseau/marais/matin
execute if score #temps currentTick matches 8500..17499 run function tdh:sound/oiseau/marais/jour
execute if score #temps currentTick matches 17500..19500 run function tdh:sound/oiseau/marais/soir
execute unless score #temps currentTick matches 5000..19500 run function tdh:sound/oiseau/marais/nuit
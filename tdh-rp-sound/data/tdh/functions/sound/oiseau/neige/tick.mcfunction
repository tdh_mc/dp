# Tick des sons d'oiseaux dans un biome neige
# Selon l'heure de la journée
execute if score #temps currentTick matches 5500..8499 run function tdh:sound/oiseau/neige/matin
execute if score #temps currentTick matches 8500..17499 run function tdh:sound/oiseau/neige/jour
execute if score #temps currentTick matches 17500..19000 run function tdh:sound/oiseau/neige/soir
execute unless score #temps currentTick matches 5500..19000 run function tdh:sound/oiseau/neige/nuit
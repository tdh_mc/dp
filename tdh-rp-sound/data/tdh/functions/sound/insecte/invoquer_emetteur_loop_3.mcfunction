# On commence par définir notre position
data modify entity @s Pos set from storage tdh:sound insecte.position

# On définit le temps d'attente à 0s (on commencera du coup immédiatement à jouer un son)
scoreboard players set @s sonInsecteTicks -1
# On enregistre le tick actuel (pour ne pas changer de loop si la période de la journée change)
scoreboard players operation @s currentTick = #temps currentTick
# On enregistre le nombre d'itérations avant de se supprimer (entre 10 et 30)
scoreboard players set #random min 10
scoreboard players set #random max 30
function tdh:random
scoreboard players operation @s remainingTicks = #random temp

# On retire le tag temporaire de l'émetteur
tag @s remove LoopInsecteTemp

# On tue l'émetteur s'il n'est pas proche d'un joueur
# ou si le bloc choisi n'est pas un bloc "naturel"
execute at @s run function tdh:sound/insecte/test_base_emetteur

# Si on ne s'est pas tués, on incrémente le compteur d'entités invoquées avec succès
execute if entity @s run scoreboard players add #insecte actuelle 1

# Si on s'est tués, on affiche un message d'erreur ;
execute unless entity @s run tellraw @a[tag=InsecteDebugLog] [{"text":"[Insecte]","color":"red"},{"text":" L'émetteur a été supprimé.","color":"gray"}]
# Sinon, on affiche la position définie
execute if entity @s run tellraw @a[tag=InsecteDebugLog] [{"text":"[Insecte]","color":"yellow"},{"text":" Nouvelle position de l'émetteur : ","color":"gray"},{"storage":"tdh:sound","nbt":"insecte.position"}]
# Selon le type de biome actuel, on joue éventuellement un son single

# Pour les biomes classiques (plaine...) s'il fait nuit
execute if predicate tdh:biome/plaine_sans_neige run function tdh:sound/insecte/single/plaine/tick

# Pour les biomes forêt non enneigés
execute if predicate tdh:biome/foret unless score @p biomeType matches 1 run function tdh:sound/insecte/single/foret/tick

# Pour les biomes marais
execute if predicate tdh:biome/marais run function tdh:sound/insecte/single/marais/tick

# Pour les biomes enneigés
execute if score @p biomeType matches 1 run function tdh:sound/insecte/single/neige/tick
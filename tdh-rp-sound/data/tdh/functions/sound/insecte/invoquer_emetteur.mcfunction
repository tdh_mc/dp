# Pour éviter la commande spreadplayers (en raison du bug https://bugs.mojang.com/browse/MC-186963),
# on va plutôt prendre une variable aléatoire pour chaque axe (X, Y et Z) et l'additionner à la pos du joueur
# En premier lieu, on récupère la position du joueur dans des variables
data modify storage tdh:sound insecte.position set from entity @p[distance=0] Pos
execute store result score #insecte_centre posX run data get storage tdh:sound insecte.position[0] 10
execute store result score #insecte_centre posY run data get storage tdh:sound insecte.position[1] 10
execute store result score #insecte_centre posZ run data get storage tdh:sound insecte.position[2] 10

# On invoque un objet pour figurer l'insecte
summon marker ~ ~ ~ {Tags:["SonInsecteEmetteur","EmetteurTemp"]}
tellraw @a[tag=InsecteDebugLog] [{"text":"[Insecte]","color":"yellow"},{"text":" Invocation d'un nouvel émetteur de son d'insecte, ","color":"gray"},{"selector":"@e[type=marker,tag=EmetteurTemp,limit=1]"},{"text":".","color":"gray"}]

# On calcule une position aléatoire :
# - entre -5 et +5 blocs sur les trois axes
# La position de référence est stockée dans #insecte_centre posX/Y/Z **et multipliée par 10**
# On remplace au fur et à mesure la position dans le storage
scoreboard players operation #insecte posX = #insecte_centre posX
scoreboard players operation #insecte posY = #insecte_centre posY
scoreboard players operation #insecte posZ = #insecte_centre posZ
scoreboard players set #random min -50
scoreboard players set #random max 50
function tdh:random
execute store result storage tdh:sound insecte.position[0] double 0.1 run scoreboard players operation #insecte posX += #random temp
function tdh:random
execute store result storage tdh:sound insecte.position[2] double 0.1 run scoreboard players operation #insecte posZ += #random temp
function tdh:random
execute store result storage tdh:sound insecte.position[1] double 0.1 run scoreboard players operation #insecte posY += #random temp

# On communique sa position au marqueur
data modify entity @e[type=marker,tag=EmetteurTemp,limit=1] Pos set from storage tdh:sound insecte.position

# Si le bloc choisi est invalide, on supprime le marqueur
execute as @e[type=marker,tag=EmetteurTemp] at @s run function tdh:sound/insecte/test_base_emetteur

# Si on s'est tués, on affiche un message d'erreur ;
execute unless entity @e[type=marker,tag=EmetteurTemp,limit=1] run tellraw @a[tag=InsecteDebugLog] [{"text":"[Insecte]","color":"red"},{"text":" L'émetteur a été supprimé.","color":"gray"}]
# Sinon, on affiche la position définie
execute if entity @e[type=marker,tag=EmetteurTemp,limit=1] run tellraw @a[tag=InsecteDebugLog] [{"text":"[Insecte]","color":"yellow"},{"text":" Nouvelle position de l'émetteur : ","color":"gray"},{"storage":"tdh:sound","nbt":"insecte.position"}]

# Sinon, on lui retire son tag temporaire
tag @e[type=marker,tag=EmetteurTemp] remove EmetteurTemp
# On exécute cette fonction en tant qu'un émetteur de loop d'insecte, à sa position

# S'il n'y a pas de joueur dans un rayon de 30 blocs, on se détruit
execute unless entity @p[distance=..30] run function tdh:sound/insecte/tuer_emetteur

# Si on n'a pas d'identifiant de boucle, on en génère un
execute unless score @s idBoucle matches 0.. run function tdh:sound/insecte/loop/gen_id

# On incrémente notre tick si on doit émettre un son
scoreboard players add @s[scores={idBoucle=1..}] sonInsecteTicks 1

# Si notre tick est à 0, on émet notre boucle
execute as @s[scores={idBoucle=1..,sonInsecteTicks=0..}] run function tdh:sound/insecte/loop/play
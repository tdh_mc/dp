tellraw @a[tag=InsecteDebugLog] [{"text":"=== Génération d'insectes autour du joueur ","color":"gold"},{"selector":"@p[distance=0]"},{"text":" ("},{"score":{"name":"#insecte","objective":"actuelle"}},{"text":" sur "},{"score":{"name":"#insecte","objective":"max"}},{"text":") ==="}]

# Pour éviter la commande spreadplayers (en raison du bug https://bugs.mojang.com/browse/MC-186963),
# on va plutôt prendre une variable aléatoire pour chaque axe (X, Y et Z) et l'additionner à la pos du joueur
# En premier lieu, on récupère la position du joueur dans des variables
data modify storage tdh:sound insecte.position set from entity @p[distance=0] Pos
execute store result score #insecte_centre posX run data get storage tdh:sound insecte.position[0] 10
execute store result score #insecte_centre posY run data get storage tdh:sound insecte.position[1] 10
execute store result score #insecte_centre posZ run data get storage tdh:sound insecte.position[2] 10
# On va ensuite, de manière répétée, invoquer une entité et moduler la position du joueur pour lui fournir une position aléatoire ; et ce jusqu'à avoir atteint le maximum d'entités (défini par #insecte max) avec le compteur de ces entités (défini par #insecte actuelle)
function tdh:sound/insecte/invoquer_emetteur_loop_2

# Avant de lancer la fonction tick_joueur pour le joueur à la position duquel cette fonction est exécutée,
# on vérifie si on n'est pas trop éloignés du sol pour jouer des sons d'insecte ;
# Si c'est le cas, on définit la variable des joueurs à cette position à un score + élevé que la normale, pour ne pas refaire le calcul des blocs trop fréquemment
execute if blocks ~ ~-10 ~ ~ ~ ~ ~ ~1 ~ masked run scoreboard players set @a[distance=0] sonInsecteTicks -24
execute if entity @p[distance=0,scores={sonInsecteTicks=0..}] run function tdh:sound/insecte/tick_joueur
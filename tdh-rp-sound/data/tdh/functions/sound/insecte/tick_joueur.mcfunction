# On réinitialise le timer des sons d'insectes
scoreboard players set @a[distance=..0] sonInsecteTicks -12

# On vérifie si on a assez d'émetteurs
# On enregistre le maximum pour notre biome
# 0 émetteur par défaut
scoreboard players set #insecte max 0
# 6 émetteurs dans les biomes plaines
execute if predicate tdh:biome/plaine_sans_neige run scoreboard players set #insecte max 6
# 12 émetteurs dans les biomes forêt sans neige
execute if predicate tdh:biome/foret run scoreboard players set #insecte max 12
# 24 émetteurs dans les biomes marais
execute if predicate tdh:biome/marais run scoreboard players set #insecte max 24
# 2 émetteurs dans les biomes neige
execute if score @p biomeType matches 1 run scoreboard players set #insecte max 2

# On retire 2 émetteurs s'il pleut, 3 s'il y a de l'orage
execute if score #insecte max matches 2.. if score #meteo actuelle matches 2 run scoreboard players remove #insecte max 2
execute if score #insecte max matches 3.. if score #meteo actuelle matches 3 run scoreboard players remove #insecte max 3

# On divise le nombre d'émetteurs par 2 si on est dans un village, et par 4 si on est en ville
# C'est une solution temporaire ; éventuellement (TODO) à terme on aura des biomes custom pour les villes et les villages, et pas besoin de se casser le cul à détecter des entités pour ça
scoreboard players set #insecte temp 1
execute positioned ~-60 0 ~-60 if entity @e[type=armor_stand,tag=NomVillage,dx=120,dy=256,dz=120] run scoreboard players set #insecte temp 2
execute positioned ~-120 0 ~-120 if entity @e[type=armor_stand,tag=NomVille,dx=240,dy=256,dz=240] run scoreboard players set #insecte temp 4
scoreboard players operation #insecte max /= #insecte temp
scoreboard players reset #insecte temp

# On compte le nombre d'émetteurs de loops d'insectes autour de nous
execute store result score #insecte actuelle if entity @e[type=marker,tag=LoopInsecteEmetteur,distance=..20]

# Si on en a assez, on diffuse éventuellement un son single
execute if score #insecte actuelle >= #insecte max run function tdh:sound/insecte/tick_single

# Si on n'a pas assez d'émetteurs, on en génère un nouveau
execute if score #insecte actuelle < #insecte max run function tdh:sound/insecte/invoquer_emetteur_loop


# On multiplie le temps d'attente par 2 s'il pleut, 4 s'il y a de l'orage
scoreboard players set #insecte temp5 1
execute if score #meteo actuelle matches 2 run scoreboard players set #insecte temp5 2
execute if score #meteo actuelle matches 3 run scoreboard players set #insecte temp5 4
scoreboard players operation @a[distance=..0] sonInsecteTicks *= #insecte temp5
scoreboard players reset #insecte temp5
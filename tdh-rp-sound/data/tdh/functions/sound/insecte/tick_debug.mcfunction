# On affiche des particules de debug sur chaque insecte / oiseau
execute at @e[type=marker,tag=LoopInsecteEmetteur] run particle dust 1 0 0 1 ~ ~.5 ~ 0 0 0 1 1 force @a[tag=InsecteDebugLog]
execute at @e[type=marker,tag=SonInsecteEmetteur] run particle dust 1 1 0 1 ~ ~.5 ~ 0 0 0 1 1 force @a[tag=InsecteDebugLog]
execute at @e[type=marker,tag=SonOiseauEmetteur] run particle dust 1 0 1 1 ~ ~.5 ~ 0 0 0 1 1 force @a[tag=InsecteDebugLog]
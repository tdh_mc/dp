# On joue notre boucle en fonction de notre ID de boucle

# Plaines (Jour 100-149 / Nuit 150-199)
execute if score @s idBoucle matches 150 run function tdh:sound/insecte/loop/play/plaine/50
execute if score @s idBoucle matches 151 run function tdh:sound/insecte/loop/play/plaine/51
execute if score @s idBoucle matches 152 run function tdh:sound/insecte/loop/play/plaine/52

# Neige (Jour 200-249 / Nuit 250-299)
execute if score @s idBoucle matches 200 run function tdh:sound/insecte/loop/play/neige/0

# Marais (Jour 700-749 (pas de nuit))
execute if score @s idBoucle matches 700 run function tdh:sound/insecte/loop/play/marais/0
execute if score @s idBoucle matches 701 run function tdh:sound/insecte/loop/play/marais/1
execute if score @s idBoucle matches 702 run function tdh:sound/insecte/loop/play/marais/2
execute if score @s idBoucle matches 703 run function tdh:sound/insecte/loop/play/marais/3
execute if score @s idBoucle matches 704 run function tdh:sound/insecte/loop/play/marais/4
execute if score @s idBoucle matches 705 run function tdh:sound/insecte/loop/play/marais/5

# Forêt été (Jour 300-329 / Nuit 330-349)
execute if score @s idBoucle matches 300 run function tdh:sound/insecte/loop/play/foret_ete/0
execute if score @s idBoucle matches 301 run function tdh:sound/insecte/loop/play/foret_ete/1
execute if score @s idBoucle matches 302 run function tdh:sound/insecte/loop/play/foret_ete/2
execute if score @s idBoucle matches 303 run function tdh:sound/insecte/loop/play/foret_ete/3
execute if score @s idBoucle matches 304 run function tdh:sound/insecte/loop/play/foret_ete/4
execute if score @s idBoucle matches 305 run function tdh:sound/insecte/loop/play/foret_ete/5
execute if score @s idBoucle matches 306 run function tdh:sound/insecte/loop/play/foret_ete/6
execute if score @s idBoucle matches 307 run function tdh:sound/insecte/loop/play/foret_ete/7
execute if score @s idBoucle matches 308 run function tdh:sound/insecte/loop/play/foret_ete/8
execute if score @s idBoucle matches 309 run function tdh:sound/insecte/loop/play/foret_ete/9
execute if score @s idBoucle matches 310 run function tdh:sound/insecte/loop/play/foret_ete/10
execute if score @s idBoucle matches 311 run function tdh:sound/insecte/loop/play/foret_ete/11
execute if score @s idBoucle matches 312 run function tdh:sound/insecte/loop/play/foret_ete/12
execute if score @s idBoucle matches 313 run function tdh:sound/insecte/loop/play/foret_ete/13
execute if score @s idBoucle matches 314 run function tdh:sound/insecte/loop/play/foret_ete/14
execute if score @s idBoucle matches 315 run function tdh:sound/insecte/loop/play/foret_ete/15

execute if score @s idBoucle matches 330 run function tdh:sound/insecte/loop/play/foret_ete/30
execute if score @s idBoucle matches 331 run function tdh:sound/insecte/loop/play/foret_ete/31

# Forêt hiver (Jour 350-379 / Nuit 380-399)
execute if score @s idBoucle matches 350 run function tdh:sound/insecte/loop/play/foret_hiver/0
execute if score @s idBoucle matches 351 run function tdh:sound/insecte/loop/play/foret_hiver/1
execute if score @s idBoucle matches 352 run function tdh:sound/insecte/loop/play/foret_hiver/2
execute if score @s idBoucle matches 353 run function tdh:sound/insecte/loop/play/foret_hiver/3

execute if score @s idBoucle matches 380 run function tdh:sound/insecte/loop/play/foret_hiver/30

# Forêt nuit (450-499)
execute if score @s idBoucle matches 450 run function tdh:sound/insecte/loop/play/foret_nuit/0
execute if score @s idBoucle matches 451 run function tdh:sound/insecte/loop/play/foret_nuit/1
execute if score @s idBoucle matches 452 run function tdh:sound/insecte/loop/play/foret_nuit/2
execute if score @s idBoucle matches 453 run function tdh:sound/insecte/loop/play/foret_nuit/3


# On décrémente le nombre de loops à faire avant de mourir
scoreboard players remove @s remainingTicks 1

# Si on a fini, on se tue
execute if score @s remainingTicks matches ..0 run function tdh:sound/insecte/tuer_emetteur

# S'il nous reste des loops à faire, on se déplace
execute if score @s remainingTicks matches 1.. rotated as @e[type=#tdh:random,sort=random,limit=1] run tp @s ^ ^ ^0.5
# On joue le son
execute if score @s remainingTicks matches 10.. run playsound tdh:fx.insects.forest.spruce.day.loop6 ambient @a[distance=..30] ~ ~ ~ 0.5 1 0.05
execute if score @s remainingTicks matches 7..9 run playsound tdh:fx.insects.forest.spruce.day.loop6 ambient @a[distance=..30] ~ ~ ~ 0.4 1 0.04
execute if score @s remainingTicks matches 4..6 run playsound tdh:fx.insects.forest.spruce.day.loop6 ambient @a[distance=..30] ~ ~ ~ 0.3 1 0.03
execute if score @s remainingTicks matches 2..3 run playsound tdh:fx.insects.forest.spruce.day.loop6 ambient @a[distance=..30] ~ ~ ~ 0.2 1 0.02
execute if score @s remainingTicks matches ..1 run playsound tdh:fx.insects.forest.spruce.day.loop6 ambient @a[distance=..30] ~ ~ ~ 0.1 1 0.01

# On enregistre la durée du son
scoreboard players set @s sonInsecteTicks -23
# On génère un identifiant de boucle
# Le maximum dépend du type de biome

# Par défaut, notre ID de boucle vaudra 0 (cela signifie une absence de son et de calcul)
scoreboard players set @s min 0
scoreboard players set @s max 0

# Les biomes forêt non enneigés
execute if predicate tdh:biome/foret run function tdh:sound/insecte/loop/gen_id/foret
# Les biomes enneigés
#execute if predicate tdh:biome/neige run function tdh:sound/insecte/loop/gen_id/neige
# Les biomes marais
execute if predicate tdh:biome/marais run function tdh:sound/insecte/loop/gen_id/marais
# Les biomes classiques
execute if predicate tdh:biome/plaine run function tdh:sound/insecte/loop/gen_id/plaine


# On a à présent min et max = les bornes de l'ID
# On retire min de max pour avoir notre modulo
scoreboard players operation @s max -= @s min
scoreboard players add @s max 1
# On prend une variable aléatoire
execute store result score @s idBoucle run data get entity @e[type=#tdh:random,sort=random,limit=1] Pos[2]
scoreboard players operation @s idBoucle %= @s max
scoreboard players operation @s idBoucle += @s min

# Notre ID de boucle est à présent généré
# On supprime les variables temporaires
scoreboard players reset @s max
scoreboard players reset @s min
scoreboard players reset @s currentTick

# Si on n'a toujours pas d'ID de boucle à ce stade, on se détruit pour éviter de garder une entité inutile
execute unless score @s idBoucle matches 1.. run function tdh:sound/insecte/tuer_emetteur
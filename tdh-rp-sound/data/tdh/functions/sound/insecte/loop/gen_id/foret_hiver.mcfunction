# Selon l'heure de la journée
execute if score @s currentTick matches 8500..18999 run function tdh:sound/insecte/loop/gen_id/foret_hiver_jour
execute if score @s currentTick matches 5500..8499 run function tdh:sound/insecte/loop/gen_id/foret_hiver_matin
execute unless score @s currentTick matches 5500..18999 run function tdh:sound/insecte/loop/gen_id/foret_nuit
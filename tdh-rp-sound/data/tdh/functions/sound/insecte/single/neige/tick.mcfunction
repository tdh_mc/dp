# Selon l'horaire, on diffuse des sons d'insectes différents
execute unless score #temps currentTick matches 6000..18000 run function tdh:sound/insecte/single/neige/nuit
execute if score #temps currentTick matches 6000..18000 run function tdh:sound/insecte/single/neige/jour

# On génère un temps d'attente aléatoire entre 1 et 5s
execute store result score #insecte temp run data get entity @e[type=#tdh:random,sort=random,limit=1] Position[2]
scoreboard players set #insecte temp2 80
scoreboard players operation #insecte temp %= #insecte temp2
scoreboard players operation #insecte temp -= #insecte temp2
scoreboard players remove #insecte temp 20
scoreboard players operation @a[distance=..0] sonInsecteTicks = #insecte temp
scoreboard players reset #insecte temp 
scoreboard players reset #insecte temp2

# Selon la saison, on rallonge plus ou moins le temps d'attente avant prochain son
execute unless score #temps dateMois matches 4..9 run scoreboard players remove @a[distance=..0] sonInsecteTicks 50
execute if score #temps dateMois matches 4..9 run scoreboard players remove @a[distance=..0] sonInsecteTicks 30
execute if score #temps dateMois matches 6..8 run scoreboard players remove @a[distance=..0] sonInsecteTicks 10

# Selon l'heure de la journée, on rallonge le temps d'attente
execute unless score #temps currentTick matches 7000..17000 run scoreboard players remove @a[distance=..0] sonInsecteTicks 10
execute unless score #temps currentTick matches 5000..19000 run scoreboard players remove @a[distance=..0] sonInsecteTicks 30
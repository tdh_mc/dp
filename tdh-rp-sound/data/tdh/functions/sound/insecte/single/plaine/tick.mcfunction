# Selon l'horaire, on diffuse ou non des sons d'insectes (seulement s'il fait nuit)
execute unless score #temps currentTick matches 6500..17500 run function tdh:sound/insecte/single/plaine/nuit

# On génère un temps d'attente aléatoire entre 2 et 7s
execute store result score #insecte temp run data get entity @e[type=#tdh:random,sort=random,limit=1] Position[2]
scoreboard players set #insecte temp2 100
scoreboard players operation #insecte temp %= #insecte temp2
scoreboard players operation #insecte temp -= #insecte temp2
scoreboard players remove #insecte temp 40
scoreboard players operation @a[distance=..0] sonInsecteTicks = #insecte temp
scoreboard players reset #insecte temp 
scoreboard players reset #insecte temp2

# Si le jour est presque levé ou pas encore couché, on rallonge le temps d'attente avant prochain son
execute if score #temps currentTick matches 5000..19000 run scoreboard players remove @a[distance=..0] sonInsecteTicks 50
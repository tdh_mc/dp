# On invoque un émetteur de son à une position aléatoire proche
function tdh:sound/insecte/invoquer_emetteur

# On joue le son légèrement en dessous de l'emplacement indiqué
execute at @e[type=marker,tag=SonInsecteEmetteur,distance=..30,sort=nearest,limit=1] run playsound tdh:fx.insects.forest.spruce.day.single ambient @a[distance=..30] ~ ~-0.5 ~ 0.5 1 0.05

# On détruit l'émetteur
kill @e[type=marker,tag=SonInsecteEmetteur,distance=..30]
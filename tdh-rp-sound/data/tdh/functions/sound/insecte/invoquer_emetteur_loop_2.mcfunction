# On invoque un objet pour figurer l'insecte
summon marker ~ ~ ~ {Tags:["LoopInsecteEmetteur","LoopInsecteTemp"]}
tellraw @a[tag=InsecteDebugLog] [{"text":"[Insecte]","color":"yellow"},{"text":" Invocation d'un nouvel émetteur de loop d'insecte, ","color":"gray"},{"selector":"@e[type=marker,tag=LoopInsecteTemp,limit=1]"},{"text":".","color":"gray"}]

# On calcule une position aléatoire :
# - entre -10 et +10 blocs sur les deux axes horizontaux
# - entre -5 et +5 blocs sur l'axe vertical
# La position de référence est stockée dans #insecte_centre posX/Y/Z **et multipliée par 10**
# On remplace au fur et à mesure la position dans le storage
scoreboard players operation #insecte posX = #insecte_centre posX
scoreboard players operation #insecte posY = #insecte_centre posY
scoreboard players operation #insecte posZ = #insecte_centre posZ
scoreboard players set #random min -100
scoreboard players set #random max 100
function tdh:random
execute store result storage tdh:sound insecte.position[0] double 0.1 run scoreboard players operation #insecte posX += #random temp
function tdh:random
execute store result storage tdh:sound insecte.position[2] double 0.1 run scoreboard players operation #insecte posZ += #random temp
scoreboard players set #random min -50
scoreboard players set #random max 50
function tdh:random
execute store result storage tdh:sound insecte.position[1] double 0.1 run scoreboard players operation #insecte posY += #random temp

# On communique les variables nécessaires au marqueur
execute as @e[type=marker,tag=LoopInsecteTemp,limit=1] at @s run function tdh:sound/insecte/invoquer_emetteur_loop_3

# Si on n'a pas assez d'émetteurs, on en génère un nouveau
execute if score #insecte actuelle < #insecte max run function tdh:sound/insecte/invoquer_emetteur_loop_2
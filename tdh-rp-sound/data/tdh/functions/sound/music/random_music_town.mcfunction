# On donne un tag spécifique à X mobs, où X = le nombre de musiques town possibles
tag @e[type=!player,limit=16] add RandomMusic
scoreboard players set fakePlayer temp9 0
execute as @e[tag=RandomMusic] run function tdh:sound/music/random_temp_score

# On récupère aléatoirement un de ces nombres, qui correspond donc à la nouvelle musique jouée
scoreboard players operation @p[distance=0,tag=ourPlayer] musicID = @e[tag=RandomMusic,sort=random,limit=1] temp9

# On joue la musique correspondante + on définit la longueur de la musique
function tdh:sound/music/music_town


# Reset
function tdh:sound/music/random_temp_reset
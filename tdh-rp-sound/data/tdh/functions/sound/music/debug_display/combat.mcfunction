# On définit le type de musique
data modify storage tdh:sound debug.musique.type set value "combat"

# On définit le nom selon la valeur de la variable
execute if score @s musicID matches 0 run data modify storage tdh:sound debug.musique.morceau set value "combat1"
execute if score @s musicID matches 1 run data modify storage tdh:sound debug.musique.morceau set value "combat2"
execute if score @s musicID matches 2 run data modify storage tdh:sound debug.musique.morceau set value "combat3"
execute if score @s musicID matches 3 run data modify storage tdh:sound debug.musique.morceau set value "combat4"
execute if score @s musicID matches 4 run data modify storage tdh:sound debug.musique.morceau set value "combat5"
execute if score @s musicID matches 5 run data modify storage tdh:sound debug.musique.morceau set value "morro_choices"
execute if score @s musicID matches 6 run data modify storage tdh:sound debug.musique.morceau set value "morro_fanfare"
execute if score @s musicID matches 7 run data modify storage tdh:sound debug.musique.morceau set value "morro_suspense"
execute if score @s musicID matches 8 run data modify storage tdh:sound debug.musique.morceau set value "morro_unity"
execute if score @s musicID matches 9 run data modify storage tdh:sound debug.musique.morceau set value "knife1"
execute if score @s musicID matches 10 run data modify storage tdh:sound debug.musique.morceau set value "knife2"
execute if score @s musicID matches 11 run data modify storage tdh:sound debug.musique.morceau set value "lotr_minasmorgul"
execute if score @s musicID matches 12 run data modify storage tdh:sound debug.musique.morceau set value "procyens"
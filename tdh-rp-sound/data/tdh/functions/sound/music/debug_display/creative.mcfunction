# On définit le type de musique
data modify storage tdh:sound debug.musique.type set value "creative"

# On définit le nom selon la valeur de la variable
execute if score @s musicID matches 0 run data modify storage tdh:sound debug.musique.morceau set value "creative2"
execute if score @s musicID matches 1 run data modify storage tdh:sound debug.musique.morceau set value "creative3"
execute if score @s musicID matches 2 run data modify storage tdh:sound debug.musique.morceau set value "creative4"
execute if score @s musicID matches 3 run data modify storage tdh:sound debug.musique.morceau set value "reflexion"
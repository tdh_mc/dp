# On met dans une variable le type de musique devant être joué
# 1 = musique d'exploration (background)
# 2 = musique de combat
# 3 = musique de village/ville
# 4 = musique de donjon
# 5 = musique du nether
# 6 = musique de l'end
# 7 = musique de creative
scoreboard players set @p musicType 1
execute if entity @p[distance=0,tag=ourPlayer,gamemode=creative,tag=IsFlying] run scoreboard players set @p[distance=0,tag=ourPlayer] musicType 7
# tags pour la présence/absence d'un donjon
execute if entity @p[distance=0,tag=ourPlayer,tag=InTown] run scoreboard players set @p[distance=0,tag=ourPlayer] musicType 3
execute if entity @p[distance=0,tag=ourPlayer,tag=InVillage] run scoreboard players set @p[distance=0,tag=ourPlayer] musicType 3
# tags pour le nether et l'end

# On teste le combat ou non à la toute fin pour qu'il override toute autre situation (en ville/village, en fly...)
execute if entity @p[distance=0,tag=ourPlayer,tag=EnCombat] run scoreboard players set @p[distance=0,tag=ourPlayer] musicType 2


# On stoppe la musique actuelle
stopsound @p[distance=0,tag=ourPlayer] music

# En fonction du type de musique, on lance la sous fonction correspondante
execute if score @p[distance=0,tag=ourPlayer] musicType matches 1 run function tdh:sound/music/random_music_explore
execute if score @p[distance=0,tag=ourPlayer] musicType matches 2 run function tdh:sound/music/random_music_combat
execute if score @p[distance=0,tag=ourPlayer] musicType matches 3 run function tdh:sound/music/random_music_town
execute if score @p[distance=0,tag=ourPlayer] musicType matches 4 run function tdh:sound/music/random_music_dungeon
execute if score @p[distance=0,tag=ourPlayer] musicType matches 5 run function tdh:sound/music/random_music_nether
execute if score @p[distance=0,tag=ourPlayer] musicType matches 6 run function tdh:sound/music/random_music_end
execute if score @p[distance=0,tag=ourPlayer] musicType matches 7 run function tdh:sound/music/random_music_creative
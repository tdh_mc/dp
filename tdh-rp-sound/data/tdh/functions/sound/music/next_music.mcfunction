# On tag le joueur ayant besoin d'une nouvelle musique
tag @p[distance=0,scores={musicTicksLeft=..-200}] add ourPlayer

# Si on est en combat, on lance simplement la boucle une nouvelle fois :
execute if score @p[distance=0,tag=ourPlayer,tag=EnCombat] musicType matches 2 run tag @p[distance=..1,tag=EnCombat] add NewLoop

# Dans le cas général, on génère une nouvelle musique aléatoire
execute unless entity @p[distance=0,tag=ourPlayer,tag=NewLoop] run function tdh:sound/music/random_music

# Dans le cas où on boucle de nouveau, on lance la musique de combat à nouveau
execute if entity @p[distance=0,tag=ourPlayer,tag=NewLoop] run function tdh:sound/music/music_combat

# On supprime le tag temporaire
tag @a[tag=NewLoop] remove NewLoop
tag @a[tag=ourPlayer] remove ourPlayer
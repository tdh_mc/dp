# On décrémente le compteur de temps restant pour tous les joueurs
scoreboard players remove @a musicTicksLeft 1

# On réinitialise le score si on s'est déconnectés
#execute at @a[scores={disconnects=1..}] run function tdh:sound/music/tick/reset_disconnect

# On réinitialise le score si on est en fin de combat, ou en début de combat
execute at @a[scores={musicType=2},tag=!EnCombat,tag=!CombatEnd] run function tdh:sound/music/tick/combat_end
execute at @a[tag=EnCombat] unless score @p[tag=EnCombat] musicType matches 2 run function tdh:sound/music/tick/combat_begin
execute at @a[tag=EnCombat,tag=CombatEnd,scores={musicType=2}] run function tdh:sound/music/tick/combat_restart

# On stoppe la musique quand on arrive à -10s, si on n'est pas en combat
execute at @a[scores={musicTicksLeft=..-200},tag=!EnCombat] run function tdh:sound/music/tick/reset_end

# Pour les autres, on passe à la musique suivante
execute at @a[scores={musicTicksLeft=..-200}] run function tdh:sound/music/next_music

# Si l'on tient un CD 11 en main, on affiche les informations détaillées
#execute as @a[nbt={SelectedItem:{id:"minecraft:music_disc_11"}}] run function tdh:sound/music/debug_display

# Si l'on vient de jeter un CD 11, on passe à la musique suivante
#execute as @a unless score @s debugDiscDrops matches 0.. run scoreboard players set @s debugDiscDrops 0
#execute as @a[scores={debugDiscDrops=1..}] run function tdh:sound/music/skip
execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run playsound tdh:music.explore.day.forest1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3680
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run playsound tdh:music.explore.day.forest2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4100
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run playsound tdh:music.explore.day.sky1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4320
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run playsound tdh:music.explore.day.sky2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 6080
execute if score @p[distance=0,tag=ourPlayer] musicID matches 4 run playsound tdh:music.explore.day.sky3 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 4 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4060
execute if score @p[distance=0,tag=ourPlayer] musicID matches 5 run playsound tdh:music.explore.day.sky4 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 5 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4100
execute if score @p[distance=0,tag=ourPlayer] musicID matches 6 run playsound tdh:music.explore.day.sky5 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 6 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4200
execute if score @p[distance=0,tag=ourPlayer] musicID matches 7 run playsound tdh:music.explore.day.sky6 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 7 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 7700
execute if score @p[distance=0,tag=ourPlayer] musicID matches 8 run playsound tdh:music.explore.day.sky7 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 8 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4960
execute if score @p[distance=0,tag=ourPlayer] musicID matches 9 run playsound tdh:music.explore.day.sky8 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 9 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 6580
execute if score @p[distance=0,tag=ourPlayer] musicID matches 10 run playsound tdh:music.explore.day.sky9 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 10 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 10780
execute if score @p[distance=0,tag=ourPlayer] musicID matches 11 run playsound tdh:music.explore.day.sky10 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 11 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4760
execute if score @p[distance=0,tag=ourPlayer] musicID matches 12 run playsound tdh:music.explore.day.sky11 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 12 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4660
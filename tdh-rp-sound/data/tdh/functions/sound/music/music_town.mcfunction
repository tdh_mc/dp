execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run playsound tdh:music.town.calm1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3900
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run playsound tdh:music.town.joy1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3680
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run playsound tdh:music.town.joy2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2400
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run playsound tdh:music.town.lotr_hobbits music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3520
execute if score @p[distance=0,tag=ourPlayer] musicID matches 4 run playsound tdh:music.town.lotr_havens music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 4 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 7200
execute if score @p[distance=0,tag=ourPlayer] musicID matches 5 run playsound tdh:music.town.lotr_meetings music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 5 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3720
execute if score @p[distance=0,tag=ourPlayer] musicID matches 6 run playsound tdh:music.town.marche1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 6 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4180
execute if score @p[distance=0,tag=ourPlayer] musicID matches 7 run playsound tdh:music.town.town1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 7 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3640
execute if score @p[distance=0,tag=ourPlayer] musicID matches 8 run playsound tdh:music.town.town2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 8 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2980
execute if score @p[distance=0,tag=ourPlayer] musicID matches 9 run playsound tdh:music.town.town3 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 9 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4040
execute if score @p[distance=0,tag=ourPlayer] musicID matches 10 run playsound tdh:music.town.town4 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 10 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3860
execute if score @p[distance=0,tag=ourPlayer] musicID matches 11 run playsound tdh:music.town.town5 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 11 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3820
execute if score @p[distance=0,tag=ourPlayer] musicID matches 12 run playsound tdh:music.town.morro1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 12 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2340
execute if score @p[distance=0,tag=ourPlayer] musicID matches 13 run playsound tdh:music.town.morro2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 13 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3980
execute if score @p[distance=0,tag=ourPlayer] musicID matches 14 run playsound tdh:music.town.morro3 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 14 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3960
execute if score @p[distance=0,tag=ourPlayer] musicID matches 15 run playsound tdh:music.town.morro4 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 15 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4160
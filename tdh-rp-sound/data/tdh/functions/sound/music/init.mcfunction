scoreboard objectives add musicTicksLeft dummy "Ticks avant la fin de la musique"
scoreboard objectives add musicType dummy "Type de musique"
scoreboard objectives add musicID dummy "Identifiant de musique"

# Variables obsolètes
scoreboard objectives remove debugDiscDrops
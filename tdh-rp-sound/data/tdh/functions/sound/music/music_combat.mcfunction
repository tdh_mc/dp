execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run playsound tdh:music.combat.combat1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 1339
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run playsound tdh:music.combat.combat2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 1177
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run playsound tdh:music.combat.combat3 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 894
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run playsound tdh:music.combat.combat4 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 1669
execute if score @p[distance=0,tag=ourPlayer] musicID matches 4 run playsound tdh:music.combat.combat5 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 4 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2852
execute if score @p[distance=0,tag=ourPlayer] musicID matches 5 run playsound tdh:music.combat.morro_choices music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 5 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2258
execute if score @p[distance=0,tag=ourPlayer] musicID matches 6 run playsound tdh:music.combat.morro_fanfare music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 6 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2263
execute if score @p[distance=0,tag=ourPlayer] musicID matches 7 run playsound tdh:music.combat.morro_suspense music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 7 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2358
execute if score @p[distance=0,tag=ourPlayer] musicID matches 8 run playsound tdh:music.combat.morro_unity music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 8 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 1969
execute if score @p[distance=0,tag=ourPlayer] musicID matches 9 run playsound tdh:music.combat.knife1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 9 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 1640
execute if score @p[distance=0,tag=ourPlayer] musicID matches 10 run playsound tdh:music.combat.knife2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 10 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 1719
execute if score @p[distance=0,tag=ourPlayer] musicID matches 11 run playsound tdh:music.combat.lotr_minasmorgul music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 11 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2022
execute if score @p[distance=0,tag=ourPlayer] musicID matches 12 run stopsound @p[distance=0,tag=ourPlayer] music tdh:music.combat.procyens
execute if score @p[distance=0,tag=ourPlayer] musicID matches 12 run playsound tdh:music.combat.procyens music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 12 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 1700
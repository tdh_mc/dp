# On réinitialise la musique lorsqu'on change de niveau (car on a le jingle level up)
stopsound @p[gamemode=!spectator,distance=0] music
scoreboard players set @p[gamemode=!spectator,distance=0] musicTicksLeft 0
tag @p[scores={musicType=2},tag=!EnCombat] add CombatEnd

stopsound @p[tag=CombatEnd] music

execute if score @p[tag=CombatEnd] musicID matches 0 run playsound tdh:music.combat.combat1_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 1 run playsound tdh:music.combat.combat2_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 2 run playsound tdh:music.combat.combat3_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 3 run playsound tdh:music.combat.combat4_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 4 run playsound tdh:music.combat.combat5_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 5 run playsound tdh:music.combat.morro_choices_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 6 run playsound tdh:music.combat.morro_fanfare_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 7 run playsound tdh:music.combat.morro_suspense_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 8 run playsound tdh:music.combat.morro_unity_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 9 run playsound tdh:music.combat.knife1_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 10 run playsound tdh:music.combat.knife2_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 11 run playsound tdh:music.combat.lotr_minasmorgul_end music @p[tag=CombatEnd]
execute if score @p[tag=CombatEnd] musicID matches 12 run playsound tdh:music.combat.procyens_end music @p[tag=CombatEnd]

scoreboard players set @p[tag=CombatEnd] musicTicksLeft 240
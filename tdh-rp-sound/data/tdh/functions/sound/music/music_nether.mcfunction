execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run playsound tdh:music.nether.nether1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3300
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run playsound tdh:music.nether.nether2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3420
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run playsound tdh:music.nether.nether3 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4620
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run playsound tdh:music.nether.nether4 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 10460
execute if score @p[distance=0,tag=ourPlayer] musicID matches 4 run playsound tdh:music.nether.morro1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 4 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2520
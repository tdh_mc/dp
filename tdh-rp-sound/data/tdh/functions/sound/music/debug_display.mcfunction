# On définit les valeurs par défaut du storage
data modify storage tdh:sound debug.musique.type set value "aucun"
data modify storage tdh:sound debug.musique.morceau set value "non"

# On définit la bonne valeur pour ce storage selon nos variables
execute if score @s musicType matches 1 run function tdh:sound/music/debug_display/explore
execute if score @s musicType matches 2 run function tdh:sound/music/debug_display/combat
execute if score @s musicType matches 3 run function tdh:sound/music/debug_display/town
execute if score @s musicType matches 4 run function tdh:sound/music/debug_display/dungeon
execute if score @s musicType matches 5 run function tdh:sound/music/debug_display/nether
execute if score @s musicType matches 6 run function tdh:sound/music/debug_display/end
execute if score @s musicType matches 7 run function tdh:sound/music/debug_display/creative

# Enfin, on affiche le titre
title @s actionbar [{"text":"Groupe de musiques : ","color":"gold"},{"storage":"tdh:sound","nbt":"debug.musique.type","color":"red"},{"text":" — ID : ","color":"gold"},{"storage":"tdh:sound","nbt":"debug.musique.morceau","color":"red"},{"text":" (","color":"gold"},{"score":{"name":"@s","objective":"musicType"},"color":"yellow"},{"text":"/","color":"gold"},{"score":{"name":"@s","objective":"musicID"},"color":"yellow"},{"text":") — Temps restant : ","color":"gold"},{"score":{"name":"@s","objective":"musicTicksLeft"},"color":"red"},{"text":" ticks","color":"red"}]

# On enregistre le fait qu'on a affiché un titre
tag @a[distance=0] remove TitleTest
execute if score @p[distance=0,tag=ourPlayer] musicID matches 50 run playsound tdh:music.explore.night.morning1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 50 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4700
execute if score @p[distance=0,tag=ourPlayer] musicID matches 51 run playsound tdh:music.explore.night.ether1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 51 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4480
execute if score @p[distance=0,tag=ourPlayer] musicID matches 52 run playsound tdh:music.explore.night.ether2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 52 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2040
execute if score @p[distance=0,tag=ourPlayer] musicID matches 53 run playsound tdh:music.explore.night.ether3 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 53 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3120
execute if score @p[distance=0,tag=ourPlayer] musicID matches 54 run playsound tdh:music.explore.night.sky1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 54 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4540
execute if score @p[distance=0,tag=ourPlayer] musicID matches 55 run playsound tdh:music.explore.night.sky2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 55 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2480
execute if score @p[distance=0,tag=ourPlayer] musicID matches 56 run playsound tdh:music.explore.night.sky3 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 56 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 7300
execute if score @p[distance=0,tag=ourPlayer] musicID matches 57 run playsound tdh:music.explore.night.sky4 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 57 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 2620
execute if score @p[distance=0,tag=ourPlayer] musicID matches 58 run playsound tdh:music.explore.night.sky5 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 58 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 8780
execute if score @p[distance=0,tag=ourPlayer] musicID matches 59 run playsound tdh:music.explore.night.sky6 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 59 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 7940
execute if score @p[distance=0,tag=ourPlayer] musicID matches 60 run playsound tdh:music.explore.night.morro1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 60 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3780
execute if score @p[distance=0,tag=ourPlayer] musicID matches 61 run playsound tdh:music.explore.night.morro2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 61 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3740
execute if score @p[distance=0,tag=ourPlayer] musicID matches 62 run playsound tdh:music.explore.night.lotr1 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 62 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 3120
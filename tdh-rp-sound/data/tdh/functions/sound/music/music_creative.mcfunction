execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run playsound tdh:music.creative.creative2 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 0 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4820
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run playsound tdh:music.creative.creative3 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 1 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 7840
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run playsound tdh:music.creative.creative4 music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 2 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 4580
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run playsound tdh:music.creative.reflexion music @p[distance=0,tag=ourPlayer]
execute if score @p[distance=0,tag=ourPlayer] musicID matches 3 run scoreboard players set @p[distance=0,tag=ourPlayer] musicTicksLeft 5300
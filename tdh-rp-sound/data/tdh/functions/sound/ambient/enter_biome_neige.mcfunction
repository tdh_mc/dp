# Le jour
execute if score #temps currentTick matches 5500..18500 run playsound tdh:ambient.snow.day ambient @p ~ ~ ~ 0.54
# La nuit
execute unless score #temps currentTick matches 5500..18500 run playsound tdh:ambient.snow.night ambient @p ~ ~ ~ 0.54
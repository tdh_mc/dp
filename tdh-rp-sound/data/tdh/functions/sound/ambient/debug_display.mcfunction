# On définit les valeurs par défaut du storage
data modify storage tdh:sound debug.ambient.type_biome set value "vide"
data modify storage tdh:sound debug.ambient.type_biome_precedent set value "vide"

# On donne leur nom à ces deux items
# D'abord le type de biome actuel
execute if score @s biomeAmbType matches 1 run data modify storage tdh:sound debug.ambient.type_biome set value "désert"
execute if score @s biomeAmbType matches 2 run data modify storage tdh:sound debug.ambient.type_biome set value "forêt"
execute if score @s biomeAmbType matches 3 run data modify storage tdh:sound debug.ambient.type_biome set value "neige"
execute if score @s biomeAmbType matches 4 run data modify storage tdh:sound debug.ambient.type_biome set value "montagnes"
execute if score @s biomeAmbType matches 10 run data modify storage tdh:sound debug.ambient.type_biome set value "ville"
execute if score @s biomeAmbType matches 11 run data modify storage tdh:sound debug.ambient.type_biome set value "village"
execute if score @s biomeAmbType matches 12 run data modify storage tdh:sound debug.ambient.type_biome set value "donjon"

# Ensuite le type précédent
execute if score @s oldBiomeAmbType matches 1 run data modify storage tdh:sound debug.ambient.type_biome_precedent set value "désert"
execute if score @s oldBiomeAmbType matches 2 run data modify storage tdh:sound debug.ambient.type_biome_precedent set value "forêt"
execute if score @s oldBiomeAmbType matches 3 run data modify storage tdh:sound debug.ambient.type_biome_precedent set value "neige"
execute if score @s oldBiomeAmbType matches 4 run data modify storage tdh:sound debug.ambient.type_biome_precedent set value "montagnes"
execute if score @s oldBiomeAmbType matches 10 run data modify storage tdh:sound debug.ambient.type_biome_precedent set value "ville"
execute if score @s oldBiomeAmbType matches 11 run data modify storage tdh:sound debug.ambient.type_biome_precedent set value "village"
execute if score @s oldBiomeAmbType matches 12 run data modify storage tdh:sound debug.ambient.type_biome_precedent set value "donjon"


# Enfin, on affiche le titre
title @s actionbar [{"text":"Type d'ambiance : ","color":"gold"},{"storage":"tdh:sound","nbt":"debug.ambient.type_biome","color":"red"},{"text":" (précédent : ","color":"gold"},{"storage":"tdh:sound","nbt":"debug.ambient.type_biome_precedent","color":"yellow"},{"text":" — Tick du son d'ambiance : ","color":"gold"},{"score":{"name":"@s","objective":"timeSpentInBiome"},"color":"red"},{"text":" – Depuis dernière ambiance : ","color":"gold"},{"score":{"name":"@s","objective":"timeSinceLastAmb"},"color":"red"}]

# On enregistre le fait qu'on a affiché un titre
tag @s remove TitleTest
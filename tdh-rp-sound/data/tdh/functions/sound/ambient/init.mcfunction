#scoreboard objectives add nextBiomeJingle dummy "Prochaine ambiance musicale de biome"
#scoreboard objectives add nextBiomeJingleD dummy "Prochaine ambiance musicale de biome (N0 de jour)"
scoreboard objectives add biomeAmbType dummy "Type d'ambiance musicale de biome"
scoreboard objectives add oldBiomeAmbType dummy "Type précédent d'ambiance musicale de biome"
scoreboard objectives add timeSpentInBiome minecraft.custom:minecraft.play_time "Temps passé dans le type de biome actuel"
scoreboard objectives add timeSinceLastAmb minecraft.custom:minecraft.play_time "Temps écoulé depuis la dernière ambiance musicale de biome"

scoreboard players set #ambianceBiome on 1
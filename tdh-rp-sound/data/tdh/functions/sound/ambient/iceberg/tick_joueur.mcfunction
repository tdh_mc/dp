# On exécute cette fonction à la position d'un joueur qui se trouve dans un biome glacé
# On prend une variable aléatoire pour savoir si on va diffuser ou non
scoreboard players set #random min 0
scoreboard players set #random max 10
execute if score #random temp matches 2 run function tdh:sound/ambient/iceberg/diffuser
# On prend une nouvelle variable aléatoire pour le son à diffuser
scoreboard players set #random min 0
scoreboard players set #random max 3
function tdh:random

# On se positionne à une position aléatoire des alentours et on diffuse
execute if score #random temp matches 0 rotated as @e[type=#tdh:random,sort=random,limit=1] positioned ^1 ^-3 ^11 run playsound tdh:fx.ice.a ambient @a[distance=..30] ~ ~ ~ 1 1 0.1
execute if score #random temp matches 1 rotated as @e[type=#tdh:random,sort=random,limit=1] positioned ^-1 ^-2 ^8 run playsound tdh:fx.ice.b ambient @a[distance=..30] ~ ~ ~ 1 1 0.1
execute if score #random temp matches 2 rotated as @e[type=#tdh:random,sort=random,limit=1] positioned ^0.5 ^-7 ^17 run playsound tdh:fx.ice.c ambient @a[distance=..30] ~ ~ ~ 1 1 0.1
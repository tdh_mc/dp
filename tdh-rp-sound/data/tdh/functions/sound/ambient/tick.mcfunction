# ---- GESTION DES AMBIANCES DE BIOME ----
# Exécutée séparément à la position de chaque joueur, à chaque tick

# On enregistre la valeur précédente du type de biome
scoreboard players operation @p temp5 = @p biomeAmbType

# On calcule la nouvelle valeur
# (0 = default, 1 = désert/savane, 2 = forêt, 3 = neige, 4 = montagnes, 9 = ice spikes)
# (10 = ville, 11 = village, 12 = donjon)

# Par défaut on se donne le biome type 0 (plaines et autres lieux "basiques")
scoreboard players set @a[distance=0] biomeAmbType 0

# Biomes "naturels"
# 220..239 (désert & savane)
execute if predicate tdh:biome/desert run scoreboard players set @a[distance=0] biomeAmbType 1
# 100..139 sauf 133..135 (forêt)
execute if predicate tdh:biome/foret run scoreboard players set @a[distance=0] biomeAmbType 2
# 133..135 // 250 // 310..319 (snow)
execute if predicate tdh:biome/neige run scoreboard players set @a[distance=0] biomeAmbType 3
# 300..309 (mountains)
execute if predicate tdh:biome/montagne run scoreboard players set @a[distance=0] biomeAmbType 4

# Biomes "construits"
# Arrivée en ville (10)
scoreboard players set @a[distance=0,tag=InTown] biomeAmbType 10
# Arrivée dans un village (11)
scoreboard players set @a[distance=0,tag=InVillage] biomeAmbType 11
# Arrivée dans un donjon (12)
scoreboard players set @a[distance=0,tag=InDungeon] biomeAmbType 12

# Si on a fait un aller-retour (forêt->désert->forêt) et que le jingle du 2e (ici désert) ne s'est pas encore joué,
# on fait comme si rien ne s'était passé (et on ne re-joue pas de jingle du 1e (ici forêt))
execute if score @p biomeAmbType = @p oldBiomeAmbType if score @p timeSpentInBiome matches ..0 run function tdh:sound/ambient/cancel_switch

# Si le type du biome a changé, on enregistre l'ancien type à la place de l'ancien ancien
execute unless score @p temp5 = @p biomeAmbType run scoreboard players operation @a[distance=0] oldBiomeAmbType = @p temp5

# Si le type de biome a changé, on réinitialise le temps passé dans le biome
execute unless score @p temp5 = @p biomeAmbType run function tdh:sound/ambient/changed_biome

# Si on n'est pas en intérieur (insideness<12),
# et que le temps passé dans le biome est supérieur à 300 ticks (15 secondes),
# et qu'on n'est pas en train de voler, ni en combat,
# => on joue un jingle d'entrée dans le biome
execute at @p[distance=0,tag=!IsFlying,tag=!EnCombat] if score @p insideness matches ..12 if score @p timeSpentInBiome matches 0 run function tdh:sound/ambient/enter_biome_auto

# Si on est dans un biome comprenant des musiques d'exploration (biome naturel = <10),
# et que la musique va se terminer dans 15 secondes,
# et que le temps depuis la dernière ambiance sonore est d'au moins 10 minutes (12000 ticks),
# et que les conditions précédentes sont remplies,
# => on joue un jingle d'"entrée" dans le biome (une transition entre les 2 musiques, en réalité)
execute at @p[distance=0,tag=!IsFlying,tag=!EnCombat] if score @p insideness matches ..12 if score @p biomeAmbType matches 1.. if score @p timeSpentInBiome matches 12000.. if score @p timeSinceLastAmb matches 12000.. if score @p musicTicksLeft matches 16 if score @p biomeAmbType matches 1..9 run function tdh:sound/ambient/enter_biome_auto

# Si l'on tient un CD 13 en main, on affiche les informations détaillées
#execute as @a[nbt={SelectedItem:{id:"minecraft:music_disc_13"}}] run function tdh:sound/ambient/debug_display

# On réinitialise les variables temporaires
scoreboard players reset @p temp5
# Le jour
execute if score #temps currentTick matches 6000..18000 run playsound tdh:ambient.desert.day ambient @p ~ ~ ~ 0.54
# La nuit
execute unless score #temps currentTick matches 4500..19500 run playsound tdh:ambient.desert.night ambient @p ~ ~ ~ 0.54
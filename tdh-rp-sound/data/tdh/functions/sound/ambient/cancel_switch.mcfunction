# On exécute cette fonction si on a fait un aller-retour (forêt->désert->forêt)
# et que le jingle du 2e (ici désert) ne s'est pas encore joué,
# => on fait comme si rien ne s'était passé (et on ne re-joue pas de jingle du 1e (ici forêt))

scoreboard players set @p timeSpentInBiome 600
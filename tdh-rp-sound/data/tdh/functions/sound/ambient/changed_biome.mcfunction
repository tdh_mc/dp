# On réinitialise le temps passé dans le biome (on jouera un son à 100=dans 5 secondes)
scoreboard players set @p timeSpentInBiome -100

# On stoppe les éventuelles ambiances sonores du biome précédent
stopsound @p ambient tdh:ambient.snow.day
stopsound @p ambient tdh:ambient.snow.night
stopsound @p ambient tdh:ambient.desert.day
stopsound @p ambient tdh:ambient.desert.night
stopsound @p ambient tdh:ambient.forest.day.oak
stopsound @p ambient tdh:ambient.forest.day.birch
stopsound @p ambient tdh:ambient.forest.day.dark
stopsound @p ambient tdh:ambient.forest.day.spruce
stopsound @p ambient tdh:ambient.forest.night
stopsound @p ambient tdh:ambient.mountains.day
stopsound @p ambient tdh:ambient.mountains.night
# On enregistre qu'on vient de jouer un son
scoreboard players set @p timeSinceLastAmb 0

# On exécute les fonctions correspondantes à chaque biome
execute if score @p biomeAmbType matches 1 run function tdh:sound/ambient/enter_biome_desert
execute if score @p biomeAmbType matches 2 run function tdh:sound/ambient/enter_biome_foret
execute if score @p biomeAmbType matches 3 run function tdh:sound/ambient/enter_biome_neige
execute if score @p biomeAmbType matches 4 run function tdh:sound/ambient/enter_biome_montagnes
# On utilise biomeAmbType=10 => village pour l'instant, mais on utilisera town dès qu'on aura des events spécifiques
execute if score @p biomeAmbType matches 10 run function tdh:sound/ambient/enter_biome_village
execute if score @p biomeAmbType matches 11 run function tdh:sound/ambient/enter_biome_village
execute if score @p biomeAmbType matches 12 run function tdh:sound/ambient/enter_biome_donjon

# On stoppe la musique dans 4 secondes si on est :
# - pas en combat
# - à 0 ticks de présence dans le biome (= c'est un jingle d'entrée dans le biome et pas un jingle de transition ultérieur)
# - et qu'on fait ville>explore ou explore>ville
execute unless entity @p[distance=..1,tag=EnCombat] if score @p timeSpentInBiome matches 0 if score @p oldBiomeAmbType matches 10..19 if score @p biomeAmbType matches 0..9 run scoreboard players set @p musicTicksLeft 80
execute unless entity @p[distance=..1,tag=EnCombat] if score @p timeSpentInBiome matches 0 if score @p oldBiomeAmbType matches 0..9 if score @p biomeAmbType matches 10..19 run scoreboard players set @p musicTicksLeft 80

# Si on est dans un biome naturel, on définit le temps minimum avant de pouvoir entendre une nouvelle ambiance dans ce biome à 12000t (10 minutes)
#execute if score @p biomeAmbType matches ..9 run scoreboard players set @p timeSpentInBiome -12000

# Si on est dans un biome "non naturel", on n'a le son qu'une seule fois
#execute if score @p biomeAmbType matches 10.. run scoreboard players set @p timeSpentInBiome -999999999
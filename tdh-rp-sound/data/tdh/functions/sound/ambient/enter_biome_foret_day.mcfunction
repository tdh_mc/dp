#Biomes oak
execute if predicate tdh:biome/foret/chene run playsound tdh:ambient.forest.day.oak ambient @a[distance=0] ~ ~ ~ 0.54
#Biomes birch
execute if predicate tdh:biome/foret/bouleau run playsound tdh:ambient.forest.day.birch ambient @a[distance=0] ~ ~ ~ 0.54
#Biomes dark
execute if predicate tdh:biome/foret/humide run playsound tdh:ambient.forest.day.dark ambient @a[distance=0] ~ ~ ~ 0.54
#Biomes spruce
execute if predicate tdh:biome/foret/conifere run playsound tdh:ambient.forest.day.spruce ambient @a[distance=0] ~ ~ ~ 0.54
function tdh:sound/wind/bg/mountains_bg
execute if score #meteo intensite matches 0 run function tdh:sound/wind/bg/mountains_calm
execute if score #meteo intensite matches 1..2 run function tdh:sound/wind/bg/mountains_rain
execute if score #meteo intensite matches 3.. run function tdh:sound/wind/bg/mountains_storm
# On récupère un nombre aléatoire entre 0 et 199 pour savoir si on joue ou non la rafale
scoreboard players set #random min 0
scoreboard players set #random max 200
function tdh:random

# Selon la météo et l'intensité, on retranche des valeurs ; si le nombre est > 0 la rafale sera jouée
execute if score #meteo intensite matches 2 run scoreboard players remove #random temp 100
execute if score #meteo intensite matches 3 run scoreboard players remove #random temp 50
execute if score #meteo actuelle matches 1 run scoreboard players remove #random temp 50
execute if score #meteo actuelle matches 2 run scoreboard players remove #random temp 30

# On exécute ensuite une des sous fonctions
execute if score #random temp matches 0.. if predicate tdh:biome/plage run function tdh:sound/wind/rafale/beach
execute if score #random temp matches 0.. if predicate tdh:biome/ocean run function tdh:sound/wind/rafale/beach

execute if score #random temp matches 0.. if predicate tdh:biome/marais run function tdh:sound/wind/rafale/swamp

execute if score #random temp matches -10.. if predicate tdh:biome/foret run function tdh:sound/wind/rafale/forest

execute if score #random temp matches 20.. if predicate tdh:biome/plaine_sans_neige run function tdh:sound/wind/rafale/plains

execute if score #random temp matches 0.. if predicate tdh:biome/neige run function tdh:sound/wind/rafale/snow

execute if score #random temp matches -20.. if predicate tdh:biome/montagne run function tdh:sound/wind/rafale/mountains

# On récupère un temps d'attente aléatoire pour la prochaine rafale
function tdh:random
scoreboard players remove #random temp 100
scoreboard players operation @a[distance=0] sonRafaleTicks = #random temp

# On réinitialise la variable temporaire
scoreboard players reset @p temp6
execute if predicate tdh:biome/plage run function tdh:sound/wind/bg/beach
execute if predicate tdh:biome/ocean run function tdh:sound/wind/bg/beach

execute if predicate tdh:biome/marais run function tdh:sound/wind/bg/swamp

execute if predicate tdh:biome/foret unless predicate tdh:biome/marais run function tdh:sound/wind/bg/forest

execute if predicate tdh:biome/plaine_sans_neige run function tdh:sound/wind/bg/plains

execute if predicate tdh:biome/neige unless predicate tdh:biome/montagne run function tdh:sound/wind/bg/snow

execute if predicate tdh:biome/montagne unless predicate tdh:biome/neige run function tdh:sound/wind/bg/hills
execute if predicate tdh:biome/montagne if predicate tdh:biome/neige run function tdh:sound/wind/bg/mountains

scoreboard players set @a[distance=0] sonVentTicks 0
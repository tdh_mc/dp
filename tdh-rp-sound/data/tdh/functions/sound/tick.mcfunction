# On fait les calculs des sons d'ambiance de biomes
execute if score #ambianceBiome on matches 1 at @a run function tdh:sound/ambient/tick

# On fait les calculs de sons de pluie (s'il y a de la pluie)
execute if score #meteo actuelle matches 2..3 at @a[scores={sonPluieTicks=10..}] run function tdh:sound/rain/tick

# On fait les calculs de sons de vent (s'il y a du vent)
execute if score #meteo intensite matches 1.. at @a[scores={sonVentTicks=19..}] run function tdh:sound/wind/tick_bg
# Les sons de rafale n'existent que si l'intensité est d'au moins 2
execute if score #meteo intensite matches 2.. at @a[scores={sonRafaleTicks=200..}] rotated as @e[type=#tdh:random,sort=random,limit=1] run function tdh:sound/wind/tick_rafale

# On fait les calculs de la musique
function tdh:sound/music/tick


# On diffuse un son d'oiseau aux joueurs qui doivent en entendre un
execute at @a[scores={sonOiseauTicks=0..,insideness=..15}] run function tdh:sound/oiseau/tick

# On vérifie régulièrement s'il manque des émetteurs d'insectes autour d'un joueur
execute at @a[scores={sonInsecteTicks=0..,insideness=..15}] run function tdh:sound/insecte/test_joueur
# On vérifie à chaque tick pour chaque émetteur de loop d'insectes si on doit émettre un nouveau son
execute as @e[type=marker,tag=LoopInsecteEmetteur] at @s run function tdh:sound/insecte/tick_loop

# Si le debug des oiseaux & insectes est activé, on affiche les particules correspondantes
execute if score #insecteDebug on matches 1 run function tdh:sound/insecte/tick_debug
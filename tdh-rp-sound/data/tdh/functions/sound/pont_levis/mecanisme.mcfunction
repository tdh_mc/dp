# On exécute d'abord à notre position
playsound tdh:fx.gears.many_loop1 block @a[distance=..70] ~ ~ ~ 3 1 0.03
playsound tdh:fx.gears.many_loop1 block @a[distance=..70] ~ ~-25 ~ 3 1 0.03

# Ensuite on exécute à l'éventuelle position des sound emitters de la machinerie
execute at @e[tag=MecanismePontLevis,distance=..50] run playsound tdh:fx.gears.single_loop1 block @a[distance=..30] ~ ~ ~ 1.2 1
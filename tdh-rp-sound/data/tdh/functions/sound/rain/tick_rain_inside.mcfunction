

execute if score @p insideness matches 4 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.08
execute if score @p insideness matches 5 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.24
execute if score @p insideness matches 6 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.36
execute if score @p insideness matches 7 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.48
execute if score @p insideness matches 8 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.56
execute if score @p insideness matches 9 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.62
execute if score @p insideness matches 10 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.70
execute if score @p insideness matches 11 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.77
execute if score @p insideness matches 12 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.84
execute if score @p insideness matches 13 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.89
execute if score @p insideness matches 14 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.93
execute if score @p insideness matches 15 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.97
execute if score @p insideness matches 16.. if score @p altitudeFromZero matches 64.. run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 1
execute if score @p insideness matches 16.. if score @p altitudeFromZero matches 54..63 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.75
execute if score @p insideness matches 16.. if score @p altitudeFromZero matches 44..53 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.5
execute if score @p insideness matches 16.. if score @p altitudeFromZero matches 34..43 run playsound tdh:fx.rain.interieur weather @a[distance=0] ~ ~ ~ 0.25
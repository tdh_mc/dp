

execute if score @p insideness matches 0 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 1
execute if score @p insideness matches 1 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.95
execute if score @p insideness matches 2 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.90
execute if score @p insideness matches 3 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.84
execute if score @p insideness matches 4 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.76
execute if score @p insideness matches 5 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.68
execute if score @p insideness matches 6 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.60
execute if score @p insideness matches 7 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.50
execute if score @p insideness matches 8 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.38
execute if score @p insideness matches 9 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.24
execute if score @p insideness matches 10 run playsound tdh:fx.rain.min weather @a[distance=..0.1] ~ ~ ~ 0.10


execute if score @p insideness matches 0 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 1
execute if score @p insideness matches 1 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.94
execute if score @p insideness matches 2 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.87
execute if score @p insideness matches 3 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.80
execute if score @p insideness matches 4 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.72
execute if score @p insideness matches 5 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.62
execute if score @p insideness matches 6 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.54
execute if score @p insideness matches 7 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.45
execute if score @p insideness matches 8 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.36
execute if score @p insideness matches 9 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.24
execute if score @p insideness matches 10 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.18
execute if score @p insideness matches 11 run playsound tdh:fx.rain.medium weather @a[distance=..0.1] ~ ~ ~ 0.09
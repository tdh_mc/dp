

execute if score @p insideness matches 0 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 1
execute if score @p insideness matches 1 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.95
execute if score @p insideness matches 2 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.88
execute if score @p insideness matches 3 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.82
execute if score @p insideness matches 4 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.73
execute if score @p insideness matches 5 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.64
execute if score @p insideness matches 6 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.54
execute if score @p insideness matches 7 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.46
execute if score @p insideness matches 8 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.38
execute if score @p insideness matches 9 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.30
execute if score @p insideness matches 10 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.24
execute if score @p insideness matches 11 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.16
execute if score @p insideness matches 12 run playsound tdh:fx.rain.max weather @a[distance=..0.1] ~ ~ ~ 0.08
# ---- GESTION DES AMBIANCES DE PLUIE ----
# Exécutée séparément à la position de chaque joueur, à chaque tick

# On calcule d'abord s'il pleut, neige ou rien
# On lance les fonctions correspondantes
execute if score @p biomeType matches 2..3 run function tdh:sound/rain/tick_rain
execute if score @p biomeType matches 1 run function tdh:sound/rain/tick_snow
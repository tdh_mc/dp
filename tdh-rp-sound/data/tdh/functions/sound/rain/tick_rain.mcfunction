execute if score #meteo intensite matches 2 if score @p insideness matches ..10 run function tdh:sound/rain/tick_rain_min
execute if score #meteo intensite matches 3 if score @p insideness matches ..11 run function tdh:sound/rain/tick_rain_med
execute if score #meteo intensite matches 4 if score @p insideness matches ..12 run function tdh:sound/rain/tick_rain_max
execute if score #meteo intensite matches 2.. if score @p insideness matches 4.. run function tdh:sound/rain/tick_rain_inside

scoreboard players set @a[distance=0] sonPluieTicks 0
scoreboard objectives add sonPluieTicks minecraft.custom:minecraft.play_time "Ticks de son de pluie écoulés"
scoreboard objectives add sonVentTicks minecraft.custom:minecraft.play_time "Ticks de son de vent écoulés"
scoreboard objectives add sonRafaleTicks minecraft.custom:minecraft.play_time "Ticks de son de rafale écoulés"

scoreboard objectives add sonOiseauTicks minecraft.custom:minecraft.play_time "Ticks avant le prochain son d'oiseau"
scoreboard objectives add sonInsecteTicks minecraft.custom:minecraft.play_time "Ticks avant le prochain son d'insecte"
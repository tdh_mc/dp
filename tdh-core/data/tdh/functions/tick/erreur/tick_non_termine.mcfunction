# On exécute cette fonction si la variable #quickTick id est supérieure à 0,
# pour informer les joueurs d'une erreur dans le déroulement du tick précédent
tellraw @a[tag=TickDebugLog] [{"text":"[ERREUR]","color":"red"},{"text":" Le tick n°","color":"gray"},{"score":{"name":"#quickTick","objective":"id"}},{"text":" s'est terminé incorrectement et a dû être écourté.","color":"gray"}]

scoreboard players reset #quickTick id
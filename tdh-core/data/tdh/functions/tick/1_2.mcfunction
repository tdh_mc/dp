# On entoure l'appel au function tag d'un set/unset de variable ;
# Cela permettra au prochain tick de vérifier si celui-ci s'est terminé correctement
scoreboard players set #quickTick id 102
# On définit aussi une variable "duree", qui indique aux fonctions le nombre de ticks séparant 2 appels
scoreboard players set #quickTick duree 40

# On lance le function tag approprié
function #tdh:tick/1_2

# On reset la variable temporaire pour indiquer qu'on a terminé correctement la fonction
scoreboard players reset #quickTick id
# Fonction tick secondaire de TDH
# Gère les fréquences de tick des fonctions lentes (fréquence < 1/seconde)

# On ajoute 1 à la valeur actuelle du tick (augmente 2 fois par seconde)
scoreboard players add #slowTick currentTick 1
# On boucle tous les 64 ticks (32s)
execute if score #slowTick currentTick matches 64.. run scoreboard players set #slowTick currentTick 0


execute if score #slowTick currentTick matches 0 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 1 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 4 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 5 run function tdh:tick/1_5
execute if score #slowTick currentTick matches 6 run function tdh:tick/1_4
execute if score #slowTick currentTick matches 7 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 8 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 10 run function tdh:tick/1_8
execute if score #slowTick currentTick matches 12 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 13 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 14 run function tdh:tick/1_4
execute if score #slowTick currentTick matches 15 run function tdh:tick/1_5
execute if score #slowTick currentTick matches 16 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 17 run function tdh:tick/1_10
execute if score #slowTick currentTick matches 18 run function tdh:tick/1_16
execute if score #slowTick currentTick matches 19 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 20 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 22 run function tdh:tick/1_4
execute if score #slowTick currentTick matches 24 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 25 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 26 run function tdh:tick/1_8
execute if score #slowTick currentTick matches 27 run function tdh:tick/1_5
execute if score #slowTick currentTick matches 28 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 30 run function tdh:tick/1_4
execute if score #slowTick currentTick matches 31 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 32 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 34 run function tdh:tick/1_32
execute if score #slowTick currentTick matches 35 run function tdh:tick/1_10
execute if score #slowTick currentTick matches 36 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 37 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 38 run function tdh:tick/1_4
execute if score #slowTick currentTick matches 39 run function tdh:tick/1_5
execute if score #slowTick currentTick matches 40 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 42 run function tdh:tick/1_8
execute if score #slowTick currentTick matches 43 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 44 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 46 run function tdh:tick/1_4
execute if score #slowTick currentTick matches 47 run function tdh:tick/1_5
execute if score #slowTick currentTick matches 48 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 49 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 50 run function tdh:tick/1_16
execute if score #slowTick currentTick matches 52 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 54 run function tdh:tick/1_4
execute if score #slowTick currentTick matches 55 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 56 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 57 run function tdh:tick/1_10
execute if score #slowTick currentTick matches 58 run function tdh:tick/1_8
execute if score #slowTick currentTick matches 59 run function tdh:tick/1_5
execute if score #slowTick currentTick matches 60 run function tdh:tick/1_2
execute if score #slowTick currentTick matches 61 run function tdh:tick/1_3
execute if score #slowTick currentTick matches 62 run function tdh:tick/1_4
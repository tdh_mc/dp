# Version "lite" du module time
# On stocke le tick actuel
execute store result score #temps currentTick run time query daytime

# On stocke l'heure, puis la minute
scoreboard players set #temps temp 1000
scoreboard players operation #temps currentHour = #temps currentTick
scoreboard players operation #temps currentHour /= #temps temp
scoreboard players add #temps currentHour 6
execute if score #temps currentHour matches 24.. run scoreboard players remove #temps currentHour 24
scoreboard players operation #temps currentMinute = #temps currentTick
scoreboard players operation #temps currentMinute %= #temps temp
scoreboard players set #temps temp 3
scoreboard players operation #temps currentMinute *= #temps temp
scoreboard players set #temps temp 50
scoreboard players operation #temps currentMinute /= #temps temp
scoreboard players reset #temps temp

# On calcule le moment de la journée à partir de valeurs fixes
execute if score #temps currentTick matches 6000..17999 run scoreboard players set #temps timeOfDay 1
execute if score #temps currentTick matches 18000..19799 run scoreboard players set #temps timeOfDay 2
execute unless score #temps currentTick matches 4200..19799 run scoreboard players set #temps timeOfDay 3
execute if score #temps currentTick matches 4200..5999 run scoreboard players set #temps timeOfDay 4
# La minute a overflow lors du calcul de l'heure
# On répare le souci en réinitialisant la minute au dernier nombre possible (le nombre de minutes - 1)
scoreboard players operation #store2 currentMinute = #temps minutesPerHour
scoreboard players remove #store2 currentMinute 1
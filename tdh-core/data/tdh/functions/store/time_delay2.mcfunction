# On appelle cette fonction après avoir défini l'objectif #store2 currentHour
# Le résultat sera stocké sous forme de NBT dans le storage "tdh:store time2"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, puisque les scores correspondants changeront lors du prochain appel à cette fonction

# Cette fonction est identique à tdh:store/time2, mais affiche "30mn" au lieu de "0h30" si l'occasion se présente
# La SEULE utilité de cette fonction est de pouvoir stocker 2 délais et de pouvoir les afficher sur la même ligne de tellraw. Sinon, elle est absolument identique à tdh:store/time_delay

# On calcule l'heure et la minute
scoreboard players operation #store2 currentMinute = #store2 currentHour
scoreboard players operation #store2 currentHour /= #temps ticksPerHour
scoreboard players operation #store2 currentMinute %= #temps ticksPerHour
scoreboard players operation #store2 currentMinute *= #temps minutesPerHour
scoreboard players operation #store2 currentMinute /= #temps ticksPerHour
execute if score #store2 currentMinute >= #temps minutesPerHour run function tdh:store/time/minute_overflow2

# On enregistre l'affichage correct dans le storage
execute unless score #store2 currentHour matches 0 if score #store2 currentMinute matches ..9 run data modify storage tdh:store time2 set value '[{"score":{"name":"#store2","objective":"currentHour"}},{"text":"h0"},{"score":{"name":"#store2","objective":"currentMinute"}}]'
execute unless score #store2 currentHour matches 0 if score #store2 currentMinute matches 10.. run data modify storage tdh:store time2 set value '[{"score":{"name":"#store2","objective":"currentHour"}},{"text":"h"},{"score":{"name":"#store2","objective":"currentMinute"}}]'
execute if score #store2 currentHour matches 0 run data modify storage tdh:store time2 set value '[{"score":{"name":"#store2","objective":"currentMinute"}},{"text":"mn"}]'
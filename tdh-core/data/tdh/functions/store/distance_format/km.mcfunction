# On enregistre le nombre de kilomètres
scoreboard players add #store distance 500
scoreboard players set #store temp9 1000
scoreboard players operation #store distance /= #store temp9
scoreboard players reset #store temp9

# On enregistre l'affichage correct dans le storage
data modify storage tdh:store distance set value '[{"score":{"name":"#store","objective":"distance"}},{"text":" kilomètres"}]'
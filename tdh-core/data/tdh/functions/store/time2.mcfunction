# On appelle cette fonction après avoir défini l'objectif #store currentHour
# Le résultat sera stocké sous forme de NBT dans le storage "tdh:store time2"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, puisque les scores correspondants changeront lors du prochain appel à cette fonction

# La SEULE utilité de cette fonction est de pouvoir stocker 2 horaires et de pouvoir les afficher sur la même ligne de tellraw. Sinon, elle est absolument identique à tdh:store/time

# On calcule l'heure et la minute
scoreboard players operation #store2 currentMinute = #store2 currentHour
scoreboard players operation #store2 currentHour /= #temps ticksPerHour
scoreboard players operation #store2 currentMinute %= #temps ticksPerHour
scoreboard players operation #store2 currentMinute *= #temps minutesPerHour
scoreboard players operation #store2 currentMinute /= #temps ticksPerHour
execute if score #store2 currentMinute >= #temps minutesPerHour run function tdh:store/time/minute_overflow2

# On enregistre l'affichage correct dans le storage
execute if score #store2 currentMinute matches ..9 run data modify storage tdh:store time2 set value '[{"score":{"name":"#store2","objective":"currentHour"}},{"text":"h0"},{"score":{"name":"#store2","objective":"currentMinute"}}]'
execute if score #store2 currentMinute matches 10.. run data modify storage tdh:store time2 set value '[{"score":{"name":"#store2","objective":"currentHour"}},{"text":"h"},{"score":{"name":"#store2","objective":"currentMinute"}}]'
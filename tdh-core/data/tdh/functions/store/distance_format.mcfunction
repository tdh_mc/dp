# On appelle cette fonction après avoir défini l'objectif #store distance
# Le résultat sera stocké sous forme de NBT dans le storage "tdh:store distance"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, puisque les scores correspondants changeront lors du prochain appel à cette fonction

# On vérifie quelle unité on doit utiliser
execute if score #store distance matches ..999 run function tdh:store/distance_format/m
execute if score #store distance matches 1000.. run function tdh:store/distance_format/km
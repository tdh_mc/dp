# On appelle cette fonction après avoir défini l'objectif #store currentHour
# Le résultat sera stocké sous forme de NBT dans le storage "tdh:store realtime"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, puisque les scores correspondants changeront lors du prochain appel à cette fonction

# Cette fonction est identique à tdh:store/time_delay, mais calcule un temps réel (IRL) plutôt qu'un temps ingame
# Par exemple un input de 24000 donnera 20mn et non 12h.
# Il n'existe pas de version équivalente à tdh:store/time puisqu'on ne peut pas savoir quelle est l'heure "absolue" IRL.

# On calcule l'heure, la minute et la seconde (on utilise currentTick comme variable "seconde" car osef)
scoreboard players operation #store currentMinute = #store currentHour
# 1h = 3600s = 3600x20t = 72000t
scoreboard players set #store temp 72000
scoreboard players operation #store currentHour /= #store temp
scoreboard players operation #store currentMinute %= #store temp
scoreboard players operation #store currentTick = #store currentMinute
# 1m = 60s = 60x20t = 1200t
scoreboard players set #store temp 1200
scoreboard players operation #store currentMinute /= #store temp
scoreboard players operation #store currentTick %= #store temp
# 1s = 20t
scoreboard players set #store temp 20
scoreboard players operation #store currentTick /= #store temp
scoreboard players reset #store temp

# On enregistre l'affichage correct dans le storage
execute if score #store currentHour matches 0 if score #store currentMinute matches 0 run data modify storage tdh:store realtime set value '[{"score":{"name":"#store","objective":"currentTick"}},{"text":"s"}]'
execute if score #store currentHour matches 0 unless score #store currentMinute matches 0 if score #store currentTick matches ..9 run data modify storage tdh:store realtime set value '[{"score":{"name":"#store","objective":"currentMinute"}},{"text":"mn0"},{"score":{"name":"#store","objective":"currentTick"}}]'
execute if score #store currentHour matches 0 unless score #store currentMinute matches 0 if score #store currentTick matches 10..59 run data modify storage tdh:store realtime set value '[{"score":{"name":"#store","objective":"currentMinute"}},{"text":"mn"},{"score":{"name":"#store","objective":"currentTick"}}]'
execute unless score #store currentHour matches 0 if score #store currentMinute matches ..9 run data modify storage tdh:store realtime set value '[{"score":{"name":"#store","objective":"currentHour"}},{"text":"h0"},{"score":{"name":"#store","objective":"currentMinute"}}]'
execute unless score #store currentHour matches 0 if score #store currentMinute matches 10..59 run data modify storage tdh:store realtime set value '[{"score":{"name":"#store","objective":"currentHour"}},{"text":"h"},{"score":{"name":"#store","objective":"currentMinute"}}]'
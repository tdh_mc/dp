# On appelle cette fonction après avoir défini l'objectif #store currentHour
# Le résultat sera stocké sous forme de NBT dans le storage "tdh:store time"
# Il ne peut être utilisé qu'immédiatement après avoir été généré, puisque les scores correspondants changeront lors du prochain appel à cette fonction

# On calcule l'heure et la minute
scoreboard players operation #store currentMinute = #store currentHour
scoreboard players operation #store currentHour /= #temps ticksPerHour
scoreboard players operation #store currentMinute %= #temps ticksPerHour
scoreboard players operation #store currentMinute *= #temps minutesPerHour
scoreboard players operation #store currentMinute /= #temps ticksPerHour
execute if score #store currentMinute >= #temps minutesPerHour run function tdh:store/time/minute_overflow

# On enregistre l'affichage correct dans le storage
execute if score #store currentMinute matches ..9 run data modify storage tdh:store time set value '[{"score":{"name":"#store","objective":"currentHour"}},{"text":"h0"},{"score":{"name":"#store","objective":"currentMinute"}}]'
execute if score #store currentMinute matches 10.. run data modify storage tdh:store time set value '[{"score":{"name":"#store","objective":"currentHour"}},{"text":"h"},{"score":{"name":"#store","objective":"currentMinute"}}]'
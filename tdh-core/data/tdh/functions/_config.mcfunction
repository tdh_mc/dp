# On affiche un message générique proposant de configurer TDH
tellraw @s [{"text":"--- CONFIGURATEUR TDH ---","color":"gold"}]
tellraw @s [{"text":"Pour configurer les différents modules, utiliser les liens ci-dessous :","color":"gray"}]

# On affiche tous les messages des packs TDH chargés
function #tdh:config/prompts
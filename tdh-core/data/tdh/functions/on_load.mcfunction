# On ajoute la variable dont on a besoin pour enregistrer l'initialisation des packs
scoreboard objectives add initDone dummy "Initialization done"

# Si tdh-core n'est pas encore initialisé, on affiche le message de bienvenue
execute unless score #tdh-core initDone matches 1.. run function tdh:on_load/message_initial
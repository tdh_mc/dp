# Cette fonction se comporte comme tdh:random, mais affiche également à l'appelant les informations sur le nombre généré
tellraw @s [{"text":"On appelle ","color":"gray"},{"text":"tdh:random","color":"gold"},{"text":" avec un minimum de "},{"score":{"name":"#random","objective":"min"},"color":"yellow"},{"text":" et un maximum de "},{"score":{"name":"#random","objective":"max"},"color":"yellow"},{"text":"."}]
function tdh:random
tellraw @s [{"text":"Résultat aléatoire : ","color":"gray"},{"score":{"name":"#random","objective":"temp"},"color":"yellow"}]
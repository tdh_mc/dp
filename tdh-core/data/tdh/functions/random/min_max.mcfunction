# Cette fonction renvoie un nombre aléatoire dans #random temp
# Ce nombre est compris entre #random min (inclus) et #random max (non inclus)

# On calcule l'intervalle [min,max[ et on le stocke dans #random temp2
scoreboard players operation #random temp2 = #random max
scoreboard players operation #random temp2 -= #random min

execute store result score #random temp run data get entity @e[type=#tdh:random,sort=random,limit=1] Pos[2] 62.83184
scoreboard players operation #random temp %= #random temp2
scoreboard players operation #random temp += #random min
# Cette fonction sélectionne la méthode à utiliser pour calculer le nombre aléatoire
# Elle n'est exécutée que si #random min est différent de #random max, donc on peut le tenir pour acquis

# min = 0 : On utilise la version simple de random
execute if score #random min matches 0 run function tdh:random/max_only
# Sinon, on utilise la version complexe
execute unless score #random min matches 0 run function tdh:random/min_max
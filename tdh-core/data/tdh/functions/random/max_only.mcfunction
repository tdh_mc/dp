# Cette fonction renvoie un nombre aléatoire dans #random temp
# Ce nombre est compris entre 0 (inclus) et #random max (non inclus)

execute store result score #random temp run data get entity @e[type=#tdh:random,sort=random,limit=1] Pos[2] 62.83184
scoreboard players operation #random temp %= #random max
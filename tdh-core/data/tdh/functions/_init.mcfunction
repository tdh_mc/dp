# Fonction d'initialisation principale de TDH

# On initialise d'abord toutes les variables principales
scoreboard objectives add on dummy "Activé"
scoreboard objectives add id dummy "Identifiant"
scoreboard objectives add min dummy "Minimum"
scoreboard objectives add max dummy "Maximum"
scoreboard objectives add status dummy "Statut"
scoreboard objectives add mode dummy "Mode"
scoreboard objectives add voix dummy "Voix"
scoreboard objectives add voixMax dummy "Voix maximale"

scoreboard objectives add posX dummy "X"
scoreboard objectives add posY dummy "Y"
scoreboard objectives add posZ dummy "Z"
scoreboard objectives add deltaX dummy
scoreboard objectives add deltaY dummy
scoreboard objectives add deltaZ dummy
scoreboard objectives add rotation dummy "Rotation"
scoreboard objectives add rotationMax dummy "Rotation maximale"
scoreboard objectives add distance dummy "Distance"
scoreboard objectives add nombre dummy "Nombre"
scoreboard objectives add vitesse dummy "Vitesse"
scoreboard objectives add vitesseMin dummy "Vitesse minimale"
scoreboard objectives add vitesseMax dummy "Vitesse maximale"
scoreboard objectives add altitudeFromZero dummy "Altitude (par rapport à y=0)"
scoreboard objectives add previousAltitude dummy "Altitude précédente"
scoreboard objectives add sneakTime minecraft.custom:minecraft.sneak_time "Temps accroupi"

scoreboard objectives add temp dummy
scoreboard objectives add temp2 dummy
scoreboard objectives add temp3 dummy
scoreboard objectives add temp4 dummy
scoreboard objectives add temp5 dummy
scoreboard objectives add temp6 dummy
scoreboard objectives add temp7 dummy
scoreboard objectives add temp8 dummy
scoreboard objectives add temp9 dummy

scoreboard objectives add tickUpdate dummy "Tick du prochain update"
scoreboard objectives add jourUpdate dummy "Jour du prochain update"
scoreboard objectives add frequenceUpdate dummy "Fréquence d'update"

scoreboard objectives add currentTick dummy "Ticks actuel"
scoreboard objectives add remainingTicks dummy "Ticks restants"
scoreboard objectives add ticksPerHour dummy
scoreboard objectives add hoursPerDay dummy
scoreboard objectives add minutesPerHour dummy
scoreboard objectives add timeOfDay dummy
scoreboard objectives add currentHour dummy
scoreboard objectives add currentMinute dummy

scoreboard players set #temps ticksPerHour 1000
scoreboard players set #temps hoursPerDay 24
scoreboard players set #temps minutesPerHour 60

# On initialise les variables storées
data modify storage tdh:core motd set value '[{"text":"- Bienvenue sur les Terres du Hameau, ","color":"gold"},{"selector":"@p","color":"yellow"},{"text":" !"}]'

# On initialise ensuite tous les packs d'extension chargés
function #tdh:init
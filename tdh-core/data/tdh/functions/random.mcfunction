# Cette fonction renvoie un nombre aléatoire dans #random temp
# Ce nombre est compris entre #random min (inclus) et #random max (non inclus)

# Si min est supérieur à max, on le définit à max
scoreboard players operation #random min < #random max

# 2 cas de figures différents :
# Si min = max : On retourne min
execute if score #random min = #random max run scoreboard players operation #random temp = #random min
# Si min =/= max : On fait le calcul
execute unless score #random min = #random max run function tdh:random/calcul
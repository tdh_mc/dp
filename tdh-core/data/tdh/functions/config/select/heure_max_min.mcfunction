# Appeler cette fonction en ayant défini #configHeure min (inclusif) et #configHeure max (exclusif),
# dans le cas où min >= max
# Par exemple avec min = 22 et max = 7 on affichera 22, 23, 0, 1, 2, 3, 4, 5, 6

execute if score #configHeure max matches 13.. run function tdh:config/select/heure/12h
execute if score #configHeure max matches ..12 if score #configHeure min matches ..12 run function tdh:config/select/heure/12h
execute if score #configHeure max matches 14.. run function tdh:config/select/heure/13h
execute if score #configHeure max matches ..13 if score #configHeure min matches ..13 run function tdh:config/select/heure/13h
execute if score #configHeure max matches 15.. run function tdh:config/select/heure/14h
execute if score #configHeure max matches ..14 if score #configHeure min matches ..14 run function tdh:config/select/heure/14h
execute if score #configHeure max matches 16.. run function tdh:config/select/heure/15h
execute if score #configHeure max matches ..15 if score #configHeure min matches ..15 run function tdh:config/select/heure/15h
execute if score #configHeure max matches 17.. run function tdh:config/select/heure/16h
execute if score #configHeure max matches ..16 if score #configHeure min matches ..16 run function tdh:config/select/heure/16h
execute if score #configHeure max matches 18.. run function tdh:config/select/heure/17h
execute if score #configHeure max matches ..17 if score #configHeure min matches ..17 run function tdh:config/select/heure/17h
execute if score #configHeure max matches 19.. run function tdh:config/select/heure/18h
execute if score #configHeure max matches ..18 if score #configHeure min matches ..18 run function tdh:config/select/heure/18h
execute if score #configHeure max matches 20.. run function tdh:config/select/heure/19h
execute if score #configHeure max matches ..19 if score #configHeure min matches ..19 run function tdh:config/select/heure/19h
execute if score #configHeure max matches 21.. run function tdh:config/select/heure/20h
execute if score #configHeure max matches ..20 if score #configHeure min matches ..20 run function tdh:config/select/heure/20h
execute if score #configHeure max matches 22.. run function tdh:config/select/heure/21h
execute if score #configHeure max matches ..21 if score #configHeure min matches ..21 run function tdh:config/select/heure/21h
execute if score #configHeure max matches 23.. run function tdh:config/select/heure/22h
execute if score #configHeure max matches ..22 if score #configHeure min matches ..22 run function tdh:config/select/heure/22h
execute if score #configHeure max matches 24.. run function tdh:config/select/heure/23h
execute if score #configHeure max matches ..23 if score #configHeure min matches ..23 run function tdh:config/select/heure/23h
execute if score #configHeure max matches 1.. run function tdh:config/select/heure/0h
execute if score #configHeure max matches 2.. run function tdh:config/select/heure/1h
execute if score #configHeure max matches ..1 if score #configHeure min matches ..1 run function tdh:config/select/heure/1h
execute if score #configHeure max matches 3.. run function tdh:config/select/heure/2h
execute if score #configHeure max matches ..2 if score #configHeure min matches ..2 run function tdh:config/select/heure/2h
execute if score #configHeure max matches 4.. run function tdh:config/select/heure/3h
execute if score #configHeure max matches ..3 if score #configHeure min matches ..3 run function tdh:config/select/heure/3h
execute if score #configHeure max matches 5.. run function tdh:config/select/heure/4h
execute if score #configHeure max matches ..4 if score #configHeure min matches ..4 run function tdh:config/select/heure/4h
execute if score #configHeure max matches 6.. run function tdh:config/select/heure/5h
execute if score #configHeure max matches ..5 if score #configHeure min matches ..5 run function tdh:config/select/heure/5h
execute if score #configHeure max matches 7.. run function tdh:config/select/heure/6h
execute if score #configHeure max matches ..6 if score #configHeure min matches ..6 run function tdh:config/select/heure/6h
execute if score #configHeure max matches 8.. run function tdh:config/select/heure/7h
execute if score #configHeure max matches ..7 if score #configHeure min matches ..7 run function tdh:config/select/heure/7h
execute if score #configHeure max matches 9.. run function tdh:config/select/heure/8h
execute if score #configHeure max matches ..8 if score #configHeure min matches ..8 run function tdh:config/select/heure/8h
execute if score #configHeure max matches 10.. run function tdh:config/select/heure/9h
execute if score #configHeure max matches ..9 if score #configHeure min matches ..9 run function tdh:config/select/heure/9h
execute if score #configHeure max matches 11.. run function tdh:config/select/heure/10h
execute if score #configHeure max matches ..10 if score #configHeure min matches ..10 run function tdh:config/select/heure/10h
execute if score #configHeure max matches 12.. run function tdh:config/select/heure/11h
execute if score #configHeure max matches ..11 if score #configHeure min matches ..11 run function tdh:config/select/heure/11h

# Appeler cette fonction en ayant défini #configHeure min (inclusif) et #configHeure max (exclusif)
# Par exemple avec min = 22 et max = 4 on affichera 22, 23, 00, 01, 02, 03

execute if score #configHeure min >= #configHeure max run function tdh:config/select/heure_max_min
execute if score #configHeure min < #configHeure max run function tdh:config/select/heure_min_max
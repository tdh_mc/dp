# Ce function tag contient toutes les fonctions d'arrêt de toutes les config existantes dans les différents packs chargés,
# afin de terminer toute session de config non fermée au moment de la déconnexion
execute as @s run function #tdh:config/end_all

# On nettoie ensuite les éventuelles variables résiduelles
scoreboard players reset @s configID
scoreboard players reset @s config
tag @s remove Config
# On affiche un message générique proposant de configurer TDH
tellraw @s [{"text":"Bienvenue dans ","color":"gray","bold":"true"},{"text":"TDH","color":"gold"},{"text":" !"}]
tellraw @s [{"text":"Pour configurer les différents modules, utiliser les liens ci-dessous :","color":"gray"}]

# On affiche tous les messages des packs TDH chargés
function #tdh:config/prompts
# Fonction qui attend une variable @s config sous la forme HHMM (par exemple 1645 pour 16 h 45)
# et renvoie dans #config temp
scoreboard players operation #config temp = @s config
execute if score #config temp matches ..-1 run function tdh:config/calcul/hhmm_to_ticks/negative_init
scoreboard players operation #config temp2 = #config temp
scoreboard players set #config temp3 100
scoreboard players operation #config temp /= #config temp3
scoreboard players operation #config temp2 %= #config temp3
scoreboard players operation #config temp *= #temps ticksPerHour
scoreboard players operation #config temp2 *= #temps ticksPerHour
scoreboard players operation #config temp2 /= #temps minutesPerHour
scoreboard players operation #config temp += #config temp2
execute if score #config temp >= #temps fullDayTicks run scoreboard players set #config temp 0
execute if score @s config matches ..-1 run function tdh:config/calcul/hhmm_to_ticks/negative_end
# On veut replacer notre variable (#x temp) entre 0 et 360°
scoreboard players set #offset temp 3600
scoreboard players operation #x temp %= #offset temp

# On calcule la variable temporaire temp5 qui stocke le multiplicateur à appliquer au résultat final
execute if score #x temp matches ..1800 run scoreboard players set #x temp5 1
execute if score #x temp matches 1801.. run function tdh:math/sind/modulo_inv
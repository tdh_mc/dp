# Dans une fonction aussi "bas niveau" que celle-ci, on se fout un peu de la modularité
# Donc quand on peut hard-coder une valeur et économiser un appel à fonction, on le fait,
# et c'est tant pis pour les quelques lignes de code dupliquées

# Initialisation des variables
scoreboard players operation #x temp = #sin temp
# On calcule le modulo de notre angle, ainsi que le multiplicateur à appliquer
function tdh:math/sind/modulo
# Le maximum possible pour #x temp ici est donc 1800


# La formule complète de l'approximation qu'on utilise (Bhaskara) :
#	
# 			   4 * x * (180 - x)
#	sin x = ——————————————————————
#			 40500 - x * (180 - x)
#
# Ou plutôt, vu qu'on est dans Minecraft et qu'il n'y a que des nombres entiers :
#	
# 				 1000 * 4 * x * (180 - x)		[#sin temp]
#	1000*sin x = —————————————————————————
#				 40500 - (x * (180 - x))	[#sin temp2]
#

# temp = 10x * 10*(180 - x) = 100* x * (180-x)		[6'480'000]
scoreboard players set #sin temp 1800
scoreboard players operation #sin temp -= #x temp
scoreboard players operation #sin temp *= #x temp
# On récupère la valeur dont on aura besoin ultérieurement
scoreboard players operation #sin temp3 = #sin temp
# temp2 = 10 * 4
# temp = temp2 * (100*)(x * (180 - x))
#	   = 10 * 4 * x * (180 - x) (ce qui complète notre numérateur)	[259'200'000]
scoreboard players set #sin temp2 40
scoreboard players operation #sin temp *= #sin temp2

# temp2 = 40500 - temp3
#		= 40500 - (x * (180 - x))	[40'500]
scoreboard players set #sin temp2 4050000
scoreboard players operation #sin temp2 -= #sin temp3
scoreboard players set #sin temp3 100
scoreboard players operation #sin temp2 /= #sin temp3
# Ce qui complète notre dénominateur

# On effectue la division
# temp = temp / temp2
#	   = (1000 * 4 * x * (180 - x)) / (40500 - x * (180 - x))
scoreboard players operation #sin temp /= #sin temp2


# On multiplie à la toute fin par le multiplicateur calculé au début (pour les cas où on avait une rotation entre -180 et 0)
scoreboard players operation #sin temp *= #x temp5
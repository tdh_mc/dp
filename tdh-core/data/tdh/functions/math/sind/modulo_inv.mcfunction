# On enregistre le fait qu'on devra inverser le résultat final
scoreboard players set #x temp5 -1

# On retranche 360 (une rotation complète) à notre valeur actuelle, puis on l'inverse
scoreboard players remove #x temp 3600
scoreboard players operation #x temp *= #x temp5
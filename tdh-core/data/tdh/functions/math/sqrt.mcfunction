# On a #sqrt temp = un nombre > 0
# À la fin de la fonction on aura #sqrt temp = une valeur approchée de la racine carrée du nombre de départ

# Si notre nombre est trop grand, on divise les calculs par un facteur X, enregistré dans temp9
scoreboard players set #sqrt temp9 1
execute if score #sqrt temp matches 10000000.. run scoreboard players set #sqrt temp9 10
execute if score #sqrt temp matches 100000000.. run scoreboard players set #sqrt temp9 100
execute if score #sqrt temp matches 1000000000.. run scoreboard players set #sqrt temp9 1000
scoreboard players operation #sqrt temp /= #sqrt temp9

# On initialise tout pour la récursion
# min et max = les bornes minimum et maximum de la solution
scoreboard players set #sqrt min 0
scoreboard players operation #sqrt max = #sqrt temp
# temp2 = le nombre de départ, qu'on stocke temporairement
scoreboard players operation #sqrt temp2 = #sqrt temp
# temp = une première estimation du résultat attendu (comme on s'attend à avoir à peu près autant de demandes pour des directions < à 500m (bâtiment dans la même ville) ou > à 500m (autre ville ou lieu-dit), on divise par 500)
scoreboard players set #sqrt temp3 500
scoreboard players operation #sqrt temp /= #sqrt temp3
# temp4 = le facteur d'estimation, tjrs 2 en principe
scoreboard players set #sqrt temp4 2
# temp5 = le facteur de précision, détermine la différence acceptée entre l'estimation et la valeur recherchée pour admettre que c'est le même nombre
scoreboard players operation #sqrt temp5 = #sqrt temp2
scoreboard players set #sqrt temp6 250
scoreboard players operation #sqrt temp5 /= #sqrt temp6
scoreboard players reset #sqrt temp6
execute if score #sqrt temp5 matches ..4 run scoreboard players set #sqrt temp5 5
# temp8 = -1 (y'en a besoin pour faire l'inverse d'un nombre négatif dans la récursion...)
scoreboard players set #sqrt temp8 -1

# On enclenche ensuite la récursion
function tdh:math/sqrt/recursion

# Une fois qu'on a notre résultat, on le remultiplie par notre facteur de départ
# On veut néanmoins prendre la RACINE du facteur de départ donc comme on les connaît on a des conditions
execute if score #sqrt temp9 matches 1000 run function tdh:math/sqrt/facteur/1000
execute if score #sqrt temp9 matches 100 run function tdh:math/sqrt/facteur/100
execute if score #sqrt temp9 matches 10 run function tdh:math/sqrt/facteur/10

# Ensuite notre résultat est nickellement stocké dans #sqrt temp
# On nettoie toutes les autres variables
scoreboard players reset #sqrt temp2
scoreboard players reset #sqrt temp3
scoreboard players reset #sqrt temp4
scoreboard players reset #sqrt temp5
scoreboard players reset #sqrt temp6
scoreboard players reset #sqrt temp7
scoreboard players reset #sqrt temp8
scoreboard players reset #sqrt temp9
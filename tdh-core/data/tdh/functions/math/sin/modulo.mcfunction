# On a déjà notre offset enregistré dans la variable #offset temp,
# et on veut replacer notre variable (#x temp) entre 0 et 2 fois notre offset
scoreboard players operation #sin temp9 = #offset temp
scoreboard players operation #sin temp9 += #sin temp9
scoreboard players operation #x temp %= #sin temp9
scoreboard players reset #sin temp9

# On calcule la variable temporaire temp5 qui stocke le multiplicateur à appliquer au résultat final
execute if score #x temp <= #offset temp run scoreboard players set #x temp5 1
execute if score #x temp > #offset temp run function tdh:math/sin/modulo_inv
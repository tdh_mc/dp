# Dans une fonction aussi "bas niveau" que celle-ci, on se fout un peu de la modularité
# Donc quand on peut hard-coder une valeur et économiser un appel à fonction, on le fait,
# et c'est tant pis pour les quelques lignes de code dupliquées

# Initialisation des variables
function tdh:math/pi/1000
scoreboard players set #offset temp 1000
scoreboard players operation #x temp = #sin temp
# Si le x overflow, on le traite tout de suite (la fonction ne fonctionne qu'entre 0 et pi dans notre version)
execute unless score #x temp matches 0..1000 run function tdh:math/sin/modulo

# On multiplie par pi pour obtenir la "vraie" valeur de X
# Le maximum possible pour #x temp ici est donc 2000*3146=6292
scoreboard players operation #x temp *= #pi temp
scoreboard players operation #x temp /= #offset temp


# La formule complète de l'approximation qu'on utilise (Bhaskara) :
#	
# 				   16 * x * (pi-x)
#	sin x = ——————————————————————————————
#			 (5 * pi²) - (4 * x * (pi-x))
#
# Ou plutôt, vu qu'on est dans Minecraft et qu'il n'y a que des nombres entiers :
#	
# 					1000 * 16 * x * (pi-x)			[#sin temp]
#	1000*sin x = ——————————————————————————————
#				 (5 * pi²) - (4 * x * (pi-x))		[#sin temp2]
#


# temp = (1000*pi - 1000*x) = 1000*(pi - x)
scoreboard players operation #sin temp = #pi temp
scoreboard players operation #sin temp -= #x temp
# temp = 1000*x * [1000*(pi - x)] = 1'000'000 * x * (pi - x)		[valeur max=39'589'264]
scoreboard players operation #sin temp *= #x temp
# On récupère ici la valeur de temp3 dont on aura besoin plus bas
scoreboard players operation #sin temp3 = #sin temp
# temp = 16 * [1'000'000 * x * (pi - x)] = 1'000'000 * 16 * x * (pi - x)
scoreboard players set #sin temp2 16
scoreboard players operation #sin temp *= #sin temp2
# Juste ici, le maximum possible pour #sin temp est 16*6292*6292 = 633'428'224


# temp2 = 5 * 1000*pi²
scoreboard players set #sin temp4 5
scoreboard players operation #sin temp2 = #pi temp2
scoreboard players operation #sin temp2 *= #sin temp4

# temp3 = 1'000'000 * x * (pi - x) (voir plus haut)	[max=39'589'264]
# temp3 = 1'000'000 * 4 * x * (pi - x)
scoreboard players set #sin temp4 4
scoreboard players operation #sin temp3 *= #sin temp4
# temp3 = 1000 * 4 * x * (pi - x)
# On ajoute 500 avant de diviser par 1000 pour arrondir à l'unité la plus proche (et pas inférieure)
scoreboard players add #sin temp3 500
scoreboard players operation #sin temp3 /= #offset temp

# temp2 = (temp2 - temp3) = 1000*(5*pi²) - 1000*(4*x*(pi-x)) = 100*(5*pi² - 4*x*(pi-x))
scoreboard players operation #sin temp2 -= #sin temp3

# temp	= (temp) / (temp2)
#		= (1'000'000 * 16*x*(pi-x)) / (1000*(5*pi² - 4*x*(pi-x)))
#		= (1'000'000/1000) * (16*x*(pi-x)) / (5*pi² - 4*x*(pi-x))
#		= 1000 * sin x
scoreboard players operation #sin temp /= #sin temp2

# On multiplie à la toute fin par le multiplicateur calculé au début (pour les cas où on avait une rotation entre -180 et 0)
scoreboard players operation #sin temp *= #x temp5
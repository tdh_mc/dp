# Dans une fonction aussi "bas niveau" que celle-ci, on se fout un peu de la modularité
# Donc quand on peut hard-coder une valeur et économiser un appel à fonction, on le fait,
# et c'est tant pis pour les quelques lignes de code dupliquées

# Initialisation des variables
function tdh:math/pi/10
scoreboard players set #offset temp 10
scoreboard players operation #x temp = #sin temp
# Si le x overflow, on le traite tout de suite (la fonction ne fonctionne qu'entre 0 et pi dans notre version)
execute unless score #x temp matches 0..10 run function tdh:math/sin/modulo

# On multiplie par pi pour obtenir la "vraie" valeur de X
# Le maximum possible pour #x temp ici est donc 20*31=62
scoreboard players operation #x temp *= #pi temp
scoreboard players operation #x temp /= #offset temp


# La formule complète de l'approximation qu'on utilise (Bhaskara) :
#	
# 				   16 * x * (pi-x)
#	sin x = ——————————————————————————————
#			 (5 * pi²) - (4 * x * (pi-x))
#
# Ou plutôt, vu qu'on est dans Minecraft et qu'il n'y a que des nombres entiers :
#	
# 					10 * 16 * x * (pi-x)			[#sin temp]
#	10*sin x = ——————————————————————————————
#				 (5 * pi²) - (4 * x * (pi-x))		[#sin temp2]
#


# temp = (10*pi - 10*x) = 10*(pi - x)
scoreboard players operation #sin temp = #pi temp
scoreboard players operation #sin temp -= #x temp
# temp = 10*x * [10*(pi - x)] = 100 * x * (pi - x)		[valeur max=3844]
scoreboard players operation #sin temp *= #x temp
# On récupère ici la valeur de temp3 dont on aura besoin plus bas
scoreboard players operation #sin temp3 = #sin temp
# temp = 16 * [100 * x * (pi - x)] = 100 * 16 * x * (pi - x)
scoreboard players set #sin temp2 16
scoreboard players operation #sin temp *= #sin temp2
# Juste ici, le maximum possible pour #sin temp est 16*62*62 = 61'504


# temp2 = 5 * 10*pi²
scoreboard players set #sin temp4 5
scoreboard players operation #sin temp2 = #pi temp2
scoreboard players operation #sin temp2 *= #sin temp4

# temp3 = 100 * x * (pi - x) (voir plus haut)	[max=3844]
# temp3 = 100 * 4 * x * (pi - x)
scoreboard players set #sin temp4 4
scoreboard players operation #sin temp3 *= #sin temp4
# temp3 = 10 * 4 * x * (pi - x)
# On ajoute 5 avant de diviser par 10 pour arrondir à l'unité la plus proche (et pas inférieure)
scoreboard players add #sin temp3 5
scoreboard players operation #sin temp3 /= #offset temp

# temp2 = (temp2 - temp3) = 10*(5*pi²) - 10*(4*x*(pi-x)) = 10*(5*pi² - 4*x*(pi-x))
scoreboard players operation #sin temp2 -= #sin temp3

# temp	= (temp) / (temp2)
#		= (100 * 16*x*(pi-x)) / (10*(5*pi² - 4*x*(pi-x)))
#		= (100/10) * (16*x*(pi-x)) / (5*pi² - 4*x*(pi-x))
#		= 10 * sin x
scoreboard players operation #sin temp /= #sin temp2

# On multiplie à la toute fin par le multiplicateur calculé au début (pour les cas où on avait une rotation entre -180 et 0)
scoreboard players operation #sin temp *= #x temp5
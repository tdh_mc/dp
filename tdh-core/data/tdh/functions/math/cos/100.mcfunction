# Dans une fonction aussi "bas niveau" que celle-ci, on se fout un peu de la modularité
# Donc quand on peut hard-coder une valeur et économiser un appel à fonction, on le fait,
# et c'est tant pis pour les quelques lignes de code dupliquées

# Initialisation des variables
function tdh:math/pi/100
scoreboard players set #offset temp 100
scoreboard players operation #x temp = #cos temp
# On rajoute simplement +pi/2 (+100/2) à x pour calculer le cosinus au lieu du sinus
scoreboard players add #x temp 50
# Si le x overflow, on le traite tout de suite (la fonction ne fonctionne qu'entre 0 et pi dans notre version)
execute unless score #x temp matches 0..100 run function tdh:math/sin/modulo

# On multiplie par pi pour obtenir la "vraie" valeur de X
# Le maximum possible pour #x temp ici est donc 200*314=628
scoreboard players operation #x temp *= #pi temp
scoreboard players operation #x temp /= #offset temp


# La formule complète de l'approximation qu'on utilise (Bhaskara) :
#	
# 				   16 * x * (pi-x)
#	sin x = ——————————————————————————————
#			 (5 * pi²) - (4 * x * (pi-x))
#
# Ou plutôt, vu qu'on est dans Minecraft et qu'il n'y a que des nombres entiers :
#	
# 					100 * 16 * x * (pi-x)			[#cos temp]
#	100*sin x = ——————————————————————————————
#				 (5 * pi²) - (4 * x * (pi-x))		[#cos temp2]
#


# temp = (100*pi - 100*x) = 100*(pi - x)
scoreboard players operation #cos temp = #pi temp
scoreboard players operation #cos temp -= #x temp
# temp = 100*x * [100*(pi - x)] = 10'000 * x * (pi - x)		[valeur max=394'384]
scoreboard players operation #cos temp *= #x temp
# On récupère ici la valeur de temp3 dont on aura besoin plus bas
scoreboard players operation #cos temp3 = #cos temp
# temp = 16 * [10'000 * x * (pi - x)] = 10'000 * 16 * x * (pi - x)
scoreboard players set #cos temp2 16
scoreboard players operation #cos temp *= #cos temp2
# Juste ici, le maximum possible pour #cos temp est 16*628*628 = 6'310'144


# temp2 = 5 * 100*pi²
scoreboard players set #cos temp4 5
scoreboard players operation #cos temp2 = #pi temp2
scoreboard players operation #cos temp2 *= #cos temp4

# temp3 = 10'000 * x * (pi - x) (voir plus haut)	[max=394'384]
# temp3 = 10'000 * 4 * x * (pi - x)
scoreboard players set #cos temp4 4
scoreboard players operation #cos temp3 *= #cos temp4
# temp3 = 100 * 4 * x * (pi - x)
# On ajoute 50 avant de diviser par 100 pour arrondir à l'unité la plus proche (et pas inférieure)
scoreboard players add #cos temp3 50
scoreboard players operation #cos temp3 /= #offset temp

# temp2 = (temp2 - temp3) = 100*(5*pi²) - 100*(4*x*(pi-x)) = 100*(5*pi² - 4*x*(pi-x))
scoreboard players operation #cos temp2 -= #cos temp3

# temp	= (temp) / (temp2)
#		= (10'000 * 16*x*(pi-x)) / (100*(5*pi² - 4*x*(pi-x)))
#		= (10'000/100) * (16*x*(pi-x)) / (5*pi² - 4*x*(pi-x))
#		= 100 * sin x
scoreboard players operation #cos temp /= #cos temp2

# On multiplie à la toute fin par le multiplicateur calculé au début (pour les cas où on avait une rotation entre -180 et 0)
scoreboard players operation #cos temp *= #x temp5
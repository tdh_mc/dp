# Si notre estimation est trop haute, on peut modifier notre borne maximum
execute if score #sqrt temp6 matches ..-1 run function tdh:math/sqrt/trop_haut
# Sinon, on modifie notre borne minimum
execute if score #sqrt temp6 matches 1.. run function tdh:math/sqrt/trop_bas

# On continue la récursion
function tdh:math/sqrt/recursion
# MIN / MAX = les bornes actuelles de la racine carrée
# TEMP = notre estimation actuelle
# TEMP2 = le nombre de départ, dont on cherche la racine carrée
# TEMP4 = le facteur d'estimation, en principe tjrs 2 tant que la formule n'est pas plus développée
# TEMP5, le facteur de précision, détermine la différence max entre temp2 et notre estimation pour dire que le calcul est fini
# TEMP8 = -1

# On va utiliser aussi :
# TEMP3 = notre estimation actuelle au carré, pour comparaison avec temp2
# TEMP6 = la différence entre l'estimation et le nombre de départ
# TEMP7 = la différence entre l'estimation et le nombre de départ (valeur absolue)


# temp3 = estimation^2
scoreboard players operation #sqrt temp3 = #sqrt temp
scoreboard players operation #sqrt temp3 *= #sqrt temp

# On calcule ensuite la différence entre le résultat attendu et estimation^2
scoreboard players operation #sqrt temp6 = #sqrt temp2
scoreboard players operation #sqrt temp6 -= #sqrt temp3
# On calcule la valeur absolue de cette différence
scoreboard players operation #sqrt temp7 = #sqrt temp6
execute if score #sqrt temp7 matches ..-1 run scoreboard players operation #sqrt temp7 *= #sqrt temp8

# Si notre estimation est OK, on termine la fonction
# Sinon, on lance une nouvelle itération
execute if score #sqrt temp7 > #sqrt temp5 run function tdh:math/sqrt/continuer
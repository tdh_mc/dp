execute store result score @p dir run data get entity @p Rotation[0]

execute if score @p dir matches 41..49 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"sud-ouest","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches 50..130 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers l'","color":"gold"},{"text":"ouest","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches 131..139 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"nord-ouest","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches 140..220 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"nord","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches 221..229 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"nord-est","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches 230..310 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers l'","color":"gold"},{"text":"est","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches 311..319 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"sud-est","color":"red"},{"text":".","color":"gold"}]
execute unless score @p dir matches 41..319 unless score @p dir matches -319..-41 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"sud","color":"red"},{"text":".","color":"gold"}]

execute if score @p dir matches -319..-311 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"sud-ouest","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches -310..-230 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers l'","color":"gold"},{"text":"ouest","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches -229..-221 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"nord-ouest","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches -220..-140 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"nord","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches -139..-131 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"nord-est","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches -130..-50 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers l'","color":"gold"},{"text":"est","color":"red"},{"text":".","color":"gold"}]
execute if score @p dir matches -49..-41 run title @a[distance=0] actionbar [{"text":"Vous êtes tourné vers le ","color":"gold"},{"text":"sud-est","color":"red"},{"text":".","color":"gold"}]

# On enregistre le fait qu'on a affiché un titre
tag @a[distance=0] remove TitleTest
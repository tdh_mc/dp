execute if score #temps currentMinute matches 10..59 run title @a[distance=0] actionbar [{"text":"Il est ","color":"gold"},{"score":{"name":"#temps","objective":"currentHour"},"color":"red"},{"text":"h","color":"red"},{"score":{"name":"#temps","objective":"currentMinute"},"color":"red"},{"text":".","color":"gold"}]
execute if score #temps currentMinute matches ..9 run title @a[distance=0] actionbar [{"text":"Il est ","color":"gold"},{"score":{"name":"#temps","objective":"currentHour"},"color":"red"},{"text":"h0","color":"red"},{"score":{"name":"#temps","objective":"currentMinute"},"color":"red"},{"text":".","color":"gold"}]
execute if score #temps currentMinute matches 60.. run title @a[distance=0] actionbar [{"text":"Il est ","color":"gold"},{"score":{"name":"#temps","objective":"currentHour"},"color":"red"},{"text":"h59","color":"red"},{"text":".","color":"gold"}]

# On enregistre le fait qu'on a affiché un titre
tag @a[distance=0] remove TitleTest
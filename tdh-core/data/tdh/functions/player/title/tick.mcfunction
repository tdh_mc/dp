# TITRES ACTIONBAR
# On donne un tag à tous les joueurs
tag @a add TitleTest

# On fait le test pour chaque titre possible
execute at @a run function #tdh:player/title_check

# On supprime le tag temporaire
tag @a remove TitleTest
scoreboard objectives remove currentBiome
scoreboard objectives add biomeType dummy "Type de biome"
scoreboard objectives add disconnects minecraft.custom:minecraft.leave_game "Déconnexions"
scoreboard objectives add tempsEveil minecraft.custom:minecraft.time_since_rest "Temps d'éveil"
scoreboard objectives add insideness dummy "Intérieureté"
scoreboard objectives add lightLevel dummy "Lumière ambiante"
scoreboard objectives remove nextInsideCheck
scoreboard objectives add tickUpdate dummy "Tick du prochain update"

# Variables relatives au /locate
scoreboard objectives add distance dummy
scoreboard objectives add minDistance dummy
scoreboard objectives add weight dummy "Poids"
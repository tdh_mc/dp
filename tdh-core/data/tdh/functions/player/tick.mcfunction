# ---- PRISE EN COMPTE DU DÉCÈS ----
# Si un joueur vient de mourir, on lui joue le jingle de décès
execute at @a[scores={deces=1..}] run function tdh:player/death

# ---- PRISE EN COMPTE DE LA CONNEXION ----
# Si un joueur n'a pas de nombre de déconnexions (ne s'est jamais connecté), on lui assigne 1 (pour lui afficher quand même le message de connexion)
execute at @a unless score @p disconnects matches 0.. run scoreboard players set @p disconnects 1
# Si un joueur a 1 déconnexion ou +, c'est qu'il vient de se connecter; on lance donc la fonction correspondante
execute at @a[scores={disconnects=1..}] run function tdh:player/on_connect/main
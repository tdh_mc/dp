## PLAYER/ON_CONNECT/MAIN
# Gère toutes les actions devant se produire lors de la connexion/reconnexion d'un joueur
# et est exécutée à la position précise de ce joueur.

# On exécute les différentes fonctions de connexion définies par le function tag
function #tdh:player/on_connect

# On affiche le MOTD en dernier, sauf s'il est désactivé
execute unless score #motd on matches 0 run function tdh:player/on_connect/motd

# On réinitialise le nombre de déconnexions
scoreboard players set @a[distance=0] disconnects 0
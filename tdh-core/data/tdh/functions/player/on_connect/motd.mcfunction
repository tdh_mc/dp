# Affichage du MOTD au joueur le plus proche
tellraw @p [{"nbt":"motd","storage":"tdh:core","interpret":"true"}]
tellraw @p [{"text":"----------------------------------------------------------","color":"gold"}]

# Affichage des éventuelles lignes supplémentaires
function #tdh:player/motd

# Affichage de l'heure actuelle
function tdh:player/on_connect/motd/time

tellraw @p [{"text":"----------------------------------------------------------\n","color":"gold"}]
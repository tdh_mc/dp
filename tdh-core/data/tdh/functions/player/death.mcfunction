# On enregistre le fait qu'on vient de traiter le décès
tag @p[scores={deces=1..}] add pendingDeath
scoreboard players set @p[tag=pendingDeath] deces 0

# On effectue toutes les actions nécessaires des différents packs d'extension
function #tdh:player/on_death

# On vérifie pour toutes les méthodes "alternatives" de respawn si les conditions sont remplies
# Si l'une des conditions passe, elle retirera le tag pendingDeath
execute at @p[tag=pendingDeath] run function #tdh:player/respawn_check

# Si non, on génère un point de spawn avec la méthode générique
execute at @p[tag=pendingDeath] run function #tdh:player/respawn_default

# On réinitialise le tag temporaire
tag @a[tag=pendingDeath] remove pendingDeath
# Assignation d'une valeur d'intérieureté pour le joueur le plus proche
# On supprime d'abord toute valeur existante d'intérieureté
scoreboard players reset @a[distance=0] insideness

# On vérifie si on est sous la couche 50 (si oui on est à l'intérieur, dans tous les cas : insideness 16/16)
execute if score @p altitudeFromZero matches ..49 run scoreboard players set @a[distance=0] insideness 16

# On fait un check de la colonne au-dessus de nous ;
# Si elle contient de l'air, on est complètement à l'extérieur (insideness 0/16)
execute if blocks ~ ~1 ~ ~ ~40 ~ ~ 255 ~ masked run scoreboard players set @a[distance=0] insideness 0

# Si aucune des deux conditions ci-dessus n'a matché, on vérifie toutes les colonnes autour de nous
execute unless score @p insideness matches 0.. run function tdh:player/scores/test_interieur/test_blocs
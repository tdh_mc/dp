## TICK DU CALCUL DE LA LUMIÈRE AMBIANTE
execute if entity @p[distance=..0,advancements={tdh:light_level={dark=true}}] run scoreboard players set @p lightLevel 0
execute if entity @p[distance=..0,advancements={tdh:light_level={dim_light=true}}] run scoreboard players set @p lightLevel 5
execute if entity @p[distance=..0,advancements={tdh:light_level={medium_light=true}}] run scoreboard players set @p lightLevel 10
execute if entity @p[distance=..0,advancements={tdh:light_level={bright_light=true}}] run scoreboard players set @p lightLevel 15
advancement revoke @p only tdh:light_level
# On ajoute 1 à la valeur de la variable temporaire pour chaque colonne non vide, à 2 blocs de distance dans chaque direction
scoreboard players set #player temp 0
execute unless blocks ~2 ~2 ~ ~2 ~40 ~ ~2 255 ~ masked run scoreboard players add #player temp 1
execute unless blocks ~-2 ~2 ~ ~-2 ~40 ~ ~-2 255 ~ masked run scoreboard players add #player temp 1
execute unless blocks ~ ~2 ~2 ~ ~40 ~2 ~ 255 ~2 masked run scoreboard players add #player temp 1
execute unless blocks ~ ~2 ~-2 ~ ~40 ~-2 ~ 255 ~-2 masked run scoreboard players add #player temp 1

# On procède de la même manière pour 4 blocs de distance dans chaque direction
execute unless blocks ~4 ~2 ~ ~4 ~40 ~ ~4 255 ~ masked run scoreboard players add #player temp 1
execute unless blocks ~-4 ~2 ~ ~-4 ~40 ~ ~-4 255 ~ masked run scoreboard players add #player temp 1
execute unless blocks ~ ~2 ~4 ~ ~40 ~4 ~ 255 ~4 masked run scoreboard players add #player temp 1
execute unless blocks ~ ~2 ~-4 ~ ~40 ~-4 ~ 255 ~-4 masked run scoreboard players add #player temp 1

# On procède de la même manière pour 7 blocs de distance dans chaque direction
execute unless blocks ~7 ~2 ~1 ~7 ~40 ~1 ~7 255 ~1 masked run scoreboard players add #player temp 1
execute unless blocks ~-7 ~2 ~-1 ~-7 ~40 ~-1 ~-7 255 ~-1 masked run scoreboard players add #player temp 1
execute unless blocks ~1 ~7 ~2 ~1 ~40 ~7 ~1 255 ~7 masked run scoreboard players add #player temp 1
execute unless blocks ~-1 ~7 ~-2 ~-1 ~40 ~-7 ~-1 255 ~-7 masked run scoreboard players add #player temp 1

# On procède de la même manière pour 10 blocs de distance dans chaque direction
execute unless blocks ~10 ~2 ~-2 ~10 ~40 ~-2 ~10 255 ~-2 masked run scoreboard players add #player temp 1
execute unless blocks ~-10 ~2 ~2 ~-10 ~40 ~2 ~-10 255 ~2 masked run scoreboard players add #player temp 1
execute unless blocks ~-2 ~2 ~10 ~-2 ~40 ~10 ~-2 255 ~10 masked run scoreboard players add #player temp 1
execute unless blocks ~2 ~2 ~-10 ~2 ~40 ~-10 ~2 255 ~-10 masked run scoreboard players add #player temp 1

# On enregistre la valeur de l'insideness
scoreboard players operation @a[distance=0] insideness += #player temp
scoreboard players reset #player temp
## TICK DU CALCUL DES VARIABLES JOUEUR
# Exécuté 2 fois par seconde en principe

# On exécute les calculs pour les non-spectateurs, puis pour les spectateurs
# On part du principe qu'une fonction "non-spectateur" définira également le score de tout spectateur situé à la même position
# et on filtre donc parmi les spectateurs, uniquement ceux qui ne se situent pas à la même position qu'un non-spectateur
execute at @a[gamemode=!spectator] run function #tdh:player/scores
execute at @a[gamemode=spectator] unless entity @p[distance=0,gamemode=!spectator] run function #tdh:player/scores_spectateurs
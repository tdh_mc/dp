# On vérifie si le joueur est en train de dormir
execute unless score @p[tag=!IsSleeping,distance=0,gamemode=!spectator] tempsEveil matches 1.. run function tdh:player/scores/test_lit/debut_sommeil
execute if score @p[tag=IsSleeping,distance=0,gamemode=!spectator] tempsEveil matches 1.. run function tdh:player/scores/test_lit/fin_sommeil
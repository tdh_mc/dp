# Fonction tick principale de TDH
# Gère les fréquences de tick des autres fonctions qui s'y raccordent à l'aide de nombreux function tags

# On affiche un message d'erreur si on a terminé incorrectement le tick précédent
execute if score #quickTick id matches 1.. run function tdh:tick/erreur/tick_non_termine

# On ajoute 1 à la valeur actuelle du tick
scoreboard players add #quickTick currentTick 1
# Le quick tick boucle toutes les secondes (20t)
execute if score #quickTick currentTick matches 20.. run scoreboard players set #quickTick currentTick 0

# On a un test différent par valeur possible pour le tick
# tick/8 boucle 8 fois par seconde, tick/4 4 fois par seconde, etc...
# tick/1_2 signifie 1/2 fois par seconde (1 fois toutes les 2 secondes)
execute if score #quickTick currentTick matches 0 run function tdh:tick/8
execute if score #quickTick currentTick matches 1 run function tdh:tick/4
# Rien ne s'exécute au tick 2 pour laisser la place au slowTick
execute if score #quickTick currentTick matches 2 run function tdh:tick/slow_tick
execute if score #quickTick currentTick matches 3 run function tdh:tick/8
execute if score #quickTick currentTick matches 4 run function tdh:tick/2
execute if score #quickTick currentTick matches 5 run function tdh:tick/3
execute if score #quickTick currentTick matches 6 run function tdh:tick/8
execute if score #quickTick currentTick matches 7 run function tdh:tick/4
execute if score #quickTick currentTick matches 8 run function tdh:tick/8
execute if score #quickTick currentTick matches 9 run function tdh:tick/1
execute if score #quickTick currentTick matches 10 run function tdh:tick/8
execute if score #quickTick currentTick matches 11 run function tdh:tick/4
execute if score #quickTick currentTick matches 12 run function tdh:tick/3
execute if score #quickTick currentTick matches 13 run function tdh:tick/8
execute if score #quickTick currentTick matches 14 run function tdh:tick/2
execute if score #quickTick currentTick matches 15 run function tdh:tick/8
execute if score #quickTick currentTick matches 16 run function tdh:tick/4
# Rien ne s'exécute au tick 17 pour laisser la place au slowTick
execute if score #quickTick currentTick matches 17 run function tdh:tick/slow_tick
execute if score #quickTick currentTick matches 18 run function tdh:tick/8
execute if score #quickTick currentTick matches 19 run function tdh:tick/3
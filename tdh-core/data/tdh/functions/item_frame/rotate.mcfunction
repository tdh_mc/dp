# Appeler cette fonction en tant qu'une item frame pour faire tourner l'objet qu'elle contient
# Choisir le nombre de tours avec la variable #itemFrame rotation

# On récupère la rotation actuelle de l'item, que l'on ajoute à la rotation à effectuer
execute store result score #itemFrame temp run data get entity @s ItemRotation
scoreboard players operation #itemFrame rotation += #itemFrame temp

# On replace la rotation entre 0 et 7, puis on l'applique à l'item frame
scoreboard players set #itemFrame temp 8
execute store result entity @s ItemRotation byte 1 run scoreboard players operation #itemFrame rotation %= #itemFrame temp

# On réinitialise la variable temporaire
scoreboard players reset #itemFrame temp
scoreboard players reset #itemFrame rotation
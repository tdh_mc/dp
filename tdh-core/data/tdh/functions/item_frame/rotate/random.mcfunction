# Appeler cette fonction en tant qu'une item frame pour définir sa rotation aléatoirement

scoreboard players set #random min 0
scoreboard players set #random max 8
function tdh:random
execute store result entity @s ItemRotation byte 1 run scoreboard players get #random temp
# Exécuté à une position où l'on veut "refresh" un circuit avec comparateur
# (à proximité du comparateur, à un endroit safe où l'on peut retirer, puis placer un fil de redstone)

setblock ~ ~ ~ redstone_wire
setblock ~ ~ ~ air
## ACTIVATION DU MOTD

# On active le MOTD
scoreboard players set #motd on 1

# On affiche un message de confirmation
tellraw @s [{"text":"Le MOTD est désormais ","color":"gray"},{"text":"actif","color":"green"},{"text":"."}]

# On réaffiche le menu principal
function tdh:core/config
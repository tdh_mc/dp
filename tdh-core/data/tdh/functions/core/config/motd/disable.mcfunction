## DÉSACTIVATION DU MOTD

# On désactive le MOTD
scoreboard players set #motd on 0

# On affiche un message de confirmation
tellraw @s [{"text":"Le MOTD est désormais ","color":"gray"},{"text":"inactif","color":"red"},{"text":"."}]

# On réaffiche le menu principal
function tdh:core/config
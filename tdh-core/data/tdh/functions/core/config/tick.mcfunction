# On tick régulièrement pour lancer les sous-fonctions de prise en compte de la configuration
# Le joueur a une variable configID qui sert à stocker la variable qu'il config, et une variable config qu'il trigger pour communiquer la valeur désirée

# Cette fonction est à destination des administrateurs, donc on peut utiliser execute AS au lieu de AT

# Si on est à l'écran d'accueil, on lance une des fonctions select selon notre choix
execute as @s[scores={configID=9999}] unless score @s config matches 0 run function tdh:core/config/select

# Selon la variable qu'on cherche à config
# - Dans tous les cas (10000+), si on veut revenir au menu (config -120047):
execute as @s[scores={configID=10000..,config=-120047}] run function tdh:core/config


# Options :
# - MOTD (?????)
#execute as @s[scores={configID=10000}] unless score @s config matches ..0 run function tdh:core/config/motd_set


# Tous les joueurs qui viennent de choisir une config retournent au menu principal
execute as @s[scores={configID=10000..}] unless score @s config matches 0 run function tdh:core/config
# Cette fonction est exécutée par un administrateur
# avec configID = 9999 (menu principal) et config = le menu qu'il veut afficher

# Selon le menu qu'on veut afficher

# Options :
# - MOTD (10xxx)
# --- Activer (10010)
execute as @s[scores={config=10010}] run function tdh:core/config/motd/enable
execute as @s[scores={config=10011}] run function tdh:core/config/motd/disable

# - Quitter le configurateur (-1)
execute as @s[scores={config=-1}] run function tdh:config/end
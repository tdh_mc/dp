# Configurateur du pack tdh-core

# On démarre la config et se donne donc le tag Config et ConfigCore
tag @s add Config
tag @s add ConfigCore

# On active l'objectif de configuration à trigger (9999 = Choix d'un paramètre à modifier)
scoreboard players set @s configID 9999
scoreboard players set @s config 0
scoreboard players enable @s config

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-CORE","color":"gold"}]
tellraw @s [{"text":"Voici la configuration actuelle de ","color":"gray"},{"text":"tdh-core","bold":"true"},{"text":" :\n——————————————————————————"}]

# -- Options --
# MOTD (10000)
# Activer le motd (10010)
execute if score #motd on matches 0 run tellraw @s [{"text":"Le MOTD est ","color":"gray"},{"text":"inactif","color":"red","bold":"true","hoverEvent":{"action":"show_text","contents":"Cliquer pour activer le MOTD."},"clickEvent":{"action":"run_command","value":"/trigger config set 10010"}},{"text":"."}]
# Désactiver le motd (10011)
execute unless score #motd on matches 0 run tellraw @s [{"text":"Le MOTD est ","color":"gray"},{"text":"actif","color":"green","bold":"true","hoverEvent":{"action":"show_text","contents":"Cliquer pour désactiver le MOTD."},"clickEvent":{"action":"run_command","value":"/trigger config set 10011"}},{"text":"."}]
# Modifier le motd (?)
execute unless score #motd on matches 0 run tellraw @s [{"text":"Le MOTD est actuellement « ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour spécifier un nouveau MOTD, au format texte JSON."},"clickEvent":{"action":"suggest_command","value":"/data modify storage tdh:core motd set value '[]'"}},{"nbt":"motd","storage":"tdh:core","interpret":"true"},{"text":" »."}]

#tellraw @s [{"text":"——————————————————————————","color":"gray"}]


tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"- Quitter le configurateur","color":"gray","clickEvent":{"action":"run_command","value":"/trigger config set -1"}}]
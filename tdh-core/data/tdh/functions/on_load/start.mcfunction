# On initialise d'abord les variables des différents datapacks, si ce n'a pas déjà été fait
execute unless score #tdh initDone matches 1.. run function tdh:_init
scoreboard players set #tdh initDone 1

# On propose ensuite de configurer chaque datapack
function tdh:config/prompts
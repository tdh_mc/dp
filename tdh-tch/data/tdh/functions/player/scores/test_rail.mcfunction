# Check de présence sur des rails
# On reset l'advancement des joueurs en creative/spectateur
advancement revoke @a[gamemode=!adventure,gamemode=!survival,distance=0] only tdh:bloc/rail

# On fait le test pour les joueurs en aventure/survie
execute at @p[distance=0,gamemode=!spectator,gamemode=!creative,tag=!MortElectrocute,advancements={tdh:bloc/rail=true}] run function tch:metro/rail/test_joueur

# On retire le tag aux joueurs morts électrocutés qui ont respawn
function tch:metro/rail/test_fin_electrocution
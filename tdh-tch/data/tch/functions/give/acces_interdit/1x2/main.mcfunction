# ACCES INTERDIT

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"Choisissez l'orientation du panneau ","color":"gray"},{"text":"accès interdit 1x2","color":"#d81d0c","bold":"true"},{"text":" :"}]
tellraw @s [{"text":"","color":"gray"},{"text":"← Gauche","color":"gold","clickEvent":{"action":"run_command","value":"/minecraft:give @s iron_ingot{CustomModelData:111}"},"hoverEvent":{"action":"show_text","value":[{"text":"Interdit de passer à gauche de ce panneau."}]}},{"text":" • "},{"text":"Droite →","color":"yellow","clickEvent":{"action":"run_command","value":"/minecraft:give @s iron_ingot{CustomModelData:112}"},"hoverEvent":{"action":"show_text","value":[{"text":"Interdit de passer à droite de ce panneau."}]}}]
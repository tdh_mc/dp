# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"Panneau TVQ","color":"gray"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 9999
# On ajoute la valeur correspondant à notre type de panneau
function tch:give/acces_quais/_choix_type/_id_panneau

# On affiche ensuite l'écran de sélection de la direction
function tch:give/acces_quais/_choix_fleche_tvq
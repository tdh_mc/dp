# On se donne la version du panneau avec flèche à gauche du texte
function tch:give/acces_quais/_choix_fleche/_g

# On se donne également la bonne flèche
give @s[tag=AQBas] iron_nugget{CustomModelData:104}
give @s[tag=!AQBas,tag=!AQHaut] iron_nugget{CustomModelData:204}
give @s[tag=AQHaut] iron_nugget{CustomModelData:304}
# On se donne la version du panneau avec flèche à droite du texte
function tch:give/acces_quais/_choix_fleche/_d

# On se donne également la bonne flèche
# Comme on a un panneau version D, on summon également l'item pour qu'il apparaisse dans le bon ordre dans l'inventaire
execute at @s run summon item ~ ~ ~ {Tags:["TempAQItem"],Age:5950s,PickupDelay:1s,Item:{Count:1b,id:"minecraft:iron_nugget",tag:{CustomModelData:0}}}

execute as @s[tag=AQBas] run data modify entity @e[type=item,tag=TempAQItem,limit=1] Item.tag.CustomModelData set value 116
execute as @s[tag=!AQBas,tag=!AQHaut] run data modify entity @e[type=item,tag=TempAQItem,limit=1] Item.tag.CustomModelData set value 216
execute as @s[tag=AQHaut] run data modify entity @e[type=item,tag=TempAQItem,limit=1] Item.tag.CustomModelData set value 316

data modify entity @e[type=item,tag=TempAQItem,limit=1] Owner set from entity @s UUID
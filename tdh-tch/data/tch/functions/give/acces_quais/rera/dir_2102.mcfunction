# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"","color":"#e2231a"},{"text":"\u1a38","color":"gray"},{"text":"\u1a39"},{"text":" → ","color":"gray"},{"text":"Toutes directions (branche "},{"entity":"@e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=2102},limit=1]","nbt":"Item.tag.display.station.deDu"},{"entity":"@e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=2102},limit=1]","nbt":"Item.tag.display.station.nom"},{"text":")"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 2192
# On ajoute la valeur correspondant à notre type de panneau
function tch:give/acces_quais/_choix_type/_id_panneau

# On affiche ensuite l'écran de sélection de la direction
function tch:give/acces_quais/_choix_fleche
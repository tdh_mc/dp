# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
# La valeur de départ est l'idLine correspondant à cette direction
tellraw @s [{"text":"\u1a38","color":"gray"},{"text":"\u1a39","color":"#e2231a"},{"text":"\u1a20","color":"gray"},{"text":"\u1a23","color":"#6ec4e8"},{"text":"\u1a26","color":"#ff7e2e"}]
scoreboard players set @s idPanneau 21351
# On ajoute la valeur correspondant à notre type de panneau
function tch:give/acces_quais/_choix_type/_id_panneau_multiple

# On affiche ensuite l'écran de sélection de la direction
function tch:give/acces_quais/_choix_fleche_multiple
# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
# La valeur de départ est l'idLine correspondant à cette direction
tellraw @s [{"text":"\u1a20","color":"gray"},{"text":"\u1a21","color":"#ffcd00"},{"text":"\u1a22","color":"#003ca6"},{"text":"\u1a24","color":"#cf009e"}]
scoreboard players set @s idPanneau 1240
# On ajoute la valeur correspondant à notre type de panneau
function tch:give/acces_quais/_choix_type/_id_panneau_multiple

# On affiche ensuite l'écran de sélection de la direction
function tch:give/acces_quais/_choix_fleche_multiple
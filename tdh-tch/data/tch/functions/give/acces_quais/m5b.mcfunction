# Détection des terminus
scoreboard players set #tch idTerminus 55
function tch:tag/terminus_pc
scoreboard players set #tch idTerminus 56
function tch:tag/terminus_pc

# Choisir une direction pour l'accès aux quais :
tellraw @s [{"text":"Direction : ","color":"gray"},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=55},limit=1]","nbt":"Item.tag.display.station.prefixe","extra":[{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=55},limit=1]","nbt":"Item.tag.display.station.nom"}],"color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/acces_quais/m5b/55"},"hoverEvent":{"action":"show_text","value":[{"text":"Direction "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=55},limit=1]","nbt":"Item.tag.display.station.prefixe"},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=55},limit=1]","nbt":"Item.tag.display.station.nom"},{"text":"."}]}},{"text":" • "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=56},limit=1]","nbt":"Item.tag.display.station.prefixe","extra":[{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=56},limit=1]","nbt":"Item.tag.display.station.nom"}],"color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/acces_quais/m5b/56"},"hoverEvent":{"action":"show_text","value":[{"text":"Direction "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=56},limit=1]","nbt":"Item.tag.display.station.prefixe"},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=56},limit=1]","nbt":"Item.tag.display.station.nom"},{"text":"."}]}},{"text":" • "},{"text":"Dir","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/acces_quais/m5b/dir"},"hoverEvent":{"action":"show_text","value":[{"text":"Les deux sens de circulation dans une seule direction."}]}}]

# Suppression des tags !
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
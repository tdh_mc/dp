# ACCES_QUAIS

tellraw @s [{"text":"\n==== hgive ","color":"#cc4400","bold":"true"},{"text":"accès quais","color":"#d81d0c"},{"text":" ===="}]

# Détection automatique
# On enregistre le nombre de lignes alentour (distance<200)
scoreboard players set @s temp 0
execute at @e[distance=..200,tag=NumLigne] run scoreboard players add @s temp 1

# Si on a trouvé 0 ligne, on lance la fonction liste
execute as @s[scores={temp=0}] run function tch:give/acces_quais/_list

# Sinon, on propose les lignes pertinentes
execute as @s[scores={temp=1..}] run function tch:give/acces_quais/_near

# On propose dans tous les cas de régler les paramètres des panneaux
function tch:give/acces_quais/_choix_type

# On réinitialise la variable temporaire
scoreboard players reset @s temp
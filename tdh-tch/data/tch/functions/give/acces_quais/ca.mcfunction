# Détection des terminus
scoreboard players set #tch idTerminus 4011
function tch:tag/terminus_pc
scoreboard players set #tch idTerminus 4012
function tch:tag/terminus_pc

# Choisir une direction pour l'accès aux quais :
tellraw @s [{"text":"Direction : ","color":"gray"},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=4011},limit=1]","nbt":"Item.tag.display.station.prefixe","extra":[{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=4011},limit=1]","nbt":"Item.tag.display.station.nom"}],"color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/acces_quais/ca/4011"},"hoverEvent":{"action":"show_text","value":[{"text":"Direction "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=4011},limit=1]","nbt":"Item.tag.display.station.prefixe"},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=4011},limit=1]","nbt":"Item.tag.display.station.nom"},{"text":"."}]}},{"text":" • "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=4012},limit=1]","nbt":"Item.tag.display.station.prefixe","extra":[{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=4012},limit=1]","nbt":"Item.tag.display.station.nom"}],"color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/acces_quais/ca/4012"},"hoverEvent":{"action":"show_text","value":[{"text":"Direction "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=4012},limit=1]","nbt":"Item.tag.display.station.prefixe"},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=4012},limit=1]","nbt":"Item.tag.display.station.nom"},{"text":"."}]}},{"text":" • "},{"text":"Dir","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/acces_quais/ca/dir"},"hoverEvent":{"action":"show_text","value":[{"text":"Les deux sens de circulation dans une seule direction."}]}}]

# Suppression des tags !
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
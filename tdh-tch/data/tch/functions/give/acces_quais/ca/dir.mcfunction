# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"\u1a40","color":"gray"},{"text":"\u1a41","color":"#4b7e99"},{"text":" → "},{"text":"Toutes directions","color":"#4b7e99"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 4010

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
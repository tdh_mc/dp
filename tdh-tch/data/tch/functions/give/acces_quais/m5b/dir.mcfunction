# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"\u1a20","color":"gray"},{"text":"\u1a26","color":"#ff7e2e"},{"text":" → "},{"text":"Toutes directions","color":"#ff7e2e"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 54
# On ajoute la valeur correspondant à notre type de panneau
function tch:give/acces_quais/_choix_type/_id_panneau

# On affiche ensuite l'écran de sélection de la direction
function tch:give/acces_quais/_choix_fleche
# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"","color":"#007852"},{"text":"\u1a20","color":"gray"},{"text":"\u1a2f"},{"text":" → ","color":"gray"},{"entity":"@e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=121},limit=1]","nbt":"Item.tag.display.station.prefixe"},{"entity":"@e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=121},limit=1]","nbt":"Item.tag.display.station.nom"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 121
# On ajoute la valeur correspondant à notre type de panneau
function tch:give/acces_quais/_choix_type/_id_panneau

# On affiche ensuite l'écran de sélection de la direction
function tch:give/acces_quais/_choix_fleche
# Détection des terminus
scoreboard players set #tch idTerminus 71
function tch:tag/terminus_pc
scoreboard players set #tch idTerminus 72
function tch:tag/terminus_pc

# Choisir une direction pour le PID :
tellraw @s [{"text":"Choisissez une direction :"}]
tellraw @s [{"text":""},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=71},limit=1]","nbt":"Item.tag.display.station.prefixe","extra":[{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=71},limit=1]","nbt":"Item.tag.display.station.nom"}],"color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m7/71"}},{"text":" • "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=72},limit=1]","nbt":"Item.tag.display.station.prefixe","extra":[{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=72},limit=1]","nbt":"Item.tag.display.station.nom"}],"color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m7/72"}},{"text":" • "},{"text":"Terminus","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m7/terminus"}}]

# Suppression des tags !
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
# On enregistre dans le storage toutes les lignes siel/pidondantes

# On crée une liste pour contenir toutes les lignes trouvées
data modify storage tch:give pid set value []

# Si on détecte l'idLine, on ajoute cette ligne à la liste
# Métro
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=11..12}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m1"}},{"text":"\\u1a21","color":"#ffcd00"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=21..22}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m2"}},{"text":"\\u1a22","color":"#003ca6"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=31..32}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m3"}},{"text":"\\u1a23","color":"#6ec4e8"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=41..42}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m4"}},{"text":"\\u1a24","color":"#cf009e"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=51..52}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m5a"}},{"text":"\\u1a25","color":"#ff7e2e"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=54..55}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m5b"}},{"text":"\\u1a26","color":"#ff7e2e"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=61..62}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m6"}},{"text":"\\u1a27","color":"#60bb39"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=71..72}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m7"}},{"text":"\\u1a28","color":"#fa9aba"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=73..74}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m7b"}},{"text":"\\u1a29","color":"#6eca97"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=81..82}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m8"}},{"text":"\\u1a2a","color":"#e19bdf"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=83..84}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m8b"}},{"text":"\\u1a2b","color":"#345663"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=91..92}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m9"}},{"text":"\\u1a2c","color":"#b6bd00"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=101..102}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m10"}},{"text":"\\u1a2d","color":"#c9910d"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=111..112}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m11"}},{"text":"\\u1a2e","color":"#704b1c"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=121..122}] run data modify storage tch:give pid append value '[{"text":"\\u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m12"}},{"text":"\\u1a2f","color":"#007852"}]'

# RER
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=2100..2199}] run data modify storage tch:give pid append value '[{"text":"\\u1a38","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/rera"}},{"text":"\\u1a39","color":"#e2231a"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=2200..2299}] run data modify storage tch:give pid append value '[{"text":"\\u1a38","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/rerb"}},{"text":"\\u1a3a","color":"#3c91dc"}]'

# Câble
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=4011..4012}] run data modify storage tch:give pid append value '[{"text":"\\u1a40","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/ca"}},{"text":"\\u1a41","color":"#4b7e99"}]'
execute if entity @e[tag=NumLigne,distance=..200,scores={idLine=4021..4022}] run data modify storage tch:give pid append value '[{"text":"\\u1a40","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/cb"}},{"text":"\\u1a42","color":"#c39108"}]'


# Ensuite, on définit notre variable temp au nombre d'éléments présents dans la liste
execute store result score @s temp run data get storage tch:give pid

# On affiche le message selon le nombre d'éléments
tellraw @s [{"text":"hgive a détecté les lignes suivantes :","color":"gray"}]
execute as @s[scores={temp=1}] run tellraw @s {"nbt":"pid[0]","storage":"tch:give","interpret":"true"}
execute as @s[scores={temp=2}] run tellraw @s [{"nbt":"pid[0]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[1]","storage":"tch:give","interpret":"true"}]
execute as @s[scores={temp=3}] run tellraw @s [{"nbt":"pid[0]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[1]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[2]","storage":"tch:give","interpret":"true"}]
execute as @s[scores={temp=4}] run tellraw @s [{"nbt":"pid[0]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[1]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[2]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[3]","storage":"tch:give","interpret":"true"}]
execute as @s[scores={temp=5}] run tellraw @s [{"nbt":"pid[0]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[1]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[2]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[3]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[4]","storage":"tch:give","interpret":"true"}]
execute as @s[scores={temp=6}] run tellraw @s [{"nbt":"pid[0]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[1]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[2]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[3]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[4]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[5]","storage":"tch:give","interpret":"true"}]
execute as @s[scores={temp=7..}] run tellraw @s [{"nbt":"pid[0]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[1]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[2]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[3]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[4]","storage":"tch:give","interpret":"true"},{"text":"•"},{"nbt":"pid[5]","storage":"tch:give","interpret":"true"},{"text":" et d'autres encore..."}]

tellraw @s[scores={temp=..6}] {"text":"\nAfficher plutôt la liste complète...","color":"yellow","italic":"true","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/_list"}}
tellraw @s[scores={temp=7..}] {"text":"\nAffichez plutôt la liste complète, vu le nombre de lignes qui passent ici, ça ne changera pas grand chose...","color":"yellow","italic":"true","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/_list"}}


# On réinitialise le storage
data modify storage tch:give pid set value []
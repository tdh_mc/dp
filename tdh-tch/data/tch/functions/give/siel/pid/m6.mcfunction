# Détection des terminus
scoreboard players set #tch idTerminus 61
function tch:tag/terminus_pc
scoreboard players set #tch idTerminus 62
function tch:tag/terminus_pc

# Choisir une direction pour le PID :
tellraw @s [{"text":"Choisissez une direction :"}]
tellraw @s [{"text":""},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=61},limit=1]","nbt":"Item.tag.display.station.prefixe","extra":[{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=61},limit=1]","nbt":"Item.tag.display.station.nom"}],"color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m6/61"}},{"text":" • "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=62},limit=1]","nbt":"Item.tag.display.station.prefixe","extra":[{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=62},limit=1]","nbt":"Item.tag.display.station.nom"}],"color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m6/62"}},{"text":" • "},{"text":"Terminus","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/m6/terminus"}}]

# Suppression des tags !
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
# PID

# Détection automatique
# On enregistre le nombre de lignes alentour (distance<200)
scoreboard players set @s temp 0
execute at @e[distance=..200,tag=NumLigne] run scoreboard players add @s temp 1

# Si on a trouvé 0 ligne, on lance la fonction liste
execute as @s[scores={temp=0}] run function tch:give/siel/pid/_list

# Sinon, on propose les lignes pertinentes
execute as @s[scores={temp=1..}] run function tch:give/siel/pid/_near

# On affiche dans tous les cas la possibilité de choisir un SACD (timer)
tellraw @s [{"text":"","color":"gray","italic":"true"},{"text":"Obtenir un ","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/sacd"},"extra":[{"text":"SACD","hoverEvent":{"action":"show_text","contents":[{"text":""},{"text":"S","color":"gold"},{"text":"ystème "},{"text":"A","color":"gold"},{"text":"utomatique de "},{"text":"C","color":"gold"},{"text":"omptage "},{"text":"D","color":"gold"},{"text":"écroissant"}]}},{"text":" standard"}]},{"text":" • "},{"text":"secondaire","color":"yellow","hoverEvent":{"action":"show_text","contents":[{"text":"Pour la direction secondaire d'un PID sur ligne à branches. Se place sur le côté droit du bloc situé derrière le côté droit du PID."}]},"clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/sacd2"}}]

# On réinitialise la variable temporaire
scoreboard players reset @s temp
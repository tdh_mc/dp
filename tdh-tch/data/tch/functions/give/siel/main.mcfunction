# SIEL

tellraw @s [{"text":"\n==== hgive ","color":"#cc4400","bold":"true"},{"text":"info","color":"#d81d0c"},{"text":" ===="}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"- SIEL Horaire M3","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/horaire_m3"},"hoverEvent":{"action":"show_text","contents":"Obsolète"}}]
tellraw @s [{"text":"- Interdit de fumer","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/info_fumer"},"hoverEvent":{"action":"show_text","contents":"2x2"}}]
tellraw @s [{"text":"- Portes palières","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/signal_sonore"},"hoverEvent":{"action":"show_text","contents":"Signal sonore - 1x2"}}]
tellraw @s [{"text":"- Réservé carte Gold","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/reserve_gold"},"hoverEvent":{"action":"show_text","contents":"Réservé Gold - 1x1"}}]
tellraw @s [{"text":"- SIEL Trafic","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/trafic/main"},"hoverEvent":{"action":"show_text","contents":"Ecrans d'information trafic"}}]
tellraw @s [{"text":"- PID","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/pid/_main"},"hoverEvent":{"action":"show_text","contents":"Panneau Indicateur Directionnel"}}]
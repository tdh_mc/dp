# SIEL

# Shortcut
tellraw @s [{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n","color":"gray"},{"text":"Pour accéder rapidement à cet écran, tapez "},{"text":"/hgive","color":"light_purple"},{"text":"i","color":"aqua","bold":"true"},{"text":" (comme "},{"text":"i","color":"aqua"},{"text":"nfo).\n"},{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~","color":"gray"}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"Choisissez une information :"}]
tellraw @s [{"text":"- Hors service","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/trafic/3x2/hs"}}]
tellraw @s [{"text":"- M5a","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/trafic/3x2/5a"}}]
tellraw @s [{"text":"- M7bis","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/trafic/3x2/7bis"}}]
tellraw @s [{"text":"- M8","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/trafic/3x2/8"}}]
tellraw @s [{"text":"- M8bis","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/trafic/3x2/8bis"}}]
tellraw @s [{"text":"- RER A","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/trafic/3x2/rera"}}]
tellraw @s [{"text":"- RER B","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/siel/trafic/3x2/rerb"}}]
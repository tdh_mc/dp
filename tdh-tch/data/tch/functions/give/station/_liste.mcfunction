# On affiche d’abord un message informatif
tellraw @s [{"text":"Pour utiliser cette liste, il vous faudra ","color":"red"},{"text":"renseigner manuellement l’ID de la station","bold":"true"},{"text":", puis "},{"text":"cliquer sur le lien en bas de la liste","bold":"true"},{"text":".\n"},{"text":"Cliquer ici pour pré-remplir la commande à utiliser.","italic":"true","clickEvent":{"action":"suggest_command","value":"/scoreboard players set @s idStation "}}]

# Pour chaque station de la liste, on fait un rendu de son code station, avec en hoverEvent son nom complet + code numérique
# On utilise un tag temporaire pour pouvoir trier par ordre régional (à défaut de pouvoir faire simplement l’ordre alphabétique)
execute as @e[type=item_frame,tag=tchStationPC] run tag @s add tempStationPC
# On initialise un storage au préalable pour contenir l’ensemble des codes station
data modify storage tch:give station set value []
# On initialise également une variable qui nous permettra de savoir si on doit afficher le prochain code station en GOLD ou YELLOW
scoreboard players set #tch temp9 1
# On initialise enfin une variable valant 100 pour vérifier si les ID station sont multiples de 100
scoreboard players set #tch temp7 100
# Ensuite, on exécute la récursion
execute positioned as @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:give/station/liste/recursion

# On affiche le contenu du storage
tellraw @s [{"storage":"tch:give","nbt":"station[]","interpret":true,"separator":{"text":" • ","color":"gray"}},{"text":"\n\nUne fois la première commande exécutée, ","color":"gray","extra":[{"text":"cliquez ici","color":"red","clickEvent":{"action":"run_command","value":"/function tch:give/station/_auto_give"}},{"text":" !"}]}]

# On supprime les données temporaires
tag @e[type=item_frame,tag=tempStationPC] remove tempStationPC
scoreboard players reset #tch temp9
data remove storage tch:give station
# On exécute cette fonction en ayant taggé toutes les stations pas encore rendues avec le tag tempStationPC
# On exécute cette fonction à la position d’une item frame TexteTemporaire pour pouvoir faire les rendus facilement

# On récupère d’abord le plus petit ID de station restant
scoreboard players set #tch idStation 999999999
scoreboard players operation #tch idStation < @e[type=item_frame,tag=tempStationPC] idStation

# On fait ensuite le rendu du texte pour la station ayant cet ID
execute as @e[type=item_frame,tag=tempStationPC] if score @s idStation = #tch idStation run function tch:give/station/liste/rendu

# On inverse la valeur de #tch temp9 (qui vaut 1 si on doit mettre du GOLD et 2 si on doit mettre du YELLOW)
scoreboard players add #tch temp9 1
execute if score #tch temp9 matches 3 run scoreboard players set #tch temp9 1

# S’il reste des stations ayant un ID, on relance la récursion
execute if entity @e[type=item_frame,tag=tempStationPC,scores={idStation=1..}] run function tch:give/station/liste/recursion
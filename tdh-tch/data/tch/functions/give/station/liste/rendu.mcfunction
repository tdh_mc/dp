# On exécute cette fonction en tant qu’une station dont on souhaite faire le « rendu »
# (stocker son code station, nom complet et ID de station en vue d’un affichage ultérieur)

# On vérifie si l’ID station est multiple de 100
scoreboard players operation #tch temp8 = #tch idStation
scoreboard players operation #tch temp8 /= #tch temp7
scoreboard players operation #tch temp8 *= #tch temp7

# On fait d’abord le rendu sur le panneau TexteTemporaire
# On se donne un tag pour pouvoir se retrouver
tag @s add renduActuel
# Si l’ID station est un multiple de 100, on ajoute un retour à la ligne
execute if score #tch temp8 = #tch idStation run function tch:give/station/liste/rendu_retour_ligne
execute unless score #tch temp8 = #tch idStation run function tch:give/station/liste/rendu_sans_retour_ligne

# On copie ensuite ce rendu dans le storage
data modify storage tch:give station append from block ~ ~ ~ Text4

# On retire le tag de la station
tag @s remove tempStationPC
tag @s remove renduActuel
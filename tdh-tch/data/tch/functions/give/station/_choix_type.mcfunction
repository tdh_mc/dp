# Si on n'a pas de tag pour l'instant, on se donne le tag par défaut
tag @s[tag=!STGrandCentre,tag=!STGrandDecale,tag=!STPetitCentre,tag=!STPetitDecale] add STPetitCentre
# On affiche ensuite un message indiquant le type de panneau actuel
execute as @s[tag=STGrandCentre] run data modify storage tch:give station_format set value "Grand–Centré"
execute as @s[tag=STGrandDecale] run data modify storage tch:give station_format set value "Grand–Décalé"
execute as @s[tag=STPetitCentre] run data modify storage tch:give station_format set value "Petit–Centré"
execute as @s[tag=STPetitDecale] run data modify storage tch:give station_format set value "Petit–Décalé"
tellraw @s [{"text":"Type de panneau actuel: ","color":"gray"},{"storage":"tch:give","nbt":"station_format","color":"yellow"},{"text":"."}]
data remove storage tch:give station_format
# On propose au joueur de changer le type de panneau
tellraw @s [{"text":"Nouveau type de panneau: Petit ","color":"gray"},{"text":"Centré","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/station/_choix_type/petit_centre"},"hoverEvent":{"action":"show_text","contents":"Largeur optimale: 3 blocs"}},{"text":" • "},{"text":"Décalé","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/station/_choix_type/petit_decale"},"hoverEvent":{"action":"show_text","contents":"Largeur optimale: 2 blocs"}},{"text":" — Grand ","color":"gray"},{"text":"Centré","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/station/_choix_type/grand_centre"},"hoverEvent":{"action":"show_text","contents":"Largeur optimale: 4 blocs"}},{"text":" • "},{"text":"Décalé","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/station/_choix_type/grand_decale"},"hoverEvent":{"action":"show_text","contents":"Largeur optimale: 5 blocs"}}]
# On invoque l'objet correspondant en lui donnant le bon custommodeldata
execute at @s run summon item ~ ~ ~ {Tags:["TempSTItem"],Age:5950s,PickupDelay:0s,Item:{Count:1b,id:"minecraft:iron_ingot",tag:{CustomModelData:0}}}
execute store result entity @e[type=item,tag=TempSTItem,sort=nearest,limit=1] Item.tag.CustomModelData int 1 run scoreboard players get @s idPanneau
data modify entity @e[type=item,tag=TempSTItem,sort=nearest,limit=1] Owner set from entity @s UUID

# On retire le tag temporaire de l'item invoqué
tag @e[type=item,tag=TempSTItem] remove TempSTItem
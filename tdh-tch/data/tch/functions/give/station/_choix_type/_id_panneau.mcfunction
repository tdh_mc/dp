# On ajoute la valeur du type de panneau à celle de l'ID station déjà enregistrée
scoreboard players add @s[tag=STPetitCentre] idPanneau 0
scoreboard players add @s[tag=STPetitDecale] idPanneau 10000
scoreboard players add @s[tag=STGrandCentre] idPanneau 20000
scoreboard players add @s[tag=STGrandDecale] idPanneau 40000

# On summon ensuite le panneau
function tch:give/station/_choix_type/_don_panneau

# Si on a un grand panneau, on se donne également la partie droite
scoreboard players add @s[tag=STGrandCentre] idPanneau 10000
scoreboard players add @s[tag=STGrandDecale] idPanneau 10000
execute as @s[tag=STGrandCentre] run function tch:give/station/_choix_type/_don_panneau
execute as @s[tag=STGrandDecale] run function tch:give/station/_choix_type/_don_panneau
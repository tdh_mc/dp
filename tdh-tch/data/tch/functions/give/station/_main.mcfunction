# STATION

tellraw @s [{"text":"\n==== hgive ","color":"#cc4400","bold":"true"},{"text":"station","color":"#d81d0c"},{"text":" ===="}]

# On assigne une variable au joueur pour savoir dans quelle station il se trouve
scoreboard players set @s idStation 0
scoreboard players operation @s idStation = @e[tag=NomStation,sort=nearest,limit=1,distance=..250] idStation

# Éventuellement la station détectée
execute if score @s idStation matches 1.. run function tch:give/station/_auto_detect


# Si on a trouvé une station correspondante, on propose éventuellement d'afficher la liste complète
execute if score @s idStation matches 1.. run tellraw @s [{"text":"\nAfficher plutôt la liste complète...","color":"yellow","italic":"true","clickEvent":{"action":"run_command","value":"/function tch:give/station/_liste"}}]
# Si on n'en a pas trouvé, on affiche directement la liste
execute if score @s idStation matches 0 run function tch:give/station/_liste

# On propose dans tous les cas le choix d'un type de panneau
function tch:give/station/_choix_type
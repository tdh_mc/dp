# Auto-détection de la station
# On a l’ID station détecté à proximité dans une variable @s idStation

# On tente de récupérer le PC de station approprié
function tch:tag/station_pc

# Si on a trouvé un PC de station, on affiche le message
execute if entity @e[type=item_frame,tag=ourStationPC] run tellraw @s [{"text":"Station détectée: ","color":"yellow"},{"entity":"@e[type=item_frame,tag=ourStationPC,limit=1]","nbt":"Item.tag.display.station.nom","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/station/_auto_give"}}]
# Sinon, on affiche un message d’erreur
execute unless entity @e[type=item_frame,tag=ourStationPC] run tellraw @s [{"text":"Station détectée: ","color":"red"},{"text":"PC INTROUVABLE !","bold":"true","hoverEvent":{"action":"show_text","contents":{"text":"C’est con !","color":"red"}}}]

# On retire le tag temporaire
tag @e[type=item_frame,tag=ourStationPC] remove ourStationPC
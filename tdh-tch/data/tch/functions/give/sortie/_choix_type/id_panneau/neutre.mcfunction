# On ajoute la valeur du type de panneau à celle de l'ID station déjà enregistrée
scoreboard players add @s[tag=SOPetitBas] idPanneau 1000
scoreboard players add @s[tag=SOPetitCentre] idPanneau 2000
scoreboard players add @s[tag=SOPetitHaut] idPanneau 3000
scoreboard players add @s[tag=SOGrandBas] idPanneau 4000
scoreboard players add @s[tag=SOGrandHaut] idPanneau 5000
scoreboard players add @s[tag=SO3x1] idPanneau 10000
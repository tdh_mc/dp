# On ajoute la valeur du type de panneau à celle de l'ID station déjà enregistrée
scoreboard players add @s[tag=!SO3x1,tag=SOPetitBas] idPanneau 1000000
scoreboard players add @s[tag=!SO3x1,tag=SOPetitCentre] idPanneau 2000000
scoreboard players add @s[tag=!SO3x1,tag=SOPetitHaut] idPanneau 3000000
scoreboard players add @s[tag=SO3x1,tag=SOPetitBas] idPanneau 4000000
scoreboard players add @s[tag=SO3x1,tag=SOPetitCentre] idPanneau 5000000
scoreboard players add @s[tag=SO3x1,tag=SOPetitHaut] idPanneau 6000000
# Si on a une version Grande, comme cela n'existe pas pour les panneaux détaillés, on se donne le panneau petit correspondant
scoreboard players add @s[tag=!SO3x1,tag=SOGrandBas] idPanneau 1000000
scoreboard players add @s[tag=!SO3x1,tag=SOGrandHaut] idPanneau 3000000
scoreboard players add @s[tag=SO3x1,tag=SOGrandBas] idPanneau 4000000
scoreboard players add @s[tag=SO3x1,tag=SOGrandHaut] idPanneau 6000000
tellraw @s[tag=SOGrandBas] [{"text":"Attention","color":"red"},{"text":" : Les panneaux Sortie par station n'existent pas en version élargie. ","color":"gray"},{"text":"Hgive","color":"gold"},{"text":" vous donnera en compensation un panneau de taille standard respectant vos autres paramètres.","color":"gray"}]
tellraw @s[tag=SOGrandHaut] [{"text":"Attention","color":"red"},{"text":" : Les panneaux Sortie par station n'existent pas en version élargie. ","color":"gray"},{"text":"Hgive","color":"gold"},{"text":" vous donnera en compensation un panneau de taille standard respectant vos autres paramètres.","color":"gray"}]
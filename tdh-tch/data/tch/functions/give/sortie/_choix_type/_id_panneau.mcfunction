# Si notre score actuel est inférieur à 10000 (= un panneau sortie neutre et pas un panneau détaillée par station,
# - puisque les panneaux Station commencent à 10001 (idStation = 1000 et idSortie = 1)),
# On exécute la fonction pour les panneaux neutres ou station selon le cas
execute if score @s idPanneau matches ..9999 run tag @s add SONeutreEtNum
execute as @s[tag=SONeutreEtNum] run function tch:give/sortie/_choix_type/id_panneau/neutre
execute as @s[tag=!SONeutreEtNum] run function tch:give/sortie/_choix_type/id_panneau/station
tag @s remove SONeutreEtNum
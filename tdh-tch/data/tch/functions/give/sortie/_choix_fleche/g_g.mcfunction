# On se donne la version du panneau avec flèche à gauche du texte
function tch:give/sortie/_choix_fleche/_g

# On se donne également la bonne flèche
give @s[tag=SOPetitBas] blue_dye{CustomModelData:103}
give @s[tag=SOPetitCentre] blue_dye{CustomModelData:203}
give @s[tag=SOPetitHaut] blue_dye{CustomModelData:303}
give @s[tag=SOGrandBas] blue_dye{CustomModelData:403}
give @s[tag=SOGrandHaut] blue_dye{CustomModelData:503}
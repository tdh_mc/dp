# On se donne la version du panneau avec flèche à droite du texte
function tch:give/sortie/_choix_fleche/_d

# On se donne également la bonne flèche
# Comme on a un panneau version D, on summon également l'item pour qu'il apparaisse dans le bon ordre dans l'inventaire
execute at @s run summon item ~ ~ ~ {Tags:["TempSOItem"],Age:5950s,PickupDelay:1s,Item:{Count:1b,id:"minecraft:blue_dye",tag:{CustomModelData:0}}}

execute as @s[tag=SOPetitBas] run data modify entity @e[type=item,tag=TempSOItem,limit=1] Item.tag.CustomModelData set value 113
execute as @s[tag=SOPetitCentre] run data modify entity @e[type=item,tag=TempSOItem,limit=1] Item.tag.CustomModelData set value 213
execute as @s[tag=SOPetitHaut] run data modify entity @e[type=item,tag=TempSOItem,limit=1] Item.tag.CustomModelData set value 313
execute as @s[tag=SOGrandBas] run data modify entity @e[type=item,tag=TempSOItem,limit=1] Item.tag.CustomModelData set value 413
execute as @s[tag=SOGrandHaut] run data modify entity @e[type=item,tag=TempSOItem,limit=1] Item.tag.CustomModelData set value 513

data modify entity @e[type=item,tag=TempSOItem,limit=1] Owner set from entity @s UUID
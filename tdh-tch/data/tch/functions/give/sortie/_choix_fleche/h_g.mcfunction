# On se donne la version du panneau avec flèche à gauche du texte
function tch:give/sortie/_choix_fleche/_g

# On se donne également la bonne flèche
give @s[tag=SOPetitBas] blue_dye{CustomModelData:105}
give @s[tag=SOPetitCentre] blue_dye{CustomModelData:205}
give @s[tag=SOPetitHaut] blue_dye{CustomModelData:305}
give @s[tag=SOGrandBas] blue_dye{CustomModelData:405}
give @s[tag=SOGrandHaut] blue_dye{CustomModelData:505}
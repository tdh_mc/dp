# Si on n'a pas de tag pour l'instant, on se donne le tag par défaut
tag @s[tag=!SOGrandHaut,tag=!SOGrandBas,tag=!SOPetitHaut,tag=!SOPetitBas] add SOPetitCentre
# On affiche ensuite un message indiquant le type de panneau actuel
execute as @s[tag=SOGrandBas] run data modify storage tch:give sortie_format set value "Grand–Bas"
execute as @s[tag=SOGrandHaut] run data modify storage tch:give sortie_format set value "Grand–Haut"
execute as @s[tag=SOPetitBas] run data modify storage tch:give sortie_format set value "Petit–Bas"
execute as @s[tag=SOPetitCentre] run data modify storage tch:give sortie_format set value "Petit–Centré"
execute as @s[tag=SOPetitHaut] run data modify storage tch:give sortie_format set value "Petit–Haut"
execute as @s[tag=SO3x1] run data modify storage tch:give sortie_largeur set value "3x1"
execute as @s[tag=!SO3x1] run data modify storage tch:give sortie_largeur set value "2x1"
tellraw @s [{"text":"Type de panneau actuel: ","color":"gray"},{"storage":"tch:give","nbt":"sortie_format","color":"yellow"},{"text":" ("},{"storage":"tch:give","nbt":"sortie_largeur","color":"yellow"},{"text":")."}]
data remove storage tch:give sortie_format
data remove storage tch:give sortie_largeur
# On propose au joueur de changer le type de panneau
tellraw @s [{"text":"Nouveau type de panneau: Petit ","color":"gray"},{"text":"Bas","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/sortie/_choix_type/petit_bas"},"hoverEvent":{"action":"show_text","contents":"Taille standard, en-dessous du bloc"}},{"text":" • "},{"text":"Centre","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/sortie/_choix_type/petit_centre"},"hoverEvent":{"action":"show_text","contents":"Taille standard, au centre du bloc"}},{"text":" • "},{"text":"Haut","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/sortie/_choix_type/petit_haut"},"hoverEvent":{"action":"show_text","contents":"Taille standard, au-dessus du bloc"}},{"text":" — Grand ","color":"gray"},{"text":"Bas","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/sortie/_choix_type/grand_bas"},"hoverEvent":{"action":"show_text","contents":"Taille élargie, en-dessous du bloc"}},{"text":" • "},{"text":"Haut","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/sortie/_choix_type/grand_haut"},"hoverEvent":{"action":"show_text","contents":"Taille élargie, au-dessus du bloc"}}]
tellraw @s [{"text":"Nouvelle taille de panneau: ","color":"gray"},{"text":"3x1","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/sortie/_choix_type/3x1"},"hoverEvent":{"action":"show_text","contents":"3 blocs de large"}},{"text":" • "},{"text":"2x1","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/sortie/_choix_type/2x1"},"hoverEvent":{"action":"show_text","contents":"2 blocs de large"}}]
# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"Panneau Sortie ","color":"gray"},{"text":"2 & 3 & 5","color":"red"},{"text":" :"}]
# La valeur de départ est l'identifiant de ce panneau
scoreboard players set @s idPanneau 87
# On ajoute la valeur correspondant à notre type de panneau
function tch:give/sortie/_choix_type/_id_panneau

# On affiche ensuite l'écran de sélection de la direction
function tch:give/sortie/choix_fleche
# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"Panneau Sortie ","color":"gray"},{"text":"Neutre","color":"red"},{"text":" :"}]
# La valeur de départ est l'identifiant de ce panneau
scoreboard players set @s idPanneau 0
# On ajoute la valeur correspondant au type "Station"
# (seul moyen pour avoir les "Vide", qui sont à l'ID 00000 des panneaux station)
function tch:give/sortie/_choix_type/id_panneau/station

# On affiche ensuite l'écran de sélection de la direction
function tch:give/sortie/choix_fleche
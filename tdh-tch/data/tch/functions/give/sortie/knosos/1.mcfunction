# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"Panneau Sortie ","color":"gray"},{"text":"1 Knosos","color":"red"},{"text":" :"}]
# La valeur de départ est l'identifiant de la station + de l'ID sortie
scoreboard players set @s idPanneau 10201
# On ajoute la valeur correspondant à notre type de panneau
function tch:give/sortie/_choix_type/_id_panneau

# On affiche ensuite l'écran de sélection de la direction
function tch:give/sortie/choix_fleche
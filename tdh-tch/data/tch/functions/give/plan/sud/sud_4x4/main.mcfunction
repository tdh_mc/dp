# PLANS - SUD 4x4 : Choix des colonnes

# Shortcut
#tellraw @s [{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n","color":"gray"},{"text":"Pour accéder rapidement à cet écran, tapez "},{"text":"/hgive","color":"light_purple"},{"text":"p","color":"aqua","bold":"true"},{"text":" (comme "},{"text":"p","color":"aqua"},{"text":"lan).\n"},{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~","color":"gray"}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"Choisissez un plan :"}]
tellraw @s [{"text":"- Colonne 1","color":"red","clickEvent":{"action":"run_command","value":"/function tch:give/plan/sud/sud_4x4/colonne1"}}]
tellraw @s [{"text":"- Colonne 2","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/plan/sud/sud_4x4/colonne2"}}]
tellraw @s [{"text":"- Colonne 3","color":"green","clickEvent":{"action":"run_command","value":"/function tch:give/plan/sud/sud_4x4/colonne3"}}]
tellraw @s [{"text":"- Colonne 4","color":"blue","clickEvent":{"action":"run_command","value":"/function tch:give/plan/sud/sud_4x4/colonne4"}}]
# PLANS

# Shortcut
tellraw @s [{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n","color":"gray"},{"text":"Pour accéder rapidement à cet écran, tapez "},{"text":"/hgive","color":"light_purple"},{"text":"p","color":"aqua","bold":"true"},{"text":" (comme "},{"text":"p","color":"aqua"},{"text":"lan).\n"},{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~","color":"gray"}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"Choisissez un plan :"}]
tellraw @s [{"text":"- [2x2 blocs]","color":"red","clickEvent":{"action":"run_command","value":"/function tch:give/plan/sud/sud_2x2"},"hoverEvent":{"action":"show_text","value":"Déconseillé : les textes et légendes sont très difficilement lisibles"}}]
tellraw @s [{"text":"- [4x4 blocs]","color":"red","clickEvent":{"action":"run_command","value":"/function tch:give/plan/sud/sud_4x4/main"},"hoverEvent":{"action":"show_text","value":"Recommandé"}}]
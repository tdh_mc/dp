# PLANS

tellraw @s [{"text":"\n==== hgive ","color":"#cc4400","bold":"true"},{"text":"plan","color":"#d81d0c"},{"text":" ===="}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"- Plan de lignes","color":"aqua","clickEvent":{"action":"run_command","value":"/function tch:give/plan/lignes/main"}}]
tellraw @s [{"text":"- Réseau TCH","color":"#b560c2","clickEvent":{"action":"run_command","value":"/function tch:give/plan/reseau"}}]
tellraw @s [{"text":"- Réseau TCH Sud","color":"red","clickEvent":{"action":"run_command","value":"/function tch:give/plan/sud/main"}}]
tellraw @s [{"text":"- Plan de situation des gares","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/plan/gares/main"}}]
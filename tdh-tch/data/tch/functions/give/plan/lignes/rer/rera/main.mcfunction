# RER A

# Shortcut
# tellraw @s [{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n","color":"gray"},{"text":"Pour accéder rapidement à cet écran, tapez "},{"text":"/hgive","color":"light_purple"},{"text":"p","color":"aqua","bold":"true"},{"text":" (comme "},{"text":"p","color":"aqua"},{"text":"lan).\n"},{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~","color":"gray"}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"- Plan 2x1","color":"dark_red","clickEvent":{"action":"run_command","value":"/function tch:give/plan/lignes/rer/rera/plan_sud2x1"}}]
tellraw @s [{"text":"- Plan [4x2]","color":"dark_red","clickEvent":{"action":"run_command","value":"/function tch:give/plan/lignes/rer/rera/plan_sud4x2"}}]
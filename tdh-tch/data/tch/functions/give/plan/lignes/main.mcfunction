# LIGNES

# Shortcut
# tellraw @s [{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n","color":"gray"},{"text":"Pour accéder rapidement à cet écran, tapez "},{"text":"/hgive","color":"light_purple"},{"text":"p","color":"aqua","bold":"true"},{"text":" (comme "},{"text":"p","color":"aqua"},{"text":"lan).\n"},{"text":"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~","color":"gray"}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"Choisissez une catégorie :"}]
tellraw @s [{"text":"- RER","color":"red","clickEvent":{"action":"run_command","value":"/function tch:give/plan/lignes/rer/main"}}]
tellraw @s [{"text":"- Totem","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/plan/lignes/totem/_main"},"hoverEvent":{"action":"show_text","value":"Plans de lignes totem selon position spécifiques"}}]
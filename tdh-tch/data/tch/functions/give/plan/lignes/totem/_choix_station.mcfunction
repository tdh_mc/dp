# TOTEM
# On donne un tag au joueur
tag @s add tchGiveTotemTemp
scoreboard players set @s idStation 0

# On détecte la station la plus proche du joueur (et on sauvegarde son PC)
execute as @e[type=#tch:marqueur/station,tag=NomStation,distance=..150,sort=nearest,limit=1] run function tch:tag/station_pc
# On enregistre l'ID de station dans les scores du joueur
scoreboard players operation @s idStation = @e[type=item_frame,tag=ourStationPC,limit=1] idStation
scoreboard players operation #tchGive idStation = @s idStation

# On affiche les différentes lignes détectées aux alentours
tellraw @s [{"text":"Lignes détectées ","color":"yellow"},{"entity":"@e[type=item_frame,tag=ourStationPC,limit=1]","nbt":"Item.tag.display.station.alAu"},{"entity":"@e[type=item_frame,tag=ourStationPC,limit=1]","nbt":"Item.tag.display.station.nom","color":"gold","bold":"true"},{"text":" :"}]
# On n'affiche que les lignes pour lesquelles la station la plus proche est bien la même que pour nous
execute as @e[type=#tch:marqueur/quai,tag=Direction,distance=..100] at @s if score @e[type=#tch:marqueur/station,tag=NomStation,sort=nearest,limit=1] idStation = #tchGive idStation run tag @s add tchLignePotentielle
# On stocke le nombre de directions taggées dans une variable temporaire
execute store result score @s temp if entity @e[type=#tch:marqueur/quai,tag=tchLignePotentielle]

# On lance la récursion si on a trouvé au moins 1 direction à afficher,
# et uniquement si une item frame TexteTemporaire existe dans le monde
# (comme la fonction fille en a besoin, ça évite d'avoir à le tester ensuite dedans)
execute if score @s temp matches 1.. if entity @e[type=item_frame,tag=TexteTemporaire,limit=1] run function tch:give/plan/lignes/totem/_choix_ligne
execute if score @s temp matches 0 run tellraw @s [{"text":"Aucune ligne trouvée dans cette station !","color":"red"}]

# Si on a trouvé une station correspondante, on propose éventuellement d'afficher la liste complète
#execute if score @s temp matches 1.. run tellraw @s [{"text":"\nSinon, vous pouvez afficher l'arborescence des lignes :","color":"yellow"}]
# Si on n'en a pas trouvé, on affiche directement la liste
#execute if score @s temp matches 0 run tellraw @s [{"text":"Choisissez une catégorie :","color":"yellow"}]

# Choisir une catégorie de panneaux :
#tellraw @s [{"text":"–","color":"gray"},{"text":" Câble ","color":"green","clickEvent":{"action":"run_command","value":"/function tch:give/plan/lignes/totem/cable/main"}},{"text":"–"},{"text":" Métro ","color":"aqua","clickEvent":{"action":"run_command","value":"/function tch:give/plan/lignes/totem/metro/main"}},{"text":"–"},{"text":" RER","color":"red","clickEvent":{"action":"run_command","value":"/function tch:give/plan/lignes/totem/rer/main"}}]

# On réinitialise la variable temporaire donnée au joueur et les tags temporaires
scoreboard players reset #tchGive idStation
scoreboard players reset @s temp
tag @s remove tchGiveTotemTemp
tag @e[type=item_frame,tag=ourStationPC] remove ourStationPC
tag @e[type=#tch:marqueur/quai,tag=tchLignePotentielle] remove tchLignePotentielle
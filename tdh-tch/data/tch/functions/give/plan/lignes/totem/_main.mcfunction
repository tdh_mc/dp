# TOTEM
# On vérifie s'il existe une station et une/des lignes aux alentours
scoreboard players set @s temp2 1
execute unless entity @e[type=#tch:marqueur/station,tag=NomStation,distance=..150,limit=1] run scoreboard players set @s temp2 -1
execute if score @s temp2 matches 1 unless entity @e[type=#tch:marqueur/quai,tag=Direction,distance=..100,limit=1] run scoreboard players set @s temp2 -2

# Si on a trouvé une station correspondante, on affiche toutes les options récursivement
execute if score @s temp2 matches 1.. run function tch:give/plan/lignes/totem/_choix_station
# Si on n'en a pas trouvé, on affiche un message d'erreur
execute if score @s temp2 matches -1 run tellraw @s [{"text":"","color":"gray"},{"text":"Erreur: ","bold":"true","color":"red"},{"text":"Merci de vous placer dans une station TCH pour accéder au hgive totem."}]
execute if score @s temp2 matches -2 run tellraw @s [{"text":"","color":"gray"},{"text":"Erreur: ","bold":"true","color":"red"},{"text":"Aucune ligne n'a été trouvée à proximité. Merci de vous rapprocher de la ligne souhaitée."}]

# On réinitialise la variable temporaire donnée au joueur
# pour savoir dans quelle station il se trouve
scoreboard players reset @s temp2
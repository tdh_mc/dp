# Fonction d'affichage des détails d'une ligne
# On exécute cette fonction en tant qu'une entité "Direction" appartenant à cette station,
# pour afficher les détails de ladite entité "Direction" (= choix de cette ligne/direction)

# On exécute cette fonction à la position d'un panneau TexteTemporaire !!


# On tag le PC de notre ligne
function tch:tag/pc
# On tag le terminus de notre ligne
function tch:tag/terminus_pc

# On génère dans un premier temps le texte (coloré) avec le nom du terminus
# On stocke le nom du terminus dans le storage approprié
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.station.nom
# On génère le texte coloré en combinant le JSON du PC de ligne avec le nom stocké dans le storage
data modify block ~ ~ ~ Text1 set from entity @e[type=item_frame,tag=ourPC,limit=1] Item.tag.display.line.terminus

# On génère ensuite le texte complet ("<NOM DE LIGNE> → <NOM DE TERMINUS>")
# On stocke le nom coloré du terminus dans un storage
data modify storage tch:give totem.direction set from block ~ ~ ~ Text1
# On l'accole au nom de la ligne en utilisant à nouveau le panneau
data modify block ~ ~ ~ Text1 set value '[{"entity":"@e[type=item_frame,tag=ourPC,limit=1]","nbt":"Item.tag.display.line.nom","interpret":"true"},{"text":" → ","color":"gray"},{"storage":"tch:give","nbt":"totem.direction","interpret":"true"}]'

# On stocke ce nom complet dans le même storage
data modify storage tch:give totem.direction set from block ~ ~ ~ Text1

# Et là, ENFIN !!!
# On peut afficher le texte généré, qui devrait (je croise les doigts)
# être cliquable et lancer la bonne fonction
# (ce texte est stocké dans notre PC Terminus et pas notre PC de ligne, puisque l'ID associé est différent pour chacun des terminus d'une même ligne)
tellraw @a[tag=tchGiveTotemTemp] [{"entity":"@e[type=item_frame,tag=ourTerminusPC,limit=1]","nbt":"Item.tag.tch.give.totem.texte","interpret":"true"}]


# Une fois l'affichage réalisé, on supprime notre tag pour ne pas repasser dans cette fonction
tag @s remove tchLignePotentielle
# On supprime aussi les tags temporaires des PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
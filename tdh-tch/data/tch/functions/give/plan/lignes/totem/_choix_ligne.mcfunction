# Fonction d'affichage séquentiel des détails d'une ligne
# On exécute cette fonction pour afficher dans un ordre cohérent les différentes lignes détectées

# On a déjà taggé (avec le tag "tchLignePotentielle") toutes les lignes passant à proximité
# On veut maintenant les afficher dans l'ordre de leur ID line

# On prend une valeur extrêmement élevée comme ID line "max",
# puis on le diminue à chaque fois qu'on trouve plus petit dans une des lignes taggées
scoreboard players set #tchTotem idLine 999999
execute as @e[type=#tch:marqueur/quai,tag=tchLignePotentielle] run scoreboard players operation #tchTotem idLine < @s idLine


# On affiche les infos de la direction au plus petit ID line :

# Impossible (ou en tout cas très galère) de rendre ça "modulaire" (avec auto-détection des lignes, couleurs, terminus...) avec le système actuel car c'est impossible de passer un paramètre cliquable (même numérique) à un champ de texte
# La "bonne" manière de faire serait la suivante :
# - définir une variable temporaire à 0 avant l'entrée dans la récursion de _choix_ligne
# - incrémenter cette variable à chaque passage dans _choix_ligne, et afficher les infos comme prévu ci-dessous avec un texte différent à chaque fois (/function tch:xxx/yyy/1, tch:xxx/yyy/2, etc...)
# - enregistrer dans un array du storage ou d'une entité l'ID de ligne associé à chaque valeur de la variable (par exemple on pourrait avoir un tableau [11, 12, 41, 42, 2101, 2102, 2153, 2154] ...)
# - lors de l'appel d'une des fonctions xxx/yyy/N, lire dans le storage l'ID line correspondant et additionner à l'ID de panneau la valeur stockée * 10'000
#execute at @e[type=#tch:marqueur/quai,tag=tchLigneAAfficher,limit=1] run tellraw @s [{"text":"","color":"gray"},{"entity":"@e[type=#tch:marqueur/quai,tag=NumLigne,sort=nearest,limit=1]","nbt":"CustomName","interpret":"true"},{"text":" → "},{"entity":"@e[type=item_frame,tag=ourStationPC,limit=1]","nbt":"Item.tag.display.station.nom","color":"gold","bold":"true"},{"text":" :"}]

# Comme c'est PARFAITEMENT GALÈRE, on adopte une méthode hard-codée à la place :
execute as @e[type=#tch:marqueur/quai,tag=tchLignePotentielle] if score @s idLine = #tchTotem idLine at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:give/plan/lignes/totem/_afficher_ligne


# On passe à la direction suivante s'il en reste encore
execute if entity @e[type=#tch:marqueur/quai,tag=tchLignePotentielle,limit=1] run function tch:give/plan/lignes/totem/_choix_ligne
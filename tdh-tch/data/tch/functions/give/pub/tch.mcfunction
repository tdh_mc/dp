# PUB TCH

# Choisir une pub :
tellraw @s [{"text":"Choisissez une publicité :"}]
tellraw @s [{"text":"- Bonjour","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/pub/tch/bonjour"}}]
tellraw @s [{"text":"- Chauffage","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/pub/tch/chauffage"}}]
tellraw @s [{"text":"- Demandez-nous la ville","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/pub/tch/demandez_ville"}}]
tellraw @s [{"text":"- Sous l'eau","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/pub/tch/sous_leau"}}]
tellraw @s [{"text":"- Merdasse","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/pub/tch/merdasse"}}]
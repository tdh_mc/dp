# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
# La valeur de départ est l'idLine correspondant à cette direction
tellraw @s [{"text":"\u1a38","color":"gray"},{"text":"\u1a3a","color":"#3c91dc"},{"text":" → "},{"text":"Est","color":"#e2231a"}]
scoreboard players set @s idPanneau 2210

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
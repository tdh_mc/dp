# CORRESP MULTI-LIGNES

# Choisir un panneau :
tellraw @s [{"text":"Choisissez un panneau :"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_2"}},{"text":"\u1a21","color":"#ffcd00"},{"text":" & "},{"text":"\u1a22","color":"#003ca6"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_3"}},{"text":"\u1a21","color":"#ffcd00"},{"text":" & "},{"text":"\u1a23","color":"#6ec4e8"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_4"}},{"text":"\u1a21","color":"#ffcd00"},{"text":" & "},{"text":"\u1a24","color":"#cf009e"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_5a"}},{"text":"\u1a21","color":"#ffcd00"},{"text":" & "},{"text":"\u1a25","color":"#ff7e2e"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_5b"}},{"text":"\u1a21","color":"#ffcd00"},{"text":" & "},{"text":"\u1a26","color":"#ff7e2e"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_6"}},{"text":"\u1a21","color":"#ffcd00"},{"text":" & "},{"text":"\u1a27","color":"#60bb39"}]
tellraw @s {"text":"·—·","color":"gray"}
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m2_3"}},{"text":"\u1a22","color":"#003ca6"},{"text":" & "},{"text":"\u1a23","color":"#6ec4e8"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m2_4"}},{"text":"\u1a22","color":"#003ca6"},{"text":" & "},{"text":"\u1a24","color":"#cf009e"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m3_4"}},{"text":"\u1a23","color":"#6ec4e8"},{"text":" & "},{"text":"\u1a24","color":"#cf009e"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m3_5b"}},{"text":"\u1a23","color":"#6ec4e8"},{"text":" & "},{"text":"\u1a26","color":"#ff7e2e"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m5b_6"}},{"text":"\u1a26","color":"#ff7e2e"},{"text":" & "},{"text":"\u1a27","color":"#60bb39"}]
tellraw @s {"text":"·—·","color":"gray"}
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_2_3"}},{"text":"\u1a21","color":"#ffcd00"},{"text":", "},{"text":"\u1a22","color":"#003ca6"},{"text":" & "},{"text":"\u1a23","color":"#6ec4e8"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_2_4"}},{"text":"\u1a21","color":"#ffcd00"},{"text":", "},{"text":"\u1a22","color":"#003ca6"},{"text":" & "},{"text":"\u1a24","color":"#cf009e"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_3_4"}},{"text":"\u1a21","color":"#ffcd00"},{"text":", "},{"text":"\u1a23","color":"#6ec4e8"},{"text":" & "},{"text":"\u1a24","color":"#cf009e"}]
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m2_3_4"}},{"text":"\u1a22","color":"#003ca6"},{"text":", "},{"text":"\u1a23","color":"#6ec4e8"},{"text":" & "},{"text":"\u1a24","color":"#cf009e"}]
tellraw @s {"text":"·—·","color":"gray"}
tellraw @s [{"text":"- \u1a20","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/m1_2_3_4"}},{"text":"\u1a21","color":"#ffcd00"},{"text":", "},{"text":"\u1a22","color":"#003ca6"},{"text":", "},{"text":"\u1a23","color":"#6ec4e8"},{"text":" & "},{"text":"\u1a24","color":"#cf009e"}]
tellraw @s {"text":"·—·","color":"gray"}
tellraw @s [{"text":"- \u1a38","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/rera_m1"}},{"text":"\u1a39","color":"#e2231a"},{"text":" & "},{"text":"\u1a20"},{"text":"\u1a21","color":"#ffcd00"}]
tellraw @s [{"text":"- \u1a38","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/rera_m3"}},{"text":"\u1a39","color":"#e2231a"},{"text":" & "},{"text":"\u1a20"},{"text":"\u1a23","color":"#6ec4e8"}]
tellraw @s [{"text":"- \u1a38","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/rera_m5a"}},{"text":"\u1a39","color":"#e2231a"},{"text":" & "},{"text":"\u1a20"},{"text":"\u1a25","color":"#ff7e2e"}]
tellraw @s [{"text":"- \u1a38","color":"gray","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/multiple/rera_m5b"}},{"text":"\u1a39","color":"#e2231a"},{"text":" & "},{"text":"\u1a20"},{"text":"\u1a26","color":"#ff7e2e"}]
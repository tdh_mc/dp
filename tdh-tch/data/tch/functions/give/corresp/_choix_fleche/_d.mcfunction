# On enregistre dans un score temporaire la valeur définitive du panneau
# (on ne remplace pas notre variable au cas où l'on reclique sur un des boutons précédents)
scoreboard players set @s[tag=!CorrespMultiple] temp7 20000
# Si on a un panneau multiple, tout est multiplié par 100
scoreboard players set @s[tag=CorrespMultiple] temp7 2000000
scoreboard players operation @s temp7 += @s idPanneau

# On invoque l'objet correspondant en lui donnant le bon custommodeldata
execute at @s run summon item ~ ~ ~ {Tags:["TempAQItem"],Age:5950s,PickupDelay:0s,Item:{Count:1b,id:"minecraft:white_dye",tag:{CustomModelData:0}}}
execute store result entity @e[type=item,tag=TempAQItem,sort=nearest,limit=1] Item.tag.CustomModelData int 1 run scoreboard players get @s temp7
data modify entity @e[type=item,tag=TempAQItem,sort=nearest,limit=1] Owner set from entity @s UUID


# On retire le tag temporaire de l'item invoqué
tag @e[type=item,tag=TempAQItem] remove TempAQItem
tag @s remove CorrespMultiple
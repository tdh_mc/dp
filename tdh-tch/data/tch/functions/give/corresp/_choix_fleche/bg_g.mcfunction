# On se donne la version du panneau avec flèche à gauche du texte
function tch:give/corresp/_choix_fleche/_g

# On se donne également la bonne flèche
# On summon l'item pour qu'il apparaisse dans le bon ordre dans l'inventaire
execute at @s run summon item ~ ~ ~ {Tags:["TempAQItem"],Age:5950s,PickupDelay:1s,Item:{Count:1b,id:"minecraft:white_dye",tag:{CustomModelData:1002}}}
data modify entity @e[type=item,tag=TempAQItem,limit=1] Owner set from entity @s UUID
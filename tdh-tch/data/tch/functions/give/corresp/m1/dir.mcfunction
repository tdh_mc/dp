# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"\u1a20","color":"gray"},{"text":"\u1a21","color":"#ffcd00"},{"text":" → "},{"text":"Toutes directions","color":"#ffcd00"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 10

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"","color":"#c39108"},{"text":"\u1a40","color":"gray"},{"text":"\u1a42"},{"text":" → ","color":"gray"},{"entity":"@e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=4022},limit=1]","nbt":"Item.tag.display.station.nom"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 4022

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
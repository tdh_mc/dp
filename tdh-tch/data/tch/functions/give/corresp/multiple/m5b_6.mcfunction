# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"\u1a20","color":"gray"},{"text":"\u1a26","color":"#ff7e2e"},{"text":"\u1a27","color":"#60bb39"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 561

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche_multiple
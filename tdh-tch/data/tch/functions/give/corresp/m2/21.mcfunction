# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"","color":"#003ca6"},{"text":"\u1a20","color":"gray"},{"text":"\u1a22"},{"text":" → ","color":"gray"},{"entity":"@e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=21},limit=1]","nbt":"Item.tag.display.station.nom"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 21

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
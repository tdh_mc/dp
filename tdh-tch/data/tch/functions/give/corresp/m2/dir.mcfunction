# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
tellraw @s [{"text":"\u1a20","color":"gray"},{"text":"\u1a22","color":"#003ca6"},{"text":" → "},{"text":"Toutes directions","color":"#003ca6"}]
# La valeur de départ est l'idLine correspondant à cette direction
scoreboard players set @s idPanneau 20

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
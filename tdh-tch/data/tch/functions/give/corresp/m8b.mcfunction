# Détection des terminus
scoreboard players set #tch idTerminus 83
function tch:tag/terminus_pc
scoreboard players set #tch idTerminus 84
function tch:tag/terminus_pc

# Choisir une direction :
tellraw @s [{"text":"Direction : ","color":"gray"},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=83},limit=1]","nbt":"Item.tag.display.station.nom","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/m8b/83"},"hoverEvent":{"action":"show_text","value":[{"text":"Direction "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=83},limit=1]","nbt":"Item.tag.display.station.nom"},{"text":"."}]}},{"text":" • "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=84},limit=1]","nbt":"Item.tag.display.station.nom","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/m8b/84"},"hoverEvent":{"action":"show_text","value":[{"text":"Direction "},{"entity":"@e[type=item_frame,tag=ourTerminusPC,scores={idTerminus=84},limit=1]","nbt":"Item.tag.display.station.nom"},{"text":"."}]}},{"text":" • "},{"text":"Dir","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/m8b/dir"},"hoverEvent":{"action":"show_text","value":[{"text":"Les deux sens de circulation dans une seule direction."}]}}]

# Suppression des tags !
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
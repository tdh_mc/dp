# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
# La valeur de départ est l'idLine correspondant à cette direction
tellraw @s [{"text":"","color":"#007852"},{"text":"\u1a20","color":"gray"},{"text":"\u1a2f"},{"text":" → ","color":"gray"},{"entity":"@e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=122},limit=1]","nbt":"Item.tag.display.station.nom"}]
scoreboard players set @s idPanneau 122

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
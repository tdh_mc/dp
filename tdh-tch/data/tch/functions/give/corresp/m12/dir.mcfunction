# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
# La valeur de départ est l'idLine correspondant à cette direction
tellraw @s [{"text":"\u1a20","color":"gray"},{"text":"\u1a2f","color":"#007852"},{"text":" → "},{"text":"Toutes directions","color":"#007852"}]
scoreboard players set @s idPanneau 120

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
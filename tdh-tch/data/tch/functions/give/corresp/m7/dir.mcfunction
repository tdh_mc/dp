# Selon notre type de panneau choisi, on définit un score temporaire qui nous servira à calculer le CustomModelData
# La valeur de départ est l'idLine correspondant à cette direction
tellraw @s [{"text":"\u1a20","color":"gray"},{"text":"\u1a28","color":"#fa9aba"},{"text":" → "},{"text":"Toutes directions","color":"#fa9aba"}]
scoreboard players set @s idPanneau 70

# On affiche ensuite l'écran de sélection de la direction
function tch:give/corresp/_choix_fleche
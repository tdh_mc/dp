# TRAVAUX

tellraw @s [{"text":"\n==== hgive ","color":"#cc4400","bold":"true"},{"text":"travaux","color":"#d81d0c"},{"text":" ===="}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"- Chantier interdit au public","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/travaux/interdit_au_public"},"hoverEvent":{"action":"show_text","contents":[{"text":"Version standalone (à poser sur le sol)"}]}}]
tellraw @s [{"text":"- Un réseau + beau","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/travaux/un_reseau_plus_beau"},"hoverEvent":{"action":"show_text","contents":[{"text":"4x2 blocs"}]}}]
tellraw @s [{"text":"- Bientôt, ici, ","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/travaux/bientot_ici"},"hoverEvent":{"action":"show_text","contents":[{"text":"4x2 blocs (à combiner avec des logos normal centré/décalé)"}]}}]
# On initialise toutes les variables
scoreboard players set #hgivefAcacia temp 0
scoreboard players set #hgivefBirch temp 0
scoreboard players set #hgivefDarkOak temp 0
scoreboard players set #hgivefJungle temp 0
scoreboard players set #hgivefOak temp 0
scoreboard players set #hgivefSpruce temp 0
scoreboard players set #hgivefCrimson temp 0
scoreboard players set #hgivefWarped temp 0

scoreboard players set #hgivefStone temp 0
scoreboard players set #hgivefBlackstone temp 0
scoreboard players set #hgivefAndesite temp 0
scoreboard players set #hgivefDiorite temp 0
scoreboard players set #hgivefGranite temp 0
scoreboard players set #hgivefBricks temp 0
scoreboard players set #hgivefBasalt temp 0
scoreboard players set #hgivefQuartz temp 0

scoreboard players set #hgivefSandstone temp 0
scoreboard players set #hgivefRedSandstone temp 0

scoreboard players set #hgivef deltaX 9
scoreboard players set #hgivef deltaY 9
scoreboard players set #hgivef deltaZ 9

# On teste le premier bloc et on laisse faire la boucle
function tch:give/fond/near/test_bloc
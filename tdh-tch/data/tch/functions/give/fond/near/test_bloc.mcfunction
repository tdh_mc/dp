# On teste le bloc actuel avec tous les tags de tch:give
execute if block ~ ~ ~ #tch:give/bois/acacia run scoreboard players add #hgivefAcacia temp 1
execute if block ~ ~ ~ #tch:give/bois/birch run scoreboard players add #hgivefBirch temp 1
execute if block ~ ~ ~ #tch:give/bois/dark_oak run scoreboard players add #hgivefDarkOak temp 1
execute if block ~ ~ ~ #tch:give/bois/jungle run scoreboard players add #hgivefJungle temp 1
execute if block ~ ~ ~ #tch:give/bois/oak run scoreboard players add #hgivefOak temp 1
execute if block ~ ~ ~ #tch:give/bois/spruce run scoreboard players add #hgivefSpruce temp 1
execute if block ~ ~ ~ #tch:give/bois/crimson run scoreboard players add #hgivefCrimson temp 1
execute if block ~ ~ ~ #tch:give/bois/warped run scoreboard players add #hgivefWarped temp 1

execute if block ~ ~ ~ #tch:give/pierre/stone run scoreboard players add #hgivefStone temp 1
execute if block ~ ~ ~ #tch:give/pierre/blackstone run scoreboard players add #hgivefBlackstone temp 1
execute if block ~ ~ ~ #tch:give/pierre/andesite run scoreboard players add #hgivefAndesite temp 1
execute if block ~ ~ ~ #tch:give/pierre/diorite run scoreboard players add #hgivefDiorite temp 1
execute if block ~ ~ ~ #tch:give/pierre/granite run scoreboard players add #hgivefGranite temp 1
execute if block ~ ~ ~ #tch:give/pierre/bricks run scoreboard players add #hgivefBricks temp 1
execute if block ~ ~ ~ #tch:give/pierre/basalt run scoreboard players add #hgivefBasalt temp 1
execute if block ~ ~ ~ #tch:give/pierre/quartz run scoreboard players add #hgivefQuartz temp 1

execute if block ~ ~ ~ #tch:give/sable/sandstone run scoreboard players add #hgivefSandstone temp 1
execute if block ~ ~ ~ #tch:give/sable/red_sandstone run scoreboard players add #hgivefRedSandstone temp 1

# On boucle ensuite selon nos variables deltaX/Y/Z
scoreboard players set #hgivef temp 1
execute if score #hgivef deltaX matches ..0 run scoreboard players set #hgivef temp 2
execute if score #hgivef deltaX matches ..0 if score #hgivef deltaY matches ..0 run scoreboard players set #hgivef temp 3
execute if score #hgivef deltaX matches ..0 if score #hgivef deltaY matches ..0 if score #hgivef deltaZ matches ..0 run scoreboard players set #hgivef temp 0

execute if score #hgivef temp matches 1 run function tch:give/fond/near/next_x
execute if score #hgivef temp matches 2 run function tch:give/fond/near/next_xy
execute if score #hgivef temp matches 3 run function tch:give/fond/near/next_xyz
# Si c'est égal à 0, on a fini le loop ; on s'arrête
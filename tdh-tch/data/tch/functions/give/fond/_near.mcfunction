# On stocke dans des variables temporaires le type de bloc de tous les blocs qui nous entourent
execute positioned ~-4 ~-4 ~-4 run function tch:give/fond/near/test

# On exécute ensuite toutes les fonctions qui correspondent à des blocs qu'on a trouvé autour de nous
execute if score #hgivefAcacia temp matches 1.. run function tch:give/fond/bois/acacia
execute if score #hgivefBirch temp matches 1.. run function tch:give/fond/bois/birch
execute if score #hgivefDarkOak temp matches 1.. run function tch:give/fond/bois/dark_oak
execute if score #hgivefJungle temp matches 1.. run function tch:give/fond/bois/jungle
execute if score #hgivefOak temp matches 1.. run function tch:give/fond/bois/oak
execute if score #hgivefSpruce temp matches 1.. run function tch:give/fond/bois/spruce
execute if score #hgivefCrimson temp matches 1.. run function tch:give/fond/bois/crimson
execute if score #hgivefWarped temp matches 1.. run function tch:give/fond/bois/warped

execute if score #hgivefStone temp matches 1.. run function tch:give/fond/pierre/stone
execute if score #hgivefBlackstone temp matches 1.. run function tch:give/fond/pierre/blackstone
execute if score #hgivefAndesite temp matches 1.. run function tch:give/fond/pierre/andesite
execute if score #hgivefDiorite temp matches 1.. run function tch:give/fond/pierre/diorite
execute if score #hgivefGranite temp matches 1.. run function tch:give/fond/pierre/granite
execute if score #hgivefBricks temp matches 1.. run function tch:give/fond/pierre/bricks
execute if score #hgivefBasalt temp matches 1.. run function tch:give/fond/pierre/basalt
execute if score #hgivefQuartz temp matches 1.. run function tch:give/fond/pierre/quartz

execute if score #hgivefSandstone temp matches 1.. run function tch:give/fond/sable/sandstone
execute if score #hgivefRedSandstone temp matches 1.. run function tch:give/fond/sable/red_sandstone

function tch:give/fond/divers/unis

tellraw @s {"text":"\nAfficher plutôt la liste complète...","color":"yellow","italic":"true","clickEvent":{"action":"run_command","value":"/function tch:give/fond/_list"}}
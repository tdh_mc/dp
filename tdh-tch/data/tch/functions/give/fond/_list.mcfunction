# FOND

tellraw @s [{"text":"\n==== hgive ","color":"#cc4400","bold":"true"},{"text":"fond","color":"#d81d0c"},{"text":" ===="}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"-- BOIS --","color":"gray"}]
function tch:give/fond/bois/acacia
function tch:give/fond/bois/birch
function tch:give/fond/bois/dark_oak
function tch:give/fond/bois/jungle
function tch:give/fond/bois/oak
function tch:give/fond/bois/spruce
function tch:give/fond/bois/crimson
function tch:give/fond/bois/warped

tellraw @s [{"text":"-- PIERRE --","color":"gray"}]
function tch:give/fond/pierre/stone
function tch:give/fond/pierre/blackstone
function tch:give/fond/pierre/andesite
function tch:give/fond/pierre/diorite
function tch:give/fond/pierre/granite
function tch:give/fond/pierre/bricks
function tch:give/fond/pierre/basalt
function tch:give/fond/pierre/quartz

tellraw @s [{"text":"-- SABLE --","color":"gray"}]
function tch:give/fond/sable/sandstone
function tch:give/fond/sable/red_sandstone

tellraw @s [{"text":"-- DIVERS --","color":"gray"}]
function tch:give/fond/divers/unis

# Bienvenue dans TCHGIVE
tellraw @s [{"text":"\n==== Bienvenue dans hgive! ====","color":"#cc4400","bold":"true"}]

# Choisir une catégorie de panneaux :
tellraw @s [{"text":"- Accès interdit","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/acces_interdit/main"}}]
tellraw @s [{"text":"- Accès aux quais","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/acces_quais/_main"},"hoverEvent":{"action":"show_text","value":"Comprend les accès aux quais neutres et les détails par ligne"}}]
tellraw @s [{"text":"- Correspondances","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/corresp/_main"},"hoverEvent":{"action":"show_text","value":"1 de large x 2 de haut"}}]
tellraw @s [{"text":"- Logos lignes","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/logo/main"}}]
tellraw @s [{"text":"- Plans","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/plan/main"}}]
tellraw @s [{"text":"- Publicité","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/pub/main"}}]
tellraw @s [{"text":"- Affichages d'informations","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/siel/main"}}]
tellraw @s [{"text":"- Signalisation","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/signal/main"}}]
tellraw @s [{"text":"- Sorties","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/sortie/main"},"hoverEvent":{"action":"show_text","value":"Versions génériques + versions spécifiques à chaque gare"}}]
tellraw @s [{"text":"- Stations","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/station/_main"}}]
tellraw @s [{"text":"- Travaux","color":"gold","clickEvent":{"action":"run_command","value":"/function tch:give/travaux/main"}}]
tellraw @s [{"text":"- Vente","color":"yellow","clickEvent":{"action":"run_command","value":"/function tch:give/vente/main"},"hoverEvent":{"action":"show_text","value":"Pour les distributeurs"}}]
tellraw @s [{"text":"===============================","color":"#cc4400","bold":"true"}]
# Pour les sous-fonctions de mesure
scoreboard objectives add mesureTemps minecraft.custom:minecraft.play_time "Durée totale"
scoreboard objectives add mesureDistMetro minecraft.custom:minecraft.minecart_one_cm "Distance totale en minecart"
scoreboard objectives add mesureDistWalk minecraft.custom:minecraft.walk_one_cm "Distance totale en marchant"
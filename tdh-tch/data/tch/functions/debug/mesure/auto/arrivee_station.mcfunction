# On arrive en station, on affiche un message qui précise quelle station c'est
tellraw @s [{"text":"=== Arrivée à ","color":"#813e12"},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]"},{"text":" ("},{"selector":"@e[type=armor_stand,tag=NumLigne,sort=nearest,limit=1]"},{"text":") ==="}]

# On affiche un message de debug si le métro a le score du SISVE
execute as @e[tag=Metro,distance=..4,sort=nearest,limit=1] at @s[scores={displayTicks=-9999..9999}] run tellraw @a[distance=..3,tag=MesureAuto] [{"text":"Timer du SISVE à l'arrivée: ","color":"gray"},{"score":{"name":"@s","objective":"displayTicks"},"color":"red"},{"text":"."}]

# Enfin on affiche les données
function tch:debug/mesure/paste
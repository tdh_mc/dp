# On part de la station, on affiche un message qui précise quelle station c'est, puis on réinitialise les données
tellraw @s [{"text":"=== Départ de ","color":"#813e12"},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]"},{"text":" ("},{"selector":"@e[type=armor_stand,tag=NumLigne,sort=nearest,limit=1]"},{"text":") ==="}]
function tch:debug/mesure/on
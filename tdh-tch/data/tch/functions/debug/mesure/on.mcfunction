scoreboard players set @s mesureTemps 0
scoreboard players set @s mesureDistMetro 0
scoreboard players set @s mesureDistWalk 0
tellraw @s [{"text":"La distance et la durée ont été réinitialisés. Utilisez ","color":"gold"},{"text":"/mesure paste","color":"red","clickEvent":{"action":"run_command","value":"/function tch:debug/mesure/paste"},"hoverEvent":{"action":"show_text","value":[{"text":"Clique ici !","color":"yellow"}]}},{"text":" pour afficher le résultat."}]
# On stocke le nombre de ticks écoulés
scoreboard players operation #mesure mesureTemps = @s mesureTemps

# On stocke le nombre de mètres parcourus en minecart
scoreboard players operation #mesure mesureDistMetro = @s mesureDistMetro
scoreboard players set #mesure temp 100
scoreboard players operation #mesure mesureDistMetro /= #mesure temp

# On stocke le nombre de mètres parcourus en marchant
scoreboard players operation #mesure mesureDistWalk = @s mesureDistWalk
scoreboard players operation #mesure mesureDistWalk /= #mesure temp

# On affiche un message
execute if score #mesure mesureDistMetro matches 1.. run tellraw @s [{"text":"En métro : ","color":"gold"},{"score":{"name":"#mesure","objective":"mesureDistMetro"},"color":"yellow"},{"text":" mètres"}]
execute if score #mesure mesureDistWalk matches 1.. run tellraw @s [{"text":"En marchant : ","color":"gold"},{"score":{"name":"#mesure","objective":"mesureDistWalk"},"color":"yellow"},{"text":" mètres"}]
execute if score #mesure mesureDistMetro matches ..0 if score #mesure mesureDistWalk matches ..0 run tellraw @s [{"text":"Distance parcourue : ","color":"gold"},{"text":"0","color":"yellow"},{"text":" mètres"}]
tellraw @s [{"text":"Ticks écoulés : ","color":"gold"},{"score":{"name":"#mesure","objective":"mesureTemps"},"color":"yellow"}]
tellraw @s [{"text":"Utilisez "},{"text":"/mesure paste","color":"red","clickEvent":{"action":"run_command","value":"/function tch:debug/mesure/paste"},"hoverEvent":{"action":"show_text","value":[{"text":"Clique ici !","color":"yellow"}]}},{"text":" pour calculer un nouveau résultat.\nUtilisez "},{"text":"/mesure on","color":"red","clickEvent":{"action":"run_command","value":"/function tch:debug/mesure/on"},"hoverEvent":{"action":"show_text","value":[{"text":"Clique ici !","color":"yellow"}]}},{"text":" pour réinitialiser les valeurs."}]

# On réinitialise temp
scoreboard players reset #mesure temp
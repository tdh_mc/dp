# On récupère notre PC
function tch:tag/pc

# On enregistre l'heure actuelle
scoreboard players operation #store currentHour = #temps currentTdhTick
function tdh:store/time

execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 1..2 if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Il est ","color":"#f1ede3"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":"."}]
execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 1..2 if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Il est ","color":"#1c0000"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"dark_red"},{"text":"."}]

execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 3 run function tch:sisve/cable/incident/horaire_reprise

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
# On récupère notre PC
function tch:tag/pc

execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 1 if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Un incident nous contraint à suspendre le trafic sur ","color":"#f1ede3"},{"nbt":"Item.tag.display.line.vehicule.prefixe","entity":"@s"},{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true"},{"text":"."}]
execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 1 if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Un incident nous contraint à suspendre le trafic sur ","color":"#1c0000"},{"nbt":"Item.tag.display.line.vehicule.prefixe","entity":"@s"},{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true"},{"text":"."}]

execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 2..3 if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"","color":"#f1ede3"},{"nbt":"Item.tag.tch.incident.generic.Article","entity":"@s"},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@s"},{"text":" nous contraint à suspendre le trafic sur "},{"nbt":"Item.tag.display.line.vehicule.prefixe","entity":"@s"},{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true"},{"text":"."}]
execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 2..3 if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"","color":"#1c0000"},{"nbt":"Item.tag.tch.incident.generic.Article","entity":"@s"},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@s"},{"text":" nous contraint à suspendre le trafic sur "},{"nbt":"Item.tag.display.line.vehicule.prefixe","entity":"@s"},{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true"},{"text":"."}]

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
# On enregistre l'heure de reprise
scoreboard players operation #store2 currentHour = @s finIncident
function tdh:store/time2

execute if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Il est ","color":"#f1ede3"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":". Reprise estimée vers "},{"nbt":"time2","storage":"tdh:store","interpret":"true","color":"gold"},{"text":"."}]
execute if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Il est ","color":"#1c0000"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"dark_red"},{"text":". Reprise estimée vers "},{"nbt":"time2","storage":"tdh:store","interpret":"true","color":"gold"},{"text":"."}]
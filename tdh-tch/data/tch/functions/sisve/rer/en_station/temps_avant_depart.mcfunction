# On affiche le temps avant départ
scoreboard players set @s temp6 80
scoreboard players operation @s temp6 -= @s remainingTicks
scoreboard players set @s temp7 10
scoreboard players operation @s temp6 /= @s temp7
execute if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Départ dans ","color":"#f1ede3"},{"score":{"name":"@s","objective":"temp6"},"color":"red"},{"text":" secondes."}]
execute if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Départ dans ","color":"#1c0000"},{"score":{"name":"@s","objective":"temp6"},"color":"red"},{"text":" secondes."}]

scoreboard players reset @s temp6
scoreboard players reset @s temp7
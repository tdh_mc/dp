## TICK DU SISVE RER
## Exécuté par une motrice du RER à sa position, si un joueur au moins se trouve dans ce RER
# Si on a un timer, on le décrémente
scoreboard players remove @s displayTicks 1

# On tag les joueurs correspondant à notre RER
execute at @a[tag=RERPassager] if score @s idVehicule = @p[tag=RERPassager] idVehicule run tag @a[distance=0,tag=RERPassager] add SISVEPassager

# On affiche les informations correspondant à notre situation :
# - Si on est en cours d'arrivée en station
execute as @s[tag=PreStation,tag=!ArretUrgence] run function tch:sisve/rer/pre_station/tick
# - Si on est en arrêt d'urgence
execute as @s[tag=ArretUrgence] run function tch:sisve/rer/arret_urgence/tick
# - Si on est en station, et qu'il n'y a pas d'incident
execute as @s[tag=EnStation,tag=!Incident,tag=!FinDeService] run function tch:sisve/rer/en_station/tick
# - S'il y a un incident qui nous contraint à rester en station
execute as @s[tag=EnStation,tag=Incident] run function tch:sisve/rer/incident/tick
execute as @s[tag=EnStation,tag=FinDeService] run function tch:sisve/rer/fin_de_service/tick
# - Si on est en interstation
execute as @s[tag=!EnStation,tag=!PreStation,tag=!ArretUrgence] run function tch:sisve/rer/interstation/tick

# On retire le tag des passagers
tag @a[tag=SISVEPassager] remove SISVEPassager
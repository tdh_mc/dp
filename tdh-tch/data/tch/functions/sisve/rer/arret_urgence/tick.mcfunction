# SISVE EN ARRÊT D'URGENCE PRÉ-STATION
# On a 6 displayTicks en tout (de 6 à 1)
# Si on n'a pas encore de timer ou qu'il a atteint 0, on le (ré?)initialise
execute unless score @s displayTicks matches 1.. run scoreboard players set @s displayTicks 6

# 4-6: "Le trafic reprend dans quelques instants"
execute if score @s displayTicks matches 4..6 if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Un incident nous a contraint à arrêter ce train.","color":"#f1ede3"}]
execute if score @s displayTicks matches 4..6 if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Un incident nous a contraint à arrêter ce train.","color":"#1c0000"}]

# 1-3: "TCH présente ses excuses etc"
execute if score @s displayTicks matches 1..3 if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"TCH","color":"red"},{"text":" vous présente ses excuses pour la gêne occasionnée.","color":"#f1ede3"}]
execute if score @s displayTicks matches 1..3 if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"TCH","color":"red"},{"text":" vous présente ses excuses pour la gêne occasionnée.","color":"#1c0000"}]
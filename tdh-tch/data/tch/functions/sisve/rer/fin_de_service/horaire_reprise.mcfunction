# On récupère notre PC
function tch:tag/pc

# On enregistre l'heure actuelle
scoreboard players operation #store currentHour = #temps currentTdhTick
function tdh:store/time
# On enregistre l'heure de début de service
scoreboard players operation #store2 currentHour = @e[type=item_frame,tag=ourPC,limit=1] debutServiceAjd
function tdh:store/time2

execute as @e[type=item_frame,tag=ourPC,limit=1] if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Il est ","color":"#f1ede3"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":". Reprise du service à ","color":"#f1ede3"},{"nbt":"time2","storage":"tdh:store","interpret":"true","color":"red"},{"text":"."}]
execute as @e[type=item_frame,tag=ourPC,limit=1] if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Il est ","color":"#1c0000"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"dark_red"},{"text":". Reprise du service à ","color":"#1c0000"},{"nbt":"time2","storage":"tdh:store","interpret":"true","color":"dark_red"},{"text":"."}]

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
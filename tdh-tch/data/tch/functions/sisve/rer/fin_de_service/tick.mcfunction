# SISVE EN FDS EN STATION
# On a 12 displayTicks en tout (de 12 à 1)
# Si on n'a pas encore de timer ou qu'il a atteint 0, on le (ré?)initialise
execute unless score @s displayTicks matches 1.. run scoreboard players set @s displayTicks 12

# 9-12: Trafic terminé
execute if score @s displayTicks matches 9..12 run function tch:sisve/cable/fin_de_service/trafic_termine
# 5-8: Bonne soirée
execute if score @s displayTicks matches 5..8 run title @a[tag=SISVEPassager] actionbar [{"text":"TCH","color":"red"},{"text":" vous souhaite une excellente soirée.","color":"#8e856b"}]
# 1-4: Horaire reprise
execute if score @s displayTicks matches 1..4 run function tch:sisve/cable/fin_de_service/horaire_reprise
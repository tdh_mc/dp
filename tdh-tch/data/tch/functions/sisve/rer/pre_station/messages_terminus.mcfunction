# Message actionbar en pré-station
# 11-15: "Terminus" ou "Terminus FDS" si l'un des deux matche

# Si on a le tag FDS, on affiche le message de terminus FDS
execute as @s[tag=FinDeService,tag=!Terminus] if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Notre train est en ","color":"#f1ede3"},{"text":"fin de service","color":"red"},{"text":" et fera "},{"text":"terminus","color":"red"},{"text":" à cette station."}]
execute as @s[tag=FinDeService,tag=!Terminus] if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Notre train est en ","color":"#1c0000"},{"text":"fin de service","color":"dark_red"},{"text":" et fera "},{"text":"terminus","color":"dark_red"},{"text":" à cette station."}]
# Si notre voie est le terminus normal, on affiche le message de terminus
execute as @s[tag=Terminus] if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Nous arrivons au ","color":"#f1ede3"},{"text":"terminus","color":"red"},{"text":" de cette ligne."}]
execute as @s[tag=Terminus] if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Nous arrivons au ","color":"#1c0000"},{"text":"terminus","color":"dark_red"},{"text":" de cette ligne."}]
# Sinon, on affiche le message de corresp
execute as @s[tag=!FinDeService,tag=!Terminus] run function tch:sisve/rer/pre_station/messages_corresp
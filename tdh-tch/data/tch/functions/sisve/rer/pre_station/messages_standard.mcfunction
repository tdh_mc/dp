# MESSAGES ACTIONBAR DU SISVE EN PRÉ-STATION
# 16-20: "Nous arrivons bientôt à XX"
execute if score @s displayTicks matches 16.. run function tch:sisve/rer/pre_station/prochaine_station_bientot
# 11-15: "Terminus" ou "Terminus FDS" si l'un des deux matche
execute if score @s displayTicks matches 11..15 run function tch:sisve/rer/pre_station/messages_terminus
# 6-10: "Corresp" si on a le tag, une pub sinon
execute if score @s displayTicks matches 6..10 run function tch:sisve/rer/pre_station/messages_corresp
# 1-5: "Nous arrivons à XX"
execute if score @s displayTicks matches ..5 run function tch:sisve/rer/pre_station/prochaine_station
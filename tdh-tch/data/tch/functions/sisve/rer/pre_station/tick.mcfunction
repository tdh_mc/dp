## TICK DU SISVE EN PRÉ-STATION
# On a 20 displayTicks en tout (de 20 à 1)
# On ne réinitialise pas le timer, on veut qu'il reste à 0 une fois qu'il y est

## PARTIE 0 : Prise en compte du pré-station
# Si on n'a pas encore le tag qui indique qu'on a fait les calculs initiaux, on les fait
execute as @s[tag=!PreStationDone] run function tch:sisve/rer/pre_station/debut

## PARTIE I : Sonorisation
# D'abord les annonces :
# - Alerte arrivée station (*nom_station 1*)
execute if score @s displayTicks matches 18 run function tch:sound/arrivee_station1/jouer
# - Sous-fonction (avec la suite des sons) différente selon si on fait ou non Terminus
execute as @s[tag=Terminus] run function tch:sisve/rer/pre_station/sons_terminus
execute as @s[tag=FinDeService,tag=!Terminus] run function tch:sisve/rer/pre_station/sons_terminus
execute as @s[tag=!FinDeService,tag=!Terminus] run function tch:sisve/rer/pre_station/sons_pas_terminus

## PARTIE II : Titres actionbar
function tch:sisve/rer/pre_station/messages_standard


## PARTIE III : Réservation de la voie
# Si on est à 12 secondes de l'arrivée, on tente de réserver la voie
execute if score @s[tag=!VoieReservee,tag=!FileAttente] displayTicks matches ..12 run function tch:voie/reservation/test
# Si on est à 6 secondes de l'arrivée et qu'on est en file d'attente, on tente à nouveau de réserver la voie
execute if score @s[tag=FileAttente] displayTicks matches ..6 run function tch:voie/reservation/test2
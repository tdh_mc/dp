## ANNONCES SONORES SI ON NE FAIT PAS TERMINUS
#9s avant : Arrivée station
execute if score @s displayTicks matches 9 at @a[tag=SISVEPassager] run function tch:sound/arrivee_station2/jouer
#6s avant si corresp : Rien oublier
execute if score @s[tag=Correspondance] displayTicks matches 6 at @a[tag=SISVEPassager] run function tch:sound/rienoublier
#4s avant si corresp : Rien oublier EN
execute if score @s[tag=Correspondance] displayTicks matches 4 at @a[tag=SISVEPassager] run function tch:sound/rienoublier_en
#2s avant si corresp : Rien oublier ES
execute if score @s[tag=Correspondance] displayTicks matches 2 at @a[tag=SISVEPassager] run function tch:sound/rienoublier_es
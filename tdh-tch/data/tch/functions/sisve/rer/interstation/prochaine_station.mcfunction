# On affiche la prochaine station du métro qui exécute cette fonction

# On récupère d'abord notre PC quai
function tch:tag/prochain_quai_pc
# On enregistre notre ID line
scoreboard players operation #rer idLine = @s idLine

# On vérifie si c'est le terminus (par défaut, non = 0)
scoreboard players set @s temp 0
# Si c'est un terminus (normal ou pour cause de fin de service), on l'enregistre
execute if score @e[type=item_frame,tag=ourQuaiPC,limit=1] idTerminus = @s idLine run scoreboard players set @s temp 1
execute if score @s temp matches 0 if score @e[type=item_frame,tag=ourQuaiPC,limit=1] idTerminus < @s idLine if score @e[type=item_frame,tag=ourQuaiPC,limit=1] idTerminusMax >= @s idLine run scoreboard players set @s temp 1
scoreboard players set @s[tag=FinDeService] temp 1

# On affiche ensuite le nom de la station en fonction de notre statut
# Dans le cas général, on met juste prochaine station
execute unless score @s temp matches 1 if score @p[tag=SISVEPassager] lightLevel matches ..7 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[tag=SISVEPassager] actionbar [{"text":"Prochaine station: ","color":"#f1ede3"},{"nbt":"Item.tag.display.station.prefixe","entity":"@s","color":"red"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"red"},{"text":"."}]
execute unless score @s temp matches 1 if score @p[tag=SISVEPassager] lightLevel matches 8.. as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[tag=SISVEPassager] actionbar [{"text":"Prochaine station: ","color":"#1c0000"},{"nbt":"Item.tag.display.station.prefixe","entity":"@s","color":"dark_red"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"dark_red"},{"text":"."}]

# Le cas échéant, on met prochaine station (terminus)
execute if score @s temp matches 1 if score @p[tag=SISVEPassager] lightLevel matches ..7 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[tag=SISVEPassager] actionbar [{"text":"Prochaine station: ","color":"#f1ede3"},{"nbt":"Item.tag.display.station.prefixe","entity":"@s","color":"red"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"red"},{"text":" (terminus)","color":"red","italic":"true"},{"text":"."}]
execute if score @s temp matches 1 if score @p[tag=SISVEPassager] lightLevel matches 8.. as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[tag=SISVEPassager] actionbar [{"text":"Prochaine station: ","color":"#1c0000"},{"nbt":"Item.tag.display.station.prefixe","entity":"@s","color":"dark_red"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"dark_red"},{"text":" (terminus)","color":"dark_red","italic":"true"},{"text":"."}]

# On réinitialise les variables et tags temporaires
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
scoreboard players reset @s temp
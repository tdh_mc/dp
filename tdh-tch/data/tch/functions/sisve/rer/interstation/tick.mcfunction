## TICK DU SISVE EN INTERSTATION
# On a 16 displayTicks en tout (de 16 à 1)
# Si on n'a pas encore de timer ou qu'il a atteint 0, on le (ré?)initialise
execute unless score @s displayTicks matches 1.. run scoreboard players set @s displayTicks 16

# 13-16: Afficher l'ID line
execute if score @s displayTicks matches 13.. run function tch:sisve/rer/interstation/ligne_et_direction
# 9-12: Vitesse actuelle
execute if score @s displayTicks matches 9..12 run function tch:sisve/rer/interstation/vitesse
# 5-8 : Afficher la prochaine station
execute if score @s displayTicks matches 5..8 run function tch:sisve/rer/interstation/prochaine_station
# 1-4 : Afficher l'heure
execute if score @s displayTicks matches ..4 run function tch:sisve/rer/interstation/heure
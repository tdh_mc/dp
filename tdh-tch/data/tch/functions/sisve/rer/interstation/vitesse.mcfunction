# On calcule la vitesse du train, puis on l'affiche
# La vitesse est stockée en m/s mais on l'affiche en km/h, donc on la multiplie par 3.6
scoreboard players set #rer temp 36
scoreboard players operation #rer temp *= @s vitesse
scoreboard players set #rer temp2 10
scoreboard players operation #rer temp /= #rer temp2

execute if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Notre vitesse est de ","color":"#f1ede3"},{"score":{"name":"#rer","objective":"temp"},"color":"red","extra":[{"text":" km/h"}]},{"text":"."}]
execute if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Notre vitesse est de ","color":"#1c0000"},{"score":{"name":"#rer","objective":"temp"},"color":"dark_red","extra":[{"text":" km/h"}]},{"text":"."}]
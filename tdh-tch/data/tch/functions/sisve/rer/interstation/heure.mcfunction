# On affiche l'heure actuelle
scoreboard players operation #store currentHour = #temps currentTdhTick
function tdh:store/time
execute if score @p[tag=SISVEPassager] lightLevel matches ..7 run title @a[tag=SISVEPassager] actionbar [{"text":"Il est ","color":"#f1ede3"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":"."}]
execute if score @p[tag=SISVEPassager] lightLevel matches 8.. run title @a[tag=SISVEPassager] actionbar [{"text":"Il est ","color":"#1c0000"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"dark_red"},{"text":"."}]
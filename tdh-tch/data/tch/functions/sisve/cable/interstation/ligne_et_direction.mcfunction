# On affiche le numéro de ligne et le terminus en utilisant le PC terminus
function tch:tag/terminus_pc
execute positioned ~-2 ~-11 ~-2 as @e[type=item_frame,tag=ourTerminusPC,limit=1] run title @a[dx=4,dy=6,dz=4] actionbar [{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true","color":"#8e856b"},{"text":" → "},{"nbt":"Item.tag.display.station.prefixe","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s"}]

# On supprime le tag du PC terminus
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
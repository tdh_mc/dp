# On affiche la prochaine station du métro qui exécute cette fonction

# On récupère d'abord notre PC quai
function tch:tag/quai_pc

# On vérifie si c'est le terminus (par défaut, non = 0)
scoreboard players set @s temp 0
# Si c'est un terminus (normal ou pour cause de fin de service), on l'enregistre
execute if entity @e[type=item_frame,tag=ourQuaiPC,scores={idTerminus=1..}] run scoreboard players set @s temp 1
scoreboard players set @s[tag=FinDeService] temp 1

# On affiche ensuite le nom de la station en fonction de notre statut
# Dans le cas général, on met juste prochaine station
execute unless score @s temp matches 1 positioned ~-2 ~-11 ~-2 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Prochaine station: ","color":"#8e856b"},{"nbt":"Item.tag.display.station.prefixe","entity":"@s","color":"red"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"red"},{"text":"."}]
# Le cas échéant, on met prochaine station (terminus)
execute if score @s temp matches 1 positioned ~-2 ~-11 ~-2 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Prochaine station: ","color":"#8e856b"},{"nbt":"Item.tag.display.station.prefixe","entity":"@s","color":"red"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"red"},{"text":" (terminus)","color":"red","italic":"true"},{"text":"."}]

# On réinitialise les variables et tags temporaires
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
scoreboard players reset @s temp
# On affiche l'heure actuelle
scoreboard players operation #store currentHour = #temps currentTdhTick
function tdh:store/time
execute positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Il est ","color":"#8e856b"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":"."}]
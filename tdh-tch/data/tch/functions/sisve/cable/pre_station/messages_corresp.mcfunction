# Message actionbar en pré-station
# 6-10: "Corresp" si on a le tag, une pub sinon (TODO)

# Si on a le tag, on affiche les correspondances
execute as @s[tag=Correspondance] run function tch:sisve/cable/pre_station/corresp

# Sinon, on tente d'afficher une pub
#execute as @s[tag=!Correspondance] run function tch:sisve/cable/pre_station/messages_pub
execute as @s[tag=!Correspondance] run function tch:sisve/cable/pre_station/prochaine_station_bientot
# On récupère notre voie
function tch:tag/voie

# On joue le son stéréo à notre position
function tch:sound/cable/arrivee_train_inside
# On joue le son mono vers la position de notre voie
execute at @e[tag=ourVoie,limit=1] facing entity @s feet rotated ~ 0 positioned ^ ^ ^5 run function tch:sound/cable/arrivee_train


# On supprime le tag de notre voie
tag @e[tag=ourVoie] remove ourVoie
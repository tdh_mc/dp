# Exécutée par une cabine de câble une fois lorsqu'il détecte pour la première fois sa prochaine station

# On enregistre notre PC et notre PC quai
function tch:tag/pc
function tch:tag/quai_pc

# On enregistre le fait qu'on a fait ces calculs
tag @s add PreStationDone
# On enregistre éventuellement si c'est un terminus "normal" ou temporaire (=fin de service pour ajd)
execute if score @e[type=item_frame,tag=ourQuaiPC,limit=1] idTerminus = @s idLine run tag @s add Terminus
execute if entity @e[type=item_frame,tag=ourPC,tag=FinDeService] run tag @s add FinDeService
# On enregistre s'il y a des correspondances
execute if data entity @e[type=item_frame,tag=ourQuaiPC,limit=1] Item.tag.display.corresp[0] run tag @s add Correspondance

# Il y a 23 secondes avant d'arriver en station (TODO: Une valeur non générique stockée dans chaque voie)
scoreboard players set @s displayTicks 21



# On reset notre PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
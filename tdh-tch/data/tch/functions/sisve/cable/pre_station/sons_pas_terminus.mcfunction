## ANNONCES SONORES SI ON NE FAIT PAS TERMINUS
#9s avant : Arrivée station
execute if score @s displayTicks matches 9 run function tch:sound/arrivee_station2/jouer
#6s avant si corresp : Rien oublier
execute if score @s[tag=Correspondance] displayTicks matches 6 positioned ~-2 ~-11 ~-2 at @a[dx=4,dy=6,dz=4] run function tch:sound/rienoublier
#4s avant si corresp : Rien oublier EN
execute if score @s[tag=Correspondance] displayTicks matches 4 positioned ~-2 ~-11 ~-2 at @a[dx=4,dy=6,dz=4] run function tch:sound/rienoublier_en
#2s avant si corresp : Rien oublier ES
execute if score @s[tag=Correspondance] displayTicks matches 2 positioned ~-2 ~-11 ~-2 at @a[dx=4,dy=6,dz=4] run function tch:sound/rienoublier_es
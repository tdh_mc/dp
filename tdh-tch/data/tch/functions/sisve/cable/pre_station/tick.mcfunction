## TICK DU SISVE EN PRÉ-STATION
# On a 20 displayTicks en tout (de 20 à 1)
# On ne réinitialise pas le timer, on veut qu'il reste à 0 une fois qu'il y est

# On tag les joueurs correspondant à notre cabine
execute positioned ~-2 ~-11 ~-2 run tag @a[dx=4,dy=6,dz=4] add SISVEPassager

## PARTIE 0 : Prise en compte du pré-station
# Si on n'a pas encore le tag qui indique qu'on a fait les calculs initiaux, on les fait
execute as @s[tag=!PreStationDone] run function tch:sisve/cable/pre_station/debut

## PARTIE I : Sonorisation
# D'abord les annonces :
# - Alerte arrivée station (*nom_station 1*)
execute if score @s displayTicks matches 18 run function tch:sound/arrivee_station1/jouer
# - Sous-fonction (avec la suite des sons) différente selon si on fait ou non Terminus
execute as @s[tag=Terminus] run function tch:sisve/cable/pre_station/sons_terminus
execute as @s[tag=FinDeService,tag=!Terminus] run function tch:sisve/cable/pre_station/sons_terminus
execute as @s[tag=!FinDeService,tag=!Terminus] run function tch:sisve/cable/pre_station/sons_pas_terminus

## PARTIE II : Titres actionbar
function tch:sisve/cable/pre_station/messages_standard


## PARTIE III : Réservation de la voie
# Si on est à 12 secondes de l'arrivée, on tente de réserver la voie
#execute if score @s[tag=!VoieReservee,tag=!FileAttente] displayTicks matches ..12 run function tch:metro/voie/reservation/test
# Si on est à 6 secondes de l'arrivée et qu'on est en file d'attente, on tente à nouveau de réserver la voie
#execute if score @s[tag=FileAttente] displayTicks matches ..6 run function tch:metro/voie/reservation/test2

# On retire le tag temporaire des joueurs
tag @a[tag=SISVEPassager] remove SISVEPassager
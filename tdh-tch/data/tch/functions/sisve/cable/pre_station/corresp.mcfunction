# On tag notre PC quai
function tch:tag/quai_pc

# On stocke le nombre de correspondances si on ne l'a pas encore fait
execute unless score @s temp7 matches 1.. store result score @s temp7 run data get entity @e[type=item_frame,tag=ourQuaiPC,limit=1] Item.tag.display.corresp

# On affiche les correspondances en lisant les données du PC quai
execute if score @s temp7 matches 1 positioned ~-2 ~-11 ~-2 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Correspondance ","color":"#8e856b"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s"},{"text":" : "},{"nbt":"Item.tag.display.corresp[0].nom","interpret":"true","entity":"@s"},{"text":"."}]
execute if score @s temp7 matches 2 positioned ~-2 ~-11 ~-2 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Correspondance ","color":"#8e856b"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s"},{"text":" : "},{"nbt":"Item.tag.display.corresp[0].nom","interpret":"true","entity":"@s"},{"text":", "},{"nbt":"Item.tag.display.corresp[1].nom","interpret":"true","entity":"@s"},{"text":"."}]
execute if score @s temp7 matches 3 positioned ~-2 ~-11 ~-2 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Correspondance ","color":"#8e856b"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s"},{"text":" : "},{"nbt":"Item.tag.display.corresp[0].nom","interpret":"true","entity":"@s"},{"text":", "},{"nbt":"Item.tag.display.corresp[1].nom","interpret":"true","entity":"@s"},{"text":", "},{"nbt":"Item.tag.display.corresp[2].nom","interpret":"true","entity":"@s"},{"text":"."}]
execute if score @s temp7 matches 4.. positioned ~-2 ~-11 ~-2 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Correspondance ","color":"#8e856b"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s"},{"text":" : "},{"nbt":"Item.tag.display.corresp[0].nom","interpret":"true","entity":"@s"},{"text":", "},{"nbt":"Item.tag.display.corresp[1].nom","interpret":"true","entity":"@s"},{"text":", "},{"nbt":"Item.tag.display.corresp[2].nom","interpret":"true","entity":"@s"},{"text":", "},{"nbt":"Item.tag.display.corresp[3].nom","interpret":"true","entity":"@s"},{"text":"."}]

# On supprime le tag temporaire
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
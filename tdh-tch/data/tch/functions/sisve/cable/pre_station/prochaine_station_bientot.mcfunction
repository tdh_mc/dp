# Message actionbar en pré-station
# 16-20: "Nous arrivons bientôt à XX"

# On récupère d'abord notre PC quai
function tch:tag/quai_pc

# On affiche ensuite le nom de la station
execute positioned ~-2 ~-11 ~-2 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Nous arriverons bientôt ","color":"#8e856b"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"red"},{"text":"."}]

# On réinitialise les variables et tags temporaires
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
# Message actionbar en pré-station
# 11-15: "Terminus" ou "Terminus FDS" si l'un des deux matche

# Si on a le tag FDS, on affiche le message de terminus FDS
execute as @s[tag=FinDeService,tag=!Terminus] positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Notre cabine est en ","color":"#8e856b"},{"text":"fin de service","color":"red"},{"text":" et fera "},{"text":"terminus","color":"red"},{"text":" à cette station."}]
# Si notre voie est le terminus normal, on affiche le message de terminus
execute as @s[tag=Terminus] positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Nous arrivons au ","color":"#8e856b"},{"text":"terminus","color":"red"},{"text":" de cette ligne."}]
# Sinon, on affiche le message normal
execute as @s[tag=!FinDeService,tag=!Terminus] run function tch:sisve/cable/pre_station/prochaine_station_bientot
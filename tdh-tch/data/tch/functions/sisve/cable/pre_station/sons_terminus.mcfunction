## ANNONCES SONORES SI ON FAIT TERMINUS
#12s avant: Arrivée station
execute if score @s displayTicks matches 12 run function tch:sound/arrivee_station2/jouer
#10s avant: Jingle Terminus
execute if score @s displayTicks matches 10 run function tch:sound/cable/annonce/terminus_jingle
#8s avant: Terminus
execute if score @s displayTicks matches 8 run function tch:sound/cable/annonce/terminus
#3s avant si applicable : Terminus EN
execute if score @s displayTicks matches 3 run function tch:sound/cable/annonce/terminus_2
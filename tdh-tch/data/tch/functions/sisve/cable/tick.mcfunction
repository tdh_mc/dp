## TICK DU SISVE CÂBLE
## Exécuté par une cabine du câble à sa position, si un joueur se trouve dedans la cabine (ou très proche)
# Si on a un timer, on le décrémente
scoreboard players remove @s displayTicks 1

# On affiche les informations correspondant à notre situation :
# - Si on est en cours d'arrivée en station
execute as @s[tag=PreStation,tag=!ArretUrgence] run function tch:sisve/cable/pre_station/tick
# - Si on est en arrêt d'urgence
execute as @s[tag=ArretUrgence] run function tch:sisve/cable/arret_urgence/tick
# - Si on est en station, et qu'il n'y a pas d'incident
execute as @s[tag=EnStation,tag=!Incident,tag=!FinDeService] run function tch:sisve/cable/en_station/tick
# - S'il y a un incident qui nous contraint à rester en station
execute as @s[tag=EnStation,tag=Incident] run function tch:sisve/cable/incident/tick
execute as @s[tag=EnStation,tag=FinDeService] run function tch:sisve/cable/fin_de_service/tick
# - Si on est en interstation
execute as @s[tag=!EnStation,tag=!PreStation,tag=!ArretUrgence] run function tch:sisve/cable/interstation/tick
# SISVE EN ARRÊT D'URGENCE PRÉ-STATION
# On a 6 displayTicks en tout (de 6 à 1)
# Si on n'a pas encore de timer ou qu'il a atteint 0, on le (ré?)initialise
execute unless score @s displayTicks matches 1.. run scoreboard players set @s displayTicks 6

# 4-6: "Le trafic reprend dans quelques instants"
execute if score @s displayTicks matches 4..6 positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Un incident nous a contraint à arrêter cette cabine.","color":"#8e856b"}]
# 1-3: "TCH présente ses excuses etc"
execute if score @s displayTicks matches 1..3 positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"TCH","color":"red"},{"text":" vous présente ses excuses pour la gêne occasionnée.","color":"#8e856b"}]
# On enregistre l'heure de reprise
scoreboard players operation #store2 currentHour = @s finIncident
function tdh:store/time2

execute positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Il est ","color":"#8e856b"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":". Reprise estimée vers "},{"nbt":"time2","storage":"tdh:store","interpret":"true","color":"gold"},{"text":"."}]
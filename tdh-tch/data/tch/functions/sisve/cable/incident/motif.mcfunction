# On récupère notre PC
function tch:tag/pc

execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 1 positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Un incident nous contraint à suspendre le trafic sur ","color":"#8e856b"},{"nbt":"Item.tag.display.line.vehicule.prefixe","entity":"@s"},{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true"},{"text":"."}]
execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 2..3 positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"","color":"#8e856b"},{"nbt":"Item.tag.tch.incident.generic.Article","entity":"@s"},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@s"},{"text":" nous contraint à suspendre le trafic sur "},{"nbt":"Item.tag.display.line.vehicule.prefixe","entity":"@s"},{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true"},{"text":"."}]

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
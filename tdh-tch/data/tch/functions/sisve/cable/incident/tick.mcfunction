# SISVE EN ARRÊT IMPROMPTU EN STATION
# On a 12 displayTicks en tout (de 12 à 1)
# Si on n'a pas encore de timer ou qu'il a atteint 0, on le (ré?)initialise
execute unless score @s displayTicks matches 1.. run scoreboard players set @s displayTicks 12

# 9-12: Motif incident
execute if score @s displayTicks matches 9..12 run function tch:sisve/cable/incident/motif
# 5-8: "TCH présente ses excuses etc"
execute if score @s displayTicks matches 5..8 positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"TCH","color":"red"},{"text":" vous présente ses excuses pour la gêne occasionnée.","color":"#8e856b"}]
# 1-4: Horaire reprise
execute if score @s displayTicks matches 1..4 run function tch:sisve/cable/incident/horaire
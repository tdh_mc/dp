# On récupère notre PC
function tch:tag/pc

# On enregistre l'heure actuelle
scoreboard players operation #store currentHour = #temps currentTdhTick
function tdh:store/time

execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 1..2 positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Il est ","color":"#8e856b"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":"."}]
execute as @e[type=item_frame,tag=ourPC,limit=1] if score @s status matches 3 run function tch:sisve/cable/incident/horaire_reprise

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
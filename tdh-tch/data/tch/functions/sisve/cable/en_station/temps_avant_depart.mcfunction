# On affiche le temps avant départ
scoreboard players set @s temp6 80
scoreboard players operation @s temp6 -= @s remainingTicks
scoreboard players set @s temp7 10
scoreboard players operation @s temp6 /= @s temp7
execute positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Départ dans ","color":"#8e856b"},{"score":{"name":"@s","objective":"temp6"},"color":"red"},{"text":" secondes."}]

scoreboard players reset @s temp6
scoreboard players reset @s temp7
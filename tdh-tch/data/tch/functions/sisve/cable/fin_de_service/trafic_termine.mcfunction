# On récupère notre PC
function tch:tag/pc

execute as @e[type=item_frame,tag=ourPC,limit=1] positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Le trafic est terminé sur ","color":"#8e856b"},{"nbt":"Item.tag.display.line.vehicule.prefixe","entity":"@s"},{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true"},{"text":"."}]

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
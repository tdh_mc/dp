# On récupère notre PC
function tch:tag/pc

# On enregistre l'heure actuelle
scoreboard players operation #store currentHour = #temps currentTdhTick
function tdh:store/time
# On enregistre l'heure de début de service
scoreboard players operation #store2 currentHour = @e[type=item_frame,tag=ourPC,limit=1] debutServiceAjd
function tdh:store/time2

execute as @e[type=item_frame,tag=ourPC,limit=1] positioned ~-2 ~-11 ~-2 run title @a[dx=4,dy=6,dz=4] actionbar [{"text":"Il est ","color":"#8e856b"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":". Reprise du service à ","color":"#8e856b"},{"nbt":"time2","storage":"tdh:store","interpret":"true","color":"red"},{"text":"."}]

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
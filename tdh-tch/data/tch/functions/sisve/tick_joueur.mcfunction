## TICK DU SISVE POUR UN JOUEUR
# On vérifie si le joueur est dans un mode de transport TCH, auquel cas on tag le mode de transport en question
execute as @e[type=minecart,tag=Metro,distance=..1,sort=nearest,limit=1] run tag @s add SISVECalculMetro
execute as @e[type=armor_stand,tag=CabineCable,distance=..11,sort=nearest,limit=1] run tag @s add SISVECalculCable
execute at @p[distance=0,tag=RERPassager] run function tch:sisve/tick_joueur_rer
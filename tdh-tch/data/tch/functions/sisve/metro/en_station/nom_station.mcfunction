# On affiche le nom de la station du métro où le métro qui exécute cette fonction est arrêté

# On récupère d'abord notre PC quai
function tch:tag/quai_pc

# On affiche ensuite le nom de la station
execute unless score @s temp matches 1 if score @p lightLevel matches ..7 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[distance=..1] actionbar [{"text":"Vous êtes ","color":"#f1ede3"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"red"},{"text":"."}]
execute unless score @s temp matches 1 if score @p lightLevel matches 8.. as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[distance=..1] actionbar [{"text":"Vous êtes ","color":"#1c0000"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"dark_red"},{"text":"."}]


# On réinitialise les variables et tags temporaires
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
scoreboard players reset @s temp
## TICK DU SISVE EN STATION

# Plus de 5 secondes restantes : On affiche le nom de la station
execute if score @s remainingTicks matches 6.. run function tch:sisve/metro/en_station/nom_station
# Moins de 5 secondes restantes : On affiche le temps restant avant le départ
execute if score @s remainingTicks matches ..5 run function tch:sisve/metro/en_station/temps_avant_depart
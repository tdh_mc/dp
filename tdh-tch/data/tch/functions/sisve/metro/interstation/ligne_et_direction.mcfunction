# On affiche le numéro de ligne et le terminus en utilisant le PC terminus
function tch:tag/terminus_pc
# On affiche de la bonne couleur selon la lumière ambiante
execute if score @p lightLevel matches ..7 as @e[type=item_frame,tag=ourTerminusPC,limit=1] run title @a[distance=..1] actionbar [{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true","color":"#f1ede3"},{"text":" → "},{"nbt":"Item.tag.display.station.prefixe","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s"}]
execute if score @p lightLevel matches 8.. as @e[type=item_frame,tag=ourTerminusPC,limit=1] run title @a[distance=..1] actionbar [{"nbt":"Item.tag.display.line.nom","entity":"@s","interpret":"true","color":"#1c0000"},{"text":" → "},{"nbt":"Item.tag.display.station.prefixe","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s"}]

# On supprime le tag du PC terminus
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
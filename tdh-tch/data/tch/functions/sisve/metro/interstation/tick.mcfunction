## TICK DU SISVE EN INTERSTATION
# On a 12 displayTicks en tout (de 12 à 1)
# Si on n'a pas encore de timer ou qu'il a atteint 0, on le (ré?)initialise
execute unless score @s displayTicks matches 1.. run scoreboard players set @s displayTicks 12

# 9-12: Afficher l'ID line
execute if score @s displayTicks matches 9.. run function tch:sisve/metro/interstation/ligne_et_direction
# 5-8 : Afficher la prochaine station
execute if score @s displayTicks matches 5..8 run function tch:sisve/metro/interstation/prochaine_station
# 1-4 : Afficher l'heure
execute if score @s displayTicks matches ..4 run function tch:sisve/metro/interstation/heure
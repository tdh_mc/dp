# SISVE EN ARRÊT D'URGENCE PRÉ-STATION
# 4-6: "Le trafic reprend dans quelques instants"
execute if score @s remainingTicks matches 4.. if score @p lightLevel matches ..7 run title @a[distance=..1] actionbar [{"text":"Notre train redémarrera dans quelques instants.","color":"red"}]
execute if score @s remainingTicks matches 4.. if score @p lightLevel matches 8.. run title @a[distance=..1] actionbar [{"text":"Notre train redémarrera dans quelques instants.","color":"#1c0000"}]
# 1-3: "TCH présente ses excuses etc"
execute if score @s remainingTicks matches ..3 if score @p lightLevel matches ..7 run title @a[distance=..1] actionbar [{"text":"TCH vous présente ses excuses pour la gêne occasionnée.","color":"red"}]
execute if score @s remainingTicks matches ..3 if score @p lightLevel matches 8.. run title @a[distance=..1] actionbar [{"text":"TCH vous présente ses excuses pour la gêne occasionnée.","color":"#1c0000"}]
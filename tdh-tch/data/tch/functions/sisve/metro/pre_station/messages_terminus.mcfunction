# Message actionbar en pré-station
# 11-15: "Terminus" ou "Terminus FDS" si l'un des deux matche

# Si on a le tag FDS, on affiche le message de terminus FDS
execute as @s[tag=FinDeService,tag=Terminus] if score @p lightLevel matches ..7 run title @a[distance=..1] actionbar [{"text":"Notre train est en ","color":"#f1ede3"},{"text":"fin de service","color":"red"},{"text":" et fera "},{"text":"terminus","color":"red"},{"text":" à cette station."}]
execute as @s[tag=FinDeService,tag=Terminus] if score @p lightLevel matches 8.. run title @a[distance=..1] actionbar [{"text":"Notre train est en ","color":"#1c0000"},{"text":"fin de service","color":"dark_red"},{"text":" et fera "},{"text":"terminus","color":"dark_red"},{"text":" à cette station."}]
# Si notre voie est le terminus normal, on affiche le message de terminus
execute as @s[tag=!FinDeService,tag=Terminus] if score @p lightLevel matches ..7 run title @a[distance=..1] actionbar [{"text":"Nous arrivons au ","color":"#f1ede3"},{"text":"terminus","color":"red"},{"text":" de cette ligne."}]
execute as @s[tag=!FinDeService,tag=Terminus] if score @p lightLevel matches 8.. run title @a[distance=..1] actionbar [{"text":"Nous arrivons au ","color":"#1c0000"},{"text":"terminus","color":"dark_red"},{"text":" de cette ligne."}]
# Sinon, on affiche le message normal
execute as @s[tag=!FinDeService,tag=!Terminus] run function tch:sisve/metro/pre_station/prochaine_station_bientot
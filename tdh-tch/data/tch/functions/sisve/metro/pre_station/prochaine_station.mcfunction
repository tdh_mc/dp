# Message actionbar en pré-station
# 1-5: "Nous arrivons à XX"

# On récupère d'abord notre PC quai
function tch:tag/prochain_quai_pc

# On affiche ensuite le nom de la station
execute if score @p lightLevel matches ..7 as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[distance=..1] actionbar [{"text":"Nous arrivons ","color":"#f1ede3"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"red"},{"text":" dans un instant."}]
execute if score @p lightLevel matches 8.. as @e[type=item_frame,tag=ourQuaiPC,limit=1] run title @a[distance=..1] actionbar [{"text":"Nous arrivons ","color":"#1c0000"},{"nbt":"Item.tag.display.station.alAu","entity":"@s"},{"nbt":"Item.tag.display.station.nom","entity":"@s","color":"dark_red"},{"text":" dans un instant."}]


# On réinitialise les variables et tags temporaires
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC


# S'il nous reste la variable temporaire de la fonction précédente (*corresp*), on la supprime
scoreboard players reset @s temp7
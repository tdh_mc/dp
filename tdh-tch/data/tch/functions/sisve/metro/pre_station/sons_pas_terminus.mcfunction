## ANNONCES SONORES SI ON NE FAIT PAS TERMINUS
#14s avant si applicable : Descente à gauche anglais
execute if score @s[tag=DescenteGauche] displayTicks matches 14 run function tch:sound/descente_gauche_en
#9s avant : Arrivée station
execute if score @s displayTicks matches 9 run function tch:sound/arrivee_station2/jouer
#6s avant si corresp : Rien oublier
execute if score @s[tag=Correspondance] displayTicks matches 6 run function tch:sound/rienoublier
#4s avant si corresp : Rien oublier EN
execute if score @s[tag=Correspondance] displayTicks matches 4 run function tch:sound/rienoublier_en
#2s avant si corresp : Rien oublier ES
execute if score @s[tag=Correspondance] displayTicks matches 2 run function tch:sound/rienoublier_es

# Ensuite les sons d'ambiance :
# - L'arrivée du métro (7s avant)
execute if score @s displayTicks matches 7 run function tch:sisve/metro/pre_station/sons_arrivee_metro
# - Le son "Prochain train dans une minute" sur le PID, si on a bien réservé la voie
execute if score @s[tag=VoieReservee] displayTicks matches 4 run function tch:station/pid/forcer_diffusion
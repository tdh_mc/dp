# Exécutée par un métro une fois lorsqu'il détecte pour la première fois sa prochaine station
# La voie (AS "Direction") est déjà enregistrée en tant que "ourVoie" par la fonction test_debut.

# On enregistre notre PC et notre PC quai
function tch:tag/pc
function tch:tag/prochain_quai_pc

# On enregistre le fait qu'on approche de la station
tag @s add PreStation
# On enregistre éventuellement si c'est un terminus "normal" ou temporaire (=fin de service pour ajd)
execute if entity @e[type=armor_stand,tag=ourVoie,tag=Terminus] run tag @s add Terminus
execute as @s[tag=!Terminus] if entity @e[type=item_frame,tag=ourPC,tag=FinDeService] run tag @s add FinDeService
execute as @s[tag=FinDeService] run tag @s add Terminus
# On enregistre si on descend à gauche ou normalement
execute if entity @e[type=armor_stand,tag=ourVoie,tag=StationInversee] run tag @s add DescenteGauche
# On enregistre s'il y a des correspondances
execute if data entity @e[type=item_frame,tag=ourQuaiPC,limit=1] Item.tag.display.corresp[0] run tag @s add Correspondance

# Il y a 23 secondes avant d'arriver en station
scoreboard players set @s displayTicks 21
# On ajoute le décalage du SISVE (enregistré dans notre variable "variation")
scoreboard players operation @s displayTicks += @s variation



# On reset notre PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
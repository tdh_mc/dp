# On essaie de récupérer notre voie
function tch:tag/prochaine_voie

# Si elle existe, on initialise tout ce qu'il faut
execute if entity @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] run function tch:sisve/metro/pre_station/debut

# On reset notre voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie] remove ourVoie
## TICK DU SISVE MÉTRO
## Exécuté par un métro à sa position, si un joueur se trouve dans le métro
# Si on a un timer, on le décrémente
scoreboard players remove @s[tag=!ArretUrgence] displayTicks 1

# On vérifie si on approche de notre station
execute as @s[tag=!EnStation,tag=!PreStation] run function tch:sisve/metro/pre_station/test_debut

# On affiche les informations correspondant à notre situation :
# - Si on est en cours d'arrivée en station
execute as @s[tag=PreStation,tag=!ArretUrgence] run function tch:sisve/metro/pre_station/tick
# - Si on est en arrêt d'urgence
execute as @s[tag=ArretUrgence] run function tch:sisve/metro/arret_urgence/tick
# - Si on est en station
execute as @s[tag=EnStation] run function tch:sisve/metro/en_station/tick
# - Si on est en interstation
execute as @s[tag=!EnStation,tag=!PreStation] run function tch:sisve/metro/interstation/tick
## TICK DU SISVE (système d'information à bord)
# On vérifie pour chaque joueur pas en spectateur s'il faut lui afficher un titre
# On commence par tagger tous les modes de transport qui doivent calculer leur SISVE
execute at @a run function tch:sisve/tick_joueur

# Ensuite, on exécute le tick du SISVE pour chaque mode de transport taggé
execute as @e[type=minecart,tag=SISVECalculMetro] at @s run function tch:sisve/metro/tick
execute as @e[type=armor_stand,tag=SISVECalculCable] at @s run function tch:sisve/cable/tick
execute as @e[type=armor_stand,tag=SISVECalculRER] at @s run function tch:sisve/rer/tick

# On retire les tags temporaires
tag @e[type=minecart,tag=SISVECalculMetro] remove SISVECalculMetro
tag @e[type=armor_stand,tag=SISVECalculCable] remove SISVECalculCable
tag @e[type=armor_stand,tag=SISVECalculRER] remove SISVECalculRER
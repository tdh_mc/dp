# On est un signal à sa propre position

# On affiche une particule selon notre statut
execute if score @s status matches 2.. run particle dust 1 0 0 5 ~ ~2 ~ 0 0 0 1 5 force
execute if score @s status matches 1 run particle dust 1 .4 0 5 ~ ~2 ~ 0 0 0 1 5 force
execute unless score @s status matches 1.. run particle dust 0 1 0 5 ~ ~2 ~ 0 0 0 1 5 force
# Paramètres des signaux
scoreboard objectives add idSignal dummy "Identifiant de signal"
scoreboard objectives add prochainSignal dummy "Prochain signal"

# Paramètres de génération des nouveaux signaux
scoreboard players set #signalGen idSignal 1
scoreboard players set #signalGen prochainSignal 0
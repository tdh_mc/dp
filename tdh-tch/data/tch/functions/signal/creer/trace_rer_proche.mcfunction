# On « crée » un signal sur la même entité qu’un tracé RER existant
# On exécute cette fonction à proximité d’un tracé RER existant, mais pas besoin d’execute as puisque c’est fait ci-dessous

# On n’exécute pas la suite des opérations si le tracé proche est déjà un signal
execute as @e[type=armor_stand,tag=TraceRER,distance=..5,sort=nearest,limit=1] unless entity @s[tag=Signal,scores={idSignal=1..}] run function tch:signal/creer/existant
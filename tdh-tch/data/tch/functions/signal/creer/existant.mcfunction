# On modifie le tracé existant (RER ou câble) qui exécute cette fonction pour le transformer en signal
# On peut éventuellement définir la variable #signalGen prochainSignal avant l’appel, pour définir également le prochain signal (sinon il vaudra 0)
tag @s add Signal
scoreboard players operation @s idSignal = #signalGen idSignal
scoreboard players add #signalGen idSignal 1
scoreboard players operation @s prochainSignal = #signalGen prochainSignal
scoreboard players set #signalGen prochainSignal 0

tellraw @a[tag=SignalDebugLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Création du signal.","color":"gray"}]
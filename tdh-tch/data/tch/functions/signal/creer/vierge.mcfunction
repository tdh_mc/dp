# On crée un signal vierge (utilisable pour les métros)
# On peut éventuellement définir la variable #signalGen prochainSignal avant l’appel, pour définir également le prochain signal (sinon il vaudra 0)

execute align xyz run summon marker ~.5 ~ ~.5 {Tags:["Signal","SignalInvoqueTemp"]}
scoreboard players operation @e[type=marker,tag=SignalInvoqueTemp,limit=1] idSignal = #signalGen idSignal
scoreboard players add #signalGen idSignal 1
scoreboard players operation @e[type=marker,tag=SignalInvoqueTemp,limit=1] prochainSignal = #signalGen prochainSignal
scoreboard players set #signalGen prochainSignal 0

execute as @e[type=marker,tag=SignalInvoqueTemp] run tellraw @a[tag=SignalDebugLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Création du signal.","color":"gray"}]

tag @e[type=marker,tag=SignalInvoqueTemp] remove SignalInvoqueTemp
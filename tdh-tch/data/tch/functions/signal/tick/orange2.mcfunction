# On tag les signaux correspondant à notre ID de prochain signal
function tch:tag/prochain_signal

tellraw @a[tag=SignalDetailLog] [{"text":"→ [Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Vérification du prochain signal.","color":"gray"}]

# On exécute la suite des opérations pour chacun des prochains signaux
execute as @e[type=#tch:marqueur/signal,tag=ourNextSignal] run function tch:signal/tick/orange3

# On retire les tags temporaires de nos prochains signaux
tag @e[type=#tch:marqueur/signal,tag=ourNextSignal] remove ourNextSignal

tag @s add SignalCalcule
# On ajoute les infos du signal actuel à la liste des signaux

tag @s add tempSignal
data modify block ~ ~ ~ Text2 set value '[{"block":"~ ~ ~","nbt":"Text1","interpret":"true"},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=tempSignal]","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=tempSignal]","objective":"prochainSignal"}}]'
data modify block ~ ~ ~ Text1 set value '[{"block":"~ ~ ~","nbt":"Text2","interpret":"true"},{"text":", "}]'
tag @s remove tempSignal
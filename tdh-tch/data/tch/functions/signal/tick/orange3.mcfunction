# On exécute cette fonction en tant qu’un prochain signal (1 bloc plus loin que le signal d’origine)
# pour récupérer l’ID de « prochain prochain » signal (2 blocs plus loin que le signal d’origine)
# et vérifier si des RER possèdent cet ID
scoreboard players set #signal prochainSignal 0
scoreboard players operation #signal prochainSignal = @s prochainSignal
# Si on trouve un véhicule ayant cet ID de signal, c’est qu’il occupe encore le bloc suivant : on modifie la variable
execute if score #signal prochainSignal matches 1.. as @e[type=#tch:vehicule,tag=AllowSignals] if score @s idSignal = #signal prochainSignal run scoreboard players set #signal status 9991

execute if score #signal status matches 9991 run tellraw @a[tag=SignalDetailLog] [{"text":"→ [Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Détection d’un véhicule ayant pour ID de signal ","color":"gray","extra":[{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"."}]}]
execute unless score #signal status matches 9991 run tellraw @a[tag=SignalDetailLog] [{"text":"→ [Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Aucun RER détecté.","color":"gray"}]

execute if score #signal status matches 9991 run scoreboard players set #signal status 1
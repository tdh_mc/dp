# Si le signal est orange, on le passe au vert si on ne détecte pas de RER ayant l’ID du signal suivant

# On a déjà taggé tous les signaux de même ID que nous avec le tag "ourSignal"

# On définit le futur statut du signal
# (par défaut il repasse au vert sauf si on détecte un RER dans les blocs suivants)
scoreboard players set #signal status 0

# On tag tous les signaux correspondant au prochainSignal de **chacun** des signaux ayant le même ID que nous
execute as @e[type=#tch:marqueur/signal,tag=ourSignal] run function tch:signal/tick/orange2

# On définit notre statut en fonction de la variable statut calculée précédemment
scoreboard players operation @e[type=#tch:marqueur/signal,tag=ourSignal] status = #signal status

# On modifie tous les modèles correspondant à notre signal
scoreboard players operation #signal idSignal = @s idSignal
execute if score #signal status matches 0 positioned as @s as @e[type=item_frame,tag=SignalModel,distance=..60] if score @s idSignal = #signal idSignal positioned as @s run function tch:signal/modele/vert

execute if score @s status matches 0 run tellraw @a[tag=SignalDebugLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Passage à l’état ","color":"gray"},{"text":"VERT","color":"green"},{"text":".","color":"gray"}]
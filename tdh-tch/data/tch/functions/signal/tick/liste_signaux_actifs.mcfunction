# On liste tous les signaux actifs dans le DétailLog
# On doit être à la position d’une item frame TexteTemporaire

data modify block ~ ~ ~ Text1 set value '""'
execute as @e[type=#tch:marqueur/signal,tag=Signal,scores={status=2..}] run function tch:signal/tick/liste_signaux_actifs/signal
tellraw @a[tag=SignalDetailLog] [{"text":"– Signaux rouges : ","color":"red"},{"block":"~ ~ ~","nbt":"Text1","interpret":"true"}]

data modify block ~ ~ ~ Text1 set value '""'
execute as @e[type=#tch:marqueur/signal,tag=Signal,scores={status=1}] run function tch:signal/tick/liste_signaux_actifs/signal
tellraw @a[tag=SignalDetailLog] [{"text":"– Signaux oranges : ","color":"gold"},{"block":"~ ~ ~","nbt":"Text1","interpret":"true"}]

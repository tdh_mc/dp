# Si le RER qui exécute cette fonction a le même ID de signal que #signal prochainSignal,
# c’est qu’il occupe encore ce bloc, et le signal peut rester rouge (status = 2,
#        mais on le passe à 9992 pour signaler qu’on VIENT de le détecter)
execute if score @s idSignal = #signal prochainSignal run scoreboard players set #signal status 9992
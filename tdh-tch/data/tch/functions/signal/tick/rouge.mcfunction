# Si le signal est rouge, on le passe à l’orange si on ne détecte pas de RER ayant son ID

# On a déjà taggé tous les signaux de même ID que nous avec le tag "ourSignal"

# On définit le futur statut du signal
# (par défaut il repasse à l’orange sauf si on détecte un RER dans les blocs suivants)
scoreboard players set #signal status 1

# On enregistre l’ID de signal pour pouvoir chercher les RER correspondants
scoreboard players operation #signal idSignal = @s idSignal

# Si on trouve un véhicule ayant cet ID de signal, c’est qu’il occupe encore notre bloc : on modifie la variable STATUS pour la repasser à 2 (ROUGE)
execute as @e[type=#tch:vehicule,tag=AllowSignals] run function tch:signal/tick/test_signal

execute if score #signal status matches 2 run tellraw @a[tag=SignalDetailLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Un RER a été trouvé avec l’ID de signal ","color":"gray","extra":[{"score":{"name":"@s","objective":"idSignal"}},{"text":". Maintien de l’état "},{"text":"ROUGE","color":"red"},{"text":"."}]}]

# On exécute ensuite le test de prochain signal pour chacune des entités partageant notre ID de signal
# (car chacune peut pointer vers un signal différent)
execute unless score #signal status matches 2 as @e[type=#tch:marqueur/signal,tag=ourSignal] run function tch:signal/tick/rouge2

# On définit notre statut en fonction du statut calculé par la fonction précédente
scoreboard players operation @e[type=#tch:marqueur/signal,tag=ourSignal] status = #signal status

# On modifie tous les modèles correspondant à notre signal
execute if score #signal status matches 1 positioned as @s as @e[type=item_frame,tag=SignalModel,distance=..60] if score @s idSignal = #signal idSignal run function tch:signal/modele/orange

execute if score @s status matches 1 run tellraw @a[tag=SignalDebugLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Aucun véhicule n’a l’ID de signal ","color":"gray","extra":[{"score":{"name":"@s","objective":"idSignal"}},{"text":" ou "},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":". Passage à l’état "},{"text":"ORANGE","color":"gold"},{"text":"."}]}]
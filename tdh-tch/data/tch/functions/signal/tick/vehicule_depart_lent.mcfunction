# On exécute cette fonction en tant qu’un véhicule, pour redémarrer après un arrêt d’urgence
# Le signal qui nous a déclenchés a un tag ourSignal qui sera retiré par la fonction appelante

# Si on est un RER, on redémarre avec la fonction appropriée
execute as @s[tag=RERTete] run function tch:rer/rame/arret_urgence/depart_lent
# Si on est un câble ou un métro, TODO
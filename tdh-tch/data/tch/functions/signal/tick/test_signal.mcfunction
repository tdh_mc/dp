# Si le RER qui exécute cette fonction a le même ID de signal que #signal,
# c’est qu’il occupe encore ce bloc, et le signal peut rester rouge (status = 2)
execute if score @s idSignal = #signal idSignal run scoreboard players set #signal status 2
# On enregistre l’ID de prochain signal
scoreboard players operation #signal prochainSignal = @s prochainSignal

# Si on trouve un véhicule ayant cet ID de signal, c’est qu’il occupe encore le bloc suivant : on modifie la variable STATUS pour la repasser à 2 (ROUGE)
execute as @e[type=#tch:vehicule,tag=AllowSignals] run function tch:signal/tick/test_prochain_signal

execute if score #signal status matches 9992 run tellraw @a[tag=SignalDetailLog] [{"text":"→ [Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Un RER a été trouvé avec l’ID de signal ","color":"gray","extra":[{"score":{"name":"@s","objective":"prochainSignal"}},{"text":". Maintien de l’état "},{"text":"ROUGE","color":"red"},{"text":"."}]}]
execute if score #signal status matches 9992 run scoreboard players set #signal status 2

tag @s add SignalCalcule
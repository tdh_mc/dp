# Tick global des signaux

# Tous les véhicules en arrêt d’urgence (état déclenché par un signal rouge) sont tickés
# pour vérifier s’il leur est possible de redémarrer
execute as @e[type=#tch:vehicule,tag=AllowSignals,tag=ArretUrgence] run function tch:signal/tick_vehicule

# Tous les signaux qui sont ROUGES (status = 2) ou ORANGES (status = 1) sont tickés,
# sauf s’ils sont BLOQUÉS (tag qui permet de forcer un signal à rester orange / rouge pour faire des tests de freinage)
execute as @e[type=#tch:marqueur/signal,tag=Signal,tag=!SignalBloque,scores={status=1..}] unless entity @s[tag=SignalCalcule] run function tch:signal/tick_signal
execute if entity @p[tag=SignalDetailLog] if entity @e[type=#tch:marqueur/signal,tag=Signal,scores={status=1..}] at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:signal/tick/liste_signaux_actifs

# On retire les tags temporaires des signaux
tag @e[type=#tch:marqueur/signal,tag=SignalCalcule] remove SignalCalcule
# Tick d’un signal actif (rouge ou orange)
# Exécuté par le signal en question, mais sans execute at (on se fiche de la position)

tellraw @a[tag=SignalDetailLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Tick ","color":"gray","extra":[{"score":{"name":"#temps","objective":"currentTdhTick"}},{"text":"(état actuel "},{"score":{"name":"@s","objective":"status"},"bold":"true"},{"text":")"}]}]

# On tag tous les signaux correspondant à notre ID de signal (car il peut y avoir plusieurs entités par signal)
function tch:tag/signal

# Si le signal est rouge, on le passe à l’orange si on ne détecte pas de RER ayant son ID
execute if score @s status matches 2.. run function tch:signal/tick/rouge
# Si le signal est orange, on le repasse au vert si on ne détecte pas de RER ayant son ID de prochain signal
execute if score @s status matches 1 run function tch:signal/tick/orange

# On retire les tags temporaires de nos signaux
tag @e[type=#tch:marqueur/signal,tag=ourSignal] remove ourSignal
# Tick des véhicules arrêtés à des signaux
# On exécute cette fonction en tant qu’un véhicule ayant le tag ArretUrgence (donné par un signal rouge)
# pour vérifier si le signal a changé de couleur (et donc s’il leur est possible de redémarrer)

# On récupère l’état de notre signal si on le trouve
scoreboard players set #signal status 0
function tch:tag/prochain_signal
scoreboard players operation #signal status = @e[type=#tch:marqueur/signal,tag=ourNextSignal,limit=1] status
# Si un autre RER occupe notre signal, on définit également la variable à 2 (= pas de changement)
scoreboard players operation #signal idSignal = @s idSignal
execute at @s as @e[type=armor_stand,tag=RERTete] if score @s idSignal = #signal idSignal if entity @s[distance=1..] run scoreboard players set #signal status 2

# Si le signal est vert, on redémarre en vitesse normale
execute if score #signal status matches 0 run function tch:signal/tick/vehicule_depart
# Si le signal est orange, on redémarre en vitesse réduite
execute if score #signal status matches 1 run function tch:signal/tick/vehicule_depart_lent

# On supprime le tag temporaire du signal
tag @e[type=#tch:marqueur/signal,tag=ourNextSignal] remove ourNextSignal
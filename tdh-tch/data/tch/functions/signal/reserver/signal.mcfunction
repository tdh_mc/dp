# Si le signal est déjà rouge c’est problématique dis donc donc on affiche un message d’erreur
execute if score @s status matches 2.. run tellraw @a[tag=SignalDebugLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" [ERREUR] On vient de réserver un signal qui était déjà rouge.","color":"red"}]

# On modifie le modèle de notre signal
scoreboard players operation #signal idSignal = @s idSignal
execute positioned as @s as @e[type=item_frame,tag=SignalModel,distance=..60] if score @s idSignal = #signal idSignal run function tch:signal/modele/rouge

# Si le signal est orange ou vert, on le passe au rouge
scoreboard players set @s status 2
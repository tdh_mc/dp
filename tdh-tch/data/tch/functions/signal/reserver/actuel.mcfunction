# On « réserve » le signal ayant le même ID que l’entité appelante
# La valeur d’entrée (#signal status) indique le statut maximal (inclusif) du signal
# Par exemple avec #signal status = 1, on réservera le signal uniquement s’il est vert ou orange

# Si la réservation réussit, on se donne un tag "ReservationReussie"

# On tag tous les signaux de même ID que nous
function tch:tag/signal

# On retire ces tags si les valeurs testées ne matchent pas
execute as @e[type=#tch:marqueur/signal,tag=ourSignal] if score @s status > #signal status run function tch:signal/reserver/echec

# On réserve chacun de ces signaux
execute if entity @e[type=#tch:marqueur/signal,tag=ourSignal,limit=1] run tag @s add ReservationReussie
execute as @e[type=#tch:marqueur/signal,tag=ourSignal] run function tch:signal/reserver/signal

# On retire les tags temporaires des signaux
tag @e[type=#tch:marqueur/signal,tag=ourSignal] remove ourSignal
# On « réserve » le signal ayant le même ID que l’entité appelante
# La valeur d’entrée (#signal status) indique le statut maximal (inclusif) du signal
# Par exemple avec #signal status = 1, on réservera le signal uniquement s’il est vert ou orange
# On peut également utiliser #previousSignal status pour tester le statut maximal des signaux PRÉCÉDENTS,
# et #nextSignal status pour tester le statut maximal du signal SUIVANT

# Si la réservation réussit, on se donne un tag "ReservationReussie"

# On tag tous les signaux de même ID que nous, ainsi que tous les signaux précédents
function tch:tag/signal
function tch:tag/precedents_signaux
function tch:tag/prochain_signal

# On retire ces tags si les valeurs testées ne matchent pas
execute as @e[type=#tch:marqueur/signal,tag=ourSignal] if score @s status > #signal status run function tch:signal/reserver/echec
execute as @e[type=#tch:marqueur/signal,tag=ourPreviousSignal] if score @s status > #previousSignal status run function tch:signal/reserver/echec
execute as @e[type=#tch:marqueur/signal,tag=ourNextSignal] if score @s status > #nextSignal status run function tch:signal/reserver/echec

# On réserve chacun des signaux actuels
execute if entity @e[type=#tch:marqueur/signal,tag=ourSignal,limit=1] run tag @s add ReservationReussie
execute as @e[type=#tch:marqueur/signal,tag=ourSignal] run function tch:signal/reserver/signal

# Tous les signaux précédents qui ne sont pas déjà rouges passent au rouge
execute as @e[type=#tch:marqueur/signal,tag=ourPreviousSignal] unless score @s status matches 2.. run function tch:signal/reserver/signal

# On retire les tags temporaires des signaux
tag @e[type=#tch:marqueur/signal,tag=ourSignal] remove ourSignal
tag @e[type=#tch:marqueur/signal,tag=ourPreviousSignal] remove ourPreviousSignal
tag @e[type=#tch:marqueur/signal,tag=ourNextSignal] remove ourNextSignal
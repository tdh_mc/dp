# Si le signal est déjà rouge c’est problématique dis donc donc on affiche un message d’erreur
tellraw @a[tag=SignalDebugLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" Échec de la réservation (","color":"red","extra":[{"selector":"@s"},{"text":")."}]}]

# On retire les tags temporaires des signaux
tag @e[type=#tch:marqueur/signal,tag=ourSignal] remove ourSignal
tag @e[type=#tch:marqueur/signal,tag=ourPreviousSignal] remove ourPreviousSignal
tag @e[type=#tch:marqueur/signal,tag=ourNextSignal] remove ourNextSignal
# On calcule la valeur de l'heure d'ouverture
# Non modifiable manuellement, elle dépend des heures de service des lignes qui y passent

# On initialise les variables
scoreboard players set #temp debutServiceAjd 12
scoreboard players operation #temp debutServiceAjd *= #temps ticksPerHour

# Pour chaque ligne qui passe ici, on vérifie si son heure d'ouverture est plus tôt que l'actuelle
# On tag d'abord le PC de chaque ligne
execute at @s as @e[type=armor_stand,tag=NumLigne,distance=..150] run function tch:tag/pc
# On lit ensuite l'heure d'ouverture aujourd'hui de chaque ligne, et on prend à chaque fois le minimum
execute as @e[type=item_frame,tag=ourPC,tag=!HorsService] run scoreboard players operation #temp debutServiceAjd < @s debutServiceAjd

# L'heure de début de service est 30mn avant la première ouverture de ligne
scoreboard players remove #temp debutServiceAjd 1000
# On enregistre l'horaire calculé
scoreboard players operation @s debutServiceAjd = #temp debutServiceAjd
scoreboard players reset #temp debutServiceAjd

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
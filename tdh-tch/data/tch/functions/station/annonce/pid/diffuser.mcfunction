# On exécute régulièrement cette fonction à l'aide d'un schedule,
# pour jouer le prochain son d'annonce PID

# On donne un tag à tous les haut-parleurs de PID qui vont diffuser notre message
# On prend notre ID station si on en possède un et celui de la station la plus proche sinon
scoreboard players operation #pid idLine = @s idLine
execute if score @s prochaineLigne matches 1.. run scoreboard players operation #pid idLine = @s prochaineLigne
scoreboard players operation #pid idStation = @s[scores={idStation=1..}] idStation
execute unless score @s idStation matches 1.. run scoreboard players operation #pid idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation
execute as @e[type=item_frame,tag=PID,tag=!Muet] if score @s idLine = #pid idLine if score @s idStation = #pid idStation run tag @s add tchAnnoncePIDDiffuseur

# On incrémente notre ID de son diffusé
execute if score @s annonceID matches 1.. run scoreboard players add @s annonceIndex 1

# On exécute une sous-fonction selon notre ID d'annonce
execute if score @s annonceID matches 4001 run function tch:station/annonce/pid/diffuser/metro/dogo
execute if score @s annonceID matches 4002 run function tch:station/annonce/pid/diffuser/metro/info
# Le PID RER est TODO et nécessitera de gérer les directions multiples
# (message du type "Direction XX, le prochain dans 1mn. Direction YY, le prochain dans Ymn. Direction ZZ, le prochain dans Zmn.")

# On retire le tag de tous les diffuseurs du message
tag @e[type=item_frame,tag=tchAnnoncePIDDiffuseur] remove tchAnnoncePIDDiffuseur
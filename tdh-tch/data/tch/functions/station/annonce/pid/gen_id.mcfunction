# On génère un ID d'annonce à diffuser
# Cette fonction est exécutée par un spawner (peu importe le mode de transport),
# pour générer un ID d'annonce PID et enregistrer les scores nécessaires à sa diffusion

# On prend un nombre aléatoire (qui sera utilisé dans les sous-fonctions)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random


# On génère un ID d'annonce selon notre ID de ligne
# - RER (TODO)
execute if score @s idLine matches 2000..3999 run function tch:station/annonce/pid/gen_id/metro
# - Générique (métro/câble)
execute unless score @s idLine matches 2000..3999 run function tch:station/annonce/pid/gen_id/metro

# Si on a réussi à générer un ID d'annonce, on se donne le tag de diffusion
execute if score @s annonceID matches 1.. run tag @s add tchAnnoncePIDSpawner
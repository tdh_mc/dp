# On termine la diffusion de l'annonce actuelle
# On réinitialise nos scores
scoreboard players set @s annonceID 0
scoreboard players set @s annonceIndex 0
scoreboard players set @s annonceTicks 0

# On retire notre tag de diffusion
tag @s remove tchAnnoncePIDSpawner
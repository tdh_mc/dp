# On joue le son à la position de tous les diffuseurs taggés
execute as @e[type=item_frame,tag=tchAnnoncePIDDiffuseur] at @s run function tch:sound/annonce/pid/dogo/station/u/ile_du_singe

# On affiche un message de debug
tellraw @a[tag=AnnoncePIDDebugLog] [{"text":"[PID] ","color":"gold"},{"text":"station.","color":"gray"},{"text":"u.ile_du_singe","color":"yellow"}]

# On définit notre temps d'attente à la durée du son à jouer
scoreboard players set @s annonceTicks 19
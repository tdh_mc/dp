# On diffuse le bon son en fonction de notre nombre de minutes

# On calcule le nombre de milliers contenus dans currentMinute
scoreboard players operation #pid temp = @s currentMinute
scoreboard players set #pid temp2 1000
scoreboard players operation #pid temp /= #pid temp2
# On multiplie à nouveau par temp2 pour avoir le nombre à retrancher à currentMinute
scoreboard players operation #pid temp *= #pid temp2
# On retranche le nombre calculé à currentMinute, puis on réajoute 1000
# (pour dire 5050 "cinq mille cinquante" et pas "cinq cinquante")
scoreboard players operation @s currentMinute -= #pid temp
scoreboard players add @s currentMinute 1000

# Enfin, on diffuse le chiffre retranché
execute if score #pid temp matches 2000 run function tch:station/annonce/pid/jouer/info/nombre/u/2
execute if score #pid temp matches 3000 run function tch:station/annonce/pid/jouer/info/nombre/u/3
execute if score #pid temp matches 4000 run function tch:station/annonce/pid/jouer/info/nombre/u/4
execute if score #pid temp matches 5000 run function tch:station/annonce/pid/jouer/info/nombre/u/5
execute if score #pid temp matches 6000 run function tch:station/annonce/pid/jouer/info/nombre/u/6
execute if score #pid temp matches 7000 run function tch:station/annonce/pid/jouer/info/nombre/u/7
execute if score #pid temp matches 8000 run function tch:station/annonce/pid/jouer/info/nombre/u/8
execute if score #pid temp matches 9000 run function tch:station/annonce/pid/jouer/info/nombre/u/9
execute if score #pid temp matches 10000 run function tch:station/annonce/pid/jouer/info/nombre/u/10
#REGION BEOLLONIE + GRENAT [1100-1199]
execute if score #pid idStation matches 1100 run function tch:station/annonce/pid/jouer/info/station/u/grenat_ville
execute if score #pid idStation matches 1105 run function tch:station/annonce/pid/jouer/info/station/u/grenat_recif
execute if score #pid idStation matches 1110 run function tch:station/annonce/pid/jouer/info/station/u/grenat_arithmatie
execute if score #pid idStation matches 1115 run function tch:station/annonce/pid/jouer/info/station/u/asvaard_rudelieu

execute if score #pid idStation matches 1150 run function tch:station/annonce/pid/jouer/info/station/u/villonne
execute if score #pid idStation matches 1155 run function tch:station/annonce/pid/jouer/info/station/u/foret_de_jeej
execute if score #pid idStation matches 1160 run function tch:station/annonce/pid/jouer/info/station/u/beothas
execute if score #pid idStation matches 1170 run function tch:station/annonce/pid/jouer/info/station/u/tolbrok
execute if score #pid idStation matches 1180 run function tch:station/annonce/pid/jouer/info/station/u/duerrom
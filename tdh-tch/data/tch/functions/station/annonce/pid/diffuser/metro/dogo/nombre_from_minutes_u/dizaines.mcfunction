# On diffuse le bon son en fonction de notre nombre de minutes

# On calcule le nombre de dizaines contenues dans currentMinute
scoreboard players operation #pid temp = @s currentMinute
scoreboard players set #pid temp2 10
scoreboard players operation #pid temp /= #pid temp2
# Si ce nombre de dizaines est 7 ou 9, on en retranche une
# (pour dire soixante-seize au lieu de soixantedix-six)
execute if score #pid temp matches 7 run scoreboard players remove #pid temp 1
execute if score #pid temp matches 9 run scoreboard players remove #pid temp 1
# On multiplie à nouveau par temp2 pour avoir le nombre à retrancher à currentMinute
scoreboard players operation #pid temp *= #pid temp2
scoreboard players operation @s currentMinute -= #pid temp

# Enfin, on diffuse le chiffre retranché
execute if score #pid temp matches 40 run function tch:station/annonce/pid/jouer/dogo/nombre/u/40
execute if score #pid temp matches 50 run function tch:station/annonce/pid/jouer/dogo/nombre/u/50
execute if score #pid temp matches 60 run function tch:station/annonce/pid/jouer/dogo/nombre/u/60
execute if score #pid temp matches 80 run function tch:station/annonce/pid/jouer/dogo/nombre/u/80
# On sélectionne automatiquement le son à diffuser à partir de notre ID line

# On tag notre PC terminus
execute unless score @s prochaineLigne matches 1.. run function tch:tag/terminus_pc
execute if score @s prochaineLigne matches 1.. run function tch:tag/terminus_pc_next

# On enregistre l'ID station dudit PC terminus
scoreboard players operation #pid idStation = @e[type=item_frame,tag=ourTerminusPC] idStation

# Selon l'ID de station, on joue le son approprié
execute if score #pid idStation matches 1000..1999 run function tch:station/annonce/pid/diffuser/metro/info/station_from_line_u/1000_1999
execute if score #pid idStation matches 2000..2999 run function tch:station/annonce/pid/diffuser/metro/info/station_from_line_u/2000_2999

# On retire le tag du PC terminus
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
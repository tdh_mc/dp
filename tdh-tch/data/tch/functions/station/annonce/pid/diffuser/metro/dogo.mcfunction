# On diffuse l'annonce de PID Dogo métro (générique)
# Selon l'ID de son, on sait quelle fonction lancer

# - Direction
execute if score @s annonceIndex matches 1 run function tch:station/annonce/pid/jouer/dogo/generique/direction
# - Station de direction
execute if score @s annonceIndex matches 2 run function tch:station/annonce/pid/diffuser/metro/dogo/station_from_line_u
# - Prochain train dans
execute if score @s annonceIndex matches 3 run function tch:station/annonce/pid/jouer/dogo/generique/prochain_train_dans
# - Stockage du nombre de minutes d'attente
# On est OBLIGÉS de le faire ici et pas dans la fonction de génération,
# car sinon le temps d'attente suivant n'aurait pas le temps d'être généré !
execute if score @s annonceIndex matches 4 run function tch:station/annonce/pid/gen_id/get_minutes
# - Temps d'attente en minutes (automatique)
# (retranchera systématiquement 1 à annonceIndex si le nombre n'est pas fini)
execute if score @s annonceIndex matches 5 run function tch:station/annonce/pid/diffuser/metro/dogo/nombre_from_minutes_u
# - Minutes
execute if score @s annonceIndex matches 6 run function tch:station/annonce/pid/jouer/dogo/generique/minutes

# Fin de diffusion
execute if score @s annonceIndex matches 7.. run function tch:station/annonce/pid/fin
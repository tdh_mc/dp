# On sélectionne automatiquement le son à diffuser à partir de notre nombre de minutes

# On lance la bonne sous-fonction selon le nombre de minutes restantes
execute if score @s currentMinute matches 1..31 run function tch:station/annonce/pid/diffuser/metro/info/nombre_from_minutes_u/unites
execute if score @s currentMinute matches 32..99 run function tch:station/annonce/pid/diffuser/metro/info/nombre_from_minutes_u/dizaines
execute if score @s currentMinute matches 100..199 run function tch:station/annonce/pid/diffuser/metro/info/nombre_from_minutes_u/100
execute if score @s currentMinute matches 200..999 run function tch:station/annonce/pid/diffuser/metro/info/nombre_from_minutes_u/centaines
execute if score @s currentMinute matches 1000..1999 run function tch:station/annonce/pid/diffuser/metro/info/nombre_from_minutes_u/1000
execute if score @s currentMinute matches 2000.. run function tch:station/annonce/pid/diffuser/metro/info/nombre_from_minutes_u/milliers

# S'il nous reste des minutes, on retranche 1 à notre ID de son pour lancer à nouveau cette fonction
execute if score @s currentMinute matches 1.. run scoreboard players remove @s annonceIndex 1
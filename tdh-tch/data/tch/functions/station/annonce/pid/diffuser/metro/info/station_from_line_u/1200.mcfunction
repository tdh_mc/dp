#REGION DELIAH-LAAR [1200-1299]
execute if score #pid idStation matches 1200 run function tch:station/annonce/pid/jouer/info/station/u/dorlinor
execute if score #pid idStation matches 1205 run function tch:station/annonce/pid/jouer/info/station/u/desert_laar
execute if score #pid idStation matches 1206 run function tch:station/annonce/pid/jouer/info/station/u/rives_de_laar
execute if score #pid idStation matches 1210 run function tch:station/annonce/pid/jouer/info/station/u/ygriak
execute if score #pid idStation matches 1220 run function tch:station/annonce/pid/jouer/info/station/u/zobun_eclesta
execute if score #pid idStation matches 1230 run function tch:station/annonce/pid/jouer/info/station/u/evenis_eclesta
execute if score #pid idStation matches 1250 run function tch:station/annonce/pid/jouer/info/station/u/athes
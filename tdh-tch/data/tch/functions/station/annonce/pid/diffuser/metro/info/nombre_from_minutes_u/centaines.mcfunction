# On diffuse le bon son en fonction de notre nombre de minutes

# On calcule le nombre de centaines contenues dans currentMinute
scoreboard players operation #pid temp = @s currentMinute
scoreboard players set #pid temp2 100
scoreboard players operation #pid temp /= #pid temp2
# On multiplie à nouveau par temp2 pour avoir le nombre à retrancher à currentMinute
scoreboard players operation #pid temp *= #pid temp2
# On retranche le nombre calculé à currentMinute, puis on réajoute 100
# (pour dire 550 "cinq cent cinquante" et pas "cinq cinquante")
scoreboard players operation @s currentMinute -= #pid temp
scoreboard players add @s currentMinute 100

# Enfin, on diffuse le chiffre retranché
execute if score #pid temp matches 200 run function tch:station/annonce/pid/jouer/info/nombre/u/2
execute if score #pid temp matches 300 run function tch:station/annonce/pid/jouer/info/nombre/u/3
execute if score #pid temp matches 400 run function tch:station/annonce/pid/jouer/info/nombre/u/4
execute if score #pid temp matches 500 run function tch:station/annonce/pid/jouer/info/nombre/u/5
execute if score #pid temp matches 600 run function tch:station/annonce/pid/jouer/info/nombre/u/6
execute if score #pid temp matches 700 run function tch:station/annonce/pid/jouer/info/nombre/u/7
execute if score #pid temp matches 800 run function tch:station/annonce/pid/jouer/info/nombre/u/8
execute if score #pid temp matches 900 run function tch:station/annonce/pid/jouer/info/nombre/u/9
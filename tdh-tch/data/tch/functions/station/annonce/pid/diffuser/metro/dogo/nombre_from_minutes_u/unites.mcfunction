# On diffuse le bon son en fonction de notre nombre de minutes
# Seul 1 est relou et devra être géré dans une sous-fonction séparée (car il peut être "1" ou "et 1" selon cas) (TODO quand le RP le supportera)

execute if score @s currentMinute matches 1 run function tch:station/annonce/pid/jouer/dogo/nombre/u/1
execute if score @s currentMinute matches 2 run function tch:station/annonce/pid/jouer/dogo/nombre/u/2
execute if score @s currentMinute matches 3 run function tch:station/annonce/pid/jouer/dogo/nombre/u/3
execute if score @s currentMinute matches 4 run function tch:station/annonce/pid/jouer/dogo/nombre/u/4
execute if score @s currentMinute matches 5 run function tch:station/annonce/pid/jouer/dogo/nombre/u/5
execute if score @s currentMinute matches 6 run function tch:station/annonce/pid/jouer/dogo/nombre/u/6
execute if score @s currentMinute matches 7 run function tch:station/annonce/pid/jouer/dogo/nombre/u/7
execute if score @s currentMinute matches 8 run function tch:station/annonce/pid/jouer/dogo/nombre/u/8
execute if score @s currentMinute matches 9 run function tch:station/annonce/pid/jouer/dogo/nombre/u/9
execute if score @s currentMinute matches 10 run function tch:station/annonce/pid/jouer/dogo/nombre/u/10
execute if score @s currentMinute matches 11 run function tch:station/annonce/pid/jouer/dogo/nombre/u/11
execute if score @s currentMinute matches 12 run function tch:station/annonce/pid/jouer/dogo/nombre/u/12
execute if score @s currentMinute matches 13 run function tch:station/annonce/pid/jouer/dogo/nombre/u/13
execute if score @s currentMinute matches 14 run function tch:station/annonce/pid/jouer/dogo/nombre/u/14
execute if score @s currentMinute matches 15 run function tch:station/annonce/pid/jouer/dogo/nombre/u/15
execute if score @s currentMinute matches 16 run function tch:station/annonce/pid/jouer/dogo/nombre/u/16
execute if score @s currentMinute matches 17 run function tch:station/annonce/pid/jouer/dogo/nombre/u/17
execute if score @s currentMinute matches 18 run function tch:station/annonce/pid/jouer/dogo/nombre/u/18
execute if score @s currentMinute matches 19 run function tch:station/annonce/pid/jouer/dogo/nombre/u/19
execute if score @s currentMinute matches 20 run function tch:station/annonce/pid/jouer/dogo/nombre/u/20
execute if score @s currentMinute matches 21 run function tch:station/annonce/pid/jouer/dogo/nombre/u/21
execute if score @s currentMinute matches 22 run function tch:station/annonce/pid/jouer/dogo/nombre/u/22
execute if score @s currentMinute matches 23 run function tch:station/annonce/pid/jouer/dogo/nombre/u/23
execute if score @s currentMinute matches 24 run function tch:station/annonce/pid/jouer/dogo/nombre/u/24
execute if score @s currentMinute matches 25 run function tch:station/annonce/pid/jouer/dogo/nombre/u/25
execute if score @s currentMinute matches 26 run function tch:station/annonce/pid/jouer/dogo/nombre/u/26
execute if score @s currentMinute matches 27 run function tch:station/annonce/pid/jouer/dogo/nombre/u/27
execute if score @s currentMinute matches 28 run function tch:station/annonce/pid/jouer/dogo/nombre/u/28
execute if score @s currentMinute matches 29 run function tch:station/annonce/pid/jouer/dogo/nombre/u/29
execute if score @s currentMinute matches 30 run function tch:station/annonce/pid/jouer/dogo/nombre/u/30
execute if score @s currentMinute matches 31 run function tch:station/annonce/pid/jouer/dogo/nombre/u/31

# Dans tous les cas, on définit notre variable à 0 pour ne pas continuer d'itérer la fonction nombre_from_minutes_u
scoreboard players set @s currentMinute 0
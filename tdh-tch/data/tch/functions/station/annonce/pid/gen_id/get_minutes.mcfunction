# On enregistre le nombre de minutes d'attente dans currentMinute
scoreboard players operation @s currentMinute = @s spawnAt
scoreboard players operation @s currentMinute -= #temps currentTdhTick
scoreboard players operation @s currentMinute %= #temps fullDayTicks
# On peut se convaincre assez facilement que min = (t * (min/h)) / (t/h)
scoreboard players operation @s currentMinute *= #temps minutesPerHour
scoreboard players operation @s currentMinute /= #temps ticksPerHour
# À l'issue de ce calcul currentMinute contient le nombre de minutes d'attente
# On le stocke également dans currentHour car la fonction nombre_from_minutes_u modifie la valeur de currentMinute au fur et à mesure de son exécution répétée
scoreboard players operation @s currentHour = @s currentMinute
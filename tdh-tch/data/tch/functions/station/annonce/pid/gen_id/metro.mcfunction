# On fait le modulo 2 de notre ID de ligne pour sélectionner la version de l'annonce que l'on va diffuser
# (impair = Dogo[1], pair = info[0])
scoreboard players operation #tchAnnonce temp = @s idLine
scoreboard players set #tchAnnonce temp2 2
scoreboard players operation #tchAnnonce temp %= #tchAnnonce temp2

# On choisit l'ID d'annonce correspondant :
# - PID générique Dogo (4001)
execute if score #tchAnnonce temp matches 1.. run function tch:station/annonce/pid/gen_id/metro/dogo
# - PID générique info (4002)
execute unless score #tchAnnonce temp matches 1.. run function tch:station/annonce/pid/gen_id/metro/info
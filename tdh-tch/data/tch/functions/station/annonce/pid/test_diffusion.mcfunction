# On exécute cette fonction en tant qu'un spawner de n'importe quel mode de transport,
# qui est en cours de diffusion d'un message PID (tag tchAnnoncePIDSpawner)
# et souhaite décrémenter son compteur et éventuellement diffuser le son suivant

# On décrémente notre temps d'attente
scoreboard players remove @s annonceTicks 1

# Si le temps d'attente est arrivé à 0, on joue le son suivant
execute if score @s annonceTicks matches ..0 at @s run function tch:station/annonce/pid/diffuser
# On exécute régulièrement cette fonction à l'aide d'un schedule,
# pour jouer le prochain son d'annonce générique

# Les annonces compose d'incident et les PID ont un tick séparé et ne sont pas affectés

# On incrémente notre ID de son diffusé
# On teste si l'ID d'annonce est défini au préalable, pour éviter d'incrémenter ce score
# lorsque la fonction a été schedule depuis un appel externe d'une fonction de tch:sound
# (probablement par un joueur)
execute if score #tchAnnonce id matches 1.. run scoreboard players add #tchAnnonce status 1

# On exécute une sous-fonction selon notre "famille" d'annonce
# (pour éviter d'avoir une méga-condition de 80 lignes ci-dessous)
execute if score #tchAnnonce id matches 100..139 run function tch:station/annonce/generique/diffuser/interdit_fumer
execute if score #tchAnnonce id matches 140..179 run function tch:station/annonce/generique/diffuser/sacs_bagages

execute if score #tchAnnonce id matches 200..219 run function tch:station/annonce/generique/diffuser/pickpockets
execute if score #tchAnnonce id matches 220..239 run function tch:station/annonce/generique/diffuser/sauvette

execute if score #tchAnnonce id matches 300..319 run function tch:station/annonce/generique/diffuser/achat_retour

execute if score #tchAnnonce id matches 400..419 run function tch:station/annonce/generique/diffuser/confort_descente
execute if score #tchAnnonce id matches 420..439 run function tch:station/annonce/generique/diffuser/signal_sonore

execute if score #tchAnnonce id matches 500..599 run function tch:station/annonce/generique/diffuser/fin_service
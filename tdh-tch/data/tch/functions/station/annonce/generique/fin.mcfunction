# On termine la diffusion de l'annonce actuelle
# On réinitialise nos scores
scoreboard players set #tchAnnonce id 0

# On retire les tags de toutes les entités station et haut-parleur taggées
tag @e[type=#tch:marqueur/station,tag=tchAnnonceStation] remove tchAnnonceStation
tag @e[type=item_frame,tag=tchAnnonceDiffuseur] remove tchAnnonceDiffuseur
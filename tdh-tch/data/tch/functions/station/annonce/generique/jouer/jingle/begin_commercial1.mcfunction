# On joue le son à la position de tous les diffuseurs taggés
execute as @e[type=item_frame,tag=tchAnnonceDiffuseur] at @s run function tch:sound/annonce/generique/jingle/begin_commercial1

# On affiche un message de debug
tellraw @a[tag=AnnonceDebugLog] [{"text":"[Annonce]","color":"gold"},{"text":" Jingle générique ","color":"gray"},{"text":"begin_commercial1","color":"yellow"}]

# On schedule la fonction de diffusion après la durée du son à jouer
schedule function tch:station/annonce/generique/diffuser 58t
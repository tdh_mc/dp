# On génère un ID d'annonce à diffuser

# On commence par compter le nombre de stations chargées
execute store result score #tchAnnonce temp if entity @e[type=#tch:marqueur/station,tag=NomStation]

# Si aucune station n'est chargée, on ne génère tout simplement pas d'annonce
# Donc on lance la fonction qui déterminera si on a une annonce station ouverte ou fermée seulement si on a détecté au moins une station (dans temp)
execute if score #tchAnnonce temp matches 1.. run function tch:station/annonce/generique/gen_id/test_stations_ouvertes
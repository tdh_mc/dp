# En fonction de l'ID d'annonce, on exécute la sous-fonction correspondante

# Fin de service 1-4
execute if score #tchAnnonce id matches 500 run function tch:station/annonce/generique/diffuser/fin_service/1
execute if score #tchAnnonce id matches 501 run function tch:station/annonce/generique/diffuser/fin_service/2
execute if score #tchAnnonce id matches 502 run function tch:station/annonce/generique/diffuser/fin_service/3
execute if score #tchAnnonce id matches 503 run function tch:station/annonce/generique/diffuser/fin_service/4

# Fin de service RER
execute if score #tchAnnonce id matches 510 run function tch:station/annonce/generique/diffuser/fin_service/rer/fr
execute if score #tchAnnonce id matches 511 run function tch:station/annonce/generique/diffuser/fin_service/rer/fr_en

# Fin de service station ferme
execute if score #tchAnnonce id matches 520 run function tch:station/annonce/generique/diffuser/fin_service/station_ferme/fr
execute if score #tchAnnonce id matches 521 run function tch:station/annonce/generique/diffuser/fin_service/station_ferme/fr_en
execute if score #tchAnnonce id matches 522 run function tch:station/annonce/generique/diffuser/fin_service/station_ferme/fr_de
execute if score #tchAnnonce id matches 523 run function tch:station/annonce/generique/diffuser/fin_service/station_ferme/fr_es
execute if score #tchAnnonce id matches 524 run function tch:station/annonce/generique/diffuser/fin_service/station_ferme/fr_en_de
execute if score #tchAnnonce id matches 525 run function tch:station/annonce/generique/diffuser/fin_service/station_ferme/fr_en_es
# En fonction de l'ID d'annonce, on exécute la sous-fonction correspondante

# Sacs et bagages générique 1
execute if score #tchAnnonce id matches 140 run function tch:station/annonce/generique/diffuser/sacs_bagages/1/fr
execute if score #tchAnnonce id matches 141 run function tch:station/annonce/generique/diffuser/sacs_bagages/1/fr_en
execute if score #tchAnnonce id matches 142 run function tch:station/annonce/generique/diffuser/sacs_bagages/1/fr_de
execute if score #tchAnnonce id matches 143 run function tch:station/annonce/generique/diffuser/sacs_bagages/1/fr_es

execute if score #tchAnnonce id matches 145 run function tch:station/annonce/generique/diffuser/sacs_bagages/1/fr_en_de
execute if score #tchAnnonce id matches 146 run function tch:station/annonce/generique/diffuser/sacs_bagages/1/fr_en_es
execute if score #tchAnnonce id matches 147 run function tch:station/annonce/generique/diffuser/sacs_bagages/1/fr_es_it
execute if score #tchAnnonce id matches 148 run function tch:station/annonce/generique/diffuser/sacs_bagages/1/fr_en_it_zh

# Sacs et bagages ferré
execute if score #tchAnnonce id matches 160 run function tch:station/annonce/generique/diffuser/sacs_bagages/ferre/fr
execute if score #tchAnnonce id matches 161 run function tch:station/annonce/generique/diffuser/sacs_bagages/ferre/fr_en
execute if score #tchAnnonce id matches 162 run function tch:station/annonce/generique/diffuser/sacs_bagages/ferre/fr_de
execute if score #tchAnnonce id matches 163 run function tch:station/annonce/generique/diffuser/sacs_bagages/ferre/fr_es
execute if score #tchAnnonce id matches 164 run function tch:station/annonce/generique/diffuser/sacs_bagages/ferre/fr_it
execute if score #tchAnnonce id matches 165 run function tch:station/annonce/generique/diffuser/sacs_bagages/ferre/fr_en_de
execute if score #tchAnnonce id matches 166 run function tch:station/annonce/generique/diffuser/sacs_bagages/ferre/fr_en_es
execute if score #tchAnnonce id matches 167 run function tch:station/annonce/generique/diffuser/sacs_bagages/ferre/fr_en_es_it
# Selon l'ID de son, on sait quelle fonction lancer

# - Begin 1
execute if score #tchAnnonce status matches 1 run function tch:station/annonce/generique/jouer/jingle/begin1
# - Annonce FR
execute if score #tchAnnonce status matches 2 run function tch:station/annonce/generique/jouer/signal_sonore/fr
# - Annonce EN
execute if score #tchAnnonce status matches 3 run function tch:station/annonce/generique/jouer/signal_sonore/en
# - Annonce DE
execute if score #tchAnnonce status matches 4 run function tch:station/annonce/generique/jouer/signal_sonore/de
# - Annonce IT
execute if score #tchAnnonce status matches 5 run function tch:station/annonce/generique/jouer/signal_sonore/it

# Fin de diffusion
execute if score #tchAnnonce status matches 6.. run function tch:station/annonce/generique/fin
# En fonction de l'ID d'annonce, on exécute la sous-fonction correspondante

# Confort descente 1
execute if score #tchAnnonce id matches 400 run function tch:station/annonce/generique/diffuser/confort_descente/fr
execute if score #tchAnnonce id matches 401 run function tch:station/annonce/generique/diffuser/confort_descente/fr_en
execute if score #tchAnnonce id matches 402 run function tch:station/annonce/generique/diffuser/confort_descente/fr_de
execute if score #tchAnnonce id matches 403 run function tch:station/annonce/generique/diffuser/confort_descente/fr_it
execute if score #tchAnnonce id matches 404 run function tch:station/annonce/generique/diffuser/confort_descente/fr_en_de
execute if score #tchAnnonce id matches 405 run function tch:station/annonce/generique/diffuser/confort_descente/fr_en_it

# Confort descente court
execute if score #tchAnnonce id matches 410 run function tch:station/annonce/generique/diffuser/confort_descente/court
# Selon l'ID de son, on sait quelle fonction lancer

# - Begin 2
execute if score #tchAnnonce status matches 1 run function tch:station/annonce/generique/jouer/jingle/begin2
# - Annonce FR
execute if score #tchAnnonce status matches 2 run function tch:station/annonce/generique/jouer/sauvette/fr
# - Annonce EN
execute if score #tchAnnonce status matches 3 run function tch:station/annonce/generique/jouer/sauvette/en
# - Annonce IT
execute if score #tchAnnonce status matches 4 run function tch:station/annonce/generique/jouer/sauvette/it
# - Annonce ZH
execute if score #tchAnnonce status matches 5 run function tch:station/annonce/generique/jouer/sauvette/zh

# Fin de diffusion
execute if score #tchAnnonce status matches 6.. run function tch:station/annonce/generique/fin
# Selon l'ID de son, on sait quelle fonction lancer

# - Begin 2
execute if score #tchAnnonce status matches 1 run function tch:station/annonce/generique/jouer/jingle/begin2
# - Annonce FR
execute if score #tchAnnonce status matches 2 run function tch:station/annonce/generique/jouer/sacs_bagages/1/fr
# - Annonce ES
execute if score #tchAnnonce status matches 3 run function tch:station/annonce/generique/jouer/sacs_bagages/1/es
# - Annonce IT
execute if score #tchAnnonce status matches 4 run function tch:station/annonce/generique/jouer/sacs_bagages/1/it

# Fin de diffusion
execute if score #tchAnnonce status matches 5.. run function tch:station/annonce/generique/fin
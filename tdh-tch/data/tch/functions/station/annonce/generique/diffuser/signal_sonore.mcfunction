# En fonction de l'ID d'annonce, on exécute la sous-fonction correspondante

# Signal sonore 1
execute if score #tchAnnonce id matches 420 run function tch:station/annonce/generique/diffuser/signal_sonore/fr
execute if score #tchAnnonce id matches 421 run function tch:station/annonce/generique/diffuser/signal_sonore/fr_en
execute if score #tchAnnonce id matches 422 run function tch:station/annonce/generique/diffuser/signal_sonore/fr_de
execute if score #tchAnnonce id matches 423 run function tch:station/annonce/generique/diffuser/signal_sonore/fr_it
execute if score #tchAnnonce id matches 424 run function tch:station/annonce/generique/diffuser/signal_sonore/fr_en_de
execute if score #tchAnnonce id matches 425 run function tch:station/annonce/generique/diffuser/signal_sonore/fr_en_it
execute if score #tchAnnonce id matches 426 run function tch:station/annonce/generique/diffuser/signal_sonore/fr_en_de_it
# En fonction de l'ID d'annonce, on exécute la sous-fonction correspondante

# Interdit de fumer 1
execute if score #tchAnnonce id matches 100 run function tch:station/annonce/generique/diffuser/interdit_fumer/1/fr
execute if score #tchAnnonce id matches 101 run function tch:station/annonce/generique/diffuser/interdit_fumer/1/fr_en
execute if score #tchAnnonce id matches 102 run function tch:station/annonce/generique/diffuser/interdit_fumer/1/fr_de
execute if score #tchAnnonce id matches 103 run function tch:station/annonce/generique/diffuser/interdit_fumer/1/fr_es
execute if score #tchAnnonce id matches 104 run function tch:station/annonce/generique/diffuser/interdit_fumer/1/fr_it
execute if score #tchAnnonce id matches 105 run function tch:station/annonce/generique/diffuser/interdit_fumer/1/fr_en_de
execute if score #tchAnnonce id matches 106 run function tch:station/annonce/generique/diffuser/interdit_fumer/1/fr_en_es
execute if score #tchAnnonce id matches 107 run function tch:station/annonce/generique/diffuser/interdit_fumer/1/fr_en_it

# Interdit de fumer RER
execute if score #tchAnnonce id matches 120 run function tch:station/annonce/generique/diffuser/interdit_fumer/rer/fr
execute if score #tchAnnonce id matches 121 run function tch:station/annonce/generique/diffuser/interdit_fumer/rer/fr_en
# Selon l'ID de son, on sait quelle fonction lancer

# - Begin fin service
execute if score #tchAnnonce status matches 1 run function tch:station/annonce/generique/jouer/jingle/begin_fin_service
# - Annonce FR
execute if score #tchAnnonce status matches 2 run function tch:station/annonce/generique/jouer/fin_service/4

# Fin de diffusion
execute if score #tchAnnonce status matches 3.. run function tch:station/annonce/generique/fin
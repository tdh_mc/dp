# Selon l'ID de son, on sait quelle fonction lancer

# - Begin commercial 1
execute if score #tchAnnonce status matches 1 run function tch:station/annonce/generique/jouer/jingle/begin_commercial1
# - Annonce FR
execute if score #tchAnnonce status matches 2 run function tch:station/annonce/generique/jouer/sacs_bagages/ferre/fr
# - Annonce EN
execute if score #tchAnnonce status matches 3 run function tch:station/annonce/generique/jouer/sacs_bagages/ferre/en
# - Annonce DE
execute if score #tchAnnonce status matches 4 run function tch:station/annonce/generique/jouer/sacs_bagages/ferre/de

# Fin de diffusion
execute if score #tchAnnonce status matches 5.. run function tch:station/annonce/generique/fin
# En fonction de l'ID d'annonce, on exécute la sous-fonction correspondante

# Pickpockets 1
execute if score #tchAnnonce id matches 200 run function tch:station/annonce/generique/diffuser/pickpockets/fr
execute if score #tchAnnonce id matches 201 run function tch:station/annonce/generique/diffuser/pickpockets/fr_en
execute if score #tchAnnonce id matches 202 run function tch:station/annonce/generique/diffuser/pickpockets/fr_de
execute if score #tchAnnonce id matches 203 run function tch:station/annonce/generique/diffuser/pickpockets/fr_es
execute if score #tchAnnonce id matches 204 run function tch:station/annonce/generique/diffuser/pickpockets/fr_it
execute if score #tchAnnonce id matches 205 run function tch:station/annonce/generique/diffuser/pickpockets/fr_en_es_de
execute if score #tchAnnonce id matches 206 run function tch:station/annonce/generique/diffuser/pickpockets/fr_en_es_it
execute if score #tchAnnonce id matches 207 run function tch:station/annonce/generique/diffuser/pickpockets/fr_en_ja_zh
execute if score #tchAnnonce id matches 208 run function tch:station/annonce/generique/diffuser/pickpockets/full
# En fonction de l'ID d'annonce, on exécute la sous-fonction correspondante

# Achat retour 1
execute if score #tchAnnonce id matches 300 run function tch:station/annonce/generique/diffuser/achat_retour/fr
execute if score #tchAnnonce id matches 301 run function tch:station/annonce/generique/diffuser/achat_retour/fr_en
execute if score #tchAnnonce id matches 302 run function tch:station/annonce/generique/diffuser/achat_retour/fr_de
execute if score #tchAnnonce id matches 303 run function tch:station/annonce/generique/diffuser/achat_retour/fr_es
execute if score #tchAnnonce id matches 304 run function tch:station/annonce/generique/diffuser/achat_retour/fr_en_de
execute if score #tchAnnonce id matches 305 run function tch:station/annonce/generique/diffuser/achat_retour/fr_en_es
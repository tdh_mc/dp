# Selon l'ID de son, on sait quelle fonction lancer

# - Begin 1
execute if score #tchAnnonce status matches 1 run function tch:station/annonce/generique/jouer/jingle/begin1
# - Annonce FR
execute if score #tchAnnonce status matches 2 run function tch:station/annonce/generique/jouer/confort_descente/fr

# Fin de diffusion
execute if score #tchAnnonce status matches 3.. run function tch:station/annonce/generique/fin
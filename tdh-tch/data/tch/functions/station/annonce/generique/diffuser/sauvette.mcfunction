# En fonction de l'ID d'annonce, on exécute la sous-fonction correspondante

# Sauvette 1
execute if score #tchAnnonce id matches 220 run function tch:station/annonce/generique/diffuser/sauvette/fr
execute if score #tchAnnonce id matches 221 run function tch:station/annonce/generique/diffuser/sauvette/fr_en
execute if score #tchAnnonce id matches 222 run function tch:station/annonce/generique/diffuser/sauvette/fr_de
execute if score #tchAnnonce id matches 223 run function tch:station/annonce/generique/diffuser/sauvette/fr_it
execute if score #tchAnnonce id matches 224 run function tch:station/annonce/generique/diffuser/sauvette/fr_en_it
execute if score #tchAnnonce id matches 225 run function tch:station/annonce/generique/diffuser/sauvette/fr_en_zh
execute if score #tchAnnonce id matches 226 run function tch:station/annonce/generique/diffuser/sauvette/fr_en_it_zh
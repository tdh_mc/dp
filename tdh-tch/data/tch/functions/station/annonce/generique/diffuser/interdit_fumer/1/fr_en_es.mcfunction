# Selon l'ID de son, on sait quelle fonction lancer

# - Begin générique 1
execute if score #tchAnnonce status matches 1 run function tch:station/annonce/generique/jouer/jingle/begin1
# - Annonce FR
execute if score #tchAnnonce status matches 2 run function tch:station/annonce/generique/jouer/interdit_fumer/1/fr
# - Annonce EN
execute if score #tchAnnonce status matches 3 run function tch:station/annonce/generique/jouer/interdit_fumer/1/en
# - Annonce ES
execute if score #tchAnnonce status matches 4 run function tch:station/annonce/generique/jouer/interdit_fumer/1/es

# Fin de diffusion
execute if score #tchAnnonce status matches 5.. run function tch:station/annonce/generique/fin
# On exécute cette fonction en tant qu'une station que l'on souhaite tagger pour lui permettre de diffuser
# la prochaine annonce que l'on générera
tag @s add tchAnnonceStation

tellraw @a[tag=AnnonceDebugLog] [{"text":"[AnnonceTag]","color":"gray"},{"text":" idLine "},{"score":{"name":"#tchAnnonce","objective":"idLine"}},{"text":"-"},{"score":{"name":"#tchAnnonce","objective":"idLineMax"}}]

# On a enregistré des scores idLine et idLineMax ; selon leur valeur, on tag soit tous les haut-parleurs de notre station, soit seulement les haut-parleurs qui matchent leur valeur
scoreboard players operation #tchAnnonce idStation = @s idStation
execute if score #tchAnnonce idLine matches ..0 as @e[type=item_frame,tag=tchHP] if score @s idStation = #tchAnnonce idStation run tag @s add tchAnnonceDiffuseur
execute if score #tchAnnonce idLine matches 1.. run function tch:station/annonce/generique/gen_id/tag_station/test_id_line
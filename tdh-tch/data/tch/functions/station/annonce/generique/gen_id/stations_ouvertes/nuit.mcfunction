# Génération d'un ID d'annonce station ouverte, si on est la nuit
# On a déjà pris un nombre aléatoire 0-99 dans #random temp

# On génère un ID d'annonce au pif parmi les disponibles :
# - Interdit de fumer générique (100-104)
execute if score #random temp matches ..5 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr
execute if score #random temp matches 6..11 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en
execute if score #random temp matches 12..15 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_de
execute if score #random temp matches 16..19 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_es
execute if score #random temp matches 20..24 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_it
# - Interdit de fumer RER (120-121)
execute if score #random temp matches 25..29 run function tch:station/annonce/generique/gen_id/interdit_fumer/rer/fr
execute if score #random temp matches 30..39 run function tch:station/annonce/generique/gen_id/interdit_fumer/rer/fr_en
# - Sacs/bagages générique (140-143/148)
execute if score #random temp matches 40..42 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr
execute if score #random temp matches 43..46 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_en
execute if score #random temp matches 47 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_de
execute if score #random temp matches 48 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_es
execute if score #random temp matches 49 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_en_it_zh
# - Sacs/bagages ferré (160-164)
execute if score #random temp matches 50..52 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr
execute if score #random temp matches 53..55 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en
execute if score #random temp matches 56..57 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_de
execute if score #random temp matches 58 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_es
execute if score #random temp matches 59 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_it
# - Pickpockets (200-208)
execute if score #random temp matches 60..64 run function tch:station/annonce/generique/gen_id/pickpockets/fr
execute if score #random temp matches 65..72 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en
execute if score #random temp matches 73..75 run function tch:station/annonce/generique/gen_id/pickpockets/fr_de
execute if score #random temp matches 76..79 run function tch:station/annonce/generique/gen_id/pickpockets/fr_es
execute if score #random temp matches 80..83 run function tch:station/annonce/generique/gen_id/pickpockets/fr_it
execute if score #random temp matches 84..89 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en_ja_zh
# - Sauvette (220-226)
execute if score #random temp matches 90..91 run function tch:station/annonce/generique/gen_id/sauvette/fr
execute if score #random temp matches 92..94 run function tch:station/annonce/generique/gen_id/sauvette/fr_en
execute if score #random temp matches 95 run function tch:station/annonce/generique/gen_id/sauvette/fr_de
execute if score #random temp matches 96 run function tch:station/annonce/generique/gen_id/sauvette/fr_it
execute if score #random temp matches 97.. run function tch:station/annonce/generique/gen_id/sauvette/fr_en_zh
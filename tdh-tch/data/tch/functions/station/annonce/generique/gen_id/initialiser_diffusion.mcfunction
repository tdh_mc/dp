# On initialise la diffusion de l'annonce qu'on vient de générer

# On se place au statut 0 pour diffuser le premier son
# (le premier son est diffusé à 1, mais on fait tjrs l'incrément au DÉBUT de la fonction diffuser)
scoreboard players set #tchAnnonce status 0

# On schedule la diffusion du premier son
schedule function tch:station/annonce/generique/diffuser 1t
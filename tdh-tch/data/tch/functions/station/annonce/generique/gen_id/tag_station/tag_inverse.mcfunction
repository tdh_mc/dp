# On exécute cette fonction en tant qu'une item frame haut-parleur

# On se tag si notre ID de ligne est supérieur au minimum OU inférieur au maximum,
# car on est dans le cas où le max est inférieur au min
execute if score @s idLine >= #tchAnnonce idLine run tag @s add tchAnnonceDiffuseur
execute if score @s idLine <= #tchAnnonce idLineMax run tag @s add tchAnnonceDiffuseur
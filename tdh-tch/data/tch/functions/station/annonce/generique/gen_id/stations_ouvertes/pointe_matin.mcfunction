# Génération d'un ID d'annonce station ouverte, si on est le matin entre 6 et 9h
# On a déjà pris un nombre aléatoire 0-99 dans #random temp

# On génère un ID d'annonce au pif parmi les disponibles :
# - Interdit de fumer générique (100-107)
execute if score #random temp matches ..1 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr
execute if score #random temp matches 2..3 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en
execute if score #random temp matches 4 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_de
execute if score #random temp matches 5 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_es
execute if score #random temp matches 6 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_it
execute if score #random temp matches 7 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en_de
execute if score #random temp matches 8..9 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en_es
execute if score #random temp matches 10 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en_it
# - Interdit de fumer RER (120-121)
execute if score #random temp matches 11..12 run function tch:station/annonce/generique/gen_id/interdit_fumer/rer/fr
execute if score #random temp matches 13..14 run function tch:station/annonce/generique/gen_id/interdit_fumer/rer/fr_en
# - Sacs/bagages générique (140-142/147-148)
execute if score #random temp matches 15 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr
execute if score #random temp matches 16 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_en
execute if score #random temp matches 17 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_de
execute if score #random temp matches 18 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_es_it
execute if score #random temp matches 19 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_en_it_zh
# - Sacs/bagages ferré (160-167)
execute if score #random temp matches 20 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr
execute if score #random temp matches 21..22 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en
execute if score #random temp matches 23 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_de
execute if score #random temp matches 24 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_es
execute if score #random temp matches 25 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_it
execute if score #random temp matches 26 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en_de
execute if score #random temp matches 27..28 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en_es
execute if score #random temp matches 29 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en_es_it
# - Pickpockets (200-208)
execute if score #random temp matches 30 run function tch:station/annonce/generique/gen_id/pickpockets/fr
execute if score #random temp matches 31..32 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en
execute if score #random temp matches 33 run function tch:station/annonce/generique/gen_id/pickpockets/fr_de
execute if score #random temp matches 34 run function tch:station/annonce/generique/gen_id/pickpockets/fr_es
execute if score #random temp matches 35 run function tch:station/annonce/generique/gen_id/pickpockets/fr_it
execute if score #random temp matches 36 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en_es_de
execute if score #random temp matches 37 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en_es_it
execute if score #random temp matches 38 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en_ja_zh
execute if score #random temp matches 39 run function tch:station/annonce/generique/gen_id/pickpockets/full
# - Sauvette (220-226)
execute if score #random temp matches 40 run function tch:station/annonce/generique/gen_id/sauvette/fr
execute if score #random temp matches 41 run function tch:station/annonce/generique/gen_id/sauvette/fr_en
execute if score #random temp matches 42 run function tch:station/annonce/generique/gen_id/sauvette/fr_de
execute if score #random temp matches 43 run function tch:station/annonce/generique/gen_id/sauvette/fr_it
execute if score #random temp matches 44..45 run function tch:station/annonce/generique/gen_id/sauvette/fr_en_it
execute if score #random temp matches 46..47 run function tch:station/annonce/generique/gen_id/sauvette/fr_en_zh
execute if score #random temp matches 48..49 run function tch:station/annonce/generique/gen_id/sauvette/fr_en_it_zh
# - Achat retour (300-305)
execute if score #random temp matches 50..54 run function tch:station/annonce/generique/gen_id/achat_retour/fr
execute if score #random temp matches 55..59 run function tch:station/annonce/generique/gen_id/achat_retour/fr_en
execute if score #random temp matches 60..61 run function tch:station/annonce/generique/gen_id/achat_retour/fr_de
execute if score #random temp matches 62..63 run function tch:station/annonce/generique/gen_id/achat_retour/fr_es
execute if score #random temp matches 64..66 run function tch:station/annonce/generique/gen_id/achat_retour/fr_en_de
execute if score #random temp matches 67..69 run function tch:station/annonce/generique/gen_id/achat_retour/fr_en_es
# - Confort descente 1 (400-405)
execute if score #random temp matches 70 run function tch:station/annonce/generique/gen_id/confort_descente/fr
execute if score #random temp matches 71..73 run function tch:station/annonce/generique/gen_id/confort_descente/fr_en
execute if score #random temp matches 74 run function tch:station/annonce/generique/gen_id/confort_descente/fr_de
execute if score #random temp matches 75 run function tch:station/annonce/generique/gen_id/confort_descente/fr_it
execute if score #random temp matches 76..77 run function tch:station/annonce/generique/gen_id/confort_descente/fr_en_de
execute if score #random temp matches 78..79 run function tch:station/annonce/generique/gen_id/confort_descente/fr_en_it
# - Confort descente short (410)
execute if score #random temp matches 80..89 run function tch:station/annonce/generique/gen_id/confort_descente/court
# - Signal sonore (420-426)
execute if score #random temp matches 90 run function tch:station/annonce/generique/gen_id/signal_sonore/fr
execute if score #random temp matches 91..92 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en
execute if score #random temp matches 93 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_de
execute if score #random temp matches 94 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_it
execute if score #random temp matches 95..96 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en_de
execute if score #random temp matches 97..98 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en_it
execute if score #random temp matches 99.. run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en_de_it
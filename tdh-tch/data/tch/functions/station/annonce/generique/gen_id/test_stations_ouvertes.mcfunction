# On retire le tag temporaire de toutes les stations actuellement chargées,
# et de tous les haut-parleurs actuellement chargés, pour ne pas risquer de diffuser sur de "mauvais" quais
tag @e[type=#tch:marqueur/station,tag=NomStation] remove tchAnnonceStation
tag @e[type=item_frame,tag=tchHP] remove tchAnnonceDiffuseur

# On a déjà le nombre de stations fermées dans #tchAnnonce temp
# On compte le nombre de stations ouvertes dans temp2
execute store result score #tchAnnonce temp2 if entity @e[type=#tch:marqueur/station,tag=NomStation,tag=StationOuverte]
# La fraction (ouvertes / totales) détermine le pourcentage de chances de générer une annonce station ouverte (et "1 - cette fraction" représente le pourcentage de chances de générer une annonce station fermée)

# On génère un nombre aléatoire entre 0 et le nombre de stations ouvertes
scoreboard players set #random min 0
scoreboard players operation #random max = #tchAnnonce temp
function tdh:random
# On stocke ce nombre dans #tchAnnonce temp car on n'a plus besoin du nombre total de stations
# On aura également besoin de faire d'autres appels à tdh:random dans les sous-fonctions donc autant stocker la valeur ailleurs
scoreboard players operation #tchAnnonce temp = #random temp

# Si ce nombre est inférieur au nombre de stations ouvertes, on diffuse une annonce station ouverte
execute if score #tchAnnonce temp < #tchAnnonce temp2 run function tch:station/annonce/generique/gen_id/stations_ouvertes
# Sinon, on diffuse une annonce station fermée
execute if score #tchAnnonce temp >= #tchAnnonce temp2 run function tch:station/annonce/generique/gen_id/stations_fermees

# Si on a réussi à générer une annonce, on enregistre le fait qu'on doit diffuser
execute if score #tchAnnonce id matches 1.. run function tch:station/annonce/generique/gen_id/initialiser_diffusion
# Sinon, on dé-tag tout de suite les diffuseurs et les stations
execute unless score #tchAnnonce id matches 1.. run function tch:station/annonce/generique/fin
# On prend un nombre aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On génère un ID d'annonce au pif parmi les disponibles :
# (tous les ID fin de service sont de la forme 5xx)
# - Fin de service 1/2/3/4 (501-504)
execute if score #random temp matches ..9 run function tch:station/annonce/generique/gen_id/fin_service/1
execute if score #random temp matches 10..19 run function tch:station/annonce/generique/gen_id/fin_service/2
execute if score #random temp matches 20..29 run function tch:station/annonce/generique/gen_id/fin_service/3
execute if score #random temp matches 30..39 run function tch:station/annonce/generique/gen_id/fin_service/4
# - Fin de service RER FR (510) / FR+EN (511)
execute if score #random temp matches 40..54 run function tch:station/annonce/generique/gen_id/fin_service/rer/fr
execute if score #random temp matches 55..69 run function tch:station/annonce/generique/gen_id/fin_service/rer/fr_en
# - Fin de service station ferme FR (520) FR+EN (521) FR+DE (522) FR+ES (523) FR+EN+DE(524) FR+EN+ES(525)
execute if score #random temp matches 70..74 run function tch:station/annonce/generique/gen_id/fin_service/station_ferme/fr
execute if score #random temp matches 75..82 run function tch:station/annonce/generique/gen_id/fin_service/station_ferme/fr_en
execute if score #random temp matches 83..85 run function tch:station/annonce/generique/gen_id/fin_service/station_ferme/fr_de
execute if score #random temp matches 86..89 run function tch:station/annonce/generique/gen_id/fin_service/station_ferme/fr_es
execute if score #random temp matches 90..94 run function tch:station/annonce/generique/gen_id/fin_service/station_ferme/fr_en_de
execute if score #random temp matches 95.. run function tch:station/annonce/generique/gen_id/fin_service/station_ferme/fr_en_es

# On donne un tag à toutes les stations actuellement fermées
# Une sous-fonction à l'intérieur de celle-ci tag également tous les haut-parleurs qui correspondent aux idLine enregistrés plus haut
execute as @e[type=#tch:marqueur/station,tag=NomStation,tag=!StationOuverte] run function tch:station/annonce/generique/gen_id/tag_station
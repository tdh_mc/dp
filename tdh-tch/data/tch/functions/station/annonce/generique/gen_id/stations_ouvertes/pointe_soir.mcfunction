# Génération d'un ID d'annonce station ouverte, si on est le soir entre 5 et 9h
# On a déjà pris un nombre aléatoire 0-99 dans #random temp

# On génère un ID d'annonce au pif parmi les disponibles :
# - Interdit de fumer générique (100-107)
execute if score #random temp matches ..0 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr
execute if score #random temp matches 1 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en_de
execute if score #random temp matches 2..3 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en_es
execute if score #random temp matches 4..5 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en_it
# - Interdit de fumer RER (120-121)
execute if score #random temp matches 6 run function tch:station/annonce/generique/gen_id/interdit_fumer/rer/fr
execute if score #random temp matches 7..9 run function tch:station/annonce/generique/gen_id/interdit_fumer/rer/fr_en
# - Sacs/bagages générique (140/145-147)
execute if score #random temp matches 10 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr
execute if score #random temp matches 11 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_en_de
execute if score #random temp matches 12..13 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_en_es
execute if score #random temp matches 14..15 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_en_it_zh
# - Sacs/bagages ferré (160-167)
execute if score #random temp matches 16 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr
execute if score #random temp matches 17..19 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en
execute if score #random temp matches 20..21 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en_de
execute if score #random temp matches 22..24 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en_es_it
# - Pickpockets (200-208)
execute if score #random temp matches 25 run function tch:station/annonce/generique/gen_id/pickpockets/fr
execute if score #random temp matches 26..27 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en
execute if score #random temp matches 28 run function tch:station/annonce/generique/gen_id/pickpockets/fr_de
execute if score #random temp matches 29 run function tch:station/annonce/generique/gen_id/pickpockets/fr_es
execute if score #random temp matches 30 run function tch:station/annonce/generique/gen_id/pickpockets/fr_it
execute if score #random temp matches 31..35 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en_es_de
execute if score #random temp matches 36..41 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en_es_it
execute if score #random temp matches 42..46 run function tch:station/annonce/generique/gen_id/pickpockets/fr_en_ja_zh
execute if score #random temp matches 47..49 run function tch:station/annonce/generique/gen_id/pickpockets/full
# - Sauvette (220-226)
execute if score #random temp matches 50..51 run function tch:station/annonce/generique/gen_id/sauvette/fr
execute if score #random temp matches 52..55 run function tch:station/annonce/generique/gen_id/sauvette/fr_en
execute if score #random temp matches 56 run function tch:station/annonce/generique/gen_id/sauvette/fr_de
execute if score #random temp matches 57..59 run function tch:station/annonce/generique/gen_id/sauvette/fr_en_it
execute if score #random temp matches 60..64 run function tch:station/annonce/generique/gen_id/sauvette/fr_en_it_zh
# - Confort descente 1 (400-405)
execute if score #random temp matches 65..66 run function tch:station/annonce/generique/gen_id/confort_descente/fr
execute if score #random temp matches 67..69 run function tch:station/annonce/generique/gen_id/confort_descente/fr_en
execute if score #random temp matches 70..71 run function tch:station/annonce/generique/gen_id/confort_descente/fr_en_de
execute if score #random temp matches 72..74 run function tch:station/annonce/generique/gen_id/confort_descente/fr_en_it
# - Confort descente short (410)
execute if score #random temp matches 75..84 run function tch:station/annonce/generique/gen_id/confort_descente/court
# - Signal sonore (420-426)
execute if score #random temp matches 85 run function tch:station/annonce/generique/gen_id/signal_sonore/fr
execute if score #random temp matches 86..88 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en
execute if score #random temp matches 89..90 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en_de
execute if score #random temp matches 91..92 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en_it
execute if score #random temp matches 93.. run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en_de_it
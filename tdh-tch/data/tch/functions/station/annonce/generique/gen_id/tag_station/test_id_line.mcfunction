# On sait que la valeur idLine est non nulle
# Selon la valeur d'idLineMax, on a un comportement différent

# Si idLineMax >= idLine, alors on tag tous les haut-parleurs qui se trouvent entre ces deux ID
execute if score #tchAnnonce idLineMax >= #tchAnnonce idLine as @e[type=item_frame,tag=tchHP] if score @s idLine >= #tchAnnonce idLine if score @s idLine <= #tchAnnonce idLineMax run tag @s add tchAnnonceDiffuseur
# Dans le cas contraire, on tag tous les haut-parleurs qui se trouvent EN-DEHORS de ces deux ID
# (par exemple si idLine = 4000 et idLineMax = 1999, on veut sélectionner toutes les lignes >= à 4000 ET toutes les lignes <= 1999)
execute if score #tchAnnonce idLineMax < #tchAnnonce idLine as @e[type=item_frame,tag=tchHP] run function tch:station/annonce/generique/gen_id/tag_station/tag_inverse
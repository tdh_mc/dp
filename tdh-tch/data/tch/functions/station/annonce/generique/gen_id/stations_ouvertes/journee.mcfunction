# Génération d'un ID d'annonce station ouverte, si on est en journée
# On a déjà pris un nombre aléatoire 0-99 dans #random temp

# On génère un ID d'annonce au pif parmi les disponibles :
# - Interdit de fumer générique (100-107)
execute if score #random temp matches ..4 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr
execute if score #random temp matches 5..9 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_en
execute if score #random temp matches 10..12 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_de
execute if score #random temp matches 13..16 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_es
execute if score #random temp matches 17..19 run function tch:station/annonce/generique/gen_id/interdit_fumer/1/fr_it
# - Interdit de fumer RER (120-121)
execute if score #random temp matches 20..23 run function tch:station/annonce/generique/gen_id/interdit_fumer/rer/fr
execute if score #random temp matches 24..29 run function tch:station/annonce/generique/gen_id/interdit_fumer/rer/fr_en
# - Sacs/bagages générique (140-142/147)
execute if score #random temp matches 30 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr
execute if score #random temp matches 31 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_en
execute if score #random temp matches 32 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_de
execute if score #random temp matches 33..34 run function tch:station/annonce/generique/gen_id/sacs_bagages/1/fr_es_it
# - Sacs/bagages ferré (160-167)
execute if score #random temp matches 35 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr
execute if score #random temp matches 36 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_en
execute if score #random temp matches 37 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_de
execute if score #random temp matches 38 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_es
execute if score #random temp matches 39 run function tch:station/annonce/generique/gen_id/sacs_bagages/ferre/fr_it
# - Sauvette (220-226)
execute if score #random temp matches 40..41 run function tch:station/annonce/generique/gen_id/sauvette/fr
execute if score #random temp matches 42..44 run function tch:station/annonce/generique/gen_id/sauvette/fr_en
execute if score #random temp matches 45 run function tch:station/annonce/generique/gen_id/sauvette/fr_de
execute if score #random temp matches 46 run function tch:station/annonce/generique/gen_id/sauvette/fr_it
execute if score #random temp matches 47..52 run function tch:station/annonce/generique/gen_id/sauvette/fr_en_it
execute if score #random temp matches 53..59 run function tch:station/annonce/generique/gen_id/sauvette/fr_en_zh
execute if score #random temp matches 60..64 run function tch:station/annonce/generique/gen_id/sauvette/fr_en_it_zh
# - Achat retour (300-305)
execute if score #random temp matches 65..67 run function tch:station/annonce/generique/gen_id/achat_retour/fr
execute if score #random temp matches 68..72 run function tch:station/annonce/generique/gen_id/achat_retour/fr_en
execute if score #random temp matches 73..76 run function tch:station/annonce/generique/gen_id/achat_retour/fr_de
execute if score #random temp matches 77..79 run function tch:station/annonce/generique/gen_id/achat_retour/fr_es
# - Confort descente 1 (400-405)
execute if score #random temp matches 80 run function tch:station/annonce/generique/gen_id/confort_descente/fr
execute if score #random temp matches 81..82 run function tch:station/annonce/generique/gen_id/confort_descente/fr_en
execute if score #random temp matches 83..84 run function tch:station/annonce/generique/gen_id/confort_descente/fr_de
execute if score #random temp matches 85 run function tch:station/annonce/generique/gen_id/confort_descente/fr_it
# - Confort descente short (410)
execute if score #random temp matches 86..89 run function tch:station/annonce/generique/gen_id/confort_descente/court
# - Signal sonore (420-426)
execute if score #random temp matches 90..92 run function tch:station/annonce/generique/gen_id/signal_sonore/fr
execute if score #random temp matches 93..96 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_en
execute if score #random temp matches 97 run function tch:station/annonce/generique/gen_id/signal_sonore/fr_de
execute if score #random temp matches 98.. run function tch:station/annonce/generique/gen_id/signal_sonore/fr_it
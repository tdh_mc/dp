# On prend un nombre aléatoire (qui sera utilisé dans les sous-fonctions)
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# L'heure de la journée est toujours stockée par tdh-time dans #temps currentHour
# On lance donc différentes sous-fonctions en fonction de l'heure actuelle
# On n'utilise pas la variable timeOfDay pour discriminer entre jour/nuit parce qu'à ce moment-là on aurait des messages nuit qui commenceraient à 16h en hiver, or "nuit" ne définit pas vrmt une période astronomique en l'occurence mais une période définie par la faible affluence des stations

# 21h à 5h59 : annonces nuit
execute unless score #temps currentHour matches 6..20 run function tch:station/annonce/generique/gen_id/stations_ouvertes/nuit
# 6h à 8h59 : annonces pointe matin
execute if score #temps currentHour matches 6..8 run function tch:station/annonce/generique/gen_id/stations_ouvertes/pointe_matin
# 9h à 16h59 : annonces journée
execute if score #temps currentHour matches 9..16 run function tch:station/annonce/generique/gen_id/stations_ouvertes/journee
# 17h à 20h59 : annonces pointe soir
execute if score #temps currentHour matches 17..20 run function tch:station/annonce/generique/gen_id/stations_ouvertes/pointe_soir

# On donne un tag à toutes les stations actuellement ouvertes
# Une sous-fonction à l'intérieur de celle-ci tag également tous les haut-parleurs qui correspondent aux idLine enregistrés plus haut
execute as @e[type=#tch:marqueur/station,tag=NomStation,tag=StationOuverte] run function tch:station/annonce/generique/gen_id/tag_station
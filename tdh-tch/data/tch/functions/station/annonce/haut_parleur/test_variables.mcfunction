# On exécute cette fonction en tant qu'un haut-parleur tchHP,
# pour calculer les variables idLine et idStation si on ne les possède pas encore

execute unless score @s idStation matches 1.. at @s run function tch:id/station_proche
# Pour idLine on prend aussi la condition négative car on veut pouvoir définir une valeur négative
# afin d'avoir des hauts-parleurs "station" non liés à une ligne en particulier (entrée, salles d'échange)
execute unless score @s idLine matches 1.. unless score @s idLine matches ..-1 at @s run function tch:id/ligne_proche
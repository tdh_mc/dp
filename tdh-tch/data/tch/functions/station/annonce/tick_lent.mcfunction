# Explication du nouveau système des annonces :
# Cette fonction est exécutée à un intervalle de temps très élevé (32 secondes actuellement)
# pour démarrer la lecture des annonces dans toutes les stations à la fois
# Une autre fonction, tick_rapide, se charge de diffuser chaque son au bon moment lorsque le système est actif

# L'idée est que toutes les 32 secondes, cette fonction :
# - définira le score #tchAnnonce id à l'annonce choisie pour être diffusée ;
# - taggera tous les haut parleurs compatibles avec cette annonce actuellement chargés ;

# Ensuite, le tick_rapide prendra le relais pour diffuser chaque son individuellement au bon moment, et repasser le score annonceID à 0 lorsqu'il a fini tout en retirant les tags des haut-parleurs.
# Comme les annonces durent rarement 32 secondes, le tick rapide ne sera exécuté qu'une partie du temps

# Toutes les item frames tchHP (haut-parleurs) qui n'ont pas d'idStation ou d'idLine essaient d'en calculer un
execute as @e[type=item_frame,tag=tchHP] run function tch:station/annonce/haut_parleur/test_variables
# Tous les PID qui n'ont pas d'idStation ou d'idLine essaient d'en calculer un
# (c'est la seule partie du PID qui utilise le tick lent ; les ID d'annonce sont gérés par les spawners)
execute as @e[type=item_frame,tag=PID] run function tch:station/annonce/haut_parleur/test_variables

# Si on a un ID d'annonce mais qu'on n'a pas avancé d'ID de son à diffuser depuis 32 secondes,
# on définit notre ID d'annonce à 0 pour enclencher la condition qui suit
# (dans le cas d'une fonction diffuse buggée ou d'un gen_id mal défini qui empêche l'annonce d'avancer, pour éviter de tout bloquer)
execute if score #tchAnnonce id matches 1.. unless score #tchAnnonce status matches 2.. run scoreboard players set #tchAnnonce id 0
# On essaie de générer une annonce si aucune n'est déjà en cours de diffusion
execute unless score #tchAnnonce id matches 1.. run function tch:station/annonce/generique/gen_id
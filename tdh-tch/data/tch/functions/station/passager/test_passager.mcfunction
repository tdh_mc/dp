# Si on est pas dans un RER et qu'il y a un joueur à proximité,
# on se donne une destination RP pour sortir de la station
# (sauf si on a le tag "TrajetTermine" qui signifie qu'on est sortis de la station)
execute as @s[type=#tch:passager_intelligent,tag=!TrajetTermine] if entity @p[distance=..54] unless entity @e[type=armor_stand,tag=RER,distance=..7] run function tch:station/passager/test_item_frame

# Si on est sur un bloc de gravier, on se déplace vers notre destination
execute at @s if block ~ ~-1 ~ #tch:passager_perdu unless entity @e[type=armor_stand,tag=RER,distance=..5] run function tch:station/passager/tp_passager_perdu

# S'il n'y a pas de joueur à proximité, on se supprime
execute unless entity @p[distance=..54] run function tch:rer/rame/detruire/detruire_entite



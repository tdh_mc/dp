# On est une item frame tchPassagerDest à sa propre position
# En fonction de nos scores, on invoque des items temporaires :

# On se donne un tag pour se retrouver
tag @s add PassagerDestTemp

# On affiche l’ID de marqueur
function tch:station/passager/debug/tick/id_marqueur

# Enfin, on retire notre tag temporaire
tag @s remove PassagerDestTemp
# On exécute cette fonction à la position d’une item frame TexteTemporaire pour afficher l’ID de ligne d’un spawner

# On enregistre l’ID de ligne dans le panneau
data modify block ~ ~ ~ Text4 set value '[{"text":"[PassagerDest ","color":"gold"},{"score":{"name":"@e[type=item_frame,tag=PassagerDestTemp,limit=1]","objective":"idMarqueur"},"bold":"true"},{"text":"]"}]'

# On définit le CustomName de l’item invoqué
data modify entity @e[type=item,tag=PassagerDestDebugItemTemp,limit=1] CustomName set from block ~ ~ ~ Text4
# On invoque un item pour afficher les infos, puis on exécute le reste dans une sous-fonction à la position d’un TexteTemporaire
summon item ~ ~0.5 ~ {NoGravity:1b,Invulnerable:1b,Tags:["PassagerDestDebugItem","PassagerDestDebugItemTemp"],CustomNameVisible:1b,PickupDelay:32767s,Age:5839s,Item:{id:"polished_blackstone_button",Count:1b,tag:{a:150}}}
execute if score @s idMarqueur matches 0.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:station/passager/debug/tick/id_marqueur_texte
execute unless score @s idMarqueur matches 0.. run data modify entity @e[type=item,tag=PassagerDestDebugItemTemp,limit=1] CustomName set value '[{"text":"[PassagerDest ???]","color":"red"}]'

# Enfin, on retire les tags temporaires
tag @e[type=item,tag=PassagerDestDebugItemTemp] remove PassagerDestDebugItemTemp

# On enregistre dans un storage temporaire la position de l'exécutant
data modify storage tch:passager destination.value set value {pos:[0,0,0],dimension:"minecraft:overworld"}

execute store result score #passager temp run data get entity @s Pos[0]
execute store result storage tch:passager destination.value.pos[0] int 1 run scoreboard players get #passager temp
execute store result score #passager temp run data get entity @s Pos[1]
execute store result storage tch:passager destination.value.pos[1] int 1 run scoreboard players get #passager temp
execute store result score #passager temp run data get entity @s Pos[2]
execute store result storage tch:passager destination.value.pos[2] int 1 run scoreboard players get #passager temp

# On enregistre l'ID du marqueur
scoreboard players operation #passager idMarqueur = @s idMarqueur
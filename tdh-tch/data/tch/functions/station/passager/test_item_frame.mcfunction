# On enregistre notre ID de marqueur précédent (celui du passager)
scoreboard players operation #passager idMarqueur = @s idMarqueur
scoreboard players operation #temp idMarqueur = #passager idMarqueur

# On tag toutes les item frames qui ont un ID marqueur supérieur au nôtre
execute as @e[type=item_frame,tag=tchPassagerDest,distance=..80] if score @s idMarqueur > #passager idMarqueur run tag @s add MarqueurValide

# On trouve le minimum des ID marqueurs disponibles, et on retire le tag de tous les autres (sauf si la différence est de moins de 100)
scoreboard players set #passager temp 999999999
execute as @e[type=item_frame,tag=MarqueurValide] run scoreboard players operation #passager temp < @s idMarqueur
scoreboard players add #passager temp 100
execute as @e[type=item_frame,tag=MarqueurValide] if score @s idMarqueur > #passager temp run tag @s remove MarqueurValide

# On choisit le plus proche des marqueurs valides pour se le donner comme destination
execute as @s[type=villager] at @e[type=item_frame,tag=MarqueurValide,sort=nearest,limit=1] run function tch:station/passager/donner_destination
execute as @s[type=wandering_trader] at @e[type=item_frame,tag=MarqueurValide,sort=nearest,limit=1] run function tch:station/passager/donner_destination_derek

# Si l'ID marqueur n'a pas changé, on décrémente notre score remainingTicks
execute if score @s idMarqueur = #temp idMarqueur run scoreboard players remove @s remainingTicks 1
# Sinon, on le reset à la valeur d'origine
execute unless score @s idMarqueur = #temp idMarqueur run scoreboard players set @s remainingTicks 20

# Si l'ID marqueur n'a pas changé depuis de nombreux ticks (= on est bloqués), on se tp sur le marqueur suivant
execute if score @s remainingTicks matches ..-1 run function tch:station/passager/test_teleportation

tag @e[type=item_frame,tag=MarqueurValide] remove MarqueurValide
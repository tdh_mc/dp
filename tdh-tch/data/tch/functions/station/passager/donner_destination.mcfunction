# On donne au passager qui exécute la fonction la "destination" (=lit + travail + PdR) de l'emplacement auquel on se trouve, matérialisé par une item frame

# On enregistre d'abord dans un storage la position
execute as @e[type=item_frame,tag=tchPassagerDest,sort=nearest,limit=1] run function tch:station/passager/sauvegarder_destination
# Si on est à plus de 3 blocs du marqueur, on retranche 1 pour pouvoir le sélectionner à nouveau si on a pas réussi à s'en approcher
execute unless entity @s[distance=..3] run scoreboard players remove #passager idMarqueur 1

# On se la donne ensuite pour maison, travail et PDR
data modify entity @s Brain.memories.minecraft:home set from storage tch:passager destination
data modify entity @s Brain.memories.minecraft:job_site set from storage tch:passager destination
data modify entity @s Brain.memories.minecraft:meeting_point set from storage tch:passager destination

# On se donne l'ID marqueur
scoreboard players operation @s idMarqueur = #passager idMarqueur

tellraw @a[tag=PassagerDebugLog] [{"text":"On a donné à un passager "},{"selector":"@s"},{"text":" le score de marqueur ","color":"gray"},{"score":{"name":"@s","objective":"idMarqueur"}},{"text":" (ancien score "},{"score":{"name":"#passager","objective":"idMarqueur"}},{"text":")."}]
# On donne au passager qui exécute la fonction la "destination" (=lit + travail + PdR) de l'emplacement auquel on se trouve, matérialisé par une item frame

# On enregistre d'abord dans un storage la position
execute as @e[type=item_frame,tag=tchPassagerDest,sort=nearest,limit=1] run function tch:station/passager/sauvegarder_destination

# On se la donne ensuite pour cible d'errance
data modify entity @s WanderTarget set from storage tch:passager destination.value.pos

# Si l'ID marqueur n'a pas changé, on décrémente notre score remainingTicks
execute if score @s idMarqueur = #passager idMarqueur run scoreboard players remove @s remainingTicks 1
# Sinon, on le reset à la valeur d'origine
execute unless score @s idMarqueur = #passager idMarqueur run scoreboard players set @s remainingTicks 20

# On se donne l'ID marqueur
scoreboard players operation @s idMarqueur = #passager idMarqueur

tellraw @a[tag=PassagerDebugLog] [{"text":"On a donné à un passager "},{"selector":"@s"},{"text":" le score de marqueur ","color":"gray"},{"score":{"name":"@s","objective":"idMarqueur"}},{"text":" (ancien score "},{"score":{"name":"#passager","objective":"idMarqueur"}},{"text":")."}]
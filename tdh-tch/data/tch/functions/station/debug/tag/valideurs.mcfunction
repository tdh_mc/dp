# Cette fonction doit être exécutée après avoir défini le score #tchInfostation idStation,
# et donne un tag à tous les valideurs correspondant à cette station

# On donne un tag à tous les valideurs de la station
execute as @e[type=item_frame,tag=ValideurGold] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationValideurG
execute as @e[type=item_frame,tag=ValideurSilver] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationValideurS

# On les compte et on met le résultat dans le storage
execute store result storage tch:infostation valideur.n_gold int 1 if entity @e[type=item_frame,tag=tchInfostationValideurG]
execute store result storage tch:infostation valideur.n_silver int 1 if entity @e[type=item_frame,tag=tchInfostationValideurS]
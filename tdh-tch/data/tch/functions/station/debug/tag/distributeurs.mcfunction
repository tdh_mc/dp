# Cette fonction doit être exécutée après avoir défini le score #tchInfostation idStation,
# et donne un tag à tous les distributeurs correspondant à cette station

# On donne un tag à tous les distributeurs de la station
execute as @e[type=item_frame,tag=Distributeur] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationDistrib

# On les compte et on met le résultat dans le storage
execute store result storage tch:infostation distributeur.n int 1 if entity @e[type=item_frame,tag=tchInfostationDistrib]
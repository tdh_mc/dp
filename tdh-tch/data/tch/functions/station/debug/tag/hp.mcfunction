# Cette fonction doit être exécutée après avoir défini le score #tchInfostation idStation,
# et donne un tag à tous les haut-parleurs correspondant à cette station

# On tag tous les haut-parleurs de la station
execute as @e[type=item_frame,tag=tchHP] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationHP

# On les compte et on met le résultat dans le storage
execute store result storage tch:infostation hp.n int 1 if entity @e[type=item_frame,tag=tchInfostationHP]
execute store result storage tch:infostation hp.n_clean int 1 if entity @e[type=item_frame,tag=tchInfostationHP,tag=Clean]
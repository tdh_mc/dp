# Cette fonction doit être exécutée après avoir défini le score #tchInfostation idStation,
# et donne un tag à toutes les entrées et sorties correspondant à cette station

# On ajoute un tag aux entrées et aux sorties
execute as @e[type=item_frame,tag=tchEntrance,tag=Outside] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationEntree
execute as @e[type=item_frame,tag=tchEntrance,tag=Inside] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationSortie

# On enregistre le compte dans le storage
execute store result storage tch:infostation acces.n_entrees int 1 if entity @e[type=item_frame,tag=tchInfostationEntree]
execute store result storage tch:infostation acces.n_sorties int 1 if entity @e[type=item_frame,tag=tchInfostationSortie]
# Cette fonction doit être exécutée après avoir défini le score #tchInfostation idStation,
# et donne un tag à toutes les portes de quai correspondant à cette station

# On tag toutes les portes de quai de la station
execute as @e[type=item_frame,tag=PorteQuai] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationPorteQuai

# On les compte et on met le résultat dans le storage
execute store result storage tch:infostation porte_quai.n int 1 if entity @e[type=item_frame,tag=tchInfostationPorteQuai]
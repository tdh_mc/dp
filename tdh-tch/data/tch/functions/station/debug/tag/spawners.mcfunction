# Cette fonction doit être exécutée après avoir défini le score #tchInfostation idStation,
# et donne un tag à tous les spawners correspondant à cette station

# On donne un tag à tous les spawners de la station
execute as @e[type=#tch:marqueur/spawner,tag=SpawnMetro] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationSpawner
execute as @e[type=#tch:marqueur/spawner,tag=SpawnRER] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationSpawner
execute as @e[type=#tch:marqueur/spawner,tag=SpawnCable] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationSpawner

# On les compte et on met le résultat dans le storage
execute store result storage tch:infostation spawner.n int 1 if entity @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawner]
execute store result storage tch:infostation spawner.n_metro int 1 if entity @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawner,tag=SpawnMetro]
execute store result storage tch:infostation spawner.n_rer int 1 if entity @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawner,tag=SpawnRER]
execute store result storage tch:infostation spawner.n_cable int 1 if entity @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawner,tag=SpawnCable]
# Cette fonction doit être exécutée après avoir défini le score #tchInfostation idStation,
# et donne un tag à tous les PIDs correspondant à cette station

# On donne un tag à tous les PID de la station
execute as @e[type=item_frame,tag=PID] if score @s idStation = #tchInfostation idStation run tag @s add tchInfostationPID

# On les compte et on met le résultat dans le storage
execute store result storage tch:infostation pid.n int 1 if entity @e[type=item_frame,tag=tchInfostationPID]
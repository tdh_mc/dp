# Affichage des infos détaillées des spawners

# On tag d'abord toutes les entités appropriées de la station,
# ainsi que le joueur (pour pouvoir lui afficher le texte en faisant execute as)
function tch:station/debug/info/id_station
function tch:station/debug/tag/spawners
tag @s add tchInfostationJoueur

# Titre de l'écran d'info
tellraw @s [{"text":"La station ","color":"gold"},{"selector":"@e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1]"},{"text":" comprend "},{"storage":"tch:infostation","nbt":"spawner.n","bold":"true","extra":[{"text":" spawners"}]},{"text":" :"}]
tellraw @s [{"text":"-----------","color":"gray"}]

# On affiche les infos détaillées de chaque direction dans l'ordre des ID line
function tch:station/debug/info/spawners/tri


# On retire tous les tags temporaires
function tch:station/debug/untag/spawners
tag @s remove tchInfostationJoueur
# On exécute cette fonction en tant qu'une direction d'une station,
# pour récupérer l'ID de la prochaine station de l'itinéraire

# On récupère notre PC de quai
function tch:tag/quai_pc
# On définit les valeurs à rechercher (notre ID de ligne, pas d'ID de sortie)
# On reset l'ID station pour indiquer qu'on recherche cette valeur
scoreboard players set #tchItineraire idSortie 0
scoreboard players reset #tchItineraire idStation
scoreboard players operation #tchItineraire idLine = @s idLine

# On cherche la bonne connexion parmi celles de notre PC
execute as @e[type=item_frame,tag=ourQuaiPC,limit=1] run function tch:itineraire/noeud/trouver_connexion

# Si on a trouvé une connexion valable, on enregistre la prochaine station dans nos scores
scoreboard players reset @s prochaineStation
execute if entity @e[type=item_frame,tag=ourQuaiPC,tag=ConnexionTrouvee,limit=1] run scoreboard players operation @s prochaineStation = #tchItineraire prochaineStation

# Selon si on l'a trouvée ou non, on enregistre un message différent pour affichage
execute if score @s prochaineStation matches 1.. run function tch:station/debug/info/lignes/direction/sauvegarder_station
execute unless score @s[tag=!Terminus] prochaineStation matches 1.. run data modify storage tch:infostation ligne.prochaine_station set value {texte:'[{"text":"Tracé introuvable!","color":"red","hoverEvent":{"action":"show_text","contents":[{"text":"Impossible de trouver une connexion valide dans le système d\'itinéraire."}]}}]'}
execute unless score @s[tag=Terminus] prochaineStation matches 1.. run data modify storage tch:infostation ligne.prochaine_station set value {texte:'[{"text":"Tracé introuvable!","color":"white","hoverEvent":{"action":"show_text","contents":[{"text":"Impossible de trouver une connexion valide dans le système d\'itinéraire. Le fait qu\'on fasse terminus ici n\'y est probablement pas étranger."}]}}]'}

# On supprime le tag du PC de quai
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
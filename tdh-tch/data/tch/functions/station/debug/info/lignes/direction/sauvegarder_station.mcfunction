# On donne un tag au PC de la prochaine station enregistrée
function tch:tag/prochaine_station_pc

# On copie le nom de la station dans le storage
data remove storage tch:infostation ligne.prochaine_station
data modify storage tch:infostation ligne.prochaine_station set from entity @e[type=item_frame,tag=ourStationPC,limit=1] Item.tag.display.station
data modify storage tch:infostation ligne.prochaine_station.texte set value '[{"storage":"tch:infostation","nbt":"ligne.prochaine_station.prefixe","color":"white"},{"storage":"tch:infostation","nbt":"ligne.prochaine_station.nom"}]'

# Si on n'a pas pu récupérer le nom de la station, on remplace le texte à afficher par un message d'erreur
# On ne le met pas en blanc même si on fait terminus, car il indique un problème de configuration
# (s'il n'y avait pas de prochaine station on n'aurait simplement pas de connexion, or là il y en a une)
execute unless data storage tch:infostation ligne.prochaine_station.nom run data modify storage tch:infostation ligne.prochaine_station set value {texte:'[{"text":"Station introuvable!","color":"red","hoverEvent":{"action":"show_text","contents":[{"text":"Une connexion valide existe, mais il est impossible de trouver le PC de la station de destination ("},{"score":{"name":"@s","objective":"prochaineStation"}},{"text":")."}]}}]'}

# On retire le tag temporaire du PC
tag @e[type=item_frame,tag=ourStationPC] remove ourStationPC
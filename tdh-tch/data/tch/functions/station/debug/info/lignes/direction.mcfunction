# On affiche les infos détaillées de la direction actuelle d'une ligne (celle qui exécute cette fonction)

# On enregistre les morceaux de texte requis
# TODO pour l'auto-déblocage de la voie
execute as @s[tag=VoieReservee] run data modify storage tch:infostation ligne.voie_reservee set value '[{"text":" | "},{"text":"VR1","color":"red","bold":"true","hoverEvent":{"action":"show_text","value":"Cliquer pour tenter de libérer la voie."},"clickEvent":{"action":"run_command","value":"/me a bien de l\'espoir."}}]'
execute as @s[tag=!VoieReservee] run data modify storage tch:infostation ligne.voie_reservee set value '[{"text":" | "},{"text":"VR0","color":"green"}]'

execute as @s[scores={FileAttente=1..}] run data modify storage tch:infostation ligne.file_attente set value '[{"text":" | "},{"text":"FA","color":"red","hoverEvent":{"action":"show_text","value":"Métros en file d\'attente."},"bold":"true","extra":[{"score":{"name":"@s","objective":"FileAttente"}}]}]'
execute unless score @s FileAttente matches 1.. run data modify storage tch:infostation ligne.file_attente set value '[{"text":" | "},{"text":"FA0","color":"green","hoverEvent":{"action":"show_text","value":"Aucun métro en file d\'attente."}}]'

execute as @s[tag=StationInversee] run data modify storage tch:infostation ligne.station_inversee set value '[{"text":" | "},{"text":"DG","color":"gray","bold":"true","hoverEvent":{"action":"show_text","contents":"Descente à Gauche"}}]'
execute as @s[tag=!StationInversee] run data modify storage tch:infostation ligne.station_inversee set value '[{"text":" | "},{"text":"DD","color":"gray","hoverEvent":{"action":"show_text","contents":"Descente à Droite"}}]'

execute as @s[tag=Terminus] run data modify storage tch:infostation ligne.terminus set value '[{"text":" | "},{"text":"Terminus","color":"gold","bold":"true","hoverEvent":{"action":"show_text","contents":"Terminus de la ligne"}}]'
execute as @s[tag=!Terminus] run data remove storage tch:infostation ligne.terminus

# On tente de trouver la prochaine station
function tch:station/debug/info/lignes/direction/trouver_station

# On affiche le texte
tellraw @p[tag=tchInfostationJoueur] [{"text":"- [","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}},{"score":{"name":"@s","objective":"idLine"}},{"text":"] "},{"selector":"@e[type=#tch:marqueur/quai,tag=NumLigne,sort=nearest,limit=1]"},{"text":" → "},{"selector":"@s"},{"text":" | Prochaine station : "},{"storage":"tch:infostation","nbt":"ligne.prochaine_station.texte","interpret":"true"},{"storage":"tch:infostation","nbt":"ligne.terminus","interpret":"true"},{"storage":"tch:infostation","nbt":"ligne.station_inversee","interpret":"true"},{"storage":"tch:infostation","nbt":"ligne.voie_reservee","interpret":"true"},{"storage":"tch:infostation","nbt":"ligne.file_attente","interpret":"true"}]
execute as @e[type=#tch:marqueur/quai,tag=ProchaineStation,distance=..3,sort=nearest,limit=1] run tellraw @p[tag=tchInfostationJoueur] [{"text":"- ","color":"gray"},{"text":"Entité obsolète "},{"text":"ProchaineStation","italic":"true"},{"text":" : "},{"selector":"@s","color":"red"}]

# On retire notre tag pour ne pas repasser dans cette fonction
tag @s remove tchInfostationDirection
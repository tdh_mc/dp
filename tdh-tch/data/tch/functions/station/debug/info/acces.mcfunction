# Affichage des infos détaillées des sorties

# On tag d'abord toutes les entrées/sorties de la station,
# ainsi que le joueur (pour pouvoir lui afficher le texte en faisant execute as)
function tch:station/debug/info/id_station
function tch:station/debug/tag/entrees_sorties
tag @s add tchInfostationJoueur

# Titre de l'écran d'info
tellraw @s [{"text":"La station ","color":"gold"},{"selector":"@e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1]"},{"text":" comprend "},{"storage":"tch:infostation","nbt":"acces.n_entrees","bold":"true","extra":[{"text":" entrées"}]},{"text":" et "},{"storage":"tch:infostation","nbt":"acces.n_sorties","bold":"true","extra":[{"text":" sorties"}]},{"text":" :"}]
tellraw @s [{"text":"-----------","color":"gray"}]

# Boucle pour afficher chacune des entrées/sorties
# On affiche d'abord uniquement les sorties, pour lesquelles on devrait avoir une autre entité du même nom avec le tag entrée (dans ce cas, on retirera le tag des 2)
execute as @e[type=item_frame,tag=tchInfostationEntree] at @s run function tch:station/debug/info/acces/entree
execute as @e[type=item_frame,tag=tchInfostationSortie] at @s run function tch:station/debug/info/acces/sortie


# On retire tous les tags temporaires
function tch:station/debug/untag/entrees_sorties
tag @s remove tchInfostationJoueur
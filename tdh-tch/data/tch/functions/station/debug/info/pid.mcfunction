# Affichage des infos détaillées des PID

# On tag d'abord tous les PID de la station,
# ainsi que le joueur (pour pouvoir lui afficher le texte en faisant execute as)
function tch:station/debug/info/id_station
function tch:station/debug/tag/pid
tag @s add tchInfostationJoueur

# Titre de l'écran d'info
tellraw @s [{"text":"La station ","color":"gold"},{"selector":"@e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1]"},{"text":" comprend "},{"storage":"tch:infostation","nbt":"pid.n","bold":"true","extra":[{"text":" PID/SACD"}]},{"text":" :"}]
tellraw @s [{"text":"-----------","color":"gray"}]


# On affiche les PID ainsi que le modèle actuellement affiché,
# cependant on ne peut pas le faire directement puisqu'on souhaite les afficher dans l'ordre alphanumérique
# On a donc une sous-fonction qui les affiche séquentiellement du plus petit au plus grand idLine
function tch:station/debug/info/pid/tri


# On retire tous les tags temporaires
function tch:station/debug/untag/pid
tag @s remove tchInfostationJoueur
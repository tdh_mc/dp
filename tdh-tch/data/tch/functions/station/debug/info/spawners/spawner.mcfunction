# On affiche les infos détaillées du spawner qui exécute cette fonction
# On doit avoir notre ID line déjà stocké dans #tchInfostation idLine (c'est normalement fait par *tri*)

# On enregistre les morceaux de texte requis
# Les infos de notre ligne (on va les chercher dans notre PC, ce qui fait que le numéro de ligne ne sera PAS cliquable dans la ligne de texte qui va suivre)
execute as @e[type=item_frame,tag=tchPC] if score @s idLine <= #tchInfostation idLine if score @s idLineMax >= #tchInfostation idLine run data modify storage tch:infostation spawner.ligne set from entity @s Item.tag.display.line

# Le temps d'attente
scoreboard players operation #store currentHour = @s spawnAt
function tdh:store/time
execute if score @s spawnAt matches 0.. run data modify storage tch:infostation spawner.horaire set from storage tdh:store time
execute unless score @s spawnAt matches 0.. run data modify storage tch:infostation spawner.horaire set value '[{"text":"00","obfuscated":"true"},{"text":":","obfuscated":"false"},{"text":"47"}]'

# On affiche le texte
tellraw @p[tag=tchInfostationJoueur] [{"text":"- [","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}},{"score":{"name":"@s","objective":"idLine"}},{"text":"] "},{"storage":"tch:infostation","nbt":"spawner.ligne.nom","interpret":"true"},{"text":" → "},{"selector":"@s"},{"text":" | Prochain"},{"storage":"tch:infostation","nbt":"spawner.ligne.vehicule.suffixe"},{"text":" "},{"storage":"tch:infostation","nbt":"spawner.ligne.vehicule.nom"},{"text":" à "},{"storage":"tch:infostation","nbt":"spawner.horaire","interpret":"true"},{"text":" (tick "},{"score":{"name":"@s","objective":"spawnAt"}},{"text":")"}]

# On retire notre tag pour ne pas repasser dans cette fonction
tag @s remove tchInfostationSpawner
# On veut trier les spawners de cette station dans le bon ordre pour les afficher
# On va exécuter cette fonction de façon répétée, en affichant le spawner à l'ID le plus bas à chaque itération

# Toutes les directions que l'on veut traiter ont le tag tchInfostationSpawner,
# que l'on va leur retirer lorsqu'on les ajoutera à cette liste

# On commence par calculer l'ID le plus bas
scoreboard players set #tchInfostation idLine 9999999
execute as @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawner] run scoreboard players operation #tchInfostation idLine < @s idLine

# On exécute ensuite la suite des opérations en tant que la ligne ayant cet ID
# (et on retire son tag directement dans cette fonction)
execute as @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawner] if score @s idLine = #tchInfostation idLine at @s run function tch:station/debug/info/spawners/spawner

# S'il reste des entités ayant le tag, on répète cette fonction
execute if entity @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawner,limit=1] run function tch:station/debug/info/spawners/tri
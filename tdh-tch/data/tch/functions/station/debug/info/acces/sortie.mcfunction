# On ne peut être dans cette fonction que s'il n'y avait pas d'entrée proche de celle-ci, ou s'il n'y en avait pas le même compte

# On affiche donc un message en rouge indiquant que cette sortie est orpheline
tellraw @p[tag=tchInfostationJoueur] [{"text":"- ","color":"red"},{"text":"Sortie orpheline : ","hoverEvent":{"action":"show_text","contents":"Cette item frame Sortie est\ntrop loin d'une item frame Entrée\npour être fonctionnelle, ou bien\nle nombre d'item frames Entrée\net Sortie n'est pas équilibré."}},{"selector":"@s","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}}]

# On retire notre tag
tag @s remove tchInfostationSortie
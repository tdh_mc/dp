# On vérifie à proximité si on trouve une autre entité que nous-mêmes ayant le tag opposé

# Si c'est le cas, on affiche un message avec les deux entités sur la même ligne
execute if entity @e[type=item_frame,distance=..50,tag=tchInfostationSortie,limit=1] run tellraw @p[tag=tchInfostationJoueur] [{"text":"- ","color":"green"},{"selector":"@s","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}},{"text":" / "},{"selector":"@e[type=item_frame,tag=tchInfostationSortie,distance=..50,sort=nearest,limit=1]","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@e[type=item_frame,tag=tchInfostationSortie,distance=..50,sort=nearest,limit=1]","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@e[type=item_frame,tag=tchInfostationSortie,distance=..50,sort=nearest,limit=1]","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@e[type=item_frame,tag=tchInfostationSortie,distance=..50,sort=nearest,limit=1]","nbt":"Pos[2]"}]}}]

# Sinon, on affiche un message en rouge indiquant que cette entrée est orpheline
execute unless entity @e[type=item_frame,distance=..50,tag=tchInfostationSortie,limit=1] run tellraw @p[tag=tchInfostationJoueur] [{"text":"- ","color":"red"},{"text":"Entrée orpheline : ","hoverEvent":{"action":"show_text","contents":"Cette item frame Entrée est\ntrop loin d'une item frame Sortie\npour être fonctionnelle."}},{"selector":"@s","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}}]

# On retire le tag à la fois de nous-mêmes et de la sortie la plus proche si elle existe
tag @s remove tchInfostationEntree
tag @e[type=item_frame,tag=tchInfostationSortie,distance=..50,sort=nearest,limit=1] remove tchInfostationSortie
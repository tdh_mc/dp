# Affichage des infos détaillées des haut-parleurs

# On tag d'abord tous les HP de la station,
# ainsi que le joueur (pour pouvoir lui afficher le texte en faisant execute as)
function tch:station/debug/info/id_station
function tch:station/debug/tag/hp
tag @s add tchInfostationJoueur

# Titre de l'écran d'info
tellraw @s [{"text":"La station ","color":"gold"},{"selector":"@e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1]"},{"text":" comprend "},{"storage":"tch:infostation","nbt":"hp.n","bold":"true","extra":[{"text":" haut-parleurs"}]},{"text":", dont "},{"storage":"tch:infostation","nbt":"hp.n_clean","bold":"true","extra":[{"text":" clean"}]},{"text":" :"}]
tellraw @s [{"text":"-----------","color":"gray"}]

# On affiche les infos détaillées de chaque haut-parleur (avec son ID ligne d'assignation)
execute as @e[type=item_frame,tag=tchInfostationHP,tag=!Clean] run function tch:station/debug/info/hp/hp
execute as @e[type=item_frame,tag=tchInfostationHP,tag=Clean] run function tch:station/debug/info/hp/hp


# On retire tous les tags temporaires
function tch:station/debug/untag/hp
tag @s remove tchInfostationJoueur
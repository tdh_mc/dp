# Affichage des infos détaillées des portes de quai

# On tag d'abord toutes les portes de quai de la station,
# ainsi que le joueur (pour pouvoir lui afficher le texte en faisant execute as)
function tch:station/debug/info/id_station
function tch:station/debug/tag/portes_quai
tag @s add tchInfostationJoueur

# Titre de l'écran d'info
tellraw @s [{"text":"La station ","color":"gold"},{"selector":"@e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1]"},{"text":" comprend "},{"storage":"tch:infostation","nbt":"porte_quai.n","bold":"true","extra":[{"text":" portes de quai"}]},{"text":" :"}]
tellraw @s [{"text":"-----------","color":"gray"}]

# On affiche les infos détaillées de chaque porte de quai (avec son ID ligne d'assignation)
execute as @e[type=item_frame,tag=tchInfostationPorteQuai] run function tch:station/debug/info/portes_quai/porte_quai


# On retire tous les tags temporaires
function tch:station/debug/untag/portes_quai
tag @s remove tchInfostationJoueur
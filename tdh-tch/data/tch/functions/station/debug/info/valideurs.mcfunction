# Affichage des infos détaillées des valideurs

# On tag d'abord tous les valideurs de la station,
# ainsi que le joueur (pour pouvoir lui afficher le texte en faisant execute as)
function tch:station/debug/info/id_station
function tch:station/debug/tag/valideurs
tag @s add tchInfostationJoueur

# Titre de l'écran d'info
tellraw @s [{"text":"La station ","color":"gold"},{"selector":"@e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1]"},{"text":" comprend "},{"storage":"tch:infostation","nbt":"valideur.n_gold","bold":"true","extra":[{"text":" valideurs Gold"}]},{"text":", dont "},{"storage":"tch:infostation","nbt":"valideur.n_silver","bold":"true","extra":[{"text":" valideurs Silver"}]},{"text":" :"}]
tellraw @s [{"text":"-----------","color":"gray"}]

# Comme on n'a pas d'infos spécifiques à afficher pour chaque valideur, on les affiche juste tous sur la même ligne avec les sélecteurs multiples de Minecraft
tellraw @s [{"selector":"@e[type=item_frame,tag=tchInfostationValideurG]"}]
tellraw @s [{"selector":"@e[type=item_frame,tag=tchInfostationValideurS]"}]


# On retire tous les tags temporaires
function tch:station/debug/untag/valideurs
tag @s remove tchInfostationJoueur
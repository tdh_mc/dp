# On veut afficher le nom de notre ligne + direction et le stocker dans le storage
# Cette fonction doit être exécutée à la position d'une item frame TexteTemporaire (et donc d'un panneau)

# On récupère notre PC et notre PC station de terminus
function tch:tag/pc
function tch:tag/terminus_pc

# On fait un rendu du nom de terminus complet
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.station.prefixe
data modify block ~ ~ ~ Text1 set value '[{"entity":"@e[type=item_frame,tag=ourPC,limit=1]","nbt":"Item.tag.display.line.terminus","interpret":"true"}]'
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.station.nom
data modify block ~ ~ ~ Text2 set value '[{"entity":"@e[type=item_frame,tag=ourPC,limit=1]","nbt":"Item.tag.display.line.terminus","interpret":"true"}]'
data modify block ~ ~ ~ Text3 set value '[{"block":"~ ~ ~","nbt":"Text1","interpret":"true"},{"block":"~ ~ ~","nbt":"Text2","interpret":"true"}]'

# On affiche la phrase sur le panneau
data modify block ~ ~ ~ Text1 set value '[{"entity":"@e[type=item_frame,tag=ourPC,limit=1]","nbt":"Item.tag.display.Name","interpret":"true"},{"text":"→"},{"block":"~ ~ ~","nbt":"Text3","interpret":"true"}]'
data modify storage tch:infostation porte_quai.direction set from block ~ ~ ~ Text1

# On nettoie les tags des PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
# On affiche le nom de la porte de quai, ainsi que sa ligne d'assignation

# On stocke une phrase selon l'assignation de ligne
execute unless score @s idLine matches 1.. run data modify storage tch:infostation porte_quai.direction set value '[{"text":"Aucune assignation!","color":"red"}]'
execute if score @s idLine matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:station/debug/info/portes_quai/get_line

# On affiche le texte
tellraw @p[tag=tchInfostationJoueur] [{"text":"- ","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}},{"selector":"@s"},{"text":" ["},{"storage":"tch:infostation","nbt":"porte_quai.direction","bold":"true","interpret":"true"},{"text":"]"}]
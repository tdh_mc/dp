# On affiche le nom du haut-parleur, ainsi que sa ligne d'assignation

# On stocke une phrase selon l'assignation de ligne
execute if score @s idLine matches ..-1 run data modify storage tch:infostation hp.etat set value '[{"text":"Salle d\'échange","color":"gray"}]'
execute unless score @s idLine matches ..-1 unless score @s idLine matches 1.. run data modify storage tch:infostation hp.etat set value '[{"text":"Aucune assignation!","color":"red"}]'
execute if score @s idLine matches 1.. run function tch:station/debug/info/hp/get_line

# On affiche le texte
tellraw @p[tag=tchInfostationJoueur] [{"text":"- ","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}},{"selector":"@s"},{"text":" ["},{"storage":"tch:infostation","nbt":"hp.etat","bold":"true","interpret":"true"},{"text":"]"}]
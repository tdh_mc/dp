# On affiche les infos de tous les PID taggés par le tag tchInfostationPID,
# dans l'ordre alphanumérique (= du plus petit au plus grand idLine)

# On commence par trouver le minimum actuel
scoreboard players set #tchInfostation idLine 9999999
execute as @e[type=item_frame,tag=tchInfostationPID] run scoreboard players operation #tchInfostation idLine < @s idLine

# On affiche ensuite tous ces PID, avec une ligne différente par PID
# (la fonction enlève elle-même le tag tchInfostationPID)
execute as @e[type=item_frame,tag=tchInfostationPID] if score @s idLine = #tchInfostation idLine run function tch:station/debug/info/pid/pid

# S'il reste des PID à itérer, on recommence la fonction
execute if entity @e[type=item_frame,tag=tchInfostationPID,limit=1] run function tch:station/debug/info/pid/tri
# On affiche le nom du SACD, ainsi que son état actuel

execute store result score #tchInfostation temp2 run data get entity @s Item.tag.CustomModelData
execute if score #tchInfostation temp2 matches ..999 run function tch:station/debug/info/pid/sacd
execute if score #tchInfostation temp2 matches 1000.. run function tch:station/debug/info/pid/sacd2

# On retire notre tag pour ne pas repasser dans cette fonction
tag @s remove tchInfostationPID
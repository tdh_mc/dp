# On affiche le nom du distributeur, ainsi que son état actuel

# S'il est libre, on l'affiche en vert
execute as @s[tag=!Occupe] run tellraw @p[tag=tchInfostationJoueur] [{"text":"- ","color":"green","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}},{"selector":"@s"},{"text":" [LIBRE]","bold":"true"}]
# Sinon, on l'affiche en rouge
execute as @s[tag=Occupe] run tellraw @p[tag=tchInfostationJoueur] [{"text":"- ","color":"red","hoverEvent":{"action":"show_text","contents":[{"text":"Pos :\n"},{"entity":"@s","nbt":"Pos[0]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[1]"},{"text":"\n"},{"entity":"@s","nbt":"Pos[2]"}]}},{"selector":"@s"},{"text":" [OCCUPÉ]","bold":"true"}]
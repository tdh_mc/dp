# Enregistrement de l'ID de station actuel (la station la plus proche)
scoreboard players operation #tchInfostation idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation

# On enregistre le nom de la station dans le storage
execute as @e[type=item_frame,tag=tchStationPC] if score @s idStation = #tchInfostation idStation run data modify storage tch:infostation station set from entity @s Item.tag.display.station
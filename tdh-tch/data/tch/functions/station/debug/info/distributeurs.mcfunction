# Affichage des infos détaillées des valideurs

# On tag d'abord tous les valideurs de la station,
# ainsi que le joueur (pour pouvoir lui afficher le texte en faisant execute as)
function tch:station/debug/info/id_station
function tch:station/debug/tag/distributeurs
tag @s add tchInfostationJoueur

# Titre de l'écran d'info
tellraw @s [{"text":"La station ","color":"gold"},{"selector":"@e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1]"},{"text":" comprend "},{"storage":"tch:infostation","nbt":"distributeur.n","bold":"true","extra":[{"text":" distributeurs"}]},{"text":" :"}]
tellraw @s [{"text":"-----------","color":"gray"}]


# On affiche les distributeurs avec des infos sur leur utilisation en cours ou non
execute as @e[type=item_frame,tag=tchInfostationDistrib] at @s run function tch:station/debug/info/distributeurs/distributeur


# On retire tous les tags temporaires
function tch:station/debug/untag/distributeurs
tag @s remove tchInfostationJoueur
# La variable #tchInfostation temp doit déjà avoir été définie à 0 avant cette fonction récursive
# On doit également être à la position d'une item frame TexteTemporaire et donc d'un panneau

# On l'incrémente à chaque coup de 1
scoreboard players add #tchInfostation temp 1

# On enregistre les morceaux de texte dont on a besoin dans le storage
scoreboard players operation #tchInfostation idLine = @s idLine
execute if score #tchInfostation temp matches ..9 run data modify storage tch:infostation porte_quai.id.padding set value "-00"
execute if score #tchInfostation temp matches 10..99 run data modify storage tch:infostation porte_quai.id.padding set value "-0"
execute if score #tchInfostation temp matches 100.. run data modify storage tch:infostation porte_quai.id.padding set value "-"

# On "fixe" un nom approprié pour cette porte de quai
data modify block ~ ~ ~ Text1 set value '[{"text":"PQ "},{"storage":"tch:infostation","nbt":"station.code"},{"text":"-"},{"score":{"name":"#tchInfostation","objective":"idLine"}},{"storage":"tch:infostation","nbt":"porte_quai.id.padding"},{"score":{"name":"#tchInfostation","objective":"temp"}}]'

# On enregistre le nom généré
data modify entity @s CustomName set from block ~ ~ ~ Text1
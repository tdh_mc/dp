# On veut trier les noms des spawners passant dans cette station dans le bon ordre avant de les afficher
# On va exécuter cette fonction de façon répétée, en ajoutant la ligne à l'ID le plus bas à chaque itération

# Tous les spawners que l'on veut traiter ont le tag tchInfostationSpawnerTemp,
# que l'on va leur retirer lorsqu'on les ajoutera à cette liste

# On commence par calculer l'ID le plus bas
scoreboard players set #tchInfostation idLine 9999999
execute as @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawnerTemp] run scoreboard players operation #tchInfostation idLine < @s idLine

# On exécute ensuite la suite des opérations en tant que le spawner ayant cet ID
# On souhaite afficher ID de la ligne + direction, donc on se place à la position d'une item frame TexteTemporaire pour pouvoir "fixer" le texte avant de l'ajouter au storage
execute as @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawnerTemp] if score @s idLine = #tchInfostation idLine at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:station/debug/display/spawner/nom

# S'il reste des entités ayant le tag, on répète cette fonction
execute if entity @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawnerTemp,limit=1] run function tch:station/debug/display/tri_spawners
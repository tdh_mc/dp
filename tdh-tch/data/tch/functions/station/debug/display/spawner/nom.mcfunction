# On est à la position d'une item frame TexteTemporaire (et donc d'un panneau)

# On récupère notre PC
function tch:tag/pc

# On "fixe" le texte de la forme "Nom de la ligne + Nom de la direction"
tag @s add ourDirectionTemp
data modify block ~ ~ ~ Text1 set value '[{"entity":"@e[type=item_frame,tag=ourPC,limit=1]","nbt":"Item.tag.display.Name","interpret":"true"},{"text":"→","color":"gray"},{"entity":"@e[type=#tch:marqueur/spawner,tag=ourDirectionTemp,limit=1]","nbt":"CustomName","interpret":"true"}]'

# On ajoute le nom calculé au storage (pour pouvoir en afficher beaucoup en une seule ligne)
data modify storage tch:infostation spawner.liste append from block ~ ~ ~ Text1

# On retire notre tag pour ne pas repasser dans cette fonction, ainsi que le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
tag @s remove tchInfostationSpawnerTemp
tag @s remove ourDirectionTemp
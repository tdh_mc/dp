# La variable #tchInfostation temp doit déjà avoir été définie à 0 avant cette fonction récursive
# On doit également être à la position d'une item frame TexteTemporaire et donc d'un panneau

# On l'incrémente à chaque coup de 1, et on "fixe" un nom approprié pour ce valideur
scoreboard players add #tchInfostation temp 1
execute if score #tchInfostation temp matches ..9 run data modify block ~ ~ ~ Text1 set value '[{"text":"Distributeur D-"},{"storage":"tch:infostation","nbt":"station.code"},{"text":"-00"},{"score":{"name":"#tchInfostation","objective":"temp"}}]'
execute if score #tchInfostation temp matches 10..99 run data modify block ~ ~ ~ Text1 set value '[{"text":"Distributeur D-"},{"storage":"tch:infostation","nbt":"station.code"},{"text":"-0"},{"score":{"name":"#tchInfostation","objective":"temp"}}]'
execute if score #tchInfostation temp matches 100.. run data modify block ~ ~ ~ Text1 set value '[{"text":"Distributeur D-"},{"storage":"tch:infostation","nbt":"station.code"},{"text":"-"},{"score":{"name":"#tchInfostation","objective":"temp"}}]'

# On enregistre le nom généré
data modify entity @s CustomName set from block ~ ~ ~ Text1
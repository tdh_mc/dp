# On suppose avoir déjà enregistré #tchInfostation idStation (dans la fonction */debug/info/id_station*)

# On liste les portes de quai existantes, ainsi que les directions existantes
# Pour chaque direction, si on ne trouve pas de porte de quai ayant le même ID de ligne et station,
# on l'ajoute à une liste qu'on affichera avec un message d'erreur sur l'écran principal d'infostation
data modify storage tch:infostation porte_quai.manquantes set value []
execute as @e[type=#tch:marqueur/quai,tag=tchInfostationDirection] run function tch:station/debug/display/portes_quai/direction

# On calcule le message à afficher :
# Par défaut il n'y a pas de portes de quai, donc un message d'erreur générique
scoreboard players set #tchInfostation temp4 0
# S'il y a au moins 1 item frame porte de quai dans cette station, on a un message sans erreur
execute if entity @e[type=item_frame,tag=tchInfostationPorteQuai,limit=1] run scoreboard players set #tchInfostation temp4 1
# Si dans ce second cas, il y a au moins une direction manquante, on affiche un message d'erreur avec lesdites directions manquantes
execute if score #tchInfostation temp4 matches 1 if data storage tch:infostation porte_quai.manquantes[0] run scoreboard players set #tchInfostation temp4 2

# On enregistre le bon message dans le storage
# [0] Aucune porte de quai dans cette station
execute if score #tchInfostation temp4 matches 0 run data modify storage tch:infostation porte_quai.texte set value '[{"text":"Aucune porte de quai","bold":"true","color":"red"},{"text":" n\'existe dans cette station.","bold":"false"}]'
# [1] X portes de quai dans cette station
execute if score #tchInfostation temp4 matches 1 run data modify storage tch:infostation porte_quai.texte set value '[{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/portes_quai"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des portes de quai"},"text":"Il y a ","color":"gray"},{"storage":"tch:infostation","nbt":"porte_quai.n","color":"gold","extra":[{"text":" portes de quai"}]},{"text":" dans cette station."}]'
# [2] X lignes n'ont pas de portes de quai
execute if score #tchInfostation temp4 matches 2 run function tch:station/debug/display/portes_quai/afficher_manquantes

# On reset la variable temporaire
scoreboard players reset #tchInfostation temp4
# On veut trier les noms de lignes passant dans cette station dans le bon ordre avant de les afficher
# On va exécuter cette fonction de façon répétée, en ajoutant la ligne à l'ID le plus bas à chaque itération

# Toutes les lignes que l'on veut traiter ont le tag tchInfostationLigneTemp,
# que l'on va leur retirer lorsqu'on les ajoutera à cette liste

# On commence par calculer l'ID le plus bas
scoreboard players set #tchInfostation idLine 9999999
execute as @e[type=#tch:marqueur/quai,tag=tchInfostationLigneTemp] run scoreboard players operation #tchInfostation idLine < @s idLine

# On exécute ensuite la suite des opérations en tant que la ligne ayant cet ID
execute as @e[type=#tch:marqueur/quai,tag=tchInfostationLigneTemp] if score @s idLine = #tchInfostation idLine run function tch:station/debug/display/ligne/nom

# S'il reste des entités ayant le tag, on répète cette fonction
execute if entity @e[type=#tch:marqueur/quai,tag=tchInfostationLigneTemp,limit=1] run function tch:station/debug/display/tri_lignes
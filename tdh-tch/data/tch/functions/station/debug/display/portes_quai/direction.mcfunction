# On enregistre notre ID de ligne
scoreboard players operation #tchInfostation idLine = @s idLine

# Si on ne trouve pas de porte de quai ayant le même ID de ligne que nous, on ajoute le nom complet de notre ID de ligne (ligne + direction) à la liste des portes de quai manquantes dans le storage
scoreboard players set #tchInfostation temp4 1
execute as @e[type=item_frame,tag=tchInfostationPorteQuai] if score @s idLine = #tchInfostation idLine run scoreboard players set #tchInfostation temp4 0

execute if score #tchInfostation temp4 matches 1 run function tch:station/debug/display/portes_quai/direction_manquante
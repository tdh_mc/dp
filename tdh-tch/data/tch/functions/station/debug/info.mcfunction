# INFOSTATION V2

#########################################################################
# Mise à jour des variables et autres informations avant l'affichage
# L'ordre de ces diverses opérations est important pour éviter de devoir faire plusieurs /infostation
# pour récupérer des infos à jour :
# - [C] dépend de [A] et [B]
# - [D] dépend de [A] et [C]
# - [E] dépend de [A] et [C]
#########################################################################

# [A] ID/noms de station (armor stand "NomStation")
execute as @e[type=#tch:marqueur/station,tag=NomStation,distance=..200] at @s run function tch:station/debug/update/nom_station

# [B] ID/noms de directions (armor stands "Direction" et spawners)
execute as @e[type=#tch:marqueur/quai,tag=Direction,distance=..200] at @s run function tch:station/debug/update/direction
execute as @e[type=#tch:marqueur/spawner,tag=SpawnMetro,distance=..200] at @s run function tch:station/debug/update/direction
execute as @e[type=#tch:marqueur/spawner,tag=SpawnRER,distance=..200] at @s run function tch:station/debug/update/direction

# [C] ID/noms de ligne (armor stands "NumLigne" et "PorteQuai")
execute as @e[type=#tch:marqueur/quai,tag=NumLigne,distance=..200] at @s run function tch:station/debug/update/num_ligne
execute as @e[type=item_frame,tag=PorteQuai,distance=..200] at @s run function tch:station/debug/update/porte_quai

# [D] ID de diffusion des haut-parleurs
execute as @e[type=item_frame,tag=tchHP,distance=..200] at @s run function tch:station/annonce/haut_parleur/test_variables

# [E] ID de station d'éléments divers (sorties, PID...)
execute as @e[type=item_frame,tag=tchEntrance,distance=..200] at @s run function tch:station/debug/update/maj_id/station_proche
execute as @e[type=item_frame,tag=PID,distance=..200] at @s run function tch:station/debug/update/maj_id/station_proche
execute as @e[type=item_frame,tag=Distributeur,distance=..200] at @s run function tch:station/debug/update/maj_id/station_proche
execute as @e[type=item_frame,tag=ValideurSilver,distance=..200] at @s run function tch:station/debug/update/maj_id/station_proche
execute as @e[type=item_frame,tag=ValideurGold,distance=..200] at @s run function tch:station/debug/update/maj_id/station_proche

#########################################################################



#########################################################################
# Calcul des informations à afficher (on les enregistre dans un storage)
#########################################################################

# Enregistrement de l'ID de station actuel (la station la plus proche)
function tch:station/debug/info/id_station

# Nombre d'entrées et de sorties
function tch:station/debug/tag/entrees_sorties
# Nombre de PID
function tch:station/debug/tag/pid
# Nombre de haut-parleurs
function tch:station/debug/tag/hp
# Nombre de distributeurs
function tch:station/debug/tag/distributeurs
# Nombre de portes de quai
function tch:station/debug/tag/portes_quai
# Nombre de valideurs Gold et Silver
function tch:station/debug/tag/valideurs
# Nombre de lignes (NumLigne) + Nombre de directions (+directions terminus)
function tch:station/debug/tag/lignes_directions
# Nombre de spawners
function tch:station/debug/tag/spawners

# On nomme toutes les entités ayant des conventions de nommage automatiques
# - Les haut-parleurs
scoreboard players set #tchInfostation temp 0
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchInfostationHP] run function tch:station/debug/display/nom_hp
# - Les PID / SACD
scoreboard players set #tchInfostation temp 0
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchInfostationPID] run function tch:station/debug/display/nom_sacd
# - Les distributeurs
scoreboard players set #tchInfostation temp 0
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchInfostationDistrib] run function tch:station/debug/display/nom_distributeur
# - Les valideurs
scoreboard players set #tchInfostation temp 0
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchInfostationValideurG] run function tch:station/debug/display/nom_valideur_gold
scoreboard players set #tchInfostation temp 0
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchInfostationValideurS] run function tch:station/debug/display/nom_valideur_silver
# - Les portes de quai
scoreboard players set #tchInfostation temp 0
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchInfostationPorteQuai] run function tch:station/debug/display/nom_porte_quai

# On prépare certains messages à afficher
# Entrées/sorties
function tch:station/debug/display/entrees_sorties
# Valideurs Silver/Gold
function tch:station/debug/display/valideurs
# Portes de quai
function tch:station/debug/display/portes_quai

# Calcul de l'ordre d'affichage des lignes et des spawners
# Pour chacune de ces catégories, cela se passe dans une fonction récursive qui ajoute à la liste
# l'entité restante ayant l'ID de ligne le plus bas
data modify storage tch:infostation ligne.liste set value []
tag @e[type=#tch:marqueur/quai,tag=tchInfostationLigne] add tchInfostationLigneTemp
function tch:station/debug/display/tri_lignes

data modify storage tch:infostation spawner.liste set value []
tag @e[type=#tch:marqueur/spawner,tag=tchInfostationSpawner] add tchInfostationSpawnerTemp
function tch:station/debug/display/tri_spawners

#########################################################################



#########################################################################
# Affichage des informations
#########################################################################

# On affiche ensuite uniquement les informations importantes ; pour avoir plus de détails sur chacune des catégories, il faudra cliquer dessus pour afficher les informations complètes
tellraw @s [{"clickEvent":{"action":"run_command","value":"/function tch:itineraire/debug/_station_proche"},"hoverEvent":{"action":"show_text","contents":"Accéder aux infos de cette station\ndans le système d'itinéraire"},"text":"Informations de la station ","color":"gold"},{"selector":"@e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1]"}]
tellraw @s [{"text":"-----------","color":"gray"}]

# Nombre d'entrées et de sorties
tellraw @s [{"storage":"tch:infostation","nbt":"acces.texte","interpret":"true"}]
# Nombre de portes de quai
tellraw @s [{"storage":"tch:infostation","nbt":"porte_quai.texte","interpret":"true"}]
# Nombre de valideurs
tellraw @s [{"storage":"tch:infostation","nbt":"valideur.texte","interpret":"true"}]

# Toutes les catégories suivantes ne sont affichées que s'il y a au moins 1 élément de cette catégorie
execute if entity @e[type=item_frame,tag=tchInfostationDistrib,limit=1] run tellraw @s [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/distributeurs"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des distributeurs"},"text":"Il y a ","color":"gray"},{"storage":"tch:infostation","nbt":"distributeur.n","color":"gold","extra":[{"text":" distributeurs"}]},{"text":"."}]
execute if entity @e[type=item_frame,tag=tchInfostationPID,limit=1] run tellraw @s [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/pid"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des PID"},"text":"Il y a ","color":"gray"},{"storage":"tch:infostation","nbt":"pid.n","color":"gold","extra":[{"text":" SACD"}]},{"text":"."}]
execute if entity @e[type=item_frame,tag=tchInfostationHP,limit=1] run tellraw @s [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/hp"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des haut-parleurs"},"text":"Il y a ","color":"gray"},{"storage":"tch:infostation","nbt":"hp.n","color":"gold","extra":[{"text":" haut-parleurs"}]},{"text":"."}]
tellraw @s [{"text":"-----------","color":"gray"}]

# On affiche enfin les informations (condensées) des lignes et des spawners
execute store result score @s temp run data get storage tch:infostation ligne.liste
tellraw @s[scores={temp=0}] [{"text":"Aucune ligne","color":"red","bold":"true"},{"text":" ne passe dans cette station.","bold":"false"}]
tellraw @s[scores={temp=1}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"text":"La ligne ","color":"gray"},{"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":" dessert cette station."}]
tellraw @s[scores={temp=2}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"text":"Les lignes ","color":"gray"},{"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":" desservent cette station."}]
tellraw @s[scores={temp=3}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"text":"Les lignes ","color":"gray"},{"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":" desservent cette station."}]
tellraw @s[scores={temp=4}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"text":"Les lignes ","color":"gray"},{"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[3]","interpret":"true"},{"text":" desservent cette station."}]
tellraw @s[scores={temp=5..}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"score":{"name":"@s","objective":"temp"},"color":"gold","bold":"true","extra":[{"text":" lignes"}]},{"text":" desservent cette station :","bold":"false"}]
tellraw @s[scores={temp=5}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[3]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[4]","interpret":"true"}]
tellraw @s[scores={temp=6}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[4]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[5]","interpret":"true"}]
tellraw @s[scores={temp=7}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[5]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[6]","interpret":"true"}]
tellraw @s[scores={temp=8}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[5]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[6]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[7]","interpret":"true"}]
tellraw @s[scores={temp=9}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[5]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[6]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[7]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[8]","interpret":"true"}]
tellraw @s[scores={temp=10}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[5]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[6]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[7]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[8]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"ligne.liste[9]","interpret":"true"}]
tellraw @s[scores={temp=11..}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/lignes"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des lignes"},"storage":"tch:infostation","nbt":"ligne.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[5]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[6]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[7]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[8]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"ligne.liste[9]","interpret":"true"},{"text":" et d'autres encore..."}]

# On affiche les informations (condensées) des spawners
execute store result score @s temp run data get storage tch:infostation spawner.liste
tellraw @s[scores={temp=0}] [{"text":"Il n'y a ","color":"red"},{"text":"aucun spawner","bold":"true"},{"text":" dans cette station."}]
tellraw @s[scores={temp=1}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"text":"Spawners : ","color":"gray"},{"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":"."}]
tellraw @s[scores={temp=2}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"text":"Spawners : ","color":"gray"},{"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":"."}]
tellraw @s[scores={temp=3}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"text":"Spawners : ","color":"gray"},{"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":"."}]
tellraw @s[scores={temp=4}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"text":"Spawners : ","color":"gray"},{"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[3]","interpret":"true"},{"text":"."}]
tellraw @s[scores={temp=5..}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":[{"storage":"tch:infostation","nbt":"spawner.n_metro"},{"text":" spawners métro, "},{"storage":"tch:infostation","nbt":"spawner.n_rer"},{"text":" spawners RER, et "},{"storage":"tch:infostation","nbt":"spawner.n_cable"},{"text":" spawners câble."}]},"text":"Il y a ","color":"gold"},{"score":{"name":"@s","objective":"temp"},"bold":"true","extra":[{"text":" spawners"}]},{"text":" :","bold":"false"}]
tellraw @s[scores={temp=5}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[3]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[4]","interpret":"true"}]
tellraw @s[scores={temp=6}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[4]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[5]","interpret":"true"}]
tellraw @s[scores={temp=7}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[5]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[6]","interpret":"true"}]
tellraw @s[scores={temp=8}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[5]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[6]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[7]","interpret":"true"}]
tellraw @s[scores={temp=9}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[5]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[6]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[7]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[8]","interpret":"true"}]
tellraw @s[scores={temp=10}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[5]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[6]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[7]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[8]","interpret":"true"},{"text":" et "},{"storage":"tch:infostation","nbt":"spawner.liste[9]","interpret":"true"}]
tellraw @s[scores={temp=11..}] [{"clickEvent":{"action":"run_command","value":"/function tch:station/debug/info/spawners"},"hoverEvent":{"action":"show_text","contents":"Afficher la liste des spawners"},"storage":"tch:infostation","nbt":"spawner.liste[0]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[1]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[2]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[3]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[4]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[5]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[6]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[7]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[8]","interpret":"true"},{"text":", "},{"storage":"tch:infostation","nbt":"spawner.liste[9]","interpret":"true"},{"text":" et d'autres encore..."}]


#########################################################################


# Suppression des tags temporaires tchInfostation
function tch:station/debug/untag/all
scoreboard players reset @s temp
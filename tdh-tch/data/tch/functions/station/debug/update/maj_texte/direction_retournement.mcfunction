# On récupère la bonne station du système d'itinéraire (correspondant à l'ID de prochaine ligne de l'entité qui exécute cette fonction)
function tch:tag/terminus_pc_next
# On récupère également le PC de ligne, qui contient la couleur appropriée
function tch:tag/pc_next

# On récupère le nom de la station terminus, que l'on stocke dans un storage
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.station.nom
# On affiche sur un panneau le champ approprié (avec la bonne couleur) depuis notre PC de ligne, pour le "rendre" avec le bon nom station (stocké dans le storage ci-dessus), puis on le copie dans notre propre nom
execute if entity @e[type=item_frame,tag=ourPC,limit=1] if entity @e[type=item_frame,tag=ourTerminusPC,limit=1] at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:station/debug/update/maj_texte/direction_copie

# On supprime le tag de notre PC
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
tag @e[type=item_frame,tag=ourPC] remove ourPC
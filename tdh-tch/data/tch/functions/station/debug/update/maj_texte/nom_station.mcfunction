# On récupère la bonne station du système d'itinéraire (correspondant à l'entité qui exécute cette fonction)
function tch:tag/station_pc

# On met à jour le nom de cette entité avec le nom de la station dans le système d'itinéraire
data modify entity @s CustomName set from entity @e[type=item_frame,tag=ourStationPC,limit=1] Item.tag.display.Name

# On supprime le tag de notre PC
tag @e[type=item_frame,tag=ourStationPC] remove ourStationPC
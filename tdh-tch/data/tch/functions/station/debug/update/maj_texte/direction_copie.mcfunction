# On exécute cette fonction à la suite de *direction*,
# mais à la position d'un panneau TexteTemporaire (pour le rendu du texte)

# On copie dans les données de ce bloc (le panneau) le texte requis
data modify block ~ ~ ~ Text1 set from entity @e[type=item_frame,tag=ourPC,limit=1] Item.tag.display.line.terminus

# On met à jour le nom de cette entité avec le nom de la station dans le système d'itinéraire
data modify entity @s CustomName set from block ~ ~ ~ Text1
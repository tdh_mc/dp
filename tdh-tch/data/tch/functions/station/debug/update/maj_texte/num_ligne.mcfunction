# On récupère la bonne ligne du système d'itinéraire (correspondant à l'entité qui exécute cette fonction)
function tch:tag/pc

# On met à jour le nom de cette entité avec le nom de la ligne dans le système d'itinéraire
data modify entity @s CustomName set from entity @e[type=item_frame,tag=ourPC,limit=1] Item.tag.display.Name

# On supprime le tag de notre PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
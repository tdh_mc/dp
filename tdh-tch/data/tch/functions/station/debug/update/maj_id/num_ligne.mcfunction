# On exécute cette fonction en tant qu'une armor stand NumLigne,
# pour récupérer notre ID line min/max

# On donne un tag temporaire à toutes les directions très proches de nous
tag @e[type=#tch:marqueur/quai,tag=Direction,distance=..5] add tchDirectionTemp
# Si on n'a pas trouvé au moins 1 direction, on prend les 2 plus proches
execute unless entity @e[type=#tch:marqueur/quai,tag=tchDirectionTemp,limit=1] run tag @e[type=#tch:marqueur/quai,tag=Direction,distance=..75,sort=nearest,limit=2] add tchDirectionTemp

# Si on a trouvé au moins 2 directions, on calcule le nouvel ID de ligne
execute store result score #tchInfostation temp5 if entity @e[type=#tch:marqueur/quai,tag=tchDirectionTemp]
execute if score #tchInfostation temp5 matches 2.. run function tch:station/debug/update/maj_id/num_ligne_directions

tag @e[type=#tch:marqueur/quai,tag=tchDirectionTemp] remove tchDirectionTemp
scoreboard players reset #tchInfostation temp5
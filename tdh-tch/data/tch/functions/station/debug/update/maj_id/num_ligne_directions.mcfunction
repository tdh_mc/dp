# On calcule l'ID min/max de ces directions
scoreboard players set #tchInfostation idLine 9999999
scoreboard players set #tchInfostation idLineMax 0
execute as @e[type=#tch:marqueur/quai,tag=tchDirectionTemp] run function tch:station/debug/update/maj_id/num_ligne_direction

# On stocke les valeurs calculées ssi elles sont différentes de leurs valeurs par défaut
execute unless score #tchInfostation idLine matches 9999999.. run scoreboard players operation @s idLine = #tchInfostation idLine
execute unless score #tchInfostation idLineMax matches 0 run scoreboard players operation @s idLineMax = #tchInfostation idLineMax

# On réinitialise les variables temporaires
scoreboard players reset #tchInfostation temp
scoreboard players reset #tchInfostation idLine
scoreboard players reset #tchInfostation idLineMax

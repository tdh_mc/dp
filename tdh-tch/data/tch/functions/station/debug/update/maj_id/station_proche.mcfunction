# On met à jour une entité TCH quelconque, qui exécute cette fonction à sa position

# Si elle n'a pas encore d'ID station, elle prend celui de la station la plus proche
execute unless score @s idStation matches 1.. run scoreboard players operation @s idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation
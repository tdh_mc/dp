# On exécute cette fonction pour chaque direction proche,
# afin de récupérer l'ID line minimum et maximum de l'ensemble de ces directions

scoreboard players operation #tchInfostation idLine < @s idLine
scoreboard players operation #tchInfostation idLineMax > @s idLine
scoreboard players operation #tchInfostation idLineMax > @s idLineMax

# On retire notre tag pour ne pas repasser dans cette fonction
tag @s remove tchDirectionTemp
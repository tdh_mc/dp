# On met à jour une entité TCH quelconque, qui exécute cette fonction à sa position

# Si elle n'a pas encore d'ID line, elle prend celui de la direction la plus proche
execute unless score @s idLine matches 1.. run scoreboard players operation @s idLine = @e[type=#tch:marqueur/quai,tag=Direction,distance=..200,sort=nearest,limit=1] idLine
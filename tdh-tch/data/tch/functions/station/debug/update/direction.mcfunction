# On met à jour une armor stand Direction, qui exécute cette fonction à sa position

# Si on a déjà un score idLine, on met à jour notre nom depuis le système d'itinéraire
# On utilise le score prochaineLigne à la place si on en possède un
execute if score @s idLine matches 1.. unless score @s prochaineLigne matches 1.. run function tch:station/debug/update/maj_texte/direction
execute if score @s idLine matches 1.. if score @s prochaineLigne matches 1.. run function tch:station/debug/update/maj_texte/direction_retournement
# Si on n'en a pas encore, on récupère notre score idLine
execute unless score @s idLine matches 1.. run function tch:auto/get_id_line

# Si on n'a pas de score idStation, on le met à jour selon celui de la station la plus proche
function tch:station/debug/update/maj_id/station_proche
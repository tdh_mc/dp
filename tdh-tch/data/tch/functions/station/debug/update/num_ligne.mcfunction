# On met à jour une armor stand NumLigne, qui exécute cette fonction à sa position

# Si on a déjà un score idLine, on met à jour notre nom depuis le système d'itinéraire
execute if score @s idLine matches 1.. run function tch:station/debug/update/maj_texte/num_ligne
# Si on n'en a pas encore, on checke les Direction situées à proximité immédiate
execute unless score @s idLine matches 1.. run function tch:station/debug/update/maj_id/num_ligne

# Si on n'a pas de score idStation, on le met à jour selon celui de la station la plus proche
function tch:station/debug/update/maj_id/station_proche
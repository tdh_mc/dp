# Téléportation de tous les joueurs se trouvant sur une voie sur le quai correspondant
# Cas est-ouest et station non inversée (voies au centre)
# Cette fonction est exécutée par le "NumLigne" de ce quai à sa position

## On tag tous les joueurs se trouvant sur une voie et pas dans un minecart
# Une station "standard" fait 7 de large, et cela n'est jamais censé changer (le numligne est aussi toujours précisément au milieu)
# Une station "standard" fait 32 blocs de long, mais le NumLigne n'est pas toujours précisément au milieu
# Par sécurité, on prend donc 7 blocs de large et 48 blocs de long (suffisamment pour toujours sélectionner tout le quai si le Numligne est à 8-24)
execute positioned ~-24 ~2 ~-3.66 run tag @a[dx=48,dy=4,dz=6.66,gamemode=!spectator,tag=!MetroWorker] add JoueurSurLaVoie
execute at @a[tag=JoueurSurLaVoie] if entity @e[type=minecart,distance=...5] run tag @p[tag=JoueurSurLaVoie] remove JoueurSurLaVoie

# On téléporte chaque joueur sur la voie à la position de la porte de quai la plus proche
execute at @a[tag=JoueurSurLaVoie] run function tch:station/quai/tp_voie/tp_joueur

# On supprime les tags temporaires
tag @a[tag=JoueurSurLaVoie] remove JoueurSurLaVoie
# Fonction récursive exécutée à la position d'un joueur ayant le tag tempJSLV
# Tant qu'il n'est pas dans un bloc passable (#tdh:passable)

# On tp le joueur 1 bloc vers le haut
tp @p[tag=tempJSLV] ~ ~1 ~ ~ ~

# S'il est toujours dans un bloc, on recommence
execute at @p[tag=tempJSLV] unless block ~ ~ ~ #tdh:passable run function tch:station/quai/tp_voie/tp_joueur_up
# Téléportation du joueur au niveau du quai le plus proche
# Exécuté à la position d'un joueur ayant le tag JoueurSurLaVoie

# On tag ce joueur précisément pour le retrouver quand on aura changé de position
tag @p[tag=JoueurSurLaVoie] add tempJSLV

# On tag la direction la plus proche pour pouvoir détecter la 2e plus proche
tag @e[tag=Direction,distance=..50,sort=nearest,limit=1] add ourDirection

# On récupère la perpendiculaire des voies, puis on tp le joueur sur cet axe
execute at @e[tag=ourDirection,limit=1] facing entity @e[tag=Direction,tag=!ourDirection,sort=nearest,limit=1] feet positioned as @p[tag=tempJSLV] align y run tp @p[tag=tempJSLV] ^ ^1 ^-3.5 ~180 0

# Si le joueur est dans un bloc, on le tp vers le haut jusqu'à ce qu'il ne le soit plus
execute at @p[tag=tempJSLV] unless block ~ ~ ~ #tdh:passable run function tch:station/quai/tp_voie/tp_joueur_up

# On supprime les tags temporaires
tag @p[tag=tempJSLV] remove tempJSLV
tag @e[tag=ourDirection,distance=..50] remove ourDirection
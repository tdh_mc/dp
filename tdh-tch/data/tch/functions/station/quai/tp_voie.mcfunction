# Téléportation de tous les joueurs se trouvant sur une voie sur le quai correspondant
# Cette fonction est exécutée par le "NumLigne" de ce quai à sa position

# On vérifie si le quai est orienté nord/sud ou est/ouest
execute at @e[tag=Direction,sort=nearest,limit=1] positioned ~-3 ~ ~ if entity @e[tag=ProchaineStation,dx=6,dy=0,dz=0] run tag @s add EstOuest

# Selon notre orientation et si les quais sont inversés ou non, on exécute la fonction de tp correspondante
execute as @s[tag=!EstOuest] unless entity @e[distance=..5,tag=StationInversee] run function tch:station/quai/tp_voie/nord_sud
execute as @s[tag=EstOuest] unless entity @e[distance=..5,tag=StationInversee] run function tch:station/quai/tp_voie/est_ouest
execute as @s[tag=!EstOuest] if entity @e[distance=..5,tag=StationInversee] run function tch:station/quai/tp_voie/nord_sud_inv
execute as @s[tag=EstOuest] if entity @e[distance=..5,tag=StationInversee] run function tch:station/quai/tp_voie/est_ouest_inv

# On réinitialise notre tag
tag @s remove EstOuest
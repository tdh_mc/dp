# On calcule la valeur de l'heure de fermeture
# Non modifiable manuellement, elle dépend des heures de service des lignes qui y passent


# Pour chaque ligne qui passe ici, on vérifie si son heure d'ouverture est plus tôt que l'actuelle
# On tag d'abord le PC de chaque ligne
execute at @s as @e[type=armor_stand,tag=NumLigne,distance=..150] run function tch:tag/pc

# On initialise les variables
scoreboard players set #temp finServiceAjd 0
# On lit ensuite l'heure d'ouverture aujourd'hui de chaque ligne, et on prend à chaque fois le maximum
execute unless entity @e[type=item_frame,tag=ourPC,tag=!HorsService,scores={finServiceAjd=..7999}] as @e[type=item_frame,tag=ourPC] run scoreboard players operation #temp finServiceAjd > @s finServiceAjd
execute as @e[type=item_frame,tag=ourPC,scores={finServiceAjd=..7999}] run scoreboard players operation #temp finServiceAjd > @s finServiceAjd

# On enregistre l'horaire calculé
scoreboard players operation @s finServiceAjd = #temp finServiceAjd
scoreboard players reset #temp finServiceAjd

# On supprime le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
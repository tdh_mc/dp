# TICK DES STATIONS
# Si une station fermée n'a pas d'heure d'ouverture, on la calcule
execute as @e[type=armor_stand,tag=NomStation,tag=!StationOuverte] unless score @s debutServiceAjd matches 0.. run function tch:station/calcul_ouverture_station

# Tests d'ouverture
# Pour chaque station fermée ayant un horaire d'ouverture, on fait le test
# On n'ouvre que s'il y a un joueur à proximité
execute at @a as @e[type=armor_stand,tag=NomStation,tag=!StationOuverte,distance=..150,scores={debutServiceAjd=1..}] run function tch:station/tick/test_ouverture_station
# Pour chaque station ouverte dont le guichet est fermé mais a un horaire d'ouverture, on fait le test
# On n'ouvre que s'il y a un joueur à proximité
execute at @a as @e[type=armor_stand,tag=NomStation,tag=StationOuverte,distance=..125,tag=!GuichetOuvert,scores={debutGuichet=1..}] run function tch:station/tick/test_ouverture_guichet

# Tests de fermeture
# Pour chaque station dont le guichet est ouvert (= tag GuichetOuvert), on fait le test de fermeture
execute as @e[type=armor_stand,tag=NomStation,tag=GuichetOuvert] run function tch:station/tick/test_fermeture_guichet
# Pour chaque station ouverte, on fait le test de fermeture
execute as @e[type=armor_stand,tag=NomStation,tag=StationOuverte] run function tch:station/tick/test_fermeture_station


# Pour chaque station dont le guichet est resté ouvert, on replace les vendeurs à leur place
execute as @e[type=armor_stand,tag=NomStation,tag=GuichetOuvert] at @s run function tch:station/tick/aligner_vendeurs_guichet


# TICK DES DISTRIBUTEURS
# Pour chaque joueur ayant le tag ClientDistributeur, on vérifie s'il est toujours au distributeur
execute at @a[tag=ClientDistributeur] run function tch:station/distributeur/tick_joueur
# Pour chaque distributeur en cours d’impression, on imprime un ticket
execute as @e[type=marker,tag=DistributeurImpression] at @s run function tch:station/distributeur/tick_impression

# TICK DES ENTREES/SORTIES DES JOUEURS
execute at @a[gamemode=!spectator] run function tch:station/entree/tick


# TICK DES VALIDEURS
# On ticke tous les joueurs pour vérifier s’ils sont à proximité d’un valideur (Gold ou Badge)
execute at @a[gamemode=!spectator,tag=!EchecGold,tag=!EchecBadge] run function tch:station/valideur/test_valideur_proche

# Pour chaque joueur en attente du 2ème message "Bon voyage Valideur Gold" (= "Il vous reste XX jours d'abonnement"),
# on vérifie s'il est l'heure de lui afficher le message
execute at @a[tag=BonVoyageGold] run function tch:station/valideur/gold/test_succes2
# Pour chaque joueur en attente du 2ème message "Ticket Périmé Silver" (= "Il a expiré le XX/XX/XXX"),
# on vérifie s'il est l'heure de lui afficher un message
execute at @a[tag=PerimeSilver] run function tch:station/valideur/silver/echec/test_perime2
# Pour chaque joueur en attente du 2ème message "Ticket Valide Silver" (= "Il est valable jusqu'au XX/XX/XXX"), idem
execute at @a[tag=SuccesSilver] run function tch:station/valideur/silver/succes/test_succes2

# Pour chaque joueur ayant eu un échec Gold ou Badge, on reset le tag correspondant s'il s'est éloigné du valideur
execute at @a[tag=EchecGold] unless entity @e[type=item_frame,tag=ValideurGold,distance=..1] run tag @a[distance=0,tag=EchecGold] remove EchecGold
execute at @a[tag=EchecBadge] unless entity @e[type=item_frame,tag=ValideurBadge,distance=..1] run tag @a[distance=0,tag=EchecBadge] remove EchecBadge


# TICK DES QUAIS ET DES VOIES
# Pour chaque armor stand NumLigne si un joueur se trouve à proximité, on fait les calculs détaillés
execute as @e[type=armor_stand,tag=NumLigne] at @s if entity @p[distance=..25] run function tch:station/quai/tick
#RÉGION DU HAMEAU [1000-1099]
execute as @s[name="Le Hameau"] run scoreboard players set @s idStation 1000
execute as @s[name="Le Hameau–Rive Gauche"] run scoreboard players set @s idStation 1000
execute as @s[name="Le Hameau–Glandebruine Nord"] run scoreboard players set @s idStation 1001
execute as @s[name="Le Hameau–Rive Droite"] run scoreboard players set @s idStation 1002
execute as @s[name="Le Hameau–Port"] run scoreboard players set @s idStation 1004

execute as @s[name="Kraken"] run scoreboard players set @s idStation 1010
execute as @s[name="Knosos"] run scoreboard players set @s idStation 1020
execute as @s[name="Verchamps"] run scoreboard players set @s idStation 1030

#RÉPUBLIQUE DE GRENAT [1100-1149]
execute as @s[name="Grenat–Ville"] run scoreboard players set @s idStation 1100
execute as @s[name="Grenat–Port"] run scoreboard players set @s idStation 1102
execute as @s[name="Grenat–Récif"] run scoreboard players set @s idStation 1105
execute as @s[name="Grenat–Arithmatie"] run scoreboard players set @s idStation 1110
execute as @s[name="Asvaard–Rudelieu"] run scoreboard players set @s idStation 1115

#COMTÉ DE BÉOLLONIE [1150-1199]
execute as @s[name="Villonne"] run scoreboard players set @s idStation 1150
execute as @s[name="Forêt de Jëej"] run scoreboard players set @s idStation 1155
execute as @s[name="Béothas"] run scoreboard players set @s idStation 1160
execute as @s[name="Tolbrok"] run scoreboard players set @s idStation 1170
execute as @s[name="Duerrom"] run scoreboard players set @s idStation 1180

#ROYAUME ELFIQUE DE DELIAH [1200-1299]
execute as @s[name="Dorlinor"] run scoreboard players set @s idStation 1200
execute as @s[name="Désert Laar"] run scoreboard players set @s idStation 1205
execute as @s[name="Rives de Laar"] run scoreboard players set @s idStation 1206
execute as @s[name="Ygriak"] run scoreboard players set @s idStation 1210
execute as @s[name="Zobun–Éclesta"] run scoreboard players set @s idStation 1220
execute as @s[name="Évenis–Éclesta"] run scoreboard players set @s idStation 1230
execute as @s[name="Athès"] run scoreboard players set @s idStation 1250
execute as @s[name="La Béhue"] run scoreboard players set @s idStation 1280

#ROYAUME DES SABLONS [1300-1399]
execute as @s[name="Les Sablons"] run scoreboard players set @s idStation 1300
execute as @s[name="Chizân"] run scoreboard players set @s idStation 1320
execute as @s[name="Malarân"] run scoreboard players set @s idStation 1330
execute as @s[name="Désert Onyx"] run scoreboard players set @s idStation 1350
execute as @s[name="Alwin–Onyx"] run scoreboard players set @s idStation 1360
execute as @s[name="Sablons–Brimiel"] run scoreboard players set @s idStation 1370
execute as @s[name="Glandy"] run scoreboard players set @s idStation 1390

#RÉPUBLIQUE DU VAL [1400-1499]
execute as @s[name="Fort du Val"] run scoreboard players set @s idStation 1400
execute as @s[name="Illysia"] run scoreboard players set @s idStation 1420
execute as @s[name="Fultèz-en-Sulûm"] run scoreboard players set @s idStation 1450
execute as @s[name="Pieuze-en-Sulûm"] run scoreboard players set @s idStation 1460
execute as @s[name="Montagnes de Krypt"] run scoreboard players set @s idStation 1490

#RÉPUBLIQUE DE PERBECK [1500-1599]
execute as @s[name="Néronil-le-Pont"] run scoreboard players set @s idStation 1500
execute as @s[name="Évenis–Tamlyn"] run scoreboard players set @s idStation 1520
execute as @s[name="Eïdin-la-Vallée"] run scoreboard players set @s idStation 1530
execute as @s[name="Litoréa"] run scoreboard players set @s idStation 1550
execute as @s[name="Moronia"] run scoreboard players set @s idStation 1590

#VILLE LIBRE D'HADÈS [1600-1699]
execute as @s[name="Hadès"] run scoreboard players set @s idStation 1600
execute as @s[name="Midès"] run scoreboard players set @s idStation 1610
execute as @s[name="Quatre Chemins"] run scoreboard players set @s idStation 1650

#RÉPUBLIQUE DE PAOUPHE [1800-1899]
execute as @s[name="Le Roustiflet"] run scoreboard players set @s idStation 1800
execute as @s[name="Thoroïr–Valdée"] run scoreboard players set @s idStation 1820
execute as @s[name="Thalrion"] run scoreboard players set @s idStation 1850
execute as @s[name="Chassy-sur-Flumine"] run scoreboard players set @s idStation 1860

#COMTÉ DE SAUVEBONNE [1900-1949]
execute as @s[name="Évrocq–Sauvebonne"] run scoreboard players set @s idStation 1900
execute as @s[name="Évrocq–Le Bas"] run scoreboard players set @s idStation 1904
execute as @s[name="Évrocq–Le Haut"] run scoreboard players set @s idStation 1905
execute as @s[name="Île du Singe"] run scoreboard players set @s idStation 1910

#COMMUNAUTÉ AUTONOME DES BALCHAÏS [1950-1999]
execute as @s[name="Le Relais"] run scoreboard players set @s idStation 1950
execute as @s[name="Moldor–Balchaïs"] run scoreboard players set @s idStation 1960
execute as @s[name="Quécol-en-Drie"] run scoreboard players set @s idStation 1980

#RÉGION DE PROCYON [2000-2099]
execute as @s[name="Procyon"] run scoreboard players set @s idStation 2000
execute as @s[name="Procyon–Ténèbres"] run scoreboard players set @s idStation 2001
execute as @s[name="Procyon–Université"] run scoreboard players set @s idStation 2005
execute as @s[name="Aulnoy"] run scoreboard players set @s idStation 2010
execute as @s[name="Fénicia"] run scoreboard players set @s idStation 2020
execute as @s[name="Argençon"] run scoreboard players set @s idStation 2050

#PRINCIPAUTÉ DES GLACES [2100-2199]
execute as @s[name="Frambourg–Ternelieu"] run scoreboard players set @s idStation 2100
execute as @s[name="Géorlie"] run scoreboard players set @s idStation 2120
execute as @s[name="Calmeflot-les-Deux-Berges"] run scoreboard players set @s idStation 2150
execute as @s[name="Milloreau-les-Pics"] run scoreboard players set @s idStation 2160
execute as @s[name="Les Pics"] run scoreboard players set @s idStation 2161
execute as @s[name="La Dodène"] run scoreboard players set @s idStation 2170
execute as @s[name="Bussy-Nigel"] run scoreboard players set @s idStation 2180
execute as @s[name="Crestali"] run scoreboard players set @s idStation 2190

#COMMUNAUTÉS AGRICOLES DE CYSÉAL ET VERNOGLIE-LES-MINES [2200-2299]
execute as @s[name="Cyséal"] run scoreboard players set @s idStation 2200
execute as @s[name="Gallicy-les-Putrides"] run scoreboard players set @s idStation 2210
execute as @s[name="Le Pont-aux-Porcelets"] run scoreboard players set @s idStation 2220
execute as @s[name="Vernoglie-les-Mines"] run scoreboard players set @s idStation 2240
execute as @s[name="XXVillageAV"] run scoreboard players set @s idStation 2250
execute as @s[name="XXVillageAX"] run scoreboard players set @s idStation 2260
execute as @s[name="Port-Putride"] run scoreboard players set @s idStation 2280

#COLONIES SUD-GRENOISES [2300-2399]
execute as @s[name="Oréa-sur-Mer"] run scoreboard players set @s idStation 2300
execute as @s[name="Triel"] run scoreboard players set @s idStation 2310
execute as @s[name="Port-aux-Pendus"] run scoreboard players set @s idStation 2340
execute as @s[name="Thomorgne"] run scoreboard players set @s idStation 2350
execute as @s[name="Bourg-Filou"] run scoreboard players set @s idStation 2360
execute as @s[name="La Gambergerie"] run scoreboard players set @s idStation 2380
execute as @s[name="Poudriole"] run scoreboard players set @s idStation 2390

#CITÉS AUTONOMES DES MONTAGNES DU SUD-OUEST [2400-2499]
execute as @s[name="Le Grand-Dégueulet"] run scoreboard players set @s idStation 2400
execute as @s[name="Le Petit-Dégueulet"] run scoreboard players set @s idStation 2410
execute as @s[name="Vaillebourg"] run scoreboard players set @s idStation 2420
execute as @s[name="Chaugnes"] run scoreboard players set @s idStation 2450
execute as @s[name="Caularçay"] run scoreboard players set @s idStation 2480
execute as @s[name="Paupéry"] run scoreboard players set @s idStation 2490

#ARCHIPEL DU SUD-EST [2500-2599]
execute as @s[name="Mortgorge"] run scoreboard players set @s idStation 2500
execute as @s[name="Lyrane"] run scoreboard players set @s idStation 2510
execute as @s[name="Souciel"] run scoreboard players set @s idStation 2520
execute as @s[name="Pépinor"] run scoreboard players set @s idStation 2550
execute as @s[name="XXVillageCH"] run scoreboard players set @s idStation 2580

#CŒUR DE L'EMPIRE ARDENT [2600-2699]
execute as @s[name="XXVillageCA"] run scoreboard players set @s idStation 2600
execute as @s[name="Padras"] run scoreboard players set @s idStation 2620
execute as @s[name="XXVillageCB"] run scoreboard players set @s idStation 2650

#COLONIES DE L'EMPIRE ARDENT [2700-2799]
execute as @s[name="La Pogne-en-Ardence"] run scoreboard players set @s idStation 2700
execute as @s[name="Castelroc"] run scoreboard players set @s idStation 2710
execute as @s[name="Tilia"] run scoreboard players set @s idStation 2730
execute as @s[name="Le Cap-aux-Ardents"] run scoreboard players set @s idStation 2735
execute as @s[name="Poutaise"] run scoreboard players set @s idStation 2750
execute as @s[name="Taussec"] run scoreboard players set @s idStation 2755
execute as @s[name="Camarel-en-Ardence"] run scoreboard players set @s idStation 2760
execute as @s[name="Concagnol"] run scoreboard players set @s idStation 2770
execute as @s[name="Bouilly-les-Vignes"] run scoreboard players set @s idStation 2790

#RÉGION JSP-6 [2800-2899]
execute as @s[name="XXVillageCL"] run scoreboard players set @s idStation 2800
execute as @s[name="XXVillageBX"] run scoreboard players set @s idStation 2810
execute as @s[name="XXVillageBV"] run scoreboard players set @s idStation 2820
execute as @s[name="XXVillageCQ"] run scoreboard players set @s idStation 2850
execute as @s[name="XXVillageCR"] run scoreboard players set @s idStation 2860

#FÉDÉRATION DE GORGÉOS [2900-2999]
execute as @s[name="XXVillageCX"] run scoreboard players set @s idStation 2900
execute as @s[name="XXVillageCU"] run scoreboard players set @s idStation 2920

#CITÉ AUTONOME DE VENISE [3000-3099]
execute as @s[name="Venise"] run scoreboard players set @s idStation 3000

#RÉGION JSP-3 [3100-3149]
execute as @s[name="Barbourrigues"] run scoreboard players set @s idStation 3100

#RÉGION JSP-4 [3150-3199]
execute as @s[name="Floutrias"] run scoreboard players set @s idStation 3150
execute as @s[name="Pompélinaud"] run scoreboard players set @s idStation 3160

#ROYAUME DE CORDILAND [3200-3299]
execute as @s[name="Sommieu–Chalandières"] run scoreboard players set @s idStation 3200
execute as @s[name="Fouizy-la-Décharge"] run scoreboard players set @s idStation 3230

#RÉGION JSP-5 [3300-3399]
execute as @s[name="Chouigny"] run scoreboard players set @s idStation 3300
execute as @s[name="Montagne du Destin"] run scoreboard players set @s idStation 3315
execute as @s[name="Gléry"] run scoreboard players set @s idStation 3330
execute as @s[name="Touissy"] run scoreboard players set @s idStation 3350
execute as @s[name="Traverzy"] run scoreboard players set @s idStation 3370

#CITÉS-ÉTATS DES HUMAINS DES COLLINES [3400-3499]
execute as @s[name="Génuflay"] run scoreboard players set @s idStation 3400
execute as @s[name="Port-Castoy"] run scoreboard players set @s idStation 3410
execute as @s[name="Flachoire"] run scoreboard players set @s idStation 3415
execute as @s[name="XXVillageBJ"] run scoreboard players set @s idStation 3420
execute as @s[name="Château-Douglas"] run scoreboard players set @s idStation 3450
execute as @s[name="Croassin-le-Désert"] run scoreboard players set @s idStation 3460
execute as @s[name="Glouzy-en-Bédarve"] run scoreboard players set @s idStation 3480

#PAYS NAIN [3500-3599]
execute as @s[name="Erbi Jom"] run scoreboard players set @s idStation 3500
execute as @s[name="Morgenne"] run scoreboard players set @s idStation 3510
execute as @s[name="Évernay-l'Aumône"] run scoreboard players set @s idStation 3550
execute as @s[name="Abyess Thûr"] run scoreboard players set @s idStation 3560
execute as @s[name="Le Roc-Maëg"] run scoreboard players set @s idStation 3570

#COLONIES NAINES [3600-3699]
execute as @s[name="Chamborion"] run scoreboard players set @s idStation 3600
execute as @s[name="Monthornet"] run scoreboard players set @s idStation 3620

#RÉGION JSP-1 [3700-3799]
execute as @s[name="Beurrieux–Valdemont"] run scoreboard players set @s idStation 3700

#RÉGION JSP-2 [3800-3899]
execute as @s[name="XXVillageBL"] run scoreboard players set @s idStation 3800
execute as @s[name="XXVillageBM"] run scoreboard players set @s idStation 3850

#RÉGION JSP-0 [3900-3999]
execute as @s[name="XXVillageCE"] run scoreboard players set @s idStation 3900
execute as @s[name="XXVillageCC"] run scoreboard players set @s idStation 3950
execute as @s[name="XXVillageCD"] run scoreboard players set @s idStation 3960

#FINISTÈRE [4000-4099]
execute as @s[name="XXVillageAK"] run scoreboard players set @s idStation 4000


execute unless score @s[name=!"***"] idStation matches 1.. run tellraw @a[tag=MetroDebugLog] [{"text":"[WARN] ID de station non trouvé : ","color":"gold"},{"selector":"@s"}]
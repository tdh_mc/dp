# On vire le tag tchOutside, mais on garde tchInside pour "enregistrer" le fait qu'on est à l'intérieur du réseau TCH.
tag @a[distance=0] remove tchOutside

# On affiche un titre au joueur
execute if entity @e[type=armor_stand,tag=NumLigne,distance=..200,scores={idLine=2000..}] run title @a[distance=0] subtitle [{"text":"Gare de ","color":"gray"},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]","color":"red"}]
execute unless entity @e[type=armor_stand,tag=NumLigne,distance=..200,scores={idLine=2000..}] run title @a[distance=0] subtitle [{"text":"Station ","color":"gray"},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]","color":"red"}]
title @a[distance=0] title ""
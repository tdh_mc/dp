# Tests pour savoir quels joueurs sont ou non en station

# Toutes les entrées de station fonctionnent sur le même principe :
# Les entrées sont composées de 2 item frames (de préférence des item frames déjà existantes)
# L'une d'entre elles a un tag "Inside" et l'autre "Outside" ; les deux ont un tag "tchEntrance"

# Lorsqu'on a pas de tag temporaire et que l'item frame la plus proche est Outside, on se donne le tag tchOutside
# Dans le cas contraire, si c'est Inside, on se donne le tag tchInside
execute at @p[gamemode=!spectator,distance=0,tag=!tchInside,tag=!tchOutside] as @e[type=item_frame,tag=tchEntrance,distance=..6,sort=nearest,limit=1] as @s[tag=Outside] run tag @a[distance=0] add tchOutside
execute at @p[gamemode=!spectator,distance=0,tag=!tchInside,tag=!tchOutside] as @e[type=item_frame,tag=tchEntrance,distance=..6,sort=nearest,limit=1] as @s[tag=Inside] run tag @a[distance=0] add tchInside

# Si on a déjà le tag Outside et que l'item frame la plus proche est désormais Inside, on vient d'entrer dans une station, et on lance la fonction appropriée
execute at @p[gamemode=!spectator,distance=0,tag=!tchInside,tag=tchOutside] as @e[type=item_frame,tag=tchEntrance,distance=..6,sort=nearest,limit=1] as @s[tag=Inside] run function tch:station/entree/entree

# Si on a déjà le tag Inside et que l'item frame la plus proche est désormais Outside, on vient de sortir d'une station, et on lance la fonction appropriée
execute at @p[gamemode=!spectator,distance=0,tag=tchInside,tag=!tchOutside] as @e[type=item_frame,tag=tchEntrance,distance=..6,sort=nearest,limit=1] as @s[tag=Outside] run function tch:station/entree/sortie

# Si on a le tag Outside, mais pas Inside, et qu'on est désormais loin d'une item frame, on considère ne pas être entrés dans la station
execute at @p[gamemode=!spectator,distance=0,tag=tchOutside,tag=!tchInside] unless entity @e[type=item_frame,tag=tchEntrance,distance=..50] run tag @a[distance=0] remove tchOutside
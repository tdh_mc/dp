# On vire le tag tchInside, et on supprimera tchOutside "naturellement" en nous éloignant
tag @a[distance=0] remove tchInside

# On affiche un titre au joueur (mais ça ne sera probablement pas longtemps le cas, c'est surtout pour le debug)
execute if score @p[distance=0,gamemode=!spectator] lightLevel matches ..7 run title @a[distance=0] actionbar [{"text":"Bonne journée et ","color":"gray"},{"text":"merci d'avoir choisi TCH !","color":"red"}]
execute if score @p[distance=0,gamemode=!spectator] lightLevel matches 8.. run title @a[distance=0] actionbar [{"text":"Bonne journée et ","color":"black"},{"text":"merci d'avoir choisi TCH !","color":"dark_red"}]
title @a[distance=0] title ""
# On tag le free spot qu'on a sélectionné pour le vendeur qui exécute la commande
tag @e[tag=InterieurGuichet,tag=FreeSpot,distance=..13,sort=nearest,limit=1] add ourFreeSpot

# On tp le vendeur à cette position
execute at @e[tag=ourFreeSpot,limit=1] facing entity @p[gamemode=!spectator,distance=..30] feet unless block ~ ~-1 ~ #slabs[type=bottom] run tp @s ~ ~ ~ ~ ~
execute at @e[tag=ourFreeSpot,limit=1] facing entity @p[gamemode=!spectator,distance=..30] feet if block ~ ~-1 ~ #slabs[type=bottom] run tp @s ~ ~-0.5 ~ ~ ~

# On supprime les tags (ce n'est plus un free spot)
tag @e[tag=ourFreeSpot,limit=1] remove FreeSpot
tag @e[tag=ourFreeSpot,limit=1] remove ourFreeSpot
# Exécuté par une station pour savoir si elle peut fermer les accès

# On vérifie s'il est + que l'heure de fermeture
execute if score #temps currentTdhTick >= @s finServiceAjd if score @s debutServiceAjd < @s finServiceAjd run tag @s remove StationOuverte
execute if score #temps currentTdhTick < @s debutServiceAjd if score @s debutServiceAjd < @s finServiceAjd run tag @s remove StationOuverte
execute if score #temps currentTdhTick >= @s finServiceAjd if score #temps currentTdhTick < @s debutServiceAjd run tag @s remove StationOuverte


# Si la station a fermé, on exécute la fonction correspondante
execute as @s[tag=!StationOuverte] run function tch:station/fermeture_station
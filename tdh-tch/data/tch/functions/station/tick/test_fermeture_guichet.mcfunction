# Exécuté par une station pour savoir si elle peut fermer son guichet

# On vérifie s'il est + que l'heure de fermeture
execute if score #temps currentTdhTick >= @s finGuichetAjd if score @s debutGuichetAjd < @s finGuichetAjd run tag @s remove GuichetOuvert
execute if score #temps currentTdhTick < @s debutGuichetAjd if score @s debutGuichetAjd < @s finGuichetAjd run tag @s remove GuichetOuvert
execute if score #temps currentTdhTick >= @s finGuichetAjd if score #temps currentTdhTick < @s debutGuichetAjd run tag @s remove GuichetOuvert


# Si le guichet a fermé, on exécute la fonction correspondante
execute as @s[tag=!GuichetOuvert] run function tch:station/fermeture_guichet
# Exécuté par une station pour savoir si elle peut ouvrir ses accès

# On vérifie s'il est + que l'heure d'ouverture
execute if score #temps currentTdhTick >= @s debutServiceAjd run tag @s add StationOuverte
execute if score #temps currentTdhTick < @s finServiceAjd if score @s debutServiceAjd > @s finServiceAjd run tag @s add StationOuverte

# On retire le tag s'il est + que l'heure de fermeture
execute if score #temps currentTdhTick >= @s[tag=StationOuverte] finServiceAjd if score @s debutServiceAjd <= @s finServiceAjd run tag @s remove StationOuverte


# Si la station a ouvert, on exécute la fonction correspondante
execute as @s[tag=StationOuverte] run function tch:station/ouverture_station
# Exécuté par une station pour savoir si elle peut ouvrir son guichet

# On vérifie s'il est + que l'heure d'ouverture
execute if score #temps currentTdhTick >= @s debutGuichetAjd run tag @s add GuichetOuvert
execute if score #temps currentTdhTick < @s finGuichetAjd if score @s debutGuichetAjd > @s finGuichetAjd run tag @s add GuichetOuvert

# On retire le tag s'il est + que l'heure de fermeture
execute if score #temps currentTdhTick >= @s[tag=GuichetOuvert] finGuichetAjd if score @s debutGuichetAjd <= @s finGuichetAjd run tag @s remove GuichetOuvert


# Si le guichet a ouvert, on exécute la fonction correspondante
execute as @s[tag=GuichetOuvert] run function tch:station/ouverture_guichet
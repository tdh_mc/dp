# On centre les vendeurs sur une item frame InterieurGuichet libre s'ils en sont eux-mêmes éloignés

# On tag les spots libres
execute as @e[type=item_frame,tag=InterieurGuichet,distance=..150] at @s unless entity @e[type=villager,tag=AgentTCH,distance=...7] run tag @s add FreeSpot

# Si un joueur est proche d'un agent, on le réveille un coup
execute as @e[type=villager,tag=AgentTCH,tag=!SpeakingWithPlayer,tag=!DejaReveille,distance=..150] at @s if entity @p[distance=..25] run function tch:station/tick/reveiller_vendeur
# Si on a été réveillés mais que plus aucun joueur n'est proche, on se retire le tag
execute as @e[type=villager,tag=AgentTCH,tag=DejaReveille] unless entity @p[distance=..50] run tag @s remove DejaReveille

# Pour chaque vendeur pas sur un spot, on le tp sur un spot libre
execute as @e[type=villager,tag=AgentTCH,tag=!SpeakingWithPlayer,distance=..150] at @s unless entity @e[type=item_frame,tag=InterieurGuichet,distance=...7] run function tch:station/tick/aligner_vendeur_free_spot

# On donne l'effet slowness à chaque vendeur
effect give @e[type=villager,tag=AgentTCH,tag=!SpeakingWithPlayer,distance=..150] slowness 2 3 true

# On supprime les tags temporaires
tag @e[type=item_frame,tag=InterieurGuichet,tag=FreeSpot] remove FreeSpot
# On récupère une variable aléatoire entre 0 et 19999
scoreboard players set #random min 0
scoreboard players set #random max 20000
function tdh:random

# Si la variable aléatoire est inférieure à nos probabilités d'incident, on se met hors service
execute if score #random temp < @e[tag=NomStation,distance=..150,sort=nearest,limit=1] chancesIncident positioned as @s run function tch:station/distributeur/echec/incident_debut
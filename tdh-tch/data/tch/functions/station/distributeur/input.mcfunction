
# Si on tient un ticket TCH, on lance la fonction d'expertise pour déterminer si le titre est valable
execute if entity @p[distance=0,tag=ClientDistributeurInput,tag=ClientDistributeur,nbt={SelectedItem:{id:"minecraft:paper",tag:{tch:{type:1b}}}}] run function tch:station/distributeur/ticket

# Si on tient un abonnement, on lance la fonction d'accueil de l'abonnement
execute if entity @p[distance=0,tag=ClientDistributeurInput,tag=ClientDistributeur,nbt={SelectedItem:{id:"minecraft:paper",tag:{tch:{type:2b}}}}] run function tch:station/distributeur/abonnement

# Si on tient un moyen de paiement, on lance la fonction de détection pour savoir si on paie un ticket ou un abonnement
execute if entity @p[distance=0,tag=ClientDistributeurInput,tag=ClientDistributeur,nbt={SelectedItem:{id:"minecraft:diamond"}}] run function tch:station/distributeur/diamant
execute if entity @p[distance=0,tag=ClientDistributeurInput,tag=ClientDistributeur,nbt={SelectedItem:{id:"minecraft:gold_ingot"}}] run function tch:station/distributeur/or
execute if entity @p[distance=0,tag=ClientDistributeurInput,tag=ClientDistributeur,nbt={SelectedItem:{id:"minecraft:iron_ingot"}}] run function tch:station/distributeur/fer

# Si on a déjà le tag principal mais qu'on ne tient rien de tout ça, on joue le son d'échec
execute if entity @p[distance=0,tag=ClientDistributeurInput,tag=ClientDistributeur,tag=!ClientDistributeurPaiement,tag=!ClientDistributeurTicket,tag=!ClientDistributeurAbonnement] run function tch:station/distributeur/echec/mode_paiement

# Si on n'a pas encore le tag principal, c'est qu'on vient de commencer ;
# on affiche donc l'écran de bienvenue
execute if entity @p[distance=0,tag=ClientDistributeurInput,tag=!ClientDistributeur] run function tch:station/distributeur/debut

# On réinitialise les tags temporaires non persistants
tag @p[distance=0,tag=ClientDistributeurInput] remove ClientDistributeurPaiement
tag @p[distance=0,tag=ClientDistributeurInput] remove ClientDistributeurTicket
tag @p[distance=0,tag=ClientDistributeurInput] remove ClientDistributeurAbonnement
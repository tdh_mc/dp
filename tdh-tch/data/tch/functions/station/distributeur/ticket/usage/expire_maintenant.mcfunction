# Le ticket est usagé et plus valable
# On prépare l'heure d'expiration pour l'affichage
scoreboard players operation #store currentHour = @s currentTdhTick
function tdh:store/time

# On affiche le message
execute if score #store currentHour matches ..9 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n-- Ce ticket a ","color":"#00ff66"},{"text":"expiré","color":"#66ff66"},{"text":" à "},{"storage":"tdh:store","nbt":"time","interpret":"true","color":"#66ff66"},{"text":", ---\n-- à cet instant précis... -------\n----------------------------"}]
execute if score #store currentHour matches 10.. run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n-- Ce ticket a ","color":"#00ff66"},{"text":"expiré","color":"#66ff66"},{"text":" à "},{"storage":"tdh:store","nbt":"time","interpret":"true","color":"#66ff66"},{"text":", --\n-- à cet instant précis... -------\n----------------------------"}]
# Le ticket est usagé, mais valable
# On prépare le jour et l'heure d'expiration pour l'affichage
scoreboard players operation #store idJour = @s idJour
scoreboard players operation #store currentHour = @s currentTdhTick
function tdh:store/day_long
function tdh:store/time

# On affiche le message
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n-- Ce ticket est ","color":"#00ff66"},{"text":"valide","color":"#66ff66"},{"text":". --------"}]

# On a une commande différente pour chaque nombre de digits pour éviter que tout soit décalé
execute if score #store dateAnnee matches 0..9 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Il expirera le ","color":"#00ff66"},{"storage":"tdh:store","nbt":"day","interpret":"true","color":"#66ff66"},{"text":", -----"}]
execute if score #store dateAnnee matches -9..99 unless score #store dateAnnee matches 0..9 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Il expirera le ","color":"#00ff66"},{"storage":"tdh:store","nbt":"day","interpret":"true","color":"#66ff66"},{"text":", ----"}]
execute if score #store dateAnnee matches -99..999 unless score #store dateAnnee matches -9..99 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Il expirera le ","color":"#00ff66"},{"storage":"tdh:store","nbt":"day","interpret":"true","color":"#66ff66"},{"text":", ---"}]
execute if score #store dateAnnee matches -999..9999 unless score #store dateAnnee matches -99..999 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Il expirera le ","color":"#00ff66"},{"storage":"tdh:store","nbt":"day","interpret":"true","color":"#66ff66"},{"text":", --"}]
execute unless score #store dateAnnee matches -999..9999 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Il expirera le ","color":"#00ff66"},{"storage":"tdh:store","nbt":"day","interpret":"true","color":"#66ff66"},{"text":", -"}]

# On affiche également l'heure
execute if score #store currentHour matches ..9 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"--- à ","color":"#00ff66"},{"storage":"tdh:store","nbt":"time","interpret":"true","color":"#66ff66"},{"text":". ------------------"}]
execute if score #store currentHour matches 10.. run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"--- à ","color":"#00ff66"},{"storage":"tdh:store","nbt":"time","interpret":"true","color":"#66ff66"},{"text":". -----------------"}]

# Fin du bloc de texte
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------","color":"#00ff66"}]
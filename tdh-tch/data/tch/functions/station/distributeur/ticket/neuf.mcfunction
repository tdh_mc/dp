# On a un ticket neuf
# On enregistre sa durée de validité
execute store result score @s dureeValidite run data get entity @p[distance=0,tag=ClientDistributeurInput] SelectedItem.tag.tch.duree_validite

# On affiche dans tous les cas que ce ticket est valide
# (si ce n'est pas le cas, qu'ils aillent bien se faire foutre)
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n-- Ce ticket est ","color":"#00ff66"},{"text":"valide","color":"#66ff66"},{"text":". --------"}]

# On affiche éventuellement le nombre de jours de validité
execute if score @s dureeValidite matches 1 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Durée de validité : ","color":"#00ff66"},{"score":{"name":"@s","objective":"dureeValidite"},"color":"#66ff66","extra":[{"text":" jour"}]},{"text":" ---"}]
execute if score @s dureeValidite matches 2..9 run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Durée de validité : ","color":"#00ff66"},{"score":{"name":"@s","objective":"dureeValidite"},"color":"#66ff66","extra":[{"text":" jours"}]},{"text":" --"}]
execute if score @s dureeValidite matches 10.. run tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Durée de validité : ","color":"#00ff66"},{"score":{"name":"@s","objective":"dureeValidite"},"color":"#66ff66","extra":[{"text":" jours"}]},{"text":" -"}]

# On affiche la fin du bloc de texte
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------","color":"#00ff66"}]

# On réinitialise les variables
scoreboard players reset @s dureeValidite
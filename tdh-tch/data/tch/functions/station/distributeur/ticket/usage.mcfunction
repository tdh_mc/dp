# On a un ticket usagé

# On récupère le jour et le tick de validation, ainsi que la durée de validité
execute store result score @s idJour run data get entity @p[distance=0,tag=ClientDistributeurInput] SelectedItem.tag.tch.jour_validation
execute store result score @s currentTdhTick run data get entity @p[distance=0,tag=ClientDistributeurInput] SelectedItem.tag.tch.tick_validation
execute store result score @s dureeValidite run data get entity @p[distance=0,tag=ClientDistributeurInput] SelectedItem.tag.tch.duree_validite

# On ajoute la durée de validité au tick de validation
scoreboard players operation @s idJour += @s dureeValidite
scoreboard players reset @s dureeValidite

# Selon les valeurs des variables, on exécute la fonction "valable" ou "expiré"
execute if score @s idJour > #temps idJour run function tch:station/distributeur/ticket/usage/valable
execute if score @s idJour = #temps idJour if score @s currentTdhTick > #temps currentTdhTick run function tch:station/distributeur/ticket/usage/valable
execute if score @s idJour = #temps idJour if score @s currentTdhTick = #temps currentTdhTick run function tch:station/distributeur/ticket/usage/expire_maintenant
execute if score @s idJour = #temps idJour if score @s currentTdhTick < #temps currentTdhTick run function tch:station/distributeur/ticket/usage/expire
execute if score @s idJour < #temps idJour run function tch:station/distributeur/ticket/usage/expire

# On réinitialise les variables temporaires
scoreboard players reset @s idJour
scoreboard players reset @s currentTdhTick
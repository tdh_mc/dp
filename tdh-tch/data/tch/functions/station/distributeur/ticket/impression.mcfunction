# On est une entité temporaire d’impression à sa position, et on imprime un ticket
summon item ~ ~ ~ {Tags:["TempTicketTch"],Item:{id:"paper",Count:1b,tag:{CustomModelData:100,display:{Name:'"Ticket journée TCH"',Lore:['{"text":"Ce titre de transport est","color":"gray"}','{"text":"valable 24 heures, sur","color":"gray"}','{"text":"l\'ensemble du réseau TCH.","color":"gray"}']},tch:{type:1b,used:0b,valide:1b,duree_validite:1}}}}
data modify entity @e[type=item,tag=TempTicketTch,distance=..3,sort=nearest,limit=1] Owner set from entity @s data.Owner

# On affiche une particule
particle minecraft:item minecraft:paper ~ ~ ~ 0.04 0.04 0.04 0.1 4

# On joue un son d’impression
playsound tch:borne.impression block @a ~ ~ ~ 1 1

# On retire le tag temporaire du ticket
tag @e[type=item,tag=TempTicketTch] remove TempTicketTch

# On décrémente notre total de tickets à imprimer de 1
scoreboard players remove @s nombre 1
# On invoque un marqueur temporaire d’impression
# Il faut définir au préalable la variable #tchDistributeur nombre au nombre de tickets à imprimer

summon marker ^ ^ ^0.25 {Tags:["DistributeurImpression","DistributeurImpressionTemp"]}

# On enregistre l’UUID du joueur
data modify entity @e[type=marker,tag=DistributeurImpressionTemp,limit=1] Owner set from entity @p[tag=ClientDistributeurInput] UUID
# On enregistre le nombre de tickets à imprimer
scoreboard players operation @e[type=marker,tag=DistributeurImpressionTemp,limit=1] nombre = #tchDistributeur nombre

# On enregistre le fait qu’on est en train d’imprimer
tag @s add ImpressionEnCours

# On affiche le message
tellraw @p[tag=ClientDistributeurInput] [{"text":"----------------------------\n- Vous avez acheté ","color":"#00ff66"},{"score":{"name":"#tchDistributeur","objective":"nombre"},"color":"#66ff66"},{"text":" tickets. --"},{"text":"\n--- Merci, et bon voyage ! -----\n----------------------------"}]

# On joue le son de succès
execute at @s run playsound tch:borne.succes voice @a[tag=!ClientDistributeurInput,distance=..15] ~ ~ ~ 0.6
stopsound @p[tag=ClientDistributeurInput] voice tch:borne.succes
execute at @s run playsound tch:borne.succes voice @p[tag=ClientDistributeurInput] ~ ~ ~ 1


# On supprime le tag temporaire du marqueur d’impression
tag @e[type=marker,tag=DistributeurImpressionTemp] remove DistributeurImpressionTemp
# On réinitialise la variable temporaire
scoreboard players reset #tchDistributeur nombre
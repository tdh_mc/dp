# On exécute cette fonction en tant qu'un distributeur pour tester s'il va dysfonctionner

# Si on est hors service, on vérifie si ça a été réparé
execute as @s[tag=HorsService] run function tch:station/distributeur/echec/incident_test
# Si on est pas hors service, on vérifie si un incident va avoir lieu
execute as @s[tag=!HorsService] run function tch:station/distributeur/test_nouvel_incident

# Si on est hors service, on affiche l'échec ; sinon on lance l’input distributeur
execute as @s[tag=HorsService] at @s run function tch:station/distributeur/echec/incident

execute as @s[tag=!HorsService] run function tch:station/distributeur/input
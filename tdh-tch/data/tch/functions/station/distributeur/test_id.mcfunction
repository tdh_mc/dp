# Si le distributeur est occupé, on vérifie que le joueur actuel est celui qui l'a activé
execute as @s[tag=Occupe] if score @s idTerminus = @p[distance=0,tag=ClientDistributeurInput] idTerminus run tag @s add BonJoueur
execute as @s[tag=BonJoueur] run function tch:station/distributeur/input
execute as @s[tag=Occupe,tag=!BonJoueur] run function tch:station/distributeur/echec/occupe
tag @s remove BonJoueur

# Si le distributeur n'est pas occupé, on vérifie si le distributeur va dysfonctionner
execute as @s[tag=!Occupe] run function tch:station/distributeur/test_incident
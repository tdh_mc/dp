# On tag le joueur pour noter qu'il a emprunté le chemin "Ticket"
# Ne pas supprimer dans cette fonction, mais directement dans station/distributeur/input
tag @p[distance=..0,tag=ClientDistributeurInput] add ClientDistributeurTicket

# On a une sous-fonction différente selon si le ticket est usagé ou neuf
execute store result score @s temp9 run data get entity @p[distance=..0,tag=ClientDistributeurInput] SelectedItem.tag.tch.used
execute if score @s temp9 matches 1 run function tch:station/distributeur/ticket/usage
execute if score @s temp9 matches 0 run function tch:station/distributeur/ticket/neuf
scoreboard players reset @s temp9
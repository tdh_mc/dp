# On est un marqueur temporaire d’impression à sa position

# S’il nous reste des tickets à imprimer, on en imprime un
execute if score @s nombre matches 1.. run function tch:station/distributeur/ticket/impression

# S’il ne nous reste plus de tickets à imprimer, on libère notre distributeur et on se détruit
execute unless score @s nombre matches 1.. run tag @e[type=item_frame,tag=Distributeur,sort=nearest,limit=1] remove ImpressionEnCours
execute unless score @s nombre matches 1.. run kill @s
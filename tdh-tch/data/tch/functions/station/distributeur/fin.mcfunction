# On supprime l'identifiant d'interaction du joueur et du distributeur si on le trouve
execute as @e[type=item_frame,tag=Distributeur] if score @s idTerminus = @p[tag=ClientDistributeur] idTerminus run function tch:station/distributeur/fin_distrib
scoreboard players reset @p[distance=..0,tag=ClientDistributeur] idTerminus
tag @p[distance=..0,tag=ClientDistributeur] remove ClientDistributeur
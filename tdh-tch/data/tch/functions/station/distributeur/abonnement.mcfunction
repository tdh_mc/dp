# On tient un abonnement en main
# On tag le joueur pour noter qu'il a emprunté le chemin "Abonnement"
# Ne pas supprimer dans cette fonction, mais directement dans station/distributeur/input
tag @p[distance=..0,tag=ClientDistributeurInput] add ClientDistributeurAbonnement

# On récupère l'ID de notre abonnement
execute store result score @s idAbonnement run data get entity @p[distance=..0,tag=ClientDistributeurInput] SelectedItem.tag.tch.abonnement

# On se donne une variable temporaire pour déterminer ce qu'on fait
execute as @s[tag=!AbonnementSelect] run scoreboard players set @s temp9 1
execute as @s[tag=AbonnementSelect] run scoreboard players set @s temp9 2

# En fonction de la valeur de la variable, on exécute la fonction correspondante
execute if score @s temp9 matches 1 run function tch:station/distributeur/abonnement/select
execute if score @s temp9 matches 2 run function tch:station/distributeur/abonnement/deselect

scoreboard players reset @s temp9
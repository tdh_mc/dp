# On exécute la fonction abonnement ou ticket selon notre tag
execute as @s[tag=AbonnementSelect] run function tch:station/distributeur/abonnement/or
execute as @s[tag=!AbonnementSelect] run function tch:station/distributeur/ticket/or

# On tag le joueur pour noter qu'il a emprunté le chemin "Paiement"
# Ne pas supprimer dans cette fonction, mais directement dans station/distributeur/input
tag @p[distance=..0,tag=ClientDistributeurInput] add ClientDistributeurPaiement
# Si on est plus proche de notre distributeur, on termine la fonction
execute as @e[type=item_frame,tag=Distributeur,distance=..2.5] if score @s idTerminus = @p[tag=ClientDistributeur] idTerminus run tag @p[tag=ClientDistributeur] add ToujoursLa
execute unless entity @p[tag=ClientDistributeur,tag=ToujoursLa,distance=..0] run function tch:station/distributeur/fin
tag @p[tag=ClientDistributeur] remove ToujoursLa
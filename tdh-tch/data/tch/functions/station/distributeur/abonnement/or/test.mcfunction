# On vérifie si on a 3 lingots ou si on a moins
execute store result score @s temp8 run data get entity @p[tag=ClientDistributeurInput] SelectedItem.Count
execute if score @s temp8 matches 1..2 run function tch:station/distributeur/abonnement/or/succes1
execute if score @s temp8 matches 3.. run function tch:station/distributeur/abonnement/or/succes3
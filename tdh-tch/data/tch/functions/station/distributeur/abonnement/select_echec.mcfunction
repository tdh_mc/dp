# On affiche les données de l'abonnement, mais il est invalide
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n--- Abonnement #","color":"#00ff66"},{"score":{"name":"@s","objective":"idAbonnement"},"color":"#66ff66"},{"text":" -------"}]
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"--- Actif jusqu'au ","color":"#00ff66"},{"text":"93/19/78","obfuscated":"true","color":"#66ff66"},{"text":" ---\n----------------------------"}]
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n- Impossible de trouver -------\n- votre numéro d'abonnement. --\n- Merci de contacter un agent. -\n----------------------------","color":"#66ff66"}]

# On enregistre le fait qu'on a pas d'abonnement sélectionné
tag @s remove AbonnementSelect
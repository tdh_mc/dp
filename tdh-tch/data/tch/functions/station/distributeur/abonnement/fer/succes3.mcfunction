# On a déjà stocké le nombre de jours restants avec la fonction tch:abonnement/_find
# S'il est égal à 0, on le met à 1 (car la journée actuelle est déjà en cours, et mieux vaut 1/2 journée de trop que de pas assez)
execute if score #tch dureeValidite matches ..0 run scoreboard players set #tch dureeValidite 1
# Dans tous les cas, on ajoute 7, car on vient de payer 1 semaine d'abonnement, et on enregistre le nouveau score
execute store result storage tch:abonnement actifs[0].jours_restants int 1 run scoreboard players add #tch dureeValidite 7

# On paie
clear @p[tag=ClientDistributeurInput] iron_ingot 3


# On enregistre le nombre de jours d'abonnement, et on calcule la fin de validité
scoreboard players operation #store idJour = #temps idJour
scoreboard players operation #store idJour += #tch dureeValidite
function tdh:store/day_long

# On affiche les données de l'abonnement
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n--- Vous avez acheté ","color":"#00ff66"},{"text":"7","color":"#66ff66"},{"text":"j. -----"}]
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"--- Actif jusqu'au ","color":"#00ff66"},{"storage":"tdh:store","nbt":"day","interpret":"true","color":"#66ff66"},{"text":" ---\n----------------------------"}]

# On joue le son de succès
execute at @s run playsound tch:borne.succes voice @a[distance=..15] ~ ~ ~ 0.6
stopsound @p[tag=ClientDistributeurInput] voice tch:borne.succes
execute at @s run playsound tch:borne.succes voice @p[tag=ClientDistributeurInput] ~ ~ ~ 1
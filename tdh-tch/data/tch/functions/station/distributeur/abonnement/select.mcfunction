# On vient de sélectionner notre abonnement

# On tente de le trouver dans le fichier des abonnements
scoreboard players operation #tch idAbonnement = @s idAbonnement
function tch:abonnement/_find
# Si on a trouvé l'abonnement, on exécute la fonction succès, sinon c'est l'échec
execute if score #tch idAbonnement matches 1 run function tch:station/distributeur/abonnement/select_succes
execute unless score #tch idAbonnement matches 1 run function tch:station/distributeur/abonnement/select_echec
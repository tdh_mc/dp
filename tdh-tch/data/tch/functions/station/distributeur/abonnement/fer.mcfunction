# On sélectionne l'abonnement à partir de celui qu'on a enregistré
scoreboard players operation #tch idAbonnement = @s idAbonnement
function tch:abonnement/_find

# Si on a trouvé l'abonnement, on exécute la fonction succès, sinon c'est l'échec
execute if score #tch idAbonnement matches 1 run function tch:station/distributeur/abonnement/fer/test
execute unless score #tch idAbonnement matches 1 run function tch:station/distributeur/abonnement/select_echec
# On enregistre le nombre de jours d'abonnement, et on calcule la fin de validité
scoreboard players operation #store idJour = #temps idJour
scoreboard players operation #store idJour += #tch dureeValidite
function tdh:store/day_long

# On affiche les données de l'abonnement
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n--- Abonnement #","color":"#00ff66"},{"score":{"name":"@s","objective":"idAbonnement"},"color":"#66ff66"},{"text":" -------"}]
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"--- Actif jusqu'au ","color":"#00ff66"},{"storage":"tdh:store","nbt":"day","interpret":"true","color":"#66ff66"},{"text":" ---\n----------------------------"}]
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- Vous pouvez le ","color":"#00ff66"},{"text":"recharger","underlined":"true","color":"#66ff66","hoverEvent":{"action":"show_text","value":[{"text":"1 semaine pour ","color":"#00ff66"},{"text":"3 fer","color":"#66ff66","bold":"true"},{"text":" ou "},{"text":"1 or","color":"#66ff66","bold":"true"},{"text":"\n1 mois pour ","color":"#00ff66"},{"text":"3 or","color":"#66ff66","bold":"true"},{"text":" ou "},{"text":"1 diamant","color":"#66ff66","bold":"true"},{"text":"\n4 mois pour ","color":"#00ff66"},{"text":"3 diamants","color":"#66ff66","bold":"true"}]}},{"text":" --\n-- ou rappuyer sur le bouton --\n-- pour le "},{"text":"désélectionner","color":"#66ff66"},{"text":". -----\n----------------------------"}]

# On enregistre le fait qu'on a un abonnement sélectionné
tag @s add AbonnementSelect
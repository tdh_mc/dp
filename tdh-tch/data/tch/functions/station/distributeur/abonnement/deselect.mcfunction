# On désélectionne l'abonnement
tag @s remove AbonnementSelect

# On affiche un message
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"----------------------------\n-- ","color":"#00ff66"},{"text":"Abonnement désélectionné","color":"#66ff66"},{"text":" --\n----------------------------"}]
tellraw @p[distance=0,tag=ClientDistributeurInput] [{"text":"-- ","color":"#00ff66"},{"text":"Achetez","underlined":"true","color":"#66ff66","hoverEvent":{"action":"show_text","value":[{"text":"1 ticket pour ","color":"#00ff66"},{"text":"1 fer","color":"#66ff66","bold":"true"},{"text":"\n5 tickets pour ","color":"#00ff66"},{"text":"1 or","color":"#66ff66","bold":"true"},{"text":"\n20 tickets pour ","color":"#00ff66"},{"text":"1 diamant","color":"#66ff66","bold":"true"}]}},{"text":" des tickets, ou ----\n-- présentez une autre carte --\n-- d'abonnement. -------------\n----------------------------"}]
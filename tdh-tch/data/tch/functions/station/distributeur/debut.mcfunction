# On choisit un identifiant unique pour cette interaction
tag @p[distance=..0,tag=ClientDistributeurInput] add ClientDistributeur
execute store result score @s idTerminus run data get entity @e[type=#tdh:random,sort=random,limit=1] Pos[2] 1634.9072
scoreboard players operation @p[tag=ClientDistributeur] idTerminus = @s idTerminus

# On enregistre le fait qu'on est occupés
tag @s add Occupe

# On affiche l'écran d'accueil
tellraw @p[tag=ClientDistributeur] [{"text":"----------------------------\n","color":"#00ff66"},{"text":"----- Distributeur "},{"text":"TCH","color":"#66ff66"},{"text":" v2 -----"},{"text":"\n----------------------------"}]
tellraw @p[tag=ClientDistributeur] [{"text":"- ","color":"#00ff66"},{"text":"1 fer","color":"#66ff66","bold":"true"},{"text":" = 1 ticket ------------"}]
tellraw @p[tag=ClientDistributeur] [{"text":"- ","color":"#00ff66"},{"text":"1 or","color":"#66ff66","bold":"true"},{"text":" = 5 tickets ------------"}]
tellraw @p[tag=ClientDistributeur] [{"text":"- ","color":"#00ff66"},{"text":"1 diamant","color":"#66ff66","bold":"true"},{"text":" = 20 tickets ------"}]
tellraw @p[tag=ClientDistributeur] [{"text":"----------------------------","color":"#00ff66"}]
tellraw @p[tag=ClientDistributeur] [{"text":"- ","color":"#00ff66"},{"text":"Présentez votre carte Gold","color":"#66ff66"},{"text":" --"},{"text":"\n--  pour la recharger --------"}]
tellraw @p[tag=ClientDistributeur] [{"text":"- ","color":"#00ff66"},{"text":"Présentez un ticket","color":"#66ff66"},{"text":" ---------"},{"text":"\n-- pour vérifier sa validité ----"}]
tellraw @p[tag=ClientDistributeur] [{"text":"----------------------------","color":"#00ff66"}]
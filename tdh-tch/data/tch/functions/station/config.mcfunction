# Configurateur automatique de stations
# Permet de choisir les paramètres de la station actuelle
# Exécuté par un administrateur TCH

# On démarre la config et se donne donc le tag Config et ConfigStation
tag @s add Config
tag @s add ConfigStation
# On supprime les éventuelles variables des autres configurateurs pouvant poser problème
scoreboard players reset @s idLine

# Contrairement au configurateur de ligne, on gère toujours les options de la station la plus proche
# En premier lieu, on la tag
execute if score @s idStation matches 1.. at @s at @e[tag=NomStation,distance=..250] if score @s idStation = @e[tag=NomStation,sort=nearest,limit=1] idStation run tag @e[tag=NomStation,sort=nearest,limit=1] add ourStation
execute unless entity @e[tag=ourStation,limit=1] run tag @e[tag=NomStation,distance=..200,sort=nearest,limit=1] add ourStation

# Si elle n'a pas d'ID station, on tente de le récupérer
execute as @e[tag=ourStation,limit=1] unless score @s idStation matches 1.. run function tch:auto/get_id_station

# On enregistre l'ID station de la station la plus proche
scoreboard players operation @s idStation = @e[tag=ourStation,limit=1] idStation



# On active l'objectif de configuration à trigger (9999 = Choix d'un paramètre à modifier)
scoreboard players set @s configID 9999
scoreboard players set @s config 0
scoreboard players enable @s config

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TCH : STATION ","color":"gold"},{"selector":"@e[tag=ourStation]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le paramètre à modifier :","color":"gray"}]

# Heure d'ouverture de la station (Non modifiable)
scoreboard players operation #store currentHour = @e[tag=ourStation,limit=1] debutServiceAjd
function tdh:store/time
tellraw @s [{"text":"","clickEvent":{"action":"run_command","value":"/trigger config set 11100"},"hoverEvent":{"action":"show_text","value":"Cliquer pour recalculer"},"color":"gray"},{"text":"- "},{"text":"La station ouvre à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"gold"},{"text":" aujourd'hui."}]
# Heure de fermeture de la station (Non modifiable)
scoreboard players operation #store currentHour = @e[tag=ourStation,limit=1] finServiceAjd
function tdh:store/time
tellraw @s [{"text":"","clickEvent":{"action":"run_command","value":"/trigger config set 11200"},"hoverEvent":{"action":"show_text","value":"Cliquer pour recalculer"},"color":"gray"},{"text":"- "},{"text":"La station ferme à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"gold"},{"text":" aujourd'hui."}]
# Heure d'ouverture du point d'accueil (10000)
scoreboard players operation #store currentHour = @e[tag=ourStation,limit=1] debutGuichet
function tdh:store/time
tellraw @s [{"text":"- ","color":"gray"},{"text":"Heure d'ouverture du guichet","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 10000"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Heure de fermeture du point d'accueil (10100)
scoreboard players operation #store currentHour = @e[tag=ourStation,limit=1] finGuichet
function tdh:store/time
tellraw @s [{"text":"- ","color":"gray"},{"text":"Heure de fermeture du guichet","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 10100"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Heure d'ouverture du point d'accueil le week-end (10050)
scoreboard players operation #store currentHour = @e[tag=ourStation,limit=1] debutGuichetWe
function tdh:store/time
tellraw @s [{"text":"- ","color":"gray"},{"text":"Heure d'ouverture du guichet le week-end","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 10050"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Heure de fermeture du point d'accueil le week-end (10150)
scoreboard players operation #store currentHour = @e[tag=ourStation,limit=1] finGuichetWe
function tdh:store/time
tellraw @s [{"text":"- ","color":"gray"},{"text":"Heure de fermeture du guichet le week-end","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 10150"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Probabilité d'incident (10900)
tellraw @s [{"text":"- ","color":"gray"},{"text":"Probabilité d'incident","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 10900"}},{"text":" (actuellement "},{"score":{"name":"@e[tag=ourStation,limit=1]","objective":"chancesIncident"}},{"text":" pour 2000)"}]
tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"- Quitter le configurateur","color":"gray","clickEvent":{"action":"run_command","value":"/trigger config set -1"}}]


# Si on n'a pas trouvé de station, on annule
execute unless entity @e[tag=ourStation,limit=1] run function tdh:config/end
execute unless entity @e[tag=ourStation,limit=1] run tellraw @s [{"text":"Impossible de trouver une station proche.","color":"red"}]


# Réinitialisation des tags et variables
tag @e[tag=ourStation] remove ourStation
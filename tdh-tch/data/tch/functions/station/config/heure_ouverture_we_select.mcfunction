## SÉLECTION DE L'HEURE D'OUVERTURE DU GUICHET LE WEEK-END

# On récupère la bonne station selon la valeur de notre variable
execute at @s at @e[tag=NomStation,distance=..250] if score @s idStation = @e[tag=NomStation,sort=nearest,limit=1] idStation run tag @e[tag=NomStation,sort=nearest,limit=1] add ourStation
execute unless entity @e[tag=ourStation,limit=1] run tellraw @s [{"text":"Impossible de trouver la station d'ID ","color":"red"},{"score":{"name":"@s","objective":"idStation"},"color":"dark_red"},{"text":"..."}]

# On calcule la valeur actuelle
scoreboard players operation #store currentHour = @e[tag=ourStation,limit=1] debutGuichetWe
function tdh:store/time

# On active l'objectif de configuration à trigger
scoreboard players set @s configID 10050
scoreboard players set @s config 0
scoreboard players enable @s config

# On demande de choisir l'heure de début de service
tellraw @s [{"text":"CONFIGURATEUR TCH : STATION ","color":"gold"},{"selector":"@e[tag=ourStation,limit=1]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez l'heure d'ouverture du guichet le week-end :","color":"gray"}]
scoreboard players set #configHeure min 4
scoreboard players set #configHeure max 12
function tdh:config/select/heure
tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":")"}]
tellraw @s [{"text":"——————————————————————————\nÀ titre indicatif, voici les horaires d'ouverture des lignes passant ici :","color":"gray"}]
execute at @e[tag=ourStation,limit=1] at @e[tag=NumLigne,distance=..150] run function tch:ligne/config/debut_service_display

# On nettoie le tag temporaire
tag @e[tag=ourStation] remove ourStation
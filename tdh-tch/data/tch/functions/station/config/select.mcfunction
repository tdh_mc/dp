# Cette fonction est exécutée par un administrateur
# avec configID = 9999 (menu principal) et config = le menu qu'il veut afficher

# Selon le menu qu'on veut afficher
# - Heure d'ouverture (10000)
execute as @s[scores={config=10000}] run function tch:station/config/heure_ouverture_select
# - Heure d'ouverture WE (10050)
execute as @s[scores={config=10050}] run function tch:station/config/heure_ouverture_we_select
# - Heure de fermeture (10100)
execute as @s[scores={config=10100}] run function tch:station/config/heure_fermeture_select
# - Heure de fermeture WE (10150)
execute as @s[scores={config=10150}] run function tch:station/config/heure_fermeture_we_select
# - Heure d'ouverture (10900)
execute as @s[scores={config=10900}] run function tch:station/config/proba_incident_select
# - Recalculer les horaires d'ouverture / fermeture (11100 / 11200)
execute as @s[scores={config=11100}] run function tch:station/config/recalcul_ouverture
execute as @s[scores={config=11200}] run function tch:station/config/recalcul_fermeture

# - Quitter le configurateur (-1)
execute as @s[scores={config=-1}] run function tdh:config/end
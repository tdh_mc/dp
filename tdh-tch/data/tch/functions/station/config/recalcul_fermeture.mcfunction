## RECALCUL DE L'HEURE DE FERMETURE DE LA STATION
# On récupère la bonne station selon la valeur de notre variable
execute at @s at @e[type=armor_stand,tag=NomStation,distance=..250] if score @s idStation = @e[tag=NomStation,sort=nearest,limit=1] idStation run tag @e[tag=NomStation,sort=nearest,limit=1] add ourStation
execute unless entity @e[type=armor_stand,tag=ourStation,limit=1] run tellraw @s [{"text":"Impossible de trouver la station d'ID ","color":"red"},{"score":{"name":"@s","objective":"idStation"},"color":"dark_red"},{"text":"..."}]

# On lui fait recalculer son horaire de fermeture
execute as @e[type=armor_stand,tag=ourStation,limit=1] run function tch:station/calcul_fermeture_station

# On affiche un message de confirmation
scoreboard players operation #store currentHour = @e[type=armor_stand,tag=ourStation,limit=1] finServiceAjd
function tdh:store/time
tellraw @s [{"text":"L'heure de fermeture de la station a été recalculée et est désormais fixée à ","color":"gray"},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":" (tick "},{"score":{"name":"@e[type=armor_stand,tag=ourStation,limit=1]","objective":"finServiceAjd"}},{"text":")"}]

# On nettoie le tag temporaire
tag @e[type=armor_stand,tag=ourStation] remove ourStation

# On revient au menu
scoreboard players set @s config 0
scoreboard players enable @s config
function tch:station/config
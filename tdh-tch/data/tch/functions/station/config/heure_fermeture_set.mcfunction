## ENREGISTREMENT DE L'HEURE DE FERMETURE DU GUICHET
# Elle est indiquée par la variable config

# On récupère la bonne station selon la valeur de notre variable
execute at @s at @e[tag=NomStation,distance=..250] if score @s idStation = @e[tag=NomStation,sort=nearest,limit=1] idStation run tag @e[tag=NomStation,sort=nearest,limit=1] add ourStation
execute unless entity @e[tag=ourStation,limit=1] run tellraw @s [{"text":"Impossible de trouver la station d'ID ","color":"red"},{"score":{"name":"@s","objective":"idStation"},"color":"dark_red"},{"text":"..."}]

# On calcule l’heure en ticks à partir de l’input et on la stocke dans #config temp
function tdh:config/calcul/hhmm_to_ticks

# On définit la valeur de la variable de la station
scoreboard players operation @e[tag=ourStation,limit=1] finGuichet = #config temp

# On affiche un message de confirmation
scoreboard players operation #store currentHour = #config temp
function tdh:store/time
tellraw @s [{"text":"L'heure de fermeture du guichet en semaine a été définie à ","color":"gray"},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":" (tick "},{"score":{"name":"#config","objective":"temp"}},{"text":")"}]

# On nettoie le tag temporaire
tag @e[tag=ourStation] remove ourStation
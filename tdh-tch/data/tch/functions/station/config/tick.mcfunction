# On tick régulièrement pour lancer les sous-fonctions de prise en compte de la configuration
# Le joueur a une variable configID qui sert à stocker la variable qu'il config, et une variable config qu'il trigger pour communiquer la valeur désirée

# Cette fonction est à destination des administrateurs, donc on peut utiliser execute AS au lieu de AT

# Si on est à l'écran d'accueil, on lance une des fonctions select selon notre choix
execute as @s[scores={configID=9999}] unless score @s config matches 0 run function tch:station/config/select

# Selon la variable qu'on cherche à config
# - Dans tous les cas (10000+), si on veut revenir au menu (config -120047):
execute as @s[scores={configID=10000..,config=-120047}] run function tch:station/config
# - Heure d'ouverture de l'accueil (10000)
execute as @s[scores={configID=10000}] unless score @s config matches ..0 run function tch:station/config/heure_ouverture_set
# - Heure d'ouverture de l'accueil le week-end (10050)
execute as @s[scores={configID=10050}] unless score @s config matches ..0 run function tch:station/config/heure_ouverture_we_set
# - Heure de fermeture de l'accueil (10100)
execute as @s[scores={configID=10100}] unless score @s config matches ..0 run function tch:station/config/heure_fermeture_set
# - Heure de fermeture de l'accueil le week-end (10150)
execute as @s[scores={configID=10150}] unless score @s config matches ..0 run function tch:station/config/heure_fermeture_we_set
# - Probabilité d'incident matériel (10900)
execute as @s[scores={configID=10900}] unless score @s config matches ..0 run function tch:station/config/proba_incident_set


# Tous les joueurs qui viennent de choisir une config retournent au menu principal
execute as @s[scores={configID=10000..}] unless score @s config matches ..0 run function tch:station/config
## ENREGISTREMENT DE LA PROBABILITÉ D'INCIDENT
# Elle est indiquée par la variable config

# On récupère la bonne station selon la valeur de notre variable
execute at @s at @e[tag=NomStation,distance=..250] if score @s idStation = @e[tag=NomStation,sort=nearest,limit=1] idStation run tag @e[tag=NomStation,sort=nearest,limit=1] add ourStation
execute unless entity @e[tag=ourStation,limit=1] run tellraw @s [{"text":"Impossible de trouver la station d'ID ","color":"red"},{"score":{"name":"@s","objective":"idStation"},"color":"dark_red"},{"text":"..."}]

# On définit la valeur de la variable du PC
execute if score @s config matches 0..2000 run scoreboard players operation @e[tag=ourStation,limit=1] chancesIncident = @s config
execute if score @s config matches ..-1 run scoreboard players set @e[tag=ourStation,limit=1] chancesIncident 0
execute if score @s config matches 2001.. run scoreboard players set @e[tag=ourStation,limit=1] chancesIncident 2000

# On affiche un message de confirmation
execute if score @e[tag=ourStation,limit=1] debutServiceWe matches 1.. run tellraw @s [{"text":"Les chances d'incident sont désormais de ","color":"gray"},{"score":{"name":"@e[tag=ourStation,limit=1]","objective":"chancesIncident"},"color":"red"},{"text":" pour 2000."}]

# On nettoie le tag temporaire
tag @e[tag=ourStation] remove ourStation
# On est à la position d’un joueur

# On vérifie s’il y a un valideur Gold à proximité :
execute as @e[type=item_frame,tag=ValideurGold,tag=!PorteOuverte,distance=..1] run function tch:station/valideur/gold
# On vérifie s’il y a un valideur Badge à proximité :
execute as @e[type=item_frame,tag=ValideurBadge,tag=!PorteOuverte,distance=..1] run function tch:station/valideur/badge
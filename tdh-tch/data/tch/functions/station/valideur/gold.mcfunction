# Valideur Gold
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"--- VALIDEUR GOLD ---","color":"gold","bold":"true"}]

# TCH NBT structure
# tch:{
#	type: <1 ou 2>b (1 = ticket, 2 = abonnement) => en l'occurence c'est 2
# 	abonnement: <integer> (l'identifiant d'abonnement)
# }

# Cette fonction est exécutée par une item frame "ValideurGold"
# à la position d'un joueur se trouvant au même endroit (<1 bloc de distance)

# On tag le joueur
tag @p[distance=..1,gamemode=!spectator] add TestGold

# On lance le test pour savoir quel objet on tient
function tch:station/valideur/gold/test_objet


# On supprime le tag temporaire
tag @p[tag=TestGold] remove TestGold
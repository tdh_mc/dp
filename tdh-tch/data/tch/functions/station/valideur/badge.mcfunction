# Valideur Gold
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"--- VALIDEUR BADGE ---","color":"aqua","bold":"true"}]

# TCH NBT structure
# tch:{
#	type: <1, 2 ou 66>b (1 = ticket, 2 = abonnement, 66 = badge personnel) => en l'occurence c'est 66
# 	id_personnel: <integer> (l'identifiant d'employéE)
# }

# Cette fonction est exécutée par une item frame "ValideurBadge"
# à la position d'un joueur se trouvant au même endroit (<1 bloc de distance)

# On tag le joueur
tag @p[distance=..1,gamemode=!spectator] add TestBadge

# On lance le test pour savoir quel objet on tient
function tch:station/valideur/badge/test_objet


# On supprime le tag temporaire
tag @p[tag=TestBadge] remove TestBadge
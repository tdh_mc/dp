### VALIDEUR SILVER : Test de porte
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test porte --","color":"gray","bold":"true"}]

# On teste la porte
execute at @s[tag=Nord] at @e[type=item_frame,tag=ValideurGold,sort=nearest,limit=1] positioned ~ ~-2 ~-2 run function tch:station/valideur/porte/test_porte
execute at @s[tag=Sud] at @e[type=item_frame,tag=ValideurGold,sort=nearest,limit=1] positioned ~ ~-2 ~2 run function tch:station/valideur/porte/test_porte
execute at @s[tag=Est] at @e[type=item_frame,tag=ValideurGold,sort=nearest,limit=1] positioned ~2 ~-2 ~ run function tch:station/valideur/porte/test_porte
execute at @s[tag=Ouest] at @e[type=item_frame,tag=ValideurGold,sort=nearest,limit=1] positioned ~-2 ~-2 ~ run function tch:station/valideur/porte/test_porte

execute if entity @e[type=item,tag=PorteValide,distance=..15,limit=1] run tag @s add PorteTrouvee

# Si on a trouvé la porte, on vérifie si la station est ouverte
execute as @s[tag=PorteTrouvee] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Porte valide trouvée!","color":"green"}]
execute as @s[tag=PorteTrouvee] run function tch:station/valideur/silver/test_station

# S'il n'y a pas de porte, on lance l'échec porte
execute as @s[tag=!PorteTrouvee] run function tch:station/valideur/silver/echec/porte

# Qu'on aie utilisé la porte valide ou non, on supprime son tag pour éviter que qqn d'autre ne nous la fasse ouvrir
tag @e[type=item,distance=..15,tag=PorteValide] remove PorteValide
tag @s remove PorteTrouvee
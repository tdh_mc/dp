# Valideur Silver
# On exécute cette fonction en tant qu'un objet droppé
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Ticket usagé détecté!","color":"green","bold":"true"}]

# TCH NBT structure
# tch:{
#	type: <1 ou 2>b (1 = ticket, 2 = abonnement) (en l'occurence c'est 1)
# 	used: <0 ou 1>b (le ticket est-il usagé ?)
#	valide: <0 ou 1>b (le ticket est-il un vrai ou un faux ? ; peut être absent s'il est faux)
#	jour_validation: <integer> (le #temps.idJour au moment de la validation ; pas présent avant la validation)
# 	tick_validation: <integer> (le tick au moment de la validation ; pas présent avant la validation)
#	derniere_validation: <integer> (la date et l'heure de dernière validation du ticket ; pas présent avant la validation)
#	{
# 		jour: <integer> (l'idJour au moment de la dernière validation ; pas présent avant la validation)
# 		tick: <integer> (le tick au moment de la dernière validation ; pas présent avant la validation)
#	}
# 	duree_validite: <integer> (le nombre de jours de validité du ticket)
# }


# On enregistre les variables depuis le NBT
execute store result score @s idJour run data get entity @s Item.tag.tch.jour_validation
execute store result score @s currentTdhTick run data get entity @s Item.tag.tch.tick_validation
execute store result score @s dureeValidite run data get entity @s Item.tag.tch.duree_validite
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"- Validé le ","color":"gray"},{"text":"#","color":"gold"},{"score":{"name":"@s","objective":"idJour"},"color":"gold"},{"text":" au tick "},{"score":{"name":"@s","objective":"currentTdhTick"},"color":"gold"},{"text":".\n- Ticket valable "},{"score":{"name":"@s","objective":"dureeValidite"},"color":"gold"},{"text":" jours."}]

# On calcule dans dureeValidite le dernier jour de validité du ticket
scoreboard players operation @s dureeValidite += @s idJour
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"- Jour de fin de validité calculé : ","color":"gray"},{"text":"#","color":"gold"},{"score":{"name":"@s","objective":"dureeValidite"},"color":"gold"},{"text":"."}]

# Si le ticket est périmé, on se donne le tag périmé
execute if score #temps idJour > @s dureeValidite run tag @s add Perime
execute if score #temps idJour = @s dureeValidite if score #temps currentTdhTick > @s currentTdhTick run tag @s add Perime

# Si le ticket est périmé, on lance l'échec
execute as @s[tag=Perime] run function tch:station/valideur/silver/echec/perime
# Sinon, on vérifie qu'on a pas validé récemment ce ticket
execute as @s[tag=!Perime] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Ticket toujours valable!","color":"green","bold":"true"}]
execute as @s[tag=!Perime] run function tch:station/valideur/silver/test_validation_recente

# On réinitialise tout
scoreboard players reset @s idJour
scoreboard players reset @s currentTdhTick
scoreboard players reset @s dureeValidite
tag @s remove Perime
# Valideur Silver
# On exécute cette fonction en tant qu'un objet droppé
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test objet --","color":"gray","bold":"true"}]

# On vérifie d'abord s'il s'agit bien d'un ticket
tag @s[nbt={Item:{tag:{tch:{type:1b}}}}] add Ticket
# Si c'en est un, on lance les tests détaillés
execute as @s[tag=Ticket] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Ticket détecté!","color":"green"}]
execute as @s[tag=Ticket] run function tch:station/valideur/silver/test_ticket

# Sinon, c'est un échec
execute as @s[tag=!Ticket] run function tch:station/valideur/silver/echec/inconnu

# On supprime le tag temporaire
tag @s remove Ticket
# TCH valideur échec Silver
# (Ticket déjà validé récemment)

# On affiche le message d'erreur
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Ticket déjà validé récemment.","color":"red"}]
title @p[tag=TestSilverPlayer] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Ce ticket a déjà été utilisé il y a moins de 15mn."}]

# On joue le son d'échec
playsound tch:valideur.echec_normal voice @a[distance=..30] ~ ~ ~
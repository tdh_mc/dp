# TCH valideur échec Silver
# (Incident sur le valideur)

# On affiche le message d'erreur
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Incident en cours.","color":"red"}]
title @p[tag=TestSilverPlayer] actionbar [{"text":"","color":"gray"},{"text":"Ce valideur est "},{"text":"hors-service","bold":"true","color":"red"},{"text":"."}]

# On joue le son d'échec
playsound tdh:br.no voice @a[distance=..30] ~ ~ ~
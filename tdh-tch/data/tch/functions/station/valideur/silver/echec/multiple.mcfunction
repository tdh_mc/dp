# TCH valideur échec Silver
# (plusieurs tickets à la fois)

# On affiche le message d'erreur
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Plusieurs tickets présentés.","color":"red"}]
title @p[tag=TestSilverPlayer] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Merci de ne présenter qu'un seul ticket."}]

# On joue le son d'échec
playsound tdh:br.no voice @a[distance=..30] ~ ~ ~
# TCH valideur échec Silver
# (ticket invalide présenté)

# On affiche le message d'erreur
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Ticket non valide.","color":"red"}]
title @p[tag=TestSilverPlayer] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Titre de transport non reconnu."}]

# On change le modèle et le nom du ticket
data modify entity @s Item.tag.CustomModelData set value 103
data modify entity @s Item.tag.display.Name set value '"Ticket TCH usagé"'
data modify entity @s Item.tag.display.Lore set value ['{"text":"Ce titre de transport","color":"gray"}','{"text":"a déjà été utilisé.","color":"gray"}']

# On joue le son d'échec
playsound tch:valideur.echec_normal voice @a[distance=..30] ~ ~ ~
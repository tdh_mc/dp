# TCH valideur échec Silver
# (ticket expiré)
# (après une attente de 1s)

# On a les variables idJour et tickValidation
# On calcule la valeur du jour
scoreboard players operation #store idJour = @p[tag=PerimeSilver] idJour
function tdh:store/day
# On calcule la valeur de l'heure
scoreboard players operation #store currentHour = @p[tag=PerimeSilver] tickValidation
function tdh:store/time

# On affiche le titre
execute if score @p[tag=PerimeSilver] idJour < #temps idJour run title @p[tag=PerimeSilver] actionbar [{"text":"Il était valable jusqu'au ","color":"gray"},{"nbt":"day","storage":"tdh:store","interpret":"true","color":"red"},{"text":" à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":" !"}]
execute if score @p[tag=PerimeSilver] idJour = #temps idJour if score @p[tag=PerimeSilver] tickValidation matches ..23999 run title @p[tag=PerimeSilver] actionbar [{"text":"Il était valable jusqu'à "},{"text":"ce matin ","color":"red"},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":" !"}]
execute if score @p[tag=PerimeSilver] idJour = #temps idJour if score @p[tag=PerimeSilver] tickValidation matches 24000.. run title @p[tag=PerimeSilver] actionbar [{"text":"Il était valable jusqu'à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":" aujourd'hui","color":"red"},{"text":" !"}]

# On réinitialise les variables du joueur
scoreboard players reset @p[tag=PerimeSilver] idJour
scoreboard players reset @p[tag=PerimeSilver] tickValidation
scoreboard players reset @p[tag=PerimeSilver] ticksWaited
tag @p[tag=PerimeSilver] remove PerimeSilver
# On enregistre le fait qu'on est hors-service
tag @s add HorsService
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"On génère un incident aléatoire!","color":"red","bold":"true"}]

# On enregistre le jour actuel (ça sera réparé demain à 3h30)
scoreboard players operation @s idJour = #temps idJour
execute if score #temps currentTdhTick matches 7000.. run scoreboard players add @s idJour 1

# On affiche des particules malencontreuses au niveau du satané valideur
particle large_smoke ~ ~ ~ .05 .05 .05 .005 25
particle smoke ~ ~ ~ .05 .05 .05 .002 50
particle angry_villager ~ ~ ~ 0 0 0 1 1

# On joue un son de cassage
playsound tdh:br.chuff master @a[distance=..15] ~ ~ ~ 0.8 0.65
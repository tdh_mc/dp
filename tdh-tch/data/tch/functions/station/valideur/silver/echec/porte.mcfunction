# TCH valideur échec Silver
# (pas de porte conforme)

# On affiche le message d'erreur
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Impossible de trouver une porte valide.","color":"red"}]
title @p[tag=TestSilverPlayer] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Porte introuvable. Merci de contacter un agent."}]

# On joue le son d'échec
playsound tdh:br.no voice @a[distance=..30] ~ ~ ~
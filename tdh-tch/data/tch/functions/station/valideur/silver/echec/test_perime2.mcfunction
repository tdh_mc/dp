# On incrémente de 1 tick le temps attendu par le joueur
# (en pratique il a attendu 10, mais osef)
scoreboard players add @p[tag=PerimeSilver] ticksWaited 1

# Si on a attendu suffisamment, on affiche le second titre
execute if score @p[tag=PerimeSilver] ticksWaited matches 3.. run function tch:station/valideur/silver/echec/perime2
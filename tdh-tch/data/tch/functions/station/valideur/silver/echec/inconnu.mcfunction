# TCH valideur échec Silver
# (objet autre qu'un ticket présenté)

# On affiche le message d'erreur 
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"L'objet présenté n'est pas un ticket.","color":"red"}]
title @p[tag=TestSilverPlayer] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Titre de transport non reconnu."}]

# On joue le son d'échec
playsound tch:valideur.echec_critique voice @a[distance=..30] ~ ~ ~
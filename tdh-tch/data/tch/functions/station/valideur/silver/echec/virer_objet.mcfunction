tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Virage de l'objet --","color":"gray","bold":"true"}]

# On avance le ticket d'un bloc dans la direction appropriée
execute at @s as @e[type=item_frame,tag=ValideurSilver,sort=nearest,limit=1] run function tch:station/valideur/silver/succes/deplacer_ticket/test_direction

# Selon le résultat du test, on déplace le ticket dans la bonne direction
execute if score #valideurSilver temp matches 1 at @s run data modify entity @s Motion set value [0d,0d,.4d]
execute if score #valideurSilver temp matches 2 at @s run data modify entity @s Motion set value [0d,0d,-.4d]
execute if score #valideurSilver temp matches 3 at @s run data modify entity @s Motion set value [-.4d,0d,0d]
execute if score #valideurSilver temp matches 4 at @s run data modify entity @s Motion set value [.4d,0d,0d]
execute unless score #valideurSilver temp matches 1..4 run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Impossible de trouver la direction de virage de l'objet.","color":"red"}]
# On réinitialise le score temporaire
scoreboard players reset #valideurSilver temp

# On enregistre le fait qu'on a bougé et qu'il ne faut plus nous déplacer
tag @s remove ObjetAVirer
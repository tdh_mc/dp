# TCH valideur échec Silver
# (ticket expiré)

# On affiche le message d'erreur
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Ticket périmé.","color":"red"}]
title @p[tag=TestSilverPlayer] actionbar [{"text":"Ce titre de transport a ","color":"gray"},{"text":"expiré.","bold":"true","color":"red"}]

# On enregistre les données nécessaires pour pouvoir afficher le message suivant
scoreboard players operation @p[tag=TestSilverPlayer] idJour = @s dureeValidite
scoreboard players operation @p[tag=TestSilverPlayer] tickValidation = @s currentTdhTick
scoreboard players set @p[tag=TestSilverPlayer] ticksWaited 0
tag @p[tag=TestSilverPlayer] add PerimeSilver

# On modifie le modèle du ticket
data modify entity @s Item.tag.CustomModelData set value 102

# On joue le son d'échec
playsound tch:valideur.echec_normal voice @a[distance=..30] ~ ~ ~
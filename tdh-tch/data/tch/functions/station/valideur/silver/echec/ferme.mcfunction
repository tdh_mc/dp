# TCH valideur échec Silver
# (Station fermée)

# On enregistre l'heure d'ouverture de la station
scoreboard players operation #store currentHour = @e[tag=NomStation,sort=nearest,limit=1] debutServiceAjd
function tdh:store/time

# On affiche le message d'erreur
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"La station est fermée.","color":"red"}]
title @p[tag=TestSilverPlayer] actionbar [{"text":"","color":"gray"},{"text":"La station est "},{"text":"fermée","bold":"true","color":"red"},{"text":". Ouverture à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":"."}]

# On joue le son d'échec
playsound tdh:br.no voice @a[distance=..30] ~ ~ ~
# On a déjà comme variables assignées :
# @s temp3 = le jour de validation (hier ou ajd)
# @s temp4 = le tick de validation
# @s temp2 = le lendemain du jour de validation (osef, mais on peut utiliser temp2 comme variable buffer)

# On doit se donner le tag PeutValider si on est plus de 15mn après la précédente validation

# On calcule le tick actuel moins le tick de validation
scoreboard players operation @s temp2 = #temps currentTdhTick
scoreboard players operation @s temp2 -= @s temp4
execute if score @s temp3 < #temps idJour run scoreboard players operation @s temp2 += #temps fullDayTicks

# Si on a attendu plus de 15mn, on peut y aller
execute if score @s temp2 matches 499.. run tag @s add PeutValider
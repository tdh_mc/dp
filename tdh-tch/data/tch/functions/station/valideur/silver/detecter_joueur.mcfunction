# On exécute cette fonction en tant qu'un item droppé sur le valideur, à sa position, pour trouver le joueur l'ayant lancé
# On enregistre les 4 ints de l'UUID qu'on cherche dans 4 variables temporaires
execute store result score #targetUUID temp run data get entity @s Thrower[0]
execute store result score #targetUUID temp2 run data get entity @s Thrower[1]
execute store result score #targetUUID temp3 run data get entity @s Thrower[2]
execute store result score #targetUUID temp4 run data get entity @s Thrower[3]

# Pour chaque joueur qu'on trouve aux alentours, on vérifie si l'UUID matche
execute at @a[gamemode=!spectator,tag=!TestSilverPlayer,distance=..20] run function tch:station/valideur/silver/detecter_joueur/1

# On nettoie les variables temporaires
scoreboard players reset #targetUUID temp
scoreboard players reset #targetUUID temp2
scoreboard players reset #targetUUID temp3
scoreboard players reset #targetUUID temp4
scoreboard players reset #playerUUID temp
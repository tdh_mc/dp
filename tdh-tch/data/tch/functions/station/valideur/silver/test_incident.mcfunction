# Valideur Silver
# On exécute cette fonction en tant qu'un valideur pour tester s'il va dysfonctionner
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test incident --","color":"gray","bold":"true"}]

# Si on est hors service, on vérifie si ça a été réparé
execute as @e[type=item_frame,tag=TestSilverVal,tag=HorsService] run function tch:station/valideur/silver/echec/incident_test

# On récupère une variable aléatoire entre 0 et 19999
scoreboard players set #random min 0
scoreboard players set #random max 20000
function tdh:random

# Si la variable aléatoire est inférieure à nos probabilités d'incident, on se met hors service
execute if score #random temp < @e[tag=NomStation,distance=..150,sort=nearest,limit=1] chancesIncident as @e[type=item_frame,tag=TestSilverVal,limit=1] at @s run function tch:station/valideur/silver/echec/incident_debut
# Si on est hors service, on affiche l'échec ; sinon on continue les tests
execute as @e[type=item_frame,tag=TestSilverVal,tag=HorsService,limit=1] at @s run function tch:station/valideur/silver/echec/incident

execute unless entity @e[type=item_frame,tag=TestSilverVal,tag=HorsService,limit=1] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Pas d'incident en cours.","color":"green"}]
execute unless entity @e[type=item_frame,tag=TestSilverVal,tag=HorsService,limit=1] as @e[type=item,tag=TestSilver,limit=1] run function tch:station/valideur/silver/test_objet
# Valideur Silver
# On exécute cette fonction en tant qu'un objet droppé
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test validation récente --","color":"gray","bold":"true"}]

# TCH NBT structure
# tch:{
#	type: <1 ou 2>b (1 = ticket, 2 = abonnement) (en l'occurence c'est 1)
# 	used: <0 ou 1>b (le ticket est-il usagé ?)
#	valide: <0 ou 1>b (le ticket est-il un vrai ou un faux ? ; peut être absent s'il est faux)
#	jour_validation: <integer> (le #temps.idJour au moment de la validation ; pas présent avant la validation)
# 	tick_validation: <integer> (le tick au moment de la validation ; pas présent avant la validation)
#	derniere_validation: <integer> (la date et l'heure de dernière validation du ticket ; pas présent avant la validation)
#	{
# 		jour: <integer> (l'idJour au moment de la dernière validation ; pas présent avant la validation)
# 		tick: <integer> (le tick au moment de la dernière validation ; pas présent avant la validation)
#	}
# 	duree_validite: <integer> (le nombre de jours de validité du ticket)
# }


# On enregistre les variables depuis le NBT
execute store result score @s temp3 run data get entity @s Item.tag.tch.derniere_validation.jour
execute store result score @s temp4 run data get entity @s Item.tag.tch.derniere_validation.tick

# On vérifie si on a attendu plus de 15mn avant de revalider
scoreboard players operation @s temp2 = @s temp3
scoreboard players add @s temp2 1
# Si on est 2 jours après la dernière validation, on peut valider
execute if score #temps idJour > @s temp2 run tag @s add PeutValider
# Si on est le jour même ou le lendemain, on fait le calcul détaillé
execute if score #temps idJour <= @s temp2 if score #temps idJour >= @s temp3 run function tch:station/valideur/silver/test_validation_recente/calcul_horaire


# Si on a déjà validé, on lance l'échec
execute as @s[tag=!PeutValider] run function tch:station/valideur/silver/echec/validation_recente
# Sinon, on valide avec succès
execute as @s[tag=PeutValider] run function tch:station/valideur/silver/succes/usage

# On réinitialise tout
scoreboard players reset @s temp3
scoreboard players reset @s temp4
scoreboard players reset @s temp2
tag @s remove PeutValider
# On incrémente de 1 tick le temps attendu par le joueur
# (en pratique il a attendu 10, mais osef)
scoreboard players add @p[tag=SuccesSilver] ticksWaited 1

# Si on a attendu suffisamment, on affiche le second titre
execute if score @p[tag=SuccesSilver] ticksWaited matches 5.. run function tch:station/valideur/silver/succes/succes2
# TCH valideur succès Silver
# (ticket neuf)
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Succès de la validation!","color":"green","bold":"true"}]

# On affiche le message
title @p[tag=TestSilverPlayer] actionbar [{"text":"Bon voyage sur les lignes ","color":"gold"},{"text":"TCH","bold":"true","color":"red"},{"text":" !"}]

# On enregistre les données nécessaires pour pouvoir afficher le message suivant
scoreboard players operation @p[tag=TestSilverPlayer] idJour = @s dureeValidite
scoreboard players operation @p[tag=TestSilverPlayer] tickValidation = #temps currentTdhTick
scoreboard players set @p[tag=TestSilverPlayer] ticksWaited 0
tag @p[tag=TestSilverPlayer] add SuccesSilver

# On joue le son de succès
playsound tch:valideur.succes voice @a[distance=..30] ~ ~-1 ~

# On donne au ticket un tag pour le faire avancer dans quelques ticks
tag @s add TicketADeplacer

# Si la porte est valide, on ouvre la porte
execute unless entity @e[type=item_frame,tag=TestSilverVal,limit=1] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"","color":"gray"},{"text":"Erreur fatale: ","color":"red","bold":"true"},{"text":"Impossible de trouver le valideur."}]
execute at @e[type=item_frame,tag=TestSilverVal,limit=1] unless entity @e[type=item,tag=PorteValide,distance=..5] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"","color":"gray"},{"text":"Erreur fatale: ","color":"red","bold":"true"},{"text":"Impossible de trouver l'item porte valide."}]
execute at @e[type=item_frame,tag=TestSilverVal,limit=1] as @e[type=item_frame,tag=ValideurGold,sort=nearest,limit=1] at @e[type=item,tag=PorteValide,distance=..5] run function tch:station/valideur/porte/ouvrir_delai
# TCH valideur succès Silver
# (après une attente de 2s)

# On a les variables idJour et tickValidation
# On calcule la valeur du jour
scoreboard players operation #store idJour = @p[tag=SuccesSilver] idJour
function tdh:store/day
# On calcule la valeur de l'heure
scoreboard players operation #store currentHour = @p[tag=SuccesSilver] tickValidation
function tdh:store/time

# On affiche le titre
title @p[tag=SuccesSilver] actionbar [{"text":"Votre ticket est valable jusqu'au ","color":"gray"},{"nbt":"day","storage":"tdh:store","interpret":"true","color":"gold"},{"text":" à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"gold"},{"text":" !"}]

# On réinitialise les variables du joueur
scoreboard players reset @p[tag=SuccesSilver] idJour
scoreboard players reset @p[tag=SuccesSilver] tickValidation
scoreboard players reset @p[tag=SuccesSilver] ticksWaited
tag @p[tag=SuccesSilver] remove SuccesSilver
# On définit un score temporaire selon la direction du valideur le plus proche
execute as @s[tag=Nord] run scoreboard players set #valideurSilver temp 1
execute as @s[tag=Sud] run scoreboard players set #valideurSilver temp 2
execute as @s[tag=Est] run scoreboard players set #valideurSilver temp 3
execute as @s[tag=Ouest] run scoreboard players set #valideurSilver temp 4
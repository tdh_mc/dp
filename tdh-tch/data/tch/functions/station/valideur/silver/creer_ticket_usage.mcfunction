# Valideur Silver
# On exécute cette fonction en tant qu'un objet droppé
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Ticket neuf détecté!","color":"green","bold":"true"}]

# TCH NBT structure
# tch:{
#	type: <1 ou 2>b (1 = ticket, 2 = abonnement) (en l'occurence c'est 1)
# 	used: <0 ou 1>b (le ticket est-il usagé ?)
#	valide: <0 ou 1>b (le ticket est-il un vrai ou un faux ? ; peut être absent s'il est faux)
#	duree_validite: <integer> (le nombre de jours de validité du ticket)
#	
#	CES 3 ÉLÉMENTS SONT À CRÉER:
#	- jour_validation: <integer> (le #temps.idJour au moment de la validation ; pas présent avant la validation)
# 	- tick_validation: <integer> (le tick au moment de la validation ; pas présent avant la validation)
# 	- derniere_validation: {
#		- jour: <integer> (l'idJour au moment de la dernière validation ; pas présent avant la validation)
#		- tick: <integer> (le tick au moment de la dernière validation ; pas présent avant la validation)
#		}
# }


# On enregistre la date et l'heure actuelles dans le NBT, + le fait que le ticket est utilisé
execute store result entity @s Item.tag.tch.jour_validation int 1 run scoreboard players get #temps idJour
execute store result entity @s Item.tag.tch.tick_validation int 1 run scoreboard players get #temps currentTdhTick
execute store result entity @s Item.tag.tch.derniere_validation.jour int 1 run scoreboard players get #temps idJour
execute store result entity @s Item.tag.tch.derniere_validation.tick int 1 run scoreboard players get #temps currentTdhTick
data modify entity @s Item.tag.tch.used set value 1b

# On change également le modèle et le nom du ticket
data modify entity @s Item.tag.CustomModelData set value 101
data modify entity @s Item.tag.display.Name set value '"Ticket TCH usagé"'
data modify entity @s Item.tag.display.Lore set value ['{"text":"Ce titre de transport","color":"gray"}','{"text":"a déjà été utilisé.","color":"gray"}']

# On calcule dans dureeValidite le dernier jour de validité du ticket
execute store result score @s dureeValidite run data get entity @s Item.tag.tch.duree_validite
scoreboard players operation @s dureeValidite += #temps idJour

# On lance ensuite la fonction succès
function tch:station/valideur/silver/succes/neuf

# On réinitialise tout
scoreboard players reset @s idJour
scoreboard players reset @s currentTdhTick
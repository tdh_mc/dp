# On compare le dernier élément de l'UUID
execute store result score #playerUUID temp run data get entity @p[distance=0,gamemode=!spectator] UUID[3]
# S'il matche, on donne le tag au joueur
execute if score #playerUUID temp = #targetUUID temp4 run tag @p[distance=0,gamemode=!spectator] add TestSilverPlayer
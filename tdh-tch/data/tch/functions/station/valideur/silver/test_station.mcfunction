### VALIDEUR SILVER : Test de station ouverte
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test station ouverte --","color":"gray","bold":"true"}]

# On vérifie si la station est ouverte
execute at @e[type=armor_stand,tag=NomStation,sort=nearest,limit=1] if entity @e[type=armor_stand,tag=NomStation,tag=StationOuverte,distance=...1] run tag @s add TestSilverPossible

# Si la station est ouverte, on vérifie si on va avoir un incident
execute as @s[tag=TestSilverPossible] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"La station est ouverte!","color":"green"}]
execute as @s[tag=TestSilverPossible] run function tch:station/valideur/silver/test_incident

# Sinon, on affiche l'erreur station fermée
execute as @s[tag=!TestSilverPossible] run function tch:station/valideur/silver/echec/ferme

# On supprime le tag temporaire
tag @s remove TestSilverPossible
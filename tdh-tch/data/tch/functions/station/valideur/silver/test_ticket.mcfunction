# Valideur Silver
# On exécute cette fonction en tant qu'un objet droppé
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test ticket --","color":"gray","bold":"true"}]

# TCH NBT structure
# tch:{
#	type: <1 ou 2>b (1 = ticket, 2 = abonnement) (en l'occurence c'est 1)
# 	used: <0 ou 1>b (le ticket est-il usagé ?)
#	valide: <0 ou 1>b (le ticket est-il un vrai ou un faux ? ; peut être absent s'il est faux)
#	jour_validation: <integer> (le #temps.idJour au moment de la validation ; pas présent avant la validation)
# 	tick_validation: <integer> (le tick au moment de la validation ; pas présent avant la validation)
# 	duree_validite: <integer> (le nombre de jours de validité du ticket)
# }

# On empêche qui que ce soit d'autre que la personne qui l'a lancé de ramasser le ticket
data modify entity @s Owner set from entity @s Thrower
# Le ticket dépop dans 20 secondes si tu ne le ramasses pas, pour ne pas encombrer le passage
# 1.5 seconde d'attente pour pouvoir le ramasser pour te punir d'utiliser des tickets
data merge entity @s {Age:5400s,PickupDelay:30s}

# On enregistre les variables depuis le NBT
execute store result score @s ticketValide run data get entity @s Item.tag.tch.valide
execute store result score @s temp run data get entity @s Item.tag.tch.used
execute store result score @s temp9 run data get entity @s Item.Count

# Si le ticket est invalide, on se donne temp = -1
# Si le ticket est valide et inutilisé, on a déjà temp = 0
# Si le ticket est valide et utilisé, on a déjà temp = 1
execute unless score @s ticketValide matches 1 run scoreboard players set @s temp -1

# S'il y a plusieurs tickets, on se donne temp = -2,
# pour ne pas valider tous les tickets en même temps.
execute if score @s temp9 matches 2.. run scoreboard players set @s temp -2


# Si le ticket est invalide, on envoie un message d'erreur
execute if score @s temp matches -2 run function tch:station/valideur/silver/echec/multiple
execute if score @s temp matches -1 run function tch:station/valideur/silver/echec/invalide
# S'il est utilisé, on vérifie s'il est encore valable
execute if score @s temp matches 1 run function tch:station/valideur/silver/test_ticket_usage
# S'il est non utilisé, on génère un nouveau ticket usagé
execute if score @s temp matches 0 run function tch:station/valideur/silver/creer_ticket_usage


# On réinitialise tout
scoreboard players reset @s ticketValide
scoreboard players reset @s temp
scoreboard players reset @s temp9
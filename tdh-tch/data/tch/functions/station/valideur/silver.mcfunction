# Valideur Silver
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"--- VALIDEUR SILVER ---","color":"gray","bold":"true"}]

# TCH NBT structure
# tch:{
#	type: <1 ou 2>b (1 = ticket, 2 = abonnement) (en l'occurence c'est 1)
# 	used: <0 ou 1>b (le ticket est-il usagé ?)
#	valide: <0 ou 1>b (le ticket est-il un vrai ou un faux ? ; peut être absent s'il est faux)
#	jour_validation: <integer> (le #temps.idJour au moment de la validation ; pas présent avant la validation)
# 	tick_validation: <integer> (le tick au moment de la validation ; pas présent avant la validation)
#	derniere_validation: <integer> (la date et l'heure de dernière validation du ticket ; pas présent avant la validation)
#	{
# 		jour: <integer> (l'idJour au moment de la dernière validation ; pas présent avant la validation)
# 		tick: <integer> (le tick au moment de la dernière validation ; pas présent avant la validation)
#	}
# 	duree_validite: <integer> (le nombre de jours de validité du ticket)
# }

# Cette fonction est exécutée par un command block situé sous la plaque de pression du valideur Silver

# On tag le valideur et l'item droppé + le joueur
tag @e[type=item_frame,tag=ValideurSilver,distance=..3,sort=nearest,limit=1] add TestSilverVal
execute positioned ~ ~2 ~ run tag @e[distance=..1,type=item,sort=nearest,limit=1] add TestSilver
execute at @e[type=item_frame,tag=TestSilverVal,limit=1] run tag @p[distance=..1,gamemode=!spectator] add TestSilverPlayer
# Si on n'a pas pu détecter le joueur, on fait une recherche par UUID (plus lent, mais sûr)
execute unless entity @p[tag=TestSilverPlayer,distance=..5] as @e[type=item,tag=TestSilver,limit=1] at @s run function tch:station/valideur/silver/detecter_joueur

# On affiche des messages de debug si on a pas pu trouver certaines entités
execute unless entity @e[type=item_frame,tag=TestSilverVal] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Impossible de trouver l'item frame ","color":"red"},{"text":"ValideurSilver","color":"dark_red"},{"text":"."}]
execute unless entity @e[type=item,tag=TestSilver] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Impossible de trouver l'item droppé.","color":"red"}]
execute unless entity @p[tag=TestSilverPlayer] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Impossible de trouver le joueur.","color":"red"}]

# On immobilise l'objet droppé au centre de la plaque de pression
data modify entity @e[type=item,tag=TestSilver,limit=1] Motion set value [0d,0d,0d]
execute as @e[type=item,tag=TestSilver,limit=1] run tp @s ~ ~1.5 ~

# On lance le test pour savoir si on a une porte
execute as @e[type=item_frame,tag=TestSilverVal,sort=nearest,limit=1] at @s run function tch:station/valideur/silver/test_porte

# On schedule le virage de l'objet dans 5 ticks, si on n'a pas le tag signifiant le succès
execute as @e[type=item,tag=TestSilver,limit=1] as @s[tag=!TicketADeplacer] run tag @s add ObjetAVirer
execute as @e[type=item,tag=TestSilver,tag=ObjetAVirer,limit=1] run schedule function tch:station/valideur/silver/echec/test_virer_objet 5t

# On supprime le tag temporaire
tag @e[type=item_frame,tag=TestSilverVal] remove TestSilverVal
tag @e[type=item,tag=TestSilver] remove TestSilver
tag @p[tag=TestSilverPlayer] remove TestSilverPlayer
tag @e[tag=TestSilverPossible] remove TestSilverPossible
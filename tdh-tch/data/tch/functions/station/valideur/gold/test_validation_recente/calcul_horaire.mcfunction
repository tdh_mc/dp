# On a déjà comme variables assignées :
# #tch idJour = le jour de validation (hier ou ajd)
# #tch currentTdhTick = le tick de validation
# #tch temp2 = le lendemain du jour de validation (osef, mais on peut utiliser temp2 comme variable buffer)

# On doit se donner le tag PeutValider si on est plus de 15mn après la précédente validation

# On calcule le tick actuel moins le tick de validation
scoreboard players operation #tch temp2 = #temps currentTdhTick
scoreboard players operation #tch temp2 -= #tch currentTdhTick
execute if score #tch idJour < #temps idJour run scoreboard players operation #tch temp2 += #temps fullDayTicks

# Si on a attendu plus de 15mn, on peut y aller
execute if score #tch temp2 matches 499.. run tag @s add PeutValider
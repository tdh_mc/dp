# TCH valideur échec Gold
# (ID d'abonnement inconnu)
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Abonnement inconnu.","color":"red"}]

# On affiche le message d'erreur 
title @p[tag=TestGold] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Carte muette. Contactez un agent TCH."}]

# On joue le son d'échec
playsound tch:valideur.navigo.echec_normal voice @a[distance=..30] ~ ~ ~

# On enregistre l'échec chez le joueur
tag @p[tag=TestGold] add EchecGold
# TCH valideur échec Gold
# (Porte introuvable)
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Porte introuvable.","color":"red"}]

# On affiche le message d'erreur 
title @p[tag=TestGold] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Porte introuvable. Merci de contacter un agent."}]

# On joue le son d'échec
playsound tdh:br.no voice @a[distance=..30] ~ ~ ~

# On enregistre l'échec chez le joueur
tag @p[tag=TestGold] add EchecGold
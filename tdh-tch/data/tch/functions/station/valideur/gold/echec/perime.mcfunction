# TCH valideur échec Gold
# (Abonnement expiré)
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Abonnement périmé.","color":"red"}]

# On affiche le message d'erreur 
title @p[tag=TestGold] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Cet abonnement a expiré."}]

# On joue le son d'échec
playsound tch:valideur.navigo.echec_normal voice @a[distance=..30] ~ ~ ~

# On enregistre l'échec chez le joueur
tag @p[tag=TestGold] add EchecGold
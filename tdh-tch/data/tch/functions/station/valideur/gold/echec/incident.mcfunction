# TCH valideur échec Gold
# (Incident sur le valideur)
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Incident en cours.","color":"red"}]

# On affiche le message d'erreur
title @p[tag=TestGold] actionbar [{"text":"","color":"gray"},{"text":"Ce valideur est "},{"text":"hors-service","bold":"true","color":"red"},{"text":"."}]

# On joue le son d'échec
playsound tdh:br.no voice @a[distance=..30] ~ ~ ~

# On enregistre l'échec chez le joueur
tag @p[tag=TestGold] add EchecGold
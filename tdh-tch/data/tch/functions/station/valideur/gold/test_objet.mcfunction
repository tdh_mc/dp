tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test objet --","color":"gray","bold":"true"}]

# Si le joueur a une carte Gold en main, on teste l'abonnement :
# On récupère simplement l'ID de l'abonnement du joueur, et on le stocke dans l'argument de la fonction de recherche
scoreboard players set #tch idAbonnement 0
execute store result score #tch idAbonnement run data get entity @p[tag=TestGold] SelectedItem.tag.tch.abonnement
execute unless score #tch idAbonnement matches 0 run function tch:abonnement/_find

# Si la fonction a réussi, on vérifie si l'abonnement est valide
execute if score #tch idAbonnement matches 1 run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Abonnement détecté!","color":"green"}]
execute if score #tch idAbonnement matches 1 run function tch:station/valideur/gold/test_abo

# Si elle a échoué, on lance l'échec
execute if score #tch idAbonnement matches -1 unless entity @p[tag=TestGold,tag=EchecGold] run function tch:station/valideur/gold/echec/inconnu

# Sinon (=on tient autre chose qu'un abonnement), il ne se passe rien
# (plutôt RP, surtout que ce code marche aussi sur les valideurs Silver)
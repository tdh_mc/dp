# TCH valideur succès Gold
# (Abonnement valable et provisionné)
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Succès de la validation!","color":"green","bold":"true"}]

# On affiche le message
title @p[tag=TestGold] actionbar [{"text":"Bon voyage sur les lignes ","color":"gold"},{"text":"TCH","bold":"true","color":"red"},{"text":" !"}]

# On enregistre le jour et l'heure de dernière validation (= l'heure actuelle) dans les données de l'abonnement
execute store result storage tch:abonnement actifs[0].valide_le.tick int 1 run scoreboard players get #temps currentTdhTick
execute store result storage tch:abonnement actifs[0].valide_le.jour int 1 run scoreboard players get #temps idJour

# On enregistre les données nécessaires pour pouvoir afficher le message suivant
scoreboard players operation @p[tag=TestGold] tickValidation = #tch temp
scoreboard players set @p[tag=TestGold] ticksWaited 0
tag @p[tag=TestGold] add BonVoyageGold

# On joue le son de succès
playsound tch:valideur.navigo.succes voice @a[distance=..30] ~ ~-1 ~

# Si la porte est valide, on ouvre la porte
execute unless entity @e[type=item,tag=PorteValide,distance=..5] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"","color":"gray"},{"text":"Erreur fatale: ","color":"red","bold":"true"},{"text":"Impossible de trouver l'item porte valide."}]
execute at @e[type=item,tag=PorteValide,distance=..5] run function tch:station/valideur/porte/ouvrir
# Valideur Gold ; test de dysfonctionnement du valideur
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test incident --","color":"gray","bold":"true"}]

# Si on est hors service, on vérifie si ça a été réparé
execute as @s[tag=HorsService] run function tch:station/valideur/gold/echec/incident_test

# On récupère une variable aléatoire entre 0 et 19999
scoreboard players set #random min 0
scoreboard players set #random max 20000
function tdh:random

# Si la variable aléatoire est inférieure à nos probabilités d'incident, on se met hors service
execute if score #random temp < @e[tag=NomStation,distance=..150,sort=nearest,limit=1] chancesIncident run function tch:station/valideur/gold/echec/incident_debut
# Si on est hors service, on affiche l'échec ; sinon on valide et on ouvre la porte
execute as @s[tag=HorsService] unless entity @p[tag=TestGold,tag=EchecGold] run function tch:station/valideur/gold/echec/incident

execute as @s[tag=!HorsService] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Pas d'incident en cours.","color":"green"}]
execute as @s[tag=!HorsService] run function tch:station/valideur/gold/succes
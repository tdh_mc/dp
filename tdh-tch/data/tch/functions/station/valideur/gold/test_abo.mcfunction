
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test abonnement --","color":"gray","bold":"true"}]

# On récupère le nombre de jours de validité de l'abonnement
execute store result score #tch temp run data get storage tch:abonnement actifs[0].jours_restants

# Si ce nombre est supérieur à 0, on vérifie si on n'a pas validé récemment
execute if score #tch temp matches 1.. run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Abonnement provisionné!","color":"green"}]
execute if score #tch temp matches 1.. run function tch:station/valideur/gold/test_validation_recente

# Sinon, on lance la fonction échec (abonnement expiré)
# On ne la lance pas si le joueur a déjà eu un message d'erreur sur ce valideur
execute if score #tch temp matches ..0 unless entity @p[tag=TestGold,tag=EchecGold] run function tch:station/valideur/gold/echec/perime

# On réinitialise la variable temporaire
scoreboard players reset #tch temp
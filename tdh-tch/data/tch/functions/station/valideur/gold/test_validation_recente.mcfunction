tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test validation récente --","color":"gray","bold":"true"}]

# On récupère la date et l'heure de dernière validation
execute store result score #tch currentTdhTick run data get storage tch:abonnement resultat.valide_le.tick
execute store result score #tch idJour run data get storage tch:abonnement resultat.valide_le.jour

# On vérifie si on a attendu plus de 15mn avant de revalider
scoreboard players operation #tch temp2 = #tch idJour
scoreboard players add #tch temp2 1
# Si on est 2 jours après la dernière validation, on peut valider
execute if score #temps idJour > #tch temp2 run tag @s add PeutValider
# Si on est le jour même ou le lendemain, on fait le calcul détaillé
execute if score #temps idJour <= #tch temp2 if score #temps idJour >= #tch idJour run function tch:station/valideur/gold/test_validation_recente/calcul_horaire

# Si ce nombre est supérieur à 0, on vérifie si la porte existe
execute as @s[tag=PeutValider] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Pas de validation récente!","color":"green"}]
execute as @s[tag=PeutValider] run function tch:station/valideur/gold/test_porte

# Sinon, on lance la fonction échec (abonnement déjà validé récemment)
# On ne la lance pas si le joueur a déjà eu un message d'erreur sur ce valideur
execute as @s[tag=!PeutValider] unless entity @p[tag=TestGold,tag=EchecGold] run function tch:station/valideur/gold/echec/validation_recente

# On réinitialise les variables temporaires
tag @s remove PeutValider
scoreboard players reset #tch currentTdhTick
scoreboard players reset #tch idJour
scoreboard players reset #tch temp2
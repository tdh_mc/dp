# TCH valideur succès Gold
# (Abonnement valable et provisionné)
# (Après une attente de ~2s)

# On affiche le titre
execute if score @p[tag=BonVoyageGold] tickValidation matches 90.. run title @p[tag=BonVoyageGold] actionbar [{"text":"Il vous reste ","color":"gray"},{"score":{"name":"@p[tag=BonVoyageGold]","objective":"tickValidation"},"color":"#92d312","bold":"true"},{"text":" jours d'abonnement !","color":"gray"}]
execute if score @p[tag=BonVoyageGold] tickValidation matches 30..89 run title @p[tag=BonVoyageGold] actionbar [{"text":"Il vous reste ","color":"gray"},{"score":{"name":"@p[tag=BonVoyageGold]","objective":"tickValidation"},"color":"#b7c911","bold":"true"},{"text":" jours d'abonnement !","color":"gray"}]
execute if score @p[tag=BonVoyageGold] tickValidation matches 15..29 run title @p[tag=BonVoyageGold] actionbar [{"text":"Il vous reste ","color":"gray"},{"score":{"name":"@p[tag=BonVoyageGold]","objective":"tickValidation"},"color":"#c9ae11","bold":"true"},{"text":" jours d'abonnement !","color":"gray"}]
execute if score @p[tag=BonVoyageGold] tickValidation matches 8..14 run title @p[tag=BonVoyageGold] actionbar [{"text":"Il vous reste ","color":"gray"},{"score":{"name":"@p[tag=BonVoyageGold]","objective":"tickValidation"},"color":"#c95b11","bold":"true"},{"text":" jours d'abonnement !","color":"gray"}]
execute if score @p[tag=BonVoyageGold] tickValidation matches ..7 run title @p[tag=BonVoyageGold] actionbar [{"text":"Il vous reste ","color":"gray"},{"score":{"name":"@p[tag=BonVoyageGold]","objective":"tickValidation"},"color":"#c91711","bold":"true"},{"text":" jours d'abonnement. Pensez à le recharger!","color":"gray"}]

# On réinitialise les variables du joueur
scoreboard players reset @p[tag=BonVoyageGold] tickValidation
scoreboard players reset @p[tag=BonVoyageGold] ticksWaited
tag @p[tag=BonVoyageGold] remove BonVoyageGold
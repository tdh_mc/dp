# On referme la porte
setblock ~ ~ ~ air

# On enregistre sur le valideur Gold le plus proche que la porte est refermée
execute as @e[type=item_frame,tag=ValideurGold,distance=..10,sort=nearest,limit=1] run tag @s[tag=PorteOuverte] add PorteOuverteFound
execute unless entity @e[type=item_frame,tag=PorteOuverteFound] as @e[type=item_frame,tag=ValideurBadge,distance=..10,sort=nearest,limit=1] run tag @s[tag=PorteOuverte] add PorteOuverteFound

tag @e[type=item_frame,tag=PorteOuverteFound] remove PorteOuverte
tag @e[type=item_frame,tag=PorteOuverteFound] remove PorteOuverteFound

# On supprime l'item
kill @s
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"--- Tick des portes à ouvrir/fermer ---","color":"gray","bold":"true"}]

# On vérifie si des portes doivent être ouvertes ou fermées
execute as @e[type=item,tag=PorteValideur,tag=!Ferme] at @s run function tch:station/valideur/porte/test_fermeture
execute as @e[type=item,tag=PorteValideur,tag=Ferme] at @s run function tch:station/valideur/porte/test_ouverture

# On avance les tickets qui ont besoin de l'être
execute as @e[type=item,tag=TicketADeplacer] at @s run function tch:station/valideur/silver/succes/deplacer_ticket

# S'il reste des portes en attente d'action, on schedule à nouveau la fonction
execute if entity @e[type=item,tag=PorteValideur] run schedule function tch:station/valideur/porte/tick 5t
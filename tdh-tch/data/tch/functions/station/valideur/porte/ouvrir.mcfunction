# On ouvre la porte
setblock ~ ~ ~ redstone_torch

# On invoque un item qui permettra de la refermer avec un schedule
execute align xyz run summon item ~.5 ~.5 ~.5 {PickupDelay:32767s,Age:-30s,NoGravity:1b,Invulnerable:1b,Item:{Count:1b,id:"minecraft:polished_blackstone_button"},Tags:["PorteValideur"]}
schedule function tch:station/valideur/porte/tick 5t

# On enregistre le fait qu'on a ouvert la porte
# (si on est une item frame ; sinon cela ne fera rien)
tag @s add PorteOuverte
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Ouverture de la porte (avec délai) --","color":"gray","bold":"true"}]

# On invoque un item qui permettra d'ouvrir la porte avec un schedule
execute align xyz run summon item ~.5 ~.5 ~.5 {PickupDelay:32767s,Age:-45s,NoGravity:1b,Invulnerable:1b,Item:{Count:1b,id:"minecraft:polished_blackstone_button"},Tags:["PorteValideur","Ferme"]}
execute unless entity @e[type=item,tag=PorteValideur,tag=Ferme,distance=..5] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Échec lors de l'invocation de l'item.","color":"red"}]
schedule function tch:station/valideur/porte/tick 5t

# On enregistre le fait qu'on a ouvert la porte
# (si on est une item frame ; sinon cela ne fera rien)
tag @s add PorteOuverte
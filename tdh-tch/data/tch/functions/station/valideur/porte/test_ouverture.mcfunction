# On teste l'âge de l'objet matérialisant la torche
execute store result score #tch temp run data get entity @s Age

# On ouvre la porte si cet âge a dépassé -30
execute if score #tch temp matches -30.. run setblock ~ ~ ~ redstone_torch
execute if score #tch temp matches -30.. run tag @s remove Ferme

# On réinitialise la variable
scoreboard players reset #tch temp
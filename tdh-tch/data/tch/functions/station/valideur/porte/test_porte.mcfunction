# On vérifie s'il y a bien une porte au-dessus de nous et un bloc solide sous nous
scoreboard players set #tchValideur temp 0
execute unless block ~ ~-1 ~ #tdh:passable if block ~ ~2 ~ iron_door unless block ~ ~1 ~ iron_door run scoreboard players set #tchValideur temp 1
# Si ce n'est pas le cas, on teste aussi la même config 1 bloc plus bas (pour les valideurs en pente descendante)
execute if score #tchValideur temp matches 0 unless block ~ ~-2 ~ #tdh:passable if block ~ ~1 ~ iron_door unless block ~ ~ ~ iron_door run scoreboard players set #tchValideur temp 2

# Si c'est le cas, on invoque un item à notre emplacement
# Cet item dépopera automatiquement au tick suivant, mais il enregistre la position de la porte en cas de validation réussie et permet d'y générer une torche de redstone
execute if score #tchValideur temp matches 1 run setblock ~ ~ ~ air
execute if score #tchValideur temp matches 1 align xyz run summon item ~.5 ~.5 ~.5 {Tags:["PorteValide"],Age:5999s,NoGravity:1b,PickupDelay:32767s,Item:{id:"dark_oak_button",Count:1b}}
# Cas si la porte est 1 bloc plus bas
execute if score #tchValideur temp matches 2 run setblock ~ ~-1 ~ air
execute if score #tchValideur temp matches 2 align xyz run summon item ~.5 ~-.5 ~.5 {Tags:["PorteValide"],Age:5999s,NoGravity:1b,PickupDelay:32767s,Item:{id:"dark_oak_button",Count:1b}}

# On réinitialise le score temporaire
scoreboard players reset #tchValideur temp
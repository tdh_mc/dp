# On teste l'âge de l'objet matérialisant la torche
execute store result score #tch temp run data get entity @s Age

# On ferme la porte si cet âge a dépassé 10
execute if score #tch temp matches 10.. run function tch:station/valideur/porte/fermer

# On réinitialise la variable
scoreboard players reset #tch temp
# TCH valideur succès Badge
# (Badge valable et niveau d’accès suffisant)
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Succès de la validation!","color":"green","bold":"true"}]

# On affiche le message
title @p[tag=TestBadge] actionbar [{"text":"Bon travail sur les lignes ","color":"gold"},{"text":"TCH","bold":"true","color":"red"},{"text":" !"}]

# On joue le son de succès
playsound tdh:br.acces.ok voice @a[distance=..30] ~ ~-1 ~

# Si la porte est valide, on ouvre la porte
execute unless entity @e[type=item,tag=PorteValide,distance=..5] run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"","color":"gray"},{"text":"Erreur fatale: ","color":"red","bold":"true"},{"text":"Impossible de trouver l'item porte valide."}]
execute at @e[type=item,tag=PorteValide,distance=..5] run function tch:station/valideur/porte/ouvrir
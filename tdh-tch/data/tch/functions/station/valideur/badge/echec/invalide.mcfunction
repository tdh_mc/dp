# TCH valideur échec Badge
# (ID d'employéE inconnu)
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Niveau d’accès insuffisant.","color":"red"}]

# On affiche le message d'erreur 
title @p[tag=TestBadge] actionbar [{"text":"","color":"red"},{"text":"Erreur","bold":"true"},{"text":" : Votre niveau d’accès n’est pas suffisant."}]

# On joue le son d'échec
playsound tdh:br.acces.refuse voice @a[distance=..30] ~ ~ ~

# On enregistre l'échec chez le joueur
tag @p[tag=TestBadge] add EchecBadge
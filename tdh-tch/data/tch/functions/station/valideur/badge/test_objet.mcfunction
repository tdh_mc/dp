tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test objet --","color":"gray","bold":"true"}]

# Si le joueur a un badge en main, on teste l'ID d’employé :
# On récupère simplement l'ID d’employé, et on le stocke dans l'argument de la fonction de recherche
scoreboard players set #tchPersonnel idAbonnement 0
execute store result score #tchPersonnel idAbonnement run data get entity @p[tag=TestBadge] SelectedItem.tag.tch.id_personnel
execute unless score #tchPersonnel idAbonnement matches 0 run function tch:personnel/_find

# Si la fonction a réussi, on vérifie si l'ID est valide pour cette porte
execute if score #tchPersonnel idAbonnement matches 1 run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Badge détecté!","color":"green"}]
execute if score #tchPersonnel idAbonnement matches 1 run function tch:station/valideur/badge/test_id

# Si elle a échoué, on lance l'échec
execute if score #tchPersonnel idAbonnement matches -1 unless entity @p[tag=TestBadge,tag=EchecBadge] run function tch:station/valideur/badge/echec/inconnu

# Sinon (=on tient autre chose qu'un badge), il ne se passe rien
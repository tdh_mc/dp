
tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"-- Test ID personnel --","color":"gray","bold":"true"}]

# On récupère le niveau d’accès du badge
execute store result score #tchPersonnel niveauAcces run data get storage tch:personnel resultat.acces

# Si ce nombre est supérieur ou égal au niveau d’accès du valideur Badge, on a le droit d’entrer
# On vérifie ensuite que la porte existe
execute if score #tchPersonnel niveauAcces >= @s niveauAcces run tellraw @p[distance=..20,tag=ValideurDebugLog] [{"text":"Le niveau d’accès est suffisant!","color":"green"},{"text":" (","color":"gray","extra":[{"score":{"name":"#tchPersonnel","objective":"niveauAcces"}},{"text":" pour un minimum de "},{"score":{"name":"@s","objective":"niveauAcces"}},{"text":")"}]}]
execute if score #tchPersonnel niveauAcces >= @s niveauAcces run function tch:station/valideur/badge/test_porte

# Sinon, on lance la fonction échec (abonnement expiré)
# On ne la lance pas si le joueur a déjà eu un message d'erreur sur ce valideur
execute unless score #tchPersonnel niveauAcces >= @s niveauAcces unless entity @p[tag=TestBadge,tag=EchecBadge] run function tch:station/valideur/badge/echec/invalide

# On réinitialise la variable temporaire
scoreboard players reset #tchPersonnel niveauAcces
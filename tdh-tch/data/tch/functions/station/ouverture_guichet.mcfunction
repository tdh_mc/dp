# On ouvre le guichet
# On ferme tout passage entre le guichet et le backstage avec des barrières
execute at @s at @e[tag=BarriereGuichet,distance=..150] if block ~ ~1 ~ air run setblock ~ ~1 ~ barrier 

# On téléporte tous les vendeurs TCH dans le guichet
execute at @s as @e[tag=AgentTCH,distance=..150] at @s at @e[tag=Guichet,sort=nearest,limit=1] unless entity @e[tag=InterieurGuichet,distance=...5] run tp @s @e[tag=InterieurGuichet,sort=random,limit=1]

#say J'ouvre mon guichet
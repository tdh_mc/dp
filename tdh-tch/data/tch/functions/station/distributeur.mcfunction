# Distributeur TCH v2.1

# On initialise le joueur
execute at @e[type=item_frame,tag=Distributeur,tag=!ImpressionEnCours,distance=..2,sort=nearest,limit=1] at @p[distance=..2.5,gamemode=!spectator] run tag @p[distance=..0,gamemode=!spectator] add ClientDistributeurInput

# S'il n'y a pas de distributeur, on affiche un message d'erreur
execute unless entity @e[type=item_frame,tag=Distributeur,distance=..2] run function tch:station/distributeur/echec/machine_introuvable

# Sinon, on lance la fonction distributeur
execute as @e[type=item_frame,tag=Distributeur,distance=..2,sort=nearest,limit=1] at @p[tag=ClientDistributeurInput] run function tch:station/distributeur/test_id

# On supprime le tag temporaire
tag @p[tag=ClientDistributeurInput] remove ClientDistributeurInput
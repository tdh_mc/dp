# On calcule le temps d'attente en minutes
scoreboard players set #pid currentMinute 0
execute if score @s spawnAt matches 0.. run scoreboard players operation #pid currentMinute = @s spawnAt
execute unless score @s spawnAt matches 0.. run scoreboard players operation #pid currentMinute = #temps currentTdhTick
# On stocke le tick d'arrivée dans #store currentHour au cas où on aurait besoin de le calculer plus loin
# On ne le calcule pas tout de suite pour économiser des calculs
scoreboard players operation #store currentHour = #pid currentMinute
scoreboard players operation #pid currentMinute -= #temps currentTdhTick
scoreboard players operation #pid idJour = @s idJour
execute if score #pid idJour > #temps idJour run scoreboard players operation #pid currentMinute += #temps fullDayTicks
execute if score #pid currentMinute matches ..0 run scoreboard players set #pid currentMinute 0
# On peut se convaincre assez facilement que min = (t * (min/h)) / (t/h)
scoreboard players operation #pid currentMinute *= #temps minutesPerHour
scoreboard players operation #pid currentMinute /= #temps ticksPerHour
# À l'issue de ce calcul #pid currentMinute contient le nombre de minutes d'attente
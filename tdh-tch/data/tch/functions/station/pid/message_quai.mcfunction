# On récupère notre PC
function tch:tag/pc

# On calcule l'heure et la minute d'arrivée du train
# (#store currentHour a déjà été défini précédemment)
function tdh:store/time

# On affiche un message à tous les joueurs taggés précédemment
execute if score #pid currentMinute matches 1.. run tellraw @a[tag=PIDPlayerTemp] [{"nbt":"Item.tag.display.Name","entity":"@e[type=item_frame,tag=ourPC,limit=1]","interpret":"true","color":"gray"},{"text":" → "},{"nbt":"CustomName","interpret":"true","entity":"@s"},{"text":" : prochain"},{"nbt":"Item.tag.display.line.vehicule.suffixe","entity":"@e[type=item_frame,tag=ourPC,limit=1]"},{"text":" "},{"nbt":"Item.tag.display.line.vehicule.nom","entity":"@e[type=item_frame,tag=ourPC,limit=1]"},{"text":" dans "},{"score":{"name":"#pid","objective":"currentMinute"},"color":"gold","extra":[{"text":" minutes"}]},{"text":" (à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"yellow"},{"text":")."}]
execute if score #pid currentMinute matches ..0 run tellraw @a[tag=PIDPlayerTemp] [{"nbt":"Item.tag.display.Name","entity":"@e[type=item_frame,tag=ourPC,limit=1]","interpret":"true","color":"gray"},{"text":" → "},{"nbt":"CustomName","interpret":"true","entity":"@s"},{"text":" : un"},{"nbt":"Item.tag.display.line.vehicule.suffixe","entity":"@e[type=item_frame,tag=ourPC,limit=1]"},{"text":" "},{"nbt":"Item.tag.display.line.vehicule.nom","entity":"@e[type=item_frame,tag=ourPC,limit=1]"},{"text":" est "},{"text":"en approche","color":"gold"},{"text":"."}]

# On tag notre spawner pour éviter de réécrire plusieurs fois ce message
tag @s add MessagePIDAffiche

# On nettoie le tag de notre PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
# On exécute cette fonction en tant qu'un spawner à sa position
# Peu importe qu'on soit RER câble ou métro, tant qu'on a la variable idLine assignée

# On enregistre notre variable idLine et idStation
scoreboard players operation #pid idLine = @s idLine
# Si on est un spawner de terminus (= qui envoie un véhicule d’un certain idLine en modifiant ledit idLine après l’arrivée en station),
# on enregistre ce futur idLine plutôt que le précédent
execute if score @s prochaineLigne matches 1.. run scoreboard players operation #pid idLine = @s prochaineLigne
scoreboard players operation #pid idStation = @s idStation

# Si notre statut ZeroLocked est factice, on se retire le tag
execute as @s[tag=ZeroLocked] unless entity @p[distance=..150] run tag @s remove ZeroLocked

# On calcule le temps d'attente en minutes
execute as @s[tag=!ZeroLocked] run function tch:station/pid/update/calcul_minutes
execute as @s[tag=ZeroLocked] run scoreboard players set #pid currentMinute 0
# À l'issue de ce calcul #pid currentMinute contient le nombre de minutes d'attente

# On calcule currentMinute modulo 10,
# pour savoir quand afficher les messages dans le chat (toutes les 10mn du coup en l'occurrence)
scoreboard players set #pid temp 10
scoreboard players operation #pid temp2 = #pid currentMinute
scoreboard players operation #pid temp2 %= #pid temp

# On calcule le modèle à afficher en fonction du temps d'attente
# 00-60 = le temps d'attente, qu'on convertit en custommodeldata 100-160
scoreboard players set #pid temp 100
execute if score #pid currentMinute matches 0..59 run scoreboard players operation #pid temp += #pid currentMinute
# Si le temps d’attente est supérieur à 1 h, on affiche 60++ (modèle 160)
execute if score #pid currentMinute matches 60..119 run scoreboard players set #pid temp 160
# Si le temps d’attente est supérieur à 2 h, on indique la fin de service (modèle 198)
execute unless score #pid currentMinute matches 0..120 run scoreboard players set #pid temp 198

# On modifie les modèles en tant que chaque item frame "PID" dont le numéro de ligne correspond,
# et on tag les joueurs situés à proximité
execute as @e[type=item_frame,tag=PID] if score @s idLine = #pid idLine if score @s idStation = #pid idStation at @s run function tch:station/pid/update_pid

# On lance le message du PID si on est à 0mn
execute as @s[tag=!MessagePIDAffiche] if score #pid currentMinute matches ..0 run function tch:station/annonce/pid/gen_id
# On affiche un message aux joueurs toutes les 10mn d'attente
execute as @s[tag=!MessagePIDAffiche] if score #pid temp2 matches 0 if score #pid currentMinute matches ..60 run function tch:station/pid/message_quai
# Si on n'est pas à un multiple de 10mn d'attente, on clear notre tag
execute unless score #pid temp2 matches 0 run tag @s remove MessagePIDAffiche

# On supprime les tags temporaires "PIDPlayerTemp" donnés aux joueurs
tag @a[tag=PIDPlayerTemp] remove PIDPlayerTemp
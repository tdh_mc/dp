# On exécute cette fonction en tant qu'une item frame PID, à sa position,
# pour mettre à jour le temps d'attente affiché visuellement et éventuellement afficher un message si le nombre de minutes est entier

# Les variables suivantes sont définies :
# #pid temp contient le CustomModelData à afficher pour le PID
# #pid currentMinute contient le nombre de minutes d'attente
# #pid temp2 contient le nombre de minutes d'attente %10

# On met à jour le modèle
# Comme on a 2 types de SACD différents (principal et secondaire),
# on a 2 CustomModelData différents, qu'on filtre simplement en fonction du modèle actuel
execute store result score #pid temp3 run data get entity @s Item.tag.CustomModelData
scoreboard players operation #pid temp4 = #pid temp
# Si le modèle actuel a un ID > 1000, c'est un SACD secondaire, et on ajoute 1000 à notre score avant de l'ajouter
execute if score #pid temp3 matches 1000.. store result entity @s Item.tag.CustomModelData int 1 run scoreboard players add #pid temp4 1000
# Dans le cas contraire, c'est un SACD principal, et on assigne normalement le modèle
execute unless score #pid temp3 matches 1000.. store result entity @s Item.tag.CustomModelData int 1 run scoreboard players get #pid temp4

# Si le temps d'attente est un multiple de 10, on tag tous les joueurs situés à proximité pour qu'on leur affiche le message dans la fonction principale
# On vérifie d'abord si on a *dépassé* un multiple de 10 pour éviter, dans les timeflows trop rapides, de "louper" cet affichage en raison de l'absence de calcul à la bonne minute
scoreboard players set #pid temp4 10
scoreboard players operation #pid temp5 = #pid temp3
scoreboard players operation #pid temp5 %= #pid temp4
scoreboard players operation #pid temp4 = #pid temp2
execute if score #pid temp5 matches 1.. if score #pid temp2 > #pid temp5 run scoreboard players set #pid temp4 0
# On est obligés de le faire en 2 fois car on ne peut pas faire autre chose qu'un carré sans devoir filtrer selon l'orientation
execute unless score #pid temp3 matches 1000.. run function tch:station/pid/tag_joueur
execute if score #pid temp3 matches 1000.. run function tch:station/pid/tag_joueur2
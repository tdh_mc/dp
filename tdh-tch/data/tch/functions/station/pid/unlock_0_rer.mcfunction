# On appelle cette fonction en tant qu'un véhicule (métro, câble, RER, peu importe) pour signaler qu'on libère la voie et que le temps d'attente n'a plus besoin d'être verrouillé sur 0
scoreboard players operation #pid idLine = @s idLine
execute as @e[type=armor_stand,tag=SpawnRER,distance=..200] if score @s idLine = #pid idLine run tag @s remove ZeroLocked
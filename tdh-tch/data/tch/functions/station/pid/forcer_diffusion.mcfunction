# On exécute cette fonction en tant qu'un véhicule (tous types confondus) à sa position,
# pour forcer la diffusion d'un message "Prochain train dans une minute..." à quai

# On tag notre spawner
function tch:tag/spawner

# On diffuse le message en tant que ce spawner
execute as @e[type=#tch:marqueur/spawner,tag=ourSpawner] at @s run function tch:station/annonce/pid/gen_id

# On retire le tag temporaire du spawner
tag @e[type=#tch:marqueur/spawner,tag=ourSpawner] remove ourSpawner
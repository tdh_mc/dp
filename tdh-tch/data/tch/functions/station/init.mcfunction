# Paramètres des stations partagés avec les PC lignes
scoreboard objectives add debutService dummy "Heure de début de service"
scoreboard objectives add debutServiceWe dummy "Modificateur d'heure de début de service le week-end"
scoreboard objectives add debutServiceAjd dummy "Heure de début de service aujourd'hui"
scoreboard objectives add finService dummy "Heure de fin de service"
scoreboard objectives add finServiceWe dummy "Modificateur d'heure de fin de service le week-end"
scoreboard objectives add finServiceAjd dummy "Heure de fin de service aujourd'hui"

# Paramètres spécifiques des stations
scoreboard objectives add debutGuichet dummy "Heure d'ouverture du guichet"
scoreboard objectives add debutGuichetWe dummy "Heure d'ouverture du guichet le week-end"
scoreboard objectives add debutGuichetAjd dummy "Heure d'ouverture du guichet aujourd'hui"
scoreboard objectives add finGuichet dummy "Heure de fermeture du guichet"
scoreboard objectives add finGuichetWe dummy "Heure de fermeture du guichet le week-end"
scoreboard objectives add finGuichetAjd dummy "Heure de fermeture du guichet aujourd'hui"
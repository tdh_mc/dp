# On appelle cette fonction pour trouver une connexion spécifique dans un nœud du système d'itinéraire
# Il suffit d'exécuter cette fonction en tant que le nœud, et de définir les objectifs suivants :
# #tchItineraire idStation, idLine, idSortie (y compris ceux qui doivent ne pas exister : =0)
# En revanche ceux qui peuvent avoir n'importe quelle valeur (qu'on recherche) ne doivent pas exister, ou bien être définis à une valeur <0 (par exemple -1)

# On reset un éventuel tag ConnexionTrouvee oublié
tag @s remove ConnexionTrouvee

# On compte le nombre de connexions du noeud
execute store result score #tchItineraire nombre run data get entity @s Item.tag.tch.connexions

# Tant qu'il reste des connexions, on vérifie la première, et si elle ne matche pas, on la déplace à la fin
execute if score #tchItineraire nombre matches 1.. run function tch:itineraire/noeud/trouver_connexion/test_connexion

# On nettoie les variables
scoreboard players reset #tchItineraire nombre
# On garde ConnexionTrouvee pour servir de "valeur de retour"
# On décrémente la variable de contrôle
scoreboard players remove #tchItineraire nombre 1

# On copie les données de la première connexion de la liste
execute store result score #tchItineraire prochaineStation run data get entity @s Item.tag.tch.connexions[0].station
execute store result score #tchItineraire prochaineSortie run data get entity @s Item.tag.tch.connexions[0].sortie
execute store result score #tchItineraire prochaineLigne run data get entity @s Item.tag.tch.connexions[0].line

# Si toutes les données matchent, on se donne un tag
tag @s add ConnexionTrouvee
execute if entity @s[tag=ConnexionTrouvee] if score #tchItineraire idStation matches 0.. unless score #tchItineraire prochaineStation = #tchItineraire idStation run tag @s remove ConnexionTrouvee
execute if entity @s[tag=ConnexionTrouvee] if score #tchItineraire idLine matches 0.. unless score #tchItineraire prochaineLigne = #tchItineraire idLine run tag @s remove ConnexionTrouvee
execute if entity @s[tag=ConnexionTrouvee] if score #tchItineraire idSortie matches 0.. unless score #tchItineraire prochaineSortie = #tchItineraire idSortie run tag @s remove ConnexionTrouvee

# Si on n'a pas trouvé la connexion, on copie la connexion à la fin et on recommence la fonction
execute as @s[tag=!ConnexionTrouvee] if score #tchItineraire nombre matches 1.. run function tch:itineraire/noeud/trouver_connexion/prochaine_connexion
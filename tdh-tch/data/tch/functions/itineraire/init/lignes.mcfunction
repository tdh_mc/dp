# On initialise toutes les lignes (tchPC) avec les bons détails de ligne
# On définit la variable "voix" et également "voixMax", qui définissent la ou les voix autorisées pour la diffusion des annonces "arrivee_station1" et "arrivee_station2"

# Une "voix" ne définit pas nécessairement une voix unique, mais une station donnée à une voix donnée ne peut pas avoir plusieurs "variantes" : il s'agit toujours des 2 mêmes fichiers son respectivement pour 1 et 2.
# Par conséquent sur le réseau métro, il faut veiller à ce que 2 lignes qui correspondent l'une avec l'autre ne partagent pas la même voix.

# On définit la variable "idTerminus" qui pour un PC de ligne détermine les ID de sons "terminus" pouvant être joués aux terminus de cette ligne

# Lignes de métro
# Métro 1 : Voix 1
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=11}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a21","color":"#ffcd00"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a21","color":"#ffcd00"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#ffcd00"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=11}] voix 1
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=11}] idTerminus 1

# Métro 2 : Voix 2
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=21}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a22","color":"#003ca6"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a22","color":"#003ca6"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#003ca6"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=21}] voix 2
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=21}] idTerminus 2

# Métro 3 : Voix 3
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=31}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a23","color":"#6ec4e8"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a23","color":"#6ec4e8"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#6ec4e8"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=31}] voix 3
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=31}] idTerminus 3

# Métro 4 : Voix 4
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=41}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a24","color":"#cf009e"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a24","color":"#cf009e"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#cf009e"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=41}] voix 4
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=41}] idTerminus 4

# Métros 5a/5b : Voix 5
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=51}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a25","color":"#ff7e2e"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a25","color":"#ff7e2e"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#ff7e2e"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=51}] voix 5
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=51}] idTerminus 5

data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=55}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a26","color":"#ff7e2e"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a26","color":"#ff7e2e"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#ff7e2e"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=55}] voix 5
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=55}] idTerminus 5

# Métro 6 : Voix 4
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=61}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a27","color":"#60bb39"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a27","color":"#60bb39"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#60bb39"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=61}] voix 4
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=61}] idTerminus 6

# Métro 7 : Voix 1
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=71}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a28","color":"#fa9aba"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a28","color":"#fa9aba"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#fa9aba"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=71}] voix 1
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=71}] idTerminus 2

# Métro 7bis : Voix 3
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=73}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a29","color":"#6eca97"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a29","color":"#6eca97"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#6eca97"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=73}] voix 3
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=73}] idTerminus 7

# Métro 8 : Voix 1
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=81}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2a","color":"#e19bdf"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2a","color":"#e19bdf"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#e19bdf"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=81}] voix 1
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=81}] idTerminus 3

# Métro 8bis : Voix 5
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=83}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2b","color":"#345663"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2b","color":"#345663"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#345663"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=83}] voix 5
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=83}] idTerminus 7

# Métro 9 : Voix 1
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=91}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2c","color":"#b6bd00"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2c","color":"#b6bd00"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#b6bd00"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=91}] voix 1
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=91}] idTerminus 4

# Métro 10 : Voix 1
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=101}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2d","color":"#c9910d"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2d","color":"#c9910d"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#c9910d"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=101}] voix 1
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=101}] idTerminus 1

# Métro 11 : Voix 3
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=111}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2e","color":"#704b1c"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2e","color":"#704b1c"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#704b1c"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=111}] voix 3
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=111}] idTerminus 6

# Métro 12 : Voix 3
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=121}] Item.tag.display set value {Name:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2f","color":"#007852"}]',line:{nom:'[{"text":"\\u1a20","color":"gray"},{"text":"\\u1a2f","color":"#007852"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#007852"}]',vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=121}] voix 3
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=121}] idTerminus 5


# Lignes de RER
# RER A : Voix 4
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=2100}] Item.tag.display set value {Name:'[{"text":"\\u1a38","color":"gray"},{"text":"\\u1a39","color":"#e2231a"}]',line:{nom:'[{"text":"\\u1a38","color":"gray"},{"text":"\\u1a39","color":"#e2231a"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#e2231a"}]',vehicule:{nom:"train",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=2100}] voix 4
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=2100}] idTerminus 1

# RER B : Voix 6
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=2200}] Item.tag.display set value {Name:'[{"text":"\\u1a38","color":"gray"},{"text":"\\u1a3a","color":"#3c91dc"}]',line:{nom:'[{"text":"\\u1a38","color":"gray"},{"text":"\\u1a3a","color":"#3c91dc"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#3c91dc"}]',vehicule:{nom:"train",deDu:"du ",alAu:"au ",prefixe:"le ",suffixe:""}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=2200}] voix 6
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=2200}] idTerminus 2


# Lignes de câble
# Câble A : Voix 1
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=4011}] Item.tag.display set value {Name:'[{"text":"\\u1a40","color":"gray"},{"text":"\\u1a41","color":"#4b7e99"}]',line:{nom:'[{"text":"\\u1a40","color":"gray"},{"text":"\\u1a41","color":"#4b7e99"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#4b7e99"}]',vehicule:{nom:"cabine",deDu:"de la ",alAu:"à la ",prefixe:"la ",suffixe:"e"}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=4011}] voix 1
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=4011}] idTerminus 1

# Câble B : Voix 2 à 4
data modify entity @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=4021}] Item.tag.display set value {Name:'[{"text":"\\u1a40","color":"gray"},{"text":"\\u1a42","color":"#c39108"}]',line:{nom:'[{"text":"\\u1a40","color":"gray"},{"text":"\\u1a42","color":"#c39108"}]',terminus:'[{"storage":"tch:itineraire","nbt":"terminus.station","color":"#c39108"}]',vehicule:{nom:"cabine",deDu:"de la ",alAu:"à la ",prefixe:"la ",suffixe:"e"}}}
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=4021}] voix 2
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=4021}] voixMax 4
scoreboard players set @e[type=item_frame,tag=tchPC,limit=1,scores={idLine=4021}] idTerminus 2
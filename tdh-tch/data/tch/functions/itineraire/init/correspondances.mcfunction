# Cette fonction est exécutée par un tchQuaiPC pour initialiser le texte des correspondances, à sa position
# (par exemple, au Hameau M2, le texte dira "M1, M3 et M4" avec le bon formatage et codes couleurs)

# On donne un tag temporaire à tous les quais des autres lignes passant à la même station
scoreboard players operation #tch idStation = @s idStation
execute as @e[type=item_frame,tag=tchQuaiPC] if score @s idStation = #tch idStation run tag @s add tempOurStation
tag @s remove tempOurStation

# On vérifie si notre station comporte une liaison intérieure ;
# (on renvoie l'ID de station dans la variable #tch idStation, si on en a trouvé un)
execute as @e[type=item_frame,tag=tchStationPC] if score @s idStation = #tch idStation run function tch:itineraire/init/correspondances/test_liaison_interieure
# Si oui, on ajoute le tag également aux lignes qui passent dans cette seconde station
execute unless score #tch idStation = @s idStation as @e[type=item_frame,tag=tchQuaiPC] if score @s idStation = #tch idStation run tag @s add tempOurStation

# On initialise la liste du storage
# (on la copiera après dans les données de l'entité)
data modify storage tch:itineraire pc.corresp set value []

# On trie ensuite ces quais par ordre alphabétique en utilisant la variable temp
function tch:itineraire/init/correspondances/tri

# Une fois le calcul terminé, on copie la liste initialisée dans nos données d'entité
data modify entity @s Item.tag.display.corresp set from storage tch:itineraire pc.corresp

# On supprime le tag temporaire des quais des autres lignes
# (Il ne devrait pas en rester, mais c'est par sécurité)
tag @e[type=item_frame,tag=tempOurStation] remove tempOurStation
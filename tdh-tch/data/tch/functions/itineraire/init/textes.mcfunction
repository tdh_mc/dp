# On génère les textes de correspondance de chaque station
execute as @e[type=item_frame,tag=tchQuaiPC] at @s run function tch:itineraire/init/correspondances
# On génère également les détails répétitifs de chaque quai (détails des lignes et des stations)
execute as @e[type=item_frame,tag=tchQuaiPC] at @s run function tch:itineraire/init/details
execute as @e[type=item_frame,tag=tchSortiePC] at @s run function tch:itineraire/init/details_sortie

# On copie le nom des lignes (stocké dans les noms des items) dans les item frames elles-mêmes
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchPC] run function tch:itineraire/init/nom_pc/ligne
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchSortiePC] run function tch:itineraire/init/nom_pc/sortie
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchStationPC] run function tch:itineraire/init/nom_pc/station
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] as @e[type=item_frame,tag=tchQuaiPC] run function tch:itineraire/init/nom_pc/quai
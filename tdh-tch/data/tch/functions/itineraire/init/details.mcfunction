# Cette fonction est exécutée par un tchQuaiPC pour initialiser le texte des détails, à sa position
# (par exemple, au Hameau M2, on récupèrera les détails du Hameau ("Hameau", "Le", "du ", "au "),
# 	ainsi que les détails de la ligne ("M2", "métro")... )

# On tag notre hub station et notre PC
function tch:tag/pc
function tch:tag/station_pc

# On copie les données de notre hub
data modify entity @s Item.tag.display.station set from entity @e[type=item_frame,tag=ourStationPC,limit=1] Item.tag.display.station
data modify entity @s Item.tag.display.Name set from entity @e[type=item_frame,tag=ourStationPC,limit=1] Item.tag.display.Name
# On copie les données de notre PC ligne
data modify entity @s Item.tag.display.line set from entity @e[type=item_frame,tag=ourPC,limit=1] Item.tag.display.line

# On supprime le tag temporaire
tag @e[type=item_frame,tag=ourStationPC] remove ourStationPC
tag @e[type=item_frame,tag=ourPC] remove ourPC
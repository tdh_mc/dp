# On supprime les éventuelles infos de terminus précédentes
execute as @e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=1..}] run data remove entity @s Item.tag.tch.give
scoreboard players reset @e[type=item_frame,tag=tchQuaiPC] idTerminus
scoreboard players reset @e[type=item_frame,tag=tchQuaiPC] idTerminusMax


# On procède station par station pour initialiser les quais

### STATION LE HAMEAU RIVE GAUCHE
# Hub Hameau (=> Hub HameauRD, Hameau M1/M2/M3, Hameau S1/S2/S3/S4)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1000}] Item.tag.tch set value {position:{x:70,z:190},connexions:[{station:1002,distance:31,temps:234},{line:11,distance:67,temps:432},{line:21,distance:41,temps:324},{line:31,distance:41,temps:324},{sortie:1,distance:113,temps:576},{sortie:2,distance:28,temps:180},{sortie:3,distance:147,temps:756},{sortie:4,distance:176,temps:900}]}

# Hameau M1 (=> Hub Hameau, Hameau M2/M3, Knosos M1, HameauPort M1)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1000,idLine=11}] Item.tag.tch set value {connexions:[{distance:72,temps:360},{line:21,distance:85,temps:504},{line:31,distance:104,temps:576},{line:12,station:1020,distance:523,temps:1269},{line:11,station:1004,distance:199,temps:457}]}

# Hameau M2 (=> Hub Hameau, Hameau M1/M3, Hameau Gbrn M2, Kraken M2)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1000,idLine=21}] Item.tag.tch set value {connexions:[{distance:89,temps:432},{line:11,distance:74,temps:432},{line:31,distance:85,temps:504},{line:22,station:1001,distance:222,temps:520},{line:21,station:1010,distance:550,temps:1309}]}

# Hameau M3 (=> Hub Hameau, Hameau M1/M2, Verchamps M3, Jeej M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1000,idLine=31}] Item.tag.tch set value {connexions:[{distance:108,temps:540},{line:11,distance:95,temps:540},{line:21,distance:74,temps:432},{line:32,station:1030,distance:273,temps:611},{line:31,station:1155,distance:616,temps:1530}]}

# Hameau S1 (=> Hub Hameau)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1000,idSortie=1}] Item.tag.tch set value {position:{x:70,z:194},connexions:[{distance:14,temps:180}]}
# Hameau S2 (=> Hub Hameau)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1000,idSortie=2}] Item.tag.tch set value {position:{x:106,z:241},connexions:[{distance:92,temps:576}]}
# Hameau S3 (=> Hub Hameau)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1000,idSortie=3}] Item.tag.tch set value {position:{x:122,z:177},connexions:[{distance:95,temps:612}]}
# Hameau S4 (=> Hub Hameau)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1000,idSortie=4}] Item.tag.tch set value {position:{x:65,z:116},connexions:[{distance:83,temps:540}]}


### STATION LE HAMEAU GLANDEBRUINE NORD
# Hub Hameau Gbrn (=> Hameau Gbrn M2/M4, Hameau Gbrn S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1001}] Item.tag.tch set value {position:{x:92,z:-5},connexions:[{line:21,distance:90,temps:581},{line:41,distance:82,temps:662},{sortie:1,distance:44,temps:286}]}

# Hameau Gbrn M2 (=> Hub Hameau Gbrn, Hameau Gbrn M4, Hameau M2, AlwinOnyx M2)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1001,idLine=21}] Item.tag.tch set value {connexions:[{distance:77,temps:500},{line:41,distance:83,temps:592},{line:21,station:1000,distance:222,temps:529},{line:22,station:1360,distance:506,temps:1250}]}

# Hameau Gbrn M4 (=> Hub Hameau Gbrn, Hameau Gbrn M2, Verchamps M4, Knosos M4)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1001,idLine=41}] Item.tag.tch set value {connexions:[{distance:100,temps:646},{line:21,distance:160,temps:1064},{line:42,station:1030,distance:264,temps:702},{line:41,station:1020,distance:465,temps:1117}]}

# Hameau Gbrn S1 (=> Hub Hameau Gbrn)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1001,idSortie=1}] Item.tag.tch set value {position:{x:69,z:12},connexions:[{distance:34,temps:252}]}

### STATION LE HAMEAU RIVE DROITE
# Hub HameauRD (=> Hub Hameau, Hameau RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1002}] Item.tag.tch set value {position:{x:70,z:190},connexions:[{station:1000,distance:121,temps:766},{line:2100,distance:44,temps:414}]}

# HameauRD RERA (=> Hub HameauRD, Sablons–Brimiel RERA, Villonne RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1002,idLine=2100}] Item.tag.tch set value {connexions:[{distance:115,temps:734},{line:2153,station:1370,distance:1291,temps:1999},{line:2154,station:1370,distance:1291,temps:1999},{line:2101,station:1150,distance:1006,temps:1360},{line:2102,station:1150,distance:1006,temps:1360}]}

### STATION LE HAMEAU PORT
# Hub HameauPort (=> HameauPort M1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1004}] Item.tag.tch set value {position:{x:0,z:0},connexions:[{line:11,distance:66,temps:666}]}

# HameauPort M1 (=> Hub HameauPort, Hameau M1, Krypt M1)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1004,idLine=11}] Item.tag.tch set value {connexions:[{distance:115,temps:734},{line:12,station:1000,distance:203,temps:467},{line:11,station:1490,distance:904,temps:2005}]}

### STATION KRAKEN
# Hub Kraken (=> Kraken M2, Kraken S1
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1010}] Item.tag.tch set value {position:{x:548,z:346},connexions:[{line:21,distance:148,temps:962},{sortie:1,distance:40,temps:260}]}

# Kraken M2 (=> Hub Kraken, Hameau M2, ZobunEclesta M2)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1010,idLine=21}] Item.tag.tch set value {connexions:[{distance:149,temps:968},{line:22,station:1000,distance:549,temps:1307},{line:21,station:1220,distance:636,temps:1564}]}

# Kraken S1 (=> Hub Kraken, LD CMP, LD Int R102/R103)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1010,idSortie=1}] Item.tag.tch set value {position:{x:524,z:431},connexions:[{distance:32,temps:208},{station:99016,distance:24,temps:156},{station:99017,distance:59,temps:383}]}

### STATION KNOSOS
# Hub Knosos (=> Knosos M1/M4, Knosos S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1020}] Item.tag.tch set value {position:{x:500,z:128},connexions:[{line:11,distance:75,temps:487},{line:41,distance:84,temps:614},{sortie:1,distance:99,temps:643},{sortie:2,distance:47,temps:305}]}

# Knosos M1 (=> Hub Knosos, Knosos M4, Hameau M1, Ygriak M1)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1020,idLine=11}] Item.tag.tch set value {connexions:[{distance:116,temps:754},{line:41,distance:104,temps:610},{line:11,station:1000,distance:533,temps:1289},{line:12,station:1210,distance:697,temps:1497}]}

# Knosos M4 (=> Hub Knosos, Knosos M1, Hameau Gbrn M4, Ygriak M4)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1020,idLine=41}] Item.tag.tch set value {connexions:[{distance:126,temps:854},{line:11,distance:104,temps:610},{line:42,station:1001,distance:473,temps:1089},{line:41,station:1210,distance:700,temps:1563}]}

# Knosos S1 (=> Hub Knosos, LD MinesTotem)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1020,idSortie=1}] Item.tag.tch set value {position:{x:513,z:197},connexions:[{distance:94,temps:611},{station:99015,distance:23,temps:149}]}
# Knosos S2 (=> Hub Knosos)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1020,idSortie=2}] Item.tag.tch set value {position:{x:510,z:89},connexions:[{distance:38,temps:247}]}

### STATION VERCHAMPS
# Hub Verchamps (=> Verchamps M3/M4, Verchamps S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1030}] Item.tag.tch set value {position:{x:-129,z:93},connexions:[{line:31,distance:45,temps:292},{line:41,distance:74,temps:481},{sortie:1,distance:14,temps:91}]}

# Verchamps M3 (=> Hub Verchamps, Verchamps M4, Hameau M3, Sablons M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1030,idLine=31}] Item.tag.tch set value {connexions:[{distance:53,temps:344},{line:41,distance:158,temps:1027},{line:31,station:1000,distance:266,temps:604},{line:32,station:1300,distance:716,temps:1763}]}

# Verchamps M4 (=> Hub Verchamps, Verchamps M3, Hameau Gbrn M4, Illysia M4)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1030,idLine=41}] Item.tag.tch set value {connexions:[{distance:82,temps:533},{line:31,distance:166,temps:1079},{line:41,station:1001,distance:259,temps:523},{line:42,station:1420,distance:615,temps:1728}]}

# Verchamps S1 (=> Hub Verchamps)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1030,idSortie=1}] Item.tag.tch set value {position:{x:-139,z:73},connexions:[{distance:14,temps:91}]}

### STATION GRENAT VILLE
# Hub Grenat (=> Grenat M1/M5a/RERA, Grenat S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1100}] Item.tag.tch set value {position:{x:-649,z:1245},connexions:[{line:11,distance:65,temps:422},{line:51,distance:93,temps:604},{line:2100,distance:83,temps:556},{sortie:1,distance:53,temps:344},{sortie:2,distance:196,temps:1274},{sortie:3,distance:171,temps:1111}]}

# Grenat M1 (=> Hub Grenat, Grenat M5a, Grenat RERA, GrenatPort M1)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1100,idLine=11}] Item.tag.tch set value {connexions:[{distance:86,temps:559},{line:51,distance:90,temps:585},{line:2100,distance:155,temps:956},{line:12,station:1102,distance:216,temps:556}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1100,idLine=11}] idTerminus 11

# Grenat M5a (=> Hub Grenat, Grenat M1, Grenat RERA, AsvaardRudelieu M5a, GrenatPort M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1100,idLine=51}] Item.tag.tch set value {connexions:[{distance:104,temps:676},{line:11,distance:89,temps:578},{line:2100,distance:167,temps:1069},{line:51,station:1115,distance:484,temps:1141},{line:52,station:1102,distance:211,temps:454}]}

# Grenat RERA (=> Hub Grenat, Grenat M1, Grenat M5a, Villonne RERA, PortAuxPendus RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1100,idLine=2100}] Item.tag.tch set value {connexions:[{distance:106,temps:671},{line:11,distance:219,temps:1344},{line:51,distance:170,temps:1087},{line:2153,station:1150,distance:2130,temps:1999},{line:2154,station:1150,distance:2130,temps:1999},{line:2101,station:2340,distance:3226,temps:1999}]}

# Grenat S1 (=> Hub Grenat)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1100,idSortie=1}] Item.tag.tch set value {position:{x:-657,z:1153},connexions:[{distance:47,temps:305}]}
# Grenat S2 (=> Hub Grenat)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1100,idSortie=2}] Item.tag.tch set value {position:{x:-640,z:1327},connexions:[{distance:190,temps:1235}]}
# Grenat S3 (=> Hub Grenat)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1100,idSortie=3}] Item.tag.tch set value {position:{x:-599,z:1268},connexions:[{distance:162,temps:1053}]}

### STATION GRENAT PORT
# Hub GrenatPort (=> GrenatPort M1/M5a, GrenatPort S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1102}] Item.tag.tch set value {position:{x:-908,z:1521},connexions:[{line:11,distance:1,temps:1},{line:51,distance:1,temps:1},{sortie:1,distance:1,temps:1},{sortie:2,distance:1,temps:1}]}

# GrenatPort M1 (=> Hub GrenatPort, GrenatPort M5a, Grenat M1, Krypt M1)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1102,idLine=11}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:51,distance:1,temps:1},{line:11,station:1100,distance:209,temps:444},{line:12,station:1490,distance:1288,temps:2816}]}

# GrenatPort M5a (=> Hub GrenatPort, GrenatPort M1, Grenat M5a, Arithmatie M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1102,idLine=51}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:11,distance:1,temps:1},{line:51,station:1100,distance:214,temps:456},{line:52,station:1110,distance:815,temps:1758}]}

# GrenatPort S1 (=> Hub GrenatPort)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1102,idSortie=1}] Item.tag.tch set value {position:{x:-898,z:1512},connexions:[{distance:1,temps:1}]}
# GrenatPort S2 (=> Hub GrenatPort)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1102,idSortie=2}] Item.tag.tch set value {position:{x:-976,z:1551},connexions:[{distance:1,temps:1}]}

### STATION GRENAT RECIF
# Hub Recif (=> Recif S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1105}] Item.tag.tch set value {position:{x:19,z:1245},connexions:[{sortie:1,distance:44,temps:286},{sortie:2,distance:78,temps:507},{sortie:3,distance:98,temps:637}]}

# Recif S1 (=> Hub Recif, LD Sanor)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1105,idSortie=1}] Item.tag.tch set value {position:{x:57,z:1309},connexions:[{distance:30,temps:195},{station:99004,distance:36,temps:234}]}
# Recif S2 (=> Hub Recif, LD Preskrik)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1105,idSortie=2}] Item.tag.tch set value {position:{x:15,z:1269},connexions:[{distance:64,temps:416},{station:99005,distance:17,temps:110}]}
# Recif S3 (=> Hub Recif, LD Recif)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1105,idSortie=3}] Item.tag.tch set value {position:{x:21,z:1201},connexions:[{distance:70,temps:455},{station:99006,distance:1,temps:1}]}

### STATION GRENAT ARITHMATIE
# Hub Arithmatie (=> Arithmatie M5a)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1110}] Item.tag.tch set value {position:{x:-1410,z:1045},connexions:[{line:51,distance:67,temps:488}]}

# Arithmatie M5a (=> Hub Arithmatie, GrenatPort M5a, FortDuVal M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1110,idLine=51}] Item.tag.tch set value {connexions:[{distance:73,temps:437},{line:51,station:1102,distance:811,temps:1748},{line:52,station:1400,distance:468,temps:1118}]}

### STATION ASVAARD RUDELIEU
# Hub AsvaardRudelieu (=> AsvaardRudelieu M5a)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1115}] Item.tag.tch set value {position:{x:-1000,z:1023},connexions:[{line:51,distance:34,temps:258}]}

# AsvaardRudelieu M5a (=> Hub AsvaardRudelieu, Grenat M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1115,idLine=51}] Item.tag.tch set value {connexions:[{distance:67,temps:400},{line:52,station:1100,distance:479,temps:1131}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1115,idLine=51}] idTerminus 51

### STATION VILLONNE
# Hub Villonne (=> Villonne M3/M5b/RERA, Villonne S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1150}] Item.tag.tch set value {position:{x:610,z:1059},connexions:[{line:31,distance:48,temps:312},{line:55,distance:85,temps:552},{line:2100,distance:131,temps:832},{sortie:1,distance:72,temps:468},{sortie:2,distance:72,temps:440}]}

# Villonne M3 (=> Hub Villonne, Villonne M5b, Jeej M3, Duerrom M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1150,idLine=31}] Item.tag.tch set value {connexions:[{distance:60,temps:390},{line:55,distance:666,temps:1999},{line:32,station:1155,distance:762,temps:1883},{line:31,station:1180,distance:909,temps:1908}]}

# Villonne M5b (=> Hub Villonne, Villonne M3, EvenisTamlyn M5b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1150,idLine=55}] Item.tag.tch set value {connexions:[{distance:96,temps:624},{line:31,distance:666,temps:1999},{line:56,station:1520,distance:968,temps:2417}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1150,idLine=55}] idTerminus 55

# Villonne RERA (=> Hub Villonne, Hameau RERA, Béothas RERA, Grenat RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1150,idLine=2100}] Item.tag.tch set value {connexions:[{distance:132,temps:820},{line:2153,station:1002,distance:1006,temps:1321},{line:2154,station:1002,distance:1006,temps:1321},{line:2102,station:1160,distance:1050,temps:1290},{line:2101,station:1100,distance:2130,temps:1999}]}

# Villonne S1 (=> Hub Villonne)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1150,idSortie=1}] Item.tag.tch set value {position:{x:626,z:1070},connexions:[{distance:72,temps:468}]}
# Villonne S2 (=> Hub Villonne)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1150,idSortie=2}] Item.tag.tch set value {position:{x:577,z:1045},connexions:[{distance:72,temps:440}]}

### STATION FORET DE JEEJ
# Hub Jeej (=> Jeej M3, Jeej S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1155}] Item.tag.tch set value {position:{x:103,z:819},connexions:[{line:31,distance:38,temps:247},{sortie:1,distance:56,temps:364},{sortie:2,distance:56,temps:364}]}

# Jeej M3 (=> Hub Jeej, Hameau M3, Villonne M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1155,idLine=31}] Item.tag.tch set value {connexions:[{distance:47,temps:305},{line:32,station:1000,distance:618,temps:1537},{line:31,station:1150,distance:766,temps:1892}]}

# Jeej S1 (=> Hub Jeej)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1155,idSortie=1}] Item.tag.tch set value {position:{x:71,z:856},connexions:[{distance:50,temps:325}]}
# Jeej S2 (=> Hub Jeej, LD BaseNautiqueJeej)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1155,idSortie=2}] Item.tag.tch set value {position:{x:111,z:790},connexions:[{distance:56,temps:364},{station:99021,distance:14,temps:91}]}

### STATION BÉOTHAS
# Hub Béothas (=> Béothas RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1160}] Item.tag.tch set value {position:{x:636,z:2052},connexions:[{line:2100,distance:56,temps:369}]}

# Béothas RERA (=> Hub Béothas, Villonne RERA, Roustiflet RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1160,idLine=2100}] Item.tag.tch set value {connexions:[{distance:70,temps:478},{line:2153,station:1150,distance:1050,temps:1109},{line:2154,station:1150,distance:1050,temps:1109},{line:2102,station:1800,distance:2198,temps:1942}]}

### STATION TOLBROK
# Hub Tolbrok (=> Tolbrok M3/M7, Tolbrok S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1170}] Item.tag.tch set value {position:{x:1761,z:1656},connexions:[{line:31,distance:80,temps:520},{line:71,distance:61,temps:396},{sortie:1,distance:45,temps:292}]}

# Tolbrok M3 (=> Hub Tolbrok, Tolbrok M7, Duerrom M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1170,idLine=31}] Item.tag.tch set value {connexions:[{distance:64,temps:416},{line:71,distance:119,temps:773},{line:32,station:1180,distance:641,temps:1399}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1170,idLine=31}] idTerminus 31

# Tolbrok M7 (=> Hub Tolbrok, Tolbrok M4, EvenisTamlyn M7, Néronil M7)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1170,idLine=71}] Item.tag.tch set value {connexions:[{distance:72,temps:468},{line:31,distance:120,temps:780},{line:72,station:1520,distance:715,temps:1723},{line:71,station:1500,distance:599,temps:1311}]}

# Tolbrok S1 (=> Hub Tolbrok)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1170,idSortie=1}] Item.tag.tch set value {position:{x:1775,z:1657},connexions:[{distance:50,temps:325}]}

### STATION DUERROM
# Hub Duerrom (=> Duerrom M3, Duerrom S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1180}] Item.tag.tch set value {position:{x:1348,z:1382},connexions:[{line:31,distance:126,temps:819},{sortie:1,distance:16,temps:104}]}

# Duerrom M3 (=> Hub Duerrom, Villonne M3, Tolbrok M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1180,idLine=31}] Item.tag.tch set value {connexions:[{distance:55,temps:357},{line:32,station:1150,distance:907,temps:1904},{line:31,station:1170,distance:634,temps:1514}]}

# Duerrom S1 (=> Hub Duerrom)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1180,idSortie=1}] Item.tag.tch set value {position:{x:1361,z:1381},connexions:[{distance:16,temps:104}]}

### STATION DORLINOR
# Hub Dorlinor (=> Dorlinor M1/M5b/M6, Dorlinor S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1200}] Item.tag.tch set value {position:{x:1566,z:-522},connexions:[{line:11,distance:90,temps:585},{line:55,distance:89,temps:595},{line:61,distance:140,temps:850},{sortie:1,distance:32,temps:208}]}

# Dorlinor M1 (=> Hub Dorlinor, Dorlinor M5b, Dorlinor M6, Ygriak M1)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1200,idLine=11}] Item.tag.tch set value {connexions:[{distance:138,temps:897},{line:55,distance:48,temps:306},{line:61,distance:118,temps:689},{line:11,station:1210,distance:735,temps:1744}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1200,idLine=11}] idTerminus 12

# Dorlinor M5b (=> Hub Dorlinor, Dorlinor M1, Dorlinor M6, Désert Laar M5b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1200,idLine=55}] Item.tag.tch set value {connexions:[{distance:148,temps:862},{line:11,distance:50,temps:310},{line:61,distance:118,temps:698},{line:55,station:1205,distance:878,temps:2041}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1200,idLine=55}] idTerminus 56

# Dorlinor M6 (=> Hub Dorlinor, Dorlinor M1, Dorlinor M5b, Athès M6, AlwinOnyx M6)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1200,idLine=61}] Item.tag.tch set value {connexions:[{distance:104,temps:645},{line:11,distance:58,temps:447},{line:55,distance:57,temps:443},{line:62,station:1250,distance:660,temps:1428},{line:61,station:1360,distance:1442,temps:3257}]}

# Dorlinor S1 (=> Hub Dorlinor)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1200,idSortie=1}] Item.tag.tch set value {position:{x:1571,z:-503},connexions:[{distance:20,temps:130}]}

### STATION DESERT LAAR
# Hub Désert Laar (=> Désert Laar M5b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1205}] Item.tag.tch set value {position:{x:2047,z:-897},connexions:[{line:55,distance:83,temps:727}]}

# Désert Laar M5b (=> Hub Désert Laar, Dorlinor M5b, Rives de Laar M5b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1205,idLine=55}] Item.tag.tch set value {connexions:[{distance:95,temps:662},{line:56,station:1200,distance:882,temps:2051},{line:55,station:1206,distance:731,temps:1724}]}

### STATION RIVES DE LAAR
# Hub Rives de Laar (=> Rives de Laar M5b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1206}] Item.tag.tch set value {position:{x:2687,z:-796},connexions:[{line:55,distance:72,temps:591}]}

# Rives de Laar M5b (=> Hub Rives de Laar, Désert Laar M5b, Midès M5b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1206,idLine=55}] Item.tag.tch set value {connexions:[{distance:100,temps:678},{line:56,station:1205,distance:735,temps:1734},{line:55,station:1610,distance:643,temps:1559}]}

### STATION YGRIAK
# Hub Ygriak (=> Ygriak M1/M4, Ygriak S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1210}] Item.tag.tch set value {position:{x:1092,z:-210},connexions:[{line:11,distance:117,temps:760},{line:41,distance:117,temps:760},{sortie:1,distance:14,temps:91}]}

# Ygriak M1 (=> Hub Ygriak, Ygriak M4, Dorlinor M1, Knosos M1)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1210,idLine=11}] Item.tag.tch set value {connexions:[{distance:133,temps:864},{line:41,distance:94,temps:630},{line:12,station:1200,distance:744,temps:1764},{line:11,station:1020,distance:698,temps:1495}]}

# Ygriak M4 (=> Hub Ygriak, Ygriak M1, Béhue M4, Knosos M4)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1210,idLine=41}] Item.tag.tch set value {connexions:[{distance:133,temps:864},{line:11,distance:94,temps:630},{line:41,station:1280,distance:1446,temps:3250},{line:42,station:1020,distance:687,temps:1471}]}

# Ygriak S1 (=> Hub Ygriak)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1210,idSortie=1}] Item.tag.tch set value {position:{x:1091,z:-246},connexions:[{distance:8,temps:52}]}

### STATION ZOBUN-ECLESTA
# Hub ZobunEclesta (=> ZobunEclesta M2, ZobunEclesta S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1220}] Item.tag.tch set value {position:{x:1185,z:379},connexions:[{line:21,distance:58,temps:377},{sortie:1,distance:48,temps:312}]}

# ZobunEclesta M2 (=> Hub ZobunEclesta, Kraken M2, EvenisEclesta M2)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1220,idLine=21}] Item.tag.tch set value {connexions:[{distance:58,temps:377},{line:22,station:1010,distance:636,temps:1564},{line:21,station:1230,distance:496,temps:1225}]}

# ZobunEclesta S1 (=> Hub ZobunEclesta)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1220,idSortie=1}] Item.tag.tch set value {position:{x:1215,z:383},connexions:[{distance:28,temps:182}]}

### STATION EVENIS–ECLESTA
# Hub EvenisEclesta (=> EvenisEclesta M2, EvenisEclesta S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1230}] Item.tag.tch set value {position:{x:1642,z:420},connexions:[{line:21,distance:79,temps:513},{sortie:1,distance:20,temps:130},{sortie:2,distance:106,temps:689}]}

# EvenisEclesta M2 (=> Hub EvenisEclesta, ZobunEclesta M2)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1230,idLine=21}] Item.tag.tch set value {connexions:[{distance:100,temps:650},{line:22,station:1220,distance:500,temps:1235}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1230,idLine=21}] idTerminus 21

# EvenisEclesta S1 (=> Hub EvenisEclesta, LD Début R124, LD BaseNautiqueEvenisEclesta)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1230,idSortie=1}] Item.tag.tch set value {position:{x:1623,z:407},connexions:[{distance:16,temps:104},{station:99019,distance:22,temps:143},{station:99020,distance:10,temps:65}]}
# EvenisEclesta S2 (=> Hub EvenisEclesta)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1230,idSortie=2}] Item.tag.tch set value {position:{x:1588,z:457},connexions:[{distance:97,temps:630}]}

### STATION ATHÈS
# Hub Athès (=> Athès M6)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1250}] Item.tag.tch set value {position:{x:2086,z:-268},connexions:[{line:61,distance:82,temps:615}]}

# Athès M6 (=> Hub Athès, Dorlinor M6, Midès M6)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1250,idLine=61}] Item.tag.tch set value {connexions:[{distance:96,temps:600},{line:61,station:1200,distance:659,temps:1458},{line:62,station:1610,distance:744,temps:1775}]}

### STATION LA BÉHUE (anciennement XXVILLAGEBU)
# Hub Béhue (=> Béhue M4, Béhue S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1280}] Item.tag.tch set value {position:{x:1425,z:-1307},connexions:[{line:41,distance:48,temps:337},{sortie:1,distance:5,temps:35}]}

# Béhue M4 (=> Hub Béhue, Tilia M4, Ygriak M4)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1280,idLine=41}] Item.tag.tch set value {connexions:[{distance:18,temps:122},{line:41,station:2730,distance:1403,temps:3168},{line:42,station:1210,distance:1438,temps:3181}]}

# Béhue S1 (=> Hub Béhue)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1280,idSortie=1}] Item.tag.tch set value {position:{x:1425,z:-1307},connexions:[{distance:5,temps:35}]}

### STATION LES SABLONS
# Hub Sablons (=> Sablons M3/M6, Sablons S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1300}] Item.tag.tch set value {position:{x:-395,z:-395},connexions:[{line:31,distance:50,temps:325},{line:61,distance:71,temps:482},{sortie:1,distance:50,temps:325}]}

# Sablons M3 (=> Hub Sablons, Sablons M6, Chizân M3, Verchamps M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1300,idLine=31}] Item.tag.tch set value {connexions:[{distance:50,temps:325},{line:61,distance:122,temps:780},{line:32,station:1320,distance:887,temps:2193},{line:31,station:1030,distance:716,temps:1763}]}

# Sablons M6 (=> Hub Sablons, Sablons M3, Fultèz M6, AlwinOnyx M6)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1300,idLine=61}] Item.tag.tch set value {connexions:[{distance:103,temps:606},{line:31,distance:107,temps:670},{line:61,station:1450,distance:1101,temps:2726},{line:62,station:1360,distance:660,temps:1549}]}

# Sablons S1 (=> Hub Sablons, LD SanctuaireTemps, LD Eremos)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1300,idSortie=1}] Item.tag.tch set value {position:{x:-404,z:-408},connexions:[{distance:34,temps:221},{station:99009,distance:100,temps:650},{station:99010,distance:247,temps:1605}]}

### STATION CHIZÂN
# Hub Chizân (=> Chizân M3/M5a, Chizân S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1320}] Item.tag.tch set value {position:{x:-727,z:-945},connexions:[{line:31,distance:40,temps:260},{line:51,distance:114,temps:741},{sortie:1,distance:32,temps:208}]}

# Chizân M3 (=> Hub Chizân, Chizân M5a, Malarân M3, Sablons M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1320,idLine=31}] Item.tag.tch set value {connexions:[{distance:58,temps:377},{line:51,distance:116,temps:754},{line:32,station:1330,distance:963,temps:2108},{line:31,station:1300,distance:885,temps:2188}]}

# Chizân M5a (=> Hub Chizân, Chizân M3, Glandy M5a, Fultèz M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1320,idLine=51}] Item.tag.tch set value {connexions:[{distance:74,temps:481},{line:31,distance:132,temps:858},{line:52,station:1390,distance:498,temps:1066},{line:51,station:1450,distance:1295,temps:3120}]}

# Chizan S1 (=> Hub Chizân, LD Chizan)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1320,idSortie=1}] Item.tag.tch set value {position:{x:-708,z:-929},connexions:[{distance:26,temps:169},{station:99008,distance:31,temps:201}]}

### STATION MALARÂN (anciennement XXMETRO3XA)
# Hub Malarân (=> Malarân M3, Malarân S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1330}] Item.tag.tch set value {position:{x:-1231,z:-1499},connexions:[{line:31,distance:1,temps:1},{sortie:1,distance:1,temps:1}]}

# Malarân M3 (=> Hub Malarân, MontagneDuDestin M3, Chizân M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1330,idLine=31}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:32,station:3315,distance:795,temps:1863},{line:31,station:1320,distance:965,temps:2134}]}

# Malarân S1 (=> Hub Malarân)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1330,idSortie=1}] Item.tag.tch set value {position:{x:-1231,z:-1499},connexions:[{distance:1,temps:1}]}

### STATION DESERT ONYX
# Hub Onyx (=> Onyx M2, Onyx S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1350}] Item.tag.tch set value {position:{x:505,z:-842},connexions:[{line:21,distance:37,temps:240},{sortie:1,distance:56,temps:364},{sortie:2,distance:157,temps:1020}]}

# Onyx M2 (=> Hub Onyx, Sablons–Brimiel M2, AlwinOnyx M2)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1350,idLine=21}] Item.tag.tch set value {connexions:[{distance:81,temps:526},{line:22,station:1370,distance:-1,temps:-1},{line:21,station:1360,distance:714,temps:1737}]}

# Onyx S1 (=> Hub Onyx, LD CitadelleGzor)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1350,idSortie=1}] Item.tag.tch set value {position:{x:545,z:-871},connexions:[{distance:97,temps:630},{station:99013,distance:49,temps:318}]}
# Onyx S2 (=> Hub Onyx, LD CrypteGzor)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1350,idSortie=2}] Item.tag.tch set value {position:{x:447,z:-748},connexions:[{distance:145,temps:942},{station:99014,distance:117,temps:760}]}

### STATION ALWIN-ONYX
# Hub AlwinOnyx (=> AlwinOnyx M2/M6, AlwinOnyx S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1360}] Item.tag.tch set value {position:{x:248,z:-356},connexions:[{line:21,distance:84,temps:546},{line:61,distance:159,temps:967},{sortie:1,distance:56,temps:364},{sortie:2,distance:19,temps:123}]}

# AlwinOnyx M2 (=> Hub AlwinOnyx, AlwinOnyx M6, Onyx M2, Hameau Gbrn M2)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1360,idLine=21}] Item.tag.tch set value {connexions:[{distance:70,temps:455},{line:61,distance:166,temps:973},{line:22,station:1350,distance:710,temps:1727},{line:21,station:1001,distance:510,temps:1260}]}

# AlwinOnyx M6 (=> Hub AlwinOnyx, AlwinOnyx M2, Sablons M6, Dorlinor M6)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1360,idLine=61}] Item.tag.tch set value {connexions:[{distance:123,temps:913},{line:21,distance:80,temps:689},{line:61,station:1300,distance:669,temps:1570},{line:62,station:1200,distance:1447,temps:3260}]}

# AlwinOnyx S1 (=> Hub AlwinOnyx, LD ChateauOnyx)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1360,idSortie=1}] Item.tag.tch set value {position:{x:327,z:-352},connexions:[{distance:47,temps:305},{station:99012,distance:32,temps:208}]}
# AlwinOnyx S2 (=> Hub AlwinOnyx)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1360,idSortie=2}] Item.tag.tch set value {position:{x:275,z:-346},connexions:[{distance:12,temps:78}]}

### STATION SABLONS–BRIMIEL (anciennement Sablons–Brimiel)
# Hub Sablons–Brimiel (=> Sablons–Brimiel RER A, Sablons–Brimiel M2, Sablons–Brimiel S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1370}] Item.tag.tch set value {position:{x:226,z:-1142},connexions:[{line:2100,distance:1,temps:1},{line:21,distance:1,temps:1},{sortie:1,distance:1,temps:1}]}

# Sablons–Brimiel RER A (=> Hub Sablons–Brimiel, Sablons–Brimiel M2, Touissy RER A, Fouizy RER A, HameauRD RER A)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1370,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:21,distance:1,temps:1},{line:2153,station:3350,distance:1299,temps:1999},{line:2154,station:3230,distance:822,temps:1999},{line:2101,station:1002,distance:1291,temps:1999},{line:2102,station:1002,distance:1291,temps:1999}]}

# Sablons–Brimiel M2 (=> Hub Sablons–Brimiel, Sablons–Brimiel RER A, Onyx M2)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1370,idLine=21}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:21,station:1350,distance:-1,temps:-1}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1370,idLine=21}] idTerminus 22

# Sablons–Brimiel S1 (=> Hub Sablons–Brimiel)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1370,idSortie=1}] Item.tag.tch set value {position:{x:226,z:-1142},connexions:[{distance:1,temps:1}]}

### STATION GLANDY (anciennement XXVILLAGEBE)
# Hub Glandy (=> Glandy M5a, Glandy S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1390}] Item.tag.tch set value {position:{x:-463,z:-1296},connexions:[{line:51,distance:1,temps:1},{sortie:1,distance:1,temps:1}]}

# Glandy M5a (=> Hub Glandy, Touissy M5a, Chizân M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1390,idLine=51}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:52,station:3350,distance:706,temps:1707},{line:51,station:1320,distance:493,temps:1052}]}

# Glandy S1 (=> Hub Glandy)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1390,idSortie=1}] Item.tag.tch set value {position:{x:-463,z:-1296},connexions:[{distance:1,temps:1}]}

### STATION FORT DU VAL
# Hub FortDuVal (=> FortDuVal M5a)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1400}] Item.tag.tch set value {position:{x:-1536,z:604},connexions:[{line:51,distance:152,temps:1144}]}

# FortDuVal M5a (=> Hub FortDuVal, Arithmatie M5a, Pieuze M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1400,idLine=51}] Item.tag.tch set value {connexions:[{distance:262,temps:1475},{line:51,station:1110,distance:472,temps:1128},{line:52,station:1460,distance:608,temps:1469}]}

### STATION ILLYSIA
# Hub Illysia (=> Illysia M4, Illysia S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1420}] Item.tag.tch set value {position:{x:-720,z:221},connexions:[{line:41,distance:56,temps:373},{sortie:1,distance:60,temps:390},{sortie:2,distance:174,temps:1131}]}

# Illysia M4 (=> Hub Illysia, Pieuze M4, Verchamps M4)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1420,idLine=41}] Item.tag.tch set value {connexions:[{distance:46,temps:278},{line:42,station:1460,distance:783,temps:2124},{line:41,station:1030,distance:615,temps:1728}]}

# Illysia S2 (=> Hub Illysia, LD Entrée d'Illysia)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1420,idSortie=2}] Item.tag.tch set value {position:{x:-741,z:205},connexions:[{distance:66,temps:429},{station:99007,distance:49,temps:318}]}
# Illysia S1 (=> Hub Illysia, LD Entrée d'Illysia, LD Sortie d'Illysia)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1420,idSortie=1}] Item.tag.tch set value {position:{x:-680,z:218},connexions:[{distance:154,temps:1001},{station:99007,distance:99,temps:643},{station:99022,distance:107,temps:695}]}

### STATION FULTEZ-EN-SULUM
# Hub Fultèz (=> Fultèz M5a/M6)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1450}] Item.tag.tch set value {position:{x:-1421,z:48},connexions:[{line:51,distance:68,temps:542},{line:61,distance:57,temps:413}]}

# Fultèz M6 (=> Hub Fultèz, Fultèz M5a, Sablons M6)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1450,idLine=61}] Item.tag.tch set value {connexions:[{distance:56,temps:336},{line:51,distance:138,temps:891},{line:62,station:1300,distance:1106,temps:2730}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1450,idLine=61}] idTerminus 61

# Fultèz M5a (=> Hub Fultèz, Fultèz M6, Chizân M5a, Pieuze M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1450,idLine=51}] Item.tag.tch set value {connexions:[{distance:114,temps:675},{line:61,distance:90,temps:543},{line:52,station:1320,distance:1291,temps:3105},{line:51,station:1460,distance:379,temps:947}]}

### STATION PIEUZE-EN-SULUM
# Hub Pieuze (=> Pieuze M4/M5a, Pieuze S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1460}] Item.tag.tch set value {position:{x:-1421,z:-264},connexions:[{line:41,distance:74,temps:481},{line:51,distance:72,temps:468},{sortie:1,distance:27,temps:175}]}

# Pieuze M4 (=> Hub Pieuze, Pieuze M5a, Illysia M4)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1460,idLine=41}] Item.tag.tch set value {connexions:[{distance:85,temps:552},{line:51,distance:71,temps:417},{line:41,station:1420,distance:783,temps:2160}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1460,idLine=41}] idTerminus 42

# Pieuze M5a (=> Hub Pieuze, Pieuze M4, Fultèz M5a, FortDuVal M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1460,idLine=51}] Item.tag.tch set value {connexions:[{distance:78,temps:507},{line:41,distance:67,temps:473},{line:52,station:1450,distance:379,temps:947},{line:51,station:1400,distance:609,temps:1470}]}

# Pieuze S1 (=> Hub Pieuze)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1460,idSortie=1}] Item.tag.tch set value {position:{x:-1419,z:-312},connexions:[{distance:24,temps:156}]}

### STATION MONTAGNES DE KRYPT
# Hub Krypt (=> Krypt M1, Krypt S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1490}] Item.tag.tch set value {position:{x:-774,z:606},connexions:[{line:11,distance:104,temps:676},{sortie:1,distance:47,temps:305},{sortie:2,distance:82,temps:533},{sortie:3,distance:70,temps:455}]}

# Krypt M1 (=> Hub Krypt, GrenatPort M1, HameauPort M1)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1490,idLine=11}] Item.tag.tch set value {connexions:[{distance:87,temps:565},{line:11,station:1102,distance:1291,temps:2758},{line:12,station:1004,distance:898,temps:2007}]}

# Krypt S1 (=> Hub Krypt)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1490,idSortie=1}] Item.tag.tch set value {position:{x:-816,z:594},connexions:[{distance:38,temps:247}]}
# Krypt S2 (=> Hub Krypt)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1490,idSortie=2}] Item.tag.tch set value {position:{x:-811,z:634},connexions:[{distance:61,temps:396}]}
# Krypt S3 (=> Hub Krypt)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1490,idSortie=3}] Item.tag.tch set value {position:{x:-782,z:567},connexions:[{distance:79,temps:513}]}

### STATION NERONIL
# Hub Néronil (=> Néronil M7, Néronil S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1500}] Item.tag.tch set value {position:{x:2265,z:1798},connexions:[{line:71,distance:41,temps:266},{sortie:1,distance:36,temps:234}]}

# Néronil M7 (=> Hub Néronil, Tolbrok M7, Litoréa M7)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1500,idLine=71}] Item.tag.tch set value {connexions:[{distance:51,temps:331},{line:72,station:1170,distance:598,temps:1321},{line:71,station:1550,distance:812,temps:1921}]}

# Néronil S1 (=> Hub Néronil)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1500,idSortie=1}] Item.tag.tch set value {position:{x:2285,z:1781},connexions:[{distance:30,temps:195}]}

### STATION ÉVENIS-TAMLYN
# Hub EvenisTamlyn (=> EvenisTamlyn M5b/M7, EvenisTamlyn S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1520}] Item.tag.tch set value {position:{x:1593,z:1081},connexions:[{line:55,distance:71,temps:461},{line:71,distance:44,temps:286},{sortie:1,distance:48,temps:312}]}

# EvenisTamlyn M5b (=> Hub EvenisTamlyn, EvenisTamlyn M7, Villonne M5b, 4chemins M5b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1520,idLine=55}] Item.tag.tch set value {connexions:[{distance:98,temps:637},{line:71,distance:51,temps:331},{line:55,station:1150,distance:976,temps:2436},{line:56,station:1650,distance:1102,temps:2656}]}

# EvenisTamlyn M7 (=> Hub EvenisTamlyn, EvenisTamlyn M5b, Tolbrok M7)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1520,idLine=71}] Item.tag.tch set value {connexions:[{distance:55,temps:357},{line:55,distance:120,temps:780},{line:71,station:1170,distance:719,temps:1733}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1520,idLine=71}] idTerminus 72

# EvenisTamlyn S1 (=> Hub EvenisTamlyn)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1520,idSortie=1}] Item.tag.tch set value {position:{x:1594,z:1106},connexions:[{distance:22,temps:143}]}

### STATION EIDIN-LA-VALLÉE
# Hub Eidin (=> Eidin M7b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1530}] Item.tag.tch set value {position:{x:2768,z:1369},connexions:[{line:73,distance:94,temps:675}]}

# Eidin M7b (=> Hub Eidin, 4chemins M7b, Moronia M7b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1530,idLine=73}] Item.tag.tch set value {connexions:[{distance:113,temps:640},{line:74,station:1650,distance:601,temps:1331},{line:73,station:1590,distance:620,temps:1319}]}

### STATION LITORÉA
# Hub Litoréa (=> Litoréa M7/M7b, Litoréa S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1550}] Item.tag.tch set value {position:{x:2682,z:2338},connexions:[{line:71,distance:79,temps:513},{line:73,distance:59,temps:383},{sortie:1,distance:25,temps:162},{sortie:2,distance:41,temps:266},{sortie:3,distance:54,temps:351}]}

# Litoréa M7 (=> Hub Litoréa, Litoréa M7b, Néronil M7, ThoroirValdee M7)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1550,idLine=71}] Item.tag.tch set value {connexions:[{distance:109,temps:708},{line:73,distance:168,temps:1092},{line:72,station:1500,distance:816,temps:1931},{line:71,station:1820,distance:934,temps:2142}]}

# Litoréa M7b (=> Hub Litoréa, Litoréa M7, Moronia M7b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1550,idLine=73}] Item.tag.tch set value {connexions:[{distance:74,temps:481},{line:71,distance:53,temps:344},{line:74,station:1590,distance:646,temps:1490}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1550,idLine=73}] idTerminus 73

# Litoréa S1 (=> Hub Litoréa)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1550,idSortie=1}] Item.tag.tch set value {position:{x:2667,z:2327},connexions:[{distance:15,temps:97}]}
# Litoréa S2 (=> Hub Litoréa)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1550,idSortie=2}] Item.tag.tch set value {position:{x:2646,z:2324},connexions:[{distance:33,temps:214}]}
# Litoréa S3 (=> Hub Litoréa)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1550,idSortie=3}] Item.tag.tch set value {position:{x:2701,z:2366},connexions:[{distance:44,temps:286}]}

### STATION MORONIA
# Hub Moronia (=> Moronia M7b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1590}] Item.tag.tch set value {position:{x:2932,z:1904},connexions:[{line:73,distance:47,temps:356}]}

# Moronia M7b (=> Hub Moronia, Eidin M7b, Litoréa M7b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1590,idLine=73}] Item.tag.tch set value {connexions:[{distance:42,temps:281},{line:74,station:1530,distance:621,temps:1320},{line:73,station:1550,distance:642,temps:1479}]}

### STATION HADÈS
# Hub Hadès (=> Hadès M5b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1600}] Item.tag.tch set value {position:{x:2753,z:215},connexions:[{line:55,distance:63,temps:489}]}

# Hadès M5b (=> Hub Hadès, Midès M5b, 4chemins M5b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1600,idLine=55}] Item.tag.tch set value {connexions:[{distance:117,temps:671},{line:56,station:1610,distance:388,temps:969},{line:55,station:1650,distance:712,temps:1561}]}

### STATION MIDÈS
# Hub Midès (=> Midès M5b/M6)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1610}] Item.tag.tch set value {position:{x:2744,z:-150},connexions:[{line:55,distance:102,temps:728},{line:61,distance:41,temps:312}]}

# Midès M5b (=> Hub Midès, Midès M6, Hadès M5b, Rives de Laar M5b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1610,idLine=55}] Item.tag.tch set value {connexions:[{distance:79,temps:465},{line:61,distance:71,temps:430},{line:55,station:1600,distance:388,temps:969},{line:56,station:1206,distance:643,temps:1560}]}

# Midès M6 (=> Hub Midès, Midès M5b, Athès M6)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1610,idLine=61}] Item.tag.tch set value {connexions:[{distance:64,temps:378},{line:55,distance:77,temps:529},{line:61,station:1250,distance:748,temps:1785}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1610,idLine=61}] idTerminus 62

### STATION QUATRE CHEMINS
# Hub 4chemins (=> 4chemins M5b/M7b, 4chemins S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1650}] Item.tag.tch set value {position:{x:2506,z:857},connexions:[{line:55,distance:169,temps:1098},{line:73,distance:155,temps:1007},{sortie:1,distance:16,temps:104}]}

# 4chemins M5b (=> Hub 4chemins, 4chemins M7b, Hadès M5b, EvenisTamlyn M5b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1650,idLine=55}] Item.tag.tch set value {connexions:[{distance:139,temps:903},{line:73,distance:89,temps:598},{line:56,station:1600,distance:712,temps:1560},{line:55,station:1520,distance:1098,temps:2624}]}

# 4chemins M7b (=> Hub 4chemins, 4chemins M5b, Eidin M7b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1650,idLine=73}] Item.tag.tch set value {connexions:[{distance:177,temps:1150},{line:55,distance:89,temps:598},{line:73,station:1530,distance:601,temps:1331}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1650,idLine=73}] idTerminus 74

# 4chemins S1 (=> Hub 4chemins)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1650,idSortie=1}] Item.tag.tch set value {position:{x:2515,z:882},connexions:[{distance:10,temps:65}]}

### STATION LE ROUSTIFLET
# Hub Roustiflet (=> Roustiflet RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1800}] Item.tag.tch set value {position:{x:1641,z:3227},connexions:[{line:2100,distance:24,temps:176}]}

# Roustiflet RERA (=> Hub Roustiflet, Beothas RERA, Chassy RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1800,idLine=2100}] Item.tag.tch set value {connexions:[{distance:22,temps:195},{line:2153,station:1160,distance:2198,temps:2455},{line:2154,station:1160,distance:2198,temps:2455},{line:2102,station:1860,distance:498,temps:735}]}

### STATION THOROÏR–VALDÉE
# Hub ThoroirValdee (=> ThoroirValdee M7, ThoroirValdee S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1820}] Item.tag.tch set value {position:{x:2308,z:3017},connexions:[{line:71,distance:66,temps:429},{sortie:1,distance:59,temps:383},{sortie:2,distance:25,temps:162}]}

# ThoroirValdee M7 (=> Hub ThoroirValdee, Litoréa M7, Thalrion M7)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1820,idLine=71}] Item.tag.tch set value {connexions:[{distance:56,temps:364},{line:72,station:1550,distance:934,temps:2140},{line:71,station:1850,distance:577,temps:1340}]}

# ThoroirValdee S1 (=> Hub ThoroirValdee)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1820,idSortie=1}] Item.tag.tch set value {position:{x:2327,z:3030},connexions:[{distance:32,temps:208}]}
# ThoroirValdee S2 (=> Hub ThoroirValdee)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1820,idSortie=2}] Item.tag.tch set value {position:{x:2270,z:3001},connexions:[{distance:20,temps:130}]}

### STATION THALRION
# Hub Thalrion (=> Thalrion M7, Thalrion S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1850}] Item.tag.tch set value {position:{x:2440,z:3572},connexions:[{line:71,distance:78,temps:507},{sortie:1,distance:32,temps:208}]}

# Thalrion M7 (=> Hub Thalrion, ThoroirValdee M7, Chassy M7)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1850,idLine=71}] Item.tag.tch set value {connexions:[{distance:58,temps:377},{line:72,station:1820,distance:577,temps:1317},{line:71,station:1860,distance:676,temps:1556}]}

# Thalrion S1 (=> Hub Thalrion)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1850,idSortie=1}] Item.tag.tch set value {position:{x:2422,z:3552},connexions:[{distance:30,temps:195}]}

### STATION CHASSY SUR FLUMINE
# Hub Chassy (=> Chassy M7/RERA, Chassy S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1860}] Item.tag.tch set value {position:{x:1877,z:3665},connexions:[{line:71,distance:50,temps:348},{line:2100,distance:144,temps:909},{sortie:1,distance:72,temps:468},{sortie:2,distance:83,temps:539}]}

# Chassy M7 (=> Hub Chassy, Chassy RERA, Thalrion M7)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1860,idLine=71}] Item.tag.tch set value {connexions:[{distance:10,temps:65},{line:2100,distance:94,temps:555},{line:72,station:1850,distance:680,temps:1566}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1860,idLine=71}] idTerminus 71

# Chassy RERA (=> Hub Chassy, Chassy M7, Roustiflet RERA, Evrocq RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1860,idLine=2100}] Item.tag.tch set value {connexions:[{distance:50,temps:325},{line:71,distance:90,temps:779},{line:2153,station:1800,distance:498,temps:729},{line:2154,station:1800,distance:498,temps:729},{line:2102,station:1900,distance:791,temps:970}]}

# Chassy S1 (=> Hub Chassy)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1860,idSortie=1}] Item.tag.tch set value {position:{x:1908,z:3671},connexions:[{distance:73,temps:474}]}
# Chassy S2 (=> Hub Chassy)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1860,idSortie=2}] Item.tag.tch set value {position:{x:1822,z:3648},connexions:[{distance:89,temps:578}]}

### STATION EVROCQ SAUVEBONNE
# Hub Evrocq (=> Evrocq RERA, Evrocq CA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1900}] Item.tag.tch set value {position:{x:1669,z:4410},connexions:[{line:2100,distance:44,temps:322},{line:4011,distance:16,temps:114}]}

# Evrocq RERA (=> Hub Evrocq, Chassy RERA, Procyon RERA)
# Pas de connexion directe RERA <> CA ; il faut passer via le hub, comme IG (sortir et repasser aux valideurs)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1900,idLine=2100}] Item.tag.tch set value {connexions:[{distance:51,temps:360},{line:2153,station:1860,distance:791,temps:1108},{line:2154,station:1860,distance:791,temps:1108},{line:2102,station:2000,distance:2644,temps:2365}]}

# Evrocq CA (=> Hub Evrocq, EvrocqLeBas CA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1900,idLine=4011}] Item.tag.tch set value {connexions:[{distance:16,temps:123},{line:4011,station:1904,distance:92,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1900,idLine=4011}] idTerminus 4012

### STATION EVROCQ-LE-BAS
# Hub EvrocqLeBas (=> EvrocqLeBas CA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1904}] Item.tag.tch set value {position:{x:1669,z:4410},connexions:[{line:4011,distance:28,temps:188}]}

# EvrocqLeBas CA (=> Hub EvrocqLeBas, Evrocq CA, EvrocqLeHaut CA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1904,idLine=4011}] Item.tag.tch set value {connexions:[{distance:15,temps:171},{line:4012,station:1900,distance:92,temps:1999},{line:4011,station:1905,distance:190,temps:1999}]}

### STATION EVROCQ-LE-HAUT
# Hub EvrocqLeHaut (=> EvrocqLeHaut CA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1905}] Item.tag.tch set value {position:{x:1669,z:4410},connexions:[{line:4011,distance:8,temps:64}]}

# EvrocqLeHaut CA (=> Hub EvrocqLeHaut, EvrocqLeBas CA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1905,idLine=4011}] Item.tag.tch set value {connexions:[{distance:8,temps:64},{line:4012,station:1904,distance:190,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1905,idLine=4011}] idTerminus 4011

### STATION ILE DU SINGE
# Hub IleDuSinge (=> IleDuSinge S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1910}] Item.tag.tch set value {position:{x:1096,z:3823},connexions:[{sortie:1,distance:23,temps:149},{sortie:2,distance:23,temps:149}]}

# IleDuSinge S1 (=> Hub IleDuSinge)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1910,idSortie=1}] Item.tag.tch set value {position:{x:1097,z:3825},connexions:[{distance:23,temps:149}]}
# IleDuSinge S2 (=> Hub IleDuSinge)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1910,idSortie=2}] Item.tag.tch set value {position:{x:1098,z:3801},connexions:[{distance:23,temps:149}]}

### STATION LE RELAIS
# Hub LeRelais (=> LeRelais M8b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1950}] Item.tag.tch set value {position:{x:1578,z:5924},connexions:[{line:83,distance:42,temps:303}]}

# LeRelais M8b (=> Hub LeRelais, Moldor–Balchaïs M8b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1950,idLine=83}] Item.tag.tch set value {connexions:[{distance:48,temps:286},{line:83,station:1960,distance:675,temps:1332}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1950,idLine=83}] idTerminus 84

### STATION MOLDOR BALCHAIS
# Hub Moldor–Balchaïs (=> Moldor–Balchaïs M8b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1960}] Item.tag.tch set value {position:{x:2000,z:5527},connexions:[{line:83,distance:42,temps:300}]}

# Moldor–Balchaïs M8b (=> Hub Moldor–Balchaïs, LeRelais M8b, Quécol M8b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1960,idLine=83}] Item.tag.tch set value {connexions:[{distance:12,temps:108},{line:84,station:1950,distance:674,temps:1309},{line:83,station:1980,distance:587,temps:1328}]}

### STATION QUECOL EN DRIE
# Hub Quecol (=> Quecol M8b, Quecol S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1980}] Item.tag.tch set value {position:{x:2543,z:5536},connexions:[{line:83,distance:47,temps:291},{sortie:1,distance:12,temps:86}]}

# Quécol M8b (=> Hub Quécol, Moldor–Balchaïs M8b, FrambourgTernelieu M8b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=1980,idLine=83}] Item.tag.tch set value {connexions:[{distance:46,temps:325},{line:84,station:1960,distance:587,temps:1321},{line:83,station:2100,distance:765,temps:1620}]}

# Quécol S1 (=> Hub Quécol)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1980,idSortie=1}] Item.tag.tch set value {position:{x:2544,z:5523},connexions:[{distance:12,temps:86}]}

### STATION PROCYON
# Hub Procyon (=> Procyon M8/RERA, Procyon S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2000}] Item.tag.tch set value {position:{x:-81,z:5876},connexions:[{line:81,distance:38,temps:283},{line:2100,distance:57,temps:381},{sortie:1,distance:20,temps:130},{sortie:2,distance:20,temps:130}]}

# Procyon RERA (=> Hub Procyon, Evrocq RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2000,idLine=2100}] Item.tag.tch set value {connexions:[{distance:59,temps:461},{line:2153,station:1900,distance:666,temps:2657},{line:2154,station:1900,distance:666,temps:2657}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2000,idLine=2100}] idTerminus 2102

# Procyon M8 (=> Hub Procyon, ProcyonUniversité M8, Ténèbres M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2000,idLine=81}] Item.tag.tch set value {connexions:[{distance:36,temps:256},{line:81,station:2005,distance:666,temps:1999},{line:82,station:2001,distance:238,temps:519}]}

# Procyon S1 (=> Hub Procyon)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2000,idSortie=1}] Item.tag.tch set value {position:{x:-60,z:5876},connexions:[{distance:20,temps:130}]}
# Procyon S2 (=> Hub Procyon)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2000,idSortie=2}] Item.tag.tch set value {position:{x:-102,z:5876},connexions:[{distance:20,temps:130}]}

### STATION PROCYON TENEBRES
# Hub Ténèbres (=> Ténèbres M8, Ténèbres S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2001}] Item.tag.tch set value {position:{x:-137,z:6130},connexions:[{line:81,distance:65,temps:422},{sortie:1,distance:21,temps:136}]}

# Ténèbres M8 (=> Hub Ténèbres, Procyon M8, Aulnoy M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2001,idLine=81}] Item.tag.tch set value {connexions:[{distance:80,temps:520},{line:81,station:2000,distance:239,temps:520},{line:82,station:2010,distance:440,temps:1043}]}

# Ténèbres S1 (=> Hub Ténèbres, LD Donjon des Ténèbres)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2001,idSortie=1}] Item.tag.tch set value {position:{x:-166,z:6127},connexions:[{distance:17,temps:110},{station:99001,distance:112,temps:728}]}

### STATION PROCYON UNIVERSITÉ
# Hub ProcyonUniversité (=> ProcyonUniversité M8, ProcyonUniversité S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2005}] Item.tag.tch set value {position:{x:-82,z:5295},connexions:[{line:81,distance:1,temps:1},{sortie:1,distance:1,temps:1}]}

# ProcyonUniversité M8 (=> Hub ProcyonUniversité, Procyon M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2005,idLine=81}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:82,station:2000,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2005,idLine=81}] idTerminus 81

# ProcyonUniversité S1 (=> Hub ProcyonUniversité)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2005,idSortie=1}] Item.tag.tch set value {position:{x:-82,z:5295},connexions:[{distance:1,temps:1}]}

### STATION AULNOY
# Hub Aulnoy (=> Aulnoy M8, Aulnoy S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2010}] Item.tag.tch set value {position:{x:244,z:6184},connexions:[{line:81,distance:49,temps:318},{sortie:1,distance:17,temps:110}]}

# Aulnoy M8 (=> Hub Aulnoy, Ténèbres M8, Argençon M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2010,idLine=81}] Item.tag.tch set value {connexions:[{distance:61,temps:396},{line:81,station:2001,distance:436,temps:1033},{line:82,station:2050,distance:629,temps:1466}]}

# Aulnoy S1 (=> Hub Aulnoy)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2010,idSortie=1}] Item.tag.tch set value {position:{x:228,z:6203},connexions:[{distance:13,temps:84}]}

### STATION ARGENCON
# Hub Argençon (=> Argençon M8, Argençon S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2050}] Item.tag.tch set value {position:{x:767,z:6360},connexions:[{line:81,distance:106,temps:804},{sortie:1,distance:17,temps:110},{sortie:2,distance:43,temps:258}]}

# Argençon M8 (=> Hub Argençon, Aulnoy M8, Crestali M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2050,idLine=81}] Item.tag.tch set value {connexions:[{distance:103,temps:585},{line:81,station:2010,distance:633,temps:1477},{line:82,station:2190,distance:625,temps:1457}]}

# Argençon S1 (=> Hub Argençon)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2050,idSortie=1}] Item.tag.tch set value {position:{x:762,z:6337},connexions:[{distance:17,temps:110}]}
# Argençon S2 (=> Hub Argençon)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2050,idSortie=2}] Item.tag.tch set value {position:{x:778,z:6351},connexions:[{distance:43,temps:258}]}

### STATION FRAMBOURG TERNELIEU
# Hub FrambourgTernelieu (=> FrambourgTernelieu M8/M8b, FrambourgTernelieu S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2100}] Item.tag.tch set value {position:{x:2994,z:5942},connexions:[{line:81,distance:53,temps:344},{line:83,distance:56,temps:364},{sortie:1,distance:28,temps:182},{sortie:2,distance:28,temps:182},{sortie:3,distance:12,temps:78}]}

# FrambourgTernelieu M8 (=> Hub FrambourgTernelieu, FrambourgTernelieu M8b, Géorlie M8, Milloreau M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2100,idLine=81}] Item.tag.tch set value {connexions:[{distance:40,temps:260},{line:83,distance:91,temps:591},{line:82,station:2120,distance:660,temps:1538},{line:81,station:2160,distance:928,temps:2196}]}

# FrambourgTernelieu M8b (=> Hub FrambourgTernelieu, FrambourgTernelieu M8, Quécol M8b)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2100,idLine=83}] Item.tag.tch set value {connexions:[{distance:47,temps:305},{line:81,distance:86,temps:559},{line:84,station:1980,distance:758,temps:2070}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2100,idLine=83}] idTerminus 83

# FrambourgTernelieu S1 (=> Hub FrambourgTernelieu, LD Ternelieu)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2100,idSortie=1}] Item.tag.tch set value {position:{x:2975,z:5945},connexions:[{distance:28,temps:182},{station:99003,distance:91,temps:16}]}
# FrambourgTernelieu S2 (=> Hub FrambourgTernelieu, LD Frambourg)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2100,idSortie=2}] Item.tag.tch set value {position:{x:3012,z:5945},connexions:[{distance:28,temps:182},{station:99002,distance:114,temps:17}]}
# FrambourgTernelieu S3 (=> RIEN (sortie uniquement) + LD Frambourg, LD Ternelieu)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2100,idSortie=3}] Item.tag.tch set value {position:{x:2994,z:5966},connexions:[{station:99002,distance:113,temps:734},{station:99003,distance:106,temps:689}]}

### STATION GEORLIE
# Hub Géorlie (=> Géorlie M8)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2120}] Item.tag.tch set value {position:{x:3523,z:5814},connexions:[{line:81,distance:69,temps:405}]}

# Géorlie M8 (=> Hub Géorlie, FrambourgTernelieu M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2120,idLine=81}] Item.tag.tch set value {connexions:[{distance:52,temps:377},{line:81,station:2100,distance:664,temps:1548}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2120,idLine=81}] idTerminus 82

### STATION CALMEFLOT LES DEUX BERGES
# Hub Calmeflot (=> Calmeflot M8, Calmeflot CB)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2150}] Item.tag.tch set value {position:{x:2384,z:6678},connexions:[{line:81,distance:73,temps:478},{line:4021,distance:129,temps:738}]}

# Calmeflot M8 (=> Hub Calmeflot, Calmeflot CB, BussyNigel M8, Milloreau M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2150,idLine=81}] Item.tag.tch set value {connexions:[{distance:105,temps:608},{line:4021,distance:157,temps:890},{line:81,station:2180,distance:1255,temps:2915},{line:82,station:2160,distance:433,temps:1057}]}

# Calmeflot CB (=> Hub Calmeflot, Calmeflot M8, LesPics CB)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2150,idLine=4021}] Item.tag.tch set value {connexions:[{distance:91,temps:645},{line:81,distance:84,temps:643},{line:4022,station:2161,distance:245,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2150,idLine=4021}] idTerminus 4021

### STATION MILLOREAU LES PICS
# Hub Milloreau (=> Milloreau M8, Milloreau CB)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2160}] Item.tag.tch set value {position:{x:2806,z:6728},connexions:[{line:81,distance:110,temps:749},{line:4021,distance:24,temps:154}]}

# Milloreau M8 (=> Hub Milloreau, Milloreau CB, FrambourgTernelieu M8, Calmeflot M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2160,idLine=81}] Item.tag.tch set value {connexions:[{distance:165,temps:941},{line:4021,distance:150,temps:857},{line:82,station:2100,distance:932,temps:2206},{line:81,station:2150,distance:433,temps:1057}]}

# Milloreau CB (=> Hub Milloreau, Milloreau M8, LesPics CB, LaDodene CB)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2160,idLine=4021}] Item.tag.tch set value {connexions:[{distance:34,temps:271},{line:81,distance:114,temps:816},{line:4021,station:2161,distance:194,temps:1999},{line:4022,station:2170,distance:676,temps:1999}]}

### STATION LES PICS
# Hub LesPics (=> LesPics CB)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2161}] Item.tag.tch set value {position:{x:2656,z:6757},connexions:[{line:4021,distance:27,temps:170}]}

# LesPics CB (=> Hub LesPics, Calmeflot CB, Milloreau CB)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2161,idLine=4021}] Item.tag.tch set value {connexions:[{distance:18,temps:172},{line:4021,station:2150,distance:245,temps:1999},{line:4022,station:2160,distance:194,temps:1999}]}

### STATION LA DODENE
# Hub LaDodene (=> LaDodene CB)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2170}] Item.tag.tch set value {position:{x:2656,z:6757},connexions:[{line:4021,distance:26,temps:171}]}

# LaDodene CB (=> Hub LaDodene, Milloreau CB)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2170,idLine=4021}] Item.tag.tch set value {connexions:[{distance:15,temps:163},{line:4021,station:2160,distance:676,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2170,idLine=4021}] idTerminus 4022

### STATION BUSSY NIGEL
# Hub BussyNigel (=> BussyNigel M8)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2180}] Item.tag.tch set value {position:{x:1227,z:6918},connexions:[{line:81,distance:72,temps:518}]}

# BussyNigel M8 (=> Hub BussyNigel, Calmeflot M8, Crestali M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2180,idLine=81}] Item.tag.tch set value {connexions:[{distance:63,temps:397},{line:82,station:2150,distance:1255,temps:2915},{line:81,station:2190,distance:590,temps:1376}]}

### STATION CRESTALI
# Hub Crestali (=> Crestali M8)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2190}] Item.tag.tch set value {position:{x:685,z:6957},connexions:[{line:81,distance:58,temps:433}]}

# Crestali M8 (=> Hub Crestali, BussyNigel M8, Argençon M8)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2190,idLine=81}] Item.tag.tch set value {connexions:[{distance:69,temps:423},{line:82,station:2180,distance:595,temps:1387},{line:81,station:2050,distance:626,temps:1458}]}

### STATION CYSEAL
# Hub Cyseal (=> Cyseal RERA/M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2200}] Item.tag.tch set value {position:{x:-1636,z:6923},connexions:[{line:2100,distance:1,temps:1},{line:121,distance:1,temps:1}]}

# Cyseal RERA (=> Hub Cyseal, Vernoglie RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2200,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:121,distance:1,temps:1},{line:2153,station:2240,distance:1282,temps:1999},{line:2154,station:2240,distance:1282,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2200,idLine=2100}] idTerminus 2101

# Cyseal M12 (=> Hub Cyseal, Gallicy M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2200,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:122,station:2210,distance:666,temps:1999},{line:123,station:2210,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2200,idLine=121}] idTerminus 121

### STATION GALLICY (anciennement XXVILLAGEAY)
# Hub Gallicy (=> Gallicy M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2210}] Item.tag.tch set value {position:{x:-2465,z:6810},connexions:[{line:121,distance:1,temps:1}]}

# Gallicy M12 (=> Hub Gallicy, PontAuxPorcelets M12, Cyseal M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2210,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:122,station:2220,distance:666,temps:1999},{line:123,station:2220,distance:666,temps:1999},{line:121,station:2200,distance:666,temps:1999}]}

### STATION LE PONT-AUX-PORCELETS (anciennement XXVILLAGEAZ)
# Hub PontAuxPorcelets (=> PontAuxPorcelets M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2220}] Item.tag.tch set value {position:{x:-3300,z:6632},connexions:[{line:121,distance:1,temps:1}]}

# PontAuxPorcelets M12 (=> Hub PontAuxPorcelets, GrandDégueulet M12, Gallicy M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2220,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:122,station:2400,distance:666,temps:1999},{line:123,station:2400,distance:666,temps:1999},{line:121,station:2210,distance:666,temps:1999}]}

### STATION VERNOGLIE LES MINES
# Hub Vernoglie (=> Vernoglie RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2240}] Item.tag.tch set value {position:{x:-1959,z:5745},connexions:[{line:2100,distance:1,temps:1}]}

# Vernoglie RERA (=> Hub Vernoglie, PortPutride RERA, Cyseal RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2240,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2153,station:2280,distance:630,temps:1999},{line:2154,station:2280,distance:630,temps:1999},{line:2101,station:2200,distance:1282,temps:1999}]}

### STATION PORT-PUTRIDE (anciennement XXVILLAGEAR)
# Hub PortPutride (=> PortPutride RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2280}] Item.tag.tch set value {position:{x:-2293,z:5056},connexions:[{line:2100,distance:1,temps:1}]}

# PortPutride RERA (=> Hub PortPutride, Chaugnes RERA, Vernoglie RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2280,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2153,station:2450,distance:975,temps:1999},{line:2154,station:2450,distance:975,temps:1999},{line:2101,station:2240,distance:630,temps:1999}]}

### STATION OREA SUR MER
# Hub Orea (=> Orea M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2300}] Item.tag.tch set value {position:{x:-1716,z:4302},connexions:[{line:101,distance:1,temps:1}]}

# Orea M10 (=> Hub Orea, Triel M10, Poudriole M10)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2300,idLine=101}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:101,station:2310,distance:666,temps:1999},{line:102,station:2390,distance:666,temps:1999}]}

### STATION TRIEL (anciennement XXVILLAGEAS)
# Hub Triel (=> Triel RERA/M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2310}] Item.tag.tch set value {position:{x:-2346,z:4108},connexions:[{line:2100,distance:1,temps:1},{line:101,distance:1,temps:1}]}

# Triel RERA (=> Hub Triel, Triel M10, PortPutride RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2310,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:101,distance:1,temps:1},{line:2101,station:2280,distance:954,temps:1999}]}

# Triel M10 (=> Hub Triel, Triel RERA, Chaugnes M10, Oréa M10)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2310,idLine=101}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:101,station:2450,distance:666,temps:1999},{line:102,station:2300,distance:666,temps:1999}]}

### STATION PORT-AUX-PENDUS (anciennement XXVILLAGEAL)
# Hub PortAuxPendus (=> PortAuxPendus RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2340}] Item.tag.tch set value {position:{x:-3128,z:2666},connexions:[{line:2100,distance:1,temps:1}]}

# PortAuxPendus RERA (=> Hub PortAuxPendus, Thomorgne RERA, Grenat RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2340,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2101,station:2350,distance:813,temps:1999},{line:2153,station:1100,distance:3226,temps:1999},{line:2154,station:1100,distance:3226,temps:1999}]}

### STATION THOMORGNE (anciennement XXVILLAGEAT)
# Hub Thomorgne (=> Thomorgne RERA/M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2350}] Item.tag.tch set value {position:{x:-3010,z:3454},connexions:[{line:2100,distance:1,temps:1},{line:121,distance:1,temps:1}]}

# Thomorgne RERA (=> Hub Thomorgne, Thomorgne M12, Triel RERA, PortAuxPendus RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2350,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:121,distance:1,temps:1},{line:2101,station:2310,distance:1000,temps:1999},{line:2153,station:2340,distance:813,temps:1999},{line:2154,station:2340,distance:813,temps:1999}]}

# Thomorgne M12 (=> Hub Thomorgne, Thomorgne RERA, PetitDégueulet M12, BourgFilou M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2350,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:121,station:2410,distance:666,temps:1999},{line:123,station:2360,distance:666,temps:1999}]}

### STATION BOURG-FILOU (anciennement XXVILLAGEBY)
# Hub BourgFilou (=> BourgFilou M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2360}] Item.tag.tch set value {position:{x:-2106,z:3674},connexions:[{line:121,distance:1,temps:1}]}

# BourgFilou M12 (=> Hub BourgFilou, Thomorgne M12, Gambergerie M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2360,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:121,station:2350,distance:666,temps:1999},{line:123,station:2380,distance:666,temps:1999}]}

### STATION LA GAMBERGERIE (anciennement XXVILLAGEAJ)
# Hub Gambergerie (=> Gambergerie M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2380}] Item.tag.tch set value {position:{x:-1500,z:3025},connexions:[{line:121,distance:1,temps:1}]}

# Gambergerie M12 (=> Hub Gambergerie, BourgFilou M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2380,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:121,station:2360,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2380,idLine=121}] idTerminus 123

### STATION POUDRIOLE (anciennement XXMETRO10XA)
# Hub Poudriole (=> Poudriole M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2390}] Item.tag.tch set value {position:{x:-195,z:4100},connexions:[{line:101,distance:1,temps:1}]}

# Poudriole M10 (=> Hub Poudriole, Oréa M10)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2390,idLine=101}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:101,station:2300,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2390,idLine=101}] idTerminus 102


### STATION LE GRAND-DÉGUEULET (anciennement XXVILLAGEAP)
# Hub GrandDégueulet (=> GrandDégueulet M10/M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2400}] Item.tag.tch set value {position:{x:-3517,z:4560},connexions:[{line:101,distance:1,temps:1},{line:121,distance:1,temps:1}]}

# GrandDégueulet M10 (=> Hub GrandDégueulet, GrandDégueulet M12, Vaillebourg M10, Chaugnes M10)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2400,idLine=101}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:121,distance:1,temps:1},{line:101,station:2420,distance:666,temps:1999},{line:102,station:2450,distance:666,temps:1999}]}

# GrandDégueulet M12 (=> Hub GrandDégueulet, GrandDégueulet M10, PetitDégueulet M12, PontAuxPorcelets M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2400,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:101,distance:1,temps:1},{line:122,station:2410,distance:666,temps:1999},{line:123,station:2410,distance:666,temps:1999},{line:121,station:2220,distance:666,temps:1999}]}

### STATION LE PETIT-DÉGUEULET (anciennement XXVILLAGEAN)
# Hub PetitDégueulet (=> PetitDégueulet M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2410}] Item.tag.tch set value {position:{x:-3525,z:4217},connexions:[{line:121,distance:1,temps:1}]}

# PetitDégueulet M12 (=> Hub PetitDégueulet, Caularçay M12, Thomorgne M12, GrandDégueulet M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2410,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:122,station:2480,distance:666,temps:1999},{line:123,station:2350,distance:666,temps:1999},{line:121,station:2400,distance:666,temps:1999}]}

### STATION VAILLEBOURG (anciennement XXVILLAGEAQ)
# Hub Vaillebourg (=> Vaillebourg M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2420}] Item.tag.tch set value {position:{x:-4011,z:4694},connexions:[{line:101,distance:1,temps:1}]}

# Vaillebourg M10 (=> Hub Vaillebourg, GrandDégueulet M10)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2420,idLine=101}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:102,station:2400,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2420,idLine=101}] idTerminus 101

### STATION CHAUGNES (anciennement XXVILLAGEAU)
# Hub Chaugnes (=> Chaugnes RERA/M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2450}] Item.tag.tch set value {position:{x:-2880,z:4454},connexions:[{line:2100,distance:1,temps:1},{line:101,distance:1,temps:1}]}

# Chaugnes RERA (=> Hub Chaugnes, Chaugnes M10, Thomorgne RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2450,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:101,distance:1,temps:1},{line:2153,station:2350,distance:1039,temps:1999},{line:2154,station:2350,distance:1039,temps:1999}]}

# Chaugnes M10 (=> Hub Chaugnes, Chaugnes RERA, GrandDégueulet M10, Triel M10)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2450,idLine=101}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:101,station:2400,distance:666,temps:1999},{line:102,station:2310,distance:666,temps:1999}]}

### STATION CAULARÇAY (anciennement XXVILLAGEAO)
# Hub Caularçay (=> Caularçay M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2480}] Item.tag.tch set value {position:{x:-3459,z:3355},connexions:[{line:121,distance:1,temps:1}]}

# Caularçay M12 (=> Hub Caularçay, Paupéry M12, PetitDégueulet M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2480,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:122,station:2490,distance:666,temps:1999},{line:121,station:2410,distance:666,temps:1999}]}

### STATION PAUPÉRY (anciennement XXVILLAGEAM)
# Hub Paupéry (=> Paupéry M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2490}] Item.tag.tch set value {position:{x:-4000,z:3000},connexions:[{line:121,distance:1,temps:1}]}

# Paupéry M12 (=> Hub Paupéry, Caularçay M12)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2490,idLine=121}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:121,station:2480,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2490,idLine=121}] idTerminus 122


### STATION LA POGNE-EN-ARDENCE (anciennement XXVILLAGECM)
# Hub PogneEnArdence (=> PogneEnArdence M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2700}] Item.tag.tch set value {position:{x:1265,z:-3676},connexions:[{line:91,distance:1,temps:1}]}

# PogneEnArdence M9 (=> Hub PogneEnArdence, Pompélinaud M9)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2700,idLine=91}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:91,station:3160,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2700,idLine=91}] idTerminus 92


### STATION TILIA (anciennement XXVILLAGECN)
# Hub Tilia (=> Tilia M4)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2730}] Item.tag.tch set value {position:{x:1730,z:-2482},connexions:[{line:41,distance:1,temps:1}]}

# Tilia M4 (=> Hub Tilia, Béhue M4)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2730,idLine=41}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:42,station:1280,distance:1397,temps:3156}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=2730,idLine=41}] idTerminus 41


### STATION XXVILLAGECS
# Hub Venise (=> Venise RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3000}] Item.tag.tch set value {position:{x:1800,z:-6600},connexions:[{line:2100,distance:1,temps:1}]}

# Venise RERA (=> Hub Venise, Barbourrigues RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3000,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2101,station:3100,distance:2333,temps:1999},{line:2102,station:3100,distance:2333,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3000,idLine=2100}] idTerminus 2154


### STATION BARBOURRIGUES (anciennement XXVILLAGEBQ)
# Hub Barbourrigues (=> Barbourrigues RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3100}] Item.tag.tch set value {position:{x:420,z:-5273},connexions:[{line:2100,distance:1,temps:1}]}

# Barbourrigues RERA (=> Hub Barbourrigues, Venise RERA, Floutrias RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3100,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2154,station:3000,distance:2333,temps:1999},{line:2101,station:3150,distance:1511,temps:1999},{line:2102,station:3150,distance:1511,temps:1999}]}

### STATION FLOUTRIAS (anciennement XXVILLAGEBP)
# Hub Floutrias (=> Floutrias RERA/M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3150}] Item.tag.tch set value {position:{x:614,z:-3811},connexions:[{line:2100,distance:1,temps:1},{line:91,distance:1,temps:1}]}

# Floutrias RERA (=> Hub Floutrias, Floutrias M9, Barbourrigues RERA, SommieuChalandières RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3150,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:91,distance:1,temps:1},{line:2154,station:3100,distance:1511,temps:1999},{line:2101,station:3200,distance:1452,temps:1999},{line:2102,station:3200,distance:1452,temps:1999}]}

# Floutrias M9 (=> Hub Floutrias, Floutrias RERA, Monthornet M9, Pompélinaud M9)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3150,idLine=91}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:91,station:3620,distance:666,temps:1999},{line:92,station:3160,distance:666,temps:1999}]}

### STATION POMPÉLINAUD (anciennement CAVERNE NAINE 2)
# Hub Pompélinaud (=> Pompélinaud M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3160}] Item.tag.tch set value {position:{x:917,z:-3814},connexions:[{line:91,distance:1,temps:1}]}

# Pompélinaud M9 (=> Hub Pompélinaud, Floutrias M9, PogneEnArdence M9)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3160,idLine=91}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:91,station:3150,distance:666,temps:1999},{line:92,station:2700,distance:666,temps:1999}]}


### STATION SOMMIEU–CHALANDIÈRES (anciennement XXVILLAGEBO–BN)
# Hub SommieuChalandières (=> SommieuChalandières RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3200}] Item.tag.tch set value {position:{x:446,z:-2413},connexions:[{line:2100,distance:1,temps:1}]}

# SommieuChalandières RERA (=> Hub SommieuChalandières, Floutrias RERA, Fouizy RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3200,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2154,station:3150,distance:1452,temps:1999},{line:2101,station:3230,distance:546,temps:1999},{line:2102,station:3230,distance:546,temps:1999}]}

### STATION FOUIZY-LA-DÉCHARGE (anciennement XXVILLAGEBD)
# Hub Fouizy (=> Fouizy RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3230}] Item.tag.tch set value {position:{x:510,z:-1878},connexions:[{line:2100,distance:1,temps:1}]}

# Fouizy RERA (=> Hub Fouizy, SommieuChalandières RERA, Sablons–Brimiel RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3230,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2154,station:3200,distance:546,temps:1999},{line:2101,station:1370,distance:822,temps:1999},{line:2102,station:1370,distance:822,temps:1999}]}


### STATION CHOUIGNY (anciennement XXVILLAGEBA)
# Hub Chouigny (=> Chouigny RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3300}] Item.tag.tch set value {position:{x:-2087,z:-2993},connexions:[{line:2100,distance:1,temps:1}]}

# Chouigny RERA (=> Hub Chouigny, Évernay RERA, Traverzy RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3300,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2153,station:3550,distance:1125,temps:1999},{line:2101,station:3370,distance:773,temps:1999},{line:2102,station:3370,distance:773,temps:1999}]}


### STATION MONTAGNE DU DESTIN (ex-MONTAGNE GIGANTESQUE)
# Hub MontagneDuDestin (=> MontagneDuDestin M3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3315}] Item.tag.tch set value {position:{x:-1232,z:-2117},connexions:[{line:31,distance:1,temps:1}]}

# MontagneDuDestin M3 (=> Hub MontagneDuDestin, Gléry M3, Malarân M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3315,idLine=31}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:32,station:3330,distance:740,temps:1694},{line:31,station:1330,distance:795,temps:1862}]}


### STATION GLÉRY (anciennement XXVILLAGEBR)
# Hub Gléry (=> Gléry RERA/M3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3330}] Item.tag.tch set value {position:{x:-861,z:-2667},connexions:[{line:2100,distance:1,temps:1},{line:31,distance:1,temps:1}]}

# Gléry RERA (=> Hub Gléry, Gléry M3, Traverzy RERA, Touissy RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3330,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:31,distance:1,temps:1},{line:2153,station:3370,distance:701,temps:1999},{line:2101,station:3350,distance:883,temps:1999},{line:2102,station:3350,distance:883,temps:1999}]}

# Gléry M3 (=> Hub Gléry, Gléry RERA, MontagneDuDestin M3)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3330,idLine=31}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:31,station:3315,distance:752,temps:1686}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3330,idLine=31}] idTerminus 32


### STATION TOUISSY (anciennement XXVILLAGEBC)
# Hub Touissy (=> Touissy RERA/M5a)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3350}] Item.tag.tch set value {position:{x:-448,z:-2096},connexions:[{line:2100,distance:1,temps:1},{line:51,distance:1,temps:1}]}

# Touissy RERA (=> Hub Touissy, Touissy M5a, Gléry RERA, Sablons–Brimiel RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3350,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:51,distance:1,temps:1},{line:2153,station:3330,distance:883,temps:1999},{line:2101,station:1370,distance:1299,temps:1999},{line:2102,station:1370,distance:1299,temps:1999}]}

# Touissy M5a (=> Hub Touissy, Touissy RERA, Glandy M5a)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3350,idLine=51}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:51,station:1390,distance:706,temps:1732}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3350,idLine=51}] idTerminus 52


### STATION TRAVERZY (anciennement XXVILLAGEBB)
# Hub Traverzy (=> Traverzy RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3370}] Item.tag.tch set value {position:{x:-1485,z:-2872},connexions:[{line:2100,distance:1,temps:1}]}

# Traverzy RERA (=> Hub Traverzy, Chouigny RERA, Gléry RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3370,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2153,station:3300,distance:773,temps:1999},{line:2101,station:3330,distance:701,temps:1999},{line:2102,station:3330,distance:701,temps:1999}]}


### STATION GÉNUFLAY (anciennement XXVILLAGEBI)
# Hub Génuflay (=> Génuflay M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3400}] Item.tag.tch set value {position:{x:-3247,z:-2670},connexions:[{line:111,distance:1,temps:1}]}

# Génuflay M11 (=> Hub Génuflay, Port-Castoy M11, Glouzy M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3400,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:111,station:3410,distance:666,temps:1999},{line:112,station:3480,distance:666,temps:1999}]}

### STATION PORT-CASTOY (anciennement XXVILLAGEBK)
# Hub Port-Castoy (=> Port-Castoy M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3410}] Item.tag.tch set value {position:{x:-3506,z:-2961},connexions:[{line:111,distance:1,temps:1}]}

# Port-Castoy M11 (=> Hub Port-Castoy, Flachoire M11, Génuflay M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3410,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:111,station:3415,distance:666,temps:1999},{line:112,station:3400,distance:666,temps:1999}]}

### STATION FLACHOIRE (anciennement FOSSE DE BROUDERF)
# Hub Flachoire (=> Flachoire M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3415}] Item.tag.tch set value {position:{x:-3736,z:-3086},connexions:[{line:111,distance:1,temps:1}]}

# Flachoire M11 (=> Hub Flachoire, RocMaeg M11, Port-Castoy M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3415,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:111,station:3570,distance:666,temps:1999},{line:112,station:3410,distance:666,temps:1999}]}

### STATION CHÂTEAU-DOUGLAS (anciennement XXVILLAGEBG)
# Hub Château-Douglas (=> Château-Douglas M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3450}] Item.tag.tch set value {position:{x:-2900,z:-1689},connexions:[{line:111,distance:1,temps:1}]}

# Château-Douglas M11 (=> Hub Château-Douglas, Glouzy M11, Croassin M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3450,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:111,station:3480,distance:666,temps:1999},{line:112,station:3460,distance:666,temps:1999}]}

### STATION CROASSIN-LE-DÉSERT (anciennement XXVILLAGEBH)
# Hub Croassin (=> Croassin M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3460}] Item.tag.tch set value {position:{x:-2652,z:-1280},connexions:[{line:111,distance:1,temps:1}]}

# Croassin M11 (=> Hub Croassin, Château-Douglas M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3460,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:111,station:3450,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3460,idLine=111}] idTerminus 112

### STATION GLOUZY (anciennement XXVILLAGEBF)
# Hub Glouzy (=> Glouzy M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3480}] Item.tag.tch set value {position:{x:-2959,z:-1986},connexions:[{line:111,distance:1,temps:1}]}

# Glouzy M11 (=> Hub Glouzy, Génuflay M11, Château-Douglas M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3480,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:111,station:3400,distance:666,temps:1999},{line:112,station:3450,distance:666,temps:1999}]}


### STATION ÉVERNAY-L'AUMÔNE (anciennement XXRERA3X1)
# Hub Évernay (=> Évernay RERA/M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3550}] Item.tag.tch set value {position:{x:-2350,z:-4044},connexions:[{line:2100,distance:1,temps:1},{line:111,distance:1,temps:1}]}

# Évernay RERA (=> Hub Évernay, Évernay M11, BeurrieuxValdemont RERA, Chouigny RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3550,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:111,distance:1,temps:1},{line:2153,station:3700,distance:3119,temps:1999},{line:2101,station:3300,distance:1125,temps:1999},{line:2102,station:3300,distance:1125,temps:1999}]}

# Évernay M11 (=> Hub Évernay, Évernay RERA, AbyessThur M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3550,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2100,distance:1,temps:1},{line:112,station:3560,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3550,idLine=111}] idTerminus 111

### STATION ABYESS THÛR (anciennement FAILLE TROP SUUS)
# Hub AbyessThur (=> AbyessThur M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3560}] Item.tag.tch set value {position:{x:-3152,z:-3700},connexions:[{line:111,distance:1,temps:1}]}

# AbyessThur M11 (=> Hub AbyessThur, RocMaeg M11, Évernay M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3560,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:112,station:3570,distance:666,temps:1999},{line:111,station:3550,distance:666,temps:1999}]}

### STATION LE ROC-MAËG (anciennement GORGE WESHINTON)
# Hub RocMaeg (=> RocMaeg M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3570}] Item.tag.tch set value {position:{x:-3769,z:-3447},connexions:[{line:111,distance:1,temps:1}]}

# RocMaeg M11 (=> Hub RocMaeg, Flachoire M11, AbyessThur M11)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3570,idLine=111}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:112,station:3415,distance:666,temps:1999},{line:111,station:3560,distance:666,temps:1999}]}


### STATION CHAMBORION (anciennement TOILETTES DE TANTE ASTRUC)
# Hub Chamborion (=> Chamborion M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3600}] Item.tag.tch set value {position:{x:-1146,z:-4449},connexions:[{line:91,distance:1,temps:1}]}

# Chamborion M9 (=> Hub Chamborion, Monthornet M9)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3600,idLine=91}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:92,station:3620,distance:666,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3600,idLine=91}] idTerminus 91

### STATION MONTHORNET (anciennement CAVERNE PROFONDE LAVE)
# Hub Monthornet (=> Monthornet M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3620}] Item.tag.tch set value {position:{x:-720,z:-3870},connexions:[{line:91,distance:1,temps:1}]}

# Monthornet M9 (=> Hub Monthornet, Chamborion M9, Floutrias M9)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3620,idLine=91}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:91,station:3600,distance:666,temps:1999},{line:92,station:3150,distance:666,temps:1999}]}


### STATION BEURRIEUX–VALDEMONT (anciennement XXVILLAGEBT–BS)
# Hub BeurrieuxValdemont (=> BeurrieuxValdemont RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3700}] Item.tag.tch set value {position:{x:-3275,z:-6800},connexions:[{line:2100,distance:1,temps:1}]}

# BeurrieuxValdemont RERA (=> Hub BeurrieuxValdemont, Évernay RERA)
data modify entity @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3700,idLine=2100}] Item.tag.tch set value {connexions:[{distance:1,temps:1},{line:2101,station:3550,distance:3119,temps:1999},{line:2102,station:3550,distance:3119,temps:1999}]}
scoreboard players set @e[type=item_frame,tag=tchQuaiPC,limit=1,scores={idStation=3700,idLine=2100}] idTerminus 2153
# On procède station par station pour initialiser les quais

### STATION LE HAMEAU RIVE GAUCHE
# Hub Hameau (=> Hub HameauRD, Hameau M1/M2/M3, Hameau S1/S2/S3/S4)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1000}] Item.tag.display set value {Name:'{"text":"Le Hameau–Rive Gauche"}',station:{code:"HAG",nom:"Hameau–Rive Gauche",deDu:"du ",alAu:"au ",prefixe:"Le "}}

# Hameau S1 (=> Hub Hameau)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1000,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – BNH"}',sortie:{num:1,nom:"Banque Nationale",deDu:"de la ",alAu:"à la ",prefixe:"la "}}
# Hameau S2 (=> Hub Hameau)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1000,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Le Patio"}',sortie:{num:2,nom:"Patio",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Hameau S3 (=> Hub Hameau)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1000,idSortie=3}] Item.tag.display set value {Name:'{"text":"Sortie n°3 – Rive Droite"}',sortie:{num:3,nom:"rive droite",deDu:"de la ",alAu:"à la ",prefixe:"la "}}
# Hameau S4 (=> Hub Hameau)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1000,idSortie=4}] Item.tag.display set value {Name:'{"text":"Sortie n°4 – Glandebruine Nord"}',sortie:{num:4,nom:"Glandebruine",deDu:"de la rivière ",alAu:"à la rivière ",prefixe:""}}


### STATION LE HAMEAU GLANDEBRUINE NORD
# Hub Hameau Gbrn (=> Hameau Gbrn M2/M4, Hameau Gbrn S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1001}] Item.tag.display set value {Name:'{"text":"Le Hameau–Glandebruine Nord"}',station:{code:"HAN",nom:"Hameau–Glandebruine Nord",deDu:"du ",alAu:"au ",prefixe:"Le "}}

# Hameau Gbrn S1 (=> Hub Hameau Gbrn)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1001,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Auditorium"}',sortie:{num:1,nom:"auditorium",deDu:"de l'",alAu:"à l'",prefixe:"l'"}}

### STATION LE HAMEAU RIVE DROITE
# Hub HameauRD (=> Hub Hameau, Hameau RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1002}] Item.tag.display set value {Name:'{"text":"Le Hameau–Rive Droite"}',station:{code:"HAD",nom:"Hameau–Rive Droite",deDu:"du ",alAu:"au ",prefixe:"Le "}}

### STATION LE HAMEAU PORT
# Hub HameauPort (=> HameauPort M1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1004}] Item.tag.display set value {Name:'{"text":"Le Hameau–Port"}',station:{code:"HAP",nom:"Hameau–Port",deDu:"du ",alAu:"au ",prefixe:"Le "}}

### STATION KRAKEN
# Hub Kraken (=> Kraken M2, Kraken S1
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1010}] Item.tag.display set value {Name:'{"text":"Kraken"}',station:{code:"KRA",nom:"Kraken",deDu:"de ",alAu:"à ",prefixe:""}}

# Kraken S1 (=> Hub Kraken, LD CMP, LD Int R102/R103)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1010,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – CMP"}',sortie:{num:1,nom:"camp minier principal",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION KNOSOS
# Hub Knosos (=> Knosos M1/M4, Knosos S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1020}] Item.tag.display set value {Name:'{"text":"Knosos"}',station:{code:"KNO",nom:"Knosos",deDu:"de ",alAu:"à ",prefixe:""}}

# Knosos S1 (=> Hub Knosos, LD MinesTotem)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1020,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Mines du Totem"}',sortie:{num:1,nom:"mines du Totem",deDu:"des ",alAu:"aux ",prefixe:"les "}}
# Knosos S2 (=> Hub Knosos)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1020,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Falaises de Knosos"}',sortie:{num:2,nom:"falaises de Knosos",deDu:"des ",alAu:"aux ",prefixe:"les "}}

### STATION VERCHAMPS
# Hub Verchamps (=> Verchamps M3/M4, Verchamps S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1030}] Item.tag.display set value {Name:'{"text":"Verchamps"}',station:{code:"VER",nom:"Verchamps",deDu:"de ",alAu:"à ",prefixe:""}}

# Verchamps S1 (=> Hub Verchamps)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1030,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION GRENAT VILLE
# Hub Grenat (=> Grenat M1/M5a/RERA, Grenat S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1100}] Item.tag.display set value {Name:'{"text":"Grenat–Ville"}',station:{code:"GRV",nom:"Grenat–Ville",deDu:"de ",alAu:"à ",prefixe:""}}

# Grenat S1 (=> Hub Grenat)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1100,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Forum"}',sortie:{num:1,nom:"Forum",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Grenat S2 (=> Hub Grenat)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1100,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Place du Pendu"}',sortie:{num:2,nom:"place du Pendu",deDu:"de la ",alAu:"à la ",prefixe:"la "}}
# Grenat S3 (=> Hub Grenat)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1100,idSortie=3}] Item.tag.display set value {Name:'{"text":"Sortie n°3 – Marché"}',sortie:{num:3,nom:"place du marché",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION GRENAT PORT
# Hub GrenatPort (=> GrenatPort M1/M5a, GrenatPort S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1102}] Item.tag.display set value {Name:'{"text":"Grenat–Port"}',station:{code:"GRP",nom:"Grenat–Port",deDu:"de ",alAu:"à ",prefixe:""}}

# GrenatPort S1 (=> Hub GrenatPort)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1102,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Port de plaisance"}',sortie:{num:1,nom:"port de plaisance",deDu:"du ",alAu:"au ",prefixe:"le "}}
# GrenatPort S2 (=> Hub GrenatPort)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1102,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Place du Pendu–Port"}',sortie:{num:2,nom:"place du Pendu",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION GRENAT RECIF
# Hub Recif (=> Recif S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1105}] Item.tag.display set value {Name:'{"text":"Grenat–Récif"}',station:{code:"GRR",nom:"Grenat–Récif",deDu:"de ",alAu:"à ",prefixe:""}}

# Recif S1 (=> Hub Recif, LD Sanor)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1105,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Sanor"}',sortie:{num:1,nom:"crique de Sanor",deDu:"de la ",alAu:"à la ",prefixe:"la "}}
# Recif S2 (=> Hub Recif, LD Preskrik)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1105,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Preskrik"}',sortie:{num:2,nom:"lac de Preskrik",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Recif S3 (=> Hub Recif, LD Recif)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1105,idSortie=3}] Item.tag.display set value {Name:'{"text":"Sortie n°3 – Récif"}',sortie:{num:3,nom:"Récif",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION GRENAT ARITHMATIE
# Hub Arithmatie (=> Arithmatie M5a)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1110}] Item.tag.display set value {Name:'{"text":"Grenat–Arithmatie"}',station:{code:"GRA",nom:"Grenat–Arithmatie",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION ASVAARD RUDELIEU
# Hub AsvaardRudelieu (=> AsvaardRudelieu M5a)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1115}] Item.tag.display set value {Name:'{"text":"Asvaard–Rudelieu"}',station:{code:"ASV",nom:"Asvaard–Rudelieu",deDu:"d'",alAu:"à ",prefixe:""}}

### STATION VILLONNE
# Hub Villonne (=> Villonne M3/M5b/RERA, Villonne S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1150}] Item.tag.display set value {Name:'{"text":"Villonne"}',station:{code:"VIL",nom:"Villonne",deDu:"de ",alAu:"à ",prefixe:""}}

# Villonne S1 (=> Hub Villonne)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1150,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Villonne S2 (=> Hub Villonne)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1150,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Champs"}',sortie:{num:2,nom:"champs",deDu:"des ",alAu:"aux ",prefixe:"les "}}

### STATION FORET DE JEEJ
# Hub Jeej (=> Jeej M3, Jeej S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1155}] Item.tag.display set value {Name:'{"text":"Forêt de Jëej"}',station:{code:"FDJ",nom:"Forêt de Jëej",deDu:"de la ",alAu:"à la ",prefixe:""}}

# Jeej S1 (=> Hub Jeej)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1155,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Mont Jeëj"}',sortie:{num:1,nom:"mont Jëej",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Jeej S2 (=> Hub Jeej, LD BaseNautiqueJeej)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1155,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Base nautique"}',sortie:{num:2,nom:"base nautique",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION BÉOTHAS
# Hub Béothas (=> Béothas RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1160}] Item.tag.display set value {Name:'{"text":"Béothas"}',station:{code:"BEO",nom:"Béothas",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION TOLBROK
# Hub Tolbrok (=> Tolbrok M3/M7, Tolbrok S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1170}] Item.tag.display set value {Name:'{"text":"Tolbrok"}',station:{code:"TOL",nom:"Tolbrok",deDu:"de ",alAu:"à ",prefixe:""}}

# Tolbrok S1 (=> Hub Tolbrok)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1170,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION DUERROM
# Hub Duerrom (=> Duerrom M3, Duerrom S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1180}] Item.tag.display set value {Name:'{"text":"Duerrom"}',station:{code:"DUE",nom:"Duerrom",deDu:"de ",alAu:"à ",prefixe:""}}

# Duerrom S1 (=> Hub Duerrom)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1180,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"dernier sous-sol du pilier central",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION DORLINOR
# Hub Dorlinor (=> Dorlinor M1/M5b/M6, Dorlinor S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1200}] Item.tag.display set value {Name:'{"text":"Dorlinor"}',station:{code:"DOR",nom:"Dorlinor",deDu:"de ",alAu:"à ",prefixe:""}}

# Dorlinor S1 (=> Hub Dorlinor)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1200,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION DESERT LAAR
# Hub Désert Laar (=> Désert Laar M5b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1205}] Item.tag.display set value {Name:'{"text":"Désert Laar"}',station:{code:"LAR",nom:"Désert Laar",deDu:"du ",alAu:"au ",prefixe:""}}

### STATION RIVES DE LAAR
# Hub Rives de Laar (=> Rives de Laar M5b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1206}] Item.tag.display set value {Name:'{"text":"Rives de Laar"}',station:{code:"RIV",nom:"Rives de Laar",deDu:"des ",alAu:"aux ",prefixe:""}}

### STATION YGRIAK
# Hub Ygriak (=> Ygriak M1/M4, Ygriak S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1210}] Item.tag.display set value {Name:'{"text":"Ygriak"}',station:{code:"YGK",nom:"Ygriak",deDu:"d'",alAu:"à ",prefixe:""}}

# Ygriak S1 (=> Hub Ygriak)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1210,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION ZOBUN-ECLESTA
# Hub ZobunEclesta (=> ZobunEclesta M2, ZobunEclesta S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1220}] Item.tag.display set value {Name:'{"text":"Zobun–Éclesta"}',station:{code:"ZOE",nom:"Zobun–Éclesta",deDu:"de ",alAu:"à ",prefixe:""}}

# ZobunEclesta S1 (=> Hub ZobunEclesta)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1220,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Plateau"}',sortie:{num:1,nom:"plateau",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION EVENIS–ECLESTA
# Hub EvenisEclesta (=> EvenisEclesta M2, EvenisEclesta S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1230}] Item.tag.display set value {Name:'{"text":"Évenis–Éclesta"}',station:{code:"EVE",nom:"Évenis–Éclesta",deDu:"d'",alAu:"à ",prefixe:""}}

# EvenisEclesta S1 (=> Hub EvenisEclesta, LD Début R124, LD BaseNautiqueEvenisEclesta)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1230,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Base nautique"}',sortie:{num:1,nom:"base nautique",deDu:"de la ",alAu:"à la ",prefixe:"la "}}
# EvenisEclesta S2 (=> Hub EvenisEclesta)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1230,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Forêt d\'Évenis"}',sortie:{num:2,nom:"forêt d'Évenis",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION ATHÈS
# Hub Athès (=> Athès M6)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1250}] Item.tag.display set value {Name:'{"text":"Athès"}',station:{code:"ATS",nom:"Athès",deDu:"d'",alAu:"à ",prefixe:""}}

### STATION LA BÉHUE (anciennement XXVILLAGEBU)
# Hub Béhue (=> Béhue M4, Béhue S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1280}] Item.tag.display set value {Name:'{"text":"Béhue"}',station:{code:"BUE",nom:"Béhue",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

# Béhue S1 (=> Hub Béhue)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1280,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre-ville"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION LES SABLONS
# Hub Sablons (=> Sablons M3/M6, Sablons S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1300}] Item.tag.display set value {Name:'{"text":"Les Sablons"}',station:{code:"SAB",nom:"Sablons",deDu:"des ",alAu:"aux ",prefixe:"Les "}}

# Sablons S1 (=> Hub Sablons, LD SanctuaireTemps, LD Eremos)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1300,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION CHIZÂN
# Hub Chizân (=> Chizân M3/M5a, Chizân S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1320}] Item.tag.display set value {Name:'{"text":"Chizân"}',station:{code:"CHI",nom:"Chizân",deDu:"de ",alAu:"à ",prefixe:""}}

# Chizan S1 (=> Hub Chizân, LD Chizan)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1320,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Temple"}',sortie:{num:1,nom:"ruines de la zone résidentielle",deDu:"des ",alAu:"aux ",prefixe:"les "}}

### STATION MALARÂN (anciennement XXMETRO3XA)
# Hub Malarân (=> Malarân M3, Malarân S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1330}] Item.tag.display set value {Name:'{"text":"Malarân"}',station:{code:"MAL",nom:"Malarân",deDu:"de ",alAu:"à ",prefixe:""}}

# Malarân S1 (=> Hub Malarân)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1330,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION DESERT ONYX
# Hub Onyx (=> Onyx M2, Onyx S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1350}] Item.tag.display set value {Name:'{"text":"Désert Onyx"}',station:{code:"ONY",nom:"Désert Onyx",deDu:"du ",alAu:"au ",prefixe:""}}

# Onyx S1 (=> Hub Onyx, LD CitadelleGzor)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1350,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Citadelle de Gzor"}',sortie:{num:1,nom:"citadelle de Gzor",deDu:"de la ",alAu:"à la ",prefixe:"la "}}
# Onyx S2 (=> Hub Onyx, LD CrypteGzor)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1350,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Crypte de Gzor"}',sortie:{num:2,nom:"crypte de Gzor",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION ALWIN-ONYX
# Hub AlwinOnyx (=> AlwinOnyx M2/M6, AlwinOnyx S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1360}] Item.tag.display set value {Name:'{"text":"Alwin–Onyx"}',station:{code:"ALO",nom:"Alwin–Onyx",deDu:"d'",alAu:"à ",prefixe:""}}

# AlwinOnyx S1 (=> Hub AlwinOnyx, LD ChateauOnyx)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1360,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Château Onyx"}',sortie:{num:1,nom:"château Onyx",deDu:"du ",alAu:"au ",prefixe:"le "}}
# AlwinOnyx S2 (=> Hub AlwinOnyx)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1360,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Côtes d\'Alwin"}',sortie:{num:2,nom:"côtes d'Alwin",deDu:"des ",alAu:"aux ",prefixe:"les "}}

### STATION SABLONS–BRIMIEL (anciennement Sablons–Brimiel)
# Hub Sablons–Brimiel (=> Sablons–Brimiel RER A, Sablons–Brimiel M2, Sablons–Brimiel S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1370}] Item.tag.display set value {Name:'{"text":"Sablons–Brimiel"}',station:{code:"SAL",nom:"Sablons–Brimiel",deDu:"du ",alAu:"au ",prefixe:""}}

# Sablons–Brimiel S1 (=> Hub Sablons–Brimiel)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1370,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Plaine"}',sortie:{num:1,nom:"plaine",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION GLANDY (anciennement XXVILLAGEBE)
# Hub Glandy (=> Glandy M5a, Glandy S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1390}] Item.tag.display set value {Name:'{"text":"Glandy"}',station:{code:"GLA",nom:"Glandy",deDu:"du ",alAu:"au ",prefixe:""}}

# Glandy S1 (=> Hub Glandy)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1390,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre-ville"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION FORT DU VAL
# Hub FortDuVal (=> FortDuVal M5a)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1400}] Item.tag.display set value {Name:'{"text":"Fort du Val"}',station:{code:"FOV",nom:"Fort du Val",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION ILLYSIA
# Hub Illysia (=> Illysia M4, Illysia S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1420}] Item.tag.display set value {Name:'{"text":"Illysia"}',station:{code:"ILS",nom:"Illysia",deDu:"d'",alAu:"à ",prefixe:""}}

# Illysia S2 (=> Hub Illysia, LD Entrée d'Illysia)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1420,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Parvis"}',sortie:{num:1,nom:"parvis du château",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Illysia S1 (=> Hub Illysia, LD Entrée d'Illysia, LD Sortie d'Illysia)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1420,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Catacombes"}',sortie:{num:1,nom:"catacombes",deDu:"des ",alAu:"aux ",prefixe:"les "}}

### STATION FULTEZ-EN-SULUM
# Hub Fultèz (=> Fultèz M5a/M6)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1450}] Item.tag.display set value {Name:'{"text":"Fultèz-en-Sulûm"}',station:{code:"FUS",nom:"Fultèz-en-Sulûm",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION PIEUZE-EN-SULUM
# Hub Pieuze (=> Pieuze M4/M5a, Pieuze S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1460}] Item.tag.display set value {Name:'{"text":"Pieuze-en-Sulûm"}',station:{code:"PIS",nom:"Pieuze-en-Sulûm",deDu:"de ",alAu:"à ",prefixe:""}}

# Pieuze S1 (=> Hub Pieuze)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1460,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION MONTAGNES DE KRYPT
# Hub Krypt (=> Krypt M1, Krypt S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1490}] Item.tag.display set value {Name:'{"text":"Montagnes de Krypt"}',station:{code:"KRY",nom:"Montagnes de Krypt",deDu:"des ",alAu:"aux ",prefixe:""}}

# Krypt S1 (=> Hub Krypt)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1490,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Pic"}',sortie:{num:1,nom:"pic Kynapaz-Ankordenon",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Krypt S2 (=> Hub Krypt)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1490,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Berges du Crochu"}',sortie:{num:2,nom:"berges du Crochu",deDu:"des ",alAu:"aux ",prefixe:"les "}}
# Krypt S3 (=> Hub Krypt)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1490,idSortie=3}] Item.tag.display set value {Name:'{"text":"Sortie n°3 – Sommets"}',sortie:{num:3,nom:"sommet de Panomez-Pourlainsthan",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION NERONIL
# Hub Néronil (=> Néronil M7, Néronil S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1500}] Item.tag.display set value {Name:'{"text":"Néronil-le-Pont"}',station:{code:"NER",nom:"Néronil-le-Pont",deDu:"de ",alAu:"à ",prefixe:""}}

# Néronil S1 (=> Hub Néronil)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1500,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION ÉVENIS-TAMLYN
# Hub EvenisTamlyn (=> EvenisTamlyn M5b/M7, EvenisTamlyn S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1520}] Item.tag.display set value {Name:'{"text":"Évenis–Tamlyn"}',station:{code:"EVT",nom:"Évenis–Tamlyn",deDu:"d'",alAu:"à ",prefixe:""}}

# EvenisTamlyn S1 (=> Hub EvenisTamlyn)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1520,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Clairière"}',sortie:{num:1,nom:"clairière de l'Absence d'Indications-Explicites",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION EIDIN-LA-VALLÉE
# Hub Eidin (=> Eidin M7b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1530}] Item.tag.display set value {Name:'{"text":"Eïdin-la-Vallée"}',station:{code:"ELV",nom:"Eïdin-la-Vallée",deDu:"d'",alAu:"à ",prefixe:""}}

### STATION LITORÉA
# Hub Litoréa (=> Litoréa M7/M7b, Litoréa S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1550}] Item.tag.display set value {Name:'{"text":"Litoréa"}',station:{code:"LIT",nom:"Litoréa",deDu:"de ",alAu:"à ",prefixe:""}}

# Litoréa S1 (=> Hub Litoréa)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1550,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Litoréa S2 (=> Hub Litoréa)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1550,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Port"}',sortie:{num:2,nom:"port",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Litoréa S3 (=> Hub Litoréa)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1550,idSortie=3}] Item.tag.display set value {Name:'{"text":"Sortie n°3 – Plaine Briomir"}',sortie:{num:3,nom:"plaine Briomir",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION MORONIA
# Hub Moronia (=> Moronia M7b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1590}] Item.tag.display set value {Name:'{"text":"Moronia"}',station:{code:"MOR",nom:"Moronia",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION HADÈS
# Hub Hadès (=> Hadès M5b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1600}] Item.tag.display set value {Name:'{"text":"Hadès"}',station:{code:"HDS",nom:"Hadès",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION MIDÈS
# Hub Midès (=> Midès M5b/M6)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1610}] Item.tag.display set value {Name:'{"text":"Midès"}',station:{code:"MDS",nom:"Midès",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION QUATRE CHEMINS
# Hub 4chemins (=> 4chemins M5b/M7b, 4chemins S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1650}] Item.tag.display set value {Name:'{"text":"Quatre Chemins"}',station:{code:"QCH",nom:"Quatre Chemins",deDu:"de ",alAu:"à ",prefixe:""}}

# 4chemins S1 (=> Hub 4chemins)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1650,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Place des 4 chemins"}',sortie:{num:1,nom:"place des 4 chemins",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION LE ROUSTIFLET
# Hub Roustiflet (=> Roustiflet RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1800}] Item.tag.display set value {Name:'{"text":"Le Roustiflet"}',station:{code:"RTF",nom:"Roustiflet",deDu:"du ",alAu:"au ",prefixe:"Le "}}

### STATION THOROÏR–VALDÉE
# Hub ThoroirValdee (=> ThoroirValdee M7, ThoroirValdee S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1820}] Item.tag.display set value {Name:'{"text":"Thoroïr–Valdée"}',station:{code:"TOV",nom:"Thoroïr–Valdée",deDu:"de ",alAu:"à ",prefixe:""}}

# ThoroirValdee S1 (=> Hub ThoroirValdee)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1820,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Rade"}'}
# ThoroirValdee S2 (=> Hub ThoroirValdee)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1820,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Ascenseur"}'}

### STATION THALRION
# Hub Thalrion (=> Thalrion M7, Thalrion S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1850}] Item.tag.display set value {Name:'{"text":"Thalrion"}',station:{code:"THA",nom:"Thalrion",deDu:"de ",alAu:"à ",prefixe:""}}

# Thalrion S1 (=> Hub Thalrion)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1850,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Port"}',sortie:{num:1,nom:"port",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION CHASSY SUR FLUMINE
# Hub Chassy (=> Chassy M7/RERA, Chassy S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1860}] Item.tag.display set value {Name:'{"text":"Chassy-sur-Flumine"}',station:{code:"CSF",nom:"Chassy-sur-Flumine",deDu:"de ",alAu:"à ",prefixe:""}}

# Chassy S1 (=> Hub Chassy)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1860,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Chassy S2 (=> Hub Chassy)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1860,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Gare"}',sortie:{num:2,nom:"gare RER",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION EVROCQ SAUVEBONNE
# Hub Evrocq (=> Evrocq RERA, Evrocq CA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1900}] Item.tag.display set value {Name:'{"text":"Évrocq–Sauvebonne"}',station:{code:"EQS",nom:"Évrocq–Sauvebonne",deDu:"d'",alAu:"à ",prefixe:""}}

# Evrocq RERA (=> Hub Evrocq, Chassy RERA, Procyon RERA)
# Pas de connexion directe RERA <> CA ; il faut passer via le hub, comme IG (sortir et repasser aux valideurs)

### STATION EVROCQ-LE-BAS
# Hub EvrocqLeBas (=> EvrocqLeBas CA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1904}] Item.tag.display set value {Name:'{"text":"Évrocq–Le Bas"}',station:{code:"EQB",nom:"Évrocq–Le Bas",deDu:"d'",alAu:"à ",prefixe:""}}

### STATION EVROCQ-LE-HAUT
# Hub EvrocqLeHaut (=> EvrocqLeHaut CA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1905}] Item.tag.display set value {Name:'{"text":"Évrocq–Le Haut"}',station:{code:"EQH",nom:"Évrocq–Le Haut",deDu:"d'",alAu:"à ",prefixe:""}}

### STATION ILE DU SINGE
# Hub IleDuSinge (=> IleDuSinge S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1910}] Item.tag.display set value {Name:'{"text":"Île du Singe"}',station:{code:"SIN",nom:"Île du Singe",deDu:"de l'",alAu:"à l'",prefixe:""}}

# IleDuSinge S1 (=> Hub IleDuSinge)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1910,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Hauteurs de l\'île"}',sortie:{num:1,nom:"hauteurs de l'île",deDu:"des ",alAu:"aux ",prefixe:"les "}}
# IleDuSinge S2 (=> Hub IleDuSinge)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1910,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Plage"}',sortie:{num:1,nom:"plage",deDu:"de la ",alAu:"à la ",prefixe:"la "}}

### STATION LE RELAIS
# Hub LeRelais (=> LeRelais M8b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1950}] Item.tag.display set value {Name:'{"text":"Le Relais"}',station:{code:"REL",nom:"Relais",deDu:"du ",alAu:"au ",prefixe:"Le "}}

### STATION MOLDOR BALCHAIS
# Hub Moldor–Balchaïs (=> Moldor–Balchaïs M8b)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1960}] Item.tag.display set value {Name:'{"text":"Moldor–Balchaïs"}',station:{code:"MOB",nom:"Moldor–Balchaïs",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION QUECOL EN DRIE
# Hub Quecol (=> Quecol M8b, Quecol S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=1980}] Item.tag.display set value {Name:'{"text":"Quécol-en-Drie"}',station:{code:"QED",nom:"Quécol-en-Drie",deDu:"de ",alAu:"à ",prefixe:""}}

# Quécol S1 (=> Hub Quécol)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=1980,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Haut-plateau"}',sortie:{num:1,nom:"haut-plateau",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION PROCYON
# Hub Procyon (=> Procyon M8/RERA, Procyon S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2000}] Item.tag.display set value {Name:'{"text":"Procyon"}',station:{code:"PRO",nom:"Procyon",deDu:"de ",alAu:"à ",prefixe:""}}

# Procyon S1 (=> Hub Procyon)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2000,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Est"}',sortie:{num:1,nom:"est de la gare",deDu:"de l'",alAu:"à l'",prefixe:"l'"}}
# Procyon S2 (=> Hub Procyon)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2000,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Ouest"}',sortie:{num:2,nom:"ouest de la gare",deDu:"de l'",alAu:"à l'",prefixe:"l'"}}

### STATION PROCYON TENEBRES
# Hub Ténèbres (=> Ténèbres M8, Ténèbres S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2001}] Item.tag.display set value {Name:'{"text":"Procyon–Ténèbres"}',station:{code:"PRT",nom:"Procyon–Ténèbres",deDu:"de ",alAu:"à ",prefixe:""}}

# Ténèbres S1 (=> Hub Ténèbres, LD Donjon des Ténèbres)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2001,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Esplanade"}',sortie:{num:1,nom:"esplanade du donjon",deDu:"de l'",alAu:"à l'",prefixe:"l'"}}

### STATION PROCYON UNIVERSITÉ
# Hub ProcyonUniversité (=> ProcyonUniversité M8, ProcyonUniversité S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2005}] Item.tag.display set value {Name:'{"text":"Procyon–Université"}',station:{code:"PRU",nom:"Procyon–Université",deDu:"de ",alAu:"à ",prefixe:""}}

# ProcyonUniversité S1 (=> Hub ProcyonUniversité)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2005,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Université"}',sortie:{num:1,nom:"université de Procyon",deDu:"de l'",alAu:"à l'",prefixe:"l'"}}

### STATION AULNOY
# Hub Aulnoy (=> Aulnoy M8, Aulnoy S1)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2010}] Item.tag.display set value {Name:'{"text":"Aulnoy"}',station:{code:"AUL",nom:"Aulnoy",deDu:"d'",alAu:"à ",prefixe:""}}

# Aulnoy S1 (=> Hub Aulnoy)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2010,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Centre"}',sortie:{num:1,nom:"centre-ville",deDu:"du ",alAu:"au ",prefixe:"le "}}

### STATION ARGENCON
# Hub Argençon (=> Argençon M8, Argençon S1/S2)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2050}] Item.tag.display set value {Name:'{"text":"Argençon"}',station:{code:"ARG",nom:"Argençon",deDu:"d'",alAu:"à ",prefixe:""}}

# Argençon S1 (=> Hub Argençon)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2050,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Lac"}',sortie:{num:1,nom:"lac",deDu:"du ",alAu:"au ",prefixe:"le "}}
# Argençon S2 (=> Hub Argençon)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2050,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Collines"}',sortie:{num:1,nom:"collines",deDu:"des ",alAu:"aux ",prefixe:"les "}}

### STATION FRAMBOURG TERNELIEU
# Hub FrambourgTernelieu (=> FrambourgTernelieu M8/M8b, FrambourgTernelieu S1/S2/S3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2100}] Item.tag.display set value {Name:'{"text":"Frambourg–Ternelieu"}',station:{code:"FRT",nom:"Frambourg–Ternelieu",deDu:"de ",alAu:"à ",prefixe:""}}

# FrambourgTernelieu S1 (=> Hub FrambourgTernelieu, LD Ternelieu)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2100,idSortie=1}] Item.tag.display set value {Name:'{"text":"Sortie n°1 – Ternelieu"}',sortie:{num:1,nom:"porte de Ternelieu",deDu:"de la ",alAu:"à la ",prefixe:"la "}}
# FrambourgTernelieu S2 (=> Hub FrambourgTernelieu, LD Frambourg)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2100,idSortie=2}] Item.tag.display set value {Name:'{"text":"Sortie n°2 – Frambourg"}',sortie:{num:2,nom:"porte de Frambourg",deDu:"de la ",alAu:"à la ",prefixe:"la "}}
# FrambourgTernelieu S3 (=> RIEN (sortie uniquement) + LD Frambourg, LD Ternelieu)
data modify entity @e[type=item_frame,tag=tchSortiePC,limit=1,scores={idStation=2100,idSortie=3}] Item.tag.display set value {Name:'{"text":"Sortie n°3 – Sous les voies"}',sortie:{num:3,nom:"située sous les voies",deDu:"de la sortie ",alAu:"à la sortie ",prefixe:""}}

### STATION GEORLIE
# Hub Géorlie (=> Géorlie M8)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2120}] Item.tag.display set value {Name:'{"text":"Géorlie"}',station:{code:"GEO",nom:"Géorlie",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION CALMEFLOT LES DEUX BERGES
# Hub Calmeflot (=> Calmeflot M8, Calmeflot CB)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2150}] Item.tag.display set value {Name:'{"text":"Calmeflot-les-Deux-Berges"}',station:{code:"CAL",nom:"Calmeflot-les-Deux-Berges",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION MILLOREAU LES PICS
# Hub Milloreau (=> Milloreau M8, Milloreau CB)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2160}] Item.tag.display set value {Name:'{"text":"Milloreau-les-Pics"}',station:{code:"MIL",nom:"Milloreau-les-Pics",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION LES PICS
# Hub LesPics (=> LesPics CB)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2161}] Item.tag.display set value {Name:'{"text":"Les Pics"}',station:{code:"PIC",nom:"Pics",deDu:"des ",alAu:"aux ",prefixe:"Les "}}

### STATION LA DODENE
# Hub LaDodene (=> LaDodene CB)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2170}] Item.tag.display set value {Name:'{"text":"La Dodène"}',station:{code:"DOD",nom:"Dodène",deDu:"de la ",alAu:"à la ",prefixe:"La "}}

### STATION BUSSY NIGEL
# Hub BussyNigel (=> BussyNigel M8)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2180}] Item.tag.display set value {Name:'{"text":"Bussy-Nigel"}',station:{code:"BUS",nom:"Bussy-Nigel",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION CRESTALI
# Hub Crestali (=> Crestali M8)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2190}] Item.tag.display set value {Name:'{"text":"Crestali"}',station:{code:"CRE",nom:"Crestali",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION CYSEAL
# Hub Cyseal (=> Cyseal RERA/M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2200}] Item.tag.display set value {Name:'{"text":"Cyséal"}',station:{code:"CYS",nom:"Cyséal",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION GALLICY (anciennement XXVILLAGEAY)
# Hub Gallicy (=> Gallicy M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2210}] Item.tag.display set value {Name:'{"text":"Gallicy-les-Putrides"}',station:{code:"GAL",nom:"Gallicy-les-Putrides",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION PONT-AUX-PORCELETS (anciennement XXVILLAGEAZ)
# Hub PontAuxPorcelets (=> PontAuxPorcelets M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2220}] Item.tag.display set value {Name:'{"text":"Le Pont-aux-Porcelets"}',station:{code:"XAZ",nom:"Pont-aux-Porcelets",deDu:"du ",alAu:"au ",prefixe:"Le "}}

### STATION VERNOGLIE LES MINES
# Hub Vernoglie (=> Vernoglie RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2240}] Item.tag.display set value {Name:'{"text":"Vernoglie-les-Mines"}',station:{code:"VLM",nom:"Vernoglie-les-Mines",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION PORT-PUTRIDE (anciennement XXVILLAGEAR)
# Hub PortPutride (=> PortPutride RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2280}] Item.tag.display set value {Name:'{"text":"Port-Putride"}',station:{code:"PUP",nom:"Port-Putride",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION OREA SUR MER
# Hub Orea (=> Orea M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2300}] Item.tag.display set value {Name:'{"text":"Oréa-sur-Mer"}',station:{code:"OSM",nom:"Oréa-sur-Mer",deDu:"d'",alAu:"à ",prefixe:""}}

### STATION TRIEL (anciennement XXVILLAGEAS)
# Hub Triel (=> Triel RERA/M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2310}] Item.tag.display set value {Name:'{"text":"Triel"}',station:{code:"TRI",nom:"Triel",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION PORT-AUX-PENDUS (anciennement XXVILLAGEAL)
# Hub PortAuxPendus (=> PortAuxPendus RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2340}] Item.tag.display set value {Name:'{"text":"Port-aux-Pendus"}',station:{code:"PAP",nom:"Port-aux-Pendus",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION THOMORGNE (anciennement XXVILLAGEAT)
# Hub Thomorgne (=> Thomorgne RERA/M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2350}] Item.tag.display set value {Name:'{"text":"Thomorgne"}',station:{code:"TOM",nom:"Thomorgne",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION BOURG-FILOU (anciennement XXVILLAGEBY)
# Hub BourgFilou (=> BourgFilou M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2360}] Item.tag.display set value {Name:'{"text":"Bourg-Filou"}',station:{code:"BOF",nom:"Bourg-Filou",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION GAMBERGERIE (anciennement XXVILLAGEAJ)
# Hub Gambergerie (=> Gambergerie M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2380}] Item.tag.display set value {Name:'{"text":"La Gambergerie"}',station:{code:"GAM",nom:"Gambergerie",deDu:"de la ",alAu:"à la ",prefixe:"La "}}

### STATION POUDRIOLE (anciennement XXMETRO10XA)
# Hub Poudriole (=> Poudriole M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2390}] Item.tag.display set value {Name:'{"text":"Poudriole"}',station:{code:"POU",nom:"Poudriole",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION GRAND-DÉGUEULET (anciennement XXVILLAGEAP)
# Hub GrandDégueulet (=> GrandDégueulet M10/M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2400}] Item.tag.display set value {Name:'{"text":"Le Grand-Dégueulet"}',station:{code:"DEG",nom:"Grand-Dégueulet",deDu:"du ",alAu:"au ",prefixe:"Le "}}

### STATION PETIT-DÉGUEULET (anciennement XXVILLAGEAN)
# Hub PetitDégueulet (=> PetitDégueulet M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2410}] Item.tag.display set value {Name:'{"text":"Le Petit-Dégueulet"}',station:{code:"DEP",nom:"Petit-Dégueulet",deDu:"du ",alAu:"au ",prefixe:"Le "}}

### STATION VAILLEBOURG (anciennement XXVILLAGEAQ)
# Hub Vaillebourg (=> Vaillebourg M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2420}] Item.tag.display set value {Name:'{"text":"Vaillebourg"}',station:{code:"VAB",nom:"Vaillebourg",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION CHAUGNES (anciennement XXVILLAGEAU)
# Hub Chaugnes (=> Chaugnes RERA/M10)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2450}] Item.tag.display set value {Name:'{"text":"Chaugnes"}',station:{code:"CGN",nom:"Chaugnes",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION CAULARCAY (anciennement XXVILLAGEAO)
# Hub Caularçay (=> Caularçay M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2480}] Item.tag.display set value {Name:'{"text":"Caularçay"}',station:{code:"CAU",nom:"Caularçay",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION PAUPÉRY (anciennement XXVILLAGEAM)
# Hub Paupéry (=> Paupéry M12)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2490}] Item.tag.display set value {Name:'{"text":"Paupéry"}',station:{code:"PPY",nom:"Paupéry",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION LA POGNE-EN-ARDENCE (anciennement XXVILLAGECM)
# Hub LaPogne (=> LaPogne M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2700}] Item.tag.display set value {Name:'{"text":"La Pogne-en-Ardence"}',station:{code:"PEA",nom:"Pogne-en-Ardence",deDu:"de la ",alAu:"à la ",prefixe:"La "}}


### STATION TILIA (anciennement XXVILLAGECN)
# Hub Tilia (=> Tilia M4)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=2730}] Item.tag.display set value {Name:'{"text":"Tilia"}',station:{code:"TIL",nom:"Tilia",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION XXVILLAGECS
# Hub Venise (=> Venise RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3000}] Item.tag.display set value {Name:'{"text":"Venise"}',station:{code:"XCS",nom:"Venise",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION BARBOURRIGUES (anciennement XXVILLAGEBQ)
# Hub Barbourrigues (=> Barbourrigues RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3100}] Item.tag.display set value {Name:'{"text":"Barbourrigues"}',station:{code:"BRB",nom:"Barbourrigues",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION FLOUTRIAS (anciennement XXVILLAGEBP)
# Hub Floutrias (=> Floutrias RERA/M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3150}] Item.tag.display set value {Name:'{"text":"Floutrias"}',station:{code:"FLO",nom:"Floutrias",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION POMPÉLINAUD (anciennement CAVERNE NAINE 2)
# Hub Pompélinaud (=> Pompélinaud M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3160}] Item.tag.display set value {Name:'{"text":"Pompélinaud"}',station:{code:"POP",nom:"Pompélinaud",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION SOMMIEU–CHALANDIÈRES (anciennement XXVILLAGEBO–BN)
# Hub SommieuChalandières (=> SommieuChalandières RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3200}] Item.tag.display set value {Name:'{"text":"Sommieu–Chalandières"}',station:{code:"SCH",nom:"Sommieu–Chalandières",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION FOUIZY-LA-DÉCHARGE (anciennement XXVILLAGEBD)
# Hub Fouizy (=> Fouizy RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3230}] Item.tag.display set value {Name:'{"text":"Fouizy-la-Décharge"}',station:{code:"FZY",nom:"Fouizy-la-Décharge",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION CHOUIGNY (anciennement XXVILLAGEBA)
# Hub Chouigny (=> Chouigny RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3300}] Item.tag.display set value {Name:'{"text":"Chouigny"}',station:{code:"CNY",nom:"Chouigny",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION MONTAGNE DU DESTIN (ex-MONTAGNE GIGANTESQUE)
# Hub MontagneDuDestin (=> MontagneDuDestin M3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3315}] Item.tag.display set value {Name:'{"text":"Montagne du Destin"}',station:{code:"DES",nom:"Montagne du Destin",deDu:"de la ",alAu:"à la ",prefixe:""}}


### STATION GLÉRY (anciennement XXVILLAGEBR)
# Hub Gléry (=> Gléry RERA/M3)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3330}] Item.tag.display set value {Name:'{"text":"Gléry"}',station:{code:"GLY",nom:"Gléry",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION TOUISSY (anciennement XXVILLAGEBC)
# Hub Touissy (=> Touissy RERA/M5a)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3350}] Item.tag.display set value {Name:'{"text":"Touissy"}',station:{code:"TSY",nom:"Touissy",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION TRAVERZY (anciennement XXVILLAGEBB)
# Hub Traverzy (=> Traverzy RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3370}] Item.tag.display set value {Name:'{"text":"Traverzy"}',station:{code:"TRA",nom:"Traverzy",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION GÉNUFLAY (anciennement XXVILLAGEBI)
# Hub Génuflay (=> Génuflay M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3400}] Item.tag.display set value {Name:'{"text":"Génuflay"}',station:{code:"GEN",nom:"Génuflay",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION PORT-CASTOY (anciennement XXVILLAGEBK)
# Hub Port-Castoy (=> Port-Castoy M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3410}] Item.tag.display set value {Name:'{"text":"Port-Castoy"}',station:{code:"POC",nom:"Port-Castoy",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION FLACHOIRE (anciennement FOSSE DE BROUDERF)
# Hub Flachoire (=> Flachoire M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3415}] Item.tag.display set value {Name:'{"text":"Flachoire"}',station:{code:"FLA",nom:"Flachoire",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION CHÂTEAU-DOUGLAS (anciennement XXVILLAGEBG)
# Hub Château-Douglas (=> Château-Douglas M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3450}] Item.tag.display set value {Name:'{"text":"Château-Douglas"}',station:{code:"CHD",nom:"Château-Douglas",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION CROASSIN-LE-DÉSERT (anciennement XXVILLAGEBH)
# Hub Croassin (=> Croassin M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3460}] Item.tag.display set value {Name:'{"text":"Croassin-le-Désert"}',station:{code:"CRO",nom:"Croassin-le-Désert",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION GLOUZY (anciennement XXVILLAGEBF)
# Hub Glouzy (=> Glouzy M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3480}] Item.tag.display set value {Name:'{"text":"Glouzy-en-Bédarve"}',station:{code:"GEB",nom:"Glouzy-en-Bédarve",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION ÉVERNAY-L'AUMÔNE (anciennement XXRERA3X1)
# Hub Évernay (=> Évernay RERA/M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3550}] Item.tag.display set value {Name:'{"text":"Évernay-l\'Aumône"}',station:{code:"EAU",nom:"Évernay-l'Aumône",deDu:"d'",alAu:"à ",prefixe:""}}

### STATION ABYESS THÛR (anciennement FAILLE TROP SUUS)
# Hub AbyessThur (=> AbyessThur M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3560}] Item.tag.display set value {Name:'{"text":"Abyess Thûr"}',station:{code:"ABY",nom:"Abyess Thûr",deDu:"d'",alAu:"à ",prefixe:""}}

### STATION ROC MAËG (anciennement GORGE WESHINTON)
# Hub RocMaeg (=> RocMaeg M11)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3570}] Item.tag.display set value {Name:'{"text":"Le Roc-Maëg"}',station:{code:"ROC",nom:"Roc-Maëg",deDu:"du ",alAu:"au ",prefixe:"Le "}}


### STATION CHAMBORION (anciennement TOILETTES DE TANTE ASTRUC)
# Hub Chamborion (=> Chamborion M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3600}] Item.tag.display set value {Name:'{"text":"Chamborion"}',station:{code:"CHA",nom:"Chamborion",deDu:"de ",alAu:"à ",prefixe:""}}

### STATION MONTHORNET (anciennement CAVERNE PROFONDE LAVE)
# Hub Monthornet (=> Monthornet M9)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3620}] Item.tag.display set value {Name:'{"text":"Monthornet"}',station:{code:"MTN",nom:"Monthornet",deDu:"de ",alAu:"à ",prefixe:""}}


### STATION BEURRIEUX-VALDEMONT (anciennement XXVILLAGEBT–BS)
# Hub BeurrieuxValdemont (=> BeurrieuxValdemont RERA)
data modify entity @e[type=item_frame,tag=tchStationPC,limit=1,scores={idStation=3700}] Item.tag.display set value {Name:'{"text":"Beurrieux–Valdemont"}',station:{code:"BEV",nom:"Beurrieux–Valdemont",deDu:"de ",alAu:"à ",prefixe:""}}
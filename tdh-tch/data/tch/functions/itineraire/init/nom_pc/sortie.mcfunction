# On est à la position d'un panneau (=d'une item frame TexteTemporaire)
# On enregistre le nom du PC
data modify block ~ ~ ~ Text1 set from entity @s Item.tag.display.station.code
data modify block ~ ~ ~ Text2 set from entity @s Item.tag.display.sortie.num
data modify block ~ ~ ~ Text3 set value '[{"text":"PC "},{"block":"~ ~ ~","nbt":"Text1","interpret":"true"},{"text":"–Sortie "},{"block":"~ ~ ~","nbt":"Text2","interpret":"true"}]'
data modify entity @s CustomName set from block ~ ~ ~ Text3
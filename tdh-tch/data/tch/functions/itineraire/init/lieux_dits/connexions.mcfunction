# On initialise ensuite le système de surface
# Pour simplifier la vie de tout le monde, on donne aux lieux-dits des ID station de type 99xxx

# 99001 - Donjon des Ténèbres (=> Procyon–Ténèbres S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99001}] Item.tag.tch set value {position:{x:-217,z:6158},connexions:[{station:2001,sortie:1,distance:112,temps:728}]}
# 99002 - Frambourg (=> FrambourgTernelieu S2/S3)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99002}] Item.tag.tch set value {position:{x:3107,z:5979},connexions:[{station:2100,sortie:2,distance:114,temps:741},{station:2100,sortie:3,distance:113,temps:734}]}
# 99003 - Ternelieu (=> FrambourgTernelieu S1/S3)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99003}] Item.tag.tch set value {position:{x:2883,z:5964},connexions:[{station:2100,sortie:1,distance:91,temps:591},{station:2100,sortie:3,distance:106,temps:689}]}
# 99004 - Sanor (=> GrenatRécif S1, Preskrik)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99004}] Item.tag.tch set value {position:{x:56,z:1365},connexions:[{station:1105,sortie:1,distance:36,temps:234},{station:99005,distance:87,temps:565}]}
# 99005 - Preskrik (=> GrenatRécif S2, Sanor, Récif)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99005}] Item.tag.tch set value {position:{x:8,z:1280},connexions:[{station:1105,sortie:2,distance:17,temps:110},{station:99004,distance:87,temps:565},{station:99006,distance:1,temps:1}]}
# 99006 - Récif (=> GrenatRécif S3, Preskrik)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99006}] Item.tag.tch set value {position:{x:-87,z:1141},connexions:[{station:1105,sortie:3,distance:1,temps:1},{station:99005,distance:1,temps:1}]}
# 99007 - Entrée du Chateau d'Illysia (=> LD Sortie du Chateau, Illysia S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99007}] Item.tag.tch set value {position:{x:-734,z:204},connexions:[{station:99022,distance:53,temps:344},{station:1420,sortie:1,distance:99,temps:643}]}
# 99008 - Temple de Chizân (=> Chizân S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99008}] Item.tag.tch set value {position:{x:-809,z:-924},connexions:[{station:1320,sortie:1,distance:31,temps:201}]}
# 99009 - Sanctuaire du Temps (=> Sablons S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99009}] Item.tag.tch set value {position:{x:-285,z:-428},connexions:[{station:1300,sortie:1,distance:100,temps:650}]}
# 99010 - Chateau d'Eremos (=> Sablons S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99010}] Item.tag.tch set value {position:{x:-502,z:-212},connexions:[{station:1300,sortie:1,distance:247,temps:1605}]}
# 99011 - Chateau d'Oklam (=> ???)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99011}] Item.tag.tch set value {position:{x:-421,z:-806},connexions:[]}
# 99012 - Chateau Onyx (=> AlwinOnyx S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99012}] Item.tag.tch set value {position:{x:361,z:-353},connexions:[{station:1360,sortie:1,distance:32,temps:208}]}
# 99013 - Citadelle de Gzor (=> Onyx S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99013}] Item.tag.tch set value {position:{x:594,z:-869},connexions:[{station:1350,sortie:1,distance:49,temps:318}]}
# 99014 - Crypte de Gzor (=> Onyx S2)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99014}] Item.tag.tch set value {position:{x:350,z:-701},connexions:[{station:1350,sortie:2,distance:117,temps:760}]}
# 99015 - Mines du Totem (=> Knosos S1, Int R102/R104)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99015}] Item.tag.tch set value {position:{x:527,z:216},connexions:[{station:1020,sortie:1,distance:23,temps:149},{station:99026,distance:173,temps:1124}]}
# 99016 - Camp minier principal (=> Kraken S1, LD CMP Intérieur)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99016}] Item.tag.tch set value {position:{x:544,z:420},connexions:[{station:1010,sortie:1,distance:24,temps:156},{station:99018,distance:47,temps:305}]}
# 99017 - Intersection R102/R103 (=> Kraken S1, LD Int R102/R104, LD CMS)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99017}] Item.tag.tch set value {connexions:[{station:1010,sortie:1,distance:59,temps:383},{station:99026,distance:220,temps:1430},{station:99027,distance:95,temps:617}]}
# 99018 - Intérieur camp minier principal (=> LD CMP)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99018}] Item.tag.tch set value {connexions:[{station:99016,distance:55,temps:357}]}
# 99019 - Début R124 (=> EvenisEclesta S1, INT R124/R125)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99019}] Item.tag.tch set value {connexions:[{station:1230,sortie:1,distance:22,temps:143},{station:99023,distance:75,temps:487}]}
# 99020 - Base nautique d'Evenis Eclesta (=> EvenisEclesta S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99020}] Item.tag.tch set value {position:{x:1621,z:416},connexions:[{station:1230,sortie:1,distance:10,temps:65}]}
# 99021 - Base nautique de Jëej (=> Jeej S2)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99021}] Item.tag.tch set value {position:{x:125,z:780},connexions:[{station:1155,sortie:2,distance:14,temps:91}]}
# 99022 - Sortie du Chateau d'Illysia (=> LD Entrée du Chateau, Illysia S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99022}] Item.tag.tch set value {connexions:[{station:99007,distance:53,temps:344},{station:1420,sortie:1,distance:107,temps:695}]}
# 99023 - Intersection R124/R125 (=> Début R124, Intersection R124/RouteVolcanEclesta)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99023}] Item.tag.tch set value {connexions:[{station:99019,distance:75,temps:487},{station:99024,distance:118,temps:767}]}
# 99024 - Intersection R124/RouteVolcanEclesta (=> Int R124/R125, LD Volcan Eclesta)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99024}] Item.tag.tch set value {connexions:[{station:99023,distance:118,temps:767},{station:99025,distance:59,temps:383}]}
# 99025 - Volcan Éclesta (=> Int R124/RouteVolcanEclesta)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99025}] Item.tag.tch set value {connexions:[{station:99024,distance:59,temps:383}]}
# 99026 - Intersection R102/R104 (=> LD Int R102/R103, LD MinesTotem)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99026}] Item.tag.tch set value {connexions:[{station:99017,distance:220,temps:1430},{station:99015,distance:173,temps:1124}]}
# 99027 - Camp minier secondaire (=> LD Int R102/R103, LD Int R102/R106, LD CMS Intérieur)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99027}] Item.tag.tch set value {connexions:[{station:99017,distance:95,temps:617},{station:99028,distance:68,temps:442},{station:99029,distance:83,temps:539}]}
# 99028 - Intersection R102/R106 (=> LD CMS, LD MaisonnetBelzebuth)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99028}] Item.tag.tch set value {connexions:[{station:99027,distance:68,temps:442},{station:99030,distance:160,temps:1040}]}
# 99029 - Camp minier secondaire Intérieur (=> LD CMS)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99029}] Item.tag.tch set value {connexions:[{station:99027,distance:83,temps:539}]}
# 99030 - Maisonnet de Belzébuth (=> LD Int R102/R106)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99030}] Item.tag.tch set value {position:{x:712,z:464},connexions:[{station:99028,distance:160,temps:1040}]}
# 99031 - Plaine des Barbares (=> Le Relais S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99031}] Item.tag.tch set value {position:{x:1280,z:5651},connexions:[{station:1950,sortie:1,distance:1,temps:1}]}
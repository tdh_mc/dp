# On initialise ensuite le système de surface
# Pour simplifier la vie de tout le monde, on donne aux lieux-dits des ID station de type 99xxx

# 99001 - Donjon des Ténèbres (=> Procyon–Ténèbres S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99001}] Item.tag.display set value {Name:'{"text":"Donjon des Ténèbres"}',station:{nom:"Donjon des Ténèbres",deDu:"du ",alAu:"au ",prefixe:""}}
# 99002 - Frambourg (=> FrambourgTernelieu S2/S3)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99002}] Item.tag.display set value {Name:'{"text":"Frambourg"}',station:{nom:"Frambourg",deDu:"de ",alAu:"à ",prefixe:""}}
# 99003 - Ternelieu (=> FrambourgTernelieu S1/S3)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99003}] Item.tag.display set value {Name:'{"text":"Ternelieu"}',station:{nom:"Ternelieu",deDu:"de ",alAu:"à ",prefixe:""}}
# 99004 - Sanor (=> GrenatRécif S1, Preskrik)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99004}] Item.tag.display set value {Name:'{"text":"Crique de Sanor"}',station:{nom:"Crique de Sanor",deDu:"de la ",alAu:"à la ",prefixe:""}}
# 99005 - Preskrik (=> GrenatRécif S2, Sanor, Récif)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99005}] Item.tag.display set value {Name:'{"text":"Lac de Preskrik"}',station:{nom:"Lac de Preskrik",deDu:"du ",alAu:"au ",prefixe:""}}
# 99006 - Récif (=> GrenatRécif S3, Preskrik)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99006}] Item.tag.display set value {Name:'{"text":"Récif de Grenat"}',station:{nom:"Récif de Grenat",deDu:"du ",alAu:"au ",prefixe:""}}
# 99007 - Entrée du Chateau d'Illysia (=> LD Sortie du Chateau, Illysia S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99007}] Item.tag.display set value {Name:'{"text":"Entrée du château d\'Illysia"}',station:{nom:"entrée du Château d'Illysia",deDu:"de l'",alAu:"à l'",prefixe:"l'"}}
# 99008 - Temple de Chizân (=> Chizân S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99008}] Item.tag.display set value {Name:'{"text":"Temple de Chizân"}',station:{nom:"Temple de Chizân",deDu:"du ",alAu:"au ",prefixe:""}}
# 99009 - Sanctuaire du Temps (=> Sablons S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99009}] Item.tag.display set value {Name:'{"text":"Sanctuaire du Temps"}',station:{nom:"Sanctuaire du Temps",deDu:"du ",alAu:"au ",prefixe:""}}
# 99010 - Chateau d'Eremos (=> Sablons S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99010}] Item.tag.display set value {Name:'{"text":"Château d\'Erêmos"}',station:{nom:"Château d'Erêmos",deDu:"du ",alAu:"au ",prefixe:""}}
# 99011 - Chateau d'Oklam (=> ???)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99011}] Item.tag.display set value {Name:'{"text":"Château d\'Oklam"}',station:{nom:"Château d'Oklam",deDu:"du ",alAu:"au ",prefixe:""}}
# 99012 - Chateau Onyx (=> AlwinOnyx S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99012}] Item.tag.display set value {Name:'{"text":"Château Onyx"}',station:{nom:"Château Onyx",deDu:"du ",alAu:"au ",prefixe:""}}
# 99013 - Citadelle de Gzor (=> Onyx S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99013}] Item.tag.display set value {Name:'{"text":"Citadelle de Gzor"}',station:{nom:"Citadelle de Gzor",deDu:"de la ",alAu:"à la ",prefixe:""}}
# 99014 - Crypte de Gzor (=> Onyx S2)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99014}] Item.tag.display set value {Name:'{"text":"Crypte de Gzor"}',station:{nom:"Crypte de Gzor",deDu:"de la ",alAu:"à la ",prefixe:""}}
# 99015 - Mines du Totem (=> Knosos S1, Int R102/R104)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99015}] Item.tag.display set value {Name:'{"text":"Mines du Totem"}',station:{nom:"Mines du Totem",deDu:"des ",alAu:"aux ",prefixe:""}}
# 99016 - Camp minier principal (=> Kraken S1, LD CMP Intérieur)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99016}] Item.tag.display set value {Name:'{"text":"Camp minier principal"}',station:{nom:"Camp minier principal",deDu:"du ",alAu:"au ",prefixe:""}}
# 99017 - Intersection R102/R103 (=> Kraken S1, LD Int R102/R104, LD CMS)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99017}] Item.tag.display set value {Name:'{"text":"Intersection des routes 102 et 103"}'}
# 99018 - Intérieur camp minier principal (=> LD CMP)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99018}] Item.tag.display set value {Name:'{"text":"Camp minier principal – Intérieur"}'}
# 99019 - Début R124 (=> EvenisEclesta S1, INT R124/R125)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99019}] Item.tag.display set value {Name:'{"text":"Début de la route 124"}'}
# 99020 - Base nautique d'Evenis Eclesta (=> EvenisEclesta S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99020}] Item.tag.display set value {Name:'{"text":"Base nautique d\'Évenis–Éclesta"}',station:{nom:"Base nautique d'Évenis–Éclesta",deDu:"de la ",alAu:"à la ",prefixe:""}}
# 99021 - Base nautique de Jëej (=> Jeej S2)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99021}] Item.tag.display set value {Name:'{"text":"Base nautique de Jëej"}',station:{nom:"Base nautique de Jëej",deDu:"de la ",alAu:"à la ",prefixe:""}}
# 99022 - Sortie du Chateau d'Illysia (=> LD Entrée du Chateau, Illysia S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99022}] Item.tag.display set value {Name:'{"text":"Sortie du château d\'Illysia"}',station:{nom:"sortie du Château d'Illysia",deDu:"de la ",alAu:"à la ",prefixe:""}}
# 99023 - Intersection R124/R125 (=> Début R124, Intersection R124/RouteVolcanEclesta)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99023}] Item.tag.display set value {Name:'{"text":"Intersection des routes 124 et 125"}'}
# 99024 - Intersection R124/RouteVolcanEclesta (=> Int R124/R125, LD Volcan Eclesta)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99024}] Item.tag.display set value {Name:'{"text":"Intersection des routes 124 et du volcan d\'Éclesta"}'}
# 99025 - Volcan Éclesta (=> Int R124/RouteVolcanEclesta)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99025}] Item.tag.display set value {Name:'{"text":"Début de la route 124"}'}
# 99026 - Intersection R102/R104 (=> LD Int R102/R103, LD MinesTotem)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99026}] Item.tag.display set value {Name:'{"text":"Intersection des routes 102 et 104"}'}
# 99027 - Camp minier secondaire (=> LD Int R102/R103, LD Int R102/R106, LD CMS Intérieur)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99027}] Item.tag.display set value {Name:'{"text":"Camp minier secondaire"}',station:{nom:"Camp minier secondaire",deDu:"du ",alAu:"au ",prefixe:""}}
# 99028 - Intersection R102/R106 (=> LD CMS, LD MaisonnetBelzebuth)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99028}] Item.tag.display set value {Name:'{"text":"Intersection des routes 102 et 106"}'}
# 99029 - Camp minier secondaire Intérieur (=> LD CMS)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99029}] Item.tag.display set value {Name:'{"text":"Camp minier secondaire – Intérieur"}'}
# 99030 - Maisonnet de Belzébuth (=> LD Int R102/R106)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99030}] Item.tag.display set value {Name:'{"text":"Maisonnet de Belzébuth"}',station:{nom:"Maisonnet de Belzébuth",deDu:"du ",alAu:"au ",prefixe:""}}
# 99031 - Plaine des Barbares (=> Le Relais S1)
data modify entity @e[type=item_frame,tag=tchLieuDitPC,limit=1,scores={idStation=99031}] Item.tag.display set value {Name:'{"text":"Plaine des Barbares"}',station:{nom:"Plaine des Barbares",deDu:"de la ",alAu:"à la ",prefixe:""}}
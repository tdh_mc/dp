# On exécute cette fonction en tant qu'un PC de station du système d'itinéraire,
# pour vérifier si on contient une liaison intérieure

# Les caractéristiques d'une liaison intérieure sont les suivantes :
# - ID de station spécifié (c'est ce qu'on cherche)
# - Pas d'ID de ligne
# - Pas d'ID de sortie
scoreboard players reset #tchItineraire idStation
scoreboard players set #tchItineraire idLine 0
scoreboard players set #tchItineraire idSortie 0

# On cherche cette connexion parmi celles de ce hub
function tch:itineraire/noeud/trouver_connexion

# Si on a trouvé la connexion, on enregistre la valeur de retour (son ID de station)
execute if entity @s[tag=ConnexionTrouvee] run scoreboard players operation #tch idStation = #tchItineraire prochaineStation

# On reset notre tag
tag @s remove ConnexionTrouvee
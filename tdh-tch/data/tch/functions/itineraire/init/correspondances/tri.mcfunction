# On calcule le minimum des ID de ligne
scoreboard players set #tch idLine 99999999
scoreboard players operation #tch idLine < @e[type=item_frame,tag=tempOurStation] idLine

# On ajoute la ligne ayant le plus petit ID, et on retire son tag
execute as @e[type=item_frame,tag=tempOurStation] if score @s idLine = #tch idLine run function tch:itineraire/init/correspondances/tri/ajout

# S'il reste des quais ayant le tag, on relance la fonction
execute if entity @e[type=item_frame,tag=tempOurStation] run function tch:itineraire/init/correspondances/tri
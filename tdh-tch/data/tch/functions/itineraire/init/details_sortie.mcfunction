# Cette fonction est exécutée par un tchSortiePC pour initialiser le texte des détails, à sa position
# (par exemple, au Hameau M2, on récupèrera les détails du Hameau ("Hameau", "Le", "du ", "au ")

# On tag notre hub station
function tch:tag/station_pc

# On copie les données de notre hub
data modify entity @s Item.tag.display.station set from entity @e[type=item_frame,tag=ourStationPC,distance=..20,limit=1] Item.tag.display.station

# On supprime le tag temporaire
tag @e[type=item_frame,tag=ourStationPC] remove ourStationPC
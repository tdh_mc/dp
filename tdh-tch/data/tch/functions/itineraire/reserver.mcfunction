# On utilise cette fonction pour réserver le calcul d'itinéraire
# Les variables scoreboard suivantes servent d'argument :

# SCORE #tchItineraire stationDepart = <idStation de départ>
# SCORE #tchItineraire stationArrivee = <idStation d'arrivée>


# On enregistre ces données dans le storage
data modify storage tch:itineraire en_attente append value {id:-1,depart:0,arrivee:0}
execute store result storage tch:itineraire en_attente[-1].depart int 1 run scoreboard players get #tchItineraire stationDepart
execute store result storage tch:itineraire en_attente[-1].arrivee int 1 run scoreboard players get #tchItineraire stationArrivee
# On enregistre également l'ID de dialogue de la personne qui exécute, s'il existe, pour pouvoir transmettre le résultat
execute if score @s dialogueID matches 0.. store result storage tch:itineraire en_attente[-1].id int 1 run scoreboard players get @s dialogueID
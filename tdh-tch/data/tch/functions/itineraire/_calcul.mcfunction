# On utilise cette fonction pour calculer un itinéraire
# Les variables scoreboard suivantes servent d'argument :

# SCORE #tchItineraire stationDepart = <idStation de départ>
# SCORE #tchItineraire stationArrivee = <idStation d'arrivée>

# Cette fonction est "async" dans le sens où on n'a pas forcément de résultat tout de suite :
# on enregistre simplement stationDepart et stationArrivee dans un storage si un calcul est déjà en cours,
# et on fera le calcul lorsque le précédent sera terminé

# Si on est déjà en train de calculer, on ajoute notre requête au storage
execute if score #tchItineraire status matches 1.. run function tch:itineraire/reserver

# Sinon, on lance le calcul
execute unless score #tchItineraire status matches 1.. run function tch:itineraire/calcul
# Cette fonction initialise le NBT de toutes les item frames "tchItinNode"
# Il y a un PC Quai par quai (double sens) de métro (par ex, une station où passent 2 lignes de métro aura 2 PC Quai)

# On initialise d'abord les scores nécessaires
# Mode vaut 1 pour une recherche d'itinéraire le + rapide,
#		=   2 pour une recherche d'itinéraire le + court,
#		=	3 pour une recherche d'itinéraire le + direct (TODO)
#scoreboard objectives add mode dummy "Mode de calcul"
# mode est désormais un objectif de tdh-core
scoreboard players set #tchItineraire mode 1

scoreboard objectives add idStation dummy "TCH - Identifiant station"
scoreboard objectives add idLine dummy "TCH - Identifiant ligne"
scoreboard objectives add idSortie dummy "TCH - Identifiant sortie"

scoreboard objectives add stationDepart dummy "Station de départ"
scoreboard objectives add stationArrivee dummy "Station d'arrivée"

scoreboard objectives add prochaineStation dummy "Prochaine station"
scoreboard objectives add prochaineLigne dummy "Prochaine ligne"
scoreboard objectives add prochaineSortie dummy "Prochaine sortie"

scoreboard objectives add cumulTemps dummy "Temps total de trajet"
scoreboard objectives add cumulDistance dummy "Distance totale de trajet"


# On initialise ensuite le NBT de chaque PC Quai

# Les données TCH s'inscrivent ainsi dans la structure du NBT de chaque PC Quai :
# {
# 	Item: {
# 		tag: {
# 			display: {
#				line:{
#					nom:'<Numéro de la ligne>',
#					vehicule:{nom:"métro",deDu:"du ",alAu:"au ",prefixe:"le "}
#				},
#				corresp:'<Éventuelles lignes en correspondance>', # (ex: "M1, M3 et M4")
#				station:{
#					nom:"Hameau",
#					prefixe:"Le ",
#					deDu:"du ",
#					alAu:"au "
#				}
# 			},
# 			tch: {
#				position: { x:<int>, z:<int> },	# (la position de la station ou du lieu représenté)
#												# (n'est pas obligatoirement présent sur les tchQuaiPC)
# 				connexions: [
# 					{
# 						station: <idStation>,	# (si c'est une connexion inter-station, ex: Knosos—Ygriak)
# 						line: <idLine>,			# (si c'est une connexion intra-station, ex: Jeej M3 -> Jeej M4)
#						sortie: <idSortie>,		# (si c'est une connexion station-extérieur)
#						# (si aucun des champs précédents n'existe, c'est une correspondance entre un quai et le "hub" de la station)
#						# (le "hub" représente l'endroit où tous les flux en provenance/à destination de toutes les lignes de cette
#						#  station se rejoignent ; il s'agit très souvent de la salle d'échange, mais pas tjrs (exemple: au Hameau))
# 						temps: <durée du trajet, en minutes TDH>
# 						distance: <longueur du trajet, en mètres>
# 					},
# 					<autant d'éléments dans "connexions" que de quais pouvant être atteints depuis ce quai>
# 				],
#				connexion: {	#Terme temporaire présent seulement lors d'un calcul d'itinéraire
#					station: <idStation>, line: <idLine>, temps: <minutes>, distance: <mètres>
#				}
#				itineraire: {
#					cumulDistance: <int>,		# (représente le total du trajet en mètres)
#					cumulTemps: <int>,	# (représente le total du trajet en minutes)
#					chemin: [
#						{sortie: <idSortie>},						# (l'étape finale du trajet)
#						{station: <idStation>, line: <idLine>},		# (une étape du trajet)
#						<répéter pour chaque étape du trajet, les dernières ajoutées sont les plus en haut>
#					]
#				}
#			}
#		}
#	}

# On initialise d'abord toutes les lignes (tchPC) avec les bons détails de ligne
function tch:itineraire/init/lignes

# On procède station par station pour initialiser les quais
function tch:itineraire/init/stations/noms
function tch:itineraire/init/stations/connexions
function tch:itineraire/init/stations/give

# On initialise ensuite le système de surface
function tch:itineraire/init/lieux_dits/noms
function tch:itineraire/init/lieux_dits/connexions


# On initialise tous les autres scores laissés vides
execute as @e[type=item_frame,tag=tchItinNode] unless score @s idStation matches 1.. unless score @s idStation matches ..-1 run scoreboard players set @s idStation 0
execute as @e[type=item_frame,tag=tchItinNode] unless score @s idLine matches 1.. unless score @s idLine matches ..-1 run scoreboard players set @s idLine 0
execute as @e[type=item_frame,tag=tchItinNode] unless score @s idSortie matches 1.. unless score @s idSortie matches ..-1 run scoreboard players set @s idSortie 0


# Pour chaque PC Quai, on génère le texte de correspondance
# (dans 1 tick pour éviter de dépasser le maximum de commandes autorisées mdr)
schedule function tch:itineraire/init/textes 1t
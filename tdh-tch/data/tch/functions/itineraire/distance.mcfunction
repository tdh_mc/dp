# On utilise cette fonction pour calculer une distance à vol d'oiseau et une direction
# Les variables scoreboard suivantes servent d'argument :

# SCORE #tchItineraire posX / posZ = <position de départ>
# SCORE #tchItineraire deltaX / deltaZ = <position d'arrivée>

# On stocke dans deltaX/deltaZ la différence de position selon cet axe
scoreboard players operation #tchItineraire deltaX -= #tchItineraire posX
scoreboard players operation #tchItineraire deltaZ -= #tchItineraire posZ
# On calcule la direction à partir de la différence de position
scoreboard players operation #tchItineraire posX = #tchItineraire deltaX
scoreboard players operation #tchItineraire posZ = #tchItineraire deltaZ
function tch:itineraire/distance/direction
# On met les deux au carré, puis on les additionne
scoreboard players operation #tchItineraire deltaX *= #tchItineraire deltaX
scoreboard players operation #tchItineraire deltaZ *= #tchItineraire deltaZ
scoreboard players operation #sqrt temp = #tchItineraire deltaX
scoreboard players operation #sqrt temp += #tchItineraire deltaZ
# Comme Minecraft de chien ne permet pas de calculer une racine carrée, on a une fonction pour ça
function tdh:math/sqrt
# On formate ensuite la distance pour l'affichage
scoreboard players operation #store distance = #sqrt temp
function tdh:store/distance_format
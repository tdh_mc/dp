# On utilise cette fonction pour calculer une distance à vol d'oiseau et une direction
# Les variables scoreboard suivantes servent d'argument :

# La position du joueur le plus proche de l'exécution de cette commande sert de position de départ
# SCORE #tchItineraire stationArrivee = <position d'arrivée>

# On nettoie le storage associé
data remove storage tch:itineraire distance

# On tente de récupérer la station correspondant à notre lieu d'arrivée
# On tag la station correspondante
execute as @e[type=item_frame,tag=tchItinNode,tag=!tchQuaiPC,tag=!tchSortiePC] if score @s idStation = #tchItineraire stationArrivee run tag @s add NotreStation

# Si on a trouvé la station, on exécute la suite du calcul
execute as @e[type=item_frame,tag=NotreStation,limit=1] run function tch:itineraire/distance/sauvegarder_destination

# On supprime les tags temporaires
tag @e[type=item_frame,tag=NotreStation] remove NotreStation
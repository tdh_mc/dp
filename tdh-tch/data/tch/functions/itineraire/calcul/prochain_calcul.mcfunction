# On enregistre les variables depuis le storage
execute store result score #tchItineraire stationDepart run data get storage tch:itineraire en_attente[0].depart
execute store result score #tchItineraire stationArrivee run data get storage tch:itineraire en_attente[0].arrivee
execute if data storage tch:itineraire en_attente[0].id store result score #tchItineraire dialogueID run data get storage tch:itineraire en_attente[0].id

# On supprime le champ du storage (puisqu'on va réaliser le calcul)
data remove storage tch:itineraire en_attente[0]

# On lance le calcul
function tch:itineraire/calcul
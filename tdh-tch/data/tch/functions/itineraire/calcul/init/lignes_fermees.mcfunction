# Pour chaque PC, si la ligne ne roule pas (= hors service ou fin de service),
# on "ferme" les stations ayant cet idLine dans le système d'itinéraire

execute as @e[type=item_frame,tag=tchPC,tag=HorsService] run function tch:itineraire/calcul/init/ligne_fermee
execute as @e[type=item_frame,tag=tchPC,tag=FinDeService] run function tch:itineraire/calcul/init/ligne_fermee
# On utilise cette fonction pour initialiser chaque noeud de départ (toutes les données à 0)

# On initialise d'abord les données NBT
# On réinitialise le chemin
data modify entity @s Item.tag.tch.itineraire set value {cumulDistance:0,cumulTemps:0,chemin:[],chemin_debug:[]}
# On enregistre l'ID de station comme 1ère étape du trajet (ligne = 0, car on part du hub de la station)
#execute store result entity @s Item.tag.tch.itineraire.chemin[0].station int 1 run scoreboard players get @s idStation

# On initialise ensuite les scores
scoreboard players set @s cumulTemps 0
scoreboard players set @s cumulDistance 0

# On procède ensuite comme pour un noeud classique, en actualisant tous les noeuds voisins
function tch:itineraire/calcul/voisins
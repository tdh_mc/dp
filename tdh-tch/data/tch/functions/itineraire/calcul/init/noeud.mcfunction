# On utilise cette fonction pour initialiser un noeud du graphe

# On supprime les divers tags temporaires qu'on pourrait posséder
tag @s remove dejaCalcule
tag @s remove depart
tag @s remove arrivee
tag @s remove possible
tag @s remove pasPossible
tag @s remove nouvelleEntree

# On initialise les scores (-1 signifie que ca n'a jamais été calculé)
scoreboard players set @s cumulTemps -1
scoreboard players set @s cumulDistance -1

# On crée l'arborescence des données TCH
data modify entity @s Item.tag.tch.itineraire set value {cumulDistance:0,cumulTemps:0,chemin:[],chemin_debug:[]}
data modify entity @s Item.tag.tch.connexion set value {}
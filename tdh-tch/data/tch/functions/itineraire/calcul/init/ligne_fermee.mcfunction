# On exécute cette fonction en tant qu'un PC fermé (HS/FDS)
# pour fermer toutes les stations correspondantes

# On enregistre l'idLine qu'on recherche
scoreboard players operation #itineraire idLine = @s idLine
scoreboard players operation #itineraire idLineMax = @s idLineMax

# On vérifie pour chaque tchItinNode si la valeur est celle-ci
# Si c'est le cas, on "ferme" le node (= on enregistre le fait qu'on ne peut pas l'utiliser)
execute as @e[type=item_frame,tag=tchItinNode,scores={idLine=1..}] if score @s idLine >= #itineraire idLine if score @s idLine <= #itineraire idLineMax run tag @s add pasPossible

# On réinitialise les variables temporaires
scoreboard players reset #itineraire idLine
scoreboard players reset #itineraire idLineMax
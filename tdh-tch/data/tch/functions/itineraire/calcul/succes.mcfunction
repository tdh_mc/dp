# Message de debug
tellraw @a[tag=ItinDebugLog] [{"text":"Itinéraire trouvé!","color":"green"}]

# On a trouvé un itinéraire de notre station de départ à notre station d'arrivée
# On enregistre le résultat
data modify storage tch:itineraire dernier_resultat set value {succes:1b}
data modify storage tch:itineraire dernier_resultat.itineraire set from entity @e[type=item_frame,tag=arrivee,tag=dejaCalcule,limit=1] Item.tag.tch.itineraire
data modify storage tch:itineraire dernier_resultat.depart set from entity @e[type=item_frame,tag=depart,limit=1] Item.tag.display.station
data modify storage tch:itineraire dernier_resultat.arrivee set from entity @e[type=item_frame,tag=arrivee,limit=1] Item.tag.display.station

# On enregistre le fait que le calcul est terminé
scoreboard players set #tchItineraire status 2

# On attend 0.5s avant de faire un nouveau calcul (ou de libérer la place s'il n'y a pas de calcul en attente)
schedule function tch:itineraire/calcul/fin 10t
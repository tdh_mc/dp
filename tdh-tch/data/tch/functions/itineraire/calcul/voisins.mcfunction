# On utilise cette fonction pour initialiser chacun des noeuds indiqués dans nos "connexions"
# La fonction est exécutée par le noeud actuel (item frame avec le tag tchItinNode) à sa propre position

tellraw @a[tag=ItinDetailLog] [{"text":"Nœud sélectionné: ","color":"gray"},{"entity":"@s","nbt":"Item.tag.display.Name","interpret":"true"},{"entity":"@e[type=item_frame,tag=prochainNoeudTemp,limit=1]","nbt":"Item.tag.display.line.nom","interpret":"true"}]

# Si on nous avait donné le tag "possible", on le supprime (puisqu'on a sélectionné cette station)
tag @s[tag=possible] remove possible

# On initialise toutes les stations adjacentes (10 connexions maximum)
execute if data entity @s Item.tag.tch.connexions[0] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[0]
execute if data entity @s Item.tag.tch.connexions[0] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[1] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[1]
execute if data entity @s Item.tag.tch.connexions[1] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[2] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[2]
execute if data entity @s Item.tag.tch.connexions[2] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[3] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[3]
execute if data entity @s Item.tag.tch.connexions[3] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[4] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[4]
execute if data entity @s Item.tag.tch.connexions[4] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[5] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[5]
execute if data entity @s Item.tag.tch.connexions[5] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[6] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[6]
execute if data entity @s Item.tag.tch.connexions[6] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[7] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[7]
execute if data entity @s Item.tag.tch.connexions[7] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[8] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[8]
execute if data entity @s Item.tag.tch.connexions[8] run function tch:itineraire/calcul/voisins/trouver

execute if data entity @s Item.tag.tch.connexions[9] run data modify entity @s Item.tag.tch.connexion set from entity @s Item.tag.tch.connexions[9]
execute if data entity @s Item.tag.tch.connexions[9] run function tch:itineraire/calcul/voisins/trouver


# On se donne le tag "dejaCalcule" pour enregistrer le fait qu'on a calculé cette station
tag @s add dejaCalcule
# On utilise cette fonction pour initialiser le noeud suivant à partir des données de notre noeud actuel
# La fonction est exécutée par le noeud actuel (item frame avec le tag tchItinNode) à sa propre position

# Le noeud exécutant cette fonction a stocké les données sur la connexion actuelle dans Item.tag.tch.connexion
# On enregistre dans prochaineStation et prochaineLigne les infos sur le prochain noeud à atteindre
execute store result score #itineraire prochaineStation run data get entity @s Item.tag.tch.connexion.station
execute store result score #itineraire prochaineLigne run data get entity @s Item.tag.tch.connexion.line
execute store result score #itineraire prochaineSortie run data get entity @s Item.tag.tch.connexion.sortie
# On enregistre également temporairement nos ID existants
execute if score @s idStation matches 1.. run scoreboard players operation #itineraire idStation = @s idStation
execute if score @s idSortie matches 1.. run scoreboard players operation #itineraire idSortie = @s idSortie
#execute if score @s idLine matches 1.. run scoreboard players operation #itineraire idLine = @s idLine
# Pour l'ID line on procède différemment pour tenir compte de la différence entre les directions d'une même ligne : on copie dans l'itinéraire la dernière ligne empruntée
execute store result score #itineraire idLine run data get entity @s Item.tag.tch.itineraire.chemin[0].line.id

# Si la distance vaut -1, c'est que la connexion est désactivée, et on passe prochaineStation à -1 pour désactiver tous les checks suivants
execute as @s[nbt={Item:{tag:{tch:{connexion:{distance:-1}}}}}] run scoreboard players set #itineraire prochaineStation -1

# Si une des valeurs existe, on donne un tag au prochain noeud
# S'il y a une prochaine station ET une prochaine sortie, on cherche un noeud ayant cet ID station et cet ID sortie (entrée dans le réseau TCH)
execute if score #itineraire prochaineStation matches 1.. if score #itineraire prochaineLigne matches 0 if score #itineraire prochaineSortie matches 1.. at @s as @e[type=item_frame,tag=tchSortiePC,tag=!pasPossible,distance=..18] if score @s idStation = #itineraire prochaineStation if score @s idSortie = #itineraire prochaineSortie run tag @s add prochainNoeudTemp
# S'il y a une prochaine station mais pas d'ID de ligne, on cherche un noeud ayant cet ID station et pas d'ID de ligne non plus (marche à pied, soit vers un lieu dit soit vers un hub)
execute if score #itineraire prochaineStation matches 1.. if score #itineraire prochaineLigne matches 0 if score #itineraire prochaineSortie matches 0 at @s as @e[type=item_frame,tag=tchItinNode,tag=!pasPossible,distance=..18] if score @s idStation = #itineraire prochaineStation unless score @s idLine matches 1.. unless score @s idSortie matches 1.. run tag @s add prochainNoeudTemp
# S'il y a une prochaine station et un ID de ligne on cherche un noeud ayant cet ID station et un idLine/idLineMax contenant notre ID de ligne
execute if score #itineraire prochaineStation matches 1.. if score #itineraire prochaineLigne matches 1.. if score #itineraire prochaineSortie matches 0 at @s as @e[type=item_frame,tag=tchItinNode,tag=!pasPossible,distance=..18] if score @s idStation = #itineraire prochaineStation if score @s idLine <= #itineraire prochaineLigne if score @s idLineMax >= #itineraire prochaineLigne run tag @s add prochainNoeudTemp
# S'il y a une prochaine ligne, on cherche un noeud ayant le même ID station que nous, et cet ID de ligne
execute if score #itineraire prochaineStation matches 0 if score #itineraire prochaineLigne matches 1.. if score #itineraire prochaineSortie matches 0 at @s as @e[type=item_frame,tag=tchItinNode,tag=!pasPossible,distance=..18] if score @s idStation = #itineraire idStation if score @s idLine <= #itineraire prochaineLigne if score @s idLineMax >= #itineraire prochaineLigne run tag @s add prochainNoeudTemp
# S'il y a une prochaine sortie, on cherche un noeud ayant le même ID station que nous, et cet ID de sortie
execute if score #itineraire prochaineStation matches 0 if score #itineraire prochaineLigne matches 0 if score #itineraire prochaineSortie matches 1.. at @s as @e[type=item_frame,tag=tchSortiePC,tag=!pasPossible,distance=..18] if score @s idStation = #itineraire idStation if score @s idSortie = #itineraire prochaineSortie run tag @s add prochainNoeudTemp
# Si aucun champ n'est renseigné, on cherche un noeud ayant le même ID station que nous, et c'est tout (le hub)
execute if score #itineraire prochaineStation matches 0 if score #itineraire prochaineLigne matches 0 if score #itineraire prochaineSortie matches 0 at @s as @e[type=item_frame,tag=tchStationPC,tag=!pasPossible,distance=..18] if score @s idStation = #itineraire idStation run tag @s add prochainNoeudTemp

# Message de debug
execute as @e[type=item_frame,tag=prochainNoeudTemp,tag=!dejaCalcule] run tellraw @a[tag=ItinDetailLog] [{"text":"Ajout du nœud ","color":"dark_aqua"},{"entity":"@s","nbt":"Item.tag.display.Name","interpret":"true"},{"text":" "},{"entity":"@s","nbt":"Item.tag.display.line.nom","interpret":"true"},{"text":" aux nœuds possibles."}]
execute as @e[type=item_frame,tag=prochainNoeudTemp,tag=dejaCalcule] run tellraw @a[tag=ItinDetailLog] [{"text":"Ajout avorté du nœud ","color":"dark_red"},{"entity":"@s","nbt":"Item.tag.display.Name","interpret":"true"},{"text":" "},{"entity":"@s","nbt":"Item.tag.display.line.nom","interpret":"true"},{"text":" aux nœuds possibles."}]
execute unless entity @e[type=item_frame,tag=prochainNoeudTemp] run tellraw @a[tag=ItinDetailLog] [{"text":"Ajout échoué du nœud aux nœuds possibles. (ligne fermée ?)"}]

# Si le prochain noeud a déjà été calculé, on le nexte
tag @e[type=item_frame,tag=prochainNoeudTemp,tag=dejaCalcule] remove prochainNoeudTemp

# Si on a trouvé un prochain noeud, on exécute la sous-fonction
execute at @e[type=item_frame,tag=prochainNoeudTemp] run function tch:itineraire/calcul/voisins/update

# On supprime le tag temporaire du prochain noeud, et on le remplace par le tag définitif
tag @e[type=item_frame,tag=prochainNoeudTemp] add possible
tag @e[type=item_frame,tag=prochainNoeudTemp] remove prochainNoeudTemp

# On réinitialise les scores temporaires
scoreboard players reset #itineraire prochaineStation
scoreboard players reset #itineraire prochaineLigne
scoreboard players reset #itineraire prochaineSortie
scoreboard players reset #itineraire idLine
scoreboard players reset #itineraire idSortie
scoreboard players reset #itineraire idStation
# On utilise cette fonction pour initialiser le noeud suivant à partir des données de notre noeud actuel
# La fonction est exécutée par le noeud actuel (item frame avec le tag tchItinNode) à la position du prochain noeud (auquel on a donné le tag temporaire prochainNoeudTemp)

tellraw @a[tag=ItinDetailLog] [{"text":"Mise à jour du nœud ","color":"gray"},{"entity":"@e[type=item_frame,tag=prochainNoeudTemp,limit=1]","nbt":"Item.tag.display.Name","interpret":"true"},{"entity":"@e[type=item_frame,tag=prochainNoeudTemp,limit=1]","nbt":"Item.tag.display.line.nom","interpret":"true"},{"text":" à partir du nœud "},{"entity":"@s","nbt":"Item.tag.display.Name","interpret":"true"},{"entity":"@s","nbt":"Item.tag.display.line.nom","interpret":"true"}]

# On se donne un tag nouvelleEntree si :
# - on a changé de ligne (on est un tchQuaiPC et on a changé d'idLine entre @s et la prochaine station)
#   (mais PAS si on est restés dans la même station)
execute as @s[tag=tchQuaiPC] unless score #itineraire prochaineLigne = #itineraire idLine unless score #itineraire prochaineStation = #itineraire idStation run tag @s add nouvelleEntree
# - mais on le retire si on est un RER et qu'on change pour une direction distante de moins de 10
#   (voir explications détaillées dans la sous-fonction)
execute as @s[tag=tchQuaiPC,tag=nouvelleEntree] if score #itineraire idLine matches 2000..3999 if score #itineraire prochaineLigne matches 2000..3999 run function tch:itineraire/calcul/voisins/update/fausse_correspondance_rer
# - on le retire également si on change pour une direction qu'on a prise dans la même station
execute as @s[tag=tchQuaiPC,tag=nouvelleEntree] run function tch:itineraire/calcul/voisins/update/fausse_correspondance
#execute as @s[tag=tchQuaiPC] unless score @s idLine = @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] idLine run tag @s add nouvelleEntree
# - on est passés par un hub (on est un tchStationPC)
execute as @s[tag=tchStationPC] run tag @s add nouvelleEntree



# On calcule d'abord les scores pour savoir s'il est nécessaire de toucher au NBT
# On calcule la durée totale du trajet en passant par le chemin actuel (temp)
execute store result score @s temp run data get entity @s Item.tag.tch.connexion.temps
scoreboard players operation @s temp += @s cumulTemps
# Si on a le tag nouvelleEntree, on ajoute 20 minutes à ce temps total pour prendre en compte le battement de correspondance (et éviter de suggérer une correspondance si on a plus direct)
scoreboard players add @s[tag=nouvelleEntree] temp 1000

# On calcule de la meme manière la distance totale (temp2)
execute store result score @s temp2 run data get entity @s Item.tag.tch.connexion.distance
scoreboard players operation @s temp2 += @s cumulDistance
# Si on a le tag nouvelleEntree, on ajoute 10 mètres à cette distance totale pour prendre en compte le battement de correspondance (et éviter de suggérer une correspondance si on a plus direct)
scoreboard players add @s[tag=nouvelleEntree] temp2 10

# Ensuite, si le nouveau temps (temp) est plus court que l'ancien, ou que l'ancien était égal à -1 (=encore jamais calculé),
# et qu'on calcule en mode "itinéraire le plus rapide", on met à jour son itinéraire
execute if score #tchItineraire mode matches 1 if score @e[type=item_frame,tag=prochainNoeudTemp,limit=1] cumulTemps matches -1 run function tch:itineraire/calcul/voisins/itineraire
execute if score #tchItineraire mode matches 1 if score @e[type=item_frame,tag=prochainNoeudTemp,limit=1] cumulTemps > @s temp run function tch:itineraire/calcul/voisins/itineraire

# Ensuite, si la nouvelle distance (temp2) est plus courte que l'ancienne, ou que l'ancienne était égale à -1,
# et qu'on calcule en mode "itinéraire le plus court", on met à jour son itinéraire
execute if score #tchItineraire mode matches 2 if score @e[type=item_frame,tag=prochainNoeudTemp,limit=1] cumulDistance matches -1 run function tch:itineraire/calcul/voisins/itineraire
execute if score #tchItineraire mode matches 2 if score @e[type=item_frame,tag=prochainNoeudTemp,limit=1] cumulDistance > @s temp2 run function tch:itineraire/calcul/voisins/itineraire


# On réinitialise les variables temporaires
scoreboard players reset @s temp
scoreboard players reset @s temp2

# On supprime le tag temporaire
tag @s remove nouvelleEntree
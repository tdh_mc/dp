# On utilise cette fonction pour initialiser les données de l'itinéraire le + court du noeud suivant
# On a déjà calculé que notre itinéraire actuel était le meilleur possible, donc on assigne juste les données

# La fonction est exécutée par le noeud actuel (item frame avec le tag tchItinNode) à la position du prochain noeud (auquel on a donné le tag temporaire prochainNoeudTemp)

# On copie l'itinéraire actuel dans l'itinéraire du prochain noeud
data modify entity @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] Item.tag.tch.itineraire.chemin set from entity @s Item.tag.tch.itineraire.chemin
data modify entity @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] Item.tag.tch.itineraire.chemin_debug set from entity @s Item.tag.tch.itineraire.chemin_debug

# Si on a pas le tag nouvelleEntree, on modifie simplement l'entrée précédente
execute as @s[tag=!nouvelleEntree] as @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] run function tch:itineraire/calcul/voisins/itineraire/update_noeud

# Si on a changé de ligne ou qu'on a emprunté une sortie, on tente d'ajouter un nouveau noeud
execute as @s[tag=nouvelleEntree] as @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] run function tch:itineraire/calcul/voisins/itineraire/ajout_noeud

# Dans tous les cas, on ajoute le noeud au chemin_debug
execute as @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] run function tch:itineraire/calcul/voisins/itineraire/ajout_noeud_debug

# Dans tous les cas, on incrémente le temps total (la valeur a déjà été calculée et est stockée dans @s > temp)
# et la distance totale (la valeur a déjà été calculée et est stockée dans @s > temp2)
# On stocke la valeur à la fois dans notre score et dans notre NBT
execute store result entity @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] Item.tag.tch.itineraire.cumulTemps int 1 run scoreboard players operation @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] cumulTemps = @s temp
execute store result entity @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] Item.tag.tch.itineraire.cumulDistance int 1 run scoreboard players operation @e[type=item_frame,tag=prochainNoeudTemp,sort=nearest,limit=1] cumulDistance = @s temp2
# C'est un bugfix un peu crado pour éviter d'avoir à calculer "à l'avance" des directions lors du choix d'un tronçon à emprunter

# exemple du RER A : il a 2 directions "Sud" 2101 et 2102, et 2 directions "Nord" 2153 et 2154 ;
# Sur le tronçon central on peut prendre chacune de ces 4 directions, mais le système d'itinéraire prend systématiquement la première si deux connexions ont la même distance/durée/destination.
# Du coup arrivés à l'embranchement on se retrouve comme des cons à devoir prendre une correspondance alors qu'on aurait pu prendre la bonne direction dès le départ ;
# pour éviter de devoir calculer "à l'avance" la bonne direction sur les lignes de RER, on met en place
# ce système qui permet d'ignorer totalement l'ID de ligne "parmi" les quelques directions parallèles du tronçon central, et de faire notre choix "définitif" à chaque embranchement

# On se retrouve dans cette fonction si on est une ligne de RER ;
# On vérifie juste si la différence entre notre "prochain" ID de ligne et notre "actuel" ID de ligne est inférieure à 10 ; si c'est le cas, on va dans la même direction globale, et donc on retire notre tag nouvelleEntree pour mettre à jour, à la place, l'ID de ligne de l'étape précédente
scoreboard players operation #itineraire temp = #itineraire idLine
scoreboard players operation #itineraire temp -= #itineraire prochaineLigne
execute if score #itineraire temp matches -10..10 run tag @s remove nouvelleEntree
scoreboard players reset #itineraire temp
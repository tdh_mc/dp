# C'est un bugfix un peu crado pour éviter des correspondances inutiles (dans la même station)

# On enregistre dans quel ID station on est arrivés à la fin de l'étape précédente (chemin[1]),
# et si cet ID station est identique à celui qu'on a à la fin de *l'étape actuelle* (chemin[0]) (stockée dans #itineraire idLine/idStation),
# on considère qu'on peut remplacer l'étape actuelle par la *prochaine étape* (stockée dans #itineraire prochaineStation/Ligne)
execute store result score #itineraire temp run data get entity @s Item.tag.tch.itineraire.chemin[1].station.id
execute if score #itineraire temp matches 0 run scoreboard players operation #itineraire temp = #tchItineraire stationDepart
execute if score #itineraire temp = #itineraire idStation run tag @s remove nouvelleEntree
scoreboard players reset #itineraire temp
execute if score @s idStation matches 1.. run data modify entity @s Item.tag.tch.itineraire.chemin[0].station.details set from entity @s Item.tag.display.station
execute if score #itineraire prochaineLigne matches 1.. run data modify entity @s Item.tag.tch.itineraire.chemin[0].line.details set from entity @s Item.tag.display.line
# On fait un rendu du texte de terminus à partir de l'ID de ligne, et on le remet dans le même champ
execute if score #itineraire prochaineLigne matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:itineraire/calcul/voisins/itineraire/calcul_terminus
execute if score @s idSortie matches 1.. run data modify entity @s Item.tag.tch.itineraire.chemin[0].sortie.details set from entity @s Item.tag.display.sortie

execute if score @s idStation matches 1.. store result entity @s Item.tag.tch.itineraire.chemin[0].station.id int 1 run scoreboard players get @s idStation
execute if score #itineraire prochaineLigne matches 1.. store result entity @s Item.tag.tch.itineraire.chemin[0].line.id int 1 run scoreboard players get #itineraire prochaineLigne
execute if score @s idSortie matches 1.. store result entity @s Item.tag.tch.itineraire.chemin[0].sortie.id int 1 run scoreboard players get @s idSortie
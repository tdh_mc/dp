data modify entity @s Item.tag.tch.itineraire.chemin_debug prepend value {}

execute if score @s idStation matches 1.. store result entity @s Item.tag.tch.itineraire.chemin_debug[0].idStation int 1 run scoreboard players get @s idStation
# On utilise #itineraire prochaineLigne pour pouvoir différencier entre les différentes directions
execute if score #itineraire prochaineLigne matches 1.. store result entity @s Item.tag.tch.itineraire.chemin_debug[0].idLine int 1 run scoreboard players get #itineraire prochaineLigne
execute if score @s idSortie matches 1.. store result entity @s Item.tag.tch.itineraire.chemin_debug[0].idSortie int 1 run scoreboard players get @s idSortie

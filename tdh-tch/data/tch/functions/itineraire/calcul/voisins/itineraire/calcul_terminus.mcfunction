# On se trouve à la position d'une item frame TexteTemporaire

# On tag le terminus
execute as @e[type=item_frame,tag=tchQuaiPC] if score @s idTerminus = #itineraire prochaineLigne run tag @s add ourTerminusTemp

# On colore le nom du terminus
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusTemp,limit=1] Item.tag.display.station.nom
data modify block ~ ~ ~ Text1 set from entity @s Item.tag.tch.itineraire.chemin[0].line.details.terminus
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusTemp,limit=1] Item.tag.display.station.prefixe
data modify block ~ ~ ~ Text2 set from entity @s Item.tag.tch.itineraire.chemin[0].line.details.terminus

# On le re-stocke dans le même champ, en laissant de la place pour les deDu et compagnie
data modify entity @s Item.tag.tch.itineraire.chemin[0].line.details.terminus set from entity @e[type=item_frame,tag=ourTerminusTemp,limit=1] Item.tag.display.station
data modify entity @s Item.tag.tch.itineraire.chemin[0].line.details.terminus.nom set from block ~ ~ ~ Text1
data modify entity @s Item.tag.tch.itineraire.chemin[0].line.details.terminus.prefixe set from block ~ ~ ~ Text2

# On retire le tag du terminus
tag @e[type=item_frame,tag=ourTerminusTemp] remove ourTerminusTemp
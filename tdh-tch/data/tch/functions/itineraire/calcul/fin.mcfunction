# Message de debug
execute if score #tchItineraire ticksWaited matches ..40 run tellraw @a[tag=ItinDebugLog] [{"text":"Temps de calcul: ","color":"green"},{"score":{"name":"#tchItineraire","objective":"ticksWaited"}},{"text":" ticks!"}]
execute if score #tchItineraire ticksWaited matches 41..99 run tellraw @a[tag=ItinDebugLog] [{"text":"Temps de calcul: ","color":"gold"},{"score":{"name":"#tchItineraire","objective":"ticksWaited"}},{"text":" ticks!"}]
execute if score #tchItineraire ticksWaited matches 100.. run tellraw @a[tag=ItinDebugLog] [{"text":"Temps de calcul: ","color":"red"},{"score":{"name":"#tchItineraire","objective":"ticksWaited"}},{"text":" ticks!"}]

# On enregistre le fait que le système d'itinéraire est libre
scoreboard players set #tchItineraire status 0
scoreboard players reset #tchItineraire dialogueID

# S'il y a encore des opérations en attente, on exécute la suivante
execute if data storage tch:itineraire en_attente[0] run function tch:itineraire/calcul/prochain_calcul
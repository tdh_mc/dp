# Message de debug
tellraw @a[tag=ItinDebugLog] [{"text":"Station d'arrivée introuvable","color":"red"}]

# On ne peut pas trouver d'itinéraire

# On enregistre le résultat
data modify storage tch:itineraire dernier_resultat set value {succes:0b,erreur:"impossible de trouver le point d'arrivée"}
data modify storage tch:itineraire dernier_resultat.depart set from entity @e[type=item_frame,tag=depart,limit=1] Item.tag.display.station
data modify storage tch:itineraire dernier_resultat.arrivee set from entity @e[type=item_frame,tag=arrivee,limit=1] Item.tag.display.station

# On enregistre le fait que le calcul est terminé
scoreboard players set #tchItineraire status 2

# On attend 0.5s avant de faire un nouveau calcul (ou de libérer la place s'il n'y a pas de calcul en attente)
schedule function tch:itineraire/calcul/fin 10t
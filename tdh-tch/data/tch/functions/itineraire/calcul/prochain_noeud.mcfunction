# Message de debug
tellraw @a[tag=ItinDetailLog] [{"text":"Calcul du prochain noeud","color":"aqua"}]

# Parmi toutes les stations possibles, on en prend une au hasard et on lui donne un tag
tag @e[type=item_frame,tag=tchItinNode,tag=possible,sort=random,limit=1] add meilleurChoix

# On vérifie ensuite pour chaque station possible si elle est meilleure que le choix actuel
execute as @e[type=item_frame,tag=tchItinNode,tag=!meilleurChoix,tag=possible] run function tch:itineraire/calcul/prochain_noeud/tester_choix

# Une fois qu'on a trouvé la meilleure station, on la calcule
execute as @e[type=item_frame,tag=meilleurChoix,limit=1] run function tch:itineraire/calcul/voisins

# On supprime le tag meilleurChoix
tag @e[type=item_frame,tag=meilleurChoix] remove meilleurChoix

## Résultats possibles :
# S'il n'y a plus de noeuds possibles et qu'on n'a pas atteint l'arrivée, on termine le calcul (échec)
execute unless entity @e[type=item_frame,tag=tchItinNode,tag=arrivee,tag=dejaCalcule] unless entity @e[type=item_frame,tag=tchItinNode,tag=possible] run function tch:itineraire/calcul/echec/itineraire
# Si on est à l'arrivée, on termine le calcul (succès)
execute as @e[type=item_frame,tag=tchItinNode,tag=arrivee,tag=dejaCalcule] run function tch:itineraire/calcul/succes
# On fait une récursion si on n'a pas encore atteint l'arrivée et qu'il reste des noeuds possibles
# On met une condition également sur status=1 pour permettre d'arrêter le système s'il est en boucle
execute if score #tchItineraire status matches 1 unless entity @e[type=item_frame,tag=tchItinNode,tag=arrivee,tag=dejaCalcule] if entity @e[type=item_frame,tag=tchItinNode,tag=possible] run function tch:itineraire/calcul/prochain_noeud/recursion
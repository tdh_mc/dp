# On vérifie si le noeud actuel est "meilleur" (+proche d'un noeud déjà atteint) que celui actuellement choisi
execute if score #tchItineraire mode matches 1 if score @s cumulTemps < @e[tag=meilleurChoix,limit=1] cumulTemps run tag @s add nouveauMeilleurChoix
execute if score #tchItineraire mode matches 2 if score @s cumulDistance < @e[tag=meilleurChoix,limit=1] cumulDistance run tag @s add nouveauMeilleurChoix

# Si le noeud est meilleur, on lui donne le tag meilleurChoix à la place de l'autre
execute as @s[tag=nouveauMeilleurChoix] run function tch:itineraire/calcul/prochain_noeud/changer_choix
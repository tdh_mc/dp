# On exécute cette fonction pour passer au noeud suivant
# On va soit schedule la fonction dans 1 tick, soit l'enchaîner immédiatement avec la précédente,
# selon l'état de la variable de contrôle qui détermine le nombre maximal d'opérations par tick

# On incrémente la variable de contrôle
scoreboard players add #tchItineraire min 1


# Si elle a atteint le max, on schedule le prochain noeud dans 1 tick
execute if score #tchItineraire min > #tchItineraire max run schedule function tch:itineraire/calcul/prochain_noeud 1t
# Sinon, on passe tout de suite au prochain noeud
execute unless score #tchItineraire min > #tchItineraire max run function tch:itineraire/calcul/prochain_noeud

# On incrémente le timer du temps de calcul
execute if score #tchItineraire min > #tchItineraire max run scoreboard players add #tchItineraire ticksWaited 1
# On définit la variable à 0, si on a atteint le max ;
# à la toute fin de la fonction pour éviter de niquer la condition ci dessus
execute if score #tchItineraire min > #tchItineraire max run scoreboard players set #tchItineraire min 0
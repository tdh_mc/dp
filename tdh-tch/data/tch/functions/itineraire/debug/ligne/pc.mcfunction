# On enregistre les informations d'un PC de ligne, qui exécute cette fonction,
# et on les affiche au joueur ayant le tag tchItineraireJoueurDebug

data modify storage tch:itineraire debug.pc.display set from entity @s Item.tag.display

# On affiche les informations :
# - Nom de la ligne
tellraw @p[tag=tchItineraireJoueurDebug] [{"hoverEvent":{"action":"show_text","contents":[{"text":"Je monte dans ","color":"gray"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.prefixe","color":"yellow"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.nom"},{"text":"\nIl a peur "},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.deDu","color":"gold"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.nom"},{"text":"\nElle n'a pas fait assez attention "},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.alAu","color":"red"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.nom"}]},"text":"Ligne ","color":"gold"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.nom","interpret":"true","color":"gray"},{"text":" ["},{"score":{"name":"@s","objective":"idLine"},"bold":"true","extra":[{"text":"-"},{"score":{"name":"@s","objective":"idLineMax"}}]},{"text":"]"}]

# On enregistre l'ID line "vrai" et l'ID line max
# (car on peut très bien appeler cette fonction en définissant idLine = 22,
#  auquel cas si on définit juste idLineMax on aura comme plage 22-22 au lieu de 21-22)
scoreboard players operation #tchItinDebug idLine = @s idLine
scoreboard players operation #tchItinDebug idLineMax = @s idLineMax
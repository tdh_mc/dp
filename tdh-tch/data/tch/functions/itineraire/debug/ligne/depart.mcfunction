

# - Séparateur
tellraw @p[tag=tchItineraireJoueurDebug] [{"text":"==============================","color":"gray"}]
tellraw @p[tag=tchItineraireJoueurDebug] [{"text":"Direction "},{"score":{"name":"#tchItinDebug","objective":"idLine"},"bold":"true","color":"gray"}]

# On reset les variables dont on va se servir
scoreboard players reset #tchItinDebugNode idStation
scoreboard players reset #tchItinDebugNode idSortie
scoreboard players reset #tchItinDebugNode idLine
# On définit aussi une variable temporaire pour éviter de boucler à l'infini en cas de config erronée
scoreboard players set #tchItinDebug max 50

# On affiche les données du terminus, puis toutes les stations suivantes par récursion
function tch:itineraire/debug/ligne/etape

# On affiche un message, uniquement si l'opération s'est bien passée
# (= on a atteint "normalement" la fin de la ligne, sans station manquante)
execute if score #tchItinDebugNode temp matches 1 run tellraw @p[tag=tchItineraireJoueurDebug] [{"text":"Ce tracé est valide!","color":"green","italic":"true"}]

# On retire le tag temporaire de toutes les stations visitées
tag @e[type=item_frame,tag=tchItinDebugVisite] remove tchItinDebugVisite
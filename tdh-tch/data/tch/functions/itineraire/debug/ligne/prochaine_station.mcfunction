# La connexion que l'on doit emprunter est stockée dans le storage,
# et l'ID de la station que l'on doit rejoindre est située dans #tchItinDebugNode idStation

# On retranche 1 itération au maximum autorisé
scoreboard players remove #tchItinDebug max 1

# Avant toute chose, on copie l'ID de la station actuelle dans la station précédente
scoreboard players operation #tchItinDebug idStation = @s idStation
# On enregistre notre ID de ligne, et on tente de récupérer la station correspondante
scoreboard players operation #tchItinDebugNode idLine = @s idLine
execute as @e[type=item_frame,tag=tchQuaiPC] if score @s idStation = #tchItinDebugNode idStation if score @s idLine = #tchItinDebugNode idLine run tag @s add tchItinDebugNextNode

# Différents messages d'erreur si on ne peut pas passer à la station suivante
# - Si elle est introuvable
execute unless entity @e[type=item_frame,tag=tchItinDebugNextNode,limit=1] run tellraw @p[tag=tchItineraireJoueurDebug] [{"text":"Erreur: Impossible de trouver le quai de la ligne ","color":"red","italic":"true"},{"score":{"name":"#tchItinDebugNode","objective":"idLine"},"bold":"true"},{"text":" à la station "},{"score":{"name":"#tchItinDebugNode","objective":"idStation"},"bold":"true"},{"text":"."}]
# - Si on a atteint le maximum d'itérations
execute if score #tchItinDebug max matches ..0 run tellraw @p[tag=tchItineraireJoueurDebug] [{"text":"Erreur: Le maximum d'itérations a été atteint. Probable boucle infinie.","color":"red","italic":"true"}]

# Si on a trouvé une station, on s'y "déplace" et on vérifie les connexions depuis celle-ci
# (sauf si on a atteint le maximum d'itérations, auquel cas on s'arrête brutalement)
execute if score #tchItinDebug max matches 1.. as @e[type=item_frame,tag=tchItinDebugNextNode,limit=1] run function tch:itineraire/debug/ligne/etape
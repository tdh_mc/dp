# On enregistre les différents ID du noeud actuel :
execute store result score #tchItinDebugNode idStation run data get storage tch:itineraire debug.pc.tch.connexions[0].station
execute store result score #tchItinDebugNode idSortie run data get storage tch:itineraire debug.pc.tch.connexions[0].sortie
execute store result score #tchItinDebugNode idLine run data get storage tch:itineraire debug.pc.tch.connexions[0].line

# Si les ID ne correspondent pas à ce qu'on attend
# (= le bon ID line, pas d'ID sortie)
# On retire cette connexion
scoreboard players set #tchItinDebugNode temp 0
execute if score #tchItinDebugNode idSortie matches 1.. run scoreboard players set #tchItinDebugNode temp 1
execute unless score #tchItinDebugNode idLine = #tchItinDebug idLine run scoreboard players set #tchItinDebugNode temp 1
execute unless score #tchItinDebugNode idStation matches 1.. run scoreboard players set #tchItinDebugNode temp 1
# On la retire aussi si elle mène vers une station déjà visitée
execute as @e[type=item_frame,tag=tchItinDebugVisite] if score @s idStation = #tchItinDebugNode idStation run scoreboard players set #tchItinDebugNode temp 1
execute if score #tchItinDebugNode temp matches 1 run data remove storage tch:itineraire debug.pc.tch.connexions[0]

# S'il reste un élément à la liste (et qu'on a supprimé celui-ci), on tente également de le supprimer
# Si on n'a pas supprimé celui-ci, c'est qu'il matche ; on peut donc s'arrêter et le conserver
execute if score #tchItinDebugNode temp matches 1 if data storage tch:itineraire debug.pc.tch.connexions[0] run function tch:itineraire/debug/ligne/filtrer_connexion
# On exécute plusieurs fois la fonction depart, en direction de chacun des terminus hormis celui qui exécute la fonction

# On tag toutes les destinations possibles
tag @e[type=item_frame,tag=tchItineraireTerminusOrig] add tchItineraireTerminusDest
tag @s remove tchItineraireTerminusDest

# On tente d'afficher la ligne au départ de ce terminus en direction de chacun des autres
function tch:itineraire/debug/ligne/terminus_origine_loop
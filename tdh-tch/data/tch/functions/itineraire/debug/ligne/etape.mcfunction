# On retire l'éventuel tag donné par la fonction prochaine_station
tag @s remove tchItinDebugNextNode
# On enregistre le fait qu'on a déjà visité ce noeud/cette station
tag @s add tchItinDebugVisite

# On affiche nos propres données
tellraw @p[tag=tchItineraireJoueurDebug] [{"text":"","color":"gray"},{"text":"↓ ","bold":"true"},{"entity":"@s","nbt":"Item.tag.display.station.prefixe"},{"entity":"@s","nbt":"Item.tag.display.station.nom"},{"text":" ["},{"entity":"@s","nbt":"Item.tag.display.station.code","bold":"true"},{"text":"]"}]

# On copie nos données de connexion, et on conserve la première connexion :
# - qui relie ce quai à un autre quai de la même ligne
# - dont la destination ne soit pas une station visitée précédemment
data modify storage tch:itineraire debug.pc.tch.connexions set from entity @s Item.tag.tch.connexions
# On supprime la première connexion si elle ne satisfait pas aux conditions ci-dessus
# (et on supprime également les suivantes par récursion jusqu'à trouver ou vider la liste)
execute if data storage tch:itineraire debug.pc.tch.connexions[0] run function tch:itineraire/debug/ligne/filtrer_connexion

# S'il reste une connexion, c'est qu'elle est valide ;
# on récupère donc la station correspondante et on répète cette fonction avec elle
execute if data storage tch:itineraire debug.pc.tch.connexions[0] run function tch:itineraire/debug/ligne/prochaine_station
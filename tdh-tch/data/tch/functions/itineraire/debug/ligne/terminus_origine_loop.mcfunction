# On récupère l'ID de ligne correspondant à l'un des terminus restants, et on supprime le tag du terminus
scoreboard players operation #tchItinDebug idLine = @e[type=item_frame,tag=tchItineraireTerminusDest,limit=1] idTerminus
tag @e[type=item_frame,tag=tchItineraireTerminusDest,limit=1] remove tchItineraireTerminusDest

# On exécute la fonction depart, à destination de ce terminus (et en tant que notre terminus de départ)
function tch:itineraire/debug/ligne/depart

# S'il reste des terminus à (tenter de) desservir, on recommence cette fonction
execute if entity @e[type=item_frame,tag=tchItineraireTerminusDest,limit=1] run function tch:itineraire/debug/ligne/terminus_origine_loop
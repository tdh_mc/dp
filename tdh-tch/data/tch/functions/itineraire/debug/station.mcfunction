# On appelle cette fonction pour afficher toutes les informations liées à n'importe quelle station
# Il suffit d'avoir défini la variable #tchItinDebug idStation à la valeur souhaitée

tellraw @s [{"text":"=== Information Itinéraire ===","color":"gold"}]
tag @s add tchItineraireJoueurDebug

# On affiche d'abord les infos détaillées du "hub" de la station
execute as @e[type=item_frame,tag=tchStationPC] if score @s idStation = #tchItinDebug idStation run function tch:itineraire/debug/station/hub
# Si l'ID station correspond à un lieu-dit, on affiche les informations du lieu-dit
execute as @e[type=item_frame,tag=tchLieuDitPC] if score @s idStation = #tchItinDebug idStation run function tch:itineraire/debug/station/lieudit

# On affiche ensuite les informations de chacun des quais de cette station,
# en les triant dans l'ordre (du plus petit au plus grand ID de ligne)
# On commence donc par donner un tag à tous les quais qui correspondent :
execute as @e[type=item_frame,tag=tchQuaiPC] if score @s idStation = #tchItinDebug idStation run tag @s add tchItineraireQuaiTemp
# Tant qu'il reste des quais, on en affiche un avant de supprimer son tag
execute if entity @e[type=item_frame,tag=tchItineraireQuaiTemp,limit=1] run function tch:itineraire/debug/station/quais

# On affiche ensuite les informations de chacune des sorties de cette station,
# en les triant dans l'ordre (du plus petit au plus grand ID de sortie)
# On commence donc par donner un tag à toutes les sorties qui correspondent :
execute as @e[type=item_frame,tag=tchSortiePC] if score @s idStation = #tchItinDebug idStation run tag @s add tchItineraireSortieTemp
# Tant qu'il reste des sorties, on en affiche une avant de supprimer son tag
execute if entity @e[type=item_frame,tag=tchItineraireSortieTemp,limit=1] run function tch:itineraire/debug/station/sorties

# On retire le tag du joueur
tag @s remove tchItineraireJoueurDebug
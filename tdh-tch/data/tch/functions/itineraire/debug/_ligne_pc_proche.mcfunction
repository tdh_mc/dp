# On appelle cette fonction pour afficher toutes les informations liées à la ligne dont un PC est le plus proche de nous (PC = PC de quai, pas PC principal de ligne)
scoreboard players reset #tchItinDebug idLine
scoreboard players operation #tchItinDebug idLine = @e[type=item_frame,tag=tchQuaiPC,sort=nearest,limit=1] idLine
execute if score #tchItinDebug idLine matches 1.. run function tch:itineraire/debug/ligne
# On appelle cette fonction pour afficher toutes les informations liées à n'importe quelle ligne ;
# c'est à dire qu'on affichera d'abord les infos de son PC, puis tous ses quais liés d'un terminus à l'autre (dans les deux sens) pour être sûrs que les connexions sont bien configurées
# Il suffit d'avoir défini la variable #tchItinDebug idLine à la valeur souhaitée

tellraw @s [{"text":"=== Information Ligne ===","color":"gold"}]
tag @s add tchItineraireJoueurDebug

# On affiche d'abord les infos détaillées du PC de la ligne
execute as @e[type=item_frame,tag=tchPC] if score @s idLine <= #tchItinDebug idLine if score @s idLineMax >= #tchItinDebug idLine run function tch:itineraire/debug/ligne/pc
# On en profite pour récupérer la valeur d'idLineMax correspondante (dans la fonction ci-dessus)

# On affiche ensuite les informations de chacun des quais de cette ligne, de manière séquentielle,
# en commençant par le terminus et en suivant les connexions
# On donne un tag à chacun des terminus
execute as @e[type=item_frame,tag=tchQuaiPC,scores={idTerminus=1..}] if score @s idTerminus >= #tchItinDebug idLine if score @s idTerminus <= #tchItinDebug idLineMax run tag @s add tchItineraireTerminusOrig

# En partant de chacun des terminus, et à destination de chacun des autres terminus, on "trace" la ligne
# On affiche beaucoup moins d'infos que dans la version "station", juste une ligne par station parcourue
execute as @e[type=item_frame,tag=tchItineraireTerminusOrig] run function tch:itineraire/debug/ligne/terminus_origine

# On retire le tag du joueur et des terminus
tag @s remove tchItineraireJoueurDebug
tag @e[type=item_frame,tag=tchItineraireTerminusOrig] remove tchItineraireTerminusOrig
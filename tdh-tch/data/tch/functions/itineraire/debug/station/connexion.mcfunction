# On affiche la première connexion du tableau actuellement stocké dans le storage

# On sauvegarde les variables du noeud où on ARRIVE
execute store result score #tchItinDebugDest idStation run data get storage tch:itineraire debug.pc.tch.connexions[0].station
execute store result score #tchItinDebugDest idLine run data get storage tch:itineraire debug.pc.tch.connexions[0].line
execute store result score #tchItinDebugDest idSortie run data get storage tch:itineraire debug.pc.tch.connexions[0].sortie

# On sauvegarde les variables de l'entité qui exécute cette fonction (l'endroit dont on PART)
scoreboard players operation #tchItinDebugOrig idStation = @s idStation
scoreboard players operation #tchItinDebugOrig idLine = @s idLine
scoreboard players operation #tchItinDebugOrig idSortie = @s idSortie

# On calcule le temps de trajet dans un format humainement lisible
execute store result score #store currentHour run data get storage tch:itineraire debug.pc.tch.connexions[0].temps
function tdh:store/time_delay

# On calcule la distance dans un format humainement lisible
execute store result score #store distance run data get storage tch:itineraire debug.pc.tch.connexions[0].distance
# On multiplie la distance par le facteur de temps TDH pour obtenir un kilométrage cohérent vis-à-vis du temps de trajet
scoreboard players set #tchItinDebug temp 72
scoreboard players operation #tchItinDebug temp *= #tempsOriginal fullDayTicks
scoreboard players operation #tchItinDebug temp /= #temps fullDayTicks
scoreboard players operation #store distance *= #tchItinDebug temp
scoreboard players reset #tchItinDebug temp
function tdh:store/distance_format


# Selon le type de connexion, on a un message différent ;
# ce type de connexion dépend des variables enregistrées dans le noeud

# On se donne ce tag, qu'on va ensuite retirer si on passe dans une des fonctions ci-dessous
# S'il est encore présent à la toute fin, ça voudra dire qu'on est passé dans aucune des fonctions
# et que la connexion est mal configurée
tag @s add ConnexionInvalide

# [Station = 0, Line = 0, Sortie = 0] : Connexion vers le Hub d'une station
execute if score #tchItinDebugDest idStation matches 0 if score #tchItinDebugDest idLine matches 0 if score #tchItinDebugDest idSortie matches 0 run function tch:itineraire/debug/station/connexion/hub
# [Station = X, Line = X, Sortie = 0] : Connexion vers une station via une ligne
execute if score #tchItinDebugDest idStation matches 1.. if score #tchItinDebugDest idLine matches 1.. if score #tchItinDebugDest idSortie matches 0 if score #tchItinDebugOrig idLine matches 1.. run function tch:itineraire/debug/station/connexion/station
# [Station = X, Line = 0, Sortie = 0] : Connexion vers un lieu-dit, à pied
execute if score #tchItinDebugDest idStation matches 1.. if score #tchItinDebugDest idLine matches 0 if score #tchItinDebugDest idSortie matches 0 if score #tchItinDebugOrig idLine matches 0 run function tch:itineraire/debug/station/connexion/lieudit
# [Station = 0, Line = X, Sortie = 0] : Connexion vers le quai d'une ligne dans une station
execute if score #tchItinDebugDest idStation matches 0 if score #tchItinDebugDest idLine matches 1.. if score #tchItinDebugDest idSortie matches 0 run function tch:itineraire/debug/station/connexion/line
# [Station = 0, Line = 0, Sortie = X] : Connexion vers une sortie d'une station
execute if score #tchItinDebugDest idStation matches 0 if score #tchItinDebugDest idLine matches 0 if score #tchItinDebugDest idSortie matches 1.. run function tch:itineraire/debug/station/connexion/sortie
# [Station = X, Line = 0, Sortie = X] : Connexion vers une sortie d'une AUTRE station (donc une entrée)
execute if score #tchItinDebugDest idStation matches 1.. if score #tchItinDebugDest idLine matches 0 if score #tchItinDebugDest idSortie matches 1.. run function tch:itineraire/debug/station/connexion/entree

# Si on a encore le tag, on affiche un message d'erreur concernant cette connexion
execute as @s[tag=ConnexionInvalide] run function tch:itineraire/debug/station/connexion/invalide


# Dans tous les cas, on supprime cette connexion et on passe à la suivante s'il en reste
data remove storage tch:itineraire debug.pc.tch.connexions[0]
execute if data storage tch:itineraire debug.pc.tch.connexions[0] run function tch:itineraire/debug/station/connexion
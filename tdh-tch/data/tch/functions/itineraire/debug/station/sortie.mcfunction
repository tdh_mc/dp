# On enregistre les informations d'une sortie, qui exécute cette fonction,
# et on les affiche au joueur ayant le tag tchItineraireJoueurDebug

data modify storage tch:itineraire debug.pc.display set from entity @s Item.tag.display
data modify storage tch:itineraire debug.pc.tch set from entity @s Item.tag.tch

# On affiche les informations :
# - Nom de la station + Nom de la sortie + Position
tellraw @p[tag=tchItineraireJoueurDebug] [{"hoverEvent":{"action":"show_text","contents":[{"text":"Il vient ","color":"gray"},{"storage":"tch:itineraire","nbt":"debug.pc.display.sortie.deDu"},{"storage":"tch:itineraire","nbt":"debug.pc.display.sortie.nom"},{"text":"\nElle sort "},{"storage":"tch:itineraire","nbt":"debug.pc.display.sortie.alAu"},{"storage":"tch:itineraire","nbt":"debug.pc.display.sortie.nom"},{"text":" "},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.deDu"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.nom"}]},"text":"Station ","color":"gold"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.prefixe"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.nom"},{"text":"–"},{"storage":"tch:itineraire","nbt":"debug.pc.display.Name","interpret":"true"},{"text":" — Position (","color":"gray","extra":[{"storage":"tch:itineraire","nbt":"debug.pc.tch.position.x"},{"text":", "},{"storage":"tch:itineraire","nbt":"debug.pc.tch.position.z"},{"text":")"}]}]

# - Connexions
execute if data storage tch:itineraire debug.pc.tch.connexions[0] run function tch:itineraire/debug/station/connexion

# - Séparateur
tellraw @s [{"text":"==============================","color":"gray"}]


# On retire le tag temporaire de cette sortie
tag @s remove tchItineraireSortieTemp
# On exécute cette fonction pour afficher chacun des quais de la station, l'un après l'autre, dans l'ordre des numéros de ligne
# On a donné à toutes les lignes nécessaires un tag tchItineraireQuaiTemp

# On trouve le minimum d'ID de ligne de tous ces quais :
scoreboard players set #tchItinDebug idLine 9999999
execute as @e[type=item_frame,tag=tchItineraireQuaiTemp] run scoreboard players operation #tchItinDebug idLine < @s idLine

# On affiche le quai au plus petit ID de ligne en premier
execute as @e[type=item_frame,tag=tchItineraireQuaiTemp] if score @s idLine = #tchItinDebug idLine run function tch:itineraire/debug/station/quai

# S'il reste des quais à traiter (le tag du plus petit sera retiré par la fonction ci-dessus),
# on recommence cette fonction
execute if entity @e[type=item_frame,tag=tchItineraireQuaiTemp,limit=1] run function tch:itineraire/debug/station/quais
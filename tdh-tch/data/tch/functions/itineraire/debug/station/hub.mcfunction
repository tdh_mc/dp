# On enregistre les informations d'un hub, qui exécute cette fonction,
# et on les affiche au joueur ayant le tag tchItineraireJoueurDebug

data modify storage tch:itineraire debug.pc.display set from entity @s Item.tag.display
data modify storage tch:itineraire debug.pc.tch set from entity @s Item.tag.tch

# On affiche les informations :
# - Nom de la station + Code 3 chiffres + Position
tellraw @p[tag=tchItineraireJoueurDebug] [{"hoverEvent":{"action":"show_text","contents":[{"text":"Il vient ","color":"gray"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.deDu","color":"gold"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.nom"},{"text":"\nElle va "},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.alAu","color":"yellow"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.nom"}]},"text":"Hub ","color":"gold"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.deDu"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.nom"},{"text":" ["},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.code","bold":"true"},{"text":"]"},{"text":" — Position (","color":"gray","extra":[{"storage":"tch:itineraire","nbt":"debug.pc.tch.position.x"},{"text":", "},{"storage":"tch:itineraire","nbt":"debug.pc.tch.position.z"},{"text":")"}]}]

# - Connexions
execute if data storage tch:itineraire debug.pc.tch.connexions[0] run function tch:itineraire/debug/station/connexion

# - Séparateur
tellraw @s [{"text":"==============================","color":"gray"}]
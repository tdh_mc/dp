# On exécute cette fonction pour afficher chacune des sorties de la station, l'un après l'autre, dans l'ordre des numéros de sortie
# On a donné à toutes les lignes nécessaires un tag tchItineraireSortieTemp

# On trouve le minimum d'ID de toutes ces sorties :
scoreboard players set #tchItinDebug idSortie 9999999
execute as @e[type=item_frame,tag=tchItineraireSortieTemp] run scoreboard players operation #tchItinDebug idSortie < @s idSortie

# On affiche la sortie au plus petit ID en premier
execute as @e[type=item_frame,tag=tchItineraireSortieTemp] if score @s idSortie = #tchItinDebug idSortie run function tch:itineraire/debug/station/sortie

# S'il reste des sorties à traiter (le tag du plus petit sera retiré par la fonction ci-dessus),
# on recommence cette fonction
execute if entity @e[type=item_frame,tag=tchItineraireSortieTemp,limit=1] run function tch:itineraire/debug/station/sorties
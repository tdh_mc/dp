# On enregistre les informations d'un quai, qui exécute cette fonction,
# et on les affiche au joueur ayant le tag tchItineraireJoueurDebug

data modify storage tch:itineraire debug.pc.display set from entity @s Item.tag.display
data modify storage tch:itineraire debug.pc.tch set from entity @s Item.tag.tch

# On affiche les informations :
# - Nom de la ligne + Nom de la station
tellraw @p[tag=tchItineraireJoueurDebug] [{"hoverEvent":{"action":"show_text","contents":[{"text":"Il prend ","color":"gray"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.prefixe"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.nom"},{"text":" "},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.alAu","color":"gold"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.nom"},{"text":"\nElle sort "},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.deDu"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.nom"},{"text":"\nOn peut dire merci "},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.alAu"},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.vehicule.nom"},{"text":"!"}]},"text":"Station ","color":"gold"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.prefixe"},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.nom"},{"text":"–Quai du "},{"storage":"tch:itineraire","nbt":"debug.pc.display.line.nom","interpret":"true"},{"text":" ["},{"storage":"tch:itineraire","nbt":"debug.pc.display.station.code","bold":"true"},{"text":"]"}]

# - Connexions
execute if data storage tch:itineraire debug.pc.tch.connexions[0] run function tch:itineraire/debug/station/connexion

# - Séparateur
tellraw @s [{"text":"==============================","color":"gray"}]


# On retire le tag temporaire de ce quai
tag @s remove tchItineraireQuaiTemp
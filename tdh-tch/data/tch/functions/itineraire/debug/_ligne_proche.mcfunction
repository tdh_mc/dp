# On appelle cette fonction pour afficher toutes les informations liées à la ligne la plus proche de nous
scoreboard players reset #tchItinDebug idLine
scoreboard players operation #tchItinDebug idLine = @e[type=#tch:marqueur/quai,tag=NumLigne,sort=nearest,limit=1] idLine
execute if score #tchItinDebug idLine matches 1.. run function tch:itineraire/debug/ligne
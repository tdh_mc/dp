# On appelle cette fonction pour afficher toutes les informations liées à la station dont le PC est le plus proche de nous
scoreboard players reset #tchItinDebug idStation
scoreboard players operation #tchItinDebug idStation = @e[type=item_frame,tag=tchItinNode,sort=nearest,limit=1] idStation
execute if score #tchItinDebug idStation matches 1.. run function tch:itineraire/debug/station
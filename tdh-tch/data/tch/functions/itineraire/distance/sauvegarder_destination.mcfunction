# On enregistre le nom détaillé de la station dans le storage (avec deDu et alAu notamment)
data modify storage tch:itineraire distance.destination set from entity @s Item.tag.display.station

# On enregistre la position d'exécution de la commande dans un score
execute store result score #tchItineraire posX run data get entity @p Pos[0]
execute store result score #tchItineraire posZ run data get entity @p Pos[2]

# On enregistre la position de la station dans un score
execute store result score #tchItineraire deltaX run data get entity @s Item.tag.tch.position.x
execute store result score #tchItineraire deltaZ run data get entity @s Item.tag.tch.position.z

# On exécute ensuite les calculs comme si on avait précisé 2 couples de positions dès le départ
function tch:itineraire/distance
# On utilise cette fonction pour calculer une distance à vol d'oiseau et une direction
# Les variables scoreboard suivantes servent d'argument :

# SCORE #tchItineraire posX / posZ = vecteur direction allant de nous à la destination

# On cherche à savoir si l'axe majeur est X ou Z
scoreboard players operation #tchItineraire temp5 = #tchItineraire posX
scoreboard players set #tchItineraire temp4 10
scoreboard players operation #tchItineraire temp5 *= #tchItineraire temp4
scoreboard players operation #tchItineraire temp5 /= #tchItineraire posZ
scoreboard players set #tchItineraire temp4 -1
execute if score #tchItineraire temp5 matches ..-1 run scoreboard players operation #tchItineraire temp5 *= #tchItineraire temp4
# Si on a temp5 ~= 10 c'est qu'on est en diagonale
execute if score #tchItineraire temp5 matches 9..12 run function tch:itineraire/distance/direction/xz
# Si on a temp5 < 10 c'est qu'on est avec Z comme axe principal
execute if score #tchItineraire temp5 matches ..8 run function tch:itineraire/distance/direction/z
# Sinon si temp5 > 10 on a X comme axe principal
execute if score #tchItineraire temp5 matches 13.. run function tch:itineraire/distance/direction/x

# On réinitialise les variables
scoreboard players reset #tchItineraire temp4
scoreboard players reset #tchItineraire temp5
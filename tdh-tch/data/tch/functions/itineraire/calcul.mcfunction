# On utilise cette fonction pour calculer un itinéraire
# Les variables scoreboard suivantes servent d'argument :

# SCORE #tchItineraire stationDepart = <idStation de départ>
# SCORE #tchItineraire stationArrivee = <idStation d'arrivée>

# Message de debug
tellraw @a[tag=ItinDebugLog] [{"text":"Début du calcul d'itinéraire","color":"aqua"}]

# On enregistre le fait qu'on a lancé le calcul et que personne d'autre ne peut en lancer un
scoreboard players set #tchItineraire status 1
# On initialise le timer du temps de calcul
scoreboard players set #tchItineraire ticksWaited 0
# On initialise le compteur d'itérations, et on définit le max d'opérations par tick
scoreboard players set #tchItineraire max 10
scoreboard players set #tchItineraire min 0

# Si on a lancé le calcul en tant qu'un PNJ en dialogue, on enregistre l'ID de dialogue correspondant
execute if score @s dialogueID matches 0.. run scoreboard players operation #tchItineraire dialogueID = @s dialogueID

# On initialise d'abord tous les noeuds
function tch:itineraire/calcul/init/noeuds
# On désactive ensuite les noeuds des lignes fermées ou en fin de service
#function tch:itineraire/calcul/init/lignes_fermees

# On donne un tag à la station de départ
execute as @e[type=item_frame,tag=tchStationPC] if score @s idStation = #tchItineraire stationDepart run tag @s add depart

# Si on n'a pas trouvé de station de départ, on cherche parmi tous les noeuds
execute unless entity @e[type=item_frame,tag=depart] as @e[type=item_frame,tag=tchItinNode,tag=!tchStationPC,tag=!tchSortiePC,tag=!tchQuaiPC] if score @s idStation = #tchItineraire stationDepart run tag @s add depart

# On donne un tag à la station d'arrivée
execute as @e[type=item_frame,tag=tchStationPC] if score @s idStation = #tchItineraire stationArrivee run tag @s add arrivee

# Si on n'a pas trouvé de station d'arrivée, on cherche parmi tous les noeuds
execute unless entity @e[type=item_frame,tag=arrivee] as @e[type=item_frame,tag=tchItinNode,tag=!tchStationPC,tag=!tchSortiePC,tag=!tchQuaiPC] if score @s idStation = #tchItineraire stationArrivee run tag @s add arrivee

# On initialise les données des noeuds de départ
execute as @e[type=item_frame,tag=tchItinNode,tag=depart] at @s run function tch:itineraire/calcul/init/depart

# Ensuite, on sélectionne le prochain noeud à calculer, si on n'a pas encore atteint l'arrivée et qu'on a trouvé un départ et une arrivée
execute if entity @e[type=item_frame,tag=tchItinNode,tag=depart] if entity @e[type=item_frame,tag=tchItinNode,tag=arrivee] unless entity @e[type=item_frame,tag=tchItinNode,tag=arrivee,tag=dejaCalcule] run schedule function tch:itineraire/calcul/prochain_noeud 1t
# Si on n'a pas trouvé de départ ou d'arrivée, on termine le calcul
execute unless entity @e[type=item_frame,tag=tchItinNode,tag=depart] run function tch:itineraire/calcul/echec/depart
execute unless entity @e[type=item_frame,tag=tchItinNode,tag=arrivee] run function tch:itineraire/calcul/echec/arrivee
# Si on est déjà à l'arrivée, on termine le calcul
execute as @e[type=item_frame,tag=tchItinNode,tag=arrivee,tag=dejaCalcule] run function tch:itineraire/calcul/succes
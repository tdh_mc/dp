# On vérifie à la position de notre voie s'il y a des métros qui pourraient avoir réservé
execute at @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] at @e[type=minecart,tag=Metro,tag=VoieReservee,distance=..120] if score @e[type=minecart,distance=0,limit=1] idLine = @s idLine run tag @s add VraieReservation

# Si ce n'est pas une vraie réservation, on réserve avec succès
execute as @s[tag=!VraieReservation] run function tch:voie/reservation/succes
tag @s remove VraieReservation

# Si on n'a toujours pas fait la réservation, c'est qu'elle n'était pas fantôme finalement, et on se met en file d'attente
execute as @s[tag=!VoieReservee] run function tch:voie/reservation/test_file_attente
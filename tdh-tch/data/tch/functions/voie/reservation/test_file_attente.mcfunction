# Échec de la première réservation
# Cette fonction est exécutée par un véhicule à sa position
# Une entité "ourVoie" existe déjà et stocke notre voie (AS "Direction")

# Si on a déjà une personne en file d'attente, on fait un arrêt d'urgence maintenant
# TODO: on génère aussi une interruption de trafic sur la ligne
execute if score @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] FileAttente matches 1.. run function tch:voie/reservation/pause

# Sinon, on s'ajoute à la file d'attente
execute as @s[tag=!ArretUrgence] run function tch:voie/reservation/file_attente
# Suppression de la file d'attente du véhicule qui exécute cette fonction
# On récupère notre voie
function tch:tag/prochaine_voie

# On se retire de la file d'attente
scoreboard players remove @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] FileAttente 1
tag @s remove FileAttente

# On retire le tag de notre voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] remove ourVoie
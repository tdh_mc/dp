# On vérifie si la voie est libre
# Cette fonction est exécutée par un véhicule à sa position

# On récupère notre voie
function tch:tag/prochaine_voie

# Si la voie n'est pas réservée et n'a personne en file d'attente, on fait la réservation
execute unless score @e[type=#tch:marqueur/quai,tag=ourVoie,tag=!VoieReservee,limit=1] FileAttente matches 1.. run function tch:voie/reservation/succes
# Si on n'a pas fait la réservation, c'est qu'elle a échoué
# On a un check supplémentaire pour déterminer si c'est une réservation "fantôme",
# ce check est spécifique au type de véhicule (métro ou RER)
execute as @s[type=minecart,tag=!VoieReservee] run function tch:voie/reservation/test_reservation_fantome
execute as @s[type=armor_stand,tag=RER,tag=!VoieReservee] run function tch:voie/reservation/test_reservation_fantome_rer

# On supprime le tag de notre voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie] remove ourVoie
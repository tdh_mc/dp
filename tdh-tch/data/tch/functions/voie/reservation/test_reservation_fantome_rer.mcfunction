# Cette fonction existe mais elle ne fait aucun check pour l'instant. On considère toujours une réservation RER comme non fantôme et on verra plus tard si des soucis se présentent.

# On vérifie à la position de notre voie s'il y a des trains qui pourraient avoir réservé
scoreboard players set #rer temp 0
scoreboard players operation #rer idLine = @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] idLine
scoreboard players operation #rer idLineMax = @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] idLineMax
execute at @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] as @e[type=armor_stand,tag=RER,tag=VoieReservee,distance=..200] if score @s idLine = #rer idLine run scoreboard players set #rer temp 1
execute at @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] as @e[type=armor_stand,tag=RER,tag=VoieReservee,distance=..200] if score @s idLine >= #rer idLine if score @s idLine <= #rer idLineMax run scoreboard players set #rer temp 1

# Si ce n'est pas une vraie réservation, on réserve avec succès
execute if score #rer temp matches 0 run function tch:voie/reservation/succes
scoreboard players reset #rer temp

# Si on n'a toujours pas fait la réservation, c'est qu'elle n'était pas fantôme finalement, et on se met en file d'attente
execute as @s[tag=!VoieReservee] run function tch:voie/reservation/test_file_attente
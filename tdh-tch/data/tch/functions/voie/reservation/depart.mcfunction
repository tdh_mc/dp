# On repart du quai et termine la réservation
# Cette fonction est exécutée par un véhicule à sa position

# On récupère notre voie
function tch:tag/voie

# On supprime la réservation
tag @s remove VoieReservee
tag @e[type=#tch:marqueur/quai,tag=ourVoie] remove VoieReservee

# On supprime le tag de notre voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie] remove ourVoie
# On vérifie si la voie est libre
# Cette fonction est exécutée par un véhicule en file d'attente à sa position

# On récupère notre voie
function tch:tag/prochaine_voie

# Si la voie n'est pas réservée, on fait la réservation
execute if entity @e[type=#tch:marqueur/quai,tag=ourVoie,tag=!VoieReservee,limit=1] run function tch:voie/reservation/succes
# Si on n'a pas fait la réservation, on s'arrête
execute as @s[tag=!VoieReservee,tag=!ArretUrgence] run function tch:voie/reservation/pause

# On supprime le tag de notre voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie] remove ourVoie
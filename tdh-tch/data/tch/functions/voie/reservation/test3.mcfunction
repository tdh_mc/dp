# On vérifie si la voie est libre
# Cette fonction est exécutée par un métro en arrêt d'urgence à sa position

# On récupère notre voie
function tch:tag/prochaine_voie

# Si la voie n'est pas réservée, on la réserve
execute if entity @e[type=#tch:marqueur/quai,tag=ourVoie,tag=!VoieReservee,limit=1] run function tch:voie/reservation/succes
# Sinon on ne fait rien, on est déjà en arrêt d'urgence.

# On supprime le tag de notre voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie] remove ourVoie
# Ajout du véhicule à la file d'attente
# Cette fonction est exécutée par un véhicule à sa position
# Une entité "ourVoie" existe déjà et stocke notre voie (AS "Direction")

# On ajoute 1 à la file d'attente de notre voie
scoreboard players add @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] FileAttente 1

# On tag le véhicule
tag @s add FileAttente
# Succès de la réservation !
# Cette fonction est exécutée par un véhicule à sa position
# Une entité "ourVoie" existe déjà et stocke notre voie (AS "Direction")

# On tag à la fois le véhicule et la voie
tag @s add VoieReservee
tag @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] add VoieReservee

# Si le véhicule était en file d'attente, on supprime cet état de fait
execute as @s[tag=FileAttente] run function tch:voie/reservation/sortir_file_attente

# Si le véhicule était en arrêt d'urgence également
execute as @s[tag=ArretUrgence] run scoreboard players reset @s remainingTicks
tag @s remove ArretUrgence
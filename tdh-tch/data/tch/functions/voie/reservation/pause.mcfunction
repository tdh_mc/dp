# Arrêt d'urgence du métro/RER
# Cette fonction est exécutée par un véhicule à sa position
# Une entité "ourVoie" existe déjà et stocke notre voie (AS "Direction")

# Si on n'est pas déjà dans la file d'attente, on s'y ajoute
execute as @s[tag=!FileAttente] run function tch:voie/reservation/file_attente

# On arrête le métro ou le RER selon le cas approprié
execute as @s[type=minecart] run function tch:metro/rame/pre_station/arret_urgence
execute as @s[type=armor_stand,tag=RER] run function tch:rer/rame/interstation/arret_urgence
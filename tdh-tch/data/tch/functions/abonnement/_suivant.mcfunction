# On copie le premier élément du tableau à la fin
data modify storage tch:abonnement actifs append from storage tch:abonnement actifs[0]

# On supprime le premier élément du tableau
data remove storage tch:abonnement actifs[0]
# Cette fonction a vocation à être utilisée depuis d'autres dossiers de TCH
# Elle n'est exécutée par personne, il suffit de définir #tch idAbonnement à l'ID qu'on cherche
# Si la fonction trouve l'abonnement, elle définira l'idAbonnement à 1 ; sinon, elle le définira à -1
# Si elle trouve l'abonnement, elle renverra aussi le nombre de jours restants dans #tch dureeValidite

# Structure du storage des abonnements (tch:abonnement) :
# {
#	actifs: [
#		{ id: <INT>,
#		  valide_le: { tick: <INT>, jour: <INT> },
#		  jours_restants: <INT> },
#		{ ... },
#		{ ... }
#	],
#	archive: [
#		{ id: <INT>,
#		  valide_le: { tick: <INT>, jour: <INT> },
#		  jours_restants: <INT> },
#		{ ... },
#		{ ... }
#	],
#	resultat: {		(peut être absent)
#		id: <INT>,
#		valide_le: { tick: <INT>, jour: <INT> },
#		jours_restants: <INT>
#	}
# }

# On passe les variables nécessaires pour la fonction find
execute store result score #tch temp run data get storage tch:abonnement actifs

# On exécute la fonction de recherche
function tch:abonnement/find
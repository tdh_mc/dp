# On récupère le nombre d'abonnements enregistrés
execute store result score #tch temp run data get storage tch:abonnement actifs

# On itère à travers tout le tableau, en décrémentant chaque nombre de jours de validité de 1
function tch:abonnement/nextday_recursion
# Cette fonction génère un identifiant et le stocke dans idAbonnement, puis vérifie s'il existe déjà
# On doit impérativement avoir taggé le joueur pour qui on génère l'abonnement avec le tag tchAbonnementJoueur

# On prend un nombre aléatoire entre 100'000 et 999'999
scoreboard players set #random min 100000
scoreboard players set #random max 1000000
function tdh:random

# On stocke tout de suite ce nombre aléatoire dans #tch idAbonnement pour chercher s'il existe déjà
scoreboard players operation #tch idAbonnement = #random temp

# On vérifie que l'identifiant est bien unique
# On compare chaque ID d'abonnement
function tch:abonnement/_find

# Si l'ID est bien unique, on stocke à nouveau la variable (l'appel à _find overwrite la variable)
execute if score #tch idAbonnement matches -1 run scoreboard players operation #tch idAbonnement = #random temp
# Si l'ID n'est pas unique, on en génère un nouveau
execute if score #tch idAbonnement matches 1 run function tch:abonnement/gen_id
# On copie le premier élément du tableau à la fin
data modify storage tch:abonnement archive append from storage tch:abonnement archive[0]

# On supprime le premier élément du tableau
data remove storage tch:abonnement archive[0]
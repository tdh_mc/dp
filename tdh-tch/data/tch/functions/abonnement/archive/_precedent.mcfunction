# On copie le dernier élément du tableau au début
data modify storage tch:abonnement archive prepend from storage tch:abonnement archive[-1]

# On supprime le dernier élément du tableau
data remove storage tch:abonnement archive[-1]
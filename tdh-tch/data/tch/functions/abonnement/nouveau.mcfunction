# On génère un nouvel abonnement
# Si c'est pour un joueur, on doit l'avoir taggé avec le tag tchAbonnementJoueur

# On commence par ajouter un abonnement vierge à l'index
data modify storage tch:abonnement actifs prepend value {id:0,jours_restants:0}

# On génère un nouvel ID
function tch:abonnement/gen_id

# On ajoute ce nouvel ID à l'abonnement
execute store result storage tch:abonnement actifs[0].id int 1 run scoreboard players get #tch idAbonnement

# On enregistre le nom du joueur
execute if entity @p[tag=tchAbonnementJoueur] at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:abonnement/sauvegarder_nom

# Enfin, on remplace les derniers résultats par ce nouvel élément, pour faciliter son accès
data modify storage tch:abonnement resultat set from storage tch:abonnement actifs[0]
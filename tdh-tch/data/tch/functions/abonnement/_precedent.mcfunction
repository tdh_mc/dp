# On copie le dernier élément du tableau au début
data modify storage tch:abonnement actifs prepend from storage tch:abonnement actifs[-1]

# On supprime le dernier élément du tableau
data remove storage tch:abonnement actifs[-1]
# Ajoute une nouvelle entrée pour l'abonnement venant d'être généré et stocké dans #tch idAbonnement
data modify storage tch:abonnement actifs prepend value {id:0,jours_restants:0}
execute store result storage tch:abonnement actifs[0].id int 1 run scoreboard players get #tch idAbonnement
# On l'ajoute aussi comme résultat de la recherche pour en faciliter l'accès
data modify storage tch:abonnement resultat set from storage tch:abonnement actifs[0]
# On récupère le nombre de jours actuel
execute store result score #tch temp2 run data get storage tch:abonnement actifs[0].jours_restants

# Si cela fait plus d'un an qu'on n'a pas rechargé l'abonnement, on le supprime de la base de données
execute if score #tch temp2 matches ..-366 run function tch:abonnement/archiver

# Sinon, on décrémente normalement les jours d'abonnement
execute if score #tch temp2 matches -365.. run function tch:abonnement/decrementer_validite

# On décrémente le nombre d'éléments restants au tableau
scoreboard players remove #tch temp 1

# S'il reste des éléments à traiter, on itère les abonnements et on recommence la fonction
# On ne passe à l'abonnement suivant que si notre abonnement actuel n'a pas été supprimé (temp2 >= -365)
execute if score #tch temp matches 1.. unless score #tch temp2 matches ..-366 run function tch:abonnement/_suivant
execute if score #tch temp matches 1.. run function tch:abonnement/nextday_recursion
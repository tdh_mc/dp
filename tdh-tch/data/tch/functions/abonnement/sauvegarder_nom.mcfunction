# On exécute cette fonction à la position de l'item frame TexteTemporaire générée lors de l'initialisation,
# qui doit déjà/toujours contenir le panneau lui aussi généré lors de l'initialisation.

# On enregistre le nom du joueur ayant le tag "tchAbonnementJoueur" dans le storage des abonnements,
# à l'élément 0 car on vient de l'ajouter.

# On commence par enregistrer le nom du joueur dans le panneau
data modify block ~ ~ ~ Text1 set value '{"selector":"@p[tag=tchAbonnementJoueur]"}'

# On copie ensuite le nom "figé" dans le storage des abonnements
data modify storage tch:abonnement actifs[0].nom set from block ~ ~ ~ Text1

# Enfin, on retire le tag au joueur
tag @a[tag=tchAbonnementJoueur] remove tchAbonnementJoueur
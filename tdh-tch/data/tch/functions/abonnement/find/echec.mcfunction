# On vient de vérifier un abonnement qui n'était pas celui qu'on cherchait

# On enregistre le fait qu'on a vérifié un terme
scoreboard players remove #tch temp 1

# On passe à l'abonnement suivant
function tch:abonnement/_suivant


# S'il ne reste plus d'itération à faire, on enregistre l'échec
execute unless score #tch temp matches 1.. run scoreboard players set #tch idAbonnement -1

# Sinon, on continue à itérer
execute if score #tch temp matches 1.. run function tch:abonnement/find
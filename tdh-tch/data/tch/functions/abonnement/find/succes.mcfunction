# On passe les résultats
scoreboard players set #tch idAbonnement 1
execute store result score #tch dureeValidite run data get storage tch:abonnement actifs[0].jours_restants

# On copie les données complètes de l'abonnement dans les résultats
data modify storage tch:abonnement resultat set from storage tch:abonnement actifs[0]
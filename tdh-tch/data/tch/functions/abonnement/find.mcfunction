# Cette fonction est exécutée :
# avec la variable #tch temp assignée au nombre d'abonnements dans la liste,
# et la variable #tch idAbonnement assignée à l'abonnement que l'on recherche

# On stocke l'ID d'abonnement actuel dans temp2
execute store result score #tch temp2 run data get storage tch:abonnement actifs[0].id
# On retranche l'ID d'abonnement qu'on recherche de temp2 ; si temp2 est égal à 0, alors on aura trouvé l'abonnement
scoreboard players operation #tch temp2 -= #tch idAbonnement

# Si l'ID est le même que celui qu'on génère, on copie les résultats
execute if score #tch temp2 matches 0 run function tch:abonnement/find/succes
# Sinon, on enregistre l'échec et on continue à itérer
execute unless score #tch temp2 matches 0 run function tch:abonnement/find/echec
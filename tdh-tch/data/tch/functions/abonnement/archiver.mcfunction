# On archive l'abonnement actuel ; on le retire du fichier des abonnements pour le placer dans l'archive des abonnements
data modify storage tch:abonnement archive append from storage tch:abonnement actifs[0]

# On supprime l'abonnement du fichier principal
data remove storage tch:abonnement actifs[0]
# On décrémente les jours de validité (stockés dans temp2) de l'abonnement actuel
# On enregistre le nouveau nombre de jours dans le fichier des abonnements
execute store result storage tch:abonnement actifs[0].jours_restants int 1 run scoreboard players remove #tch temp2 1
# On affiche tous les abonnements actifs au joueur qui l'exécute
tellraw @s [{"text":"--- Abonnements TCH archivés :","color":"gold"}]

# On stocke le nombre d'abonnements archivés
execute store result score #tch temp run data get storage tch:abonnement archive

# On itère chaque abonnement en affichant ses données
execute if score #tch temp matches 1.. run function tch:abonnement/afficher/archive

# On avance une dernière fois pour ne pas décaler la liste des abonnements après l'exécution de cette fonction
# (même si en principe on se fout de l'ordre, ça voudrait dire qu'un /data modify avec un index précis
# ne correspondrait pas à l'index affiché par la dernière fonction d'affichage exécutée... ce qui serait con)
function tch:abonnement/archive/_suivant
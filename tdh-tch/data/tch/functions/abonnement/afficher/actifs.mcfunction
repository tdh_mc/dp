# On stocke le jour et l'heure de dernière validation
execute store result score #store idJour run data get storage tch:abonnement actifs[0].valide_le.jour
function tdh:store/day
execute store result score #store currentHour run data get storage tch:abonnement actifs[0].valide_le.tick
function tdh:store/time

# On affiche les données de l'abonnement courant
tellraw @s [{"text":" - Abonnement ","color":"gray"},{"text":"#","color":"gold","extra":[{"nbt":"actifs[0].id","storage":"tch:abonnement"}]},{"text":" : "},{"nbt":"actifs[0].jours_restants","storage":"tch:abonnement","color":"yellow"},{"text":" jours restants. "},{"text":"Dernière val. le ","italic":"true","extra":[{"nbt":"day","storage":"tdh:store","interpret":"true","color":"yellow"},{"text":" à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"yellow"},{"text":"."}]},{"text":" Possesseur : "},{"nbt":"actifs[0].nom","storage":"tch:abonnement","interpret":"true","color":"gold"},{"text":"."}]

# On décrémente notre compteur d'abonnements
scoreboard players remove #tch temp 1

# S'il reste des abonnements, on itère une nouvelle fois
execute if score #tch temp matches 1.. run function tch:abonnement/_suivant
execute if score #tch temp matches 1.. run function tch:abonnement/afficher/actifs
## TEST DE SPAWN PARTIE 4 :
## On vérifie si le signal qui nous précède est vert (status = 0).

# On essaie de réserver le signal correspondant à notre ID
# (uniquement si le signal est orange ou vert, que les signaux précédents sont oranges ou verts, et que le signal suivant est orange ou vert)
scoreboard players set #signal status 1
scoreboard players set #previousSignal status 1
scoreboard players set #nextSignal status 1
function tch:signal/reserver/complet

# Si la réservation a échoué, on décale l’horaire de spawn de 5 minutes pour réessayer plus tard
execute unless entity @s[tag=ReservationReussie] run function tch:rer/spawner/spawn/decaler_horaire

# Sinon, si elle a réussi, on réinitialise notre horaire de spawn
scoreboard players reset @s[tag=ReservationReussie] spawnAt

# Si on a réussi à réserver le signal, on fait spawner un RER
# On précise explicitement qu'on veut être à notre position et rotation (celle du spawner)
execute if entity @s[tag=ReservationReussie] at @s run function tch:rer/spawner/spawn/spawn

# On réinitialise notre tag de réservation
tag @s remove ReservationReussie
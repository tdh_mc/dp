# On est à la position d'une "Direction" et c'est elle qui exécute la fonction
# On affiche un message "Un train est en approche" à tous les voyageurs présents sur le quai

# On essaie de récupérer notre quai
function tch:tag/quai

# On tag tous les voyageurs présents sur le quai
execute positioned ~-15 ~ ~-15 run tag @a[dx=30,dy=5,dz=30] add JoueurSurLeQuai

# On affiche un message à tous ces joueurs
tellraw @a[tag=JoueurSurLeQuai,tag=!SpeakingToNPC] [{"text":"","color":"gray"},{"selector":"@e[tag=ourQuai]"},{"text":" → "},{"selector":"@s"},{"text":" : un train est en approche."}]

# On supprime les tags temporaires
tag @a[tag=JoueurSurLeQuai] remove JoueurSurLeQuai
tag @e[type=#tch:marqueur/quai,tag=ourQuai] remove ourQuai
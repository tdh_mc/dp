# On exécute cette fonction en tant qu’un spawner pour décaler l’horaire de spawn de 5 minutes
# (si le signal auquel on doit spawn est déjà rouge)
scoreboard players operation #spawnAt temp = #temps ticksPerHour
scoreboard players set #spawnAt temp2 12
scoreboard players operation #spawnAt temp /= #spawnAt temp2
scoreboard players operation @s spawnAt += #spawnAt temp
execute if score @s spawnAt >= #temps fullDayTicks run function tch:rer/spawner/ajuster_multiples/prochain_jour
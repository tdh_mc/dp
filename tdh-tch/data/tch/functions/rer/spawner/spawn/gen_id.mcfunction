# On génère un identifiant aléatoire
scoreboard players set #random min 100000
scoreboard players set #random max 999999
function tdh:random

scoreboard players operation #rer idVehicule = #random temp

# Tant que l'identifiant généré existe déjà, on recommence la fonction
execute as @e[type=armor_stand,tag=RERTete] if score @s idVehicule = #rer idVehicule run function tch:rer/spawner/spawn/gen_id
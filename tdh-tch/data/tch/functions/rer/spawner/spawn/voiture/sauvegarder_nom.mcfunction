# On enregistre le nom de notre voiture en fonction de notre ID line
# (à la position d'une item frame TexteTemporaire)
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.station.prefixe
data modify block ~ ~ ~ Text1 set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.line.terminus
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.station.nom
data modify block ~ ~ ~ Text2 set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.line.terminus
data modify block ~ ~ ~ Text3 set value '[{"entity":"@e[type=item_frame,tag=ourTerminusPC,limit=1]","nbt":"Item.tag.display.line.nom","interpret":"true"},{"text":" → ","color":"gray"},{"block":"~ ~ ~","nbt":"Text1","interpret":"true"},{"block":"~ ~ ~","nbt":"Text2","interpret":"true"}]'
data modify entity @s CustomName set from block ~ ~ ~ Text3

# On enregistre nos variables de voix et d'annonces sonores depuis notre PC :
scoreboard players operation @s voix = @e[type=item_frame,tag=ourPC,limit=1] voix
execute if score @e[type=item_frame,tag=ourPC,limit=1] voixMax matches 1.. run function tch:metro/spawner/spawn/num_voix
scoreboard players operation @s idTerminus = @e[type=item_frame,tag=ourPC,limit=1] idTerminus
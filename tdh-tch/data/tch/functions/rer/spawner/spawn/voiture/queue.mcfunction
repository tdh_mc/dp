# Cette fonction est exécutée par une entité temporaire à sa position et avec la rotation de l'armor stand SpawnRER d'origine

# On invoque d'abord l'avant de la voiture
summon armor_stand ^ ^ ^3.5 {Marker:1b,Invisible:1b,Invulnerable:1b,NoGravity:1b,Tags:["RER","RERQueue","RERParent","RERSpawnTemp"],HandItems:[{id:"minecraft:yellow_dye",Count:1b,tag:{CustomModelData:2}},{id:"minecraft:yellow_dye",Count:1b,tag:{CustomModelData:20}}],Pose:{LeftArm:[0f,0f,0f],RightArm:[0f,0f,0f]},DisabledSlots:16191}
# On invoque ensuite la seconde entité à l'arrière de la voiture
summon armor_stand ^ ^ ^-3.5 {Marker:1b,Invisible:1b,Invulnerable:1b,NoGravity:1b,Tags:["RER","RERQueueBack","RERChild","RERSpawnTemp"],HandItems:[{id:"minecraft:yellow_dye",Count:1b,tag:{CustomModelData:20}},{id:"minecraft:yellow_dye",Count:1b,tag:{CustomModelData:2}}],Pose:{LeftArm:[0f,0f,0f],RightArm:[0f,0f,0f]},DisabledSlots:16191}

# On donne les bons scores à ces entités
execute as @e[type=armor_stand,tag=RERSpawnTemp,distance=..20] positioned as @s run function tch:rer/spawner/spawn/voiture/sauvegarder_variables

# On invoque des passagers
# On va utiliser des variables aléatoires pour savoir qui invoquer et en quelle quantité
# On invoquera qqn à cette position si la variable aléatoire est inférieure à un certain seuil
# Par conséquent en heure de pointe on RÉDUIT la valeur du seuil pour AUGMENTER les chances de spawn
function tch:tag/pc
scoreboard players set #random min 0
scoreboard players set #random max 500
execute as @e[type=item_frame,tag=ourPC,limit=1] run function tch:rer/spawner/spawn/voiture/passagers/moduler_probabilite
# On exécute ensuite la fonction aléatoire à chaque position d'invocation possible
# Debout, collés au conducteur
execute positioned ^1 ^ ^-1.5 rotated ~180 0 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
execute positioned ^0 ^ ^-1.5 rotated ~180 0 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
execute positioned ^-1 ^ ^-1.5 rotated ~180 0 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
# Debout, collés aux sièges côté conducteur
execute positioned ^1 ^ ^-0.4 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
execute positioned ^-1 ^ ^-0.4 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
# Debout, collés aux sièges côté opposé
execute positioned ^1 ^ ^2.3 rotated ~180 0 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
execute positioned ^-1 ^ ^2.3 rotated ~180 0 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
# Debout, collés au côté opposé
execute positioned ^1 ^ ^3.5 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
execute positioned ^0 ^ ^3.5 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
execute positioned ^-1 ^ ^3.5 run function tch:rer/spawner/spawn/voiture/passagers/test_debout
# Assis
execute positioned ^0.8 ^ ^1.4 run function tch:rer/spawner/spawn/voiture/passagers/test_assis
execute positioned ^-0.8 ^ ^1.4 run function tch:rer/spawner/spawn/voiture/passagers/test_assis
execute positioned ^0.8 ^ ^0.6 rotated ~180 0 run function tch:rer/spawner/spawn/voiture/passagers/test_assis
execute positioned ^-0.8 ^ ^0.6 rotated ~180 0 run function tch:rer/spawner/spawn/voiture/passagers/test_assis

# On déplace l'entité temporaire de la longueur de la voiture (pour la motrice, 10)
tp @s ^ ^ ^-10

# Enfin, on incrémente l'id de voiture de 1 pour passer (éventuellement) à la voiture suivante
scoreboard players add #rer idVoiture 1
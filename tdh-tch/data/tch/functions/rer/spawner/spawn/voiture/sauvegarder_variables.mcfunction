# Cette fonction est appelée par toutes les entités composant le RER au moment de spawner,
# et permet à la fois de leur donner la bonne rotation et les bons scores idVéhicule/Voiture


# On se donne la bonne rotation
tp @s ~ ~ ~ ~ ~
# On se donne également les bons scores (idVehicule a été généré au préalable, et idVoiture est toujours 1 puisqu'on est la motrice de tête)
scoreboard players operation @s idVehicule = #rer idVehicule
scoreboard players operation @s idVoiture = #rer idVoiture
scoreboard players operation @s idLine = #rer idLine
scoreboard players operation @s prochaineStation = #rer prochaineStation
scoreboard players operation @s prochaineLigne = #rer prochaineLigne
# On donne une partie des scores uniquement si on est la voiture de tête
scoreboard players operation @s[tag=RERTete] vitesseMax = #rer vitesseMax
scoreboard players operation @s[tag=RERTete] vitesse = #rer vitesse
scoreboard players operation @s[tag=RERTete] idSignal = #rer idSignal
scoreboard players operation @s[tag=RERTete] prochainSignal = #rer prochainSignal

# On tag notre PC et PC de terminus pour récupérer les bonnes infos
function tch:tag/pc
function tch:tag/terminus_pc

# On se donne le nom correspondant à notre ligne
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/spawner/spawn/voiture/sauvegarder_nom

# On retire notre tag temporaire
tag @s remove RERSpawnTemp
tag @e[type=item_frame,tag=ourPC] remove ourPC
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
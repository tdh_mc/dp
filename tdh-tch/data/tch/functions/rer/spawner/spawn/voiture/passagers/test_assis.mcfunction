# On exécute cette fonction à la position et rotation à laquelle on souhaite invoquer une entité passagère d'un RER générée lors d'un spawn RER

# On a déjà défini les variables #random min et max aux valeurs nécessaires
# (max de 200 en heure creuse et 75-100 en heure pleine)
function tdh:random

# On invoque un passager aléatoire ou rien selon la variable temporaire générée
execute if score #random temp matches -1 run function tch:rer/spawner/spawn/voiture/passagers/spawn_derek
# On doit garder spawn_villageois à la fin car elle fait usage également de fonctions random
execute if score #random temp matches -1 run function tch:rer/spawner/spawn/voiture/passagers/spawn_villageois
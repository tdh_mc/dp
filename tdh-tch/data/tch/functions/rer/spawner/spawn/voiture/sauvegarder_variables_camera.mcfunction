# Cette fonction est appelée par toutes les entités composant le RER au moment de spawner,
# et permet à la fois de leur donner la bonne rotation et les bons scores idVéhicule/Voiture


# On se donne la bonne rotation
tp @s ~ ~ ~ ~ ~
# On se donne également les bons scores (idVehicule a été généré au préalable, et idVoiture est toujours 1 puisqu'on est la motrice de tête)
scoreboard players operation @s idVehicule = #rer idVehicule
scoreboard players operation @s idVoiture = #rer idVoiture
# On retire notre tag temporaire
tag @s remove RERCameraSpawnTemp
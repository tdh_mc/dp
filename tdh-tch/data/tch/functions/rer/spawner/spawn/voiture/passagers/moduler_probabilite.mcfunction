# On exécute cette fonction en tant qu'un PC de ligne,
# pour moduler la probabilité de spawn des passagers dans une rame de RER
# en fonction de notre statut (heure de pointe ou heure creuse)

# On n'a pas besoin de savoir pour quel RER on exécute l'opération car on modifie juste #random max
execute if score #temps currentTdhTick >= @s debutPointe1 if score #temps currentTdhTick <= @s finPointe1 run scoreboard players set #random max 200
execute if score #temps currentTdhTick >= @s debutPointe2 if score #temps currentTdhTick <= @s finPointe2 run scoreboard players set #random max 250
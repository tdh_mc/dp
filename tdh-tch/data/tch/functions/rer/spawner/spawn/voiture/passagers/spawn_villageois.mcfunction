# On invoque un villageois à la position et rotation d'exécution
summon villager ~ ~ ~ {Tags:["RERPassager","PassagerInvoque","RERPassagerTemp"],NoAI:1b,Invulnerable:1b,PersistenceRequired:0b,ActiveEffects:[{Id:11,Amplifier:10,Duration:999999,ShowParticles:0b}]}

# On donne un biome aléatoire à ce villageois
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
# ("plains" est implicite ci-dessous donc pas besoin de l'écrire)
#execute if score #random temp matches 0..19 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] VillagerData.type set value "minecraft:plains"
execute if score #random temp matches 20..34 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] VillagerData.type set value "minecraft:desert"
execute if score #random temp matches 35..49 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] VillagerData.type set value "minecraft:jungle"
execute if score #random temp matches 50..64 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] VillagerData.type set value "minecraft:savanna"
execute if score #random temp matches 65..79 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] VillagerData.type set value "minecraft:snow"
execute if score #random temp matches 80..89 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] VillagerData.type set value "minecraft:swamp"
execute if score #random temp matches 90.. run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] VillagerData.type set value "minecraft:taiga"

# On donne un prénom aléatoire à ce villageois
function tdh:random
execute if score #random temp matches ..2 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Antéric"'
execute if score #random temp matches 3..5 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Anthony"'
execute if score #random temp matches 6..8 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Bérénick"'
execute if score #random temp matches 9..10 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Bertrand"'
execute if score #random temp matches 11..13 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Christophe"'
execute if score #random temp matches 14..16 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Cyprien"'
execute if score #random temp matches 17..19 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Donald"'
execute if score #random temp matches 20..21 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Édouard"'
execute if score #random temp matches 22..24 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Ferdinand"'
execute if score #random temp matches 25..27 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Gérace"'
execute if score #random temp matches 28..30 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Gérald"'
execute if score #random temp matches 31..32 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Gontran"'
execute if score #random temp matches 33..35 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Gorgelet"'
execute if score #random temp matches 36..38 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Hector"'
execute if score #random temp matches 39..41 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Horace"'
execute if score #random temp matches 42..43 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Hubernard"'
execute if score #random temp matches 44..46 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Ismaël"'
execute if score #random temp matches 47..49 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Jérémy"'
execute if score #random temp matches 50..52 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Jérôme"'
execute if score #random temp matches 53..54 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Joël"'
execute if score #random temp matches 55..57 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Joseph"'
execute if score #random temp matches 58..60 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Joséphin"'
execute if score #random temp matches 61..63 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Landolf"'
execute if score #random temp matches 64..66 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Laurent"'
execute if score #random temp matches 67..69 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Ludovic"'
execute if score #random temp matches 70..72 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Mophort"'
execute if score #random temp matches 73..75 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Nathanaël"'
execute if score #random temp matches 76..78 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Patricien"'
execute if score #random temp matches 79..81 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Patrick"'
execute if score #random temp matches 82..84 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Philippe"'
execute if score #random temp matches 85..87 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Rétorick"'
execute if score #random temp matches 88..90 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Rodérick"'
execute if score #random temp matches 91..93 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Thomas"'
execute if score #random temp matches 94..96 run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Victor"'
execute if score #random temp matches 97.. run data modify entity @e[type=villager,tag=RERPassagerTemp,distance=..2,limit=1] CustomName set value '"Vulcain"'

# On repositionne correctement le villageois
tp @e[type=villager,tag=RERPassagerTemp,distance=..2] ~ ~ ~ ~ ~
execute as @e[type=villager,tag=RERPassagerTemp,distance=..2] run data modify entity @s PersistenceRequired set value 0b

# On enregistre les scores nécessaires
scoreboard players operation @e[type=villager,tag=RERPassagerTemp,distance=..2] idVehicule = #rer idVehicule
scoreboard players operation @e[type=villager,tag=RERPassagerTemp,distance=..2] idVoiture = #rer idVoiture

# On supprime le tag temporaire
tag @e[type=villager,tag=RERPassagerTemp,distance=..2] remove RERPassagerTemp
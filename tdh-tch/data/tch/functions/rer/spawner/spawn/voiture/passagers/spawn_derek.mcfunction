# On invoque un derek à la position et rotation d'exécution
summon wandering_trader ~ ~ ~ {Tags:["RERPassager","PassagerInvoque","RERPassagerTemp"],NoAI:1b,Invulnerable:1b,PersistenceRequired:0b,ActiveEffects:[{Id:11,Amplifier:10,Duration:999999,ShowParticles:0b}]}

# On repositionne correctement le derek
tp @e[type=wandering_trader,tag=RERPassagerTemp,distance=..2] ~ ~ ~ ~ ~
execute as @e[type=wandering_trader,tag=RERPassagerTemp,distance=..2] run data modify entity @s PersistenceRequired set value 0b

# On enregistre les scores nécessaires
scoreboard players operation @e[type=wandering_trader,tag=RERPassagerTemp,distance=..2] idVehicule = #rer idVehicule
scoreboard players operation @e[type=wandering_trader,tag=RERPassagerTemp,distance=..2] idVoiture = #rer idVoiture

# On supprime le tag temporaire
tag @e[type=wandering_trader,tag=RERPassagerTemp,distance=..2] remove RERPassagerTemp
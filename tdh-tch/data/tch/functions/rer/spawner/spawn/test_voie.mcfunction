## TEST DE SPAWN PARTIE 3 :
## On vérifie si des joueurs sont présents à quai.

# Le tag temporaire "ourPC" est déjà enregistré par la partie 2 des tests (test_ligne).
# On essaie de récupérer notre voie (*Direction*)
function tch:tag/voie

# On supprime le tag de notre voie (et donc on ne spawne pas) SI :
# - Il y a des trains en file d'attente (score FileAttente > 0)
#execute as @e[type=armor_stand,tag=ourVoie,scores={FileAttente=1..}] run tag @s remove ourVoie
# - Il n'y a pas de joueur dans un rayon de 75 blocs
execute as @e[type=armor_stand,tag=ourVoie,distance=..200] at @s unless entity @p[distance=..75] run tag @s remove ourVoie

# Si la voie est réservée, on vérifie si c'est une réservation fantôme
#execute as @e[type=armor_stand,tag=ourVoie,tag=VoieReservee] at @s run function tch:rer/spawner/spawn/test_reservation_fantome

# Si on n’a pas trouvé de quai (ou qu’on l’a retiré en raison des checks ci-dessus),
# on réinitialise notre horaire de spawn pour en générer un nouveau
execute unless entity @e[type=armor_stand,tag=ourVoie,distance=..200,limit=1] run scoreboard players reset @s spawnAt

# Si on a toujours notre voie, on exécute la suite des tests
execute if entity @e[type=armor_stand,tag=ourVoie,distance=..200,limit=1] run function tch:rer/spawner/spawn/test_signal

# On réinitialise le tag de notre voie
tag @e[type=armor_stand,tag=ourVoie,distance=..200] remove ourVoie
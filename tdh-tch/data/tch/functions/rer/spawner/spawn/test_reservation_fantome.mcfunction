# On exécute cette fonction en tant qu'une voie qui a le tag VoieReservee, à sa position,
# pour vérifier si elle est bel et bien réservée ou si c'est un RER ayant disparu qui l'a volée

scoreboard players operation #rer idLine = @s idLine
scoreboard players operation #rer idLineMax = @s idLineMax
scoreboard players set #rer temp 1

tellraw @a[tag=RERDebugLog] [{"text":"Test de réservation de la voie ","color":"gray"},{"selector":"@s"},{"text":" d'IDline "},{"score":{"name":"#rer","objective":"idLine"},"color":"yellow","extra":[{"text":"—"},{"score":{"name":"#rer","objective":"idLineMax"}}]}]

execute as @e[type=armor_stand,tag=RERTete,tag=VoieReservee,distance=..200] if score @s idLine = #rer idLine run scoreboard players set #rer temp 0
execute as @e[type=armor_stand,tag=RERTete,tag=VoieReservee,distance=..200] if score @s idLine >= #rer idLine if score @s idLine <= #rer idLineMax run scoreboard players set #rer temp 0

execute if entity @p[tag=RERDebugLog] as @e[type=armor_stand,tag=RERTete,tag=VoieReservee,distance=..200] if score @s idLine = #rer idLine run tellraw @a[tag=RERDebugLog] [{"text":"Le RER ","color":"gray"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":" "},{"selector":"@s"},{"text":" correspond à la réservation."}]
execute if entity @p[tag=RERDebugLog] as @e[type=armor_stand,tag=RERTete,tag=VoieReservee,distance=..200] if score @s idLine >= #rer idLine if score @s idLine <= #rer idLineMax run tellraw @a[tag=RERDebugLog] [{"text":"Le RER ","color":"gray"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":" "},{"selector":"@s"},{"text":" correspond à la réservation."}]

# Si c'est une réservation fantôme, on supprime le tag de réservation ;
execute if score #rer temp matches 1 run tag @s remove VoieReservee
# Sinon, on supprime le tag ourVoie pour signaler qu'on ne peut pas spawner
execute unless score #rer temp matches 1 run tag @s remove ourVoie

execute as @s[tag=ourVoie] run tellraw @a[tag=RERDebugLog] [{"text":"La voie a été déréservée : aucun RER correspondant trouvé.","color":"gray"}]
execute as @s[tag=VoieReservee] run tellraw @a[tag=RERDebugLog] [{"text":"La voie est toujours réservée.","color":"gray"}]
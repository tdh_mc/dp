## SPAWN (après tous les tests)

# On enregistre l'ID line, l'ID station, les vitesses et les différentes autres variables correspondant à ce spawner
scoreboard players operation #rer idLine = @s idLine
scoreboard players operation #rer prochaineStation = @s idStation
scoreboard players operation #rer prochaineLigne = @s prochaineLigne
scoreboard players operation #rer idSignal = @s idSignal
scoreboard players operation #rer prochainSignal = @s prochainSignal
scoreboard players operation #rer vitesseMax = @s vitesseMax
scoreboard players operation #rer vitesse = @s vitesse
execute unless score #rer vitesse matches 1.. run scoreboard players operation #rer vitesse = #rer vitesseMax

# On invoque une entité temporaire qui servira à matérialiser la position de chaque nouvelle voiture à ajouter au train
summon item ~ ~-5 ~ {Item:{id:"iron_nugget",Count:1b},Tags:["RERSpawnerItem"],Age:5999s,PickupDelay:32767s,NoGravity:1b,Invulnerable:1b}

# On invoque toutes les voitures en tant que cette entité, pour pouvoir déplacer facilement le point de référence sans répéter 10 fois de suite le même sélecteur
execute as @e[type=item,tag=RERSpawnerItem,distance=..10,limit=1] positioned as @s run function tch:rer/spawner/spawn/spawn_entites

# Une fois l'invocation terminée, on supprime l'entité temporaire
kill @e[type=item,tag=RERSpawnerItem,distance=..10]

# On réserve la voie
#tag @e[type=armor_stand,tag=ourVoie] add VoieReservee
#tag @e[type=armor_stand,tag=RERTete,distance=..10,limit=1] add VoieReservee

tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"#rer","objective":"idVehicule"}},{"text":"]"},{"text":" : Invocation ","color":"gray","extra":[{"selector":"@s"},{"text":"."}]}]


# On joue le message de PID
function tch:station/annonce/pid/gen_id
# On bloque le temps d'attente affiché
function tch:station/pid/lock_0
# On affiche le message aux joueurs présents sur le quai
#execute as @e[type=armor_stand,tag=ourVoie,limit=1] at @s run function tch:rer/spawner/spawn/message_quai
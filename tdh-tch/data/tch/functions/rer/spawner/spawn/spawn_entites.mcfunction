# On invoque toutes les entités correspondant aux voitures du train
# On exécute cette fonction en tant qu'une entité temporaire déplaçable, à notre position mais avec la rotation de l'ORIGINE (l'armor stand SpawnRER)

# On génère un ID pour le nouveau véhicule. Il doit être unique et sera partagé par toutes les entités du train
function tch:rer/spawner/spawn/gen_id
# Une fois la fonction terminée, la variable #rer idVehicule contient l'identifiant unique généré

# On initialise l'ID de voiture (qui commence toujours à 1)
scoreboard players set #rer idVoiture 1

# Contrairement à ce qu'on pourrait croire, les appels successifs à "execute positioned as @s" ne sont pas inutiles,
# puisqu'après chaque exécution de fonction, notre position est déplacée de la longueur de la voiture que l'on vient d'invoquer

# On invoque une caméra, qui se trouve au départ au même endroit que l'avant
# (osef car ça ne sera *jamais* vu, le temps en station étant de 15 secondes et le loop du changement de position des caméras toutes les 8 secondes...)
summon armor_stand ^ ^ ^3.5 {Invisible:1b,Invulnerable:1b,NoGravity:1b,Tags:["RERCamera","NoUpdate","RERCameraSpawnTemp"],CustomName:'"Caméra du \\u1a38"'}
execute as @e[type=armor_stand,tag=RERCameraSpawnTemp,distance=..10] positioned as @s run function tch:rer/spawner/spawn/voiture/sauvegarder_variables_camera

# On invoque la voiture de tête
execute positioned as @s run function tch:rer/spawner/spawn/voiture/tete
# TODO quand il y aura le modèle : Mettre les voitures intermédiaires
# On invoque la voiture de queue
execute positioned as @s run function tch:rer/spawner/spawn/voiture/queue
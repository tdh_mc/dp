# On exécute cette fonction en tant qu'un spawner RER qui vient de générer son horaire de spawn,
# à la position d'un autre spawner proche qui a déjà un horaire de spawn,
# dans le but de prolonger l'attente pour le nôtre si les deux horaires risquent de se télescoper

# On enregistre notre horaire de spawn dans une variable temporaire
scoreboard players operation #rer spawnAt = @e[type=armor_stand,tag=SpawnRER,distance=..10,sort=nearest,limit=1] spawnAt

# On mesure la différence entre les 2 horaires de spawn
scoreboard players operation #rer spawnAt -= @s spawnAt

# Si elle est de moins de 400 ticks, on la prolonge
scoreboard players add #rer spawnAt 550
execute if score #rer spawnAt matches 0..550 run scoreboard players operation @s spawnAt += #rer spawnAt
execute if score @s spawnAt >= #temps fullDayTicks run function tch:rer/spawner/ajuster_multiples/prochain_jour

# TICK DES SPAWNERS RER
# Cette fonction est exécutée régulièrement par chacun des spawners du RER
# pour générer leurs horaires de spawn et éventuellement leurs RER

# Si on a une heure de spawn prévue, on vérifie si c'est l'heure
execute as @s[scores={spawnAt=0..}] run function tch:rer/spawner/spawn/test_horaire

# Si on a pas d'heure de spawn prévue, on génère un nouvel horaire
execute unless score @s spawnAt matches 0.. run function tch:ligne/horaire/gen/test

# Si on a une heure de spawn prévue mais qu'il existe un autre spawner RER proche ayant lui aussi une heure de spawn prévue, on vérifie l'écart entre ces deux horaires de spawn et on l'augmente le cas échéant
execute if score @s spawnAt matches 0.. at @e[type=armor_stand,tag=SpawnRER,distance=0.01..5,scores={spawnAt=0..}] run function tch:rer/spawner/ajuster_multiples
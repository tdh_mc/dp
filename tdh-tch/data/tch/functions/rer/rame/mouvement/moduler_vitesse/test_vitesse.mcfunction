# On exécute cette fonction en tant qu'une motrice (de tête) d'un RER,
# si notre score "vitesse" est inférieur à la vitesse maximale actuelle,
# pour éventuellement accélérer ou ralentir

# On a déjà vérifié qu'on avait pas changé la vitesse trop récemment,
# donc il nous suffit de choisir une des 2 fonctions "accélérer" ou "ralentir"
execute if score @s vitesse < @s vitesseMax run function tch:rer/rame/mouvement/moduler_vitesse/accelerer
execute if score @s vitesse > @s vitesseMax run function tch:rer/rame/mouvement/moduler_vitesse/ralentir
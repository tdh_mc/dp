scoreboard players add @s vitesse 1

# Si on vient d'atteindre 6m/s, on passe tous les joueurs passagers de ce véhicule en mode caméra
execute if score @s vitesse matches 6 run function tch:rer/rame/passager/vers_camera

# On définit le temps d'attente avant d'accélérer à nouveau
# (1 seconde car on "copie" le MI09 qui a une accélération de ~1m/s²)
# (on rajoute 1 tick car on va forcément décrémenter de 1 dans la foulée, cf fonction moduler_vitesse)
scoreboard players set @s remainingTicks 21
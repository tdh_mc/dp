scoreboard players remove @s vitesse 1

# Si on vient de descendre sous 6m/s, on passe tous les joueurs passagers de ce véhicule en mode intérieur
execute if score @s vitesse matches 5 run function tch:rer/rame/passager/vers_inside

# On définit le temps d'attente avant de ralentir à nouveau
# (on part du principe qu'on peut freiner + vite qu'on n'accélère parce que bon après les arrivées station...)
# (on rajoute 1 tick car on va forcément décrémenter de 1 dans la foulée, cf fonction moduler_vitesse)
scoreboard players set @s[tag=!ArretUrgence] remainingTicks 11
# On ralentit plus vite si on est en train de faire un arrêt d’urgence
scoreboard players set @s[tag=ArretUrgence] remainingTicks 6

# Si on est en freinage d’urgence et que la vitesse vient d’atteindre 0, on s’arrête
execute if score @s[tag=ArretUrgence] vitesse matches ..0 run function tch:rer/rame/arret
# Cette fonction permet de déplacer la voiture de RER appelante et son contenu à une vitesse définie
# Cette vitesse est déjà définie dans #rer vitesse,
# ainsi que l'ID véhicule du train concerné dans #rer idVehicule et l’ID de la voiture dans #rer idVoiture

# Vitesse : 19 blocs par seconde (0.95 bloc par tick)

# On déplace tous les passagers de notre voiture
execute as @e[type=#tch:passager_rer,tag=OurPassager,distance=..10] positioned as @s run tp @s ^ ^ ^0.95
# On déplace les entités composant le RER
execute as @e[type=armor_stand,tag=OurRER,distance=..10] if score @s idVoiture = #rer idVoiture positioned as @s run tp @s ^ ^ ^0.95

# On déplace les caméras selon la voiture à laquelle elles appartiennent
execute as @e[type=armor_stand,tag=OurCamera,tag=!FixedCamera,distance=..150] positioned as @s run tp @s ^ ^ ^0.95

# Ensuite, chaque entité PARENTE (l'avant de chaque voiture) détectant une armor stand "TraceRER" à une distance inférieure à celle dont elle vient de se déplacer, se téléporte automatiquement à sa position et "prend en compte" cette étape du tracé en copiant sa rotation
# On prend une marge de sécurité sur la distance pour éviter de sauter un tick trop régulièrement
execute at @s run function tch:rer/rame/mouvement/voiture/test_trace/19
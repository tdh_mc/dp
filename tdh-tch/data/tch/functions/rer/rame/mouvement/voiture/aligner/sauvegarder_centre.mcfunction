# Cette fonction est exécutée par une armor stand "tête" de n'importe quelle voiture contenant des passagers ; on souhaite sauvegarder la position du centre (de la voiture) comme centre (de rotation) desdits passagers

# On commence par sauvegarder la position de la tête
execute store result score #rer_rotpass_centre posX run data get entity @s Pos[0] 100
execute store result score #rer_rotpass_centre posY run data get entity @s Pos[1] 100
execute store result score #rer_rotpass_centre posZ run data get entity @s Pos[2] 100
# On récupère ensuite la position de la queue
execute as @e[type=armor_stand,tag=OurRER,tag=RERChild,distance=..10] if score @s idVoiture = #rer idVoiture run function tch:rer/rame/mouvement/voiture/aligner/sauvegarder_queue

# On fait la moyenne des deux positions pour obtenir celle du centre
scoreboard players operation #rer_rotpass_centre posX += #rer_rotpass_centre deltaX
scoreboard players operation #rer_rotpass_centre posY += #rer_rotpass_centre deltaY
scoreboard players operation #rer_rotpass_centre posZ += #rer_rotpass_centre deltaZ
scoreboard players set #rer_rotpass_centre temp 2
scoreboard players operation #rer_rotpass_centre posX /= #rer_rotpass_centre temp
scoreboard players operation #rer_rotpass_centre posY /= #rer_rotpass_centre temp
scoreboard players operation #rer_rotpass_centre posZ /= #rer_rotpass_centre temp
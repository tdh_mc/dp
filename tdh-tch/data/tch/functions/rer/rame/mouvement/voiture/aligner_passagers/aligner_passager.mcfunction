# On exécute cette fonction en tant qu'un passager d'une voiture de RER,
# à la position de la "tête" de sa voiture,
# pour repositionner le passager suite à un alignement de la voiture

# Inputs :
# #rer_rotpass deltaX/Y/Z contiennent le delta de positionnement réalisé à ce tick

#execute positioned ^ ^ ^3.5 run tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"#rer","objective":"idVehicule"}},{"text":"/"},{"score":{"name":"#rer","objective":"idVoiture"}},{"text":"]"},{"text":" : Alignement passager "},{"selector":"@s"}]

# On assigne simplement les bonne positions au passager après avoir calculé le delta
execute store result score #rer_rotpass posX run data get entity @s Pos[0] 100
execute store result score #rer_rotpass posY run data get entity @s Pos[1] 100
execute store result score #rer_rotpass posZ run data get entity @s Pos[2] 100
execute store result entity @s Pos[0] double 0.01 run scoreboard players operation #rer_rotpass posX += #rer_rotpass deltaX
execute store result entity @s Pos[1] double 0.01 run scoreboard players operation #rer_rotpass posY += #rer_rotpass deltaY
execute store result entity @s Pos[2] double 0.01 run scoreboard players operation #rer_rotpass posZ += #rer_rotpass deltaZ
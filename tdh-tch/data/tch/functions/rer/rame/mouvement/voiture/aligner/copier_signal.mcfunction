# Cette fonction doit être exécutée en tant qu'une armor stand TraceRER étant également un Signal,
# pour copier dans le scoreboard ses ID de signaux

# On copie les identifiants de signal
scoreboard players operation #rer idSignal = @s idSignal
scoreboard players operation #rer prochainSignal = @s prochainSignal

# Selon le statut actuel du signal, on modifie les infos précédentes
# Si le signal est rouge, on envoie une vitesseMax de -1 (freinage d’urgence)
execute if score @s status matches 2.. run scoreboard players set #rer vitesseMax -1
# Si le signal est orange, on circule à une vitesseMax réduite (10 m/s maximum, ou 5 m/s max en zone <10 m/s)
execute if score @s status matches 1 if score #rer vitesseMax matches 6..10 run scoreboard players set #rer vitesseMax 5
execute if score @s status matches 1 if score #rer vitesseMax matches 11.. run scoreboard players set #rer vitesseMax 10

# On affiche des infos de debug
tellraw @a[tag=SignalDebugLog] [{"text":"[Signal ","color":"#ff6969"},{"score":{"name":"@s","objective":"idSignal"}},{"text":"→"},{"score":{"name":"@s","objective":"prochainSignal"}},{"text":"]"},{"text":" : Déclenchement par le RER ","color":"gray","extra":[{"selector":"@e[type=armor_stand,tag=RERTete,sort=nearest,limit=1]"},{"text":" d’ID "},{"score":{"name":"#rer","objective":"idVehicule"}},{"text":". Passage à l’état "},{"text":"ROUGE","color":"red"},{"text":"."}]}]
execute if score @s status matches 2.. run tellraw @a[tag=SignalDebugLog] [{"text":"Le signal était déjà ","color":"gray"},{"text":"ROUGE","color":"red"},{"text":", déclenchement du "},{"text":"freinage d’urgence","color":"red"},{"text":"."}]
execute if score @s status matches 1 run tellraw @a[tag=SignalDebugLog] [{"text":"Le signal était ","color":"gray"},{"text":"ORANGE","color":"gold"},{"text":", on circule désormais à vitesse réduite ("},{"score":{"name":"#rer","objective":"vitesseMax"},"color":"red","extra":[{"text":" m/s"}]},{"text":")."}]

# On passe le signal au rouge dans tous les cas
scoreboard players set #signal status 2
function tch:signal/reserver/actuel
tag @s remove ReservationReussie
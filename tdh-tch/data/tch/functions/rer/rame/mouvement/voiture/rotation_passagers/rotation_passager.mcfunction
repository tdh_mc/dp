# On exécute cette fonction en tant qu'un passager d'une voiture de RER,
# à la position de la "tête" de sa voiture,
# pour repositionner le passager suite à une rotation de la voiture

# Inputs :
# #sin temp et #cos temp contiennent le sin et le cos de la rotation à réaliser
# #rer_rotpass rotation contient la rotation réalisée
# #rer_rotpass deltaX/Y/Z contiennent le delta de positionnement réalisé à ce tick

#execute positioned ^ ^ ^3.5 run tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"#rer","objective":"idVehicule"}},{"text":"/"},{"score":{"name":"#rer","objective":"idVoiture"}},{"text":"]"},{"text":" : Rotation passager "},{"selector":"@s"}]

# On enregistre la position actuelle du passager à déplacer
execute store result score #rer_rotpass posX run data get entity @s Pos[0] 100
execute store result score #rer_rotpass posY run data get entity @s Pos[1] 100
execute store result score #rer_rotpass posZ run data get entity @s Pos[2] 100
scoreboard players operation #rer_rotpass posX += #rer_rotpass deltaX
scoreboard players operation #rer_rotpass posZ += #rer_rotpass deltaZ

# On assigne simplement la bonne position Y au passager car on ne la rotate pas ci dessous
execute store result entity @s Pos[1] double 0.01 run scoreboard players operation #rer_rotpass posY += #rer_rotpass deltaY

# La formule pour la rotation est :
# (avec x1 = position finale, x0 = position initiale et xC = position du centre)
# x1 = (x0 - xC) * cos(R°) - (z0 - zC) * sin(R°) + xC
# z1 = (x0 - xC) * sin(R°) + (z0 - zC) * cos(R°) + zC

# Le cos/sin sont multipliés par 1000
# On garde donc ce facteur dans un coin
scoreboard players set #rer_rotpass temp3 1000

# On override x0 et z0 (#rer_rotpass posX et posZ) par (x0-xC) et (z0-zC)
# En effet on n'aura plus besoin de la position initiale
scoreboard players operation #rer_rotpass posX -= #rer_rotpass_centre posX
scoreboard players operation #rer_rotpass posZ -= #rer_rotpass_centre posZ

# x1 = (x0 - xC) * cos(R°) - (z0 - zC) * sin(R°) + xC
#	 = 		posX * cos	   - 	  posZ * sin	 + xC
scoreboard players operation #rer_rotpass temp = #rer_rotpass posX
scoreboard players operation #rer_rotpass temp *= #cos temp
scoreboard players operation #rer_rotpass temp /= #rer_rotpass temp3

scoreboard players operation #rer_rotpass temp2 = #rer_rotpass posZ
scoreboard players operation #rer_rotpass temp2 *= #sin temp
scoreboard players operation #rer_rotpass temp2 /= #rer_rotpass temp3

scoreboard players operation #rer_rotpass temp -= #rer_rotpass temp2
execute store result entity @s Pos[0] double 0.01 run scoreboard players operation #rer_rotpass temp += #rer_rotpass_centre posX
# La dernière opération assigne automatiquement la bonne position X au passager

# z1 = (x0 - xC) * sin(R°) + (z0 - zC) * cos(R°) + zC
#	 = 		posX * sin	   + 	  posZ * cos	 + zC
scoreboard players operation #rer_rotpass temp = #rer_rotpass posX
scoreboard players operation #rer_rotpass temp *= #sin temp
scoreboard players operation #rer_rotpass temp /= #rer_rotpass temp3

scoreboard players operation #rer_rotpass temp2 = #rer_rotpass posZ
scoreboard players operation #rer_rotpass temp2 *= #cos temp
scoreboard players operation #rer_rotpass temp2 /= #rer_rotpass temp3

scoreboard players operation #rer_rotpass temp += #rer_rotpass temp2
execute store result entity @s Pos[2] double 0.01 run scoreboard players operation #rer_rotpass temp += #rer_rotpass_centre posZ
# La dernière opération assigne automatiquement la bonne position Z au passager

# Enfin, on modifie la rotation du passager
execute store result score #rer_rotpass temp run data get entity @s Rotation[0] 10
execute store result entity @s Rotation[0] float 0.1 run scoreboard players operation #rer_rotpass temp += #rer_rotpass rotation
# On exécute cette fonction en tant qu’une voiture de tête de RER qui vient de rencontrer un tracé ayant le tag ClearCamera
# On doit donc modifier les données de la caméra de ce RER pour ne plus la forcer à utiliser une position existante

# On trouve la caméra de notre RER et on la libère
execute as @e[type=armor_stand,tag=RERCamera,distance=..150] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/mouvement/voiture/aligner/clear_camera/liberer
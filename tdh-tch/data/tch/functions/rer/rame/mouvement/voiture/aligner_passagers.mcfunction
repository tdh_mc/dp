# On exécute cette fonction en tant qu'une voiture du RER que l'on vient de réaligner à la position précise d'exécution de la fonction, et dont on souhaite déplacer les passagers pour que leur position relative à la voiture reste la même

tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"/"},{"score":{"name":"@s","objective":"idVoiture"}},{"text":"]"},{"text":" : Alignement passagers. Delta "},{"score":{"name":"#rer_rotpass","objective":"deltaX"}},{"text":";"},{"score":{"name":"#rer_rotpass","objective":"deltaY"}},{"text":";"},{"score":{"name":"#rer_rotpass","objective":"deltaZ"}}]

# On exécute ensuite la fonction d'alignement pour chaque passager
execute as @e[type=#tch:passager_rer,tag=OurPassager,distance=..10] run function tch:rer/rame/mouvement/voiture/aligner_passagers/aligner_passager
# On l'exécute également pour toutes les caméras (sauf si elles sont fixées à une position spécifique)
execute as @e[type=armor_stand,tag=OurCamera,tag=!FixedCamera,distance=..150] run function tch:rer/rame/mouvement/voiture/aligner_passagers/aligner_passager
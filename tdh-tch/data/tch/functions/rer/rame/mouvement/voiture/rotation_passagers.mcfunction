# On exécute cette fonction en tant qu'une voiture du RER que l'on vient de réaligner aux position et rotation précises d'exécution de la fonction, et dont on souhaite déplacer les passagers pour que leur position relative à la voiture reste la même

# Comme ce sont des opérations lourdes, on calcule une seule fois le cosinus et le sinus de la différence de rotation
# On commence par calculer cette différence de rotation, et par prendre son modulo (ex: si on était à 150° et qu'on est désormais à -165°, la différence serait de 315° alors qu'on a fait qu'un 8e de tour)
scoreboard players operation #sin temp = #rer_rotpass rotationMax
scoreboard players operation #sin temp -= #rer_rotpass rotation
scoreboard players set #cos temp 3600
scoreboard players operation #sin temp %= #cos temp
execute if score #sin temp matches 1801.. run scoreboard players remove #sin temp 3600
# On copie la valeur calculée dans les 2 autres variables qui doivent contenir cette valeur
# (#sin et #cos doivent la contenir pour pouvoir calculer leur sin/cos et #rer_rotpass pour pouvoir faire tourner les passagers dans la fonction suivante)
scoreboard players operation #rer_rotpass rotation = #sin temp
scoreboard players operation #cos temp = #sin temp
# Enfin, on calcule le sin et le cos (décimaux)
function tdh:math/sind/1000
function tdh:math/cosd/1000

# On enregistre aussi la position d'exécution de la fonction,
# (le "centre" de la rotation) qui est la position du milieu de la voiture
#execute positioned ^ ^ ^3.5 as @e[type=armor_stand,tag=RERParent,distance=..1,sort=nearest,limit=1] run function tch:rer/rame/mouvement/voiture/rotation_passagers/sauvegarder_centre
# Désactivé car déjà fait dans aligner2

execute positioned ^ ^ ^3.5 run tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"/"},{"score":{"name":"@s","objective":"idVoiture"}},{"text":"]"},{"text":" : Rotation passagers ","color":"gray","extra":[{"score":{"name":"#rer_rotpass","objective":"rotation"}},{"text":"° autour de l'armor stand "},{"selector":"@e[type=armor_stand,tag=RERParent,distance=..1,sort=nearest,limit=1]"},{"text":" de tags "},{"entity":"@e[type=armor_stand,tag=RERParent,distance=..1,sort=nearest,limit=1]","nbt":"Tags"},{"text":". Coordonnées "},{"score":{"name":"#rer_rotpass_centre","objective":"posX"}},{"text":";"},{"score":{"name":"#rer_rotpass_centre","objective":"posZ"}}]}]

# On exécute ensuite la fonction de rotation pour chaque passager
execute as @e[type=#tch:passager_rer,tag=OurPassager,distance=..10] run function tch:rer/rame/mouvement/voiture/rotation_passagers/rotation_passager
# On l'exécute également pour toutes les caméras (sauf si elles sont fixées à une position spécifique)
execute as @e[type=armor_stand,tag=OurCamera,tag=!FixedCamera,distance=..150] run function tch:rer/rame/mouvement/voiture/rotation_passagers/rotation_passager
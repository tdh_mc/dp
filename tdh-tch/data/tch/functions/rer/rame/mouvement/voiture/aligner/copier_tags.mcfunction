# Cette fonction doit être exécutée en tant qu'une armor stand TraceRER,
# pour copier dans le scoreboard toutes ses données numéraires

# On enregistre la vitesse maximale autorisée
scoreboard players operation #rer vitesseMax = @s vitesseMax
# On enregistre l'ID station si on a un tag prochainestation
scoreboard players operation #rer prochaineStation = @s[tag=ProchaineStation] idStation
# On enregistre les ticks restants si on a le tag correspondant
scoreboard players operation #rer remainingTicks = @s[tag=Direction] remainingTicks
# On enregistre la prochaine ligne si on a un tag prochaineLigne
execute if entity @s[tag=ProchaineLigne] run function tch:rer/rame/mouvement/voiture/aligner/copier_ligne
# On enregistre l’ID de caméra si on a un tag ForceCamera
scoreboard players operation #rer idCamera = @s[tag=ForceCamera] idCamera
# Si on a un tag ClearCamera, on définit idCamera à -1
execute if entity @s[tag=ClearCamera] run scoreboard players set #rer idCamera -1
# On enregistre les informations de signal si on est un signal,
# sauf s’il s’agit du signal dans lequel on se trouve déjà
execute if entity @s[tag=Signal] unless score @s idSignal = #rer idSignal run function tch:rer/rame/mouvement/voiture/aligner/copier_signal

# On enregistre une variable STATUS différente selon nos tags (par défaut 0)
# Selon notre statut :
# - PreStation = 50
execute as @s[tag=PreStation] run scoreboard players set #rer status 50
# - Direction (Station) = 100
execute as @s[tag=Direction] run scoreboard players set #rer status 100
# - ProchaineStation = 150 (Osef de ce tag donc on ne l'enregistre pas)
#execute as @s[tag=ProchaineStation] run scoreboard players set #rer status 150
# - Retournement = 200
# (on l’ajoute, comme ça si on a Direction en même temps, ça fait un retournement en station = 300)
execute as @s[tag=Retournement] run scoreboard players add #rer status 200
# - Detruire = 1000
execute as @s[tag=Detruire] run scoreboard players set #rer status 1000

# On ajoute DescenteDroite car il peut être associé à d'autres
# Mais il ne sera pris en compte que s'il est associé à PreStation ou rien du tout pour l'instant
execute as @s[tag=DescenteDroite] run scoreboard players add #rer status 10

# On enregistre pas NoAlign car on ne s'en sert pas du tout au même endroit
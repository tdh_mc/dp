# On exécute cette fonction en tant qu'une voiture du RER que l'on souhaite réaligner aux position et rotation précises d'exécution de la fonction
# On doit avoir déjà taggé l'armor stand TraceRER qu'on veut prendre en compte avec le tag OurTraceRER

# Pour des raisons pratiques les armor stands se trouvent proches des extrémités des voitures, mais par contre la position de réalignement (à laquelle on détecte les armor stands TraceRER) est au CENTRE de la voiture pour éviter de faire partir les passagers dans le décor (et leur faire prendre des dégâts...)
# A ce stade j'ai hardcodé la valeur d'écartement (le traceRER sera détecté 3.5 blocs en arrière) mais à terme il faudra faire ça proprement et avoir une variable qui... varie selon les voitures. (TODO)
# La position d'exécution de cette fonction (ou de aligner2) ne doit par conséquent pas être modifiée par un at ou un positioned as

# On a déjà enregistré les variables #rer idVehicule et idVoiture
# on enregistre également la variable idLine et prochaineLigne
scoreboard players operation #rer idLine = @s idLine
scoreboard players operation #rer prochaineLigne = @s prochaineLigne

# On tente d'enregistrer l'ID line et l'ID line max du tracé et on l’ignore s’il existe et qu’on n’a pas le même
execute as @e[type=armor_stand,tag=OurTraceRER,distance=..15,limit=1] run function tch:rer/rame/mouvement/voiture/aligner/sauvegarder_id_line
execute unless score #rer temp matches 1.. run function tch:rer/rame/mouvement/voiture/aligner2
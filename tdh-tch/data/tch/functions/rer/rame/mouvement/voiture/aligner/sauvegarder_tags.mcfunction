# Cette fonction est exécutée par la voiture de tête d'un train pour récupérer la vitesse d'un point de contrôle que l'on vient de rencontrer
# Ce point de contrôle doit avoir le tag OurTraceRER

# On réinitialise tous les scores donnés par la fonction ci-après
scoreboard players set #rer vitesseMax 0
scoreboard players set #rer prochaineStation 0
scoreboard players set #rer prochaineLigne 0
scoreboard players set #rer remainingTicks 0
scoreboard players set #rer status 0
scoreboard players set #rer idCamera 0
# On copie nos ID de signaux actuels
scoreboard players operation #rer idSignal = @s idSignal
scoreboard players operation #rer prochainSignal = @s prochainSignal

# On exécute la première partie de la copie en tant que l'armor stand OurTraceRER pour éviter de multiplier les sélecteurs
execute as @e[type=armor_stand,tag=OurTraceRER,distance=..15,limit=1] run function tch:rer/rame/mouvement/voiture/aligner/copier_tags

# Si les variables enregistrables ont une valeur >0, on les prend en compte :
execute if score #rer vitesseMax matches 1.. run scoreboard players operation @s vitesseMax = #rer vitesseMax
execute if score #rer vitesseMax matches ..-1 run function tch:rer/rame/mouvement/arret_urgence
execute if score #rer prochaineStation matches 1.. run scoreboard players operation @s prochaineStation = #rer prochaineStation
execute if score #rer prochaineLigne matches 1.. run scoreboard players operation @e[type=armor_stand,tag=OurRER,distance=..50] prochaineLigne = #rer prochaineLigne
execute if score #rer remainingTicks matches 1.. run scoreboard players operation @s remainingTicks = #rer remainingTicks
execute if score #rer idSignal matches 1.. run scoreboard players operation @s idSignal = #rer idSignal
execute if score #rer prochainSignal matches 1.. run scoreboard players operation @s prochainSignal = #rer prochainSignal
# Si on a un ID de caméra, on transmet l’information à notre caméra
execute if score #rer idCamera matches 1.. run function tch:rer/rame/mouvement/voiture/aligner/forcer_camera
execute if score #rer idCamera matches ..-1 run function tch:rer/rame/mouvement/voiture/aligner/clear_camera

# Selon la valeur du statut, on se donne les bons tags
execute if score #rer status matches 50..99 run tag @s add PreStation
execute if score #rer status matches 10..99 unless score #rer status matches 50..59 run tag @s add DescenteDroite
execute if score #rer status matches 200..299 run function tch:rer/rame/mouvement/retournement
execute if score #rer status matches 300..399 run tag @s add RetournementPrevu
execute if score #rer status matches 100..999 unless score #rer status matches 200..299 positioned ^ ^ ^3.5 run function tch:rer/rame/en_station/arrivee
execute if score #rer status matches 1000.. run function tch:rer/rame/detruire
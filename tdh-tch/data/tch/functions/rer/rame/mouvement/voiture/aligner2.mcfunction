
tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"/"},{"score":{"name":"@s","objective":"idVoiture"}},{"text":"]"},{"text":" : Rencontre PDC ","color":"gray","extra":[{"selector":"@e[type=armor_stand,tag=OurTraceRER,limit=1]","color":"yellow"},{"text":" avec les tags "},{"entity":"@e[type=armor_stand,tag=OurTraceRER,limit=1]","nbt":"Tags"},{"text":"."}]}]

# On enregistre la position et rotation originale (avant prise en compte du TraceRER)
execute store result score #rer_rotpass rotation run data get entity @s Rotation[0] 10
function tch:rer/rame/mouvement/voiture/aligner/sauvegarder_centre
scoreboard players operation #rer_rotpass posX = #rer_rotpass_centre posX
scoreboard players operation #rer_rotpass posY = #rer_rotpass_centre posY
scoreboard players operation #rer_rotpass posZ = #rer_rotpass_centre posZ

# On se positionne précisément
tp @s ^ ^ ^3.5 ~ ~

# On positionne précisément l'arrière de notre voiture
execute as @e[type=armor_stand,tag=OurRER,tag=RERChild,distance=..10] if score @s idVoiture = #rer idVoiture run tp @s ^ ^ ^-3.5 ~ ~

# On enregistre la nouvelle rotation et position
execute store result score #rer_rotpass rotationMax run data get entity @s Rotation[0] 10
function tch:rer/rame/mouvement/voiture/aligner/sauvegarder_centre
scoreboard players operation #rer_rotpass deltaX = #rer_rotpass_centre posX
scoreboard players operation #rer_rotpass deltaY = #rer_rotpass_centre posY
scoreboard players operation #rer_rotpass deltaZ = #rer_rotpass_centre posZ

scoreboard players operation #rer_rotpass deltaX -= #rer_rotpass posX
scoreboard players operation #rer_rotpass deltaY -= #rer_rotpass posY
scoreboard players operation #rer_rotpass deltaZ -= #rer_rotpass posZ

# Si la rotation n'a pas changé, on déplace simplement le passager
execute if score #rer_rotpass rotation = #rer_rotpass rotationMax run function tch:rer/rame/mouvement/voiture/aligner_passagers
# Si la rotation a changé, on calcule la nouvelle position de toutes les entités passagères de cette voiture
# (on ajoutera directement le deltaX/Y/Z à la nouvelle position calculée)
execute unless score #rer_rotpass rotation = #rer_rotpass rotationMax run function tch:rer/rame/mouvement/voiture/rotation_passagers

# On définit l'inclinaison des modèles à notre propre rotation verticale
execute as @e[type=armor_stand,tag=OurRER,distance=..10] if score @s idVoiture = #rer idVoiture run function tch:rer/rame/mouvement/voiture/aligner_verticalement


# Si on est la voiture de tête du RER, on prend également en compte la vitesse
execute as @s[tag=RERTete] run function tch:rer/rame/mouvement/voiture/aligner/sauvegarder_tags


# Enfin, si on n'est pas la voiture de tête et que notre tracé ne bloque pas cette opération, on réaligne toutes les voitures situées devant nous
execute as @s[tag=!RERTete] unless entity @e[type=armor_stand,tag=OurTraceRER,tag=NoAlign,distance=..15] run function tch:rer/rame/mouvement/voiture/aligner_precedente
# On exécute cette fonction après la fonction aligner située dans le même dossier,
# si on n'est pas la voiture de tête et qu'on souhaite réaligner la voiture située devant nous

# On décrémente l'ID de voiture pour signaler qu'on veut sélectionner la précédente
scoreboard players remove #rer idVoiture 1

# On retire le tag de notre Tracé RER pour éviter que la voiture de tête ne croie qu'elle s'aligne sur lui
tag @e[type=armor_stand,tag=OurTraceRER,distance=..15] remove OurTraceRER

# On retire aussi les tags de nos passagers et caméras pour les remplacer par ceux de la voiture précédente
tag @e[type=#tch:passager,tag=OurPassager,distance=..10] remove OurPassager
tag @e[type=armor_stand,tag=OurCamera,distance=..150] remove OurCamera
execute as @e[type=#tch:passager_rer,tag=RERPassager,distance=..10] if score @s idVehicule = #rer idVehicule if score @s idVoiture = #rer idVoiture run tag @s add OurPassager
execute as @e[type=armor_stand,tag=RERCamera,distance=..10] if score @s idVehicule = #rer idVehicule if score @s idVoiture = #rer idVoiture run tag @s add OurCamera

# On exécute la fonction d’alignement en tant que la voiture précédente
execute as @e[type=armor_stand,tag=OurRER,tag=RERParent,distance=..25] if score @s idVoiture = #rer idVoiture positioned ^ ^ ^5 rotated as @s positioned ^ ^ ^5 run function tch:rer/rame/mouvement/voiture/aligner2
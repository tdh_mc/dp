# On exécute cette fonction en tant qu'un TraceRER pour sauvegarder notre idLine/idLineMax
scoreboard players operation #rer temp = @s idLine
scoreboard players operation #rer temp2 = @s idLineMax
scoreboard players operation #rer temp3 = @s prochaineLigne
# On ne prend pas en compte l’ID de prochaine ligne s’il ne sert pas à filtrer
# (sur les ProchaineLigne, sert à signaler au train quel est son prochain ID de ligne ;
#  sur les Direction, sert à signaler à la station à quelle ligne correspond ce quai)
execute if entity @s[tag=ProchaineLigne] run scoreboard players set #rer temp3 0
execute if entity @s[tag=Direction] run scoreboard players set #rer temp3 0

# S'il en contient un (ou deux, et qu'on est entre les deux),
# il s'agit d'un aiguillage et on doit l'ignorer si on ne possède pas le même
execute if score #rer temp = #rer idLine run scoreboard players set #rer temp 0
execute if score #rer temp < #rer idLine if score #rer temp2 >= #rer idLine run scoreboard players set #rer temp 0
# On ne prend pas en compte l’aiguillage s’il teste un ID de prochaine ligne différent du nôtre
# (utilisé aux terminus pour savoir sur quel quai envoyer un RER)
execute unless score #rer temp matches 1.. if score #rer temp3 matches 1.. unless score #rer temp3 = #rer prochaineLigne run scoreboard players set #rer temp 1
# On exécute cette fonction en tant qu’une voiture de tête de RER qui vient de rencontrer un tracé ayant le tag ForceCamera
# On doit donc modifier les données de la caméra de ce RER pour la forcer à utiliser une position existante

# On trouve une caméra aléatoire correspondant à notre idCamera dans un rayon de 100 blocs
execute as @e[type=armor_stand,tag=RERCameraPosition,distance=..100] if score @s idCamera = #rer idCamera run tag @s add ourPotentialCameraPos
tag @e[type=armor_stand,tag=ourPotentialCameraPos,distance=..100,sort=random,limit=1] add ourCameraPos
tag @e[type=armor_stand,tag=ourPotentialCameraPos,distance=..100] remove ourPotentialCameraPos

# On trouve la caméra de notre RER
execute as @e[type=armor_stand,tag=RERCamera,distance=..150] if score @s idVehicule = #rer idVehicule run tag @s add ourCamera

# On téléporte les caméras à la position de notre caméra imposée
execute as @e[type=armor_stand,tag=ourCamera,distance=..150] run function tch:rer/rame/mouvement/voiture/aligner/forcer_camera/teleporter

# On nettoie les tags temporaires
tag @e[type=armor_stand,tag=ourCamera,distance=..150] remove ourCamera
tag @e[type=armor_stand,tag=ourCameraPos,distance=..150] remove ourCameraPos
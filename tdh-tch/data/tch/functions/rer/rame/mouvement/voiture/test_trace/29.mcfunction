# On exécute cette fonction en tant qu'une armor stand RERParent à sa position et rotation,
# afin de vérifier s'il existe une armor stand TraceRER suffisamment proche de nous pour la prendre en compte

# On prend en compte le tracé, si on en trouve un, en lui donnant un tag
execute positioned ^ ^ ^-3.5 positioned ~ ~5 ~ run tag @e[type=armor_stand,tag=TraceRER,distance=..1.015,limit=1] add OurTraceRER

# On exécute ensuite la fonction d'alignement à la position de l'entité qu'on vient de tagger
# (ce qui ne fera rien du tout si on n'a pas trouvé d'entité)
execute at @e[type=armor_stand,tag=OurTraceRER,distance=..10,limit=1] positioned ~ ~-5 ~ run function tch:rer/rame/mouvement/voiture/aligner

# On supprime le tag temporaire du tracé
tag @e[type=armor_stand,tag=OurTraceRER,distance=..10] remove OurTraceRER
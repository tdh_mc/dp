# On tag la caméra pour l’informer qu’elle n’est plus ni forcée ni fixe
tag @s remove ForcedCamera
tag @s remove FixedCamera
scoreboard players set @s idCamera 0
# Cette fonction doit être exécutée en tant qu'une armor stand TraceRER,
# pour copier dans le scoreboard le prochain ID de ligne du RER qui vient de l’activer

# On doit faire ça dans une sous-fonction car l’ID de ligne peut être aléatoire parmi une certaine plage
execute if score @s idLineMax matches 1.. run function tch:rer/rame/mouvement/voiture/aligner/copier_ligne_random
execute unless score @s idLineMax matches 1.. run scoreboard players operation #rer prochaineLigne = @s prochaineLigne
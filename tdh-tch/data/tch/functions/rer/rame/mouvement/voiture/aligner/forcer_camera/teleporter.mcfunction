# On téléporte la caméra à la position de notre caméra imposée
tp @s @e[type=armor_stand,tag=ourCameraPos,distance=..150,limit=1]

# On tag la caméra pour l’informer qu’elle est forcée (et éventuellement fixe)
tag @s add ForcedCamera
scoreboard players operation @s idCamera = #rer idCamera
execute if entity @e[type=armor_stand,tag=ourCameraPos,tag=FixedCamera,distance=..150] run tag @s add FixedCamera
execute if entity @e[type=armor_stand,tag=ourCameraPos,tag=RegarderQueue,distance=..150] run tag @s add RegarderQueue
execute unless entity @e[type=armor_stand,tag=ourCameraPos,tag=FixedCamera,distance=..150] run tag @s remove FixedCamera
execute unless entity @e[type=armor_stand,tag=ourCameraPos,tag=RegarderQueue,distance=..150] run tag @s remove RegarderQueue

# On s’aligne sur la voiture la plus proche de nous
execute at @s run scoreboard players operation @s idVoiture = @e[type=armor_stand,tag=OurRER,distance=..150,sort=nearest,limit=1] idVoiture
# On définit la pose d'armor stand de la voiture appelante à la même rotation que celle de l'armor stand
data modify entity @s Pose.LeftArm[0] set from entity @s Rotation[1]
data modify entity @s Pose.RightArm[0] set from entity @s Rotation[1]
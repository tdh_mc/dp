# Fonction de mouvement exécutée par l’entité parente d’une voiture de RER, à sa position et rotation
# On a déjà enregistré #rer idVehicule et vitesse
# On enregistre également #rer idVoiture
scoreboard players operation #rer idVoiture = @s idVoiture

# On donne un tag temporaire aux passagers et aux caméras
execute as @e[type=#tch:passager_rer,tag=RERPassager,distance=..15] if score @s idVehicule = #rer idVehicule if score @s idVoiture = #rer idVoiture run tag @s add OurPassager
execute as @e[type=armor_stand,tag=RERCamera,distance=..150] if score @s idVehicule = #rer idVehicule if score @s idVoiture = #rer idVoiture run tag @s add OurCamera

# Comme Minecraft est toujours adorable on doit avoir une ligne de commande différente par valeur de la vitesse possible (la commande /tp n'acceptant pas d'arguments)
# On sépare ça en plusieurs choix binaires pour limiter le nombre de conditions
# La vitesse max est de 32 blocs par seconde et c'est probablement suffisant (ça fait 120 km/h) et ça pourra tjrs être augmenté au besoin
execute if score #rer vitesse matches 1..16 run function tch:rer/rame/mouvement/1_16
execute if score #rer vitesse matches 17.. run function tch:rer/rame/mouvement/17_32

# On retire les tags temporaires des passagers et caméras
tag @e[type=#tch:passager_rer,tag=OurPassager,distance=..15] remove OurPassager
tag @e[type=armor_stand,tag=OurCamera,distance=..150] remove OurCamera
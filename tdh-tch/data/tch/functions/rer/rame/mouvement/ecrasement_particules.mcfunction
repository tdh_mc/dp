# On affiche des particules
# Coup critique (gris clair)
particle crit ^ ^1 ^.75 0.05 0.6 0.05 .25 100
# Explosion de sang (de si bon goût)
particle dust 1 0 0 5 ^ ^.75 ^1.25 0.3 0.3 0.3 1 100
particle crimson_spore ^ ^1 ^1 0.2 0.2 0.2 1 250
# Cendre qui retombe (noir)
particle squid_ink ^ ^.5 ^1 0.25 0.25 0.25 .15 50

# On joue le son d'électrocution
playsound tch:br.rer.collision block @a[distance=..30] ~ ~ ~ 2 1
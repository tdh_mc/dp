# On inverse l’ID de voiture du passager qui appelle cette fonction
# (le RER qui le contient est en train de se retourner)
scoreboard players set @s[scores={idVoiture=1}] idVoiture -1
scoreboard players set @s[scores={idVoiture=2}] idVoiture 1
execute as @s[scores={idVoiture=-1}] run scoreboard players set idVoiture 2
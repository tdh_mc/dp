# On exécute cette fonction en tant qu'une motrice (de tête) d'un RER,
# si notre score "vitesse" est inférieur à la vitesse maximale actuelle,
# pour éventuellement accélérer ou ralentir

# Si on peut accélérer/décélérer maintenant, on exécute la sous-fonction
execute unless score @s remainingTicks matches 1.. run function tch:rer/rame/mouvement/moduler_vitesse/test_vitesse

# Si on a accéléré/décéléré trop récemment, on décrémente le timer
execute if score @s remainingTicks matches 1.. run scoreboard players remove @s remainingTicks 1
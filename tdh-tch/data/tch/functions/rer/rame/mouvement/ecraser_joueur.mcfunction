# On affiche les effets visuels
function tch:rer/rame/mouvement/ecrasement_particules

# On affiche un message dans le chat
tellraw @a[gamemode=!spectator,distance=0,tag=!RERPassager] [{"text":"Vous êtes mort écrasé par un train.","color":"red"},{"text":" À l'avenir, veillez à ne jamais traverser les voies !","color":"gray"}]

# On tag le joueur pour ne pas répéter le message plusieurs fois
tag @a[gamemode=!spectator,distance=0,tag=!RERPassager] add MortEcrase

# On déplace le joueur dans la direction de son écrasement
tp @a[gamemode=!spectator,distance=0,tag=!RERPassager] ^ ^.35 ^1

# On tue le joueur
execute positioned ^ ^.35 ^1 run kill @a[gamemode=!spectator,distance=..0.01,tag=!RERPassager]
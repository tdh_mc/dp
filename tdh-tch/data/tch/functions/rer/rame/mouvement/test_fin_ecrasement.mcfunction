# Si un joueur est mort écrasé, on vérifie s'il est vivant avant de lui retirer le tag
tag @a[tag=MortEcrase,scores={vie=1..}] remove MortEcrase
# On arrête le RER au plus vite en raison de circonstances exceptionnelles

# On définit la vitesse maximale à 0
scoreboard players set @s vitesseMax 0
# On se donne un tag notifiant l’arrêt d’urgence
tag @s add ArretUrgence
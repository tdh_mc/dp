# On exécute cette fonction en tant qu'une motrice (de tête) d'un RER,
# pour faire avancer l'ensemble du train

# On enregistre les valeurs partagées actuelles (=communes à l'ensemble du train)
# ↓ Ligne commentée car on l’a déjà défini dans tick_rame, je me trompe ??
#scoreboard players operation #rer idVehicule = @s idVehicule
scoreboard players operation #rer vitesse = @s vitesse
# La vitesse est exprimée en blocs par seconde, pour permettre d'avoir des déplacements plus courts qu'un bloc entier
# (1 bloc par seconde = 0.05 bloc par tick)

# On donne un tag temporaire aux entités qui composent le train
execute as @e[type=armor_stand,tag=RER,distance=..30] if score @s idVehicule = #rer idVehicule run tag @s add OurRER

# On écrase toutes les entités situées devant nous
execute if score #rer vitesse matches 1.. at @e[type=armor_stand,tag=OurRER,tag=RERParent,distance=..30] run function tch:rer/rame/mouvement/ecraser_entites

# On fait avancer chaque voiture correspondant à notre train séparément,
# (= en suivant sa propre orientation), mais à la même vitesse

# On exécute la fonction de déplacement séparément pour chaque voiture
execute as @e[type=armor_stand,tag=OurRER,tag=RERParent,distance=..30] at @s run function tch:rer/rame/mouvement/voiture

# On module éventuellement la vitesse du véhicule, si on est en-dessous de la vitesse maximale
execute unless score @s vitesse = @s vitesseMax run function tch:rer/rame/mouvement/moduler_vitesse


# On supprime les tags temporaires de nos entités
tag @e[type=armor_stand,tag=OurRER,distance=..30] remove OurRER
# TICK DES RAMES DE RER
# Cette fonction est exécutée régulièrement par chacun des trains (uniquement la tête) pour exécuter tout ce qui est nécessaire

# On enregistre l'ID de notre rame
scoreboard players operation #rer idVehicule = @s idVehicule

# Si on est en station, on vérifie quand on doit repartir
execute as @s[tag=EnStation] run function tch:rer/rame/en_station/tick

# Si on est en arrêt d'urgence, on vérifie quand on doit repartir (TODO)
#execute as @s[tag=ArretUrgence] run function tch:rer/rame/pre_station/tick_arret_urgence

# Si on doit se déplacer maintenant, on le fait
execute as @s[tag=Mouvement] if entity @p[distance=..160] run function tch:rer/rame/mouvement


# Si on est à plus de 160 blocs d'un joueur, on se détruit
execute unless entity @p[distance=..159] run function tch:rer/rame/detruire
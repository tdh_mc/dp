# On exécute cette fonction en tant qu’une tête de RER pour effectuer un départ après un arrêt d’urgence

# On se retire les tags temporaires
tag @s remove ArretUrgence

# On effectue un départ normal
function tch:rer/rame/depart

# On définit notre vitesse maximale à celle du signal, ou 8 m/s si le signal n’a pas de vitesse max enregistrée
scoreboard players operation @s vitesseMax = @e[type=#tch:marqueur/signal,tag=ourSignal,distance=..150,limit=1] vitesseMax
execute unless score @s vitesseMax matches 1.. run scoreboard players set @s vitesseMax 8
# On exécute cette fonction en tant qu’une tête de RER pour effectuer un départ à vitesse réduite après un arrêt d’urgence

# On se retire les tags temporaires
tag @s remove ArretUrgence

# On effectue un départ normal
function tch:rer/rame/depart

# On définit notre vitesse maximale à une vitesse réduite, ou 2 m/s si le signal n’a pas de vitesse max enregistrée
scoreboard players operation @s vitesseMax = @e[type=#tch:marqueur/signal,tag=ourSignal,distance=..150,limit=1] vitesseMax
execute if score @s vitesseMax matches 11.. run scoreboard players set @s vitesseMax 10
execute if score @s vitesseMax matches 6..9 run scoreboard players set @s vitesseMax 5
execute unless score @s vitesseMax matches 1.. run scoreboard players set @s vitesseMax 2
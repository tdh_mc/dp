# On arrête le RER qui exécute cette fonction
tag @s remove Mouvement
scoreboard players set @s vitesse 0

# On exécute une rotation pour « réparer » le bug des rotations RER
execute as @e[type=armor_stand,tag=RER,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/arret/reparer_rotation
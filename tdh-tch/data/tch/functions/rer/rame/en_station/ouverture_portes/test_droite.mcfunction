# On exécute cette fonction en tant qu'une entité appartenant à un RER,
# pour vérifier quel type de voiture on est et poser la porte ouverte appropriée

# Pour chaque modèle on a 2 versions qui déterminent si on est le modèle placé à l'avant ou à l'arrière du RER

# Si on est une voiture de tête
item replace entity @s[tag=RERTete] weapon.offhand with yellow_dye{CustomModelData:12}
item replace entity @s[tag=RERTeteBack] weapon.mainhand with yellow_dye{CustomModelData:12}
# TODO : Si on est une voiture intermédiaire
# Si on est une voiture de queue
item replace entity @s[tag=RERQueue] weapon.offhand with yellow_dye{CustomModelData:22}
item replace entity @s[tag=RERQueueBack] weapon.mainhand with yellow_dye{CustomModelData:22}


# On place également les barrières
execute as @s[tag=RERParent] run fill ^-2 ^-1 ^-8 ^2 ^3 ^1 barrier keep
execute as @s[tag=RERTete] run fill ^-1 ^0 ^-7 ^1 ^2 ^-2 air
execute as @s[tag=RERTete] run fill ^-1 ^0 ^0 ^1 ^2 ^0 air
execute as @s[tag=RERQueue] run fill ^-1 ^0 ^-5 ^1 ^2 ^0 air
execute as @s[tag=RERQueue] run fill ^-1 ^0 ^-7 ^1 ^2 ^-7 air
# Seules ces commandes changent entre test_gauche et test_droite :
execute as @s[tag=RERTete] run fill ^-2 ^0 ^-2 ^-2 ^1 ^-2 air
execute as @s[tag=RERTete] run fill ^-2 ^0 ^-7 ^-2 ^1 ^-7 air
execute as @s[tag=RERQueue] run fill ^-2 ^0 ^0 ^-2 ^1 ^0 air
execute as @s[tag=RERQueue] run fill ^-2 ^0 ^-5 ^-2 ^1 ^-5 air
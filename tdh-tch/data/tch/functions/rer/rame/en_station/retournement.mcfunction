# On exécute cette fonction pour procéder au retournement du RER qui va partir de la station

# On déréserve la voie que l'on occupait
# (TODO : retirer entièrement la résa de voie puisqu’on a remplacé ça par des signaux)
function tch:voie/reservation/depart
function tch:station/pid/unlock_0_rer

# On retire notre tag RetournementPrevu
tag @s remove RetournementPrevu

tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"/"},{"score":{"name":"@s","objective":"idVoiture"}},{"text":"]"},{"text":" : Retournement en station","color":"gray","extra":[{"text":", changement de ligne ("},{"score":{"name":"@s","objective":"idLine"},"color":"red"},{"text":"→"},{"score":{"name":"@s","objective":"prochaineLigne"},"color":"green"},{"text":")"}]}]

# On donne un tag temporaire aux entités qui composent le train
scoreboard players operation #rer idVehicule = @s idVehicule
execute as @e[type=armor_stand,tag=RER,distance=..30] if score @s idVehicule = #rer idVehicule run tag @s add OurRER

# On place des marqueurs temporaires aux positions des entités du RER
execute at @e[type=armor_stand,tag=OurRER,tag=RERTete,distance=..30,limit=1] run summon marker ~ ~ ~ {Tags:["OurRERTeteMarker","RERTempMarker"]}
execute at @e[type=armor_stand,tag=OurRER,tag=RERTeteBack,distance=..30,limit=1] run summon marker ~ ~ ~ {Tags:["OurRERTeteBackMarker","RERTempMarker"]}
execute at @e[type=armor_stand,tag=OurRER,tag=RERQueue,distance=..30,limit=1] run summon marker ~ ~ ~ {Tags:["OurRERQueueMarker","RERTempMarker"]}
execute at @e[type=armor_stand,tag=OurRER,tag=RERQueueBack,distance=..30,limit=1] run summon marker ~ ~ ~ {Tags:["OurRERQueueBackMarker","RERTempMarker"]}

# On va positionner la queue enfant à l’endroit de la tête parent (et vice versa)
# puis la tête enfant à l’endroit de la queue parent (et vice versa)
execute positioned as @e[type=marker,tag=OurRERQueueBackMarker,distance=..30,limit=1] run tp @e[type=armor_stand,tag=OurRER,tag=RERTete,distance=..30,limit=1] ~ ~ ~ ~180 ~
execute positioned as @e[type=marker,tag=OurRERTeteMarker,distance=..30,limit=1] run tp @e[type=armor_stand,tag=OurRER,tag=RERQueueBack,distance=..30,limit=1] ~ ~ ~ ~180 ~
execute positioned as @e[type=marker,tag=OurRERQueueMarker,distance=..30,limit=1] run tp @e[type=armor_stand,tag=OurRER,tag=RERTeteBack,distance=..30,limit=1] ~ ~ ~ ~180 ~
execute positioned as @e[type=marker,tag=OurRERTeteBackMarker,distance=..30,limit=1] run tp @e[type=armor_stand,tag=OurRER,tag=RERQueue,distance=..30,limit=1] ~ ~ ~ ~180 ~

# On nettoie tous les marqueurs temporaires
kill @e[type=marker,tag=RERTempMarker,distance=..30]

# On remplace l’ID de ligne par le « prochain » ID de ligne (désormais actuel)
scoreboard players operation #rer prochaineLigne = @s prochaineLigne
scoreboard players operation @e[type=armor_stand,tag=OurRER,distance=..30,scores={idLine=1..}] idLine = #rer prochaineLigne
scoreboard players operation #rer idLine = #rer prochaineLigne
scoreboard players set #rer prochaineLigne 0
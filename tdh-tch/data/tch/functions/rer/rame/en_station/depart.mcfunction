
tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"]"},{"text":" : Départ de la gare de ","color":"gray","extra":[{"selector":"@e[tag=NomStation,distance=..200,sort=nearest,limit=1]","color":"yellow"},{"text":"."}]}]

# On déréserve la voie que l'on occupait
# (TODO : retirer entièrement la résa de voie puisqu’on a remplacé ça par des signaux)
function tch:voie/reservation/depart
function tch:station/pid/unlock_0_rer

# On signale au train qu'il peut se remettre en mouvement
function tch:rer/rame/depart
# On initialise la variable de comptage des joueurs dans le train
scoreboard players set #rerPassager temp 0

# On détruit toutes les barrières situées "dans" notre train
scoreboard players operation #rer idVehicule = @s idVehicule
execute as @e[type=armor_stand,distance=..30,tag=RERParent] if score @s idVehicule = #rer idVehicule at @s run function tch:rer/rame/en_station/depart/supprimer_barrieres

# On enregistre tous les passagers du train
# Pour ce faire, on enregistre déjà notre position maximale et minimale en déposant 2 entités temporaires
# TODO : Rendre ce code générique quelle que soit la longueur du train
execute positioned ^-1.5 ^-0.5 ^-17.5 run summon item ~ ~ ~ {Item:{id:"iron_nugget",Count:1b},NoGravity:1b,Invulnerable:1b,PickupDelay:32767s,Age:5999s,Tags:["RERVolumeMarker1"]}
execute positioned ^1.5 ^2.5 ^0.5 run summon item ~ ~ ~ {Item:{id:"iron_nugget",Count:1b},NoGravity:1b,Invulnerable:1b,PickupDelay:32767s,Age:5999s,Tags:["RERVolumeMarker2"]}

execute as @e[type=item,tag=RERVolumeMarker1,distance=..30,limit=1] run function tch:rer/rame/en_station/depart/sauver_pos1
execute as @e[type=item,tag=RERVolumeMarker2,distance=..30,limit=1] run function tch:rer/rame/en_station/depart/sauver_pos2
# Les variables appropriées se trouvent dans #rerVolMin/Max posX/Y/Z
# On teste toutes les entités correspondantes proches pour savoir lesquelles prennent le RER
execute as @e[type=#tch:passager_rer,distance=..30] run function tch:rer/rame/en_station/depart/test_passager
execute at @a[gamemode=!spectator,distance=..30] run function tch:rer/rame/en_station/depart/test_joueur
# Si la variable temporaire vaut >0, c'est qu'on a au moins un joueur dans la rame et qu'on peut update les caméras
# Dans le cas contraire, on n'updatera pas les caméras
execute if score #rerPassager temp matches 1.. as @e[type=armor_stand,tag=RERCamera,distance=..150] if score @s idVehicule = #rer idVehicule run tag @s remove NoUpdate
execute if score #rerPassager temp matches ..0 as @e[type=armor_stand,tag=RERCamera,distance=..150] if score @s idVehicule = #rer idVehicule run tag @s add NoUpdate

# On quitte la station
tag @s remove EnStation
# Arrivée en station (détection de l'entité appropriée par le RER)

# On n'a pas de différenciation entre arrêt simple et terminus ici,
# puisque le train s'arrête dans tous les cas pour laisser descendre les gens
# Par conséquent il n'y a pas de fonction "arret", tout se passe ici

tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"]"},{"text":" : Arrivée en gare de ","color":"gray","extra":[{"selector":"@e[tag=NomStation,distance=..200,sort=nearest,limit=1]","color":"yellow"},{"text":"."}]}]

# On affiche à nouveau les modèles de l'arrière des voitures
execute as @e[type=armor_stand,tag=RERTeteBack,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/mouvement/afficher_modeles/tete
execute as @e[type=armor_stand,tag=RERQueue,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/mouvement/afficher_modeles/queue

# On s'arrête
tag @s add EnStation
function tch:rer/rame/arret

# On récupère notre ID de station
scoreboard players operation @s idStation = @s prochaineStation
# TODO: Récupérer la station dans le système d'itinéraire, comme pour les métros

# On ouvre les portes
function tch:rer/rame/en_station/ouverture_portes
# On joue la seconde partie du son d’arrivée
execute at @e[type=armor_stand,tag=OurRER,distance=..30] run function tch:sound/rer/arrivee_stop

# On libère les passagers joueurs
# Je sais que ça a l'air bizarre, mais on commence par tous les téléporter dans une direction aléatoire pour qu'aucun ne soit au même endroit précisément (ça permet de remettre chacun d'entre eux à la bonne position)
execute at @a[tag=RERPassager] if score @p[distance=0,tag=RERPassager] idVehicule = #rer idVehicule facing entity @e[type=#tdh:random,sort=random,limit=1] feet run tp @r[distance=0] ^ ^ ^1
execute at @a[tag=RERPassager] if score @p[distance=0,tag=RERPassager] idVehicule = #rer idVehicule run function tch:rer/rame/en_station/arrivee/retirer_joueur
# On supprime tous les mobs temporaires associés à notre rame
execute as @e[type=piglin,tag=RERPlayerMarker,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/en_station/arrivee/supprimer_mob
# On rend leur liberté à tous les passagers non-joueurs
execute as @e[type=#tch:passager_rer,tag=RERPassager,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/en_station/arrivee/retirer_passager

# On supprime les tags de pré-station
tag @s remove PreStation
tag @s remove Corresp
tag @s remove DescenteDroite

# 15 secondes avant le départ
scoreboard players set @s remainingTicks 299
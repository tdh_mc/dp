# On retire l'entité qui exécute la fonction des passagers du RER dont l'ID est stocké dans #rer idVehicule
# Cette fonction est exécutée à la position de cette entité

# On se désimmobilise
data modify entity @s NoAI set value 0b
data modify entity @s Invulnerable set value 0b

# On se tp légèrement au dessus de notre position pour éviter de tomber dans un bloc
execute at @s positioned ~ ~-0.2 ~ align y run tp @s ~ ~1.3 ~ ~ ~
execute at @s if block ~ ~ ~ barrier run function tch:rer/rame/en_station/arrivee/reparer_position

# On supprime notre ID véhicule, et notre ID voiture
scoreboard players reset @s idVehicule
scoreboard players reset @s idVoiture

# On enregistre le fait qu'on n'est plus un passager
tag @s remove RERPassager
# On trouve la position la plus proche qui matche ce qu'on cherche (une barrière en-dessous et 2 blocs d'air au-dessus)
# Mais uniquement au-dessus de nous

# On teste en croix autour de nous (1 de distance)
execute if block ~ ~ ~ barrier if block ~ ~1 ~ air if block ~ ~2 ~ air align xyz run tp @s ~0.5 ~1 ~0.5
execute as @s[distance=0] if block ~-1 ~-1 ~ barrier if block ~-1 ~0 ~ air if block ~-1 ~1 ~ air align xyz run tp @s ~-0.5 ~ ~0.5
execute as @s[distance=0] if block ~1 ~-1 ~ barrier if block ~1 ~0 ~ air if block ~1 ~1 ~ air align xyz run tp @s ~1.5 ~ ~0.5
execute as @s[distance=0] if block ~ ~-1 ~-1 barrier if block ~ ~0 ~-1 air if block ~ ~1 ~-1 air align xyz run tp @s ~0.5 ~ ~-0.5
execute as @s[distance=0] if block ~ ~-1 ~1 barrier if block ~ ~0 ~1 air if block ~ ~1 ~1 air align xyz run tp @s ~0.5 ~ ~1.5
execute as @s[distance=0] if block ~-1 ~ ~ barrier if block ~-1 ~1 ~ air if block ~-1 ~2 ~ air align xyz run tp @s ~-0.5 ~1 ~0.5
execute as @s[distance=0] if block ~1 ~ ~ barrier if block ~1 ~1 ~ air if block ~1 ~2 ~ air align xyz run tp @s ~1.5 ~1 ~0.5
execute as @s[distance=0] if block ~ ~ ~-1 barrier if block ~ ~1 ~-1 air if block ~ ~2 ~-1 air align xyz run tp @s ~0.5 ~1 ~-0.5
execute as @s[distance=0] if block ~ ~ ~1 barrier if block ~ ~1 ~1 air if block ~ ~2 ~1 air align xyz run tp @s ~0.5 ~1 ~1.5
# Si on a tjrs pas trouvé de position valide, on réessaye avec les diagonales
execute as @s[distance=0] if block ~-1 ~-1 ~1 barrier if block ~-1 ~0 ~1 air if block ~-1 ~1 ~1 air run tp @s ~-0.5 ~ ~1.5
execute as @s[distance=0] if block ~1 ~-1 ~-1 barrier if block ~1 ~0 ~-1 air if block ~1 ~1 ~-1 air run tp @s ~1.5 ~ ~-0.5
execute as @s[distance=0] if block ~-1 ~-1 ~-1 barrier if block ~-1 ~0 ~-1 air if block ~-1 ~1 ~-1 air run tp @s ~-0.5 ~ ~-0.5
execute as @s[distance=0] if block ~1 ~-1 ~1 barrier if block ~1 ~0 ~1 air if block ~1 ~1 ~1 air run tp @s ~1.5 ~ ~1.5
execute as @s[distance=0] if block ~-1 ~ ~1 barrier if block ~-1 ~1 ~1 air if block ~-1 ~2 ~1 air run tp @s ~-0.5 ~1 ~1.5
execute as @s[distance=0] if block ~1 ~ ~-1 barrier if block ~1 ~1 ~-1 air if block ~1 ~2 ~-1 air run tp @s ~1.5 ~1 ~-0.5
execute as @s[distance=0] if block ~-1 ~ ~-1 barrier if block ~-1 ~1 ~-1 air if block ~-1 ~2 ~-1 air run tp @s ~-0.5 ~1 ~-0.5
execute as @s[distance=0] if block ~1 ~ ~1 barrier if block ~1 ~1 ~1 air if block ~1 ~2 ~1 air run tp @s ~1.5 ~1 ~1.5
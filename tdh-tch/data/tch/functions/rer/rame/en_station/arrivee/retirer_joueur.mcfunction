# On retire tous les joueurs à la position de la fonction comme passagers du RER dont l'ID est stocké dans #rer idVehicule

tag @p[distance=0] add RERTpEndTemp

# On retire le mode spectateur d'entité
# ATTENTION : On a défini une permission globale dans le serveur pour tous les joueurs pour la commande "spectate" pour que ceci fonctionne
# On est bien d'accord que c'est parfaitement GOGOLESQUE mais en l'absence d'une meilleure syntaxe on doit s'en contenter... cordialement
execute as @p[tag=RERTpEndTemp] run spectate

# On se replace à la position de notre marqueur une dernière fois si on était en mode caméra
scoreboard players operation #rer idMarqueur = @p[tag=RERTpEndTemp] idMarqueur
execute as @e[type=piglin,tag=RERPlayerMarker,distance=..150] if score @s idMarqueur = #rer idMarqueur at @s run tp @p[tag=RERTpEndTemp] @s
execute at @p[tag=RERTpEndTemp] run tp @p[tag=RERTpEndTemp] ~ ~.3 ~ ~ ~

execute at @p[tag=RERTpEndTemp] if block ~ ~ ~ barrier run function tch:rer/rame/en_station/arrivee/reparer_position_joueur

# On se désimmobilise
#attribute @a[tag=RERTpEndTemp] minecraft:generic.knockback_resistance modifier remove 00112233-4444-5555-6666-777788889999
#attribute @a[tag=RERTpEndTemp] minecraft:generic.movement_speed modifier remove 00112233-4444-5555-6666-777777888888
effect clear @p[tag=RERTpEndTemp] levitation
effect clear @p[tag=RERTpEndTemp] resistance

# On reset notre ID véhicule, et notre ID voiture
scoreboard players reset @p[tag=RERTpEndTemp] idVehicule
scoreboard players reset @p[tag=RERTpEndTemp] idVoiture
scoreboard players reset @p[tag=RERTpEndTemp] idMarqueur

# On se retire le tag signalant qu'on est passager
tag @p[tag=RERTpEndTemp] remove RERPassager
tag @p[tag=RERTpEndTemp] remove RERCamera

# On restaure notre ancien gamemode
gamemode survival @p[tag=RERTpEndTemp,scores={mode=0}]
gamemode creative @p[tag=RERTpEndTemp,scores={mode=1}]
gamemode adventure @p[tag=RERTpEndTemp,scores={mode=2}]

tag @p[tag=RERTpEndTemp] remove RERTpEndTemp
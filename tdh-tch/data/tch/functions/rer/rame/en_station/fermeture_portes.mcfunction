# On exécute cette fonction en tant que la voiture de tête du RER

# On enregistre l'ID véhicule
scoreboard players operation #rer idVehicule = @s idVehicule

# On sélectionne toutes les entités appartenant au RER, qu'on teste ensuite dans une sous-fonction pour choisir le bon modèle de porte
execute as @e[type=armor_stand,tag=RER,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/en_station/fermeture_portes/test_entite

# On joue le son de fermeture
#execute positioned ~ ~3.3 ~ run function tch:sound/station_doors_close
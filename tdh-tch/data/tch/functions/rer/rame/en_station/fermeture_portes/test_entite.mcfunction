# On exécute cette fonction en tant qu'une entité appartenant à un RER,
# pour vérifier quel type de voiture on est et poser la porte fermée appropriée

# Pour chaque modèle on a 2 versions qui déterminent si on est le modèle placé à l'avant ou à l'arrière du RER

# Si on est une voiture de tête
item replace entity @s[tag=RERTete] weapon.offhand with yellow_dye{CustomModelData:10}
item replace entity @s[tag=RERTeteBack] weapon.mainhand with yellow_dye{CustomModelData:10}
# TODO : Si on est une voiture intermédiaire
# Si on est une voiture de queue
item replace entity @s[tag=RERQueue] weapon.offhand with yellow_dye{CustomModelData:20}
item replace entity @s[tag=RERQueueBack] weapon.mainhand with yellow_dye{CustomModelData:20}


# On place également les barrières (uniquement celles correspondant aux portes)
execute as @s[tag=RERTete] run fill ^-2 ^0 ^-7 ^-2 ^1 ^-2 barrier keep
execute as @s[tag=RERTete] run fill ^2 ^0 ^-7 ^2 ^1 ^-2 barrier keep
execute as @s[tag=RERQueue] run fill ^-2 ^0 ^-5 ^-2 ^1 ^0 barrier keep
execute as @s[tag=RERQueue] run fill ^2 ^0 ^-5 ^2 ^1 ^0 barrier keep

# On pousse les entités soit vers l’intérieur, soit vers l’extérieur
execute as @s[y_rotation=45..135] run tag @s add PousserZ
execute as @s[y_rotation=-135..-45] run tag @s add PousserZ

execute at @s[tag=RERTete,tag=PousserZ] positioned ^-2 ^0 ^-4.5 run function tch:rer/rame/en_station/fermeture_portes/pousser_entites_z
execute at @s[tag=RERTete,tag=PousserZ] positioned ^2 ^0 ^-4.5 run function tch:rer/rame/en_station/fermeture_portes/pousser_entites_z
execute at @s[tag=RERTete,tag=!PousserZ] positioned ^-2 ^0 ^-4.5 run function tch:rer/rame/en_station/fermeture_portes/pousser_entites_x
execute at @s[tag=RERTete,tag=!PousserZ] positioned ^2 ^0 ^-4.5 run function tch:rer/rame/en_station/fermeture_portes/pousser_entites_x
execute at @s[tag=RERQueue,tag=PousserZ] positioned ^-2 ^0 ^-2.5 run function tch:rer/rame/en_station/fermeture_portes/pousser_entites_z
execute at @s[tag=RERQueue,tag=PousserZ] positioned ^2 ^0 ^-2.5 run function tch:rer/rame/en_station/fermeture_portes/pousser_entites_z
execute at @s[tag=RERQueue,tag=!PousserZ] positioned ^-2 ^0 ^-2.5 run function tch:rer/rame/en_station/fermeture_portes/pousser_entites_x
execute at @s[tag=RERQueue,tag=!PousserZ] positioned ^2 ^0 ^-2.5 run function tch:rer/rame/en_station/fermeture_portes/pousser_entites_x

tag @s remove PousserZ
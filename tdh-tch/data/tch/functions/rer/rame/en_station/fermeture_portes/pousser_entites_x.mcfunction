# On pousse les entités situées autour des portes
# On est déjà positionnés au bon endroit

execute positioned ~-.3 ~ ~-3 as @e[type=#tch:passager_rer,dx=-.75,dy=2,dz=6] at @s run tp @s ~-.75 ~ ~
execute positioned ~.3 ~ ~-3 as @e[type=#tch:passager_rer,dx=.75,dy=2,dz=6] at @s run tp @s ~.75 ~ ~

execute positioned ~-.3 ~ ~-3 at @a[gamemode=!spectator,dx=-.75,dy=2,dz=6] run tp @a[gamemode=!spectator,distance=0] ~-.75 ~ ~
execute positioned ~.3 ~ ~-3 at @a[gamemode=!spectator,dx=.75,dy=2,dz=6] run tp @a[gamemode=!spectator,distance=0] ~.75 ~ ~
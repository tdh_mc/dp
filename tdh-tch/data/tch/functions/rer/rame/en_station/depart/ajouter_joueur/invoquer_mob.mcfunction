# Cette fonction est exécutée à la position et rotation d'un joueur,
# et invoque un mob placeholder destiné à repositionner le joueur à chaque tick
summon piglin ~ ~ ~ {NoGravity:1b,NoAI:1b,Invulnerable:1b,Tags:["RERPassager","RERPlayerMarker","RERMarkerTemp"],IsImmuneToZombification:1b,Silent:1b,PersistenceRequired:1b}

# On copie toutes les données nécessaires au piglin (scores, inventaire...)
# depuis une sous-fonction qu'il exécute lui-même
execute as @e[type=piglin,tag=RERMarkerTemp,distance=..1,limit=1] run function tch:rer/rame/en_station/depart/ajouter_joueur/copier_donnees
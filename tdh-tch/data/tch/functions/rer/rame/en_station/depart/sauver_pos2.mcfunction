# On enregistre dans une variable temporaire notre position

execute store result score #rerVolMax posX run data get entity @s Pos[0] 100
execute store result score #rerVolMax posY run data get entity @s Pos[1] 100
execute store result score #rerVolMax posZ run data get entity @s Pos[2] 100

# On vérifie lequel est le min et lequel est le max, et on inverse si c'est mal assigné
execute if score #rerVolMin posX > #rerVolMax posX run scoreboard players operation #rerVolMin posX >< #rerVolMax posX
execute if score #rerVolMin posY > #rerVolMax posY run scoreboard players operation #rerVolMin posY >< #rerVolMax posY
execute if score #rerVolMin posZ > #rerVolMax posZ run scoreboard players operation #rerVolMin posZ >< #rerVolMax posZ
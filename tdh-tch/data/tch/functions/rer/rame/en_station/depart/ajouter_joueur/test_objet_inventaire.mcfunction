# On exécute cette fonction à la position d'un joueur qui vient d'invoquer un piglin pour servir de placeholder à sa place dans le RER, en tant que ce piglin

# On vient également de copier les données d'un des derniers slots d'inventaire du joueur, pour :
# - vérifier si c'est un des slots qu'on recherche (armure ou item main gauche)
# - le copier sur le piglin au bon emplacement le cas échéant

# On commence par récupérer la valeur du slot
execute store result score #rerPassager temp5 run data get storage tch:passager item.Slot

# Si c'est un des slots qu'on cherche, on le copie sur le piglin
execute if score #rerPassager temp5 matches 100 run data modify entity @s ArmorItems[0] set from storage tch:passager item
execute if score #rerPassager temp5 matches 101 run data modify entity @s ArmorItems[1] set from storage tch:passager item
execute if score #rerPassager temp5 matches 102 run data modify entity @s ArmorItems[2] set from storage tch:passager item
execute if score #rerPassager temp5 matches 103 run data modify entity @s ArmorItems[3] set from storage tch:passager item
execute if score #rerPassager temp5 matches -106 run data modify entity @s HandItems[1] set from storage tch:passager item
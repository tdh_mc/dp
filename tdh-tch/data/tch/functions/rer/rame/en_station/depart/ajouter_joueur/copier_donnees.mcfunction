
# En tout premier on copie la rotation du joueur (qui est celle d'exécution de la fonction) au marqueur
# On aligne en y pour éviter d'envoyer les joueurs en creative dans le ciel à force de les remonter de quelques dizièmes de bloc à chaque station
execute align y run tp @s ~ ~ ~ ~ ~

# On donne d'abord le nom du joueur au piglin
# On est obligés de donner un tag au joueur pour pouvoir le retrouver de là-bas
tag @p[gamemode=!spectator] add RERPlayerNameTemp
execute positioned as @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/rame/en_station/depart/ajouter_joueur/copier_nom
tag @p[tag=RERPlayerNameTemp] remove RERPlayerNameTemp

# On copie l'armure du joueur et CROIS MOI mon cochon que c'est une belle galère
# Les derniers éléments dans l'inventaire sont, dans l'ordre :
# - Slot 100, les bottes
# - Slot 101, les jambes
# - Slot 102, le torse
# - Slot 103, la tête
# - Slot -106, l'item main gauche
# Mais chacun de ces éléments n'est présent QUE s'il existe, si c'est vide y'a pas !
# Par conséquent on va exécuter 5 fois la fonction qui va copier les données d'un des derniers éléments de l'inventaire, "tester" son slot, et l'assigner au piglin si c'est un des slots qu'on recherche
data modify storage tch:passager item set from entity @p[gamemode=!spectator] Inventory[-1]
function tch:rer/rame/en_station/depart/ajouter_joueur/test_objet_inventaire
data modify storage tch:passager item set from entity @p[gamemode=!spectator] Inventory[-2]
function tch:rer/rame/en_station/depart/ajouter_joueur/test_objet_inventaire
data modify storage tch:passager item set from entity @p[gamemode=!spectator] Inventory[-3]
function tch:rer/rame/en_station/depart/ajouter_joueur/test_objet_inventaire
data modify storage tch:passager item set from entity @p[gamemode=!spectator] Inventory[-4]
function tch:rer/rame/en_station/depart/ajouter_joueur/test_objet_inventaire
data modify storage tch:passager item set from entity @p[gamemode=!spectator] Inventory[-5]
function tch:rer/rame/en_station/depart/ajouter_joueur/test_objet_inventaire
# Après cette purge, on peut au moins définir l'objet main droite comme des gens sains d'esprit
data modify entity @s HandItems[0] set from entity @p[gamemode=!spectator] SelectedItem

# On génère un ID unique destiné à enregistrer le lien entre ce mob et ce joueur
function tch:rer/rame/en_station/depart/ajouter_joueur/generer_id
# En sortie de cette fonction, il se trouve simplement dans #random temp

# On enregistre cet ID unique chez le joueur et le marqueur
scoreboard players operation @p[gamemode=!spectator] idMarqueur = #random temp
scoreboard players operation @s idMarqueur = #random temp
# On enregistre également pour le marqueur l'ID véhicule, car à chaque arrivée en station on supprimera directement tous les items du véhicule plutôt que de se faire chier à sélectionner depuis chaque joueur
scoreboard players operation @s idVehicule = #rer idVehicule
# On a aussi besoin de l'ID voiture pour faire les rotations
scoreboard players operation @s idVoiture = @p[gamemode=!spectator] idVoiture

tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"#rer","objective":"idVehicule"}},{"text":"/"},{"score":{"name":"@s","objective":"idVoiture"}},{"text":"]"},{"text":" : Données copiées du joueur ","color":"gray","extra":[{"selector":"@p[gamemode=!spectator]","color":"yellow","hoverEvent":{"action":"show_text","contents":[{"entity":"@p[gamemode=!spectator]","nbt":"Pos"}]}},{"text":" au piglin "},{"selector":"@s","color":"green","hoverEvent":{"action":"show_text","contents":[{"text":"Pos: "},{"entity":"@s","nbt":"Pos"},{"text":"\nID véhicule: "},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"\nID voiture: "},{"score":{"name":"@s","objective":"idVoiture"}},{"text":"\nID marqueur: "},{"score":{"name":"@s","objective":"idMarqueur"}},{"text":"\nTags: "},{"entity":"@s","nbt":"Tags"}]}},{"text":"."}]}]

# Puis on supprime le tag temporaire du marqueur qui n'avait que cette utilité
tag @s remove RERMarkerTemp
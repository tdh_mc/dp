# On génère un ID
scoreboard players set #random min 10000
scoreboard players set #random max 99999
function tdh:random

# Si un marqueur ayant cet ID existe déjà, on génère un nouvel ID
execute as @e[type=piglin,tag=RERPlayerMarker] if score @s idMarqueur = #random temp run function tch:rer/rame/en_station/depart/ajouter_joueur/generer_id
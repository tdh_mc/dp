# On vérifie si notre position nous place à l'intérieur du RER

execute store result score #rerPassager posX run data get entity @s Pos[0] 100
execute store result score #rerPassager posY run data get entity @s Pos[1] 100
execute store result score #rerPassager posZ run data get entity @s Pos[2] 100

# Cette condition barbare vérifie si chaque pos(X/Y/Z) est située dans les bornes du RER calculées dans les fonctions précédentes
# On utilise > et < au lieu de >= et <= car les marqueurs de position eux-mêmes sont des items (donc des passager_rer), et on souhaite éviter de les inclure
execute if score #rerPassager posX > #rerVolMin posX if score #rerPassager posX < #rerVolMax posX if score #rerPassager posY > #rerVolMin posY if score #rerPassager posY < #rerVolMax posY if score #rerPassager posZ > #rerVolMin posZ if score #rerPassager posZ < #rerVolMax posZ at @s run function tch:rer/rame/en_station/depart/ajouter_passager
# On ajoute le joueur à la position de la fonction comme passager du RER dont l'ID est stocké dans #rer idVehicule
# Cette fonction est exécutée à la position de cette entité pour pouvoir savoir dans quelle voiture elle est

tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"]"},{"text":" : Ajout du joueur ","color":"gray","extra":[{"selector":"@p[gamemode=!spectator]","color":"yellow"},{"text":" aux passagers."}]}]

# On s'immobilise
# Désactivé pour l'instant car c'est compliqué à retirer ensuite
#attribute @p[gamemode=!spectator] minecraft:generic.knockback_resistance modifier add 00112233-4444-5555-6666-777788889999 "Dans un train" 1.0 add
#attribute @p[gamemode=!spectator] minecraft:generic.movement_speed modifier add 00112233-4444-5555-6666-777777888888 "Dans un train" -10000 add
effect give @p[gamemode=!spectator] resistance 999999 10 true
effect give @p[gamemode=!spectator] levitation 999999 0 true
# Lévitation est pratique car on monte quand même moins vite qu'on ne tombe et ça n'accélère pas avec le temps passé en l'air

# On enregistre notre ID véhicule, et notre ID voiture
scoreboard players operation @p[gamemode=!spectator] idVehicule = #rer idVehicule
scoreboard players operation @p[gamemode=!spectator] idVoiture = @e[type=armor_stand,tag=RER,distance=..30,sort=nearest,limit=1] idVoiture

# On se donne un tag signalant qu'on est passager, et on invoque un mob à notre position pour permettre de nous replacer au bon endroit à chaque tick
tag @p[gamemode=!spectator] add RERPassager
execute at @p[gamemode=!spectator] run function tch:rer/rame/en_station/depart/ajouter_joueur/invoquer_mob

# On enregistre notre gamemode actuel dans une variable pour pouvoir le restaurer en sortant du train
execute store result score @p[gamemode=!spectator] mode run data get entity @p[gamemode=!spectator] playerGameType
gamemode spectator @p[gamemode=!spectator]

# On enregistre le fait qu'on vient d'ajouter un joueur
scoreboard players add #rerPassager temp 1
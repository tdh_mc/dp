# On copie le nom du joueur taggé avec le tag "RERPlayerNameTemp" sur l'entité qui exécute cette fonction
# On est obligé de le faire en deux étapes pour pouvoir "interpréter" le nom ;
# On doit aussi exécuter cette fonction à la position d'un panneau qui pourra stocker et interpréter le nom du joueur (de préférence le panneau de tag "TexteTemporaire" généré à l'initialisation de TCH)
data modify block ~ ~ ~ Text1 set value '{"selector":"@p[tag=RERPlayerNameTemp]","color":"yellow"}'
data modify entity @s CustomName set from block ~ ~ ~ Text1
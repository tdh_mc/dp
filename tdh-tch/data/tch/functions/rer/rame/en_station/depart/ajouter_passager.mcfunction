# On ajoute l'entité qui exécute la fonction comme passager du RER dont l'ID est stocké dans #rer idVehicule
# Cette fonction est exécutée à la position de cette entité pour pouvoir savoir dans quelle voiture elle est

# On s'immobilise
data modify entity @s NoAI set value 1b
data modify entity @s Invulnerable set value 1b

# On enregistre notre ID véhicule, et notre ID voiture
scoreboard players operation @s idVehicule = #rer idVehicule
scoreboard players operation @s idVoiture = @e[type=armor_stand,tag=RER,distance=..30,sort=nearest,limit=1] idVoiture
# On supprime notre ID marqueur au cas où on serait re-rentrés depuis une station
scoreboard players reset @s idMarqueur

# On enregistre le fait qu'on est un passager
tag @s add RERPassager
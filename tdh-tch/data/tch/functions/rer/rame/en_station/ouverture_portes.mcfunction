# On exécute cette fonction en tant que la voiture de tête du RER

# On enregistre l'ID véhicule
scoreboard players operation #rer idVehicule = @s idVehicule

# On sélectionne toutes les entités appartenant au RER, qu'on teste ensuite dans une sous-fonction pour choisir le bon modèle de porte
# On a 2 versions de la commande selon qu'on fait descente à gauche (standard) ou à droite
execute as @s[tag=!DescenteDroite] as @e[type=armor_stand,tag=RER,distance=..30] if score @s idVehicule = #rer idVehicule at @s run function tch:rer/rame/en_station/ouverture_portes/test_gauche
execute as @s[tag=DescenteDroite] as @e[type=armor_stand,tag=RER,distance=..30] if score @s idVehicule = #rer idVehicule at @s run function tch:rer/rame/en_station/ouverture_portes/test_droite

# On joue le son d'ouverture
#execute positioned ~ ~3.3 ~ run function tch:sound/metro/station_doors_open
# On exécute cette fonction après avoir défini le score #rer idVehicule à l'ID que l'on souhaite supprimer

tellraw @a[tag=RERDebugLog] [{"text":"[RER ","color":"gold"},{"score":{"name":"@s","objective":"idVehicule"}},{"text":"/"},{"score":{"name":"@s","objective":"idVoiture"}},{"text":"]"},{"text":" : Destruction du véhicule."}]

# On trouve le parent du RER ayant ce numéro et on exécute le reste en tant que lui et à sa position
execute as @e[type=armor_stand,tag=RERTete] if score @s idVehicule = #rer idVehicule at @s run function tch:rer/rame/detruire/parent
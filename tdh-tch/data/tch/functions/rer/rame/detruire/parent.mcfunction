# On exécute cette fonction pour détruire le RER qui appelle cette fonction (à sa position)
# Le score #rer idVehicule doit également avoir été défini

# Si on est en station, on détruit les barrières situées autour de nous
execute as @s[tag=EnStation] as @e[type=armor_stand,tag=RERParent,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/en_station/depart/supprimer_barrieres

# Si on a une voie réservée, on la libère
execute as @s[tag=VoieReservee] run function tch:voie/reservation/depart
execute as @s[tag=VoieReservee] run function tch:station/pid/unlock_0_rer
# Si on est en file d'attente, on la libère également
execute as @s[tag=FileAttente] run function tch:voie/reservation/sortir_file_attente

# On supprime toutes les entités correspondantes
execute as @e[type=armor_stand,tag=RER,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/detruire/detruire_entite
execute as @e[type=armor_stand,tag=RERCamera,distance=..150] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/detruire/detruire_entite
# On tue tous les passagers non-joueurs qui avaient été invoqués avec le RER
execute as @e[type=#tch:passager_rer,tag=RERPassager,tag=PassagerInvoque,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/detruire/detruire_entite
# On rend leur liberté à tous les passagers joueurs
execute at @a[tag=RERPassager] if score @p[distance=0,tag=RERPassager] idVehicule = #rer idVehicule run function tch:rer/rame/en_station/arrivee/retirer_joueur
# On supprime tous les piglins temporaires associés à notre rame
execute as @e[type=piglin,tag=RERPlayerMarker,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/en_station/arrivee/supprimer_mob
# On rend leur liberté à tous les passagers non-joueurs
execute as @e[type=#tch:passager_rer,tag=RERPassager,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/en_station/arrivee/retirer_passager
# TICK DES RAMES DE RER
# Cette fonction est exécutée à chaque tick pour ticker ce qui concerne les rames de RER

# On ticke chaque voiture de tête pour prendre en compte mouvement, rotation et points de contrôle
execute as @e[type=armor_stand,tag=RERTete] at @s run function tch:rer/rame/tick_rame

# On ticke chaque joueur pour le repositionner à la position de l'entité qui matérialise sa position
execute at @a[tag=RERPassager,tag=!RERCamera] run function tch:rer/rame/passager/tick_inside
# On ticke chaque caméra pour repositionner les joueurs qui sont en mode caméra à sa position
execute as @e[type=armor_stand,tag=RERCamera,tag=!NoUpdate] at @s run function tch:rer/rame/passager/tick_camera
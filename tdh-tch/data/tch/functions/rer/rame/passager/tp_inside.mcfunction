# On exécute cette fonction à la position d'un joueur qu'on souhaite téléporter,
# en tant que le marqueur sur lequel on souhaite le téléporter et le mettre en spectateur

# On téléporte le joueur à la bonne position
tp @p[tag=RERTpTemp] @s
# On le (re)met en spectateur de son entité
spectate @s @p[tag=RERTpTemp]
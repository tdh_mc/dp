# TICK DES CAMÉRAS DES RER
# Cette fonction est exécutée à chaque tick par une armor stand RERCamera, à sa propre position et rotation,
# pour replacer les joueurs en mode caméra à cette position et rotation

# On avance d'un bloc si on se trouve dans un mur et qu’on a pas un angle de caméra imposé
execute unless entity @s[tag=ForcedCamera] unless block ~ ~1 ~ #tch:camera_passable run tp @s ^ ^ ^1.42 ~ ~
# On recentre notre vue sur la voiture la plus proche de nous si on a un angle imposé et fixe
execute if entity @s[tag=FixedCamera,tag=!RegarderQueue] run function tch:rer/rame/camera/recentrer
execute if entity @s[tag=FixedCamera,tag=RegarderQueue] run function tch:rer/rame/camera/recentrer_queue

# On trouve les joueurs qui correspondent, puis on les téléporte à notre position
scoreboard players operation #rer idVehicule = @s idVehicule
execute positioned as @a[tag=RERCamera] if score @p[tag=RERCamera] idVehicule = #rer idVehicule run tp @a[tag=RERCamera,distance=0] @s

# On réajuste ensuite la position/rotation de tous ces joueurs
execute at @s run tp @a[distance=0] ~ ~ ~ ~ ~
# Comme la commande spectate n'accepte pas plusieurs joueurs, on tente d'en prendre juste un random à chaque fois
# Comme il y a rarement 50 joueurs par RER et qu'ils ne passent pas leur vie à tenter de sortir du mode spectateur, ça devrait suffire
spectate @s @r[distance=0]

# Si on a un angle de caméra imposé ET fixe et qu’on est trop loin, on retire le tag qui nous impose de rester fixe
execute if entity @s[tag=ForcedCamera] run function tch:rer/rame/camera/test_fixe
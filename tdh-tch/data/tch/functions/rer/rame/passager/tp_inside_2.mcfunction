# On exécute cette fonction à la position d'un joueur qu'on vient de téléporter à la position d'un marqueur,
# en tant que ce marqueur (mais avec la rotation du joueur !)
tp @p[tag=RERTpTemp] ~ ~.1 ~ ~ ~

# On retire le tag temporaire du joueur
tag @a[tag=RERTpTemp] remove RERTpTemp
# TICK DES JOUEURS DANS LES RAMES DE RER
# Cette fonction est exécutée à chaque tick pour replacer le joueur à la position duquel on se trouve

# On tag le joueur le plus proche
tag @p add RERTpTemp

# On trouve l'entité sur laquelle on doit se tp, puis on se téléporte dessus
scoreboard players operation #rer idMarqueur = @p[tag=RERTpTemp] idMarqueur
execute as @e[type=piglin,tag=RERPlayerMarker,distance=..30] if score @s idMarqueur = #rer idMarqueur run function tch:rer/rame/passager/tp_inside

# On supprime le tag du joueur
tag @p[tag=RERTpTemp] remove RERTpTemp

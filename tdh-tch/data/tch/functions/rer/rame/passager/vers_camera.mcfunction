# On exécute cette fonction pour passer tous les joueurs d'un RER en caméra cinématique
# La variable #rer idVehicule doit être définie

# On tag les joueurs qui partagent notre idVehicule
execute at @a[tag=RERPassager] if score @p[tag=RERPassager] idVehicule = #rer idVehicule run tag @a[distance=0,tag=RERPassager] add RERCamera


# On cache les modèles de l'arrière des voitures
execute as @e[type=armor_stand,tag=RERTeteBack,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/mouvement/cacher_modeles
execute as @e[type=armor_stand,tag=RERQueue,distance=..30] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/mouvement/cacher_modeles
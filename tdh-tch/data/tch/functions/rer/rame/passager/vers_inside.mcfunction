# On exécute cette fonction pour passer tous les joueurs d'un RER en caméra intérieure
# La variable #rer idVehicule doit être définie

# On détag les joueurs qui partagent notre idVehicule
execute at @a[tag=RERCamera] if score @p[tag=RERCamera] idVehicule = #rer idVehicule run tag @a[distance=0,tag=RERCamera] remove RERCamera


# On affiche à nouveau les modèles de l'arrière des voitures
execute as @e[type=armor_stand,tag=RERTeteBack,distance=..150] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/mouvement/afficher_modeles/tete
execute as @e[type=armor_stand,tag=RERQueue,distance=..150] if score @s idVehicule = #rer idVehicule run function tch:rer/rame/mouvement/afficher_modeles/queue
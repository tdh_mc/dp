# Fix à l’arrache du bug MC-103800
# On re-fixe la rotation de toutes les entités ayant le tag ReparerRotation
execute as @e[type=armor_stand,tag=RER,tag=RotationTemporaire2] at @s run function tch:rer/rame/arret/reparer_rotation3
execute as @e[type=armor_stand,tag=RER,tag=RotationTemporaire] at @s run function tch:rer/rame/arret/reparer_rotation2

execute if entity @e[type=armor_stand,tag=RER,tag=RotationTemporaire2] run schedule function tch:rer/rame/arret/reparer_rotation_test 3t replace
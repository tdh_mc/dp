# Fix à l’arrache du bug MC-103800
# On répare la rotation et on se donne un tag
tag @s add RotationTemporaire

# On schedule la seconde partie de la réparation dans 3 ticks
schedule function tch:rer/rame/arret/reparer_rotation_test 3t replace
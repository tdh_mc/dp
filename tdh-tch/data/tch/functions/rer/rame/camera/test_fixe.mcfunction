# On teste si une caméra fixe (qui exécute cette fonction à sa propre position) est trop loin de son RER
execute as @e[type=armor_stand,tag=RERTete,distance=..150] if score @s idVehicule = #rer idVehicule run tag @s add ourVehicule

# Si elle est trop loin, on retire le tag fixed
execute at @e[type=armor_stand,tag=ourVehicule,distance=..150,limit=1] unless entity @s[distance=..120] run function tch:rer/rame/camera/fin_fixe

# On retire le tag temporaire
tag @e[type=armor_stand,tag=ourVehicule,distance=..150] remove ourVehicule
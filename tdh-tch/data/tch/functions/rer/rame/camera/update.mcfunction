# On exécute cette fonction régulièrement pour changer la vue de toutes les caméras de RER
# On ne prend qu'un seul nombre aléatoire car on ne va pas non plus s'emmerder
scoreboard players set #random min 0
scoreboard players set #random max 20
function tdh:random

# On exécute la fonction de repositionnement pour chaque caméra, en fonction de la position
# sélectionnée par notre variable aléatoire

# On n’inclut pas les caméras qui ont le tag ForcedCamera
# (cela signifie qu’un tronçon de voie a défini un angle de caméra)

# Intérieur = vue des passagers à l'intérieur de la voiture, depuis le "fond" de celle-ci
# (l'arrière si on est la voiture de tête ou centrale, et l'avant tourné à 180 si on est la voiture de queue)
execute if score #random temp matches 0 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/interieur_tete
execute if score #random temp matches 1 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/interieur_queue
# Plan américain = comme au cinéma, coupé à mi-hauteur (en l'occurence à mi-longueur du RER)
# Centré sur l'avant du train et tourné à 45° en regardant vers l'avant
execute if score #random temp matches 2 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/americain_droite
execute if score #random temp matches 3 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/americain_gauche
# Vue du dessus, à la GTA classique
execute if score #random temp matches 4 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/dessus
# Vue de côté, toujours centrée sur l'avant du train
execute if score #random temp matches 5 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/cote_droit
execute if score #random temp matches 6 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/cote_gauche
# Vues éloignées, toujours centrées sur l'avant du train (à 45° de tous les angles)
execute if score #random temp matches 7 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/eloigne_avant_droit
execute if score #random temp matches 8 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/eloigne_avant_gauche
execute if score #random temp matches 9 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/eloigne_arriere_droit
execute if score #random temp matches 10 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/eloigne_arriere_gauche
# Plan de face, rapproché
execute if score #random temp matches 11 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/face
# Vues éloignées du dessus, toujours centrées sur l'avant du train (à 45° de tous les angles)
execute if score #random temp matches 12 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/eloigne_avant_droit_haut
execute if score #random temp matches 13 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/eloigne_avant_gauche_haut
execute if score #random temp matches 14 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/eloigne_arriere_droit_haut
execute if score #random temp matches 15 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/eloigne_arriere_gauche_haut
# Intérieur cockpit
execute if score #random temp matches 16 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/interieur_cockpit
# Pantographe avant
execute if score #random temp matches 17 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/pantographe_avant
# Collé arrière droite/gauche
execute if score #random temp matches 18 as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/colle_arriere_droite
execute if score #random temp matches 19.. as @e[type=armor_stand,tag=RERCamera,tag=!ForcedCamera] at @s run function tch:rer/rame/camera/update/colle_arriere_gauche
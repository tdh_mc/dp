# On tag l'armor stand correspondant à notre ID véhicule
scoreboard players operation #rer idVehicule = @s idVehicule
function tch:rer/rame/camera/tag/tete_back

# On se positionne à la bonne position relativement à notre armor stand
execute at @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150,limit=1] rotated ~ 90 run tp @s ^ ^ ^-30 ~ ~

# On se donne l'ID de voiture correspondant (récupéré dans la fonction tag/ ci-dessus)
scoreboard players operation @s idVoiture = #rer idVoiture

# On supprime le tag temporaire
tag @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150] remove RERCameraTempTarget


# Si on est dans un bloc, on essaie avec un autre angle de caméra
execute at @s unless block ~ ~1 ~ #tch:camera_passable run function tch:rer/rame/camera/update/face
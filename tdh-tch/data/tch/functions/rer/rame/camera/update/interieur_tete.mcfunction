# On tag l'armor stand correspondant à notre ID véhicule
scoreboard players operation #rer idVehicule = @s idVehicule
function tch:rer/rame/camera/tag/tete_back

# On se positionne en haut à gauche de notre armor stand
execute at @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150,limit=1] run tp @s ^1.4 ^1 ^ ~35 ~25

# On se donne l'ID de voiture correspondant (récupéré dans la fonction tag/ ci-dessus)
scoreboard players operation @s idVoiture = #rer idVoiture

# On supprime le tag temporaire
tag @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150] remove RERCameraTempTarget
# On tag l'armor stand correspondant à notre ID véhicule
scoreboard players operation #rer idVehicule = @s idVehicule
function tch:rer/rame/camera/tag/tete

# On se positionne à la position de notre armor stand, tournés à 180° pour regarder vers l'arrière
# On ne peut pas simplement se tourner à "~180 ~", car sinon on regarderait dans le mauvais sens verticalement quand on est en pente (vers le haut quand on devrait regarder vers le bas et vice-versa)
execute at @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150,limit=1] positioned ^ ^ ^1 facing entity @e[type=armor_stand,tag=RERCameraTempTarget,distance=..2,limit=1] feet run tp @s ^3 ^1.5 ^-5 ~50 ~5

# On se donne l'ID de voiture correspondant (récupéré dans la fonction tag/ ci-dessus)
scoreboard players operation @s idVoiture = #rer idVoiture

# On supprime le tag temporaire
tag @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150] remove RERCameraTempTarget


# Si on est dans un bloc, on essaie avec un autre angle de caméra
execute at @s unless block ~ ~1 ~ #tch:camera_passable run function tch:rer/rame/camera/update/interieur_queue
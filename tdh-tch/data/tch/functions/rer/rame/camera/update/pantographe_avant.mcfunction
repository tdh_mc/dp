# On tag l'armor stand correspondant à notre ID véhicule
scoreboard players operation #rer idVehicule = @s idVehicule
function tch:rer/rame/camera/tag/tete

# On se positionne à droite du pantographe avant
execute at @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150,limit=1] run tp @s ^-2.5 ^2.2 ^-7.5 ~-27 ~-5

# On se donne l'ID de voiture correspondant (récupéré dans la fonction tag/ ci-dessus)
scoreboard players operation @s idVoiture = #rer idVoiture

# On supprime le tag temporaire
tag @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150] remove RERCameraTempTarget
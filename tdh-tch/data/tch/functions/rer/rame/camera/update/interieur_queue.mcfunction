# On tag l'armor stand correspondant à notre ID véhicule
scoreboard players operation #rer idVehicule = @s idVehicule
function tch:rer/rame/camera/tag/queue

# On se positionne à la position de notre armor stand, tournés à 180° pour regarder vers l'arrière
# On ne peut pas simplement se tourner à "~180 ~", car sinon on regarderait dans le mauvais sens verticalement quand on est en pente (vers le haut quand on devrait regarder vers le bas et vice-versa)
execute at @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150,limit=1] positioned ^ ^ ^1 facing entity @e[type=armor_stand,tag=RERCameraTempTarget,distance=..2,limit=1] feet run tp @s ^1.47 ^1.08 ^0.6 ~42 ~30

# On se donne l'ID de voiture correspondant (récupéré dans la fonction tag/ ci-dessus)
scoreboard players operation @s idVoiture = #rer idVoiture

# On supprime le tag temporaire
tag @e[type=armor_stand,tag=RERCameraTempTarget,distance=..150] remove RERCameraTempTarget
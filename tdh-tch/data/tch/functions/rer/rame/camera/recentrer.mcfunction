# Fonction exécutée pour recentrer la caméra qui l’exécute sur la voiture de son RER la plus proche d’elle
# On tag toutes les voitures de notre véhicule
scoreboard players operation #rer idVehicule = @s idVehicule
execute as @e[type=armor_stand,tag=RERTete,distance=..150] if score @s idVehicule = #rer idVehicule run tag @s add OurRER

# On recentre notre position sur la plus proche
execute facing entity @e[type=armor_stand,tag=OurRER,distance=..150,limit=1] feet run tp @s ~ ~ ~ ~ ~

# On supprime les tags temporaires
tag @e[type=armor_stand,tag=OurRER,distance=..150] remove OurRER
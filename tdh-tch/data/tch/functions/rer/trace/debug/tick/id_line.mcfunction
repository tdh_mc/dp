# On invoque un item pour afficher les infos, puis on exécute le reste dans une sous-fonction à la position d’un TexteTemporaire
summon item ~ ~0.65 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem","TRERDI_Signal"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,Item:{id:"polished_blackstone_button",Count:1b,tag:{a:666}}}
execute unless score @s idLineMax matches 1.. unless score @s prochaineLigne matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/trace/debug/tick/id_line_texte
execute unless score @s idLineMax matches 1.. if score @s prochaineLigne matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/trace/debug/tick/id_line_next_texte
execute if score @s idLineMax matches 1.. unless score @s prochaineLigne matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/trace/debug/tick/id_line_max_texte
execute if score @s idLineMax matches 1.. if score @s prochaineLigne matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/trace/debug/tick/id_line_max_next_texte

# Enfin, on retire les tags temporaires
tag @e[type=item,tag=TRERDI_Signal] remove TRERDI_Signal
# On exécute cette fonction à la position d’une item frame TexteTemporaire pour afficher les ID de signaux

# On enregistre l’ID de caméra dans le panneau
data modify block ~ ~ ~ Text1 set value '[{"text":"Caméra fixe ","color":"#a56af0"},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=TRERDI_Parent,limit=1]","objective":"idCamera"},"bold":"true"}]'

# On définit le CustomName de l’item invoqué
data modify entity @e[type=item,tag=TRERDI_Signal,limit=1] CustomName set from block ~ ~ ~ Text1
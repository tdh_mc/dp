# On exécute cette fonction à la position d’une item frame TexteTemporaire pour afficher la vitesse max d’un spawner

# On enregistre l’ID de ligne dans le panneau
execute unless score @s vitesse matches 1.. run data modify block ~ ~ ~ Text1 set value '[{"text":"","color":"gold"},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=TRERDI_Parent,limit=1]","objective":"vitesseMax"},"bold":"true"},{"text":" m/s"}]'
execute if score @s vitesse matches 1.. run data modify block ~ ~ ~ Text1 set value '[{"text":"","color":"gold"},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=TRERDI_Parent,limit=1]","objective":"vitesseMax"},"bold":"true"},{"text":" m/s ("},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=TRERDI_Parent,limit=1]","objective":"vitesse"}},{"text":")"}]'

# On définit le CustomName de l’item invoqué
data modify entity @e[type=item,tag=TRERDI_Signal,limit=1] CustomName set from block ~ ~ ~ Text1
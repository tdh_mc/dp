# On invoque un item pour afficher les infos, puis on exécute le reste dans une sous-fonction à la position d’un TexteTemporaire
summon item ~ ~0.65 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem","TRERDI_Signal"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,Item:{id:"polished_blackstone_button",Count:1b,tag:{a:3}}}
execute if score @s idLine matches 1.. unless score @s idLineMax matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/trace/debug/tick/direction_texte
execute if score @s idLine matches 1.. if score @s idLineMax matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/trace/debug/tick/direction_multiple_texte
execute unless score @s idLine matches 1.. run data modify entity @e[type=item,tag=TRERDI_Signal,limit=1] CustomName set value '[{"text":"[Direction ???]","color":"red"}]'

# Enfin, on retire les tags temporaires
tag @e[type=item,tag=TRERDI_Signal] remove TRERDI_Signal
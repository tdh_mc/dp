# On invoque un item pour afficher les infos, puis on exécute le reste dans une sous-fonction à la position d’un TexteTemporaire
summon item ~ ~0.0 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem","TRERDI_Signal"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,Item:{id:"polished_blackstone_button",Count:1b,tag:{a:-1000}}}
execute if score @s vitesseMax matches 1.. at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:rer/trace/debug/tick/vitesse_texte
execute unless score @s vitesseMax matches 1.. run data modify entity @e[type=item,tag=TRERDI_Signal,limit=1] CustomName set value '[{"text":"-----","color":"yellow"}]'

# Enfin, on retire les tags temporaires
tag @e[type=item,tag=TRERDI_Signal] remove TRERDI_Signal
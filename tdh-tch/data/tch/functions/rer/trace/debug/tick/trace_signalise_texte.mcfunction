# On exécute cette fonction à la position d’une item frame TexteTemporaire pour afficher les ID de signaux

# On enregistre l’ID de signal et de prochain signal dans le panneau
data modify block ~ ~ ~ Text1 set value '[{"text":"[","color":"#bfaa96"},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=TRERDI_Parent,limit=1]","objective":"idSignal"},"bold":"true"},{"text":"→"},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=TRERDI_Parent,limit=1]","objective":"prochainSignal"}},{"text":"]"}]'

# On définit le CustomName de l’item invoqué
data modify entity @e[type=item,tag=TRERDI_Signal,limit=1] CustomName set from block ~ ~ ~ Text1
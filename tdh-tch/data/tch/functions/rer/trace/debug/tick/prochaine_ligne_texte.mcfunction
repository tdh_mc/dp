# On exécute cette fonction à la position d’une item frame TexteTemporaire pour afficher l’ID de ligne d’un spawner

# On enregistre l’ID de ligne dans le panneau
data modify block ~ ~ ~ Text1 set value '[{"text":"[ProchaineLigne ","color":"gold"},{"score":{"name":"@e[type=#tch:marqueur/signal,tag=TRERDI_Parent,limit=1]","objective":"prochaineLigne"},"bold":"true"},{"text":"]"}]'

# On définit le CustomName de l’item invoqué
data modify entity @e[type=item,tag=TRERDI_Signal,limit=1] CustomName set from block ~ ~ ~ Text1
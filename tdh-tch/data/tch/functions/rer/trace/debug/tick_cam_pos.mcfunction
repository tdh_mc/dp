# On est une armor stand RERCameraPosition à sa position
# En fonction de nos scores, on invoque des items temporaires :

# On se donne un tag pour se retrouver
tag @s add TRERDI_Parent

execute as @s[tag=FixedCamera] run function tch:rer/trace/debug/tick/fixed_camera_position
execute as @s[tag=!FixedCamera] run function tch:rer/trace/debug/tick/camera_position

execute as @s[tag=RegarderQueue] run summon item ~ ~0.45 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"RegarderQueue","color":"#e1a6f6"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:387}}}

# Enfin, on retire notre tag temporaire
tag @s remove TRERDI_Parent
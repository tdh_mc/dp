# On est une armor stand TraceRER à sa position
# En fonction de nos scores, on invoque des items temporaires :

# On se donne un tag pour se retrouver
tag @s add TRERDI_Parent

# Pour afficher notre vitesse
function tch:rer/trace/debug/tick/vitesse

# On invoque des items supplémentaires pour les tags
execute as @s[tag=AlerteArriveeStation] run summon item ~ ~0.15 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"AlerteArriveeStation","color":"red"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:0}}}
execute as @s[tag=ArriveeStation] run summon item ~ ~0.15 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"ArriveeStation","color":"red"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:1}}}
execute as @s[tag=FinCamera] run summon item ~ ~0.15 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"FinCamera","color":"red"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:6}}}

execute as @s[tag=PreStation] run summon item ~ ~0.15 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"PreStation","color":"gold"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:2}}}
execute as @s[tag=ProchaineStation] run function tch:rer/trace/debug/tick/prochaine_station
execute as @s[tag=Detruire] run summon item ~ ~0.15 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"Detruire","color":"gold"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:5}}}

execute as @s[tag=DescenteDroite] run summon item ~ ~0.25 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"DescenteDroite","color":"gold"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:100}}}
execute as @s[tag=ProchaineLigne] run function tch:rer/trace/debug/tick/prochaine_ligne

execute as @s[tag=Corresp] run summon item ~ ~0.35 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"Corresp","color":"red"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:200}}}
execute as @s[tag=Retournement] run summon item ~ ~0.35 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"Retournement","color":"gold"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:260}}}

execute as @s[tag=NoAlign] run summon item ~ ~0.45 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"NoAlign","color":"gold"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:300}}}

execute as @s[tag=SpawnRER] run function tch:rer/trace/debug/tick/spawner
execute as @s[tag=Direction] run function tch:rer/trace/debug/tick/direction
execute as @s[tag=!SpawnRER,tag=!Direction,scores={idLine=1..}] run function tch:rer/trace/debug/tick/id_line
execute as @s[tag=ClearCamera] run summon item ~ ~0.65 ~ {NoGravity:1b,Invulnerable:1b,Tags:["TraceRERDebugItem"],CustomNameVisible:1b,PickupDelay:32767s,Age:5750s,CustomName:'[{"text":"ClearCamera","color":"#a56af0"}]',Item:{id:"polished_blackstone_button",Count:1b,tag:{a:69}}}
execute as @s[tag=ForceCamera] run function tch:rer/trace/debug/tick/force_camera

execute as @s[tag=Signal] run function tch:rer/trace/debug/tick/signal
execute as @s[tag=!Signal,scores={idSignal=1..}] run function tch:rer/trace/debug/tick/trace_signalise

# Enfin, on retire notre tag temporaire
tag @s remove TRERDI_Parent
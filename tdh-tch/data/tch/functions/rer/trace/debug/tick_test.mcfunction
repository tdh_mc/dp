# On supprime tous les items précédents
kill @e[type=item,tag=TraceRERDebugItem]

# On vérifie si le système de debug est activé, et si oui on exécute le tick pour chaque TraceRER
execute if score #rerTraceDebug on matches 1 as @e[type=armor_stand,tag=TraceRER,nbt={Small:1b}] at @s run function tch:rer/trace/debug/tick
execute if score #rerTraceDebug on matches 1 as @e[type=armor_stand,tag=TraceRER,nbt={Small:0b}] at @s positioned ^ ^ ^1 run function tch:rer/trace/debug/tick
execute if score #rerTraceDebug on matches 1 as @e[type=armor_stand,tag=RERCameraPosition] at @s run function tch:rer/trace/debug/tick_cam_pos
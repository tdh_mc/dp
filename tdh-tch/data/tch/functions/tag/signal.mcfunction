# Fonction utilitaire exécutée pour récupérer le signal correspondant à l’ID de signal de l’entité qui l’appelle

# On enregistre l'ID de signal
scoreboard players operation #tch idSignal = @s idSignal

# On trouve le signal
execute as @e[type=#tch:marqueur/signal,tag=Signal] if score #tch idSignal = @s idSignal run tag @s add ourSignal

# On supprime la variable temporaire
scoreboard players reset #tch idSignal
# Fonction utilitaire exécutée pour récupérer le·s signal·aux menant à l’ID de signal de l’entité qui l’appelle
# (c’est-à-dire les signaux ayant comme **prochainSignal** l’ID de signal de l’entité qui l’appelle)

# On enregistre l'ID de signal
scoreboard players operation #tch idSignal = @s idSignal

# On trouve le signal
execute as @e[type=#tch:marqueur/signal,tag=Signal] if score #tch idSignal = @s prochainSignal run tag @s add ourPreviousSignal

# On supprime la variable temporaire
scoreboard players reset #tch idSignal
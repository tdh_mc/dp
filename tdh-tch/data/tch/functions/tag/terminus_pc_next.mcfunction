# Fonction utilitaire exécutée pour récupérer le PC Quai de notre PROCHAINE ligne à son terminus
# On doit appeler cette fonction en tant qu'une entité qui possède un ID de prochaine ligne

# On enregistre l'ID line
scoreboard players operation #tch idTerminus = @s prochaineLigne

# On trouve le PC du terminus
execute as @e[type=item_frame,tag=tchQuaiPC] if score #tch idTerminus = @s idTerminus run tag @s add ourTerminusPC
execute unless entity @e[type=item_frame,tag=ourTerminusPC] as @e[type=item_frame,tag=tchQuaiPC] if score #tch idTerminus > @s idTerminus if score #tch idTerminus <= @s idTerminusMax run tag @s add ourTerminusPC

# On supprime la variable temporaire
scoreboard players reset #tch idTerminus
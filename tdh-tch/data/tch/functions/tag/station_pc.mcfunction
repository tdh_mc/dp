# Fonction utilitaire exécutée pour récupérer le PC Station de la station correspondante
# On doit appeler cette fonction en tant qu'une entité qui possède l'ID station qu'on recherche

# On enregistre l'ID line et l'ID station
scoreboard players operation #tch idStation = @s idStation

# On trouve le PC
execute as @e[type=item_frame,tag=tchStationPC] if score #tch idStation = @s idStation run tag @s add ourStationPC
execute as @e[type=item_frame,tag=tchLieuDitPC] if score #tch idStation = @s idStation run tag @s add ourStationPC

# On supprime la variable temporaire
scoreboard players reset #tch idStation
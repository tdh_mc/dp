# Fonction utilitaire exécutée pour récupérer le quai de notre ligne
# On doit appeler cette fonction en tant qu'une entité qui possède l'ID line qu'on recherche, à sa position

# On enregistre l'ID line et l'ID station le plus proche de nous (le nôtre si on en possède un)
scoreboard players operation #tch idLine = @s idLine
scoreboard players operation #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation
scoreboard players operation #tch idStation = @s[scores={idStation=1..}] idStation

# On trouve la voie correspondante si elle se trouve dans la même station que nous
# (évitera probablement une partie des soucis récurrents de spawn entre Le Hameau et les stations trop proches).
execute as @e[type=#tch:marqueur/quai,tag=Direction,distance=..200] at @s if score #tch idLine = @s idLine if score #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation run tag @e[type=#tch:marqueur/quai,tag=NumLigne,distance=..6,sort=nearest,limit=1] add ourQuai

# On supprime les variables temporaires
scoreboard players reset #tch idLine
scoreboard players reset #tch idStation
# Fonction utilitaire exécutée pour récupérer la voie correspondant à notre ID line
# On doit appeler cette fonction en tant qu'une entité qui possède l'ID line qu'on recherche, à sa position

# On enregistre l'ID line et l'ID station le plus proche de nous (le nôtre si on en possède un)
scoreboard players operation #tch idLine = @s idLine
scoreboard players operation #tch prochaineLigne = @s prochaineLigne
scoreboard players operation #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation
scoreboard players operation #tch idStation = @s[scores={idStation=1..}] idStation

# On trouve la voie correspondante si elle se trouve dans la même station que nous
# (évitera probablement une partie des soucis récurrents de spawn entre Le Hameau et les stations trop proches).
execute as @e[type=#tch:marqueur/quai,tag=Direction,distance=..200] at @s if score #tch idLine = @s idLine if score #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation run tag @s add ourVoie

# Si on n'a pas trouvé notre voie, on essaie d'en trouver une qui aurait un idLine et un idLineMax
execute unless entity @e[type=#tch:marqueur/quai,tag=ourVoie] as @e[type=#tch:marqueur/quai,tag=Direction,distance=..200] if score #tch idLine <= @s idLineMax if score #tch idLine >= @s idLine if score #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation run tag @s add ourVoie

# Si on a trouvé une ou plusieurs voies et qu’on a un score prochaineLigne,
# on retire les voies qui ont un ID prochaineLigne différent
execute if score #tch prochaineLigne matches 1.. as @e[type=#tch:marqueur/quai,tag=ourVoie] if score @s prochaineLigne matches 1.. unless score @s prochaineLigne = #tch prochaineLigne run tag @s remove ourVoie

# On supprime les variables temporaires
scoreboard players reset #tch idLine
scoreboard players reset #tch prochaineLigne
scoreboard players reset #tch idStation
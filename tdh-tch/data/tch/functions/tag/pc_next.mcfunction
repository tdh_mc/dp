# Fonction utilitaire exécutée pour récupérer le PC de notre PROCHAINE ligne
# On doit appeler cette fonction en tant qu'une entité qui possède l'ID de prochaine ligne qu'on recherche

# On enregistre l'ID line
scoreboard players operation #tch idLine = @s prochaineLigne

# On trouve le PC
execute as @e[type=item_frame,tag=tchPC] if score #tch idLine >= @s idLine if score #tch idLine <= @s idLineMax run tag @s add ourPC

# On supprime la variable temporaire
scoreboard players reset #tch idLine
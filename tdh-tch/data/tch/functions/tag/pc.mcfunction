# Fonction utilitaire exécutée pour récupérer le PC de notre ligne
# On doit appeler cette fonction en tant qu'une entité qui possède l'ID line qu'on recherche

# On enregistre l'ID line s’il n’a pas été défini avant l’appel à la fonction
execute unless score #tch idLine matches 1.. run scoreboard players operation #tch idLine = @s idLine

# On trouve le PC
execute as @e[type=item_frame,tag=tchPC] if score #tch idLine >= @s idLine if score #tch idLine <= @s idLineMax run tag @s add ourPC

# On supprime la variable temporaire
scoreboard players reset #tch idLine
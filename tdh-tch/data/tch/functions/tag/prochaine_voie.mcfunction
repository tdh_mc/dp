# Fonction utilitaire exécutée pour récupérer la voie correspondant à notre ID line
# On doit appeler cette fonction en tant qu'une entité qui possède l'ID line qu'on recherche, à sa position

# On enregistre l'ID line et l'ID de prochaine station de l'entité qui exécute cette fonction
scoreboard players operation #tch idLine = @s idLine
scoreboard players operation #tch idStation = @s prochaineStation

# On trouve la voie correspondante si elle se trouve dans la même station que nous
# (évitera probablement une partie des soucis récurrents de spawn entre Le Hameau et les stations trop proches).
execute as @e[type=#tch:marqueur/quai,tag=Direction,distance=..200] at @s if score #tch idLine = @s idLine if score #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation run tag @s add ourVoie

# Si on n'a pas trouvé notre voie, on essaie d'en trouver une qui aurait un idLine et un idLineMax
execute unless entity @e[type=#tch:marqueur/quai,tag=ourVoie] as @e[type=#tch:marqueur/quai,tag=Direction,distance=..200] if score #tch idLine <= @s idLineMax if score #tch idLine >= @s idLine if score #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation run tag @s add ourVoie

# On supprime les variables temporaires
scoreboard players reset #tch idLine
scoreboard players reset #tch idStation
# Fonction utilitaire exécutée pour récupérer le spawner correspondant à notre ID line
# On doit appeler cette fonction en tant qu'une entité qui possède l'ID line qu'on recherche, à sa position

# On enregistre l'ID line et l'ID station le plus proche de nous (le nôtre si on en possède un)
scoreboard players operation #tch idLine = @s idLine
scoreboard players operation #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation
scoreboard players operation #tch idStation = @s[scores={idStation=1..}] idStation

# On trouve le spawner correspondant s'il se trouve dans la même station que nous
execute as @e[type=#tch:marqueur/spawner,tag=SpawnRER,distance=..200] at @s if score #tch idLine = @s idLine if score #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation run tag @s add ourSpawner
execute as @e[type=#tch:marqueur/spawner,tag=SpawnCable,distance=..200] at @s if score #tch idLine = @s idLine if score #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation run tag @s add ourSpawner
execute as @e[type=#tch:marqueur/spawner,tag=SpawnMetro,distance=..200] at @s if score #tch idLine = @s idLine if score #tch idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation run tag @s add ourSpawner

# On supprime les variables temporaires
scoreboard players reset #tch idLine
scoreboard players reset #tch idStation
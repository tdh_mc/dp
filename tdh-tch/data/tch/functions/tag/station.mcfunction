# Fonction utilitaire exécutée pour récupérer le NomStation qui correspond à notre ID
# On doit appeler cette fonction en tant qu'une entité qui possède l'ID station qu'on recherche, à sa position

# On enregistre notre ID station
scoreboard players operation #tch idStation = @s[scores={idStation=1..}] idStation

# On trouve la station correspondante
execute as @e[type=#tch:marqueur/station,tag=NomStation,distance=..250] if score #tch idStation = @s idStation run tag @s add ourStation

# On supprime les variables temporaires
scoreboard players reset #tch idStation
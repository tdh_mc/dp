# Fonction utilitaire exécutée pour récupérer le PC Quai de notre ligne à la station correspondante
# On doit appeler cette fonction en tant qu'une entité qui possède le combo d'ID line et station qu'on recherche, l'ID station étant stocké dans prochaineStation plutôt que dans idStation

# On enregistre l'ID line et l'ID station
scoreboard players operation #tch idLine = @s idLine
scoreboard players operation #tch idStation = @s prochaineStation

# On trouve le PC
execute as @e[type=item_frame,tag=tchQuaiPC] if score #tch idStation = @s idStation if score #tch idLine >= @s idLine if score #tch idLine <= @s idLineMax run tag @s add ourQuaiPC

# On supprime la variable temporaire
scoreboard players reset #tch idLine
scoreboard players reset #tch idStation
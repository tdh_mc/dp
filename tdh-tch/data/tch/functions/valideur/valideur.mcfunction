# Valideur TCH
# Obsolète !

# On affiche un message d'erreur
tellraw @a[distance=..10] [{"text":"","color":"red"},{"text":"Erreur","color":"dark_red"},{"text":" : Ce valideur utilise une fonction obsolète ("},{"text":"tch:valideur/valideur","bold":"true"},{"text":" au lieu de "},{"text":"tch:station/valideur","bold":"true"},{"text":"). Merci de configurer correctement le valideur et de réessayer."}]
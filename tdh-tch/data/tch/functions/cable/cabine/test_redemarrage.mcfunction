# Si on est en fin de service, on vérifie si la ligne a rouvert
execute at @s[tag=FinDeService] run function tch:cable/cabine/test_redemarrage/fin_de_service
# Si on est en incident, on vérifie si c'est fini
execute at @s[tag=Incident] run function tch:cable/cabine/test_redemarrage/incident
# Si on est en arrêt d'urgence, on vérifie si on peut repartir
execute at @s[tag=ArretUrgence] run function tch:cable/cabine/test_redemarrage/arret_urgence

# Si on n'a plus aucun des tags, on peut redémarrer
tag @s[tag=!FinDeService,tag=!ArretUrgence,tag=!Incident] remove Interrompu

# Dans tous les cas, on réinitialise notre timer
scoreboard players set @s remainingTicks 0
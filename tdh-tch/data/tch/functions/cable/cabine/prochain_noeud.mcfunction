# On exécute cette fonction en tant qu'une cabine de câble à sa position, si on est au même endroit qu'un noeud du parcours

# On enregistre toutes les informations nécessaires
execute as @e[type=armor_stand,tag=TraceCable,distance=..0.5,limit=1] run function tch:cable/cabine/prochain_noeud/sauver_score
execute if score #cable deltaX matches -999.. run scoreboard players operation @s deltaX = #cable deltaX
execute if score #cable deltaY matches -999.. run scoreboard players operation @s deltaY = #cable deltaY
execute if score #cable deltaZ matches -999.. run scoreboard players operation @s deltaZ = #cable deltaZ
execute if score #cable vitesse matches 0.. run scoreboard players operation @s vitesse = #cable vitesse
execute if score #cable vitesseMax matches 0.. run scoreboard players operation @s vitesseMax = #cable vitesseMax
execute if score #cable idLine matches 0.. run scoreboard players operation @s idLine = #cable idLine
scoreboard players reset #cable deltaX
scoreboard players reset #cable deltaY
scoreboard players reset #cable deltaZ
scoreboard players reset #cable vitesse
scoreboard players reset #cable vitesseMax
scoreboard players reset #cable idLine

# On prend en compte les tags du noeud où l'on se trouve
execute if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=AlerteArriveeStation] run tag @s add PreStation
execute if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=ArriveeStation] run tag @s add ArriveeStation
execute if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=RotationCW] run tag @s add RotationCW
execute if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=RotationCCW] run tag @s add RotationCCW
execute if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=Station] run function tch:cable/cabine/station
execute if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=ProchaineStation] run function tch:cable/cabine/prochaine_station
execute if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=RetirerBarriere] run function tch:cable/cabine/barriere/retirer
execute if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=PlacerBarriere] run function tch:cable/cabine/barriere/placer

# Si on a besoin d'un nouveau modèle, on le récupère
execute as @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=NouveauModele] run function tch:cable/cabine/remplacer_modele
execute as @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=AjouterModele] run function tch:cable/cabine/charger_modele
execute as @e[type=armor_stand,tag=TraceCable,distance=..0.5,tag=RetirerModele] run function tch:cable/cabine/charger_modele_air
# On incrémente le nombre de ticks attendus
scoreboard players add @s remainingTicks 1

# Si on doit se déplacer maintenant, on le fait
# Vitesse fonctionne à l'envers: c'est le nombre de ticks à attendre avant de se déplacer de 1 bloc
execute if score @s[tag=!Interrompu] remainingTicks >= @s vitesse if entity @p[distance=..120] run function tch:cable/cabine/mouvement
# Si on est interrompus, on vérifie à la place au bout d'un délai de 10s si on peut redémarrer
execute if score @s[tag=Interrompu] remainingTicks matches 100.. run function tch:cable/cabine/test_redemarrage


# Si on est à plus de 120 blocs d'un joueur, on se détruit
execute unless entity @p[distance=..119] run function tch:cable/cabine/detruire
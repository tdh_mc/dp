# On récupère les cabines alentour selon notre direction de déplacement
execute if score @s deltaX matches 1.. if score @s deltaZ matches 1.. positioned ~ ~-20 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=40,dy=40,dz=40] add AutreCabineCable
execute if score @s deltaX matches 1.. if score @s deltaZ matches 0 positioned ~ ~-20 ~-20 run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=40,dy=40,dz=40] add AutreCabineCable
execute if score @s deltaX matches 1.. if score @s deltaZ matches ..-1 positioned ~ ~-20 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=40,dy=40,dz=-40] add AutreCabineCable
execute if score @s deltaX matches 0 if score @s deltaZ matches 1.. positioned ~-20 ~-20 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=40,dy=40,dz=40] add AutreCabineCable
execute if score @s deltaX matches 0 if score @s deltaZ matches ..-1 positioned ~-20 ~-20 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=40,dy=40,dz=-40] add AutreCabineCable
execute if score @s deltaX matches ..-1 if score @s deltaZ matches 1.. positioned ~ ~-20 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=-40,dy=40,dz=40] add AutreCabineCable
execute if score @s deltaX matches ..-1 if score @s deltaZ matches 0 positioned ~ ~-20 ~-20 run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=-40,dy=40,dz=40] add AutreCabineCable
execute if score @s deltaX matches ..-1 if score @s deltaZ matches ..-1 positioned ~ ~-20 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=-40,dy=40,dz=-40] add AutreCabineCable
tag @s remove AutreCabineCable

# On vérifie si ces cabines sont de la même ligne que nous; sinon, on supprime leur tag
scoreboard players operation #cabineCable idLine = @s idLine
execute as @e[type=armor_stand,tag=AutreCabineCable] unless score @s idLine = #cabineCable idLine run tag @s remove AutreCabineCable
scoreboard players reset #cabineCable idLine

# Si on a trouvé une cabine de notre ligne, on se met en arrêt d'urgence
execute if entity @e[type=armor_stand,tag=AutreCabineCable] run function tch:cable/cabine/arret_urgence

# On supprime les tags temporaires des cabines
tag @e[type=armor_stand,tag=AutreCabineCable,distance=..200] remove AutreCabineCable
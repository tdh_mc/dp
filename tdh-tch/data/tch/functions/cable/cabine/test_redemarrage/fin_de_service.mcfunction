# On récupère notre PC
function tch:tag/pc

# Si notre PC n'a plus le tag FinDeService, on retire le nôtre également
execute unless entity @e[type=item_frame,tag=ourPC,tag=FinDeService] run tag @s remove FinDeService

# On supprime le tag de notre PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
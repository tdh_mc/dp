# On récupère notre PC
function tch:tag/pc

# Si notre PC n'a pas un statut d'incident interrompant le trafic, on retire notre tag
execute unless entity @e[type=item_frame,tag=ourPC,scores={status=1..3}] run tag @s remove Incident

# On supprime le tag de notre PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
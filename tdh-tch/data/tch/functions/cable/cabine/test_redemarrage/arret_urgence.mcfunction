# On récupère les cabines alentour selon notre direction de déplacement
execute if score @s deltaX matches 1.. if score @s deltaZ matches 1.. positioned ~ ~-30 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=60,dy=60,dz=60] add AutreCabineCable
execute if score @s deltaX matches 1.. if score @s deltaZ matches 0 positioned ~ ~-30 ~-30 run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=60,dy=60,dz=60] add AutreCabineCable
execute if score @s deltaX matches 1.. if score @s deltaZ matches ..-1 positioned ~ ~-30 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=60,dy=60,dz=-60] add AutreCabineCable
execute if score @s deltaX matches 0 if score @s deltaZ matches 1.. positioned ~-30 ~-30 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=60,dy=60,dz=60] add AutreCabineCable
execute if score @s deltaX matches 0 if score @s deltaZ matches ..-1 positioned ~-30 ~-30 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=60,dy=60,dz=-60] add AutreCabineCable
execute if score @s deltaX matches ..-1 if score @s deltaZ matches 1.. positioned ~ ~-30 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=-60,dy=60,dz=60] add AutreCabineCable
execute if score @s deltaX matches ..-1 if score @s deltaZ matches 0 positioned ~ ~-30 ~-30 run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=-60,dy=60,dz=60] add AutreCabineCable
execute if score @s deltaX matches ..-1 if score @s deltaZ matches ..-1 positioned ~ ~-30 ~ run tag @e[distance=1..,type=armor_stand,tag=CabineCable,dx=-60,dy=60,dz=-60] add AutreCabineCable
tag @s remove AutreCabineCable

# On vérifie si ces cabines sont de la même ligne que nous; sinon, on supprime leur tag
scoreboard players operation #cabineCable idLine = @s idLine
execute as @e[type=armor_stand,tag=AutreCabineCable] unless score @s idLine = #cabineCable idLine run tag @s remove AutreCabineCable
scoreboard players reset #cabineCable idLine

# Si on n'a pas trouvé de cabine de notre ligne, on se retire le tag
execute unless entity @e[type=armor_stand,tag=AutreCabineCable] run tag @s remove ArretUrgence

# On supprime les tags temporaires des cabines
tag @e[type=armor_stand,tag=AutreCabineCable,distance=..200] remove AutreCabineCable
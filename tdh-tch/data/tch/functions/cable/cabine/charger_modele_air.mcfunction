# On exécute cette fonction en tant qu'une armor stand "TraceCable"/"NouveauModele" pour vérifier quel modèle on doit charger

# On charge l'air du nouveau modèle à l'emplacement temporaire selon celui qu'on souhaite
execute as @s[tag=NordO] at @e[type=item_frame,tag=ModeleCabineCable,tag=NordO] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=NordF2] at @e[type=item_frame,tag=ModeleCabineCable,tag=NordF2] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=NordF1] at @e[type=item_frame,tag=ModeleCabineCable,tag=NordF1] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=NordF] at @e[type=item_frame,tag=ModeleCabineCable,tag=NordF] run function tch:cable/cabine/charger_modele/enregistrer_air

execute as @s[tag=SudO] at @e[type=item_frame,tag=ModeleCabineCable,tag=SudO] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=SudF2] at @e[type=item_frame,tag=ModeleCabineCable,tag=SudF2] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=SudF1] at @e[type=item_frame,tag=ModeleCabineCable,tag=SudF1] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=SudF] at @e[type=item_frame,tag=ModeleCabineCable,tag=SudF] run function tch:cable/cabine/charger_modele/enregistrer_air

execute as @s[tag=EstO] at @e[type=item_frame,tag=ModeleCabineCable,tag=EstO] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=EstF2] at @e[type=item_frame,tag=ModeleCabineCable,tag=EstF2] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=EstF1] at @e[type=item_frame,tag=ModeleCabineCable,tag=EstF1] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=EstF] at @e[type=item_frame,tag=ModeleCabineCable,tag=EstF] run function tch:cable/cabine/charger_modele/enregistrer_air

execute as @s[tag=OuestO] at @e[type=item_frame,tag=ModeleCabineCable,tag=OuestO] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=OuestF2] at @e[type=item_frame,tag=ModeleCabineCable,tag=OuestF2] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=OuestF1] at @e[type=item_frame,tag=ModeleCabineCable,tag=OuestF1] run function tch:cable/cabine/charger_modele/enregistrer_air
execute as @s[tag=OuestF] at @e[type=item_frame,tag=ModeleCabineCable,tag=OuestF] run function tch:cable/cabine/charger_modele/enregistrer_air

# On copie ensuite le nouveau modèle à notre position
function tch:cable/cabine/charger_modele/recuperer_air
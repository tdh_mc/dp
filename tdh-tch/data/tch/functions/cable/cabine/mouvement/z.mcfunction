# On exécute cette fonction en tant qu'une cabine de câble à sa position, si on doit se déplacer sur l'axe des Z

# On se déplace selon la valeur du score
execute if score @s deltaZ matches ..-2 run function tch:cable/cabine/mouvement/z/-2
execute if score @s deltaZ matches -1 run function tch:cable/cabine/mouvement/z/-1
execute if score @s deltaZ matches 1 run function tch:cable/cabine/mouvement/z/1
execute if score @s deltaZ matches 2.. run function tch:cable/cabine/mouvement/z/2
# On exécute cette fonction en tant qu'une cabine de câble à sa position, si on doit se déplacer sur l'axe des X et des Y

# On se déplace selon la valeur du score
execute if score @s deltaX matches ..-2 run function tch:cable/cabine/mouvement/xy/-2
execute if score @s deltaX matches -1 run function tch:cable/cabine/mouvement/xy/-1
execute if score @s deltaX matches 1 run function tch:cable/cabine/mouvement/xy/1
execute if score @s deltaX matches 2.. run function tch:cable/cabine/mouvement/xy/2
# On exécute cette fonction en tant qu'une cabine de câble à sa position, si on doit se déplacer sur les 3 axes à la fois

# On se déplace selon la valeur du score
execute if score @s deltaX matches ..-2 run function tch:cable/cabine/mouvement/xyz/-2
execute if score @s deltaX matches -1 run function tch:cable/cabine/mouvement/xyz/-1
execute if score @s deltaX matches 1 run function tch:cable/cabine/mouvement/xyz/1
execute if score @s deltaX matches 2.. run function tch:cable/cabine/mouvement/xyz/2
# On a les bonnes déviations par rapport à notre propre position dans deltaX/Z,
# et notre propre position dans posX/Z
# On calcule la position absolue, puis on l'applique directement
tag @e[tag=DansLaCabine,distance=0] add RotationNow
execute as @e[tag=RotationNow,distance=0] store result entity @s Pos[0] double 0.01 run scoreboard players operation #cableTemp deltaX += #cableTemp posX
execute as @e[tag=RotationNow,distance=..15] store result entity @s Pos[2] double 0.01 run scoreboard players operation #cableTemp deltaZ += #cableTemp posZ

# On supprime les tags temporaires
tag @e[tag=RotationNow] remove RotationNow
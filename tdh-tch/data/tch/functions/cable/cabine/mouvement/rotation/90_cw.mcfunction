# On applique en premier lieu la rotation appropriée
execute as @e[type=!player,tag=DansLaCabine,distance=0] at @s run tp @s ~ ~ ~ ~90 ~
execute at @p[tag=DansLaCabine,distance=0] run tp @p[tag=DansLaCabine,distance=0] ~ ~ ~ ~90 ~

# On enregistre dans des variables temporaires le décalage entre notre position (= celle de la cabine)
# et celle de l'entité que l'on souhaite tourner en conséquence (à la position de laquelle on exécute la fonction)
execute store result score #cableTemp deltaX run data get entity @e[tag=DansLaCabine,distance=0,limit=1] Pos[0] 100
execute store result score #cableTemp posX run data get entity @s Pos[0] 100
scoreboard players operation #cableTemp deltaX -= #cableTemp posX

execute store result score #cableTemp deltaZ run data get entity @e[tag=DansLaCabine,distance=0,limit=1] Pos[2] 100
execute store result score #cableTemp posZ run data get entity @s Pos[2] 100
scoreboard players operation #cableTemp deltaZ -= #cableTemp posZ

# On applique la transformation horaire de 90°
# Nord (Z-) devient Est (X+)
# Est (X+) devient Sud (Z+)
# Sud (Z+) devient Ouest (X-)
# Ouest (X-) devient Nord (Z-)
# => donc X = -Z et Z = X

scoreboard players operation #cableTemp deltaX >< #cableTemp deltaZ
scoreboard players set #cableTemp temp -1
scoreboard players operation #cableTemp deltaX *= #cableTemp temp

# On a les bons "nouveaux" deltaX et deltaZ
# On va désormais soit :
# - déplacer directement l'entité si ce n'est pas un joueur
execute if entity @p[tag=DansLaCabine,distance=0] run scoreboard players set #cableTemp temp 1
execute unless score #cableTemp temp matches 1 run function tch:cable/cabine/mouvement/rotation/appliquer_entite
# - invoquer un item, puis y tp le joueur si c'en est un
execute if score #cableTemp temp matches 1 run function tch:cable/cabine/mouvement/rotation/appliquer_joueur


# On nettoie ensuite les variables temporaires
scoreboard players reset #cableTemp deltaX
scoreboard players reset #cableTemp deltaZ
scoreboard players reset #cableTemp posX
scoreboard players reset #cableTemp posZ
scoreboard players reset #cableTemp temp
# On a les bonnes déviations par rapport à notre propre position dans deltaX/Z,
# et notre propre position dans posX/Z
# On calcule la position absolue, puis on l'applique à un objet servant de placeholder pour téléporter le joueur
tag @a[tag=DansLaCabine,distance=0] add RotationNow
summon item ~ ~ ~ {Tags:["TempCableRotationItem"],Age:5999s,PickupDelay:32767s,Item:{Count:1b,id:"minecraft:polished_blackstone_button"}}
execute as @e[type=item,tag=TempCableRotationItem,sort=nearest,limit=1] store result entity @s Pos[0] double 0.01 run scoreboard players operation #cableTemp deltaX += #cableTemp posX
execute as @e[type=item,tag=TempCableRotationItem,sort=nearest,limit=1] store result entity @s Pos[2] double 0.01 run scoreboard players operation #cableTemp deltaZ += #cableTemp posZ

# On téléporte ensuite les joueurs
execute at @e[type=item,tag=TempCableRotationItem,sort=nearest,limit=1] rotated as @p[tag=RotationNow] run tp @a[tag=RotationNow] ~ ~ ~ ~ ~

# On détruit ensuite l'item temporaire
kill @e[type=item,tag=TempCableRotationItem]

# On supprime les tags temporaires
tag @a[tag=RotationNow] remove RotationNow
# On exécute cette fonction en tant qu'une cabine de câble,
# à la position d'un TraceCable Station, pour prendre en compte le fait qu'on vient d'arriver à une station

tag @s add EnStation
tag @s remove Terminus
tag @s remove Correspondance
tag @s remove PreStation
tag @s remove PreStationDone
tag @s remove ArriveeStation
tag @s remove ArriveeStationDone

# On enregistre qu'on est arrivés dans l'ex-prochaine station
scoreboard players operation @s idStation = @s prochaineStation

# Si on est en fin de service, on se donne le tag Interrompu pour ne pas redémarrer
tag @s[tag=FinDeService] add Interrompu

# Si notre ligne subit un incident interrompant le trafic, on se met en attente
function tch:tag/pc
execute if score @e[type=item_frame,tag=ourPC,limit=1] status matches 1..3 run function tch:cable/cabine/arret_incident
tag @e[type=item_frame,tag=ourPC] remove ourPC
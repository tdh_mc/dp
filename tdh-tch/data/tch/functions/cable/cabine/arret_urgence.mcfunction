# On interrompt notre mouvement
tag @s add Interrompu
tag @s add ArretUrgence
# On joue le bruitage d'arrêt
function tch:sound/cable/arrivee
# On réinitialise le SISVE
scoreboard players set @s displayTicks 0
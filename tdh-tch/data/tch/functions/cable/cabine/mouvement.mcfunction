# On exécute cette fonction en tant qu'une cabine de câble à sa position, si on doit se déplacer maintenant

# On tag les entités se trouvant dans la cabine
execute positioned ~-2.5 ~-11 ~-2.5 run tag @e[type=!player,type=!armor_stand,type=!item_frame,dx=5.0,dy=12,dz=5.0] add DansLaCabine
execute positioned ~-2.5 ~-11 ~-2.5 run tag @a[gamemode=!spectator,dx=5.0,dy=12,dz=5.0] add DansLaCabine
execute at @e[tag=DansLaCabine] unless block ~ ~ ~ #tch:cable unless block ~ ~-1 ~ #tch:cable run tag @e[tag=DansLaCabine,distance=..0] remove DansLaCabine

# S'il y a une cabine de même ligne trop proche de nous, on vérifie s'il faut faire un arrêt d'urgence
execute at @e[type=armor_stand,tag=CabineCable,distance=1..20] if score @e[type=armor_stand,tag=CabineCable,distance=0,limit=1] idLine = @s idLine at @s run function tch:cable/cabine/test_arret_urgence

# S'il y a une rotation à effectuer, on l'exécute
execute as @s[tag=RotationCW] at @e[tag=DansLaCabine,distance=..25] run function tch:cable/cabine/mouvement/rotation/90_cw
execute as @s[tag=RotationCCW] at @e[tag=DansLaCabine,distance=..25] run function tch:cable/cabine/mouvement/rotation/90_ccw

# On se déplace en direction de notre destination
# On a des sous fonctions différentes selon le/les axes de déplacement
execute at @s if score @s deltaY matches 0 if score @s deltaZ matches 0 run function tch:cable/cabine/mouvement/x
execute at @s if score @s deltaX matches 0 if score @s deltaZ matches 0 run function tch:cable/cabine/mouvement/y
execute at @s if score @s deltaX matches 0 if score @s deltaY matches 0 run function tch:cable/cabine/mouvement/z
execute at @s if score @s deltaX matches 0 run function tch:cable/cabine/mouvement/yz
execute at @s if score @s deltaY matches 0 run function tch:cable/cabine/mouvement/xz
execute at @s if score @s deltaZ matches 0 run function tch:cable/cabine/mouvement/xy
execute at @s unless score @s deltaX matches 0 unless score @s deltaY matches 0 unless score @s deltaZ matches 0 run function tch:cable/cabine/mouvement/xyz

# On supprime les tags temporaires des entités devant être déplacées
tag @e[tag=DansLaCabine] remove DansLaCabine

# On réinitialise le temps d'attente avant déplacement
scoreboard players set @s remainingTicks 0

# On diffuse le son du moteur
execute at @s run function tch:sound/cable/speed_auto

# Si on va à moins de 1 itération par seconde, on remonte la vitesse
execute if score @s vitesse matches 16.. run scoreboard players set @s vitesse 15
# Si on n'est pas à notre vitesse max, on augmente la vitesse pour la prochaine itération
execute if score @s vitesseMax < @s vitesse run scoreboard players remove @s vitesse 1
# Si on a dépassé notre vitesse max, on diminue la vitesse
execute if score @s vitesseMax > @s vitesse run scoreboard players add @s vitesse 1


# On supprime nos propres tags temporaires, qu'on s'apprête ou non à prendre en compte une étape
# On ne reste en station que pendant 1 tick (celui où on est à l'arrêt)
tag @s remove EnStation
# On ne veut jouer le son d'arrivée et prendre en compte l'arrivée en station qu'une fois même si l'armor stand ArriveeStation est plusieurs blocs avant la station
tag @s[tag=ArriveeStation] add ArriveeStationDone
# On n'effectue la rotation qu'une seule fois
tag @s remove RotationCW
tag @s remove RotationCCW

# On prend en compte la prochaine étape si elle se trouve sur le même bloc que nous
execute at @s if entity @e[type=armor_stand,tag=TraceCable,distance=..0.5] run function tch:cable/cabine/prochain_noeud
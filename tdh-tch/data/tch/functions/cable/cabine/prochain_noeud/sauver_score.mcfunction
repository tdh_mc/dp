# On enregistre dans les scores du câble les variables nécessaires
execute if score @s deltaX matches -999.. run scoreboard players operation #cable deltaX = @s deltaX
execute if score @s deltaY matches -999.. run scoreboard players operation #cable deltaY = @s deltaY
execute if score @s deltaZ matches -999.. run scoreboard players operation #cable deltaZ = @s deltaZ
execute if score @s vitesse matches 0.. run scoreboard players operation #cable vitesse = @s vitesse
execute if score @s vitesseMax matches 0.. run scoreboard players operation #cable vitesseMax = @s vitesseMax
execute if score @s idLine matches 0.. run scoreboard players operation #cable idLine = @s idLine
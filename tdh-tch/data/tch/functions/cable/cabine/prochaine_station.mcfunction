# On exécute cette fonction en tant qu'une cabine de câble,
# à la position d'un TraceCable ProchaineStation, pour prendre en compte l'ID de la prochaine station

# Si on ne connaît pas l'ID de station, on essaie de le trouver à partir du nom
execute as @e[type=armor_stand,distance=..0.5,tag=TraceCable,tag=ProchaineStation,limit=1] unless score @s idStation matches 0.. run function tch:station/get_id

# On enregistre l'ID de station
scoreboard players operation @s idStation = @e[type=armor_stand,distance=..0.5,tag=TraceCable,tag=ProchaineStation,limit=1] idStation
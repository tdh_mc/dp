# On exécute cette fonction en tant qu'une CabineCable à sa position, lorsqu'on est arrivés au terminus
# On inverse l'ID de ligne
scoreboard players set @s[scores={idLine=4011}] idLine 44012
scoreboard players set @s[scores={idLine=4012}] idLine 4011
scoreboard players set @s[scores={idLine=44012}] idLine 4012

scoreboard players set @s[scores={idLine=4021}] idLine 44022
scoreboard players set @s[scores={idLine=4022}] idLine 4021
scoreboard players set @s[scores={idLine=44022}] idLine 4022

scoreboard players set @s[scores={idLine=4031}] idLine 44032
scoreboard players set @s[scores={idLine=4032}] idLine 4031
scoreboard players set @s[scores={idLine=44032}] idLine 4032

scoreboard players set @s[scores={idLine=4041}] idLine 44042
scoreboard players set @s[scores={idLine=4042}] idLine 4041
scoreboard players set @s[scores={idLine=44042}] idLine 4042

scoreboard players set @s[scores={idLine=4051}] idLine 44052
scoreboard players set @s[scores={idLine=4052}] idLine 4051
scoreboard players set @s[scores={idLine=44052}] idLine 4052
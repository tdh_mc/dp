# Si on a une heure de spawn prévue, on vérifie si c'est l'heure
execute as @e[type=armor_stand,tag=SpawnCable,scores={spawnAt=0..}] at @s run function tch:cable/spawner/spawn/test_horaire

# Si on a pas d'heure de spawn prévue, on génère un nouvel horaire
execute as @e[type=armor_stand,tag=SpawnCable] unless score @s spawnAt matches 0.. at @s run function tch:ligne/horaire/gen/test
## TEST DE SPAWN PARTIE 1 :
## On vérifie si l'heure a été dépassée.

# Si c'est le cas on exécute la fonction test_ligne qui s'occupe de la suite des tests
execute if score @s spawnAt <= #temps currentTdhTick if score @s idJour = #temps idJour run function tch:cable/spawner/spawn/test_ligne
execute if score @s idJour < #temps idJour run function tch:cable/spawner/spawn/test_ligne

# Dans tous les cas, on met à jour le temps d'attente sur les PID
function tch:station/pid/update
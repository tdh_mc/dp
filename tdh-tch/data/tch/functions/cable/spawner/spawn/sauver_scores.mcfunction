# ID de ligne et de station
scoreboard players operation @s idLine = @e[type=armor_stand,tag=SpawnCable,distance=..0.5,limit=1] idLine
scoreboard players operation @s prochaineStation = @e[type=armor_stand,tag=SpawnCable,distance=..0.5,limit=1] idStation


# On tag notre PC et PC de terminus pour récupérer les bonnes infos
function tch:tag/pc
function tch:tag/terminus_pc

# On enregistre notre nom à partir de notre ID de ligne
execute at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:cable/spawner/spawn/nom_cabine

# On enregistre les données de déplacement depuis la fonction idoine
function tch:cable/cabine/prochain_noeud

# On se donne d'office la vitesse maximale
scoreboard players operation @s vitesse = @s vitesseMax

# On retire notre tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
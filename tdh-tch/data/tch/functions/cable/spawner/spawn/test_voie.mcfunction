## TEST DE SPAWN PARTIE 3 :
## On vérifie si notre voie est libre et notre station chargée.

# Si notre station est chargée et qu'il y a un joueur à moins de 100 blocs, on peut spawner
execute as @e[type=armor_stand,tag=TraceCable,tag=Station,distance=..100] unless score @s idStation matches 0.. run function tch:station/get_id
execute at @e[type=armor_stand,tag=TraceCable,tag=Station,distance=..100] if entity @p[distance=..100] if score @s idStation = @e[type=armor_stand,tag=TraceCable,tag=Station,distance=0,limit=1] idStation unless entity @e[type=armor_stand,tag=CabineCable,distance=..50] run tag @s add SpawnPossible

# S'il n'y a pas de joueur à 100 blocs à la ronde, on se retire le tag
execute unless entity @p[distance=..99] run tag @s remove SpawnPossible

# S'il y a une cabine de la même ligne à 70 blocs à la ronde, on se retire également le tag
execute at @e[type=armor_stand,tag=CabineCable,distance=..70] if score @s idLine = @e[type=armor_stand,tag=CabineCable,sort=nearest,limit=1] idLine run tag @s remove SpawnPossible

# Si on n'a pas le droit de spawner, on reset notre horaire de spawn (pour en générer un nouveau)
execute as @s[tag=!SpawnPossible] run scoreboard players reset @s spawnAt
# Si on peut spawner, on le fait
execute as @s[tag=SpawnPossible] run function tch:cable/spawner/spawn/spawn
tag @s remove SpawnPossible
# On fait apparaître une cabine correspondant aux tags du spawner
function tch:cable/cabine/charger_modele
# On invoque également la cabine
summon armor_stand ~ ~ ~ {Tags:["CabineCable","AllowSignals"],Invulnerable:1b,NoGravity:1b,CustomName:'"Cabine du câble"'}

# On se donne les bons tags et scores
execute as @e[type=armor_stand,tag=CabineCable,distance=..0,limit=1] run function tch:cable/spawner/spawn/sauver_scores

# On joue le message de PID
function tch:station/annonce/pid/gen_id

# Todo :
# Réserver la voie
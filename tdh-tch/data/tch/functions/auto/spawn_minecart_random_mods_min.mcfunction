
# Ensuite on ajoute une durée d'attente aléatoire dépendant de l'heure
# 21h30 - 6h00 [pas de service — prochain train à 6h]
execute unless score #temps currentTdhTick matches 12000..42999 run scoreboard players set @s spawnAt 12000
execute unless score #temps currentTdhTick matches 12000..42999 run scoreboard players set @s spawnMod 1200

# 6h00 - 7h30 [heure creuse matin]
execute if score #temps currentTdhTick matches 12000..14999 run scoreboard players add @s spawnAt 1200
execute if score #temps currentTdhTick matches 12000..14999 run scoreboard players set @s spawnMod 800

# 7h30 - 9h30 [heure de pointe matin]
execute if score #temps currentTdhTick matches 15000..18999 run scoreboard players add @s spawnAt 750
execute if score #temps currentTdhTick matches 15000..18999 run scoreboard players set @s spawnMod 250

# 9h30 - 12h00 [heure creuse fin matin]
execute if score #temps currentTdhTick matches 19000..23999 run scoreboard players add @s spawnAt 1000
execute if score #temps currentTdhTick matches 19000..23999 run scoreboard players set @s spawnMod 1000

# 12h00 - 18h00 [heure creuse après-midi]
execute if score #temps currentTdhTick matches 24000..35999 run scoreboard players add @s spawnAt 1200
execute if score #temps currentTdhTick matches 24000..35999 run scoreboard players set @s spawnMod 800

# 18h00 - 20h30 [heure de pointe soir]
execute if score #temps currentTdhTick matches 36000..40999 run scoreboard players add @s spawnAt 750
execute if score #temps currentTdhTick matches 36000..40999 run scoreboard players set @s spawnMod 350

# 20h30 - 21h30 [heure creuse soirée]
execute if score #temps currentTdhTick matches 41000..42999 run scoreboard players add @s spawnAt 1000
execute if score #temps currentTdhTick matches 41000..42999 run scoreboard players set @s spawnMod 1000
#execute if score @s temp matches ..0 if score @s dateJour = #temps dateJour run say On lance un train, car le tick a été dépassé.
#execute if score @s dateJour < #temps dateJour run say On lance un train, car on est le jour suivant.
#execute if score @s dateMois < #temps dateMois run say On lance un train, car on est le mois suivant.
#execute if score @s dateAnnee < #temps dateAnnee run say On lance un train, car on est l'année suivante.

# On fait jouer le message de PID
function tch:auto/annonce/force_sound_pid

# On invoque le métro
summon minecart ~ ~-1 ~ {Invulnerable:1b,Tags:["Metro","EnStation"],CustomDisplayTile:1b,DisplayState:{Name:"minecraft:quartz_stairs"},DisplayOffset:-3}

# On enregistre l'ID de ligne, l'ID de station
scoreboard players operation @e[tag=Metro,sort=nearest,limit=1] idStation = @e[tag=NomStation,sort=nearest,limit=1] idStation
scoreboard players operation @e[tag=Metro,sort=nearest,limit=1] idLine = @s idLine
execute as @e[tag=Metro,sort=nearest,limit=1] run function tch:auto/title/save_line_id

# On réinitialise l'heure de spawn du prochain métro, afin de pouvoir en générer une autre
scoreboard players reset @s spawnAt
tag @s remove TrainRetarde

# On réserve la voie
scoreboard players set @e[tag=spawnNowAS,limit=1] VoieReservee 1

# On envoie un message sur le quai
execute at @e[tag=spawnNowAS,limit=1] positioned ~-19.5 ~-1.5 ~-19.5 run tellraw @a[dx=39,dy=9,dz=39,tag=!SpeakingToTCHAgent] [{"text":"","color":"gray"},{"selector":"@e[dx=39,dy=9,dz=39,tag=NumLigne,sort=nearest,limit=1]"},{"text":" → "},{"selector":"@s"},{"text":" : un train est en approche."}]
execute if score @s launchDirection matches 1 run data modify entity @s Motion set value [0d,0d,-2d]
execute if score @s launchDirection matches 2 run data modify entity @s Motion set value [2d,0d,0d]
execute if score @s launchDirection matches 3 run data modify entity @s Motion set value [0d,0d,2d]
execute if score @s launchDirection matches 4 run data modify entity @s Motion set value [-2d,0d,0d]

execute if score @s launchDirection matches 12 run data modify entity @s Motion set value [2d,0d,-2d]
execute if score @s launchDirection matches 23 run data modify entity @s Motion set value [2d,0d,2d]
execute if score @s launchDirection matches 34 run data modify entity @s Motion set value [-2d,0d,2d]
execute if score @s launchDirection matches 41 run data modify entity @s Motion set value [-2d,0d,-2d]

execute at @e[tag=Direction,distance=..200] if score @s idLine = @e[tag=Direction,sort=nearest,limit=1] idLine run function tch:auto/reserver_voie_success

scoreboard players reset @s launchDirection
scoreboard players reset @s remainingTicks
scoreboard players reset @s incidentEnCours
scoreboard players reset @s ArretUrgence
# On replace les portes de quai
# TODO : remplacer cette version par une version avec ^ ^ ^
execute unless entity @e[distance=..100,tag=StationInversee] if score @s launchDirection matches 1 run fill ~2 ~1 ~-1 ~2 ~2 ~1 light_gray_stained_glass_pane keep
execute unless entity @e[distance=..100,tag=StationInversee] if score @s launchDirection matches 2 run fill ~-1 ~1 ~2 ~1 ~2 ~2 light_gray_stained_glass_pane keep
execute unless entity @e[distance=..100,tag=StationInversee] if score @s launchDirection matches 3 run fill ~-2 ~1 ~-1 ~-2 ~2 ~1 light_gray_stained_glass_pane keep
execute unless entity @e[distance=..100,tag=StationInversee] if score @s launchDirection matches 4 run fill ~-1 ~1 ~-2 ~1 ~2 ~-2 light_gray_stained_glass_pane keep

execute if entity @e[distance=..100,tag=StationInversee] if score @s launchDirection matches 1 run fill ~-2 ~1 ~-1 ~-2 ~2 ~1 light_gray_stained_glass_pane keep
execute if entity @e[distance=..100,tag=StationInversee] if score @s launchDirection matches 2 run fill ~-1 ~1 ~-2 ~1 ~2 ~-2 light_gray_stained_glass_pane keep
execute if entity @e[distance=..100,tag=StationInversee] if score @s launchDirection matches 3 run fill ~2 ~1 ~-1 ~2 ~2 ~1 light_gray_stained_glass_pane keep
execute if entity @e[distance=..100,tag=StationInversee] if score @s launchDirection matches 4 run fill ~-1 ~1 ~2 ~1 ~2 ~2 light_gray_stained_glass_pane keep

# On joue le son de fermeture des portes de quai
execute positioned ~ ~3 ~ run function tch:sound/station_doors_close

# On lance le métro dans la direction correspondante
execute if score @s launchDirection matches 1 run data modify entity @s Motion set value [0d,0d,-2d]
execute if score @s launchDirection matches 2 run data modify entity @s Motion set value [2d,0d,0d]
execute if score @s launchDirection matches 3 run data modify entity @s Motion set value [0d,0d,2d]
execute if score @s launchDirection matches 4 run data modify entity @s Motion set value [-2d,0d,0d]
execute if score @s launchDirection matches 12 run data modify entity @s Motion set value [2d,0d,-2d]
execute if score @s launchDirection matches 23 run data modify entity @s Motion set value [2d,0d,2d]
execute if score @s launchDirection matches 34 run data modify entity @s Motion set value [-2d,0d,2d]
execute if score @s launchDirection matches 41 run data modify entity @s Motion set value [-2d,0d,-2d]

# On déréserve la voie que l'on occupait
execute at @e[tag=Direction,distance=..50] if score @e[sort=nearest,limit=1] idLine = @s idLine run scoreboard players set @e[sort=nearest,limit=1] VoieReservee 0

# On récupère l'ID de la prochaine station
execute as @e[tag=ProchaineStation,sort=nearest,limit=1] unless score @s idStation matches 1.. run function tch:auto/get_id_station
scoreboard players operation @s idStation = @e[tag=ProchaineStation,sort=nearest,limit=1] idStation

# On réinitialise tous nos scores relatifs à notre redémarrage
scoreboard players reset @s launchDirection
scoreboard players reset @s remainingTicks
scoreboard players reset @s incidentEnCours

# On met à jour le fait qu'on est sortis de la station
tag @s remove EnStation

# Si les chunks ne sont pas chargés 32 blocs (2 chunks) devant nous, on s'auto supprime pour éviter de se faire décharger
# Si les 2 conditions échouent (le bloc n'est pas de l'air, ni autre chose que de l'air), ça signifie que le chunk est déchargé
tag @s add ChunkCheck
execute if block ^ ^ ^32 air run tag @s remove ChunkCheck
execute unless block ^ ^ ^32 air run tag @s remove ChunkCheck
# Si le chunk est déchargé, on décharge aussi le minecart
execute at @s[tag=ChunkCheck] run function tch:auto/destroy_minecart_one
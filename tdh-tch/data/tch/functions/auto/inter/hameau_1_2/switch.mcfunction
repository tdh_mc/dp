# On exécute la fonction correspondante selon l'état actuel de l'aiguillage
execute as @e[tag=tchAiguillage,sort=nearest,limit=1] if score @s currentState matches 0 run function tch:auto/inter/hameau_1_2/on
execute as @e[tag=tchAiguillage,sort=nearest,limit=1] if score @s currentState matches 1.. run function tch:auto/inter/hameau_1_2/off

execute as @e[tag=tchAiguillage,sort=nearest,limit=1] if score @s currentState matches ..-1 run scoreboard players set @s currentState 0
# On émet un message d'avertissement
tellraw @a[distance=..10,tag=MetroWorker] [{"text":"[TCH] — L'aiguillage ","color":"gray"},{"selector":"@e[tag=tchAiguillage,sort=nearest,limit=1]"},{"text":" a été ","color":"gray"},{"text":"désactivé","color":"red"},{"text":".","color":"gray"}]

# On place tous les aiguillages correspondants en position On
# Dans cette position :
# - M1 : ÉvenisÉclesta — Dorlinor
# - M2 : Onyx — Grenat
setblock 112 30 165 rail[shape=north_west]
setblock 110 30 164 rail[shape=south_east]
setblock 91 30 104 rail[shape=south_east]
setblock 93 30 105 rail[shape=north_west]
setblock 97 30 174 rail[shape=south_west]
setblock 110 30 101 rail[shape=south_west]
setblock 110 30 97 rail[shape=south_west]
setblock 93 30 165 rail[shape=north_east]

# On active le levier pour éteindre la torche informative de l'état de l'aiguillage
setblock 96 40 138 lever[face=wall,facing=west,powered=true]
setblock 98 40 138 redstone_wall_torch[lit=true,facing=east]

# On modifie le score de l'aiguillage
scoreboard players set @s currentState -1
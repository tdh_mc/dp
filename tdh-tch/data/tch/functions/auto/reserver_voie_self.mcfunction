# Cette fonction doit être exécutée par le minecart, à la position de l'armor stand Direction correspondante, et tente de réserver le quai correspondant à cette direction

#execute if score @e[tag=Direction,sort=nearest,limit=1] VoieReservee matches 1 at @s run function tch:auto/pause_voie_occupee
#execute unless score @e[tag=Direction,sort=nearest,limit=1] VoieReservee matches 1 run function tch:auto/reserver_voie_self_success
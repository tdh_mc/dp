# On indique aux métros des lignes 1/6/8 que c'est la fin de service
tag @e[tag=Metro,scores={idLine=11..12}] add FinDeService
tag @e[tag=Metro,scores={idLine=61..62}] add FinDeService
tag @e[tag=Metro,scores={idLine=81..82}] add FinDeService

# On joue l'annonce de fin de service pour informer les joueurs
execute at @p[tag=tchInside] at @e[tag=NumLigne,sort=nearest,distance=..30,limit=1] if score @e[sort=nearest,limit=1] idLine matches 11..12 run function tch:sound/metro/fin_service1
execute at @p[tag=tchInside] at @e[tag=NumLigne,sort=nearest,distance=..30,limit=1] if score @e[sort=nearest,limit=1] idLine matches 61..62 run function tch:sound/metro/fin_service1
execute at @p[tag=tchInside] at @e[tag=NumLigne,sort=nearest,distance=..30,limit=1] if score @e[sort=nearest,limit=1] idLine matches 81..82 run function tch:sound/metro/fin_service1
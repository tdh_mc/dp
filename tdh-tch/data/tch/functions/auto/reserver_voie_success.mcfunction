execute at @s run say succès de la réservation (je contiens @p[distance=..2])

# On réserve la voie que l'on va utiliser
scoreboard players set @e[tag=Direction,sort=nearest,limit=1] VoieReservee 1
execute if score @s VoieReservee matches 1..2 at @e[tag=Direction,distance=..150] if score @e[tag=Direction,sort=nearest,limit=1] idLine = @s idLine run scoreboard players remove @e[tag=Direction,sort=nearest,limit=1] FileAttente 1
scoreboard players set @s VoieReservee 3
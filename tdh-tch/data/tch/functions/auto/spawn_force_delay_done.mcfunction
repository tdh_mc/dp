scoreboard players operation @s temp2 = @s spawnAt
scoreboard players operation @s temp4 = @s spawnAt
scoreboard players set @s temp3 1000
scoreboard players operation @s temp2 /= @s temp3
scoreboard players operation @s temp4 %= @s temp3
scoreboard players set @s temp3 17
scoreboard players operation @s temp4 /= @s temp3

scoreboard players operation @s temp5 = @s spawnAt
scoreboard players operation @s temp5 -= #temps currentTick
scoreboard players set @s temp3 20
scoreboard players operation @s temp5 /= @s temp3

execute at @e[type=armor_stand,sort=nearest,limit=1,tag=NumLigne] run tellraw @a[distance=..50] [{"text":"En raison de l'arrivée imprévue d'un train en gare, le prochain train est retardé et arrivera dans ","color":"gray"},{"score":{"name":"@s","objective":"temp5"},"color":"red"},{"text":" secondes (nouvelle heure d'arrivée : ","color":"gray"},{"score":{"name":"@s","objective":"temp2"},"color":"red"},{"text":"h","color":"red"},{"score":{"name":"@s","objective":"temp2"},"color":"red"},{"text":").","color":"gray"}]
execute if score @s spawnAt matches 24000.. run scoreboard players remove @s spawnAt 24000
execute if score @s spawnAt matches 24000.. run scoreboard players add @s dateJour 1
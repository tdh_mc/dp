execute as @e[type=minecart,distance=..5] run data modify entity @s Motion set value [0d,0d,0d]

execute if score @e[type=minecart,distance=..5,limit=1,sort=nearest] launchDirection matches 1 run setblock ~-2 ~3 ~ air
execute if score @e[type=minecart,distance=..5,limit=1,sort=nearest] launchDirection matches 2 run setblock ~ ~3 ~-2 air
execute if score @e[type=minecart,distance=..5,limit=1,sort=nearest] launchDirection matches 3 run setblock ~2 ~3 ~ air
execute if score @e[type=minecart,distance=..5,limit=1,sort=nearest] launchDirection matches 4 run setblock ~ ~3 ~2 air

execute positioned ~ ~3 ~ run function tch:sound/metro/station_doors_open
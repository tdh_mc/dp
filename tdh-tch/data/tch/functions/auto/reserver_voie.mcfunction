execute at @s run say Réserver voie (Je contiens @p[distance=..2])
# Cette fonction doit être exécutée par le minecart, à la position de l'armor stand Direction correspondante, et tente de réserver le quai correspondant à cette direction

execute if score @e[tag=Direction,sort=nearest,limit=1] VoieReservee matches 1 as @s[scores={VoieReservee=1}] run function tch:auto/reserver_voie_pause
execute if score @e[tag=Direction,sort=nearest,limit=1] VoieReservee matches 1 as @s[scores={VoieReservee=0}] run function tch:auto/reserver_voie_file_attente

#execute if score @e[tag=Direction,sort=nearest,limit=1] VoieReservee matches 1 at @s as @s[tag=ResaPrioritaire] run function tch:auto/pause_voie_occupee
#execute if score @e[tag=Direction,sort=nearest,limit=1] VoieReservee matches 1 at @s as @s[tag=!ResaPrioritaire] run tag @s add ResaPrioritaire

execute unless score @e[tag=Direction,sort=nearest,limit=1] VoieReservee matches 1 run function tch:auto/reserver_voie_success
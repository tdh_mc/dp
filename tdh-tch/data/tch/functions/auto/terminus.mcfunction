# On affiche un message de terminus
execute as @e[tag=Metro,sort=nearest,limit=1] at @s if entity @e[tag=Terminus,distance=..20] run tellraw @p[distance=..1] [{"text":"Vous êtes arrivé à la station ","color":"gray"},{"selector":"@e[sort=nearest,tag=NomStation,limit=1]","color":"aqua","bold":"true"},{"text":", terminus du ","color":"gray"},{"selector":"@e[tag=NumLigne,limit=1,sort=nearest]"},{"text":".","color":"gray"},{"text":"\nTous les voyageurs sont invités à descendre","color":"red"},{"text":". Merci !","color":"gray"}]

# On joue un son de fin de service si on s'est fait virer pour cause de fin de service
execute as @e[tag=Metro,sort=nearest,limit=1] as @s[tag=FinDeService] at @e[tag=Direction,sort=nearest,limit=1] run function tch:sound/metro/fin_service_1

# On téléporte le joueur
execute as @e[tag=Metro,sort=nearest,limit=1] at @s facing entity @e[tag=NumLigne,sort=nearest,limit=1] eyes run tp @a[distance=..1] ^3.5 ^0.5 ^-3.5 ~-120 ~

# On déréserve la voie
execute as @e[tag=Metro,sort=nearest,limit=1] at @s at @e[tag=Direction,distance=..50] if score @e[sort=nearest,limit=1] idLine = @s idLine run scoreboard players set @e[sort=nearest,limit=1] VoieReservee 0

#DEBUG
execute at @s unless entity @e[type=armor_stand,distance=..75,tag=NomStation] run tellraw @p [{"text":"TCH_DEBUG ","color":"red"},{"text":"— Vous ne devriez pas voir ce message, mais puisque c'est le cas, informez l'agent TCH le plus proche du problème suivant :","color":"gray"},{"text":" Pas de NomStation enregistré","color":"dark_red"},{"text":".","color":"gray"}]
#DEBUG
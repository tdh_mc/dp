execute at @e[tag=Metro,tag=ResaPrioritaire,distance=..200] if score @e[tag=Metro,sort=nearest,limit=1] idLine = @s idLine run tag @e[tag=ResaPrioritaire,sort=nearest,limit=1] add isPrio
execute if entity @e[tag=isPrio] run tag @s add isThrottled

#execute as @s[tag=isThrottled] at @e[tag=Direction,distance=..200] if score @e[tag=Direction,sort=nearest,limit=1] idLine = @s idLine as @e[tag=isPrio,sort=nearest,limit=1] run function tch:auto/reserver_voie
execute as @s[tag=isThrottled] run function tch:auto/spawn_minecart_tick_delay
execute as @s[tag=!isThrottled] at @s run function tch:auto/spawn_minecart_tick

tag @s[tag=isThrottled] remove isThrottled
tag @e[tag=isPrio] remove isPrio
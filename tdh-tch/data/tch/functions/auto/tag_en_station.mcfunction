tag @s add EnStation
tag @s remove PreStation
tag @s remove PreStationDone
tag @s remove Corresp
tag @s remove ResaPrioritaire
tag @s remove DescenteGauche
tag @s remove Terminus

scoreboard players reset @s annonceTicks
scoreboard players reset @s ArretUrgence
execute as @s[scores={VoieReservee=1..2}] at @e[tag=Direction,distance=..50] if score @e[tag=Direction,sort=nearest,limit=1] idLine = @s idLine if score @e[tag=Direction,sort=nearest,limit=1] FileAttente matches 1.. run scoreboard players remove @e[tag=Direction,sort=nearest,limit=1] FileAttente 1
scoreboard players set @s VoieReservee 0
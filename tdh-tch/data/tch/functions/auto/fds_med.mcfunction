# On indique aux métros des lignes 2/3/7 que c'est la fin de service
tag @e[tag=Metro,scores={idLine=21..39}] add FinDeService
tag @e[tag=Metro,scores={idLine=71..72}] add FinDeService

# On joue l'annonce de fin de service pour informer les joueurs
execute at @p[tag=tchInside] at @e[tag=NumLigne,sort=nearest,distance=..30,limit=1] if score @e[sort=nearest,limit=1] idLine matches 21..39 run function tch:sound/metro/fin_service1
execute at @p[tag=tchInside] at @e[tag=NumLigne,sort=nearest,distance=..30,limit=1] if score @e[sort=nearest,limit=1] idLine matches 71..72 run function tch:sound/metro/fin_service2
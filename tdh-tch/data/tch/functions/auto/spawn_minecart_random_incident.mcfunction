# On définit la variable spawnAt à l'heure de fin d'incident
# On enregistre le jour, le mois, l'année et le tick
scoreboard players operation @s idJour = @e[type=item_frame,tag=ourPC,limit=1] idJour
scoreboard players operation @s spawnAt = @e[type=item_frame,tag=ourPC,limit=1] finIncident
# On ajoute une journée si le tick de fin d'incident est >48000
execute if score @s spawnAt >= #temps fullDayTicks run scoreboard players add @s idJour 1
execute if score @s spawnAt >= #temps fullDayTicks run scoreboard players operation @s spawnAt -= #temps fullDayTicks

# On fait la même chose dans l'autre direction si elle existe
tag @s add us
execute at @e[tag=Direction,distance=..50] if score @s idLine = @e[tag=Direction,sort=nearest,limit=1] idLine at @e[tag=NumLigne,sort=nearest,limit=1] as @e[tag=SpawnMetro,tag=!us,sort=nearest,limit=1] run tag @s add notUs
scoreboard players operation @e[tag=notUs,sort=nearest,limit=1] idJour = @s idJour
scoreboard players operation @e[tag=notUs,sort=nearest,limit=1] spawnAt = @s spawnAt
tag @s remove us
tag @e[tag=notUs] remove notUs

# On calcule l'heure pour son affichage
scoreboard players operation #store currentHour = @s spawnAt
function tdh:store/time

# On tag notre armor stand NumLigne
execute at @e[tag=Direction,sort=nearest,distance=..50] if score @s idLine = @e[tag=Direction,sort=nearest,limit=1] idLine run tag @e[tag=NumLigne,sort=nearest,limit=1] add ourAS

# On informe les joueurs présents sur un quai
execute at @e[tag=ourAS,limit=1] positioned ~-19.5 ~-1.5 ~-19.5 run tellraw @a[dx=39,dy=9,dz=39,tag=!SpeakingToTCHAgent] [{"text":"","color":"gray"},{"selector":"@e[tag=ourAS,limit=1]"},{"text":" : "},{"text":"Le trafic est "},{"text":"interrompu ","color":"red"},{"text":"en raison "},{"nbt":"Item.tag.tch.incident.generic.deDu","entity":"@e[type=item_frame,tag=ourPC,limit=1]"},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@e[type=item_frame,tag=ourPC,limit=1]","color":"yellow"},{"text":".\nReprise estimée vers "},{"storage":"tdh:store","nbt":"time","interpret":"true","color":"yellow"},{"text":"."}]

# On fait en sorte que l'armor stand diffuse une annonce informative
execute as @e[tag=ourAS,limit=1] unless score @s annonceID matches 1.. run scoreboard players set @s annonceTicks 1
execute as @e[tag=ourAS,limit=1] unless score @s annonceID matches 1000 run scoreboard players set @s annonceIndex 0
execute as @e[tag=ourAS,limit=1] unless score @s annonceID matches 1000 run scoreboard players set @s annonceID 1000

# On supprime le tag temporaire
tag @e[tag=ourAS] remove ourAS
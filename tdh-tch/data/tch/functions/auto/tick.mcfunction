# GESTION DES SPAWNERS
# Si un spawner n'a pas d'heure de spawn prévue, on en génère une
execute as @e[tag=SpawnMetro] unless score @s spawnAt matches 0.. at @s run function tch:auto/spawn_minecart_random_try

# Si un spawner a une heure de spawn prévue, on fait les calculs pour savoir si c'est l'heure
execute as @e[tag=SpawnMetro,scores={spawnAt=0..}] at @s run function tch:auto/spawn_minecart_tick_try


# GESTION DES MÉTROS À QUAI
# Si un métro a un score remainingTicks (signifiant qu'il doit repartir dans X secondes), on décrémente ce score
execute as @e[tag=Metro,scores={remainingTicks=1..}] run scoreboard players remove @s remainingTicks 1

# Si un métro doit être lancé (remainingTicks = 0), on le lance
execute as @e[tag=Metro,scores={remainingTicks=..0}] unless score @s ArretUrgence matches 1 at @s rotated as @s run function tch:auto/launch

# Si un métro ayant fait un arret d'urgence doit être lancé, on vérifie d'abord si la voie est libre
execute as @e[tag=Metro,scores={ArretUrgence=1,remainingTicks=..0}] at @s rotated as @s run function tch:auto/relaunch_try

# 6 secondes avant le départ, on lance le son de départ
execute at @e[tag=Metro,scores={remainingTicks=6}] run function tch:sound/metro/signal_depart


# GESTION GLOBALE DES MÉTROS
# Pour chaque ligne ayant terminé son service, on vérifie si l'heure de début de service a été dépassée
# Cela fonctionne seulement si l'heure actuelle est propice à un début de service (4h-20h)
execute if score #temps currentTdhTick matches 7980..40020 as @e[tag=tchPC,tag=FinDeService] run function tch:ligne/test_debut_service
# Pour chaque ligne n'ayant pas terminé son service, on vérifie si l'heure de fin de service a été dépassée
# Cela fonctionne seulement si l'heure actuelle est propice à une fin de service (14h-4h)
# On ne le fait que pour les lignes pouvant terminer leur service (pas de tag PasDeFinDeServiceAjd)
execute unless score #temps currentTdhTick matches 8020..27980 as @e[tag=tchPC,tag=!FinDeService,tag=!PasDeFinDeServiceAjd] run function tch:ligne/test_fin_service

# Gestion des incidents (dans une sous-fonction)
function tch:ligne/incident/tick



# On reschedule la fonction dans 1s si le système est actif
#execute if score #metro on matches 1 run schedule function tch:auto/tick 20t
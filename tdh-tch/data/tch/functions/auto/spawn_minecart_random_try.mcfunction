# Cette fonction est exécutée par l'item frame SpawnMetro, pour tester si un incident est en cours sur la ligne
# Si on n'a pas d'ID ligne enregistré, on le récupère
execute unless score @s idLine matches 1.. run function tch:auto/get_id_line

# On tagge le PC correspondant à notre ligne (seulement si elle est en service)
execute at @e[tag=tchPC,tag=EnService] if score @e[tag=tchPC,sort=nearest,limit=1] idLine <= @s idLine if score @e[tag=tchPC,sort=nearest,limit=1] idLineMax >= @s idLine run tag @e[tag=tchPC,sort=nearest,limit=1] add ourPC
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @a [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"@s","objective":"idLine"},"color":"dark_red"},{"text":"... "},{"text":"(dans spawn_minecart_random)","italic":"true"}]


# Si un incident est en cours et qu'il a été détecté
execute if entity @e[type=item_frame,tag=ourPC,tag=IncidentEnCours,scores={status=1..}] run tag @s add incident
execute as @s[tag=incident] run function tch:auto/spawn_minecart_random_incident

# Si notre PC n'existe pas
execute unless entity @e[type=item_frame,tag=ourPC] run function tch:auto/spawn_minecart_random_closed

# Si aucun incident n'est en cours et que le trafic n'est pas terminé, on calcule normalement l'heure de spawnAt
execute as @s[tag=!incident] if entity @e[type=item_frame,tag=ourPC,tag=!FinDeService] run function tch:auto/spawn_minecart_random
# Sinon, si le trafic est terminé, on calcule une heure de spawnAt pour demain
execute as @s[tag=!incident] if entity @e[type=item_frame,tag=ourPC,tag=FinDeService] run function tch:auto/spawn_minecart_random_fds

# On nettoie les tags temporaires
tag @s remove incident
tag @e[type=item_frame,tag=ourPC] remove ourPC
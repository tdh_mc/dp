# On attribue à chaque PC son numéro de ligne
scoreboard players set @s[tag=pcM1] idLine 11
scoreboard players set @s[tag=pcM1] idLineMax 12
scoreboard players set @s[tag=pcM2] idLine 21
scoreboard players set @s[tag=pcM2] idLineMax 22
scoreboard players set @s[tag=pcM3] idLine 31
scoreboard players set @s[tag=pcM3] idLineMax 32
scoreboard players set @s[tag=pcM4] idLine 41
scoreboard players set @s[tag=pcM4] idLineMax 42
scoreboard players set @s[tag=pcM5a] idLine 51
scoreboard players set @s[tag=pcM5a] idLineMax 52
scoreboard players set @s[tag=pcM5b] idLine 55
scoreboard players set @s[tag=pcM5b] idLineMax 56
scoreboard players set @s[tag=pcM6] idLine 61
scoreboard players set @s[tag=pcM6] idLineMax 62
scoreboard players set @s[tag=pcM7] idLine 71
scoreboard players set @s[tag=pcM7] idLineMax 72
scoreboard players set @s[tag=pcM7b] idLine 73
scoreboard players set @s[tag=pcM7b] idLineMax 74
scoreboard players set @s[tag=pcM8] idLine 81
scoreboard players set @s[tag=pcM8] idLineMax 82
scoreboard players set @s[tag=pcM8b] idLine 83
scoreboard players set @s[tag=pcM8b] idLineMax 84

# Heures maximales de service (au delà, les métros ne spawnent pas, et les métros existants font terminus à la prochaine station)
# scoreboard players set @s[tag=pcM1] debutService 8000
# scoreboard players set @s[tag=pcM6] debutService 8000
# scoreboard players set @s[tag=pcM8] debutService 8000
# scoreboard players set @s[tag=pcM1] finService 1570
# scoreboard players set @s[tag=pcM6] finService 1570
# scoreboard players set @s[tag=pcM8] finService 1570

# scoreboard players set @s[tag=pcM2] debutService 10000
# scoreboard players set @s[tag=pcM3] debutService 10000
# scoreboard players set @s[tag=pcM7] debutService 10000
# scoreboard players set @s[tag=pcM2] finService 0
# scoreboard players set @s[tag=pcM3] finService 0
# scoreboard players set @s[tag=pcM7] finService 0

# scoreboard players set @s[tag=pcM4] debutService 12000
# scoreboard players set @s[tag=pcM5a] debutService 12000
# scoreboard players set @s[tag=pcM5b] debutService 12000
# scoreboard players set @s[tag=pcM7b] debutService 12000
# scoreboard players set @s[tag=pcM8b] debutService 12000
# scoreboard players set @s[tag=pcM4] finService 45000
# scoreboard players set @s[tag=pcM5a] finService 45000
# scoreboard players set @s[tag=pcM5b] finService 45000
# scoreboard players set @s[tag=pcM7b] finService 45000
# scoreboard players set @s[tag=pcM8b] finService 45000

# Chances d'incident journalières (pour 1000)
# scoreboard players set @s[tag=pcM1] chancesIncident 5
# scoreboard players set @s[tag=pcM2] chancesIncident 10
# scoreboard players set @s[tag=pcM3] chancesIncident 10
# scoreboard players set @s[tag=pcM4] chancesIncident 50
# scoreboard players set @s[tag=pcM5a] chancesIncident 15
# scoreboard players set @s[tag=pcM5b] chancesIncident 15
# scoreboard players set @s[tag=pcM6] chancesIncident 10
# scoreboard players set @s[tag=pcM7] chancesIncident 20
# scoreboard players set @s[tag=pcM7b] chancesIncident 25
# scoreboard players set @s[tag=pcM8] chancesIncident 5
# scoreboard players set @s[tag=pcM8b] chancesIncident 25
# Cette fonction est exécutée tous les jours entre 2 et 3h du matin (lorsqu'aucun métro ne circule).
# Elle détermine si des lignes feront l'objet d'incidents au cours de la prochaine journée.
# On enregistre le fait qu'on a calculé les incidents aujourd'hui
scoreboard players operation #tchIncident dateJour = #temps dateJour

# On vérifie qu'on a bien initialisé les ID de ligne pour chaque PC
execute as @e[tag=tchPC] unless score @s idLine matches 1.. run function tch:auto/incident/get_id_line

# On récupère une variable aléatoire pour chaque ligne
execute as @e[tag=tchPC] store result score @s temp run data get entity @e[type=#tdh:random,sort=random,limit=1] Pos[2] 120
scoreboard players set #metro temp 2000
execute as @e[tag=tchPC] run scoreboard players operation @s temp %= #metro temp

# On vérifie pour chaque ligne si elle génère un incident
execute as @e[tag=tchPC] run scoreboard players set @s incident 0
execute as @e[tag=tchPC] if score @s temp < @s chancesIncident run function tch:auto/incident/gen


# En cas d'incident sur une ligne, on affiche un message de debug
#execute as @e[tag=tchPC] if score @s incident matches 1.. run tellraw @a [{"text":"[DEBUG] - ","color":"red"},{"text":"Incident sur la ligne ","color":"gray"},{"selector":"@s"},{"text":".","color":"gray"}]

# On réinitialise les variables temporaires
scoreboard players reset #metro temp
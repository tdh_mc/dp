# On récupère une pos au hasard pour l'aléatoire du code incident
execute store result score @s temp2 run data get entity @e[type=#tdh:random,sort=random,limit=1] Pos[2] 87
scoreboard players set #metro temp 1000
scoreboard players operation @s temp2 %= #metro temp

# On récupère une 2e et 3e pos aléatoire car "ça peut servir"
execute store result score @s temp3 run data get entity @e[type=#tdh:random,sort=random,limit=1] Pos[2] 131
scoreboard players operation @s temp3 %= #metro temp
execute store result score @s temp4 run data get entity @e[type=#tdh:random,sort=random,limit=1] Pos[2] 131
scoreboard players operation @s temp4 %= #metro temp

# On choisit le code incident en fonction du nombre aléatoire

# 10, 11, 12 (incidents affectant la voie) - 10%
execute if score @s temp2 matches 0..19 run scoreboard players set @s incident 10
execute if score @s temp2 matches 20..69 run scoreboard players set @s incident 11
execute if score @s temp2 matches 70..99 run scoreboard players set @s incident 12
# 20, 21 (panne de signalisation) - 10%
execute if score @s temp2 matches 100..179 run scoreboard players set @s incident 20
execute if score @s temp2 matches 180..199 run scoreboard players set @s incident 21
# 30, 31, 32, 33 (incident technique) - 5%
execute if score @s temp2 matches 200..209 run scoreboard players set @s incident 30
execute if score @s temp2 matches 210..226 run scoreboard players set @s incident 31
execute if score @s temp2 matches 227..239 run scoreboard players set @s incident 32
execute if score @s temp2 matches 240..249 run scoreboard players set @s incident 33
# 40, 41, 42, 43, 44 (incident voyageur) - 20%
execute if score @s temp2 matches 250..269 run scoreboard players set @s incident 40
execute if score @s temp2 matches 270..299 run scoreboard players set @s incident 41
execute if score @s temp2 matches 300..379 run scoreboard players set @s incident 42
execute if score @s temp2 matches 380..419 run scoreboard players set @s incident 43
execute if score @s temp2 matches 420..449 run scoreboard players set @s incident 44
# 50, 51, 52, 53 (malaise voyageur) - 10%
execute if score @s temp2 matches 450..499 run scoreboard players set @s incident 50
execute if score @s temp2 matches 500..514 run scoreboard players set @s incident 51
execute if score @s temp2 matches 515..534 run scoreboard players set @s incident 52
execute if score @s temp2 matches 535..549 run scoreboard players set @s incident 53
# 60, 61, 62, 63, 64, 65 (panne mécanique) - 15%
execute if score @s temp2 matches 550..569 run scoreboard players set @s incident 60
execute if score @s temp2 matches 570..599 run scoreboard players set @s incident 61
execute if score @s temp2 matches 600..609 run scoreboard players set @s incident 62
execute if score @s temp2 matches 610..639 run scoreboard players set @s incident 63
execute if score @s temp2 matches 640..669 run scoreboard players set @s incident 64
execute if score @s temp2 matches 670..699 run scoreboard players set @s incident 65
# 70, 71, 72, 73, 74 (mesures de sécurité) - 5%
execute if score @s temp2 matches 700..704 run scoreboard players set @s incident 70
execute if score @s temp2 matches 705..719 run scoreboard players set @s incident 71
execute if score @s temp2 matches 720..734 run scoreboard players set @s incident 72
execute if score @s temp2 matches 735..739 run scoreboard players set @s incident 73
execute if score @s temp2 matches 740..749 run scoreboard players set @s incident 74
# 80, 81, 82, 83 (incident d'exploitation) - 10%
execute if score @s temp2 matches 750..799 run scoreboard players set @s incident 80
execute if score @s temp2 matches 800..809 run scoreboard players set @s incident 81
execute if score @s temp2 matches 810..824 run scoreboard players set @s incident 82
execute if score @s temp2 matches 825..849 run scoreboard players set @s incident 83
# 90, 91, 92, 93, 94 (malveillance) - 5%
execute if score @s temp2 matches 850..864 run scoreboard players set @s incident 90
execute if score @s temp2 matches 865..869 run scoreboard players set @s incident 91
execute if score @s temp2 matches 870..879 run scoreboard players set @s incident 92
execute if score @s temp2 matches 880..884 run scoreboard players set @s incident 93
execute if score @s temp2 matches 885..899 run scoreboard players set @s incident 94
# 100, 101, 102, 103, 104 (divers) - 10%
execute if score @s temp2 matches 900..909 run scoreboard players set @s incident 100
execute if score @s temp2 matches 910..929 run scoreboard players set @s incident 101
execute if score @s temp2 matches 930..939 run scoreboard players set @s incident 102
execute if score @s temp2 matches 940..989 run scoreboard players set @s incident 103
execute if score @s temp2 matches 990..999 run scoreboard players set @s incident 104
# 110, 111, 112, 113 (conditions météo) - NON IMPLEMENTE POUR L'INSTANT


# En fonction du type d'incident, on génère un début et une fin d'incident
# Si c'est un incident grave, ça dure toute la journée à partir de l'incident ;
# notre heure de fin par défaut est donc 3h du matin (48000+6000=54000t)
scoreboard players set @s finIncident 54000

# On choisit une heure de début
# Dans le cas d'un incident affectant la voie/la signalisation, c'est toute la journée (à partir de 4h du matin)
execute if score @s incident matches 10..29 run scoreboard players set @s debutIncident 8000
# Dans le cas d'un incident technique, c'est soit toute la journée, soit à une heure aléatoire
execute if score @s incident matches 30..39 if score @s temp3 matches 0..499 run scoreboard players set @s debutIncident 8000
execute if score @s incident matches 30..39 if score @s temp3 matches 500..999 run tag @s add randomDebutIncident
# Dans le cas d'un incident voyageur, malaise voyageur, panne mécanique, mesures de sécurité, c'est toujours à une heure aléatoire
execute if score @s incident matches 40..79 run tag @s add randomDebutIncident
# Dans le cas d'un incident d'exploitation, de malveillance ou de divers incidents, c'est soit toute la journée, soit à une heure aléatoire
execute if score @s incident matches 80..109 if score @s temp3 matches 0..349 run scoreboard players set @s debutIncident 8000
execute if score @s incident matches 80..109 if score @s temp3 matches 350..999 run tag @s add randomDebutIncident

# Si on doit générer une heure de début aléatoire, on la génère
scoreboard players set @s[tag=randomDebutIncident] debutIncident 400
scoreboard players operation @s[tag=randomDebutIncident] debutIncident += @s[tag=randomDebutIncident] temp3
scoreboard players set @s[tag=randomDebutIncident] temp9 30
scoreboard players operation @s[tag=randomDebutIncident] debutIncident *= @s[tag=randomDebutIncident] temp9
scoreboard players reset @s[tag=randomDebutIncident] temp9

# Si l'incident ne dure pas toute la journée, ou qu'il n'est pas grave, on génère une heure de fin aléatoire
execute unless score @s debutIncident matches ..8500 run tag @s add randomFinIncident
execute if score @s incident matches 20..39 run tag @s add randomFinIncident
execute if score @s incident matches 90..99 run tag @s add randomFinIncident

# Si on doit générer une heure de fin aléatoire, on la génère
scoreboard players operation @s[tag=randomFinIncident] finIncident = @s[tag=randomFinIncident] debutIncident
scoreboard players set @s[tag=randomFinIncident] temp9 24
scoreboard players operation @s[tag=randomFinIncident] temp9 *= @s[tag=randomFinIncident] temp4
scoreboard players operation @s[tag=randomFinIncident] finIncident += @s[tag=randomFinIncident] temp9
scoreboard players reset @s[tag=randomFinIncident] temp9

# On se retire les tags temporaires
tag @s remove randomDebutIncident
tag @s remove randomFinIncident
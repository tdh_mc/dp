# Le PC est déjà enregistré par spawn_minecart_random_try

# On enregistre le jour actuel
scoreboard players operation @s idJour = #temps idJour
scoreboard players operation @s jourSemaine = #temps jourSemaine

# Si on est après l'heure de début de service, cela veut dire qu'on est le soir (puisqu'on est en fin de service)
# On doit donc rajouter un jour pour générer un horaire pour le lendemain
execute if score #temps currentTdhTick > @e[type=item_frame,tag=ourPC,limit=1] debutServiceAjd run scoreboard players add @s idJour 1
execute if score #temps currentTdhTick > @e[type=item_frame,tag=ourPC,limit=1] debutServiceAjd run scoreboard players add @s jourSemaine 1

# On récupère une rotation au hasard pour l'aléatoire (0-360 ticks de délai par rapport au début de service)
execute store result score @s spawnAt run data get entity @e[type=#tdh:random,sort=random,limit=1] Rotation[0] 1
# On prend le modulo, juste au cas où on dépasse
scoreboard players set @s temp 360
scoreboard players operation @s spawnAt %= @s temp
# On ajoute 100 ticks de sécurité pour éviter le skip de la journée entière
scoreboard players add @s spawnAt 100

# On ajoute l'heure de début de service pour obtenir l'heure de spawn
scoreboard players operation @s spawnAt += @e[type=item_frame,tag=ourPC,limit=1] debutService
# Si c'est le week-end DEMAIN, on ajoute le modificateur
execute if score @s jourSemaine matches 6..7 run scoreboard players operation @s spawnAt += @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe


# On se branle complètement du temps d'attente en minutes

# Calcul de l'heure et de la minute
scoreboard players operation #store currentHour = @s spawnAt
function tdh:store/time


# On enregistre notre armor stand
execute at @e[tag=Direction,distance=..50] if score @e[tag=Direction,sort=nearest,limit=1] idLine = @s idLine as @e[tag=NumLigne,sort=nearest,limit=1] run tag @s add ourAS

# Ensuite on avertit tous les gens dans un parallélépipède de 39x9x39 centré sur la position de l'armor stand centrale
# Les dimensions d'un quai étant de 25x43 (spawners inclus), cela devrait recouvrir la totalité de la zone
execute at @e[tag=ourAS,limit=1] positioned ~-19.5 ~-1.5 ~-19.5 run tellraw @a[dx=39,dy=9,dz=39,tag=!SpeakingToTCHAgent] [{"text":"","color":"gray"},{"selector":"@e[tag=ourAS,limit=1]"},{"text":" → "},{"selector":"@s"},{"text":" : le prochain "},{"text":"demain matin","color":"red"},{"text":" (à "},{"storage":"tdh:store","nbt":"time","interpret":"true"},{"text":")"}]



# Réinitialisation des variables temporaires
scoreboard players reset @s temp2
scoreboard players reset @s temp3
tag @e[tag=ourAS] remove ourAS
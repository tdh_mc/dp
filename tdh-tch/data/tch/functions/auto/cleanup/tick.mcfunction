# Pour chaque direction dont la voie est réservée, on donne le tag trueRes s'il y a effectivement un métro à proximité qui peut avoir réservé la voie
execute as @e[tag=Direction,scores={VoieReservee=1..}] at @s at @e[tag=Metro,distance=..200] unless score @e[tag=Metro,sort=nearest,limit=1] ArretUrgence matches 1 if score @e[tag=Metro,sort=nearest,limit=1] idLine = @s idLine run tag @s add trueRes

# Si on a une voie réservée sans avoir le tag trueRes, on déréserve notre voie
execute as @e[tag=Direction,scores={VoieReservee=1..},tag=!trueRes] at @s run tellraw @a [{"text":"Annulation de la réservation du quai direction ","color":"gold"},{"selector":"@s"},{"text":" à la station "},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]","color":"red"},{"text":"."}]
execute as @e[tag=Direction,scores={VoieReservee=1..},tag=!trueRes] run scoreboard players set @s VoieReservee 0

# On schedule à nouveau cette fonction dans 10 secondes si le système est actif
#execute if score #tchMetroCleanup on matches 1 run schedule function tch:auto/cleanup/tick 10s

# On nettoie le tag temporaire
tag @e[tag=trueRes] remove trueRes
# Le PC est déjà enregistré par spawn_minecart_random_try

# On se tag si on est en heure de pointe
execute if score #temps currentTdhTick >= @e[type=item_frame,tag=ourPC,limit=1] debutPointe1 if score #temps currentTdhTick <= @e[type=item_frame,tag=ourPC,limit=1] finPointe1 run tag @s add FrequencePointe
execute if score #temps currentTdhTick >= @e[type=item_frame,tag=ourPC,limit=1] debutPointe1 if score #temps currentTdhTick <= @e[type=item_frame,tag=ourPC,limit=1] finPointe1 run tag @s add FrequencePointe

# On enregistre le jour actuel
scoreboard players operation @s idJour = #temps idJour

# On récupère une rotation au hasard pour l'aléatoire (0-3599)
execute store result score @s spawnAt run data get entity @e[type=#tdh:random,sort=random,limit=1] Rotation[0] 10
# On prend le modulo, juste au cas où on dépasse
scoreboard players set @s temp 3600
# Si on est en heure de pointe, on n'a que la moitié de la variation potentielle
scoreboard players operation @s spawnAt %= @s temp
# On retire 1800 pour avoir une variable alignée sur 0
scoreboard players remove @s spawnAt 1800

# On a au départ une variable aléatoire comprise entre -1800 et 1799
# On multiplie cette rotation par la fréquence moyenne de la ligne
# On multiplie par la fréquence de pointe si on est en heure de pointe 1 ou 2 ;
scoreboard players operation @s[tag=FrequencePointe] spawnAt *= @e[type=item_frame,tag=ourPC,limit=1] frequencePointe
# On multiplie par la fréquence creuse dans le cas contraire
scoreboard players operation @s[tag=!FrequencePointe] spawnAt *= @e[type=item_frame,tag=ourPC,limit=1] frequenceCreuse
# Enfin, on divise par 3600 (l'amplitude initiale de la variable aléatoire)
scoreboard players set @s temp 3600
scoreboard players operation @s spawnAt /= @s temp
# On a au final une variable aléatoire comprise entre (-fréquence/2) et (fréquence/2)
# Si on est en heure de pointe, on divise cette variable par 2 pour avoir moins de variation
scoreboard players set @s temp7 2
scoreboard players operation @s[tag=FrequencePointe] spawnAt /= @s temp7

# On ajoute la fréquence
# On aura donc une variable aléatoire comprise entre fréquence/2 et 3fréquence/2 si on est en creuse,
# et entre 3fréquence/4 et 5fréquence/4 si on est en pointe
scoreboard players operation @s[tag=FrequencePointe] temp6 = @e[type=item_frame,tag=ourPC,limit=1] frequencePointe
scoreboard players operation @s[tag=!FrequencePointe] temp6 = @e[type=item_frame,tag=ourPC,limit=1] frequenceCreuse
scoreboard players operation @s spawnAt += @s temp6

# On ajoute le tick actuel (pour passer en gros de "SpawnIN" à réellement SpawnAt)
scoreboard players operation @s spawnAt += #temps currentTdhTick




# Si le tick actuel dépasse, on le normalise et on incrémente l'id du jour de spawn
execute if score @s spawnAt >= #temps fullDayTicks run scoreboard players add @s idJour 1
execute if score @s spawnAt >= #temps fullDayTicks run scoreboard players operation @s spawnAt -= #temps fullDayTicks

# On calcule le temps d'attente en minutes
scoreboard players operation #store currentHour = @s spawnAt
scoreboard players operation #store currentHour -= #temps currentTdhTick
function tdh:store/time_delay

# On calcule l'heure précise d'arrivée
scoreboard players operation #store2 currentHour = @s spawnAt
function tdh:store/time2


# On enregistre notre armor stand
execute at @e[tag=Direction,distance=..50] if score @e[tag=Direction,sort=nearest,limit=1] idLine = @s idLine as @e[tag=NumLigne,sort=nearest,limit=1] run tag @s add ourAS

# On enregistre les variables nécessaires dans le storage
# Le "dernier" dans ou le "prochain" dans
data remove storage tch:pid dernierOuNon
execute if score @s spawnAt >= @e[type=item_frame,tag=ourPC,limit=1] finServiceAjd if score #temps currentTdhTick < @e[type=item_frame,tag=ourPC,limit=1] finServiceAjd run data modify storage tch:pid dernierOuNon set value "dernier"
execute unless data storage tch:pid dernierOuNon run data modify storage tch:pid dernierOuNon set value "prochain"

# Ensuite on avertit tous les gens dans un parallélépipède de 39x9x39 centré sur la position de l'armor stand centrale
# Les dimensions d'un quai étant de 25x43 (spawners inclus), cela devrait recouvrir la totalité de la zone
execute at @e[tag=ourAS,limit=1] positioned ~-19.5 ~-1.5 ~-19.5 run tellraw @a[dx=39,dy=9,dz=39,tag=!SpeakingToTCHAgent] [{"text":"","color":"gray"},{"selector":"@e[tag=ourAS,limit=1]"},{"text":" → "},{"selector":"@s"},{"text":" : le "},{"storage":"tch:pid","nbt":"dernierOuNon"},{"text":" dans "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"red"},{"text":" (à "},{"storage":"tdh:store","nbt":"time2","interpret":"true"},{"text":")"}]



# Réinitialisation des variables temporaires
scoreboard players reset @s temp
scoreboard players reset @s temp2
scoreboard players reset @s temp3
scoreboard players reset @s temp4
scoreboard players reset @s temp6
scoreboard players reset @s temp7
tag @s remove FrequencePointe
tag @e[tag=ourAS] remove ourAS
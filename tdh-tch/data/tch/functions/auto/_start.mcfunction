# On passe la variable qui détermine l'activation du système à 1
scoreboard players set #metro on 1

# On lance la boucle principale du métro
function tch:auto/tick
function tch:auto/title/tick
function tch:auto/player/tick
scoreboard players set @s incidentEnCours 1

execute store result score @s temp run data get entity @s Motion[0] 1000
execute store result score @s temp2 run data get entity @s Motion[2] 1000

execute if score @s temp matches 10.. run function tch:auto/add_minecart_est_long
execute if score @s temp matches ..-10 run function tch:auto/add_minecart_ouest_long
execute if score @s temp2 matches 10.. run function tch:auto/add_minecart_sud_long
execute if score @s temp2 matches ..-10 run function tch:auto/add_minecart_nord_long

function tch:auto/stop_minecart
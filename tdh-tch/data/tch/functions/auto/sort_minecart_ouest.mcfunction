scoreboard players set @e[type=minecart,distance=..5,limit=1,sort=nearest] hasPassenger 0
execute as @e[type=minecart,distance=..5,limit=1,sort=nearest] at @s if entity @p[distance=..1] run scoreboard players set @s hasPassenger 1

execute if score @e[type=minecart,distance=..5,limit=1,sort=nearest] hasPassenger matches 1 run setblock ~-2 ~2 ~ rail[shape=south_east] replace
execute unless score @e[type=minecart,distance=..5,limit=1,sort=nearest] hasPassenger matches 1 run setblock ~-2 ~2 ~ rail[shape=north_east] replace

execute as @e[type=minecart,distance=..5,limit=1,sort=nearest] as @s[scores={hasPassenger=1}] run scoreboard players reset @s currentTick
execute as @e[type=minecart,distance=..5,limit=1,sort=nearest] as @s[scores={hasPassenger=1}] run scoreboard players reset @s dateJour
execute as @e[type=minecart,distance=..5,limit=1,sort=nearest] as @s[scores={hasPassenger=1}] run scoreboard players reset @s dateMois
execute as @e[type=minecart,distance=..5,limit=1,sort=nearest] as @s[scores={hasPassenger=1}] run scoreboard players reset @s dateAnnee

scoreboard players reset @e[type=minecart,distance=..5,limit=1,sort=nearest] hasPassenger

# Ensuite on ajoute une durée d'attente aléatoire dépendant de l'heure
# 0h00 - 4h00 [pas de service — prochain train à 4h]
execute if score #temps currentTdhTick matches 0..7999 run scoreboard players set @s spawnAt 8000
execute if score #temps currentTdhTick matches 0..7999 run scoreboard players set @s spawnMod 800

# 4h00 - 5h30 [heure très creuse matin]
execute if score #temps currentTdhTick matches 8000..10999 run scoreboard players add @s spawnAt 1000
execute if score #temps currentTdhTick matches 8000..10999 run scoreboard players set @s spawnMod 650

# 5h30 - 7h30 [heure creuse matin]
execute if score #temps currentTdhTick matches 11000..14999 run scoreboard players add @s spawnAt 700
execute if score #temps currentTdhTick matches 11000..14999 run scoreboard players set @s spawnMod 300

# 7h30 - 9h15 [heure de pointe matin]
execute if score #temps currentTdhTick matches 15000..18499 run scoreboard players add @s spawnAt 400
execute if score #temps currentTdhTick matches 15000..18499 run scoreboard players set @s spawnMod 100

# 9h15 - 12h00 [heure creuse fin matin]
execute if score #temps currentTdhTick matches 18500..23999 run scoreboard players add @s spawnAt 700
execute if score #temps currentTdhTick matches 18500..23999 run scoreboard players set @s spawnMod 500

# 12h00 - 16h00 [heure creuse après-midi]
execute if score #temps currentTdhTick matches 24000..31999 run scoreboard players add @s spawnAt 600
execute if score #temps currentTdhTick matches 24000..31999 run scoreboard players set @s spawnMod 400

# 16h00 - 18h00 [heure moins creuse après-midi]
execute if score #temps currentTdhTick matches 32000..35999 run scoreboard players add @s spawnAt 500
execute if score #temps currentTdhTick matches 32000..35999 run scoreboard players set @s spawnMod 300

# 18h00 - 20h30 [heure de pointe soir]
execute if score #temps currentTdhTick matches 36000..40999 run scoreboard players add @s spawnAt 420
execute if score #temps currentTdhTick matches 36000..40999 run scoreboard players set @s spawnMod 140

# 20h30 - 22h30 [heure creuse soirée]
execute if score #temps currentTdhTick matches 41000..44999 run scoreboard players add @s spawnAt 600
execute if score #temps currentTdhTick matches 41000..44999 run scoreboard players set @s spawnMod 400

# 22h30 - 00h00 [heure très creuse soirée]
execute if score #temps currentTdhTick matches 45000.. run scoreboard players add @s spawnAt 1000
execute if score #temps currentTdhTick matches 45000.. run scoreboard players set @s spawnMod 400
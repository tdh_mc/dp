
# Ensuite on ajoute une durée d'attente aléatoire dépendant de l'heure
# 23h00 - 5h00 [pas de service — prochain train à 5h]
execute unless score #temps currentTdhTick matches 10000..45999 run scoreboard players set @s spawnAt 10000
execute unless score #temps currentTdhTick matches 10000..45999 run scoreboard players set @s spawnMod 1000

# 5h00 - 6h30 [heure très creuse matin]
execute if score #temps currentTdhTick matches 10000..12999 run scoreboard players add @s spawnAt 800
execute if score #temps currentTdhTick matches 10000..12999 run scoreboard players set @s spawnMod 800

# 6h30 - 8h00 [heure creuse matin]
execute if score #temps currentTdhTick matches 13000..15999 run scoreboard players add @s spawnAt 800
execute if score #temps currentTdhTick matches 13000..15999 run scoreboard players set @s spawnMod 400

# 8h00 - 9h30 [heure de pointe matin]
execute if score #temps currentTdhTick matches 16000..18999 run scoreboard players add @s spawnAt 500
execute if score #temps currentTdhTick matches 16000..18999 run scoreboard players set @s spawnMod 150

# 9h30 - 12h00 [heure creuse fin matin]
execute if score #temps currentTdhTick matches 19000..23999 run scoreboard players add @s spawnAt 850
execute if score #temps currentTdhTick matches 19000..23999 run scoreboard players set @s spawnMod 400

# 12h00 - 16h00 [heure creuse après-midi]
execute if score #temps currentTdhTick matches 24000..31999 run scoreboard players add @s spawnAt 750
execute if score #temps currentTdhTick matches 24000..31999 run scoreboard players set @s spawnMod 400

# 16h00 - 18h00 [heure moins creuse après-midi]
execute if score #temps currentTdhTick matches 32000..35999 run scoreboard players add @s spawnAt 600
execute if score #temps currentTdhTick matches 32000..35999 run scoreboard players set @s spawnMod 400

# 18h00 - 20h30 [heure de pointe soir]
execute if score #temps currentTdhTick matches 36000..40999 run scoreboard players add @s spawnAt 500
execute if score #temps currentTdhTick matches 36000..40999 run scoreboard players set @s spawnMod 100

# 20h30 - 21h30 [heure creuse soirée]
execute if score #temps currentTdhTick matches 41000..42999 run scoreboard players add @s spawnAt 800
execute if score #temps currentTdhTick matches 41000..42999 run scoreboard players set @s spawnMod 400

# 21h30 - 23h00 [heure très creuse soirée]
execute if score #temps currentTdhTick matches 43000..45999 run scoreboard players add @s spawnAt 800
execute if score #temps currentTdhTick matches 43000..45999 run scoreboard players set @s spawnMod 800
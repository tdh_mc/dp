# Si on n'a pas d'idJour, on se reset (conversion de l'ancien système avec dateJour/Mois/annee)
execute unless score @s idJour matches 1.. run scoreboard players reset @s spawnAt

# On vérifie si l'heure de spawn a été dépassée : si elle l'a été, on se donne le tag spawnNow
execute if score @s spawnAt <= #temps currentTdhTick if score @s idJour = #temps idJour run tag @s add spawnNow
execute if score @s idJour < #temps idJour run tag @s add spawnNow

# Si l'heure de spawn a été dépassée, on donne à l'armor stand de notre direction le tag spawnNowAS pour l'identifier plus facilement
execute as @s[tag=spawnNow] at @e[tag=Direction,distance=..50] if score @e[tag=Direction,sort=nearest,limit=1] idLine = @s idLine run tag @e[tag=Direction,sort=nearest,limit=1] add spawnNowAS
# On donne également le tag ourPC à notre PC
# (seulement si la ligne est en service ; sinon de toute façon on ne peut pas spawner)
execute as @s[tag=spawnNow] at @e[tag=tchPC,tag=EnService] if score @e[tag=tchPC,sort=nearest,limit=1] idLine <= @s idLine if score @e[tag=tchPC,sort=nearest,limit=1] idLineMax >= @s idLine run tag @e[tag=tchPC,sort=nearest,limit=1] add ourPC

#execute as @s[tag=spawnNow] run tellraw @a [{"text":""},{"selector":"@e[tag=spawnNowAS,limit=1]"},{"text":" VR "},{"score":{"name":"@e[tag=spawnNowAS,limit=1]","objective":"VoieReservee"},"color":"red"},{"text":" - FA "},{"score":{"name":"@e[tag=spawnNowAS,limit=1]","objective":"FileAttente"},"color":"red"}]

# Si on devrait spawner maintenant mais qu'on en est empêchés, on se donne le tag dontSpawn :
# - Si on n'est pas parvenus à trouver notre PC, on ne spawn pas
execute as @s[tag=spawnNow] unless entity @e[type=item_frame,tag=ourPC] run tag @s add dontSpawn
# - Si on n'est pas parvenus à trouver notre voie, on ne spawn pas
execute as @s[tag=spawnNow] unless entity @e[tag=spawnNowAS] run tag @s add dontSpawn
# - Si la voie est réservée
execute as @s[tag=spawnNow] if score @e[tag=spawnNowAS,limit=1] VoieReservee matches 1 run tag @s add dontSpawn
# - Si un autre métro est en file d'attente
execute as @s[tag=spawnNow] if score @e[tag=spawnNowAS,limit=1] FileAttente matches 1 run tag @s add dontSpawn
# - S'il n'y a pas de joueur dans un rayon de 50 blocs
execute as @s[tag=spawnNow] at @e[tag=spawnNowAS,limit=1] unless entity @p[distance=..50] run tag @s add dontSpawn
# - Si l'incident indiqué par le PC est en cours
execute as @s[tag=spawnNow] if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 1.. if score @e[type=item_frame,tag=ourPC,limit=1] debutIncident <= @s spawnAt if score @e[type=item_frame,tag=ourPC,limit=1] finIncident >= @s spawnAt run tag @s add dontSpawn
# - Si on est en dehors des horaires de passage
execute as @s[tag=spawnNow] if entity @e[type=item_frame,tag=ourPC,tag=FinDeService,limit=1] run tag @s add dontSpawn

# Si on a le tag dontSpawn, on génère un nouvel horaire de passage
execute as @s[tag=dontSpawn] run function tch:auto/spawn_minecart_random_try

# Si on n'a pas ce tag, on spawne un métro
execute as @s[tag=spawnNow,tag=!dontSpawn] run function tch:auto/spawn_minecart_tick

#execute as @s[tag=dontSpawn] run tellraw @a [{"text":"On n'a ","color":"gold"},{"text":"pas","color":"red"},{"text":" spawné."}]
#execute as @s[tag=spawnNow,tag=!dontSpawn] run tellraw @a [{"text":"On a spawné."}]

# On supprime éventuellement les tags
tag @s remove spawnNow
tag @s remove dontSpawn
tag @e[tag=spawnNowAS] remove spawnNowAS
tag @e[type=item_frame,tag=ourPC] remove ourPC
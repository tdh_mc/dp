# Si on n'a pas d'ID d'annonce, on génère une annonce
execute as @s[scores={annonceID=0}] run function tch:auto/annonce/gen_annonce_id

# Si on a déjà un ID d'annonce, on lance la sous-fonction correspondante :
# 10 : Confort_descente
execute as @s[scores={annonceID=10}] run function tch:auto/annonce/annonce/confort_descente
# 15 : Signal_sonore
execute as @s[scores={annonceID=15}] run function tch:auto/annonce/annonce/signal_sonore
# 20 : Interdit_fumer
execute as @s[scores={annonceID=20}] run function tch:auto/annonce/annonce/interdit_fumer
# 30 : Sacs_bagages
execute as @s[scores={annonceID=30}] run function tch:auto/annonce/annonce/sacs_bagages
# 35 : Oublie_sacs_bagages
execute as @s[scores={annonceID=35}] run function tch:auto/annonce/annonce/oublie_sacs_bagages
# 40 : Vendeur_sauvette
execute as @s[scores={annonceID=40}] run function tch:auto/annonce/annonce/vendeur_sauvette
# 45 : Achat_retour
execute as @s[scores={annonceID=45}] run function tch:auto/annonce/annonce/achat_retour
# 50 : Pickpockets
execute as @s[scores={annonceID=50}] run function tch:auto/annonce/annonce/pickpockets
# 90 : Evacuation
execute as @s[scores={annonceID=90}] run function tch:auto/annonce/annonce/evacuation

# 100 : Fin_service station fermée
execute as @s[scores={annonceID=100}] run function tch:auto/annonce/annonce/fin_service_station_ferme
# 110 : Fin_service
execute as @s[scores={annonceID=110}] run function tch:auto/annonce/annonce/fin_service
# 120 : Fin_service_RER
execute as @s[scores={annonceID=120}] run function tch:auto/annonce/annonce/fin_service_rer

# 210 : Trafic.x.travaux.m1
execute as @s[scores={annonceID=210}] run function tch:auto/annonce/annonce/trafic/x/travaux_m1
# 220 : Trafic.x.travaux.m2
execute as @s[scores={annonceID=220}] run function tch:auto/annonce/annonce/trafic/x/travaux_m2
# 230 : Trafic.x.travaux.m3
execute as @s[scores={annonceID=230}] run function tch:auto/annonce/annonce/trafic/x/travaux_m3
# 240 : Trafic.x.travaux.m4
execute as @s[scores={annonceID=240}] run function tch:auto/annonce/annonce/trafic/x/travaux_m4
# 250 : Trafic.x.travaux.m5
execute as @s[scores={annonceID=250}] run function tch:auto/annonce/annonce/trafic/x/travaux_m5
# 260 : Trafic.x.travaux.m6
execute as @s[scores={annonceID=260}] run function tch:auto/annonce/annonce/trafic/x/travaux_m6
# 290 : Trafic.x.travaux.m9
execute as @s[scores={annonceID=290}] run function tch:auto/annonce/annonce/trafic/x/travaux_m9
# 400 : Trafic.x.travaux.rera_global
execute as @s[scores={annonceID=400}] run function tch:auto/annonce/annonce/trafic/x/travaux_rera

# 500 : Trafic.x.incident_tech
execute as @s[scores={annonceID=500}] run function tch:auto/annonce/annonce/trafic/x/incident_technique
# 510 : Trafic.x.incident_bagage
execute as @s[scores={annonceID=510}] run function tch:auto/annonce/annonce/trafic/x/incident_bagage
# 520 : Trafic.x.incident_voyageur
execute as @s[scores={annonceID=520}] run function tch:auto/annonce/annonce/trafic/x/incident_voyageur
# 530 : Trafic.x.incident_accident
execute as @s[scores={annonceID=530}] run function tch:auto/annonce/annonce/trafic/x/incident_accident

# 600 : Trafic.p.incident_tech
execute as @s[scores={annonceID=600}] run function tch:auto/annonce/annonce/trafic/p/incident_technique
# 610 : Trafic.p.incident_voyageur
execute as @s[scores={annonceID=610}] run function tch:auto/annonce/annonce/trafic/p/incident_voyageur
# 680 : Trafic.p.reprise
execute as @s[scores={annonceID=680}] run function tch:auto/annonce/annonce/trafic/p/reprise
# 690 : Trafic.p.reprise_perturbe
execute as @s[scores={annonceID=690}] run function tch:auto/annonce/annonce/trafic/p/reprise_perturbe


# 1000 : Annonce incident (composée)
execute as @s[scores={annonceID=1000}] run function tch:auto/annonce/annonce/compose/annonce_incident
# 1010 : Fin service (composée)
execute as @s[scores={annonceID=1010}] run function tch:auto/annonce/annonce/compose/fin_service
# 1020 : Ligne fermée (composée)
execute as @s[scores={annonceID=1020}] run function tch:auto/annonce/annonce/compose/ligne_fermee

# 1100 : Appel usager (composée)
execute as @s[scores={annonceID=1100}] run function tch:auto/annonce/annonce/compose/appel_usager


# On incrémente l'index du son de l'annonce si on a un ID d'annonce
scoreboard players add @s[scores={annonceID=1..}] annonceIndex 1
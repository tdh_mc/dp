# On définit notre ID d'annonce et notre index à 0, pour enregistrer le fait qu'on doit en générer un
# seulement si on n'est pas en train de diffuser une annonce trafic interrompu
function tch:auto/annonce/next_reset

# On définit le temps d'attente à 1s (un peu plus de 30s TDH)
scoreboard players set @s annonceTicks 20
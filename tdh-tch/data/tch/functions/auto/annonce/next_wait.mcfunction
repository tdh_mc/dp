# On génère un temps d'attente aléatoire : entre 15s et 45s de silence

# On récupère d'abord une variable aléatoire
execute store result score @s annonceTicks run data get entity @e[sort=random,limit=1] Pos[2] 47

# On module par 600 (30s) et on ajoute 300 (15s)
scoreboard players set @s temp 600
scoreboard players operation @s annonceTicks %= @s temp
scoreboard players add @s annonceTicks 300

# Si on avait l'annonce Fin de service ou Ligne fermée, on rajoute 1mn (1200t)
execute if score @s annonceID matches 1010..1020 run scoreboard players add @s annonceTicks 1200

# On réinitialise la variable temporaire
scoreboard players reset @s temp


# On définit notre ID d'annonce et notre index à 0, pour enregistrer le fait qu'on doit en générer un
# seulement si on n'est pas en train de diffuser une annonce trafic interrompu
execute unless score @s annonceID matches 1000 run function tch:auto/annonce/next_reset

# Si le trafic est interrompu, on vérifie si l'incident est terminé
execute if score @s annonceID matches 1000 at @e[tag=Direction,sort=nearest,limit=1] as @e[tag=tchPC] if score @s idLine <= @e[tag=Direction,sort=nearest,limit=1] idLine if score @s idLineMax >= @e[tag=Direction,sort=nearest,limit=1] idLine run tag @s add ourPC

execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 0 run function tch:auto/annonce/next_reset
execute if score @e[type=item_frame,tag=ourPC,limit=1] debutIncident > #temps currentTdhTick run function tch:auto/annonce/next_reset
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident < #temps currentTdhTick run function tch:auto/annonce/next_reset
# TODO : définir ici l'annonce ID "reprise progressive du trafic" au lieu de 0

execute if score @s annonceID matches 1000 run scoreboard players set @s annonceIndex -1
execute if score @s annonceID matches 1000 at @e[type=#tdh:random,sort=random,limit=1] if entity @e[type=#tdh:random,distance=..1,y_rotation=120..179] unless score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 4000..7999 run scoreboard players add @e[type=item_frame,tag=ourPC,limit=1] finIncident 1000

# On supprime le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
# On a un nombre aléatoire entre 0 et 99 dans #random temp

# Lignes fermées (M9, RERA)
execute if score #random temp matches ..9 run scoreboard players set @s annonceID 290
execute if score #random temp matches 10..19 run scoreboard players set @s annonceID 400
# Appel usager (composé)
execute if score #random temp matches 20..24 run scoreboard players set @s annonceID 1100

# Grosses probas de signal sonore/confort descente et pickpockets
execute if score #random temp matches 25..44 run scoreboard players set @s annonceID 10
execute if score #random temp matches 45..64 run scoreboard players set @s annonceID 15
execute if score #random temp matches 65..79 run scoreboard players set @s annonceID 50

# Interdit de fumer !
execute if score #random temp matches 80..89 run scoreboard players set @s annonceID 20
# Oublie_sacs_bagages
execute if score #random temp matches 90..99 run scoreboard players set @s annonceID 35
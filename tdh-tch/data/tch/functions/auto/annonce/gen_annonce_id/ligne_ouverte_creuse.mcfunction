# On a un nombre aléatoire entre 0 et 99 dans #random temp

# Lignes fermées (M9, RERA)
execute if score #random temp matches ..9 run scoreboard players set @s annonceID 290
execute if score #random temp matches 10..19 run scoreboard players set @s annonceID 400

# S'il est moins de 16h, achat retour ; sinon, pickpockets
execute if score #random temp matches 20..40 if score #temps currentTdhTick matches 8000..32000 run scoreboard players set @s annonceID 45
execute if score #random temp matches 20..40 unless score #temps currentTdhTick matches 8000..32000 run scoreboard players set @s annonceID 50

# S'il est moins de 16h, sauvette ; sinon, sacs_bagages
execute if score #random temp matches 41..60 if score #temps currentTdhTick matches 8666..36666 run scoreboard players set @s annonceID 40
execute if score #random temp matches 41..60 unless score #temps currentTdhTick matches 8666..36666 run scoreboard players set @s annonceID 30

# Interdit de fumer !
execute if score #random temp matches 60..83 run scoreboard players set @s annonceID 20
# Oublie_sacs_bagages
execute if score #random temp matches 84..92 run scoreboard players set @s annonceID 35

# Petite proba de signal_sonore ou confort_descente quand même
execute if score #random temp matches 93..95 run scoreboard players set @s annonceID 10
execute if score #random temp matches 96.. run scoreboard players set @s annonceID 15
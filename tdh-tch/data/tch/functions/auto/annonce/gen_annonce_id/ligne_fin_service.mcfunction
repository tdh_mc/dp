# On génère une annonce aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

scoreboard players set @s annonceID 0

# S'il est moins de minuit, on a 20% de chances de jouer l'annonce composée
execute if score #random temp matches ..19 if score #temps currentTdhTick matches 32000..47999 run scoreboard players set @s annonceID 1010
# S'il est plus de minuit ou dans le cas où on aurait pas passé la condition aléatoire précédente, on joue fin service normal
execute if score @s annonceID matches 0 run scoreboard players set @s annonceID 110
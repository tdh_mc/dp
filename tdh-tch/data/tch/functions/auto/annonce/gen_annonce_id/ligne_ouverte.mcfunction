# Génère une annonce aléatoire pouvant passer à n'importe quel moment sur un quai
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On récupère une référence à notre PC
function tch:tag/pc

# On exécute une fonction différente selon si on est ou non en heure de pointe
scoreboard players operation #temp debutPointe1 = @e[type=item_frame,tag=ourPC] debutPointe1
scoreboard players operation #temp finPointe1 = @e[type=item_frame,tag=ourPC] finPointe1
scoreboard players operation #temp debutPointe2 = @e[type=item_frame,tag=ourPC] debutPointe2
scoreboard players operation #temp finPointe2 = @e[type=item_frame,tag=ourPC] finPointe2
execute if score #temps currentTdhTick >= #temp debutPointe1 if score #temps currentTdhTick <= #temp finPointe1 run tag @s add Heure2Pointe
execute if score #temps currentTdhTick >= #temp debutPointe2 if score #temps currentTdhTick <= #temp finPointe2 run tag @s add Heure2Pointe

execute as @s[tag=Heure2Pointe] run function tch:auto/annonce/gen_annonce_id/ligne_ouverte_pointe
execute as @s[tag=!Heure2Pointe] run function tch:auto/annonce/gen_annonce_id/ligne_ouverte_creuse
tag @s remove Heure2Pointe


# On nettoie la référence à notre PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
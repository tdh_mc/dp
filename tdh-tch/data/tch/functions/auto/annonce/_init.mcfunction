scoreboard objectives add annonceTicks dummy "Temps avant diffusion de la prochaine annonce"
scoreboard objectives add annonceID dummy "TCH - Identifiant annonce"
scoreboard objectives add annonceIndex dummy "TCH - Index dans l'annonce actuelle"
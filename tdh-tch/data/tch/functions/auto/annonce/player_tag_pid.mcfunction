# Cette fonction doit être exécutée par un spawnmetro,
# à la position d'un joueur dont on veut tester s'il entend les annonces
# au volume standard (= s'il est proche de la bonne ligne), ou à volume
# réduit (= si ce n'est pas le cas)
execute at @e[tag=Direction,sort=nearest,limit=1,distance=..25] if score @e[tag=Direction,sort=nearest,limit=1] idLine = @s idLine run tag @e[tag=Direction,sort=nearest,limit=1] add Matches
execute positioned ~-25 ~-4 ~-25 unless entity @e[tag=Matches,dx=50,dy=8,dz=50] run tag @e[tag=Matches] remove Matches

execute if entity @e[tag=Matches,distance=..25] run tag @a[distance=..1] add PlayerTagSuccess

tag @e[tag=Matches,distance=..50] remove Matches
# On tag notre PC
execute at @s at @e[tag=Direction,sort=nearest,limit=1,distance=..10] as @e[tag=tchPC] if score @s idLine <= @e[tag=Direction,sort=nearest,limit=1] idLine if score @s idLineMax >= @e[tag=Direction,sort=nearest,limit=1] idLine run tag @s add ourPC

# Si la ligne est fermée, on génère les annonces ligne fermée
execute if entity @e[type=item_frame,tag=ourPC,tag=HorsService] run function tch:auto/annonce/gen_annonce_id/ligne_fermee
# Si la ligne est ouverte mais hors service, on génère les annonces ligne fermée
execute if entity @e[type=item_frame,tag=ourPC,tag=EnService,tag=FinDeService] run function tch:auto/annonce/gen_annonce_id/ligne_fin_service
# Si la ligne est ouverte, on génère les annonces classiques
execute if entity @e[type=item_frame,tag=ourPC,tag=EnService,tag=!FinDeService] run function tch:auto/annonce/gen_annonce_id/ligne_ouverte

# On supprime le tag temporaire
tag @s remove ServiceFini
tag @e[type=item_frame,tag=ourPC] remove ourPC
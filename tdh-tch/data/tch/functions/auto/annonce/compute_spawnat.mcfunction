# Doit etre exécutée par l'item frame SpawnMetro diffusant l'annonce,
# et calcule les différents sons devant être diffusés à la suite pour recréer le temps d'attente en minutes

# On initialise d'abord les buffers
function tch:auto/annonce/compute_date/reset_buffers

# On récupère le temps d'attente en minutes dans temp6
scoreboard players operation @s temp6 = @s spawnAt
scoreboard players operation @s temp6 -= #temps currentTdhTick
execute if score @s temp6 matches ..-1 run scoreboard players operation @s temp6 += #temps fullDayTicks
# On peut se convaincre assez facilement que min = (t * (min/h) * (h/j)) / (t/j)
scoreboard players operation @s temp6 *= #temps hoursPerDay
scoreboard players operation @s temp6 *= #temps minutesPerHour
scoreboard players operation @s temp6 /= #temps fullDayTicks

# On stocke dans annonceBuf1>9 les différents sons
execute if score @s temp6 matches 100.. run function tch:auto/annonce/compute_date/centaines
execute if score @s temp6 matches 32.. run function tch:auto/annonce/compute_date/dizaines
scoreboard players operation @s temp = @s temp6
function tch:auto/annonce/compute_date/add_temp
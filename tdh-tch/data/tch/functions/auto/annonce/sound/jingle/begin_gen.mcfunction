execute store result score @s temp8 run data get entity @e[type=#tdh:random,sort=random,limit=1] Rotation[0]
scoreboard players set @s temp7 3
scoreboard players operation @s temp8 %= @s temp7

execute if score @s temp8 matches 0 run function tch:auto/annonce/sound/jingle/begin_gen1
execute if score @s temp8 matches 1 run function tch:auto/annonce/sound/jingle/begin_gen2
execute if score @s temp8 matches 2 run function tch:auto/annonce/sound/jingle/begin_commercial1

scoreboard players reset @s temp7
scoreboard players reset @s temp8
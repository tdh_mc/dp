# Doit etre exécutée par l'armor stand NumLigne diffusant l'annonce

# Selon le jour, on joue le son correspondant
execute if score @s dateMois matches 1 run function tch:auto/annonce/sound/compose/mois/janvier_u
execute if score @s dateMois matches 2 run function tch:auto/annonce/sound/compose/mois/fevrier_u
execute if score @s dateMois matches 3 run function tch:auto/annonce/sound/compose/mois/mars_u
execute if score @s dateMois matches 4 run function tch:auto/annonce/sound/compose/mois/avril_u
execute if score @s dateMois matches 5 run function tch:auto/annonce/sound/compose/mois/mai_u
execute if score @s dateMois matches 6 run function tch:auto/annonce/sound/compose/mois/juin_u
execute if score @s dateMois matches 7 run function tch:auto/annonce/sound/compose/mois/juillet_u
execute if score @s dateMois matches 8 run function tch:auto/annonce/sound/compose/mois/aout_u
execute if score @s dateMois matches 9 run function tch:auto/annonce/sound/compose/mois/septembre_u
execute if score @s dateMois matches 10 run function tch:auto/annonce/sound/compose/mois/octobre_u
execute if score @s dateMois matches 11 run function tch:auto/annonce/sound/compose/mois/novembre_u
execute if score @s dateMois matches 12 run function tch:auto/annonce/sound/compose/mois/decembre_u
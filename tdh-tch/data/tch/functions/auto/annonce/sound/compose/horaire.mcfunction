# Doit etre exécutée par l'armor stand NumLigne diffusant l'annonce

# On tag notre PC
execute at @s as @e[tag=Direction,sort=nearest,limit=1] at @e[tag=tchPC] if score @e[tag=tchPC,sort=nearest,limit=1] idLine <= @s idLine if score @e[tag=tchPC,sort=nearest,limit=1] idLineMax >= @s idLine run tag @e[tag=tchPC,sort=nearest,limit=1] add ourPC

# Selon l'horaire de reprise, on joue le son correspondant
# 3h-6h : "Vers 6h"
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 8000..11999 run function tch:auto/annonce/sound/compose/horaire/6h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 12000..12499 run function tch:auto/annonce/sound/compose/horaire/6h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 12500..12999 run function tch:auto/annonce/sound/compose/horaire/6h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 13000..13499 run function tch:auto/annonce/sound/compose/horaire/6h45
# 6h45-7h00 : "Vers 7h"
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 13500..13999 run function tch:auto/annonce/sound/compose/horaire/7h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 14000..14499 run function tch:auto/annonce/sound/compose/horaire/7h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 14500..14999 run function tch:auto/annonce/sound/compose/horaire/7h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 15000..15499 run function tch:auto/annonce/sound/compose/horaire/7h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 15500..15999 run function tch:auto/annonce/sound/compose/horaire/8h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 16000..16499 run function tch:auto/annonce/sound/compose/horaire/8h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 16500..16999 run function tch:auto/annonce/sound/compose/horaire/8h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 17000..17499 run function tch:auto/annonce/sound/compose/horaire/8h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 17500..17999 run function tch:auto/annonce/sound/compose/horaire/9h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 18000..18499 run function tch:auto/annonce/sound/compose/horaire/9h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 18500..18999 run function tch:auto/annonce/sound/compose/horaire/9h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 19000..19499 run function tch:auto/annonce/sound/compose/horaire/9h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 19500..19999 run function tch:auto/annonce/sound/compose/horaire/10h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 20000..20499 run function tch:auto/annonce/sound/compose/horaire/10h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 20500..20999 run function tch:auto/annonce/sound/compose/horaire/10h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 21000..21499 run function tch:auto/annonce/sound/compose/horaire/10h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 21500..21999 run function tch:auto/annonce/sound/compose/horaire/11h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 22000..22499 run function tch:auto/annonce/sound/compose/horaire/11h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 22500..22999 run function tch:auto/annonce/sound/compose/horaire/11h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 23000..23499 run function tch:auto/annonce/sound/compose/horaire/11h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 23500..23999 run function tch:auto/annonce/sound/compose/horaire/12h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 24000..24499 run function tch:auto/annonce/sound/compose/horaire/12h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 24500..24999 run function tch:auto/annonce/sound/compose/horaire/12h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 25000..25499 run function tch:auto/annonce/sound/compose/horaire/12h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 25500..25999 run function tch:auto/annonce/sound/compose/horaire/13h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 26000..26499 run function tch:auto/annonce/sound/compose/horaire/13h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 26500..26999 run function tch:auto/annonce/sound/compose/horaire/13h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 27000..27499 run function tch:auto/annonce/sound/compose/horaire/13h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 27500..27999 run function tch:auto/annonce/sound/compose/horaire/14h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 28000..28499 run function tch:auto/annonce/sound/compose/horaire/14h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 28500..28999 run function tch:auto/annonce/sound/compose/horaire/14h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 29000..29499 run function tch:auto/annonce/sound/compose/horaire/14h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 29500..29999 run function tch:auto/annonce/sound/compose/horaire/15h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 30000..30499 run function tch:auto/annonce/sound/compose/horaire/15h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 30500..30999 run function tch:auto/annonce/sound/compose/horaire/15h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 31000..31499 run function tch:auto/annonce/sound/compose/horaire/15h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 31500..31999 run function tch:auto/annonce/sound/compose/horaire/16h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 32000..32499 run function tch:auto/annonce/sound/compose/horaire/16h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 32500..32999 run function tch:auto/annonce/sound/compose/horaire/16h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 33000..33499 run function tch:auto/annonce/sound/compose/horaire/16h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 33500..33999 run function tch:auto/annonce/sound/compose/horaire/17h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 34000..34499 run function tch:auto/annonce/sound/compose/horaire/17h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 34500..34999 run function tch:auto/annonce/sound/compose/horaire/17h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 35000..35499 run function tch:auto/annonce/sound/compose/horaire/17h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 35500..35999 run function tch:auto/annonce/sound/compose/horaire/18h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 36000..36499 run function tch:auto/annonce/sound/compose/horaire/18h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 36500..36999 run function tch:auto/annonce/sound/compose/horaire/18h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 37000..37499 run function tch:auto/annonce/sound/compose/horaire/18h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 37500..37999 run function tch:auto/annonce/sound/compose/horaire/19h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 38000..38499 run function tch:auto/annonce/sound/compose/horaire/19h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 38500..38999 run function tch:auto/annonce/sound/compose/horaire/19h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 39000..39499 run function tch:auto/annonce/sound/compose/horaire/19h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 39500..39999 run function tch:auto/annonce/sound/compose/horaire/20h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 40000..40499 run function tch:auto/annonce/sound/compose/horaire/20h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 40500..40999 run function tch:auto/annonce/sound/compose/horaire/20h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 41000..41499 run function tch:auto/annonce/sound/compose/horaire/20h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 41500..41999 run function tch:auto/annonce/sound/compose/horaire/21h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 42000..42499 run function tch:auto/annonce/sound/compose/horaire/21h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 42500..42999 run function tch:auto/annonce/sound/compose/horaire/21h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 43000..43499 run function tch:auto/annonce/sound/compose/horaire/21h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 43500..43999 run function tch:auto/annonce/sound/compose/horaire/22h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 44000..44499 run function tch:auto/annonce/sound/compose/horaire/22h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 44500..44999 run function tch:auto/annonce/sound/compose/horaire/22h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 45000..45499 run function tch:auto/annonce/sound/compose/horaire/22h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 45500..45999 run function tch:auto/annonce/sound/compose/horaire/23h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 46000..46499 run function tch:auto/annonce/sound/compose/horaire/23h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 46500..46999 run function tch:auto/annonce/sound/compose/horaire/23h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 47000..47499 run function tch:auto/annonce/sound/compose/horaire/23h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 47500..47999 run function tch:auto/annonce/sound/compose/horaire/24h
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 0..499 run function tch:auto/annonce/sound/compose/horaire/24h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 500..999 run function tch:auto/annonce/sound/compose/horaire/24h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 1000..1499 run function tch:auto/annonce/sound/compose/horaire/24h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 48000..48499 run function tch:auto/annonce/sound/compose/horaire/24h15
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 48500..48999 run function tch:auto/annonce/sound/compose/horaire/24h30
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 49000..49499 run function tch:auto/annonce/sound/compose/horaire/24h45
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 1500..7999 run function tch:auto/annonce/sound/compose/horaire/demain_matin
execute if score @e[type=item_frame,tag=ourPC,limit=1] finIncident matches 49500.. run function tch:auto/annonce/sound/compose/horaire/demain_matin

# On supprime les trucs temporaires
tag @e[type=item_frame,tag=ourPC] remove ourPC
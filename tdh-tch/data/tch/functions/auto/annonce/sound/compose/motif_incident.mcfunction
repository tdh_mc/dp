# Doit etre exécutée par l'armor stand NumLigne diffusant l'annonce

# On tag notre PC
execute at @s as @e[tag=Direction,sort=nearest,limit=1] at @e[tag=tchPC] if score @e[tag=tchPC,sort=nearest,limit=1] idLine <= @s idLine if score @e[tag=tchPC,sort=nearest,limit=1] idLineMax >= @s idLine run tag @e[tag=tchPC,sort=nearest,limit=1] add ourPC

# Selon le code incident de notre PC, on joue la phrase correspondante
# 10-19 : Incident affectant la voie
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 10..19 run function tch:auto/annonce/sound/compose/motif_incident/incident_affectant_la_voie
# 20-29 : Panne de signalisation
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 20..29 run function tch:auto/annonce/sound/compose/motif_incident/panne_signalisation
# 30-32 : 50% Panne électrique, 50% Incident technique
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 30..32 run function tch:auto/annonce/sound/compose/motif_incident_panne_electrique
# 33-39 : Incident technique
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 33..39 run function tch:auto/annonce/sound/compose/motif_incident/incident_technique
# 40-49 : Incident voyageur
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 40..49 run function tch:auto/annonce/sound/compose/motif_incident/incident_voyageur
# 50-59 : Malaise voyageur
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 50..59 run function tch:auto/annonce/sound/compose/motif_incident/malaise_voyageur
# 60-69 : Panne mécanique
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 60..69 run function tch:auto/annonce/sound/compose/motif_incident/panne_mecanique
# 70-79 : Mesure de sécurité
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 70..79 run function tch:auto/annonce/sound/compose/motif_incident/mesure_de_securite
# 80 : Diverses options concernant toutes des collègues
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 80 run function tch:auto/annonce/sound/compose/motif_incident_collegue
#♂81 : Abandon de poste
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 81 run function tch:auto/annonce/sound/compose/motif_incident/abandon_de_poste
# 82-89 : Incident d'exploitation
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 82..89 run function tch:auto/annonce/sound/compose/motif_incident/difficultes_exploitation
# 90-92 : Acte de vandalisme
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 90..92 run function tch:auto/annonce/sound/compose/motif_incident/acte_de_vandalisme
# 93-99 : Acte de malveillance
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 93..99 run function tch:auto/annonce/sound/compose/motif_incident/acte_de_malveillance
# 100-109 : Divers incidents
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 100..109 run function tch:auto/annonce/sound/compose/motif_incident/divers_incidents
# 110-119 : Conditions météo
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 110..119 run function tch:auto/annonce/sound/compose/motif_incident/conditions_meteo
# 120-129 : Mouvement social
execute if score @e[type=item_frame,tag=ourPC,limit=1] incident matches 120..129 run function tch:auto/annonce/sound/compose/motif_incident/mouvement_social

# On supprime les trucs temporaires
tag @e[type=item_frame,tag=ourPC] remove ourPC
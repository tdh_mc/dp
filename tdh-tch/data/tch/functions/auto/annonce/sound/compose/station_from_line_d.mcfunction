execute unless score @s idLine matches 1.. run function tch:auto/get_id_line

execute as @s[scores={idLine=11}] run function tch:auto/annonce/sound/compose/station/grenat_ville_d
execute as @s[scores={idLine=12}] run function tch:auto/annonce/sound/compose/station/dorlinor_d
execute as @s[scores={idLine=21}] run function tch:auto/annonce/sound/compose/station/evenis_eclesta_d
execute as @s[scores={idLine=22}] run function tch:auto/annonce/sound/compose/station/desert_onyx_d
execute as @s[scores={idLine=31}] run function tch:auto/annonce/sound/compose/station/villonne_d
execute as @s[scores={idLine=32}] run function tch:auto/annonce/sound/compose/station/chizan_d
execute as @s[scores={idLine=41}] run function tch:auto/annonce/sound/compose/station/tolbrok_d
execute as @s[scores={idLine=42}] run function tch:auto/annonce/sound/compose/station/pieuze_d
execute as @s[scores={idLine=51}] run function tch:auto/annonce/sound/compose/station/grenat_ville_d
execute as @s[scores={idLine=52}] run function tch:auto/annonce/sound/compose/station/chizan_d
execute as @s[scores={idLine=55}] run function tch:auto/annonce/sound/compose/station/villonne_d
execute as @s[scores={idLine=56}] run function tch:auto/annonce/sound/compose/station/dorlinor_d
execute as @s[scores={idLine=61}] run function tch:auto/annonce/sound/compose/station/fultez_d
execute as @s[scores={idLine=62}] run function tch:auto/annonce/sound/compose/station/mides_d
execute as @s[scores={idLine=71}] run function tch:auto/annonce/sound/compose/station/ile_du_singe_d
execute as @s[scores={idLine=72}] run function tch:auto/annonce/sound/compose/station/evenis_tamlyn_d
execute as @s[scores={idLine=73}] run function tch:auto/annonce/sound/compose/station/litorea_d
execute as @s[scores={idLine=74}] run function tch:auto/annonce/sound/compose/station/quatre_chemins_d
execute as @s[scores={idLine=81}] run function tch:auto/annonce/sound/compose/station/procyon_d
execute as @s[scores={idLine=82}] run function tch:auto/annonce/sound/compose/station/georlie_d
execute as @s[scores={idLine=83}] run function tch:auto/annonce/sound/compose/station/frambourg_ternelieu_d
execute as @s[scores={idLine=84}] run function tch:auto/annonce/sound/compose/station/le_relais_d

execute as @s[scores={idLine=2101}] run function tch:auto/annonce/sound/compose/station/cyseal_d
execute as @s[scores={idLine=2102}] run function tch:auto/annonce/sound/compose/station/procyon_d
execute as @s[scores={idLine=2150..2199}] run function tch:auto/annonce/sound/compose/station/le_hameau_rive_droite_d

execute as @s[scores={idLine=4011}] run function tch:auto/annonce/sound/compose/station/evrocq_le_haut_d
execute as @s[scores={idLine=4012}] run function tch:auto/annonce/sound/compose/station/evrocq_le_bas_d
execute as @s[scores={idLine=4021}] run function tch:auto/annonce/sound/compose/station/calmeflot_d
execute as @s[scores={idLine=4022}] run function tch:auto/annonce/sound/compose/station/la_dodene_d
execute store result score @s temp7 run data get entity @e[sort=random,limit=1] Pos[0]
scoreboard players set @s temp8 3
scoreboard players operation @s temp7 %= @s temp8

tag @s add PrenomNeeded

execute if score @s temp7 matches 0 run function tch:auto/annonce/sound/compose/motif_incident/absence_collegue
execute if score @s temp7 matches 1 run function tch:auto/annonce/sound/compose/motif_incident/panne_reveil_collegue
execute if score @s temp7 matches 2 run function tch:auto/annonce/sound/compose/motif_incident/retard_collegue

scoreboard players reset @s temp7
scoreboard players reset @s temp8
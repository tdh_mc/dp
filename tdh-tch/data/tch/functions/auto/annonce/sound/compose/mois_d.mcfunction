# Doit etre exécutée par l'armor stand NumLigne diffusant l'annonce

# Selon le jour, on joue le son correspondant
execute if score @s dateMois matches 1 run function tch:auto/annonce/sound/compose/mois/janvier_d
execute if score @s dateMois matches 2 run function tch:auto/annonce/sound/compose/mois/fevrier_d
execute if score @s dateMois matches 3 run function tch:auto/annonce/sound/compose/mois/mars_d
execute if score @s dateMois matches 4 run function tch:auto/annonce/sound/compose/mois/avril_d
execute if score @s dateMois matches 5 run function tch:auto/annonce/sound/compose/mois/mai_d
execute if score @s dateMois matches 6 run function tch:auto/annonce/sound/compose/mois/juin_d
execute if score @s dateMois matches 7 run function tch:auto/annonce/sound/compose/mois/juillet_d
execute if score @s dateMois matches 8 run function tch:auto/annonce/sound/compose/mois/aout_d
execute if score @s dateMois matches 9 run function tch:auto/annonce/sound/compose/mois/septembre_d
execute if score @s dateMois matches 10 run function tch:auto/annonce/sound/compose/mois/octobre_d
execute if score @s dateMois matches 11 run function tch:auto/annonce/sound/compose/mois/novembre_d
execute if score @s dateMois matches 12 run function tch:auto/annonce/sound/compose/mois/decembre_d
# Doit etre exécutée par l'armor stand NumLigne diffusant l'annonce

# On tag notre PC
execute at @s as @e[tag=Direction,sort=nearest,limit=1] at @e[tag=tchPC] if score @e[tag=tchPC,sort=nearest,limit=1] idLine <= @s idLine if score @e[tag=tchPC,sort=nearest,limit=1] idLineMax >= @s idLine run tag @e[tag=tchPC,sort=nearest,limit=1] add ourPC

# Selon l'ID de ligne de notre PC, on joue la phrase correspondante
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 11 run function tch:auto/annonce/sound/compose/ligne/m1_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 21 run function tch:auto/annonce/sound/compose/ligne/m2_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 31 run function tch:auto/annonce/sound/compose/ligne/m3_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 41 run function tch:auto/annonce/sound/compose/ligne/m4_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 51 run function tch:auto/annonce/sound/compose/ligne/m5a_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 55 run function tch:auto/annonce/sound/compose/ligne/m5b_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 61 run function tch:auto/annonce/sound/compose/ligne/m6_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 71 run function tch:auto/annonce/sound/compose/ligne/m7_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 73 run function tch:auto/annonce/sound/compose/ligne/m7b_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 81 run function tch:auto/annonce/sound/compose/ligne/m8_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 83 run function tch:auto/annonce/sound/compose/ligne/m8b_d

execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 1101 run function tch:auto/annonce/sound/compose/ligne/rer_a_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 1201 run function tch:auto/annonce/sound/compose/ligne/rer_b_d

execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 2011 run function tch:auto/annonce/sound/compose/ligne/cable_a_d
execute if score @e[type=item_frame,tag=ourPC,limit=1] idLine matches 2021 run function tch:auto/annonce/sound/compose/ligne/cable_b_d

# On supprime les trucs temporaires
tag @e[type=item_frame,tag=ourPC] remove ourPC
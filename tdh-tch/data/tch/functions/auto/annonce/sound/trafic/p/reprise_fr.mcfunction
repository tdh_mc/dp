scoreboard players set #annonce temp 2
scoreboard players operation #annonce temp2 = #temps currentTdhTick
scoreboard players operation #annonce temp2 %= #annonce temp

execute if score #annonce temp2 matches ..0 run function tch:auto/annonce/sound/trafic/p/reprise1
execute if score #annonce temp2 matches 1.. run function tch:auto/annonce/sound/trafic/p/reprise2

scoreboard players reset #annonce temp
scoreboard players reset #annonce temp2
tag @e[tag=Direction,sort=nearest,limit=1] add Direction1
tag @e[tag=Direction,tag=!Direction1,sort=nearest,limit=1] add Direction2

scoreboard players operation @s idLine = @e[tag=Direction1,sort=nearest,limit=1] idLine
scoreboard players operation @s idLine < @e[tag=Direction2,sort=nearest,limit=1] idLine

tag @e[tag=Direction1] remove Direction1
tag @e[tag=Direction2] remove Direction2
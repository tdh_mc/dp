# Dans l'ordre :
# - Begin inopiné mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/begin_inopine
# - Votre attention s'il vous plaît
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/compose/generic/begin
# - Pré prénom ("Un usager prénommé... ")
execute as @s[scores={annonceIndex=2}] run function tch:auto/annonce/sound/compose/generic/usager_guichet_pre
# - Prénom aléatoire
execute as @s[scores={annonceIndex=3}] run function tch:auto/annonce/sound/compose/prenom/random
# - Post prénom (".. est attendu à l'accueil.")
execute as @s[scores={annonceIndex=4}] run function tch:auto/annonce/sound/compose/generic/usager_guichet_post
# - Drop mic
execute as @s[scores={annonceIndex=5}] run function tch:auto/annonce/sound/compose/generic/dropmic
# - End inopiné
execute as @s[scores={annonceIndex=6}] run function tch:auto/annonce/sound/end_inopine

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=7..}] run function tch:auto/annonce/next_wait
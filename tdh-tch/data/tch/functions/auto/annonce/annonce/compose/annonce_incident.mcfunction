# Dans l'ordre :
# - Begin inopiné mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/begin_inopine
# - Votre attention s'il vous plaît
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/compose/generic/begin
# - Exposition du motif ("En raison d'une panne mécanique, ")
execute as @s[scores={annonceIndex=2}] run function tch:auto/annonce/sound/compose/motif_incident

# Si on n'a pas besoin d'afficher un prénom à cet endroit, on passe au terme suivant
execute as @s[scores={annonceIndex=3},tag=!PrenomNeeded] run scoreboard players add @s annonceIndex 1
# Sinon, on prend un prénom aléatoire
execute as @s[scores={annonceIndex=3}] run function tch:auto/annonce/sound/compose/prenom/random
execute as @s[scores={annonceIndex=3}] run tag @s remove PrenomNeeded

# - On prend une variable aléatoire : Si on a temp<30, on utilise une phrase en x_pre, sinon x_post
execute as @s[scores={annonceIndex=4}] run function tch:auto/annonce/proba

# - Si on a une phrase en POST (temp>=30), on fait LIGNE => EFFET_INCIDENT
execute as @s[scores={annonceIndex=4,temp=30..}] run function tch:auto/annonce/sound/compose/ligne_u
execute as @s[scores={annonceIndex=5,temp=30..}] run function tch:auto/annonce/sound/compose/effet_incident/x_post
# - Si on a une phrase en PRE (temp<30), on fait EFFET_INCIDENT => LIGNE
execute as @s[scores={annonceIndex=4,temp=..29}] run function tch:auto/annonce/sound/compose/effet_incident/x_pre
execute as @s[scores={annonceIndex=5,temp=..29}] run function tch:auto/annonce/sound/compose/ligne_d

# - On prend une variable aléatoire : Si on a temp<30, on utilise une reprise_trafic_prog, sinon reprise_trafic
execute as @s[scores={annonceIndex=6}] run function tch:auto/annonce/proba

# - Si on a une phrase en REPRISE_TRAFIC
execute as @s[scores={annonceIndex=6,temp=30..}] run function tch:auto/annonce/sound/compose/generic/reprise_trafic
# - Si on a une phrase en REPRISE_TRAFIC_PROG
execute as @s[scores={annonceIndex=6,temp=..29}] run function tch:auto/annonce/sound/compose/generic/reprise_trafic_prog

# - On donne ensuite une estimation de l'horaire
execute as @s[scores={annonceIndex=7}] run function tch:auto/annonce/sound/compose/horaire

# [40%] Informations supplémentaires
execute as @s[scores={annonceIndex=8}] run function tch:auto/annonce/proba
execute as @s[scores={annonceIndex=8,temp=..59}] run scoreboard players add @s annonceIndex 1
execute as @s[scores={annonceIndex=8}] run function tch:auto/annonce/sound/compose/generic/corresp_guichet

# - Merci de votre compréhension
execute as @s[scores={annonceIndex=9}] run function tch:auto/annonce/sound/compose/generic/end
# - Drop mic
execute as @s[scores={annonceIndex=10}] run function tch:auto/annonce/sound/compose/generic/dropmic
# - End inopiné
execute as @s[scores={annonceIndex=11}] run function tch:auto/annonce/sound/end_inopine

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=12..}] run function tch:auto/annonce/next_wait
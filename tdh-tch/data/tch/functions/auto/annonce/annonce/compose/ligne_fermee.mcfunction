# On enregistre la date d'ouverture prévue de la ligne
execute at @e[tag=Direction,sort=nearest,limit=1] as @e[tag=tchPC] if score @s idLine <= @e[tag=Direction,sort=nearest,limit=1] idLine if score @s idLineMax >= @e[tag=Direction,sort=nearest,limit=1] idLine run tag @s add ourPC
execute store result score @s dateJour run data get entity @e[type=item_frame,tag=ourPC,limit=1] Item.tag.tch.date_ouverture.jour
execute store result score @s dateMois run data get entity @e[type=item_frame,tag=ourPC,limit=1] Item.tag.tch.date_ouverture.mois
execute store result score @s dateAnnee run data get entity @e[type=item_frame,tag=ourPC,limit=1] Item.tag.tch.date_ouverture.annee
tag @e[type=item_frame,tag=ourPC] remove ourPC

# Dans l'ordre :
# - Begin travaux mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/begin_travaux
# - Motif (en raison de travaux...)
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/compose/motif_incident/travaux
# - Identifiant ligne
execute as @s[scores={annonceIndex=2}] run function tch:auto/annonce/sound/compose/ligne_u
# - Post ligne (".. est fermée.")
execute as @s[scores={annonceIndex=3}] run function tch:auto/annonce/sound/compose/effet_incident/xx_post
# - Ouverture prévue le
execute as @s[scores={annonceIndex=4}] run function tch:auto/annonce/compute_date
execute as @s[scores={annonceIndex=4}] run function tch:auto/annonce/sound/compose/generic/ouverture_prevue
execute as @s[scores={annonceIndex=4}] run scoreboard players remove @s annonceTicks 1
# - DateJour
execute as @s[scores={annonceIndex=5}] run scoreboard players operation @s temp = @s dateJour
execute as @s[scores={annonceIndex=5}] run function tch:auto/annonce/sound/compose/nombre_temp_u
# - DateMois
execute as @s[scores={annonceIndex=6}] run function tch:auto/annonce/sound/compose/mois_u
# - DateAnnee (les 9 termes possibles)
execute as @s[scores={annonceIndex=7}] run scoreboard players operation @s temp = @s annonceBuf1
execute as @s[scores={annonceIndex=7,temp=1..,annonceBuf2=..0}] run function tch:auto/annonce/sound/compose/nombre_temp_d
execute as @s[scores={annonceIndex=7,temp=1..,annonceBuf2=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_u

execute as @s[scores={annonceIndex=8,temp=1..}] run scoreboard players operation @s temp = @s annonceBuf2
execute as @s[scores={annonceIndex=8,temp=1..,annonceBuf3=..0}] run function tch:auto/annonce/sound/compose/nombre_temp_d
execute as @s[scores={annonceIndex=8,temp=1..,annonceBuf3=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_u

execute as @s[scores={annonceIndex=9,temp=1..}] run scoreboard players operation @s temp = @s annonceBuf3
execute as @s[scores={annonceIndex=9,temp=1..,annonceBuf4=..0}] run function tch:auto/annonce/sound/compose/nombre_temp_d
execute as @s[scores={annonceIndex=9,temp=1..,annonceBuf4=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_u

execute as @s[scores={annonceIndex=10,temp=1..}] run scoreboard players operation @s temp = @s annonceBuf4
execute as @s[scores={annonceIndex=10,temp=1..,annonceBuf5=..0}] run function tch:auto/annonce/sound/compose/nombre_temp_d
execute as @s[scores={annonceIndex=10,temp=1..,annonceBuf5=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_u

execute as @s[scores={annonceIndex=11,temp=1..}] run scoreboard players operation @s temp = @s annonceBuf5
execute as @s[scores={annonceIndex=11,temp=1..,annonceBuf6=..0}] run function tch:auto/annonce/sound/compose/nombre_temp_d
execute as @s[scores={annonceIndex=11,temp=1..,annonceBuf6=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_u

execute as @s[scores={annonceIndex=12,temp=1..}] run scoreboard players operation @s temp = @s annonceBuf6
execute as @s[scores={annonceIndex=12,temp=1..,annonceBuf7=..0}] run function tch:auto/annonce/sound/compose/nombre_temp_d
execute as @s[scores={annonceIndex=12,temp=1..,annonceBuf7=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_u

execute as @s[scores={annonceIndex=13,temp=1..}] run scoreboard players operation @s temp = @s annonceBuf7
execute as @s[scores={annonceIndex=13,temp=1..,annonceBuf8=..0}] run function tch:auto/annonce/sound/compose/nombre_temp_d
execute as @s[scores={annonceIndex=13,temp=1..,annonceBuf8=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_u

execute as @s[scores={annonceIndex=14,temp=1..}] run scoreboard players operation @s temp = @s annonceBuf8
execute as @s[scores={annonceIndex=14,temp=1..,annonceBuf9=..0}] run function tch:auto/annonce/sound/compose/nombre_temp_d
execute as @s[scores={annonceIndex=14,temp=1..,annonceBuf9=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_u

execute as @s[scores={annonceIndex=15,temp=1..}] run scoreboard players operation @s temp = @s annonceBuf9
execute as @s[scores={annonceIndex=15,temp=1..}] run function tch:auto/annonce/sound/compose/nombre_temp_d

# - Si d'autres lignes roulent, on joue finservice_sortie_corresp ; sinon, on joue finservice_sortie
execute as @s[scores={annonceIndex=16}] at @s at @e[tag=NumLigne,distance=3..150] as @e[tag=Direction,sort=nearest,limit=2] at @e[tag=SpawnMetro,distance=..50] if score @e[tag=SpawnMetro,sort=nearest,limit=1] idLine = @s idLine if score @e[tag=SpawnMetro,sort=nearest,limit=1] dateJour = #temps dateJour run tag @s add StillWorking
execute as @s[scores={annonceIndex=16}] if entity @e[tag=StillWorking] run function tch:auto/annonce/sound/compose/generic/fin_service_sortie_corresp
execute as @s[scores={annonceIndex=16}] unless entity @e[tag=StillWorking] run function tch:auto/annonce/sound/compose/generic/fin_service_sortie
execute as @s[scores={annonceIndex=16}] run tag @e[tag=StillWorking] remove StillWorking
# - End travaux
execute as @s[scores={annonceIndex=17}] run function tch:auto/annonce/sound/jingle/end_travaux

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=18..}] run function tch:auto/annonce/next_wait
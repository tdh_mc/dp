# On tag les gens pouvant entendre ce PID
execute at @e[type=armor_stand,tag=Direction,sort=nearest,limit=1] at @a[distance=..60] run function tch:auto/annonce/player_tag_pid

# On tag nos zones d'émission
#execute as @e[type=item_frame,tag=PID,distance=..50] unless score @s idLine matches 1.. run tellraw @a [{"text":"[WARN] — PID sans ID ligne enregistré. Il ne pourra pas diffuser : ","color":"gold"},{"selector":"@s"}]
execute at @e[type=item_frame,tag=PID,tag=!Muet,distance=..150] if score @s idLine = @e[type=item_frame,tag=PID,sort=nearest,limit=1] idLine run tag @e[type=item_frame,tag=PID,sort=nearest,limit=1] add ourSoundSource
execute at @e[type=item_frame,tag=PID,tag=!Muet,distance=..150] if score @s idLine = @e[type=item_frame,tag=PID,sort=nearest,limit=1] idLine run tag @e[type=item_frame,tag=PID,sort=nearest,limit=1] add ourSoundSource
execute unless entity @e[type=item_frame,tag=ourSoundSource] run tag @s add ourSoundSource

# Dans l'ordre :
# - Direction
execute as @s[scores={annonceIndex=0}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/generic/direction
# - Station de direction
execute as @s[scores={annonceIndex=1}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/station_from_line_u
# - Prochain train dans
execute as @s[scores={annonceIndex=2}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/generic/prochain_train_dans
# - On calcule le temps d'attente,
# puis on le joue sonorement
execute as @s[scores={annonceIndex=3}] run function tch:auto/annonce/compute_spawnat
execute as @s[scores={annonceIndex=3}] run scoreboard players operation @s temp = @s annonceBuf1
execute as @s[scores={annonceIndex=3,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=4}] run scoreboard players operation @s temp = @s annonceBuf2
execute as @s[scores={annonceIndex=4,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=5}] run scoreboard players operation @s temp = @s annonceBuf3
execute as @s[scores={annonceIndex=5,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=6}] run scoreboard players operation @s temp = @s annonceBuf4
execute as @s[scores={annonceIndex=6,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=7}] run scoreboard players operation @s temp = @s annonceBuf5
execute as @s[scores={annonceIndex=7,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=8}] run scoreboard players operation @s temp = @s annonceBuf6
execute as @s[scores={annonceIndex=8,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=9}] run scoreboard players operation @s temp = @s annonceBuf7
execute as @s[scores={annonceIndex=9,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=10}] run scoreboard players operation @s temp = @s annonceBuf8
execute as @s[scores={annonceIndex=10,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=11}] run scoreboard players operation @s temp = @s annonceBuf9
execute as @s[scores={annonceIndex=11,temp=1..}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/nombre_temp_u
execute as @s[scores={annonceIndex=4..11,temp=0}] run scoreboard players set @s annonceIndex 12

# - Minutes
execute as @s[scores={annonceIndex=12}] at @e[type=item_frame,tag=ourSoundSource] run function tch:auto/annonce/sound/compose/generic/minutes

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=13..}] run function tch:auto/annonce/next_wait_pid

# On supprime les tags temporaires donnés aux joueurs pouvant entendre ce PID
tag @e[tag=PlayerTagSuccess] remove PlayerTagSuccess
tag @e[type=item_frame,tag=ourSoundSource] remove ourSoundSource
tag @s remove ourSoundSource
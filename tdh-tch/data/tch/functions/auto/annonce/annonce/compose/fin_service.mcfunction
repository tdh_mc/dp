# Dans l'ordre :
# - Begin fds mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/begin_fin_service
# - Effet (Le trafic est terminé sur...)
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/compose/effet_incident/finservice_pre
# - Identifiant ligne
execute as @s[scores={annonceIndex=2}] run function tch:auto/annonce/sound/compose/ligne_d
# - Si d'autres lignes roulent, on joue finservice_sortie_corresp ; sinon, on joue finservice_sortie
execute as @s[scores={annonceIndex=3}] at @s at @e[tag=NumLigne,distance=3..150] as @e[tag=Direction,sort=nearest,limit=2] at @e[tag=SpawnMetro,distance=..50] if score @e[tag=SpawnMetro,sort=nearest,limit=1] idLine = @s idLine if score @e[tag=SpawnMetro,sort=nearest,limit=1] dateJour = #temps dateJour run tag @s add StillWorking
execute as @s[scores={annonceIndex=3}] if entity @e[tag=StillWorking] run function tch:auto/annonce/sound/compose/generic/fin_service_sortie_corresp
execute as @s[scores={annonceIndex=3}] unless entity @e[tag=StillWorking] run function tch:auto/annonce/sound/compose/generic/fin_service_sortie
execute as @s[scores={annonceIndex=3}] run tag @e[tag=StillWorking] remove StillWorking

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=4..}] run function tch:auto/annonce/next_wait
# On génère un nombre aléatoire
scoreboard players set #random max 100
execute if score @s annonceIndex matches 2.. run function tdh:random

# Dans l'ordre :
# - Begin travaux mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/jingle/begin_travaux
# - Annonce en français
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/trafic/x/travaux/m2_fr
# - Annonce en anglais (60%)
execute as @s[scores={annonceIndex=2}] if score #random temp matches ..60 run function tch:auto/annonce/sound/trafic/x/travaux/m2_en
execute as @s[scores={annonceIndex=2}] if score #random temp matches 61.. run scoreboard players add @s annonceIndex 1
# - Annonce en espagnol (50%)
execute as @s[scores={annonceIndex=3}] if score #random temp matches ..50 run function tch:auto/annonce/sound/trafic/x/travaux/m2_es
execute as @s[scores={annonceIndex=3}] if score #random temp matches 51.. run scoreboard players add @s annonceIndex 1
# - End travaux mono
execute as @s[scores={annonceIndex=4}] run function tch:auto/annonce/sound/jingle/end_travaux

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=5..}] run function tch:auto/annonce/next_wait
# Dans l'ordre :
# - Begin travaux mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/jingle/begin_travaux
# - Annonce en français
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/trafic/x/incident_accident
# - End travaux mono
execute as @s[scores={annonceIndex=2}] run function tch:auto/annonce/sound/jingle/end_travaux

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=3..}] run function tch:auto/annonce/next_wait
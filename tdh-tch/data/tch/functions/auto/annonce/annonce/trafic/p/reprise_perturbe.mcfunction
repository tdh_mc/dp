# On génère un nombre aléatoire
scoreboard players set #random max 100

# Dans l'ordre :
# - Begin travaux mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/jingle/begin_travaux
# - Annonce en français
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/trafic/p/reprise_perturbe
# - Annonce en anglais (70%)
execute as @s[scores={annonceIndex=2}] run function tdh:random
execute as @s[scores={annonceIndex=2}] if score #random temp matches ..70 run function tch:auto/annonce/sound/trafic/p/reprise_en
execute as @s[scores={annonceIndex=2}] if score #random temp matches 71.. run scoreboard players add @s annonceIndex 1
# - Annonce en italien (50%)
execute as @s[scores={annonceIndex=3}] run function tdh:random
execute as @s[scores={annonceIndex=3}] if score #random temp matches ..30 run function tch:auto/annonce/sound/trafic/p/reprise_it
execute as @s[scores={annonceIndex=3}] if score #random temp matches 31.. run scoreboard players add @s annonceIndex 1
# - End travaux mono
execute as @s[scores={annonceIndex=4}] run function tch:auto/annonce/sound/jingle/end_travaux

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=5..}] run function tch:auto/annonce/next_wait
# On génère un nombre aléatoire
scoreboard players set #random max 100

# Dans l'ordre :
# - Begin générique mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/jingle/begin_gen
# - Annonce en français
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/evacuation_fr
# - Annonce en anglais (95%)
execute as @s[scores={annonceIndex=2}] run function tdh:random
execute as @s[scores={annonceIndex=2}] if score #random temp matches ..95 run function tch:auto/annonce/sound/evacuation_en
execute as @s[scores={annonceIndex=2}] if score #random temp matches 96.. run scoreboard players add @s annonceIndex 1
# - Annonce en allemand (50%)
execute as @s[scores={annonceIndex=3}] run function tdh:random
execute as @s[scores={annonceIndex=3}] if score #random temp matches ..50 run function tch:auto/annonce/sound/evacuation_de
execute as @s[scores={annonceIndex=3}] if score #random temp matches 51.. run scoreboard players add @s annonceIndex 1
# - Annonce en espagnol (50%)
execute as @s[scores={annonceIndex=4}] run function tdh:random
execute as @s[scores={annonceIndex=4}] if score #random temp matches ..50 run function tch:auto/annonce/sound/evacuation_es
execute as @s[scores={annonceIndex=4}] if score #random temp matches 51.. run scoreboard players add @s annonceIndex 1

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=5..}] run function tch:auto/annonce/next_wait
# On génère un nombre aléatoire
scoreboard players set #random max 100

# Dans l'ordre :
# - Begin générique mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/jingle/begin_fin_service
# - Annonce en français aléatoire
execute as @s[scores={annonceIndex=1}] run function tdh:random
execute as @s[scores={annonceIndex=1}] if score #random temp matches ..24 run function tch:auto/annonce/sound/trafic/x/fin_service1
execute as @s[scores={annonceIndex=1}] if score #random temp matches 25..49 run function tch:auto/annonce/sound/trafic/x/fin_service2
execute as @s[scores={annonceIndex=1}] if score #random temp matches 50..74 run function tch:auto/annonce/sound/trafic/x/fin_service3
execute as @s[scores={annonceIndex=1}] if score #random temp matches 75.. run function tch:auto/annonce/sound/trafic/x/fin_service4

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=2..}] run function tch:auto/annonce/next_wait
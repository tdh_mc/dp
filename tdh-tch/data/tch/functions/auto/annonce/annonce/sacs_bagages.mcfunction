# On génère un nombre aléatoire
scoreboard players set #random max 100

# Dans l'ordre :
# - Begin générique mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/jingle/begin_gen
# - Annonce en français
execute as @s[scores={annonceIndex=1}] run function tch:auto/annonce/sound/sacs_bagages_fr
# - Annonce en anglais (50%)
execute as @s[scores={annonceIndex=2}] run function tdh:random
execute as @s[scores={annonceIndex=2}] if score #random temp matches ..50 run function tch:auto/annonce/sound/sacs_bagages_en
execute as @s[scores={annonceIndex=2}] if score #random temp matches 51.. run scoreboard players add @s annonceIndex 1
# - Annonce en allemand (60%)
execute as @s[scores={annonceIndex=3}] run function tdh:random
execute as @s[scores={annonceIndex=3}] if score #random temp matches ..60 run function tch:auto/annonce/sound/sacs_bagages_de
execute as @s[scores={annonceIndex=3}] if score #random temp matches 61.. run scoreboard players add @s annonceIndex 1
# - Annonce en espagnol (60%)
execute as @s[scores={annonceIndex=4}] run function tdh:random
execute as @s[scores={annonceIndex=4}] if score #random temp matches ..60 run function tch:auto/annonce/sound/sacs_bagages_es
execute as @s[scores={annonceIndex=4}] if score #random temp matches 61.. run scoreboard players add @s annonceIndex 1
# - Annonce en italien (30%)
execute as @s[scores={annonceIndex=5}] run function tdh:random
execute as @s[scores={annonceIndex=5}] if score #random temp matches ..30 run function tch:auto/annonce/sound/sacs_bagages_it
execute as @s[scores={annonceIndex=5}] if score #random temp matches 31.. run scoreboard players add @s annonceIndex 1
# - Annonce en mandarin (50%)
execute as @s[scores={annonceIndex=6}] run function tdh:random
execute as @s[scores={annonceIndex=6}] if score #random temp matches ..50 run function tch:auto/annonce/sound/sacs_bagages_zh
execute as @s[scores={annonceIndex=6}] if score #random temp matches 51.. run scoreboard players add @s annonceIndex 1

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=7..}] run function tch:auto/annonce/next_wait
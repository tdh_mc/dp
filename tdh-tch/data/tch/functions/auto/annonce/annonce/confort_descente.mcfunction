# On génère un nombre aléatoire
scoreboard players set #random max 100

# Dans l'ordre :
# - Begin générique mono
execute as @s[scores={annonceIndex=0}] run function tch:auto/annonce/sound/jingle/begin_gen
# - Annonce en français
execute as @s[scores={annonceIndex=1}] run function tdh:random
execute as @s[scores={annonceIndex=1}] if score #random temp matches ..65 run function tch:auto/annonce/sound/confort_descente_fr
execute as @s[scores={annonceIndex=1}] if score #random temp matches 66.. run function tch:auto/annonce/sound/confort_descente_fr_short
# - Annonce en anglais (60%)
execute as @s[scores={annonceIndex=2}] run function tdh:random
execute as @s[scores={annonceIndex=2}] if score #random temp matches ..60 run function tch:auto/annonce/sound/confort_descente_en
execute as @s[scores={annonceIndex=2}] if score #random temp matches 61.. run scoreboard players add @s annonceIndex 1
# - Annonce en allemand (30%)
execute as @s[scores={annonceIndex=3}] run function tdh:random
execute as @s[scores={annonceIndex=3}] if score #random temp matches ..30 run function tch:auto/annonce/sound/confort_descente_de
execute as @s[scores={annonceIndex=3}] if score #random temp matches 31.. run scoreboard players add @s annonceIndex 1
# - Annonce en italien (30%)
execute as @s[scores={annonceIndex=4}] run function tdh:random
execute as @s[scores={annonceIndex=4}] if score #random temp matches ..30 run function tch:auto/annonce/sound/confort_descente_it
execute as @s[scores={annonceIndex=4}] if score #random temp matches 31.. run scoreboard players add @s annonceIndex 1

# Fin et attente de la prochaine annonce
execute as @s[scores={annonceIndex=5..}] run function tch:auto/annonce/next_wait
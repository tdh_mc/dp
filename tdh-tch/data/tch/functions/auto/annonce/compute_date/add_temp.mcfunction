# On initialise la valeur de temp2
scoreboard players set @s temp2 0

# On enregistre dans temp2 l'ID du premier buffer libre
execute if score @s annonceBuf1 matches 0 run scoreboard players set @s temp2 1
execute if score @s temp2 matches 0 if score @s annonceBuf2 matches 0 run scoreboard players set @s temp2 2
execute if score @s temp2 matches 0 if score @s annonceBuf3 matches 0 run scoreboard players set @s temp2 3
execute if score @s temp2 matches 0 if score @s annonceBuf4 matches 0 run scoreboard players set @s temp2 4
execute if score @s temp2 matches 0 if score @s annonceBuf5 matches 0 run scoreboard players set @s temp2 5
execute if score @s temp2 matches 0 if score @s annonceBuf6 matches 0 run scoreboard players set @s temp2 6
execute if score @s temp2 matches 0 if score @s annonceBuf7 matches 0 run scoreboard players set @s temp2 7
execute if score @s temp2 matches 0 if score @s annonceBuf8 matches 0 run scoreboard players set @s temp2 8
execute if score @s temp2 matches 0 if score @s annonceBuf9 matches 0 run scoreboard players set @s temp2 9

# On enregistre la valeur de temp dans le buffer indiqué par temp2
execute if score @s temp2 matches 1 run scoreboard players operation @s annonceBuf1 = @s temp
execute if score @s temp2 matches 2 run scoreboard players operation @s annonceBuf2 = @s temp
execute if score @s temp2 matches 3 run scoreboard players operation @s annonceBuf3 = @s temp
execute if score @s temp2 matches 4 run scoreboard players operation @s annonceBuf4 = @s temp
execute if score @s temp2 matches 5 run scoreboard players operation @s annonceBuf5 = @s temp
execute if score @s temp2 matches 6 run scoreboard players operation @s annonceBuf6 = @s temp
execute if score @s temp2 matches 7 run scoreboard players operation @s annonceBuf7 = @s temp
execute if score @s temp2 matches 8 run scoreboard players operation @s annonceBuf8 = @s temp
execute if score @s temp2 matches 9 run scoreboard players operation @s annonceBuf9 = @s temp
# On initialise une variable temporaire
scoreboard players set @s temp 0

# On incrémente cette variable pour chaque dizaine
execute if score @s temp6 matches 10.. run function tch:auto/annonce/compute_date/dizaines_inc

# S'il y avait 7 ou 9 dizaines, on en retranche une et on rajoute à nouveau 10
execute if score @s temp matches 7 run function tch:auto/annonce/compute_date/dizaines_dec
execute if score @s temp matches 9 run function tch:auto/annonce/compute_date/dizaines_dec

# S'il y avait bien une dizaine, on enregistre cette valeur
scoreboard players set @s temp4 10
scoreboard players operation @s temp *= @s temp4
execute if score @s temp matches 1.. run function tch:auto/annonce/compute_date/add_temp
scoreboard players reset @s temp4
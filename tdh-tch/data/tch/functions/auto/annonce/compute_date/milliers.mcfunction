# On initialise une variable temporaire
scoreboard players set @s temp 0

# On incrémente cette variable pour chaque millier
execute if score @s temp6 matches 1000.. run function tch:auto/annonce/compute_date/milliers_inc

# S'il y avait + d'1 millier, on enregistre cette valeur ; dans tous les cas on enregistre "1000"
execute if score @s temp matches 2.. run function tch:auto/annonce/compute_date/add_temp
execute if score @s temp matches 1.. run scoreboard players set @s temp 1000
execute if score @s temp matches 1000 run function tch:auto/annonce/compute_date/add_temp
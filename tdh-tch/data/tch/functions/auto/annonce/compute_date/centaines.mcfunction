# On initialise une variable temporaire
scoreboard players set @s temp 0

# On incrémente cette variable pour chaque centaine
execute if score @s temp6 matches 100.. run function tch:auto/annonce/compute_date/centaines_inc

# S'il y avait + d'1 centaine, on enregistre cette valeur : dans tous les cas on enregistre "100"
execute if score @s temp matches 2.. run function tch:auto/annonce/compute_date/add_temp
execute if score @s temp matches 1.. run scoreboard players set @s temp 100
execute if score @s temp matches 100 run function tch:auto/annonce/compute_date/add_temp
# Doit etre exécutée par l'armor stand NumLigne diffusant l'annonce,
# et calcule les différents sons devant être diffusés à la suite pour recréer la date présente dans dateAnnee

# On initialise d'abord les buffers
function tch:auto/annonce/compute_date/reset_buffers

# On récupère la dateAnnée dans temp6
scoreboard players operation @s temp6 = @s dateAnnee

# On stocke dans annonceBuf1>9 les différents sons
execute if score @s temp6 matches 1000.. run function tch:auto/annonce/compute_date/milliers
execute if score @s temp6 matches 100.. run function tch:auto/annonce/compute_date/centaines
execute if score @s temp6 matches 32.. run function tch:auto/annonce/compute_date/dizaines
scoreboard players operation @s temp = @s temp6
function tch:auto/annonce/compute_date/add_temp
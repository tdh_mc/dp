

# Si on a le score 4001, on diffuse un message de PID
execute as @s[scores={annonceID=4001}] run function tch:auto/annonce/annonce/compose/pid


# On incrémente l'index du son de l'annonce si on a un ID d'annonce
scoreboard players add @s[scores={annonceID=1..}] annonceIndex 1
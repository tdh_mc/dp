# Si on a atteint la fin du décompte, on essaie de parler
execute as @e[type=armor_stand,tag=NumLigne,scores={annonceTicks=0}] at @s if entity @p[distance=..54] run function tch:auto/annonce/next_sound
execute as @e[type=item_frame,tag=SpawnMetro,scores={annonceTicks=0}] at @s if entity @p[distance=..99] run function tch:auto/annonce/next_sound_pid
execute as @e[type=armor_stand,tag=SpawnCable,scores={annonceTicks=0}] at @s if entity @p[distance=..149] run function tch:auto/annonce/next_sound_pid
execute as @e[type=armor_stand,tag=SpawnRER,scores={annonceTicks=0}] at @s if entity @p[distance=..149] run function tch:auto/annonce/next_sound_pid

# Si on n'a jamais diffusé sur un quai, on génère un temps d'attente aléatoire avant d'entendre la première annonce
execute as @e[type=armor_stand,tag=NumLigne] unless score @s annonceTicks matches 0.. run function tch:auto/annonce/next_wait
execute as @e[type=item_frame,tag=SpawnMetro] unless score @s annonceTicks matches 0.. run function tch:auto/annonce/next_wait_pid
execute as @e[type=armor_stand,tag=SpawnCable] unless score @s annonceTicks matches 0.. run function tch:auto/annonce/next_wait_pid
execute as @e[type=armor_stand,tag=SpawnRER] unless score @s annonceTicks matches 0.. run function tch:auto/annonce/next_wait_pid

# Si on n'a pas de voix, on récupère notre voix
execute as @e[type=armor_stand,tag=NumLigne] unless score @s annonceVoice matches 1.. run function tch:auto/annonce/get_voice
execute as @e[type=item_frame,tag=SpawnMetro] unless score @s annonceVoice matches 1.. run function tch:auto/annonce/get_voice_pid
execute as @e[type=armor_stand,tag=SpawnCable] unless score @s annonceVoice matches 1.. run function tch:auto/annonce/get_voice_pid
execute as @e[type=armor_stand,tag=SpawnRER] unless score @s annonceVoice matches 1.. run function tch:auto/annonce/get_voice_pid

# Pour chaque diffuseur, on décrémente le nombre de ticks restants
execute as @e[type=armor_stand,tag=NumLigne,scores={annonceTicks=1..}] run scoreboard players remove @s annonceTicks 1
execute as @e[type=item_frame,tag=SpawnMetro,scores={annonceTicks=1..}] run scoreboard players remove @s annonceTicks 1
execute as @e[type=armor_stand,tag=SpawnCable,scores={annonceTicks=1..}] run scoreboard players remove @s annonceTicks 1
execute as @e[type=armor_stand,tag=SpawnRER,scores={annonceTicks=1..}] run scoreboard players remove @s annonceTicks 1
# Cette fonction doit être exécutée par une direction,
# à la position d'un joueur dont on veut tester s'il entend les annonces
# au volume standard (= s'il est proche de la bonne ligne), ou à volume
# réduit (= si ce n'est pas le cas)
execute at @e[tag=Direction,sort=nearest,limit=1,distance=..35] if score @e[tag=Direction,sort=nearest,limit=1] idLine = @s idLine run tag @e[tag=Direction,sort=nearest,limit=1] add Matches
execute at @e[tag=Direction,sort=nearest,limit=1,distance=..35] if score @e[tag=Direction,distance=1..,sort=nearest,limit=1] idLine = @s idLine run tag @e[tag=Direction,sort=nearest,limit=1] add Matches
execute positioned ~-35 ~-8 ~-35 unless entity @e[tag=Matches,dx=70,dy=16,dz=70] run tag @e[tag=Matches] remove Matches

execute if entity @e[tag=Matches,distance=..35] run tag @a[distance=..1] add PlayerTagSuccess

tag @e[tag=Matches,distance=..50] remove Matches
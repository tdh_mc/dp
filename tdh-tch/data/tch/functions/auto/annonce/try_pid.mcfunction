scoreboard players operation @s temp2 = @s spawnAt
scoreboard players operation @s temp2 -= #temps currentTdhTick
execute if score @s temp2 matches ..-1 run scoreboard players operation @s temp2 += #temps fullDayTicks

execute if score @s temp2 matches ..20 run tag @s add CanSpeak
execute unless score @s temp2 matches ..20 run function tch:auto/annonce/next_wait_pid
# LIGNE 1
execute at @s[tag=SpawnM1,name="Grenat–Ville"] run scoreboard players set @s idLine 11
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM1,distance=..2] as @s[name="Grenat–Ville"] run scoreboard players set @s idLine 11
execute at @s[tag=SpawnM1,name="Dorlinor"] run scoreboard players set @s idLine 12
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM1,distance=..2] as @s[name="Dorlinor"] run scoreboard players set @s idLine 12

# LIGNE 2
execute at @s[tag=SpawnM2,name="Évenis–Éclesta"] run scoreboard players set @s idLine 21
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM2,distance=..2] as @s[name="Évenis–Éclesta"] run scoreboard players set @s idLine 21
execute at @s[tag=SpawnM2,name="Sablons–Brimiel"] run scoreboard players set @s idLine 22
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM2,distance=..2] as @s[name="Sablons–Brimiel"] run scoreboard players set @s idLine 22

# LIGNE 3
execute at @s[tag=SpawnM3,name="Tolbrok"] run scoreboard players set @s idLine 31
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM3,distance=..2] as @s[name="Tolbrok"] run scoreboard players set @s idLine 31
execute at @s[tag=SpawnM3,name="Gléry"] run scoreboard players set @s idLine 32
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM3,distance=..2] as @s[name="Gléry"] run scoreboard players set @s idLine 32

# LIGNE 4
execute at @s[tag=SpawnM4,name="Tilia"] run scoreboard players set @s idLine 41
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM4,distance=..2] as @s[name="Tilia"] run scoreboard players set @s idLine 41
execute at @s[tag=SpawnM4,name="Pieuze-en-Sulûm"] run scoreboard players set @s idLine 42
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM4,distance=..2] as @s[name="Pieuze-en-Sulûm"] run scoreboard players set @s idLine 42

# LIGNE 5a
execute at @s[tag=SpawnM5a,name="Asvaard–Rudelieu"] run scoreboard players set @s idLine 51
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM5a,distance=..2] as @s[name="Asvaard–Rudelieu"] run scoreboard players set @s idLine 51
execute at @s[tag=SpawnM5a,name="Touissy"] run scoreboard players set @s idLine 52
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM5a,distance=..2] as @s[name="Touissy"] run scoreboard players set @s idLine 52

# LIGNE 5b
execute at @s[tag=SpawnM5b,name="Villonne"] run scoreboard players set @s idLine 55
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM5b,distance=..2] as @s[name="Villonne"] run scoreboard players set @s idLine 55
execute at @s[tag=SpawnM5b,name="Dorlinor"] run scoreboard players set @s idLine 56
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM5b,distance=..2] as @s[name="Dorlinor"] run scoreboard players set @s idLine 56

# LIGNE 6
execute at @s[tag=SpawnM6,name="Fultèz-en-Sulûm"] run scoreboard players set @s idLine 61
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM6,distance=..2] as @s[name="Fultèz-en-Sulûm"] run scoreboard players set @s idLine 61
execute at @s[tag=SpawnM6,name="Midès"] run scoreboard players set @s idLine 62
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM6,distance=..2] as @s[name="Midès"] run scoreboard players set @s idLine 62

# LIGNE 7
execute at @s[tag=SpawnM7,name="Chassy-sur-Flumine"] run scoreboard players set @s idLine 71
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM7,distance=..2] as @s[name="Chassy-sur-Flumine"] run scoreboard players set @s idLine 71
execute at @s[tag=SpawnM7,name="Évenis–Tamlyn"] run scoreboard players set @s idLine 72
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM7,distance=..2] as @s[name="Évenis–Tamlyn"] run scoreboard players set @s idLine 72

# LIGNE 7 bis
execute at @s[tag=SpawnM7b,name="Litoréa"] run scoreboard players set @s idLine 73
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM7b,distance=..2] as @s[name="Litoréa"] run scoreboard players set @s idLine 73
execute at @s[tag=SpawnM7b,name="Quatre Chemins"] run scoreboard players set @s idLine 74
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM7b,distance=..2] as @s[name="Quatre Chemins"] run scoreboard players set @s idLine 74

# LIGNE 8
execute at @s[tag=SpawnM8,name="Procyon–Université"] run scoreboard players set @s idLine 81
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM8,distance=..2] as @s[name="Procyon–Université"] run scoreboard players set @s idLine 81
execute at @s[tag=SpawnM8,name="Géorlie"] run scoreboard players set @s idLine 82
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM8,distance=..2] as @s[name="Géorlie"] run scoreboard players set @s idLine 82

# LIGNE 8 bis
execute at @s[tag=SpawnM8b,name="Frambourg–Ternelieu"] run scoreboard players set @s idLine 83
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM8b,distance=..2] as @s[name="Frambourg–Ternelieu"] run scoreboard players set @s idLine 83
execute at @s[tag=SpawnM8b,name="Le Relais"] run scoreboard players set @s idLine 84
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM8b,distance=..2] as @s[name="Le Relais"] run scoreboard players set @s idLine 84

# LIGNE 9
execute at @s[tag=SpawnM9,name="Chamborion"] run scoreboard players set @s idLine 91
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM9,distance=..2] as @s[name="Chamborion"] run scoreboard players set @s idLine 91
execute at @s[tag=SpawnM9,name="La Pogne-en-Ardence"] run scoreboard players set @s idLine 92
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM9,distance=..2] as @s[name="La Pogne-en-Ardence"] run scoreboard players set @s idLine 92

# LIGNE 10
execute at @s[tag=SpawnM10,name="Vaillebourg"] run scoreboard players set @s idLine 101
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM10,distance=..2] as @s[name="Vaillebourg"] run scoreboard players set @s idLine 101
execute at @s[tag=SpawnM10,name="Poudriole"] run scoreboard players set @s idLine 102
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM10,distance=..2] as @s[name="Poudriole"] run scoreboard players set @s idLine 102

# LIGNE 11
execute at @s[tag=SpawnM11,name="Évernay-l'Aumône"] run scoreboard players set @s idLine 111
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM11,distance=..2] as @s[name="Évernay-l'Aumône"] run scoreboard players set @s idLine 111
execute at @s[tag=SpawnM11,name="Croassin-le-Désert"] run scoreboard players set @s idLine 112
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM11,distance=..2] as @s[name="Croassin-le-Désert"] run scoreboard players set @s idLine 112

# LIGNE 12
execute at @s[tag=SpawnM12,name="Cyséal"] run scoreboard players set @s idLine 121
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM12,distance=..2] as @s[name="Cyséal"] run scoreboard players set @s idLine 121
execute at @s[tag=SpawnM12,name="Paupéry"] run scoreboard players set @s idLine 122
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM12,distance=..2] as @s[name="Paupéry"] run scoreboard players set @s idLine 122
execute at @s[tag=SpawnM12,name="La Gambergerie"] run scoreboard players set @s idLine 123
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationM12,distance=..2] as @s[name="La Gambergerie"] run scoreboard players set @s idLine 123

# RER A
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERA,distance=..2] as @s[name="Sud"] run scoreboard players set @s idLine 2100
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERA,distance=..2] as @s[name="Sud"] run scoreboard players set @s idLineMax 2149
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERA,distance=..2] as @s[name="Cyséal"] run scoreboard players set @s idLine 2101
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERA,distance=..2] as @s[name="Procyon"] run scoreboard players set @s idLine 2102
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERA,distance=..2] as @s[name="Nord"] run scoreboard players set @s idLine 2150
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERA,distance=..2] as @s[name="Nord"] run scoreboard players set @s idLineMax 2199
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERA,distance=..2] as @s[name="Beurrieux–Valdemont"] run scoreboard players set @s idLine 2153
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERA,distance=..2] as @s[name="Venise"] run scoreboard players set @s idLine 2154

# RER B
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERB,distance=..2] as @s[name="Est"] run scoreboard players set @s idLine 2200
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERB,distance=..2] as @s[name="Est"] run scoreboard players set @s idLineMax 2249
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERB,distance=..2] as @s[name="Ouest"] run scoreboard players set @s idLine 2250
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationRERB,distance=..2] as @s[name="Ouest"] run scoreboard players set @s idLineMax 2299

# CÂBLE A
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationCA,distance=..2] as @s[name="Évrocq–Le Haut"] run scoreboard players set @s idLine 4011
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationCA,distance=..2] as @s[name="Évrocq–Sauvebonne"] run scoreboard players set @s idLine 4012

# CÂBLE B
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationCB,distance=..2] as @s[name="Calmeflot-les-Deux-Berges"] run scoreboard players set @s idLine 4021
execute at @s at @e[tag=NumLigne,distance=..100,sort=nearest,limit=1] if entity @e[tag=StationCB,distance=..2] as @s[name="La Dodène"] run scoreboard players set @s idLine 4022


execute unless score @s idLine matches 1.. run tellraw @a [{"text":"[WARN] ID de ligne non trouvé : ","color":"gold"},{"selector":"@s"}]
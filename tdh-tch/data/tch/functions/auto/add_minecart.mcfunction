execute store result score @s deltaX run data get entity @s Motion[0] 10000
execute store result score @s deltaZ run data get entity @s Motion[2] 10000
data modify entity @s Motion set value [0d,0d,0d]

execute if score @s deltaX matches 0 if score @s deltaZ matches ..-1 run scoreboard players set @s launchDirection 1
execute if score @s deltaX matches 1.. if score @s deltaZ matches 0 run scoreboard players set @s launchDirection 2
execute if score @s deltaX matches 0 if score @s deltaZ matches 1.. run scoreboard players set @s launchDirection 3
execute if score @s deltaX matches ..-1 if score @s deltaZ matches 0 run scoreboard players set @s launchDirection 4

execute if score @s deltaX matches 1.. if score @s deltaZ matches 1.. run scoreboard players set @s launchDirection 23
execute if score @s deltaX matches ..-1 if score @s deltaZ matches 1.. run scoreboard players set @s launchDirection 34
execute if score @s deltaX matches ..-1 if score @s deltaZ matches ..-1 run scoreboard players set @s launchDirection 41
execute if score @s deltaX matches 1.. if score @s deltaZ matches ..-1 run scoreboard players set @s launchDirection 12

scoreboard players set @s remainingTicks 9


# Si on n'a pas réussi à trouver de launchDirection correcte, on détruit ce minecart
execute unless score @s launchDirection matches 1..41 at @s run function tch:auto/destroy_minecart_one
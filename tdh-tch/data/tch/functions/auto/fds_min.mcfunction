# On indique aux métros des lignes 4/5a/5b/7b/8b que c'est la fin de service
tag @e[tag=Metro,scores={idLine=41..59}] add FinDeService
tag @e[tag=Metro,scores={idLine=73..74}] add FinDeService
tag @e[tag=Metro,scores={idLine=83..84}] add FinDeService

# On joue l'annonce de fin de service pour informer les joueurs
execute at @p[tag=tchInside] at @e[tag=NumLigne,sort=nearest,distance=..30,limit=1] if score @e[sort=nearest,limit=1] idLine matches 41..59 run function tch:sound/metro/fin_service2
execute at @p[tag=tchInside] at @e[tag=NumLigne,sort=nearest,distance=..30,limit=1] if score @e[sort=nearest,limit=1] idLine matches 73..74 run function tch:sound/metro/fin_service2
execute at @p[tag=tchInside] at @e[tag=NumLigne,sort=nearest,distance=..30,limit=1] if score @e[sort=nearest,limit=1] idLine matches 83..84 run function tch:sound/metro/fin_service2
# On enregistre toutes les infos du minecart dans la fonction add_minecart
execute as @e[tag=Metro,distance=..5,limit=1,sort=nearest] at @s run function tch:auto/add_minecart

# On stoppe le minecart et on le téléporte précisément à la bonne position (2 blocs au dessus du command block)
execute as @e[tag=Metro,distance=..5,limit=1,sort=nearest] run data modify entity @s Motion set value [0d,0d,0d]
execute as @e[tag=Metro,distance=..5,limit=1,sort=nearest] run tp @s ~ ~2 ~

# On réinitialise les tags et on sauvegarde les infos de la station suivante
execute as @e[tag=Metro,distance=..5,limit=1,sort=nearest] run function tch:auto/tag_en_station
execute as @e[tag=Metro,distance=..5,sort=nearest,limit=1] run function tch:auto/title/save_line_id

# On ouvre les portes
execute unless entity @e[distance=..50,tag=StationInversee] if score @e[tag=Metro,distance=..5,limit=1,sort=nearest] launchDirection matches 1 run fill ~2 ~3 ~ ~2 ~4 ~ air
execute unless entity @e[distance=..50,tag=StationInversee] if score @e[tag=Metro,distance=..5,limit=1,sort=nearest] launchDirection matches 2 run fill ~ ~3 ~2 ~ ~4 ~2 air
execute unless entity @e[distance=..50,tag=StationInversee] if score @e[tag=Metro,distance=..5,limit=1,sort=nearest] launchDirection matches 3 run fill ~-2 ~3 ~ ~-2 ~4 ~ air
execute unless entity @e[distance=..50,tag=StationInversee] if score @e[tag=Metro,distance=..5,limit=1,sort=nearest] launchDirection matches 4 run fill ~ ~3 ~-2 ~ ~4 ~-2 air

execute if entity @e[distance=..50,tag=StationInversee] if score @e[tag=Metro,distance=..5,limit=1,sort=nearest] launchDirection matches 1 run fill ~-2 ~3 ~ ~-2 ~4 ~ air
execute if entity @e[distance=..50,tag=StationInversee] if score @e[tag=Metro,distance=..5,limit=1,sort=nearest] launchDirection matches 2 run fill ~ ~3 ~-2 ~ ~4 ~-2 air
execute if entity @e[distance=..50,tag=StationInversee] if score @e[tag=Metro,distance=..5,limit=1,sort=nearest] launchDirection matches 3 run fill ~2 ~3 ~ ~2 ~4 ~ air
execute if entity @e[distance=..50,tag=StationInversee] if score @e[tag=Metro,distance=..5,limit=1,sort=nearest] launchDirection matches 4 run fill ~ ~3 ~2 ~ ~4 ~2 air

# On joue le son d'ouverture des portes
execute positioned ~ ~3 ~ run function tch:sound/metro/station_doors_open
execute positioned ~ ~1 ~ run function tch:sound/metro/arrivee_train_end
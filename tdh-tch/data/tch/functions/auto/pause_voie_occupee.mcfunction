execute at @s run say mise en pause (je contiens @p[distance=..2])

execute store result score @s deltaX run data get entity @s Motion[0] 1000
execute store result score @s deltaZ run data get entity @s Motion[2] 1000
data modify entity @s Motion set value [0d,0d,0d]

execute if score @s deltaX matches 0 if score @s deltaZ matches ..-1 run scoreboard players set @s launchDirection 1
execute if score @s deltaX matches 1.. if score @s deltaZ matches 0 run scoreboard players set @s launchDirection 2
execute if score @s deltaX matches 0 if score @s deltaZ matches 1.. run scoreboard players set @s launchDirection 3
execute if score @s deltaX matches ..-1 if score @s deltaZ matches 0 run scoreboard players set @s launchDirection 4

execute if score @s deltaX matches 1.. if score @s deltaZ matches 1.. run scoreboard players set @s launchDirection 23
execute if score @s deltaX matches ..-1 if score @s deltaZ matches 1.. run scoreboard players set @s launchDirection 34
execute if score @s deltaX matches ..-1 if score @s deltaZ matches ..-1 run scoreboard players set @s launchDirection 41
execute if score @s deltaX matches 1.. if score @s deltaZ matches ..-1 run scoreboard players set @s launchDirection 12

scoreboard players set @s remainingTicks 5
scoreboard players set @s ArretUrgence 1

tellraw @p[distance=..3] [{"text":"Notre train est actuellement ","color":"gray"},{"text":"arrêté en pleine voie","color":"red"},{"text":". TCH vous prie d'accepter ses excuses pour la gêne occasionnée.","color":"gray"}]
execute at @p[distance=..3] run function tch:sound/arret_imprevu
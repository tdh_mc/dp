function tch:sound/arrivee_station_auto

tag @s add tempTag
tellraw @p[distance=..1] [{"text":"Prochaine station : ","color":"gray"},{"selector":"@e[tag=NomStation,sort=nearest,limit=1,distance=..200]"}]
execute if entity @e[tag=NumLigne,tag=!tempTag,distance=..200] run tellraw @p[distance=..1] [{"text":"Correspondance avec les lignes ","color":"gray"},{"selector":"@e[tag=NumLigne,tag=!tempTag,distance=..200]"}]
tag @s remove tempTag
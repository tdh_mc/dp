# Ligne 1 : haute fréquence
execute if score @s idLine matches 11..12 run function tch:auto/spawn_minecart_random_mods_max

# Lignes 2 et 3 : moyenne fréquence
execute if score @s idLine matches 21..32 run function tch:auto/spawn_minecart_random_mods_med

# Lignes 4 et 5a/5b : basse fréquence
execute if score @s idLine matches 41..56 run function tch:auto/spawn_minecart_random_mods_min

# Ligne 6 : haute fréquence
execute if score @s idLine matches 61..62 run function tch:auto/spawn_minecart_random_mods_max

# Ligne 7: moyenne fréquence
execute if score @s idLine matches 71..72 run function tch:auto/spawn_minecart_random_mods_med

# Ligne 7b : basse fréquence
execute if score @s idLine matches 73..74 run function tch:auto/spawn_minecart_random_mods_min

# Ligne 8 : haute fréquence
execute if score @s idLine matches 81..82 run function tch:auto/spawn_minecart_random_mods_max

# Ligne 8b : basse fréquence
execute if score @s idLine matches 83..84 run function tch:auto/spawn_minecart_random_mods_min
execute at @s if entity @e[type=armor_stand,distance=..75,tag=NomStation] run tellraw @p[distance=..1] [{"text":"Vous êtes arrivé à la station ","color":"gray"},{"selector":"@e[type=armor_stand,distance=..75,tag=NomStation,limit=1]","color":"aqua","bold":"true"},{"text":", terminus de la ligne.","color":"gray"}]
execute at @s run tellraw @p[distance=..1] [{"text":"Tous les voyageurs sont invités à descendre","color":"red"},{"text":". Merci !","color":"gray"}]

scoreboard players set @e[type=armor_stand,distance=..50,sort=nearest,limit=1,tag=Direction] VoieReservee 0

execute at @s run tellraw @p [{"text":"DEBUG -- ","color":"gold"},{"text":"Fonction terminus obsolète! Merci de remplacer la fonction ","color":"gray"},{"text":"terminus_descente_xxx","italic":"true"},{"text":" par la fonction ","color":"gray"},{"text":"terminus","italic":"true"},{"text":", tout simplement.","color":"gray"}]
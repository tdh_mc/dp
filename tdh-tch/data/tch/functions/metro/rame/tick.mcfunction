# TICK DES MÉTROS
# Cette fonction est exécutée régulièrement par chacun des métros pour exécuter tout ce qui est nécessaire

# Si on est en station, on vérifie quand on doit repartir
execute as @s[tag=EnStation] run function tch:metro/rame/en_station/tick

# Si on est en arrêt d'urgence, on vérifie quand on doit repartir
execute as @s[tag=ArretUrgence] run function tch:metro/rame/pre_station/tick_arret_urgence

# On supprime tout métro qui serait trop loin d'une station sans voyageur à proximité
execute as @s[tag=!EnStation] unless entity @e[tag=NomStation,distance=..150,limit=1] unless entity @p[distance=..25] run function tch:metro/rame/se_detruire


# On affiche un message de debug si le joueur a le tag automatique de mesure
execute as @e[tag=Metro,distance=..4,sort=nearest,limit=1] at @s as @p[distance=..1,gamemode=!spectator,tag=MesureAuto] run function tch:debug/mesure/auto/depart_station

# On lance le métro dans la bonne direction
execute store result entity @s Motion[0] double 0.001 run scoreboard players get @s deltaX
execute store result entity @s Motion[1] double 0.001 run scoreboard players get @s deltaY
execute store result entity @s Motion[2] double 0.001 run scoreboard players get @s deltaZ

# On déréserve la voie que l'on occupait
function tch:voie/reservation/depart
tag @s remove EnStation

# On récupère l'ID de la prochaine station
# (en principe c'est désormais géré par le système d'itinéraire et récupéré automatiquement dans la fonction *arrivee* plutôt que depart)
#scoreboard players operation @s idStation = @e[tag=ProchaineStation,sort=nearest,limit=1] idStation


# On supprime nos variables
scoreboard players reset @s launchDirection
scoreboard players reset @s remainingTicks
scoreboard players reset @s deltaX
scoreboard players reset @s deltaY
scoreboard players reset @s deltaZ

# S'il n'y a pas de joueur à proximité, on supprime le minecart plutôt que de le faire partir
execute unless entity @p[distance=..60] run function tch:metro/rame/se_detruire

# On fait un test pour éviter de faire partir le minecart dans un chunk déchargé
#function tch:metro/rame/en_station/verifier_chunks
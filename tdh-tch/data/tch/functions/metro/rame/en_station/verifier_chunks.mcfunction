# Si les chunks ne sont pas chargés 32 blocs (2 chunks) devant nous, on s'auto supprime pour éviter de se faire décharger
# Si les 2 conditions échouent (le bloc n'est pas de l'air, ni autre chose que de l'air), ça signifie que le chunk est déchargé
# Comme on ne peut pas savoir si le minecart va "vers l'avant"(vers sa droite) ou "vers l'arrière"(vers sa gauche), on fait 2x le test
tag @s add ChunkCheck1
tag @s add ChunkCheck2
execute if block ^32 ^ ^ air run tag @s remove ChunkCheck1
execute unless block ^32 ^ ^ air run tag @s remove ChunkCheck1
execute if block ^-32 ^ ^ air run tag @s remove ChunkCheck2
execute unless block ^-32 ^ ^ air run tag @s remove ChunkCheck2
# Si le chunk est déchargé, on décharge aussi le minecart
execute as @s[tag=ChunkCheck1] run function tch:metro/rame/se_detruire
execute as @s[tag=ChunkCheck2] run function tch:metro/rame/se_detruire
# L'item frame "PorteQuai" est 3 blocs en dessous du bas de la "porte"
# (2 blocs solides + sa position à elle)

# On retire la porte
fill ~ ~3 ~ ~ ~4 ~ air

# On joue le son d'ouverture
execute positioned ~ ~3.3 ~ run function tch:sound/metro/station_doors_open
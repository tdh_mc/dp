# Cette fonction est exécutée par un métro à sa position, pour faire terminus
# (Il ne s'arrête pas à la station et sort le joueur du métro)

# On affiche un message de terminus
# On n'utilise pas le même code couleur que dans les tunnels car ça serait trop clair donc illisible
# Le "noir" (#181517) est quant à lui lisible même en moody sur des stone bricks/andesite peu éclairées
execute as @s[tag=!FinDeService] run title @a[distance=..1] actionbar [{"text":"Vous êtes arrivé à ","color":"#181517"},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]","color":"#3d090e"},{"text":", terminus du "},{"selector":"@e[tag=NumLigne,sort=nearest,limit=1]","color":"#181517"},{"text":"."}]
# Si on est en fin de service, on précise bien que c'est le terminus DE LA RAME et pas de la ligne
execute as @s[tag=FinDeService] run title @a[distance=..1] actionbar [{"text":"Vous êtes arrivé à ","color":"#181517"},{"selector":"@e[tag=NomStation,sort=nearest,limit=1]","color":"#3d090e"},{"text":", terminus de cette rame."}]


# On joue un son de fin de service, si on est en fin de service
#TODO: Remplacer cet appel dégueulasse par une fonction similaire à *force_sound_pid* pour forcer la diffusion de l'annonce FDS
execute as @s[tag=FinDeService] at @e[tag=tchHP,distance=..15] run function tch:sound/metro/fin_service_1


# On déréserve la voie
function tch:voie/reservation/depart

# Enfin, on sort le joueur du métro
function tch:metro/rame/en_station/tp_quai
# On exécute cette fonction en tant qu'un métro venant d'arriver en station,
# pour récupérer l'ID de la prochaine station de l'itinéraire
# (Il y en a forcément une puisqu'on n'a pas fait terminus dans cette station)

# On récupère notre PC de quai
function tch:tag/quai_pc
# On définit les valeurs à rechercher (notre ID de ligne, pas d'ID de sortie)
# On reset l'ID station pour indiquer qu'on recherche cette valeur
scoreboard players set #tchItineraire idSortie 0
scoreboard players reset #tchItineraire idStation
scoreboard players operation #tchItineraire idLine = @s idLine
# On initialise la valeur de retour (-1 = pas de station trouvée)
scoreboard players set @s prochaineStation -1

# On cherche la bonne connexion parmi celles de notre PC
execute as @e[type=item_frame,tag=ourQuaiPC,limit=1] run function tch:itineraire/noeud/trouver_connexion
# Si on l'a trouvée, on récupère le bon ID de prochaine station
execute if entity @e[type=item_frame,tag=ourQuaiPC,tag=ConnexionTrouvee,limit=1] run function tch:metro/rame/en_station/prochaine_station/copier_donnees

# Si on a pas trouvé de station, on affiche un message d'erreur aux users MetroDebugLog
execute unless score @s prochaineStation matches 1.. run tellraw @a[tag=MetroDebugLog] [{"text":"[ERREUR]","color":"red"},{"text":" Impossible de trouver un trajet pour le métro ","color":"gray","extra":[{"selector":"@s"},{"text":" d'ID "}]},{"score":{"name":"@s","objective":"idLine"}},{"text":" au départ de ","color":"gray"},{"selector":"@e[type=item_frame,tag=ourQuaiPC,limit=1]"},{"text":".","color":"gray"}]

# On supprime le tag du PC de quai
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
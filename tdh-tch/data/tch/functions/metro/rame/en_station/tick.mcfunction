## TICK DES MÉTROS EN STATION

# 6 secondes avant le départ, on lance le son correspondant
execute if score @s remainingTicks matches 6 run function tch:sound/metro/signal_depart
# 2 secondes avant le départ, on cesse de forcer le PID à afficher 00
execute if score @s remainingTicks matches 2 run function tch:station/pid/unlock_0_metro
# 1 seconde avant le départ, on referme les portes de quai
execute if score @s remainingTicks matches 1 at @e[type=item_frame,tag=PorteQuai,distance=..5,sort=nearest,limit=1] run function tch:metro/rame/en_station/fermeture_portes
# Si on doit être lancés, c'est parti!
execute if score @s remainingTicks matches ..0 run function tch:metro/rame/en_station/depart

# Si on doit être lancés prochainement, on décrémente le temps d'attente restant
execute if score @s remainingTicks matches 1.. run scoreboard players remove @s remainingTicks 1
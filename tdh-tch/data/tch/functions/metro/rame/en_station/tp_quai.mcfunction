# On tag tous les joueurs se trouvant dans le métro
tag @a[distance=..1,gamemode=!spectator] add PlayerTerminus

# On téléporte tous les joueurs devant la porte de quai
execute at @e[tag=PorteQuai,distance=..5,sort=nearest,limit=1] facing entity @s eyes rotated ~ 0 align y run tp @a[tag=PlayerTerminus,distance=..5] ^ ^3 ^-1 ~180 0

# S'il n'y a pas de porte de quai, on téléporte les joueurs à l'arrache
execute unless entity @e[tag=PorteQuai,distance=..5] facing entity @e[tag=PID,distance=..20,sort=nearest,limit=1] feet rotated ~ 0 run tp @a[tag=PlayerTerminus,distance=..5] ^ ^1 ^3 ~ 0

# On retire le tag desdits joueurs
tag @a[tag=PlayerTerminus,distance=..10] remove PlayerTerminus
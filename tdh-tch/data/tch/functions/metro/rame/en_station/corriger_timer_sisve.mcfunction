# Le timer du SISVE devrait être égal à 0 lors de l'arrivée en station
# Si ce n'est pas le cas, on passe dans cette fonction (en tant que le métro)

# On commence par récupérer le PC du quai *précédent*, qu'on n'a pas encore remplacé puisqu'on n'est pas
# encore passés par la fonction "arret"
function tch:tag/quai_pc
# On retrouve la connexion correspondant à notre ID de ligne en déplaçant les connexions dans la liste
scoreboard players operation #tchItineraire idStation = @s prochaineStation
scoreboard players operation #tchItineraire idLine = @s idLine
scoreboard players set #tchItineraire idSortie 0
execute as @e[type=item_frame,tag=ourQuaiPC,limit=1] run function tch:itineraire/noeud/trouver_connexion
execute unless entity @e[type=item_frame,tag=ourQuaiPC,limit=1] run tellraw @a[tag=MetroDebugLog] [{"text":"[ERREUR]","color":"red"},{"text":" Impossible de trouver un quai de la ligne ","color":"gray","extra":[{"selector":"@s"},{"text":" d'ID "}]},{"score":{"name":"@s","objective":"idLine"}},{"text":" au départ de la station ","color":"gray"},{"score":{"name":"@s","objective":"idStation"},"color":"red"},{"text":".","color":"gray"}]

# La variation qu'on a appliquée au timer du SISVE est stockée dans @s variation :
# On la module par ce qu'il reste dans notre timer du SISVE

# Par exemple, si la valeur du timer est de -5, cela signifie qu'il MANQUE 5 secondes à notre timer ;
# par conséquent, il faut AJOUTER ces 5 secondes à la variation
# Donc on retranche la valeur de notre timer à la variation appliquée, avant de remplacer l'originale
execute store result entity @e[type=item_frame,tag=ourQuaiPC,tag=ConnexionTrouvee,limit=1] Item.tag.tch.connexions[0].timerSisve int 1 run scoreboard players operation @s variation -= @s displayTicks

# On retire le tag du PC
tag @e[type=item_frame,tag=ourQuaiPC,tag=ConnexionTrouvee] remove ConnexionTrouvee
tag @e[type=item_frame,tag=ourQuaiPC] remove ourQuaiPC
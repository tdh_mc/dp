# Arrivée en station (passage du minecart sur le rail détecteur)
# Cette fonction est exécutée par un command block et non pas par le métro comme la plupart des autres fonctions

# On affiche un message de debug si le joueur a le tag automatique de mesure
execute as @e[type=minecart,tag=Metro,distance=..4,sort=nearest,limit=1] at @s as @p[distance=..1,gamemode=!spectator,tag=MesureAuto] run function tch:debug/mesure/auto/arrivee_station

# Qu'il ait ou non le tag automatique de mesure, s'il ne vient pas de spawner (=il n'a pas déjà le tag "EnStation"), on vérifie le statut de son timer SISVE, et on tente de modifier la valeur enregistrée dans le système d'itinéraire si celui-ci n'est pas parfait (= égal à 0 à l'arrivée)
execute as @e[type=minecart,tag=Metro,distance=..4,sort=nearest,limit=1] unless score @s[tag=!EnStation] displayTicks matches 0 run function tch:metro/rame/en_station/corriger_timer_sisve

# Si on fait terminus ici, on affiche un message
execute as @e[type=minecart,tag=Metro,distance=..4,sort=nearest,limit=1] at @s[tag=Terminus] run function tch:metro/rame/en_station/terminus

# Sinon, on arrête normalement le métro
execute as @e[type=minecart,tag=Metro,distance=..4,sort=nearest,limit=1] at @s[tag=!Terminus] run function tch:metro/rame/en_station/arret
# On le replace après la fonction puisqu'on change la position d'exécution
execute positioned ~ ~2 ~ as @e[tag=Metro,tag=!Terminus,distance=..2,sort=nearest,limit=1] rotated as @s run tp @s ~ ~ ~
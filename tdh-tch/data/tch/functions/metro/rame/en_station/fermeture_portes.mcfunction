# L'item frame "PorteQuai" est 3 blocs en dessous du bas de la "porte"
# (2 blocs solides + sa position à elle)

# On pose la porte
fill ~ ~3 ~ ~ ~4 ~ light_gray_stained_glass_pane keep

# On joue le son de fermeture
execute positioned ~ ~3.3 ~ run function tch:sound/station_doors_close
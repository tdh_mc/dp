# Cette fonction est exécutée par un métro à sa position, pour marquer l'arrêt en station
# (Il s'arrête à la station et ne sort pas le joueur du métro)

# On enregistre le fait qu'on est arrivés en station
tag @s add EnStation
tag @s remove PreStation
tag @s remove Corresp
tag @s remove DescenteGauche
# 10 secondes avant le départ
scoreboard players set @s remainingTicks 9

# On enregistre l'ID de cette station
scoreboard players operation @s idStation = @e[type=#tch:marqueur/station,tag=NomStation,distance=..200,sort=nearest,limit=1] idStation
# On enregistre l'ID de la prochaine station
function tch:metro/rame/en_station/prochaine_station

# On enregistre notre mouvement actuel pour pouvoir le restaurer lors du redémarrage
execute unless score @s deltaX matches 1.. store result score @s deltaX run data get entity @s Motion[0] 1000
execute unless score @s deltaY matches 1.. store result score @s deltaY run data get entity @s Motion[1] 1000
execute unless score @s deltaZ matches 1.. store result score @s deltaZ run data get entity @s Motion[2] 1000
# On met un check supplémentaire pour remettre le métro à la vitesse max
execute if score @s deltaX matches 1.. run scoreboard players set @s deltaX 2000
execute if score @s deltaX matches ..-1 run scoreboard players set @s deltaX -2000
execute if score @s deltaZ matches 1.. run scoreboard players set @s deltaZ 2000
execute if score @s deltaZ matches ..-1 run scoreboard players set @s deltaZ -2000

# On arrête tout mouvement
data modify entity @s Motion set value [0d,0d,0d]

# On ouvre les portes de quai
execute at @e[type=item_frame,tag=PorteQuai,distance=..5,sort=nearest,limit=1] run function tch:metro/rame/en_station/ouverture_portes

# On joue la fin du son d'arrivée métro (en mono pour tout le monde, maintenant que la rame n'est plus en mouvement)
function tch:sound/metro/arrivee_train_end
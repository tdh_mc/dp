# On copie les données requises de notre PC vers les nôtres

# La prochaine station est passée en retour par la fonction trouver_connexion
scoreboard players operation @s prochaineStation = #tchItineraire prochaineStation
# Par contre celle-ci ne récupère pas le décalage du SISVE, on doit donc faire un data nous-mêmes
execute store result score @s variation run data get entity @e[type=item_frame,tag=ourQuaiPC,limit=1] Item.tag.tch.connexions[0].timerSisve

# On supprime le tag ConnexionTrouvee
tag @e[type=item_frame,tag=ourQuaiPC] remove ConnexionTrouvee
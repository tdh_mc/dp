# Fonction d'arrêt d'urgence du métro, exécutée par un métro à sa position

# On récupère notre voie
function tch:tag/prochaine_voie

# On enregistre le fait qu'on a fait un arrêt d'urgence
tag @s add ArretUrgence
scoreboard players set @s remainingTicks 5

# On enregistre notre mouvement actuel pour pouvoir le restaurer lors du redémarrage
execute store result score @s deltaX run data get entity @s Motion[0] 1000
execute store result score @s deltaY run data get entity @s Motion[1] 1000
execute store result score @s deltaZ run data get entity @s Motion[2] 1000
# On met un check supplémentaire pour remettre le métro à la vitesse max
execute if score @s deltaX matches 1.. run scoreboard players set @s deltaX 2000
execute if score @s deltaX matches ..-1 run scoreboard players set @s deltaX -2000
execute if score @s deltaZ matches 1.. run scoreboard players set @s deltaZ 2000
execute if score @s deltaZ matches ..-1 run scoreboard players set @s deltaZ -2000

# Si on est en pente, on met un powered rail sous nous
execute if block ~ ~ ~ rail[shape=ascending_east] run tag @s add EnPente
execute if block ~ ~ ~ rail[shape=ascending_west] run tag @s add EnPente
execute if block ~ ~ ~ rail[shape=ascending_north] run tag @s add EnPente
execute if block ~ ~ ~ rail[shape=ascending_south] run tag @s add EnPente
execute as @s[tag=EnPente] run setblock ~ ~ ~ powered_rail[powered=false]

# On arrête tout mouvement
data modify entity @s Motion set value [0d,0d,0d]

# On stoppe les sons d'arrivée métro en train de se jouer
stopsound @a[distance=..1] ambient tch:br.metro.arrive_inside
execute at @e[tag=ourVoie,limit=1] run stopsound @a[distance=..24] ambient tch:br.metro.arrive

# On lance un son d'excuses
function tch:sound/arret_imprevu



# On supprime le tag de notre voie
tag @e[tag=ourVoie] remove ourVoie
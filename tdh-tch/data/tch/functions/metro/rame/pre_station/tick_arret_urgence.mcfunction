## TICK DES MÉTROS EN ARRÊT D'URGENCE
# Exécuté par un métro à sa position


# Si on doit être lancés, on vérifie d'abord si la voie est libre
execute if score @s remainingTicks matches ..0 run function tch:metro/rame/pre_station/test_arret_urgence

# Si on doit être lancés prochainement, on décrémente le temps d'attente restant
execute if score @s remainingTicks matches 1.. run scoreboard players remove @s remainingTicks 1

# On vérifie si on peut repartir
function tch:voie/reservation/test3

# Si on n'a plus le tag ArretUrgence, on peut repartir
execute as @s[tag=!ArretUrgence] run function tch:metro/rame/pre_station/depart

# Sinon, on attend à nouveau 6 secondes
execute as @s[tag=ArretUrgence] run scoreboard players set @s remainingTicks 7
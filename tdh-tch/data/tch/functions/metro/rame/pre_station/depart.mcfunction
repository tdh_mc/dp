# Redémarrage après arrêt d'urgence

# Si on est en pente, on remet un rail normal sous nous
execute as @s[tag=EnPente] if block ~ ~ ~ powered_rail[powered=false] run setblock ~ ~ ~ rail
tag @s remove EnPente

# On lance le métro dans la bonne direction
execute store result entity @s Motion[0] double 0.001 run scoreboard players get @s deltaX
execute store result entity @s Motion[1] double 0.001 run scoreboard players get @s deltaY
execute store result entity @s Motion[2] double 0.001 run scoreboard players get @s deltaZ

# DEBUG
tellraw @a[tag=MetroDebugLog,distance=..10] [{"text":"Métro en arrêt d'urgence relancé: ","color":"gray"},{"score":{"name":"@s","objective":"deltaX"},"color":"red"},{"text":"/"},{"score":{"name":"@s","objective":"deltaY"},"color":"red"},{"text":"/"},{"score":{"name":"@s","objective":"deltaZ"},"color":"red"},{"text":"."}]

# Si notre ligne est en fin de service, on se donne le tag Fin de service
function tch:tag/pc
execute if entity @e[type=item_frame,tag=ourPC,tag=FinDeService] run tag @s add FinDeService
execute as @s[tag=FinDeService] run tag @s add Terminus
tag @e[type=item_frame,tag=ourPC] remove ourPC

# On relance les sons d'arrivée du métro
function tch:metro/rame/sisve/pre_station/sons_arrivee_metro

# On supprime nos variables
scoreboard players reset @s deltaX
scoreboard players reset @s deltaY
scoreboard players reset @s deltaZ
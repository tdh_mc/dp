# Si on a une voie réservée, on la libère
execute as @s[tag=VoieReservee] run function tch:voie/reservation/depart
execute as @s[tag=VoieReservee] run function tch:station/pid/unlock_0_metro

# Si on est en file d'attente, on la libère également
execute as @s[tag=FileAttente] run function tch:voie/reservation/sortir_file_attente

# On se tue
scoreboard players reset @s
kill @s
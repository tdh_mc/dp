# TICK DU RÉSEAU MÉTRO (métros & spawners)
# Tick des spawners à métros
# On ne tick pas les spawners s'il n'y a pas de joueur proche
execute as @e[type=item_frame,tag=SpawnMetro] at @s run function tch:metro/spawner/tick

# Tick des métros eux-mêmes
execute as @e[type=minecart,tag=Metro] at @s run function tch:metro/rame/tick

# Check de présence d'entités sur des rails ;
# on tue les entités à proximité d'un joueur, si elles ne sont pas dans un minecart, si elles sont sur un rail
execute at @a as @e[type=#tch:electrocutable,distance=..80] at @s unless entity @e[type=minecart,distance=..1] if block ~ ~ ~ #tch:rail run function tch:metro/rail/electrocution_entite
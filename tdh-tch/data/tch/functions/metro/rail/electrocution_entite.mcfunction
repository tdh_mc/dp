# On exécute cette fonction en tant qu'une entité qui doit mourir d'électrocution

# On affiche les effets audio/visuels
function tch:metro/rail/electrocution

# On se déplace légèrement vers le haut
tp @s ~ ~.5 ~

# On se tue
execute at @s run function tdh:kill_lootless
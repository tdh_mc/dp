# On se trouve dans cette fonction si on est à la position d'un joueur se trouvant sur un rail,
# en gamemode adventure ou survival, et qu'on souhaite vérifier si on doit ou non se faire électrifier la gueule

# On donne un tag au joueur en question pour le retrouver
tag @p[distance=0,gamemode=!spectator,gamemode=!creative] add MortElectrocute

# Si le joueur est dans un minecart, il ne risque pas de décéder
execute if entity @e[type=minecart,distance=..1] run tag @p[distance=0,tag=MortElectrocute] remove MortElectrocute
# Si le joueur est immunisé à l'électrification, il ne risque pas de décéder
tag @p[distance=0,tag=ImmuniteElectricite] remove MortElectrocute

# On reset l'advancement du joueur (ainsi que de tous ceux situés exactement à la même position, qui ne peuvent être en principe que des spectateurs)
advancement revoke @a[distance=0] only tdh:bloc/rail

# Si le joueur a encore le tag, c'est qu'il doit décéder
execute at @p[distance=0,tag=MortElectrocute] run function tch:metro/rail/electrocution_joueur
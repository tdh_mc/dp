# On exécute cette fonction à la position d'un joueur qui doit mourir d'électrocution

# On affiche les effets audio/visuels
function tch:metro/rail/electrocution

# On affiche un message dans le chat
tellraw @a[distance=0,gamemode=!spectator] [{"text":"Vous êtes mort électrocuté.","color":"red"},{"text":" À l'avenir, veillez à ne jamais traverser les voies !","color":"gray"}]

# On se déplace vers le haut
tp @a[distance=0] ~ ~.5 ~

# On tue le joueur
kill @a[distance=..1,gamemode=!spectator,tag=MortElectrocute]
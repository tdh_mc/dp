# On affiche des particules
# Coup critique (gris clair)
particle crit ~ ~1 ~ 0.05 0.6 0.05 .25 100
# Électrocution (violet)
particle minecraft:dragon_breath ~ ~1 ~ 0.05 0.6 0.05 1 150 force
# Cendre qui retombe (noir)
particle squid_ink ~ ~1 ~ 0.15 0.6 0.15 .04 70
# "Explosion" de lumière (flash + explosion)
particle explosion ~ ~.25 ~ 0 0 0 20 1
particle flash ~ ~.5 ~ 0 0 0 1 4

# On joue le son d'électrocution
playsound tdh:br.electrocution block @a[distance=..30] ~ ~ ~ 2 1
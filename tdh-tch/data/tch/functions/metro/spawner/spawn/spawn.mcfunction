## SPAWN (après tous les tests)
# On invoque le métro
summon minecart ~ ~-1 ~ {Invulnerable:1b,Tags:["Metro","VoieReservee","EnStation","AllowSignals","TempSpawnedMetro"]}

# On enregistre l'ID de ligne, l'ID de station
scoreboard players operation @e[type=minecart,tag=TempSpawnedMetro,limit=1] idLine = @s idLine
scoreboard players operation @e[type=minecart,tag=TempSpawnedMetro,limit=1] idStation = @s idStation

# On tag notre PC de terminus pour pouvoir accéder à ses infos
function tch:tag/pc
function tch:tag/terminus_pc
# On copie les informations nécessaires du PC à notre métro
execute as @e[type=minecart,tag=TempSpawnedMetro,limit=1] at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:metro/spawner/spawn/nom_voiture

# On réserve la voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie] add VoieReservee


# On joue le message de PID
function tch:station/annonce/pid/gen_id
# On verrouille le nombre de minutes d'attente à 0
function tch:station/pid/lock_0
#execute as @e[tag=ourVoie,limit=1] at @s run function tch:metro/spawner/spawn/message_quai


# On supprime les tags temporaires
tag @e[type=minecart,tag=TempSpawnedMetro] remove TempSpawnedMetro
tag @e[type=item_frame,tag=ourPC] remove ourPC
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
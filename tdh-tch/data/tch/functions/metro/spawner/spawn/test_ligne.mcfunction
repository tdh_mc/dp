## TEST DE SPAWN PARTIE 2 :
## On vérifie si notre ligne est en service.

# On essaie de récupérer notre PC
function tch:tag/pc

# On supprime le tag de notre PC (et donc on ne spawne pas) si :
# - Il est en fin de service (tag FinDeService) (on vérifie au préalable si on est réellement en fin de service)
# - Il n'est pas ouvert (tag HorsService)
# - Il y a un incident en cours (tag IncidentEnCours et statut de minimum 1 (début de l'interruption trafic) et <4 (reprise progressive)
execute as @e[type=item_frame,tag=ourPC,tag=FinDeService] run function tch:ligne/test_debut_service
execute as @e[type=item_frame,tag=ourPC,tag=FinDeService] run tag @s remove ourPC
execute as @e[type=item_frame,tag=ourPC,tag=HorsService] run tag @s remove ourPC
execute as @e[type=item_frame,tag=ourPC,tag=IncidentEnCours,scores={status=1..3}] run tag @s remove ourPC

# Si notre PC a été trouvé, on exécute la suite des tests
execute if entity @e[type=item_frame,tag=ourPC,limit=1] run function tch:metro/spawner/spawn/test_voie

# Dans tous les cas, on réinitialise notre horaire de spawn pour en générer un nouveau
scoreboard players reset @s spawnAt

# On réinitialise le tag du PC
tag @e[type=item_frame,tag=ourPC] remove ourPC
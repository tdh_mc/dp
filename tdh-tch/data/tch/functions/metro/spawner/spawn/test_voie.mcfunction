## TEST DE SPAWN PARTIE 3 :
## On vérifie si notre voie est libre.

# Le tag temporaire "ourPC" est déjà enregistré par la partie 2 des tests (test_ligne).
# On essaie de récupérer notre voie (*Direction*)
function tch:tag/voie

# On supprime le tag de notre voie (et donc on ne spawne pas) SI :
# - Il y a des métros en file d'attente (score FileAttente > 0)
# - Il n'y a pas de joueur dans un rayon de 50 blocs
execute as @e[type=#tch:marqueur/quai,tag=ourVoie,scores={FileAttente=1..}] run tag @s remove ourVoie
execute as @e[type=#tch:marqueur/quai,tag=ourVoie] at @s unless entity @p[distance=..60] run tag @s remove ourVoie

# Si la voie est réservée, et qu'on trouve un métro ayant réservé de même idLine que nous à proximité de la voie, on se retire le tag
# On prend 120 blocs de distance pour être BIEN larges (si c'est un joueur qui arrive bientôt qui a pré-réservé),
# et on prend la distance autour du spawner plutôt que celle autour de la voie pour moins détecter les métros vraiment loin dans l'autre sens (déjà repartis de la station)
execute as @e[type=#tch:marqueur/quai,tag=ourVoie,tag=VoieReservee] at @e[type=minecart,tag=Metro,tag=VoieReservee,distance=..150] if score @e[type=minecart,distance=0,limit=1] idLine = @s idLine run tag @s remove ourVoie
# Sinon, on peut le garder : ça veut dire qu'on avait affaire à une réservation fantôme

# Si on a toujours notre voie, on fait spawner un métro
execute if entity @e[type=#tch:marqueur/quai,tag=ourVoie,limit=1] run function tch:metro/spawner/spawn/spawn

# On réinitialise le tag de notre voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie] remove ourVoie
# TICK DES SPAWNERS MÉTRO
# Cette fonction est exécutée régulièrement par chacun des spawners du métro
# pour générer leurs horaires de spawn et éventuellement leurs métros

# Si on a une heure de spawn prévue, on vérifie si c'est l'heure
execute as @s[scores={spawnAt=0..}] run function tch:metro/spawner/spawn/test_horaire

# Si on a pas d'heure de spawn prévue, on génère un nouvel horaire
execute unless score @s spawnAt matches 0.. run function tch:ligne/horaire/gen/test
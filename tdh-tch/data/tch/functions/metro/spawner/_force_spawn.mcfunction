# On exécute cette fonction pour forcer le spawn du spawner le plus proche
# Pour éviter de bousiller un métro déjà présent à quai, on teste uniquement la dernière étape (test voie)
# ce qui nous permet tout de même de réserver la voie de manière propre (ce qui n'arrive pas si on appelle
# juste la fonction spawn)
execute as @e[type=#tch:marqueur/spawner,tag=SpawnMetro,distance=..50,sort=nearest,limit=1] at @s run function tch:metro/spawner/spawn/test_voie
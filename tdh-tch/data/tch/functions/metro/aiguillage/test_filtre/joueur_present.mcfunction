# Les données de notre filtre sont déjà stockées dans des variables scoreboard :
# - #tchAiguillage mode = Notre mode de filtre (actuellement 1, "joueur présent")
# - #tchAiguillage status = La valeur recherchée (dans notre cas, 0 = pas de joueur et 1 = un joueur ou +)
# - #tchAiguillage idFleche = Le type de rail à définir si le filtre passe

# On vérifie s'il y a un joueur à proximité du métro (= notre position d'exécution)
execute if score #tchAiguillage status matches 0 unless entity @p[distance=..0.4] run tag @s add Succes
execute unless score #tchAiguillage status matches 0 if entity @p[distance=..0.4] run tag @s add Succes
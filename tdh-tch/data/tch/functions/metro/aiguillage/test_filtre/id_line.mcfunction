# Les données de notre filtre sont déjà stockées dans des variables scoreboard :
# - #tchAiguillage mode = Notre mode de filtre (actuellement 2, "ID de ligne")
# - #tchAiguillage status = La valeur recherchée (dans notre cas, l'ID de ligne en question)
# - #tchAiguillage idFleche = Le type de rail à définir si le filtre passe

# On vérifie si le métro le plus proche a le bon ID de ligne
execute if score @e[type=minecart,tag=MetroAiguillageTemp,sort=nearest,limit=1] idLine = #tchAiguillage status run tag @s add Succes
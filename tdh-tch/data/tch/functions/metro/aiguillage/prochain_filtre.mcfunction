# On passe au prochain filtre
data modify entity @s data.filtres append from entity @s data.filtres[0]
data remove entity @s data.filtres[0]

# On décrémente le nombre de filtres restants
scoreboard players remove #tchAiguillage nombre 1

# On teste ce nouveau filtre
function tch:metro/aiguillage/test_filtre
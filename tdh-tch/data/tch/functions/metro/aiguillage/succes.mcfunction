# On assigne le type de rail du filtre actuel (stocké dans #tchAiguillage idFleche)
# 6 modes de rail :
# - 1 = Nord/Sud
# - 2 = Est/Ouest
# - 3 = Nord/Est
# - 4 = Nord/Ouest
# - 5 = Sud/Est
# - 6 = Sud/Ouest

tellraw @a[tag=AiguillageDebugLog] [{"text":"[","color":"gold"},{"selector":"@s"},{"text":"] "},{"text":"Calcul réussi! Modification du rail...","color":"gray"}]

execute if score #tchAiguillage idFleche matches 1 run setblock ~ ~ ~ rail[shape=north_south]
execute if score #tchAiguillage idFleche matches 2 run setblock ~ ~ ~ rail[shape=east_west]
execute if score #tchAiguillage idFleche matches 3 run setblock ~ ~ ~ rail[shape=north_east]
execute if score #tchAiguillage idFleche matches 4 run setblock ~ ~ ~ rail[shape=north_west]
execute if score #tchAiguillage idFleche matches 5 run setblock ~ ~ ~ rail[shape=south_east]
execute if score #tchAiguillage idFleche matches 6 run setblock ~ ~ ~ rail[shape=south_west]

# On retire notre tag
tag @s remove Succes
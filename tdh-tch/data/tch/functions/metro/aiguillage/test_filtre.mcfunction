# Selon nos données, on setblock un rail différent à notre position

# Structure des données de l'entité :
#	{
#		data: {
#			filtres:[
#				{
#					mode: 1b,	<Mode 1 = Filtre selon la présence d'un joueur dans le métro>
#					valeur: 0,	<Mode 1, valeur 0 = Pas de joueur dans le métro>
#					rail: 1b	<Rail 1 = Nord/Sud>
#				},
#				{
#					mode: 2b,	<Mode 2 = Filtre selon l'ID de ligne>
#					valeur: 81,	<Mode 2 = L'ID de ligne concerné>
#					rail: 4b	<Rail 4 = Nord/Ouest>
#				}
#				<autant de filtres que nécessaire>
#			]
#		}
#	}


# On teste le premier filtre de la liste
execute store result score #tchAiguillage mode run data get entity @s data.filtres[0].mode
execute store result score #tchAiguillage status run data get entity @s data.filtres[0].valeur
execute store result score #tchAiguillage idFleche run data get entity @s data.filtres[0].rail

execute if score #tchAiguillage mode matches 1 run function tch:metro/aiguillage/test_filtre/joueur_present
execute if score #tchAiguillage mode matches 2 run function tch:metro/aiguillage/test_filtre/id_line
execute if score #tchAiguillage mode matches 3 run tag @s add Succes

# Si on n'a pas trouvé de filtre correspondant et qu'il en reste d'autres,
# on recommence la fonction
execute if entity @s[tag=!Succes] if score #tchAiguillage nombre matches 1.. run function tch:metro/aiguillage/prochain_filtre
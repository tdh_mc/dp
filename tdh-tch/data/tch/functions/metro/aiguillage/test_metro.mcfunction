# Selon nos données, on setblock un rail différent à notre position

# Structure des données de l'entité :
#	{
#		data: {
#			filtres:[
#				{
#					mode: 1b,	<Mode 1 = Filtre selon la présence d'un joueur dans le métro>
#					valeur: 0,	<Mode 1, valeur 0 = Pas de joueur dans le métro>
#					rail: 1b	<Rail 1 = Nord/Sud>
#				},
#				{
#					mode: 2b,	<Mode 2 = Filtre selon l'ID de ligne>
#					valeur: 81,	<Mode 2 = L'ID de ligne concerné>
#					rail: 4b	<Rail 4 = Nord/Ouest>
#				}
#				<autant de filtres que nécessaire>
#			]
#		}
#	}

# 3 modes de filtre :
# - Mode 1 = présence d'un joueur dans le métro
# - Mode 2 = filtre selon l'ID de ligne
# - Mode 3 = non filtré (valeur "fallback" si toutes les précédentes échouent)
# 6 modes de rail :
# - 1 = Nord/Sud
# - 2 = Est/Ouest
# - 3 = Nord/Est
# - 4 = Nord/Ouest
# - 5 = Sud/Est
# - 6 = Sud/Ouest

# On stocke le nombre de filtres paramétrés
execute store result score #tchAiguillage nombre run data get entity @s data.filtres

tellraw @a[tag=AiguillageDebugLog] [{"text":"[","color":"gold"},{"selector":"@s"},{"text":"] "},{"text":"Calcul d'aiguillage du métro ","color":"gray","extra":[{"selector":"@e[type=minecart,tag=MetroAiguillageTemp,sort=nearest,limit=1]"},{"text":" : "},{"score":{"name":"#tchAiguillage","objective":"nombre"}},{"text":" filtres trouvés."}]}]

# S'il y a au moins 1 filtre, on vérifie si le métro passe sa condition
# On vérifiera aussi tous les suivants de manière récursive
execute if score #tchAiguillage nombre matches 1.. run function tch:metro/aiguillage/test_filtre

# Si on n'a pas trouvé de filtre, on écrit un message d'erreur
execute if entity @s[tag=!Succes] run tellraw @a[tag=AiguillageDebugLog] [{"text":"[","color":"red"},{"selector":"@s"},{"text":"] "},{"text":"Aucun filtre valide trouvé pour le métro ","color":"gray","extra":[{"selector":"@e[type=minecart,tag=MetroAiguillageTemp,sort=nearest,limit=1]"},{"text":"."}]}]
# Si on a trouvé un filtre valable correspondant à notre situation, on modifie le rail à notre position
execute at @s[tag=Succes] run function tch:metro/aiguillage/succes

# Qu'on aie trouvé ou non un filtre valable, si on n'a pas terminé de faire défiler toutes les données de la liste, on les remet dans leur position "d'origine"
execute if score #tchAiguillage nombre matches 1.. run function tch:metro/aiguillage/prochain_filtre_rec
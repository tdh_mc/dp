# Configurateur automatique d'aiguillage
# Permet de choisir les paramètres de l'aiguillage le plus proche
# Exécuté par un administrateur TCH

# On démarre la config et se donne donc le tag Config et ConfigAiguillage
tag @s add Config
tag @s add ConfigAiguillage

# Contrairement au configurateur de ligne, on gère toujours les options de l'aiguillage le plus proche
# En premier lieu, on le tag
tag @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] add ourAiguillage
# On affiche des particules autour du marqueur pour le "matérialiser"
# et être immédiatement sûrs visuellement qu'on a sélectionné le bon
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle happy_villager ~ ~1 ~ 0.1 0.5 0.1 1 10 force @s
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle block_marker detector_rail ~ ~0.5 ~ 0 0 0 1 1 force @s

# On enregistre l'ID line de l'aiguillage
scoreboard players operation @s idLine = @e[type=marker,tag=ourAiguillage,limit=1] idLine



# On active l'objectif de configuration à trigger (9999 = Choix d'un paramètre à modifier)
scoreboard players set @s configID 9999
scoreboard players set @s config 0
scoreboard players enable @s config

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TCH : AIGUILLAGE ","color":"gold"},{"selector":"@e[type=marker,tag=ourAiguillage]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le paramètre à modifier :","color":"gray"}]

# Affichage de chacun des filtres (jusqu'à 6 maximum puisqu'on a 6 formes de rail possibles au total.)
# Et puis 6 options à un aiguillage ça fait déjà beaucoup non ??
tellraw @s [{"text":"Cet aiguillage concerne les métros d'ID ","color":"gray"},{"score":{"name":"@e[type=marker,tag=ourAiguillage,limit=1]","objective":"idLine"},"color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 15000"},"hoverEvent":{"action":"show_text","contents":[{"text":"Modifier l'ID de ligne minimal."}]}},{"text":" à "},{"score":{"name":"@e[type=marker,tag=ourAiguillage,limit=1]","objective":"idLineMax"},"color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 15100"},"hoverEvent":{"action":"show_text","contents":[{"text":"Modifier l'ID de ligne maximal."}]}},{"text":"."}]
execute if data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[0] run function tch:metro/aiguillage/config/display/filtre_0
execute if data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[1] run function tch:metro/aiguillage/config/display/filtre_1
execute if data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[2] run function tch:metro/aiguillage/config/display/filtre_2
execute if data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[3] run function tch:metro/aiguillage/config/display/filtre_3
execute if data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[4] run function tch:metro/aiguillage/config/display/filtre_4
execute if data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[5] run function tch:metro/aiguillage/config/display/filtre_5
execute unless data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[5] run tellraw @s [{"text":"Ajouter un filtre...","color":"gold","italic":"true","clickEvent":{"action":"run_command","value":"/trigger config set 14550"},"hoverEvent":{"action":"show_text","contents":[{"text":"Ajouter un filtre supplémentaire à la fin de la liste."}]}}]

tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"- Quitter le configurateur","color":"gray","clickEvent":{"action":"run_command","value":"/trigger config set -2"}}]


# Si on n'a pas trouvé d'aiguillage, on annule
execute unless entity @e[type=marker,tag=ourAiguillage,limit=1] run function tdh:config/end
execute unless entity @e[type=marker,tag=ourAiguillage,limit=1] run tellraw @s [{"text":"Impossible de trouver un aiguillage proche.","color":"red"}]


# Réinitialisation des tags et variables
tag @e[type=marker,tag=ourAiguillage] remove ourAiguillage
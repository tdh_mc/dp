## SÉLECTION DU TYPE DE FILTRE

# On active l'objectif de configuration à trigger
scoreboard players set @s configID 10000
scoreboard players set @s config -1
scoreboard players enable @s config

# On affiche ensuite l'écran de sélection (générique pour tous les types de filtre)
function tch:metro/aiguillage/config/type_filtre_select

# En premier lieu, on tag notre aiguillage
tag @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] add ourAiguillage
# On affiche des particules autour du marqueur pour le "matérialiser"
# et être immédiatement sûrs visuellement qu'on a sélectionné le bon
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle happy_villager ~ ~1 ~ 0.1 0.5 0.1 1 10 force @s
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle block_marker detector_rail ~ ~0.5 ~ 0 0 0 1 1 force @s

# On demande de choisir le type de filtre
tellraw @s [{"text":"CONFIGURATEUR TCH : AIGUILLAGE ","color":"gold"},{"selector":"@e[type=marker,tag=ourAiguillage,limit=1]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le type de rail :","color":"gray"}]
tellraw @s [{"text":"","color":"gray"},{"text":"Nord/Sud","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 1"},"hoverEvent":{"action":"show_text","contents":[{"text":"Rail droit orienté Nord/Sud."}]}},{"text":" ● "},{"text":"Est/Ouest","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 2"},"hoverEvent":{"action":"show_text","contents":[{"text":"Rail droit orienté Est/Ouest"}]}},{"text":" ● "},{"text":"Nord/Est","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 3"},"hoverEvent":{"action":"show_text","contents":[{"text":"Rail diagonal orienté Nord/Est."}]}},{"text":" ● "},{"text":"Nord/Ouest","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 4"},"hoverEvent":{"action":"show_text","contents":[{"text":"Rail diagonal orienté Nord/Ouest"}]}},{"text":" ● "},{"text":"Sud/Est","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 5"},"hoverEvent":{"action":"show_text","contents":[{"text":"Rail diagonal orienté Sud/Est."}]}},{"text":" ● "},{"text":"Sud/Ouest","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 6"},"hoverEvent":{"action":"show_text","contents":[{"text":"Rail diagonal orienté Sud/Ouest"}]}}]
tellraw @s [{"text":"Conserver le réglage actuel","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}}]

# On nettoie le tag temporaire
tag @e[type=marker,tag=ourAiguillage] remove ourAiguillage
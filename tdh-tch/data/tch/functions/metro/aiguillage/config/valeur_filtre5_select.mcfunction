## SÉLECTION DE LA VALEUR DU FILTRE

# On active l'objectif de configuration à trigger
scoreboard players set @s configID 11500
scoreboard players set @s config -1
scoreboard players enable @s config

# On affiche ensuite l'écran de sélection (générique pour tous les types de filtre)
function tch:metro/aiguillage/config/valeur_filtre_select
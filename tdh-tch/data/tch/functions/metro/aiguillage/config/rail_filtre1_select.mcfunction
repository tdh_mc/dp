## SÉLECTION DU TYPE DE RAIL

# On active l'objectif de configuration à trigger
scoreboard players set @s configID 12100
scoreboard players set @s config -1
scoreboard players enable @s config

# On affiche ensuite l'écran de sélection (générique pour tous les types de filtre)
function tch:metro/aiguillage/config/rail_filtre_select
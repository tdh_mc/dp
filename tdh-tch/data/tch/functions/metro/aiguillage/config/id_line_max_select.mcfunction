## SÉLECTION DE L'ID DE LIGNE MAXIMUM

# On active l'objectif de configuration à trigger
scoreboard players set @s configID 15100
scoreboard players set @s config -1
scoreboard players enable @s config

# En premier lieu, on tag notre aiguillage
tag @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] add ourAiguillage
# On affiche des particules autour du marqueur pour le "matérialiser"
# et être immédiatement sûrs visuellement qu'on a sélectionné le bon
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle happy_villager ~ ~1 ~ 0.1 0.5 0.1 1 10 force @s
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle block_marker detector_rail ~ ~0.5 ~ 0 0 0 1 1 force @s

# On demande de choisir le type de filtre
tellraw @s [{"text":"CONFIGURATEUR TCH : AIGUILLAGE ","color":"gold"},{"selector":"@e[type=marker,tag=ourAiguillage,limit=1]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez l'ID de ligne maximal :","color":"gray"}]
tellraw @s [{"text":"Entrer une valeur","color":"#ff7738","hoverEvent":{"action":"show_text","contents":[{"text":"N'importe quelle valeur supérieure à ","color":"gray"},{"text":"0","color":"gold","bold":"true"},{"text":" est autorisée, et détermine quels ID de ligne/direction déclenchent cet aiguillage."}]},"clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"score":{"name":"@e[type=marker,tag=ourAiguillage,limit=1]","objective":"idLineMax"}},{"text":")"}]

# On nettoie le tag temporaire
tag @e[type=marker,tag=ourAiguillage] remove ourAiguillage
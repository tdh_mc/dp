# Cette fonction est exécutée par un administrateur
# avec configID = 9999 (menu principal) et config = le menu qu'il veut afficher

# Selon le menu qu'on veut afficher
# - Type de filtre pour chacun des 6 filtres (10000/10100/10200/10300/10400/10500)
execute at @s[scores={config=10000}] run function tch:metro/aiguillage/config/type_filtre0_select
execute at @s[scores={config=10100}] run function tch:metro/aiguillage/config/type_filtre1_select
execute at @s[scores={config=10200}] run function tch:metro/aiguillage/config/type_filtre2_select
execute at @s[scores={config=10300}] run function tch:metro/aiguillage/config/type_filtre3_select
execute at @s[scores={config=10400}] run function tch:metro/aiguillage/config/type_filtre4_select
execute at @s[scores={config=10500}] run function tch:metro/aiguillage/config/type_filtre5_select
# - Valeur du filtre pour chacun des 6 filtres (11000/11100/11200/11300/11400/11500)
execute at @s[scores={config=11000}] run function tch:metro/aiguillage/config/valeur_filtre0_select
execute at @s[scores={config=11100}] run function tch:metro/aiguillage/config/valeur_filtre1_select
execute at @s[scores={config=11200}] run function tch:metro/aiguillage/config/valeur_filtre2_select
execute at @s[scores={config=11300}] run function tch:metro/aiguillage/config/valeur_filtre3_select
execute at @s[scores={config=11400}] run function tch:metro/aiguillage/config/valeur_filtre4_select
execute at @s[scores={config=11500}] run function tch:metro/aiguillage/config/valeur_filtre5_select
# - Type de rail pour chacun des 6 filtres (12000/12100/12200/12300/12400/12500)
execute at @s[scores={config=12000}] run function tch:metro/aiguillage/config/rail_filtre0_select
execute at @s[scores={config=12100}] run function tch:metro/aiguillage/config/rail_filtre1_select
execute at @s[scores={config=12200}] run function tch:metro/aiguillage/config/rail_filtre2_select
execute at @s[scores={config=12300}] run function tch:metro/aiguillage/config/rail_filtre3_select
execute at @s[scores={config=12400}] run function tch:metro/aiguillage/config/rail_filtre4_select
execute at @s[scores={config=12500}] run function tch:metro/aiguillage/config/rail_filtre5_select
# - Supprimer un des 6 filtres (14000/14100/14200/14300/14400/14500)
execute at @s[scores={config=14000}] run function tch:metro/aiguillage/config/filtre0_delete
execute at @s[scores={config=14100}] run function tch:metro/aiguillage/config/filtre1_delete
execute at @s[scores={config=14200}] run function tch:metro/aiguillage/config/filtre2_delete
execute at @s[scores={config=14300}] run function tch:metro/aiguillage/config/filtre3_delete
execute at @s[scores={config=14400}] run function tch:metro/aiguillage/config/filtre4_delete
execute at @s[scores={config=14500}] run function tch:metro/aiguillage/config/filtre5_delete
# - Ajouter un filtre (14550)
execute at @s[scores={config=14550}] run function tch:metro/aiguillage/config/filtre_create
# - Minimum et maximum d'ID de ligne à détecter (15000/15100)
execute at @s[scores={config=15000}] run function tch:metro/aiguillage/config/id_line_min_select
execute at @s[scores={config=15100}] run function tch:metro/aiguillage/config/id_line_max_select

# - Quitter le configurateur (-2)
execute at @s[scores={config=-2}] run function tdh:config/end
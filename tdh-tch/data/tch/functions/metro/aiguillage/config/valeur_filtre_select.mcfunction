
# En premier lieu, on tag notre aiguillage
tag @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] add ourAiguillage
# On affiche des particules autour du marqueur pour le "matérialiser"
# et être immédiatement sûrs visuellement qu'on a sélectionné le bon
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle happy_villager ~ ~1 ~ 0.1 0.5 0.1 1 10 force @s
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle block_marker detector_rail ~ ~0.5 ~ 0 0 0 1 1 force @s

# On demande de choisir le type de filtre
tellraw @s [{"text":"CONFIGURATEUR TCH : AIGUILLAGE ","color":"gold"},{"selector":"@e[type=marker,tag=ourAiguillage,limit=1]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez la valeur du filtre :","color":"gray"}]
tellraw @s [{"text":"Entrer une valeur","color":"#ff7738","hoverEvent":{"action":"show_text","contents":[{"text":"Avec le type de filtre ","color":"gray"},{"text":"Plein/Vide","color":"gold","bold":"true"},{"text":", il n'y a que deux valeurs possibles : "},{"text":"0","color":"gold","bold":"true"},{"text":" (pas de joueur à bord) et "},{"text":"1","color":"gold"},{"text":" (un ou plusieurs joueurs à bord).\n\nAvec le type de filtre "},{"text":"ID de ligne","color":"yellow","bold":"true"},{"text":", n'importe quelle valeur supérieure à "},{"text":"0","color":"yellow","bold":"true"},{"text":" est autorisée, et détermine quel ID de direction permet à ce filtre de passer."}]},"clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}}]

# On nettoie le tag temporaire
tag @e[type=marker,tag=ourAiguillage] remove ourAiguillage
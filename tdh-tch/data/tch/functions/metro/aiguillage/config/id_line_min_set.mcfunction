## ENREGISTREMENT DE L'ID DE LIGNE MINIMAL
# Il est indiqué par la variable config

# En premier lieu, on tag notre aiguillage
tag @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] add ourAiguillage
# On affiche des particules autour du marqueur pour le "matérialiser"
# et être immédiatement sûrs visuellement qu'on a sélectionné le bon
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle happy_villager ~ ~1 ~ 0.1 0.5 0.1 1 10 force @s
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle block_marker detector_rail ~ ~0.5 ~ 0 0 0 1 1 force @s

# On enregistre la valeur du filtre dans les données de l'aiguillage
scoreboard players operation @e[type=marker,tag=ourAiguillage,limit=1] idLine = @s config

# On nettoie le tag temporaire
tag @e[type=marker,tag=ourAiguillage] remove ourAiguillage
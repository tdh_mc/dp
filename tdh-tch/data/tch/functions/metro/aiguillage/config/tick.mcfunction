# On tick régulièrement pour lancer les sous-fonctions de prise en compte de la configuration
# Le joueur a une variable configID qui sert à stocker la variable qu'il config, et une variable config qu'il trigger pour communiquer la valeur désirée

# Cette fonction est à destination des administrateurs, donc on peut utiliser execute AS au lieu de AT

# Si on est à l'écran d'accueil, on lance une des fonctions select selon notre choix
execute at @s[scores={configID=9999}] unless score @s config matches 0 run function tch:metro/aiguillage/config/select

# Selon la variable qu'on cherche à config
# - Dans tous les cas (10000+), si on veut revenir au menu (config -120047):
execute at @s[scores={configID=10000..,config=-120047}] run function tch:metro/aiguillage/config
# - Type de filtre pour chacun des 6 filtres (10000/10100/10200/10300/10400/10500)
execute at @s[scores={configID=10000}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/type_filtre0_set
execute at @s[scores={configID=10100}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/type_filtre1_set
execute at @s[scores={configID=10200}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/type_filtre2_set
execute at @s[scores={configID=10300}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/type_filtre3_set
execute at @s[scores={configID=10400}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/type_filtre4_set
execute at @s[scores={configID=10500}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/type_filtre5_set
# - Valeur du filtre pour chacun des 6 filtres (11000/11100/11200/11300/11400/11500)
execute at @s[scores={configID=11000}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/valeur_filtre0_set
execute at @s[scores={configID=11100}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/valeur_filtre1_set
execute at @s[scores={configID=11200}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/valeur_filtre2_set
execute at @s[scores={configID=11300}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/valeur_filtre3_set
execute at @s[scores={configID=11400}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/valeur_filtre4_set
execute at @s[scores={configID=11500}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/valeur_filtre5_set
# - Type de rail pour chacun des 6 filtres (12000/12100/12200/12300/12400/12500)
execute at @s[scores={configID=12000}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/rail_filtre0_set
execute at @s[scores={configID=12100}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/rail_filtre1_set
execute at @s[scores={configID=12200}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/rail_filtre2_set
execute at @s[scores={configID=12300}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/rail_filtre3_set
execute at @s[scores={configID=12400}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/rail_filtre4_set
execute at @s[scores={configID=12500}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/rail_filtre5_set
# - Minimum et maximum d'ID de ligne à détecter (15000/15100)
execute at @s[scores={configID=15000}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/id_line_min_set
execute at @s[scores={configID=15100}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config/id_line_max_set


# Tous les joueurs qui viennent de choisir une config retournent au menu principal
execute at @s[scores={configID=10000..}] unless score @s config matches ..-1 run function tch:metro/aiguillage/config
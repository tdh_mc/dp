# En premier lieu, on tag notre aiguillage
tag @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] add ourAiguillage

# On ajoute un nouveau filtre s'il n'y en a pas déjà 6
execute if data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[5] run tellraw @s [{"text":"Impossible d'ajouter un filtre supplémentaire : le maximum a été atteint.","color":"red"}]
execute unless data entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[5] run data modify entity @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] data.filtres append value {mode:3b,valeur:0,rail:0b}

# On retire le tag de notre aiguillage
tag @e[type=marker,tag=ourAiguillage] remove ourAiguillage

# On relance la fonction principale
tellraw @s [{"text":"Filtre supprimé avec succès!","color":"gray"}]
function tch:metro/aiguillage/config
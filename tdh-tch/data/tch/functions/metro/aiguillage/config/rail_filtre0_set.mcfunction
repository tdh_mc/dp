## ENREGISTREMENT DU TYPE DE RAIL
# Il est indiqué par la variable config

# En premier lieu, on tag notre aiguillage
tag @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] add ourAiguillage
# On affiche des particules autour du marqueur pour le "matérialiser"
# et être immédiatement sûrs visuellement qu'on a sélectionné le bon
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle happy_villager ~ ~1 ~ 0.1 0.5 0.1 1 10 force @s
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle block_marker detector_rail ~ ~0.5 ~ 0 0 0 1 1 force @s

# On enregistre la valeur du filtre dans les données de l'aiguillage
execute if score @s config matches ..0 run data modify entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[0].type set value 1b
execute if score @s config matches 1..6 store result entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[0].rail byte 1 run scoreboard players get @s config
execute if score @s config matches 7.. run data modify entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[0].type set value 6b

# On nettoie le tag temporaire
tag @e[type=marker,tag=ourAiguillage] remove ourAiguillage
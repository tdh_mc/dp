
# En premier lieu, on tag notre aiguillage
tag @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] add ourAiguillage
# On affiche des particules autour du marqueur pour le "matérialiser"
# et être immédiatement sûrs visuellement qu'on a sélectionné le bon
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle happy_villager ~ ~1 ~ 0.1 0.5 0.1 1 10 force @s
execute at @e[type=marker,tag=ourAiguillage,limit=1] run particle block_marker detector_rail ~ ~0.5 ~ 0 0 0 1 1 force @s

# On demande de choisir le type de filtre
tellraw @s [{"text":"CONFIGURATEUR TCH : AIGUILLAGE ","color":"gold"},{"selector":"@e[type=marker,tag=ourAiguillage,limit=1]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le type de filtre :","color":"gray"}]
tellraw @s [{"text":"","color":"gray"},{"text":"Plein/Vide","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 1"},"hoverEvent":{"action":"show_text","contents":[{"text":"Filtrer selon la présence ou non d'un joueur à bord du métro."}]}},{"text":" ● "},{"text":"ID de ligne","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 2"},"hoverEvent":{"action":"show_text","contents":[{"text":"Filtrer selon l'ID de direction du métro."}]}},{"text":" ● "},{"text":"Défaut","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 3"},"hoverEvent":{"action":"show_text","contents":[{"text":"Pas de filtre : sera toujours sélectionné en cas d'échec des filtres précédents."}]}}]
tellraw @s [{"text":"Conserver le réglage actuel","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}}]

# On nettoie le tag temporaire
tag @e[type=marker,tag=ourAiguillage] remove ourAiguillage
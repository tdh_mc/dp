# On exécute cette fonction à la position d'un panneau TexteTemporaire
data modify storage tch:itineraire terminus.station set from entity @e[type=item_frame,tag=ourTerminusPC,limit=1] Item.tag.display.station.nom
data modify block ~ ~ ~ Text1 set value '[{"entity":"@e[type=item_frame,tag=ourTerminusPC,limit=1]","nbt":"Item.tag.display.line.nom","interpret":"true"},{"text":" → ","color":"gray"},{"entity":"@e[type=item_frame,tag=ourTerminusPC,limit=1]","nbt":"Item.tag.display.line.terminus","interpret":"true"}]'
data modify storage tch:aiguillage filtre.texte set from block ~ ~ ~ Text1
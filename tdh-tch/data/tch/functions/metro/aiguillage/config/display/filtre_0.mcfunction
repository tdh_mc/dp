# On affiche une ligne correspondant au 1er filtre

# On génère d'abord les morceaux de phrase requis
execute store result score #tchAiguillage mode run data get entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[0].mode
execute store result score #tchAiguillage status run data get entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[0].valeur
execute store result score #tchAiguillage idFleche run data get entity @e[type=marker,tag=ourAiguillage,limit=1] data.filtres[0].rail

execute if score #tchAiguillage mode matches 1 run function tch:metro/aiguillage/config/display/store/filtre_joueurs
execute if score #tchAiguillage mode matches 2 run function tch:metro/aiguillage/config/display/store/filtre_id_ligne
execute if score #tchAiguillage mode matches 3 run function tch:metro/aiguillage/config/display/store/filtre_sans_filtre
function tch:metro/aiguillage/config/display/store/type_rail

tellraw @s [{"text":"","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Modifier les paramètres du filtre n°0."}]},"clickEvent":{"action":"run_command","value":"/trigger config set 10000"}},{"text":"[X]","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set 14000"},"hoverEvent":{"action":"show_text","contents":[{"text":"Supprimer le filtre n°0."}]}},{"text":" Filtre "},{"text":"0","bold":"true"},{"text":" : "},{"storage":"tch:aiguillage","nbt":"filtre.texte","interpret":"true"},{"text":" ["},{"storage":"tch:aiguillage","nbt":"rail.texte","interpret":"true"},{"text":"]"}]
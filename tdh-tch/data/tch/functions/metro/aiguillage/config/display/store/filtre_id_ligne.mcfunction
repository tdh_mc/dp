# On récupère le PC de terminus correspondant
scoreboard players operation #tch idTerminus = #tchAiguillage status
execute as @e[type=item_frame,tag=tchQuaiPC] if score #tch idTerminus = @s idTerminus run tag @s add ourTerminusPC

# Si on a trouvé un PC, on affiche son nom + ID de terminus
execute if entity @e[type=item_frame,tag=ourTerminusPC,limit=1] at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:metro/aiguillage/config/display/store/filtre_id_ligne/generer_texte
execute unless entity @e[type=item_frame,tag=ourTerminusPC,limit=1] run data modify storage tch:aiguillage filtre.texte set value '[{"text":"Ligne non trouvée!","color":"red"}]'

# On retire le tag temporaire du PC
tag @e[type=item_frame,tag=ourTerminusPC] remove ourTerminusPC
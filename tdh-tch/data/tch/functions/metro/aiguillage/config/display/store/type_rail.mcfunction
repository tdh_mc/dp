
# 6 modes de rail stockés dans #tchAiguillage idFleche
# - 1 = Nord/Sud
# - 2 = Est/Ouest
# - 3 = Nord/Est
# - 4 = Nord/Ouest
# - 5 = Sud/Est
# - 6 = Sud/Ouest

execute if score #tchAiguillage idFleche matches 1 run data modify storage tch:aiguillage rail.texte set value '[{"text":"Nord/Sud","color":"yellow"}]'
execute if score #tchAiguillage idFleche matches 2 run data modify storage tch:aiguillage rail.texte set value '[{"text":"Est/Ouest","color":"yellow"}]'
execute if score #tchAiguillage idFleche matches 3 run data modify storage tch:aiguillage rail.texte set value '[{"text":"Nord/Est","color":"yellow"}]'
execute if score #tchAiguillage idFleche matches 4 run data modify storage tch:aiguillage rail.texte set value '[{"text":"Nord/Ouest","color":"yellow"}]'
execute if score #tchAiguillage idFleche matches 5 run data modify storage tch:aiguillage rail.texte set value '[{"text":"Sud/Est","color":"yellow"}]'
execute if score #tchAiguillage idFleche matches 6 run data modify storage tch:aiguillage rail.texte set value '[{"text":"Sud/Ouest","color":"yellow"}]'
execute unless score #tchAiguillage idFleche matches 1..6 run data modify storage tch:aiguillage rail.texte set value '[{"text":"Type de rail inconnu!","color":"red"}]'
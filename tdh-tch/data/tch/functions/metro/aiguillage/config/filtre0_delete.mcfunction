# On supprime le filtre sélectionné
data remove entity @e[type=marker,tag=tchAiguillage,distance=..200,sort=nearest,limit=1] data.filtres[0]

# On relance la fonction principale
tellraw @s [{"text":"Filtre supprimé avec succès!","color":"gray"}]
function tch:metro/aiguillage/config
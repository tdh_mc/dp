# On exécute régulièrement ce tick en tant que chaque marqueur "tchAiguillage",
# pour détecter les métros à proximité et modifier le rail à notre position

# On tag les métros proches qui correspondent à nos ID de ligne
scoreboard players operation #tchAiguillage idLine = @s idLine
scoreboard players operation #tchAiguillage idLineMax = @s idLineMax
execute as @e[type=minecart,tag=Metro,distance=..50] if score @s idLine >= #tchAiguillage idLine if score @s idLine <= #tchAiguillage idLineMax run tag @s add MetroAiguillageTemp

# On exécute le test d'aiguillage avec le plus proche de ces métros
execute at @e[type=minecart,tag=MetroAiguillageTemp,sort=nearest,limit=1] run function tch:metro/aiguillage/test_metro

# On retire le tag temporaire des métros
tag @e[type=minecart,tag=MetroAiguillageTemp] remove MetroAiguillageTemp
# On passe au prochain filtre
data modify entity @s data.filtres append from entity @s data.filtres[0]
data remove entity @s data.filtres[0]

# On décrémente le nombre de filtres restants
scoreboard players remove #tchAiguillage nombre 1

# S'il reste des filtres, on recommence
execute if score #tchAiguillage nombre matches 1.. run function tch:metro/aiguillage/prochain_filtre_rec
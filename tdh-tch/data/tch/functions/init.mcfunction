scoreboard objectives add spawnAt dummy
scoreboard objectives add totalStillTime dummy "Temps total d'immobilité"
scoreboard objectives add launchDirection dummy "Direction de lancement"
scoreboard objectives add idStation dummy "TCH – Identifiant station"
scoreboard objectives add idLine dummy "TCH – Identifiant ligne"
scoreboard objectives add idLineMax dummy "TCH – Identifiant max de ligne"
scoreboard objectives add idTerminus dummy "Identifiant de terminus"
scoreboard objectives add idTerminusMax dummy "Identifiant max de terminus"
scoreboard objectives add idVehicule dummy "TCH – Identifiant de véhicule"
scoreboard objectives add idVoiture dummy "TCH – Identifiant de voiture"
scoreboard objectives add idMarqueur dummy "TCH – Identifiant de marqueur"
scoreboard objectives add idCamera dummy "Identifiant de caméra"
scoreboard objectives add idAbonnement dummy "TCH – Identifiant d'abonnement"
scoreboard objectives add niveauAcces dummy "Niveau d’accès"
scoreboard objectives add tickValidation dummy "Tick de validation"

scoreboard objectives add incident dummy "Code incident"
scoreboard objectives add debutIncident dummy "Heure de début d'incident"
scoreboard objectives add finIncident dummy "Heure de fin d'incident"
scoreboard objectives add chancesIncident dummy "Probabilité d'incident"
scoreboard objectives add debutService dummy "Heure de début de service"
scoreboard objectives add finService dummy "Heure de fin de service"

# Pour les intersections
scoreboard objectives add currentState dummy "État actuel"

# Si on ne trouve pas d'item frame TexteTemporaire,
# on affiche un message d'erreur

execute unless entity @e[type=item_frame,tag=TexteTemporaire,limit=1] run tellraw @a [{"text":"[ERREUR] : Aucune item frame 'TexteTemporaire' n'a été trouvée dans le monde.\n","color":"red"},{"text":"Merci de vous placer ","color":"gray"},{"text":"au spawn","bold":"true"},{"text":" (dans un chunk toujours chargé),\ndans un bloc ","color":"gray"},{"text":"caché","bold":"true"},{"text":" et ","color":"gray"},{"text":"contenant de l'air","bold":"true"},{"text":", puis ","color":"gray"},{"text":"cliquez ici.","color":"gold","hoverEvent":{"action":"show_text","value":"Oui, juste ici."},"clickEvent":{"action":"run_command","value":"/function tch:init/gen_panneau"}}]

# Suppression de variables obsolètes des précédentes versions du pack
scoreboard objectives remove lastAbo
scoreboard objectives remove lastSpawn
scoreboard objectives remove doAutoSpawn
scoreboard objectives remove spawnLocked
scoreboard objectives remove incidentEnCours
scoreboard objectives remove randomHelper
scoreboard objectives remove hasPassenger
scoreboard objectives remove number60
scoreboard objectives remove ticksPerSecond

scoreboard objectives remove annonceVoice
scoreboard objectives remove annonceBuf1
scoreboard objectives remove annonceBuf2
scoreboard objectives remove annonceBuf3
scoreboard objectives remove annonceBuf4
scoreboard objectives remove annonceBuf5
scoreboard objectives remove annonceBuf6
scoreboard objectives remove annonceBuf7
scoreboard objectives remove annonceBuf8
scoreboard objectives remove annonceBuf9
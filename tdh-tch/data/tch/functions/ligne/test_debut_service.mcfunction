# Exécuté par une ligne pour tester si elle peut débuter son service

# Si le service va commencer dans 1h ou moins et qu'il devait commencer il y a 4h ou moins, on commence le service
scoreboard players operation @s temp9 = @s debutServiceAjd
scoreboard players operation @s temp9 -= #temps currentTdhTick
execute if score @s temp9 matches -8000..2000 run tag @s remove FinDeService
scoreboard players reset @s temp9


# Si on vient de démarrer le service, on calcule l'heure de fin de service
execute as @s[tag=!FinDeService] run function tch:ligne/calcul_fin_service
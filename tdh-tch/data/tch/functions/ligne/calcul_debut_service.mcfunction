## CALCUL DES HORAIRES (DÉBUT DE SERVICE)
# Cette fonction est exécutée par un PC à la fin du service pour calculer l'heure de début de service

# Par défaut, on prend l'horaire standard
scoreboard players operation @s debutServiceAjd = @s debutService

# On calcule 4h et 14h selon la durée actuelle du jour
scoreboard players set #ligne temp 4
scoreboard players set #ligne temp2 14
scoreboard players operation #ligne temp *= #temps ticksPerHour
scoreboard players operation #ligne temp2 *= #temps ticksPerHour

# Si on est samedi ou dimanche, on ajoute aux horaires de début de service les modificateurs nécessaires
# Il y a 2 conditions, une si on est avant minuit (on génère les horaires de demain) et l'autre après minuit (ceux d'ajd)
execute if score #temps currentTdhTick >= #ligne temp if score #temps jourSemaine matches 5..6 run scoreboard players operation @s debutServiceAjd += @s debutServiceWe
execute if score #temps currentTdhTick < #ligne temp if score #temps jourSemaine matches 6..7 run scoreboard players operation @s debutServiceAjd += @s debutServiceWe

# Le début de service doit être compris entre 4h et 14h
execute if score @s debutServiceAjd < #ligne temp run scoreboard players operation @s debutServiceAjd = #ligne temp
execute if score @s debutServiceAjd > #ligne temp2 run scoreboard players operation @s debutServiceAjd = #ligne temp2
# Paramètres des PC lignes
scoreboard objectives add debutService dummy "Heure de début de service"
scoreboard objectives add debutServiceAjd dummy "Heure de début de service aujourd'hui"
scoreboard objectives add debutServiceWe dummy "Modificateur d'heure de début de service le week-end"
scoreboard objectives add finService dummy "Heure de fin de service"
scoreboard objectives add finServiceAjd dummy "Heure de fin de service aujourd'hui"
scoreboard objectives add finServiceWe dummy "Modificateur d'heure de fin de service le week-end"
scoreboard objectives add debutPointe1 dummy "Début de l'heure de pointe 1"
scoreboard objectives add debutPointe2 dummy "Début de l'heure de pointe 2"
scoreboard objectives add finPointe1 dummy "Fin de l'heure de pointe 1"
scoreboard objectives add finPointe2 dummy "Fin de l'heure de pointe 2"
scoreboard objectives add frequencePointe dummy "Fréquence en heure de pointe"
scoreboard objectives add frequenceCreuse dummy "Fréquence en heure creuse"
scoreboard objectives add chancesIncident dummy "Probabilité d'incident"

# On attribue à chaque PC son numéro de ligne
scoreboard players set @e[type=item_frame,tag=pcM1] idLine 11
scoreboard players set @e[type=item_frame,tag=pcM1] idLineMax 12
scoreboard players set @e[type=item_frame,tag=pcM2] idLine 21
scoreboard players set @e[type=item_frame,tag=pcM2] idLineMax 22
scoreboard players set @e[type=item_frame,tag=pcM3] idLine 31
scoreboard players set @e[type=item_frame,tag=pcM3] idLineMax 32
scoreboard players set @e[type=item_frame,tag=pcM4] idLine 41
scoreboard players set @e[type=item_frame,tag=pcM4] idLineMax 42
scoreboard players set @e[type=item_frame,tag=pcM5a] idLine 51
scoreboard players set @e[type=item_frame,tag=pcM5a] idLineMax 52
scoreboard players set @e[type=item_frame,tag=pcM5b] idLine 55
scoreboard players set @e[type=item_frame,tag=pcM5b] idLineMax 56
scoreboard players set @e[type=item_frame,tag=pcM6] idLine 61
scoreboard players set @e[type=item_frame,tag=pcM6] idLineMax 62
scoreboard players set @e[type=item_frame,tag=pcM7] idLine 71
scoreboard players set @e[type=item_frame,tag=pcM7] idLineMax 72
scoreboard players set @e[type=item_frame,tag=pcM7b] idLine 73
scoreboard players set @e[type=item_frame,tag=pcM7b] idLineMax 74
scoreboard players set @e[type=item_frame,tag=pcM8] idLine 81
scoreboard players set @e[type=item_frame,tag=pcM8] idLineMax 82
scoreboard players set @e[type=item_frame,tag=pcM8b] idLine 83
scoreboard players set @e[type=item_frame,tag=pcM8b] idLineMax 84
scoreboard players set @e[type=item_frame,tag=pcM9] idLine 91
scoreboard players set @e[type=item_frame,tag=pcM9] idLineMax 92
scoreboard players set @e[type=item_frame,tag=pcM10] idLine 101
scoreboard players set @e[type=item_frame,tag=pcM10] idLineMax 102
scoreboard players set @e[type=item_frame,tag=pcM11] idLine 111
scoreboard players set @e[type=item_frame,tag=pcM11] idLineMax 112
scoreboard players set @e[type=item_frame,tag=pcM12] idLine 121
scoreboard players set @e[type=item_frame,tag=pcM12] idLineMax 123

scoreboard players set @e[type=item_frame,tag=pcRERA] idLine 2100
scoreboard players set @e[type=item_frame,tag=pcRERA] idLineMax 2199
scoreboard players set @e[type=item_frame,tag=pcRERB] idLine 2200
scoreboard players set @e[type=item_frame,tag=pcRERB] idLineMax 2299

scoreboard players set @e[type=item_frame,tag=pcCA] idLine 4011
scoreboard players set @e[type=item_frame,tag=pcCA] idLineMax 4012
scoreboard players set @e[type=item_frame,tag=pcCB] idLine 4021
scoreboard players set @e[type=item_frame,tag=pcCB] idLineMax 4022

# On initialise les incidents
function tch:ligne/incident/init
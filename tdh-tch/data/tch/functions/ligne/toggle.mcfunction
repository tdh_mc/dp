# On inverse notre statut
tag @s[tag=EnService] add HorsService
tag @s[tag=HorsService,tag=!EnService] remove HorsService
tag @s[tag=!HorsService,tag=!EnService] add EnService
tag @s[tag=HorsService,tag=EnService] remove EnService

# On modifie le bloc situé en-dessous de nous
execute at @s[tag=HorsService] run function tch:ligne/toggle/lumiere_hors_service
execute at @s[tag=EnService] run function tch:ligne/toggle/lumiere_en_service

# On affiche un message informatif
execute as @s[tag=HorsService] run tellraw @a[tag=MetroDebugLog] [{"text":"La ligne ","color":"gray"},{"selector":"@s"},{"text":" est maintenant "},{"text":"hors service","color":"red"},{"text":"."}]
execute as @s[tag=EnService] run tellraw @a[tag=MetroDebugLog] [{"text":"La ligne ","color":"gray"},{"selector":"@s"},{"text":" est maintenant "},{"text":"en service","color":"green"},{"text":"."}]
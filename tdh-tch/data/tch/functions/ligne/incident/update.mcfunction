# Chaque PC ligne exécute cette fonction une fois tous les quarts d'heure 

# Si elle a déjà un incident, c'est pour éventuellement le faire évoluer
execute as @s[tag=IncidentEnCours] run function tch:ligne/incident/update_incident

# Si elle n'a pas encore d'incident, c'est pour éventuellement générer un incident
execute as @s[tag=!IncidentEnCours] run function tch:ligne/incident/update_normal

# On calcule l'heure de prochain calcul
function tch:ligne/incident/update/heure_prochain_calcul
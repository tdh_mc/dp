# On récupère une variable aléatoire entre 0 et 2000 * (le nombre d'itérations par jour avec nos réglages actuels)
scoreboard players set #random min 0
# max = fullDayTicks/frequenceUpdate * 2000
scoreboard players operation #random max = #temps fullDayTicks
scoreboard players set #random temp 500
scoreboard players operation #random max /= #random temp
scoreboard players set #random temp 2000
scoreboard players operation #random max *= #random temp
# On calcule la variable aléatoire
function tdh:random

# On choisit un type d'incident si la variable matche
execute if score #random temp < @s chancesIncident run function tch:ligne/incident/update/nouvel_incident
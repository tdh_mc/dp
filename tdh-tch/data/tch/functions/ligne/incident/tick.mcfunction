## CALCUL DES NOUVEAUX INCIDENTS
# On fait le calcul des incidents une fois toutes les demi-heures
# On vérifie si on a dépassé l'horaire d'update
scoreboard players set #tchIncident temp 0
execute if score #tchIncident idJour < #temps idJour run scoreboard players set #tchIncident temp 1
execute if score #tchIncident idJour = #temps idJour if score #tchIncident currentTdhTick <= #temps currentTdhTick run scoreboard players set #tchIncident temp 1

# Si on a dépassé l'horaire, on exécute le calcul des incidents
# On l'exécute aussi pour les lignes en fin de service, car c'est la seule manière de générer des incidents durant toute la journée
# (comme des incidents affectant la voie ou des pannes de signalisation)
execute if score #tchIncident temp matches 1 as @e[tag=tchPC,tag=EnService] run function tch:ligne/incident/update


# On réinitialise la variable temporaire
scoreboard players reset #tchIncident temp
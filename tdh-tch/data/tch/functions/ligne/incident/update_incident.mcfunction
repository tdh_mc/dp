## CALCUL DES INCIDENTS EN COURS
# On prend un nombre aléatoire (l'incident ne change pas de statut tous les quarts d'heure...) (0-999)
scoreboard players set #random min 0
scoreboard players set #random max 1000
function tdh:random

# En fonction du statut actuel de l'incident, on vérifie quoi faire
# Code 4 : Le trafic reprend progressivement
execute if score @s status matches 4 run function tch:ligne/incident/update/test_fin_incident
# Code 3 : Les équipes sont en cours d'intervention.
execute if score @s status matches 3 run function tch:ligne/incident/update/test_reprise
# Code 2 : On a un horaire de reprise prévu.
execute if score @s status matches 2 run function tch:ligne/incident/update/test_intervention
# Code 1 : L'incident vient de débuter, on est en attente de plus d'informations.
execute if score @s status matches 1 run function tch:ligne/incident/update/test_horaire_reprise
# Code 0 : L'incident initial est en cours (on n'a pas encore l'information qu'il existe et la ligne n'est pas interrompue)
execute if score @s status matches 0 run function tch:ligne/incident/update/test_debut_incident
# On génère une nouvelle panne de signalisation
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"divers incidents",deDu:"de ",alAu:"à ",article:"",Article:""}

execute if score @s temp3 matches ..6999 run function tch:ligne/incident/update/fin_de_service/divers/fin_tardive_de_chantier
execute if score @s temp3 matches 7000.. run function tch:ligne/incident/update/fin_de_service/divers/travaux
# DÉFAUT D'ALIMENTATION ÉLECTRIQUE - Code incident 32
scoreboard players set @s incident 32

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"défaut d'alimentation électrique",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}


# On coupe le courant sur les quais de la ligne concernée ?
#TODO
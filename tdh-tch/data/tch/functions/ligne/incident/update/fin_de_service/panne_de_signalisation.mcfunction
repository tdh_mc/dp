# On génère une nouvelle panne de signalisation
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"panne de signalisation",deDu:"d'une ",alAu:"à une ",article:"une ",Article:"Une "}

execute if score @s temp3 matches ..7999 run function tch:ligne/incident/update/fin_de_service/panne_de_signalisation/panne_des_installations_fixes
execute if score @s temp3 matches 8000.. run function tch:ligne/incident/update/fin_de_service/panne_de_signalisation/panne_des_installations_embarquees
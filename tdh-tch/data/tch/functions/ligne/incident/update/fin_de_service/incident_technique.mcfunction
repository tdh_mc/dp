# On génère une nouvelle panne de signalisation
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"incident technique",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}

# Pas de condition car il n'y a qu'un seul incident technique pouvant être généré toute la journée
function tch:ligne/incident/update/fin_de_service/incident_technique/defaut_alimentation_electrique
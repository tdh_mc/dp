# On génère un nouvel incident affectant la voie
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"incident affectant la voie",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}

execute if score @s temp3 matches ..3999 run function tch:ligne/incident/update/fin_de_service/incident_affectant_la_voie/rail_casse
execute if score @s temp3 matches 4000..7999 run function tch:ligne/incident/update/fin_de_service/incident_affectant_la_voie/rail_endommage
execute if score @s temp3 matches 8000.. run function tch:ligne/incident/update/fin_de_service/incident_affectant_la_voie/affaissement_de_voie
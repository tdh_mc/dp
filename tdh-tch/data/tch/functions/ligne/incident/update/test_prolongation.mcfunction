# On vérifie si on prolonge le délai d'attente de ces gros bâtards de voyageurs
# On prend un nombre aléatoire pour tester si on prolonge (0-999)
scoreboard players set #random min 0
scoreboard players set #random max 1000
function tdh:random

# On définit le maximum de ce nombre pour pouvoir prolonger l'horaire
# (au-dessus, on garde l'horaire ; en-dessous, on prolonge)
# Par défaut, on a 1 chance sur 25 de prolonger l'attente de 1h tous les 1/4 d'heure
scoreboard players set @s temp6 40
scoreboard players set @s temp7 2000
# Dans le cas d'incidents ou de malaises voyageurs, les prolongations sont plus courtes mais plus fréquentes (30mn pour 1 chance sur 10)
execute if score @s incident matches 40..59 run scoreboard players set @s temp6 100
execute if score @s incident matches 40..59 run scoreboard players set @s temp7 1000
# Dans le cas d'actes de malveillance, il y a un peu plus de chances de prolongation (1 sur 20)
execute if score @s incident matches 90..99 run scoreboard players set @s temp6 50
# Divers incidents ne se prolongent presque jamais
execute if score @s incident matches 100..109 run scoreboard players set @s temp6 3
execute if score @s incident matches 100..109 run scoreboard players set @s temp7 3000


# Si la variable aléatoire matche, on prolonge le temps d'attente
execute if score #random temp < @s temp6 run scoreboard players operation @s finIncident += @s temp7
execute if score @s finIncident >= #temps fullDayTicks run scoreboard players add @s idJour 1
execute if score @s finIncident >= #temps fullDayTicks run scoreboard players operation @s finIncident -= #temps fullDayTicks


# On réinitialise les variables
scoreboard players reset @s temp6
scoreboard players reset @s temp7
# On a déjà le nombre aléatoire qui est stocké dans #random temp
# On génère des incidents EN SERVICE (durée et gravité moins importantes)

# On stocke le premier nombre aléatoire
scoreboard players operation @s temp2 = #random temp

# On génère un nouveau nombre aléatoire pour le choix du sous-type d'incident
# Il est entre 0 et 10000 car on adore se faire chier à écrire des nombres à 4 chiffres dans les conditions!
scoreboard players set #random min 0
scoreboard players set #random max 10000
function tdh:random
scoreboard players operation @s temp3 = #random temp

# Types d'incidents disponibles :
# Incident technique 10% (partagé avec les incidents FIN DE SERVICE)
execute if score @s temp2 matches ..999 run function tch:ligne/incident/update/en_service/incident_technique
# Incident voyageur 28%
execute if score @s temp2 matches 1000..3799 run function tch:ligne/incident/update/en_service/incident_voyageur
# Malaise voyageur 9%
execute if score @s temp2 matches 3800..4699 run function tch:ligne/incident/update/en_service/malaise_voyageur
# Panne mécanique 16%
execute if score @s temp2 matches 4700..6299 run function tch:ligne/incident/update/en_service/panne_mecanique
# Mesures de sécurité 7%
execute if score @s temp2 matches 6300..6999 run function tch:ligne/incident/update/en_service/mesures_de_securite
# Difficultés d'exploitation 11%
execute if score @s temp2 matches 7000..8099 run function tch:ligne/incident/update/en_service/difficultes_exploitation
# Acte de malveillance 14%
execute if score @s temp2 matches 8100..9499 run function tch:ligne/incident/update/en_service/acte_de_malveillance
# Divers incidents 5% SAUF fin tardive de chantier et travaux
execute if score @s temp2 matches 9500.. run function tch:ligne/incident/update/en_service/divers
# Concernant les conditions météo, ça sera commandé directement par le système météorologique


# TODO : Choisir une gare où ça se produit en choisissant au hasard un tchQuaiPC dont on partage l'idLine


# On réinitialise le nouveau nombre aléatoire
scoreboard players reset @s temp2

tellraw @a[tag=MetroDebugLog] [{"text":"Debug/ On génère un incident sur la ligne ","color":"gray"},{"selector":"@s"},{"text":"."}]
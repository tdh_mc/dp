# On vérifie si l'horaire a été dépassé
execute if score @s finIncident <= #temps currentTdhTick if score @s idJour = #temps idJour run scoreboard players set @s status 5
execute if score @s idJour < #temps idJour run scoreboard players set @s status 5

# Si c'est le cas, on termine l'incident
execute if score @s status matches 5 run function tch:ligne/incident/update/fin_incident
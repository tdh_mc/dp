# On jure officiellement que nos incidents sont connus
scoreboard players set @s status 1

tellraw @a[tag=MetroDebugLog] [{"text":"Debug/ ","color":"gray"},{"nbt":"Item.tag.tch.incident.generic.Article","entity":"@s"},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@s","color":"yellow"},{"text":" vient de se produire et la ligne "},{"selector":"@s"},{"text":" est interrompue."}]
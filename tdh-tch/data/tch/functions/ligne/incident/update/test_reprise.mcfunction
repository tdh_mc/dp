# On vérifie si on a dépassé l'heure de reprise et qu'on peut reprendre le service
execute if score @s finIncident <= #temps currentTdhTick if score @s idJour = #temps idJour run scoreboard players set @s status 4
execute if score @s idJour < #temps idJour run scoreboard players set @s status 4

# Si on est encore en cours d'incident, on prolonge éventuellement l'horaire de reprise
execute if score @s status matches 3 run function tch:ligne/incident/update/test_prolongation

# Sinon, on termine l'incident
execute if score @s status matches 4 run function tch:ligne/incident/update/reprise_trafic
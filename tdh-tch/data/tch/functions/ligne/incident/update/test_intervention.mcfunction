# Une "intervention", en l'absence (temporaire) d'actions ingame, servira principalement à l'information voyageur (un message différent sera donné lorsqu'on sera en cours d'intervention)

# Si ça reprend dans moins de 2h, on est forcément en cours d'intervention
scoreboard players operation @s temp5 = @s finIncident
scoreboard players operation @s temp5 -= #temps currentTdhTick
execute if score @s temp5 matches 0..2000 if score @s idJour = #temps idJour run scoreboard players set @s status 3

# Sinon, on prend un nombre aléatoire pour tester si on intervient (0-999)
scoreboard players set #random min 0
scoreboard players set #random max 1000
function tdh:random
execute if score #random temp matches 333..666 run scoreboard players set @s status 3

# Si on a évolué, on lance la fonction intervention
execute if score @s status matches 3 run function tch:ligne/incident/update/debut_intervention


# Dans tous les cas, on prolonge éventuellement l'horaire de reprise
function tch:ligne/incident/update/test_prolongation


# On réinitialise les variables
scoreboard players reset @s temp5
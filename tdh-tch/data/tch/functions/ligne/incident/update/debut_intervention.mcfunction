# On débute officiellement l'intervention
scoreboard players set @s status 3

scoreboard players operation #store currentHour = @s finIncident
function tdh:store/time
tellraw @a[tag=MetroDebugLog] [{"text":"Debug/ L'intervention sur la ligne ","color":"gray"},{"selector":"@s"},{"text":" est en cours ("},{"nbt":"Item.tag.tch.incident.generic.article","entity":"@s"},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@s"},{"text":"). Reprise estimée à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"yellow"},{"text":"."}]
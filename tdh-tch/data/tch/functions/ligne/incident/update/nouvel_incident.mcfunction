# On génère déjà le nombre aléatoire, pas besoin de l'écrire deux fois!
# Il est entre 0 et 10000 car on adore se faire chier à écrire des nombres à 4 chiffres dans les conditions!
scoreboard players set #random min 0
scoreboard players set #random max 10000
function tdh:random

# On enregistre le fait qu'on a un incident (même si on n'est pas encore supposés le savoir et que la ligne ne sera pas encore interrompue)
tag @s add IncidentEnCours
# On enregistre le statut de l'incident (0=vient de débuter, on ne sait pas encore qu'il est là)
scoreboard players set @s status 0

# On choisit un type d'incident en fonction de l'état de la ligne
# Si la ligne est en service, on va choisir des incidents ponctuels
execute as @s[tag=!FinDeService] run function tch:ligne/incident/update/nouvel_incident_en_service

# Si la ligne est en fin de service, on va choisir des incidents sur toute la journée
#execute as @s[tag=FinDeService] run function tch:ligne/incident/update/nouvel_incident_fin_de_service


# On réinitialise la variable contenant le nombre aléatoire
scoreboard players reset @s temp2
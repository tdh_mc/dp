# On termine l'incident et réinitialise toutes les variables liées
scoreboard players reset @s debutIncident
scoreboard players reset @s finIncident
scoreboard players set @s incident 0
scoreboard players reset @s status
data remove entity @s Item.tag.tch.incident
tag @s remove IncidentEnCours

tellraw @a[tag=MetroDebugLog] [{"text":"Debug/ Fin d'incident sur la ligne ","color":"gray"},{"selector":"@s"},{"text":"."}]
# On termine officiellement l'intervention, le trafic reprend progressivement
scoreboard players set @s status 4

# On enregistre dans finIncident l'horaire où le trafic sera à nouveau normal (par défaut 2h)
scoreboard players set @s finIncident 4000
scoreboard players operation @s finIncident += #temps currentTdhTick
execute if score @s finIncident >= #temps fullDayTicks run scoreboard players operation @s finIncident -= #temps fullDayTicks

tellraw @a[tag=MetroDebugLog] [{"text":"Debug/ Le trafic reprend progressivement sur la ligne ","color":"gray"},{"selector":"@s"},{"text":" ("},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@s"},{"text":")."}]
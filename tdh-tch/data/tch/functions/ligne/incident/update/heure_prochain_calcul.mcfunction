# On stocke l'heure de prochain calcul des incidents
scoreboard players operation #tchIncident idJour = #temps idJour
scoreboard players operation #tchIncident currentTdhTick = #temps currentTdhTick
# 507 ticks=un tout petit peu plus de 15mn pour que ça se décale avec le temps et ne se produise pas tjrs à heure fixe
scoreboard players add #tchIncident currentTdhTick 507

# Si on a dépassé minuit, on ajoute 1 jour
execute if score #tchIncident currentTdhTick >= #temps fullDayTicks run scoreboard players add #tchIncident idJour 1
execute if score #tchIncident currentTdhTick >= #temps fullDayTicks run scoreboard players operation #tchIncident currentTdhTick -= #temps fullDayTicks
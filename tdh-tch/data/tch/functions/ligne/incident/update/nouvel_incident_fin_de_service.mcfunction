# On a déjà le nombre aléatoire qui est stocké dans @s temp2 (0-9999)
# On génère des incidents FIN DE SERVICE (interruption toute la journée)

# On stocke le premier nombre aléatoire
scoreboard players operation @s temp2 = #random temp

# On génère un nouveau nombre aléatoire pour le choix du sous-type d'incident
# Il est entre 0 et 10000 car on adore se faire chier à écrire des nombres à 4 chiffres dans les conditions!
scoreboard players set #random min 0
scoreboard players set #random max 10000
function tdh:random
scoreboard players operation @s temp3 = #random temp

# Types d'incidents disponibles :
# Incident affectant la voie 30%
execute if score @s temp2 matches ..2999 run function tch:ligne/incident/update/fin_de_service/incident_affectant_la_voie
# Panne de signalisation 45%
execute if score @s temp2 matches 3000..7499 run function tch:ligne/incident/update/fin_de_service/panne_de_signalisation
# Incident technique 15% (partagé avec les incidents FIN DE SERVICE)
execute if score @s temp2 matches 7500..8999 run function tch:ligne/incident/update/fin_de_service/incident_technique
# Divers incidents 10% (seulement fin tardive de chantier ou travaux)
execute if score @s temp2 matches 9000.. run function tch:ligne/incident/update/fin_de_service/divers


# TODO : Choisir une gare où ça se produit en choisissant au hasard un tchQuaiPC dont on partage l'idLine

# Dans le cas d'un incident en fin de service, on démarre directement à l'étape 2 (incident connu et horaire de reprise prévu)
scoreboard players set @s status 2
# L'horaire de reprise est le lendemain en début de service
scoreboard players operation @s finIncident = @s debutService
scoreboard players operation @s idJour = #temps idJour
scoreboard players add @s idJour 1
execute if score #temps currentTdhTick > @s debutService run scoreboard players add @s idJour 1


# On réinitialise le nouveau nombre aléatoire
scoreboard players reset @s temp3

scoreboard players operation #store currentHour = @s finIncident
scoreboard players operation #store idJour = @s idJour
function tdh:store/time
function tdh:store/day
tellraw @a[tag=MetroDebugLog] [{"text":"Debug/ On génère un incident ","color":"gray"},{"nbt":"Item.tag.tch.incident.generic.article","entity":"@s"},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@s"},{"text":" ("},{"nbt":"Item.tag.tch.incident.precis.article","entity":"@s"},{"nbt":"Item.tag.tch.incident.precis.nom","entity":"@s","color":"yellow"},{"text":") sur la ligne "},{"selector":"@s"},{"text":". Reprise estimée à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"yellow"},{"text":" le "},{"nbt":"day","storage":"tdh:store","interpret":"true","color":"yellow"},{"text":"."}]]
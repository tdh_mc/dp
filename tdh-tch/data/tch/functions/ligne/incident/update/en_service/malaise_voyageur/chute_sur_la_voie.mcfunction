# CHUTE SUR LA VOIE - Code incident 53
scoreboard players set @s incident 53

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"chute sur la voie",deDu:"d'une ",alAu:"à une ",article:"une ",Article:"Une "}


# Un voyageur tombe depuis un quai (apriori pas grave, mais trafic interrompu par précaution)
#TODO
# Cette fonction est exécutée par le PC ligne, à la position d'une armor stand NumLigne de la même ligne,
# lorsqu'un dégagement de fumée vient d'avoir lieu (incident 33)

# On crée des particules de fumée
particle poof ~ ~3 ~ 0.5 0.2 0.5 0.1 200
particle campfire_signal_smoke ~ ~2.5 ~ 0.1 0.2 0.1 0.002 200
particle campfire_signal_smoke ~ ~2.5 ~ 0.2 0.3 0.2 0.006 120
particle campfire_signal_smoke ~ ~2.5 ~ 0.3 0.4 0.3 0.01 100
particle campfire_signal_smoke ~ ~2.5 ~ 0.4 0.4 0.4 0.02 50


# On balance des bruitages
playsound tdh:fx.gears.stop ambient @a[distance=..30] ~ ~ ~ 1.8 0.7 0.05
playsound tdh:br.chuff ambient @a[distance=..30] ~ ~ ~ 2 0.5 0.04
playsound tdh:br.chuff ambient @a[distance=..30] ~ ~ ~ 1.6 0.55 0.02
playsound tdh:br.chuff ambient @a[distance=..30] ~ ~ ~ 1.0 0.61 0.02
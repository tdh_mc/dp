# On génère un nouvel incident voyageur
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"incident voyageur",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}

execute if score @s temp3 matches ..1399 run function tch:ligne/incident/update/en_service/incident_voyageur/incident_entre_voyageurs
execute if score @s temp3 matches 1400..2999 run function tch:ligne/incident/update/en_service/incident_voyageur/chute_voyageur_a_bord
execute if score @s temp3 matches 3000..6099 run function tch:ligne/incident/update/en_service/incident_voyageur/altercation_entre_voyageurs
execute if score @s temp3 matches 6100..8299 run function tch:ligne/incident/update/en_service/incident_voyageur/actionnement_signal_alarme
execute if score @s temp3 matches 8300.. run function tch:ligne/incident/update/en_service/incident_voyageur/personnes_sur_les_voies
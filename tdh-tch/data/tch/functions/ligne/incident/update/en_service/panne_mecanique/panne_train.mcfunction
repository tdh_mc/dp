# PANNE D'UN TRAIN - Code incident 61
scoreboard players set @s incident 61

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"panne soudaine d'un train",deDu:"de la ",alAu:"à la ",article:"la ",Article:"La "}


# Potentiellement c'est ton train (si tu es sur la ligne à ce moment-là) qui tombe en panne (avant que le reste du trafic soit interrompu).
#TODO
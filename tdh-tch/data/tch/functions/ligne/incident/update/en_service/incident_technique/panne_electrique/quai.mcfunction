# Cette fonction est exécutée par le PC ligne, à la position d'une armor stand NumLigne de la même ligne,
# lorsqu'une panne électrique vient d'avoir lieu (incident 31)

# On crée des particules électriques
particle firework ~ ~3 ~ 0.02 0.02 0.02 0.3 50 force
particle firework ~ ~3 ~ 0.05 0.05 0.05 0.18 400
particle crit ~ ~3 ~ 0.2 0.05 0.2 1 150
particle poof ~ ~2.5 ~ 0.2 0.2 0.2 0.02 50


# On balance des bruitages
playsound minecraft:entity.blaze.death ambient @a[distance=..30] ~ ~ ~ 1.8 0.5 0.05
playsound tdh:br.chuff ambient @a[distance=..30] ~ ~ ~ 1.5 0.7 0.04
playsound tdh:br.chuff ambient @a[distance=..30] ~ ~ ~ 1.0 0.51 0.02


# Todo: également éteindre les lumières dans la station affectée
# On génère un nouvel incident technique
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"incident technique",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}

# Les motifs 30, 31 et 33 peuvent être générés, mais 32 est réservé aux incidents durant toute la journée
execute if score @s temp3 matches ..2499 run function tch:ligne/incident/update/en_service/incident_technique/court_circuit
execute if score @s temp3 matches 2500..8299 run function tch:ligne/incident/update/en_service/incident_technique/panne_electrique
execute if score @s temp3 matches 8300.. run function tch:ligne/incident/update/en_service/incident_technique/degagement_de_fumee
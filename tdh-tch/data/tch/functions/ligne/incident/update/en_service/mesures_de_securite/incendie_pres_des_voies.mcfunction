# INCENDIE PRES DES VOIES - Code incident 70
scoreboard players set @s incident 70

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"incendie aux abords des voies",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}


# On fait démarrer un incendie quelque part sur près d'une voie
#TODO
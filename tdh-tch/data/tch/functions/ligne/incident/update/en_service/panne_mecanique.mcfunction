# On génère une nouvelle panne mécanique
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"panne mécanique",deDu:"d'une ",alAu:"à une ",article:"une ",Article:"Une "}

execute if score @s temp3 matches ..3999 run function tch:ligne/incident/update/en_service/panne_mecanique/avarie_materiel
execute if score @s temp3 matches 4000..5499 run function tch:ligne/incident/update/en_service/panne_mecanique/panne_train
execute if score @s temp3 matches 5500..5999 run function tch:ligne/incident/update/en_service/panne_mecanique/panne_mecanique
execute if score @s temp3 matches 6000..7499 run function tch:ligne/incident/update/en_service/panne_mecanique/non_deblocage_des_freins
execute if score @s temp3 matches 7500..8999 run function tch:ligne/incident/update/en_service/panne_mecanique/non_demarrage_dun_train
execute if score @s temp3 matches 9000.. run function tch:ligne/incident/update/en_service/panne_mecanique/probleme_de_fermeture_des_portes
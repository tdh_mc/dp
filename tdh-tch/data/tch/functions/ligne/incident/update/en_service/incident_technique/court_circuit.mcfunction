# COURT CIRCUIT - Code incident 30
scoreboard players set @s incident 30

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"court-circuit",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}


# On applique les effets à toutes les stations chargées
execute at @e[tag=NumLigne] if score @s idLine <= @e[tag=NumLigne,sort=nearest,limit=1] idLine if score @s idLineMax >= @e[tag=NumLigne,sort=nearest,limit=1] idLine run function tch:ligne/incident/update/en_service/incident_technique/court_circuit/quai
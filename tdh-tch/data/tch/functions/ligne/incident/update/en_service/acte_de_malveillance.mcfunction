# On génère un nouvel acte de malveillance
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"acte de malveillance",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}

execute if score @s temp3 matches ..3499 run function tch:ligne/incident/update/en_service/acte_de_malveillance/degradation_des_equipements
execute if score @s temp3 matches 3500..4999 run function tch:ligne/incident/update/en_service/acte_de_malveillance/degradation_du_materiel_roulant
execute if score @s temp3 matches 5000..6499 run function tch:ligne/incident/update/en_service/acte_de_malveillance/acte_de_vandalisme
execute if score @s temp3 matches 6500..7999 run function tch:ligne/incident/update/en_service/acte_de_malveillance/gene_fermeture_des_portes
execute if score @s temp3 matches 8000.. run function tch:ligne/incident/update/en_service/acte_de_malveillance/forcage_portes_palieres
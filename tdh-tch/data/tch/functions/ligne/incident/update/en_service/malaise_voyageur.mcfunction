# On génère un nouveau malaise voyageur
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"malaise voyageur",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}

execute if score @s temp3 matches ..3999 run function tch:ligne/incident/update/en_service/malaise_voyageur/voyageur_malade
execute if score @s temp3 matches 4000..5599 run function tch:ligne/incident/update/en_service/malaise_voyageur/intervention_des_pompiers
execute if score @s temp3 matches 5600..7999 run function tch:ligne/incident/update/en_service/malaise_voyageur/accident_grave_de_personne
execute if score @s temp3 matches 8000.. run function tch:ligne/incident/update/en_service/malaise_voyageur/chute_sur_la_voie
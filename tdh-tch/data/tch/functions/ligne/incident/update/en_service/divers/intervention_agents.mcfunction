# INTERVENTION DES AGENTS - Code incident 100
scoreboard players set @s incident 100

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"intervention des agents",deDu:"(et je vous le dis parce que vous ne comprendrez pas le terme) de l' ",alAu:"(et si je vous le dis, c'est bien parce que vous ne comprendrez pas le mot) à un ",article:"l' ",Article:"L'"}


# Lève le pied.
#TODO
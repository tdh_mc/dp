# ACCIDENT GRAVE DE PERSONNE - Code incident 52
scoreboard players set @s incident 52

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"accident grave de personne",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}


# Un voyageur se jette devant un métro en station (et éventuellement devant le tien si tu vas arriver en gare)
#TODO
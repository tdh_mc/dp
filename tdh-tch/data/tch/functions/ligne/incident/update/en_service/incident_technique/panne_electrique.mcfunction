# PANNE ELECTRIQUE - Code incident 31
scoreboard players set @s incident 31

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"panne électrique",deDu:"d'une ",alAu:"à une ",article:"une ",Article:"Une "}


# On applique les effets à toutes les stations chargées
execute at @e[tag=NumLigne] if score @s idLine <= @e[tag=NumLigne,sort=nearest,limit=1] idLine if score @s idLineMax >= @e[tag=NumLigne,sort=nearest,limit=1] idLine run function tch:ligne/incident/update/en_service/incident_technique/panne_electrique/quai
# DEGAGEMENT DE FUMEE - Code incident 33
scoreboard players set @s incident 33

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"dégagement de fumée",deDu:"d'un ",alAu:"à un ",article:"un ",Article:"Un "}


# On applique les effets à toutes les stations chargées
execute at @e[tag=NumLigne] if score @s idLine <= @e[tag=NumLigne,sort=nearest,limit=1] idLine if score @s idLineMax >= @e[tag=NumLigne,sort=nearest,limit=1] idLine run function tch:ligne/incident/update/en_service/incident_technique/degagement_de_fumee/quai
# SUCCESSION DE PLUSIEURS INCIDENTS - Code incident 102
scoreboard players set @s incident 102

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"succession de plusieurs incidents",deDu:"d'une ",alAu:"à une ",article:"une ",Article:"Une "}


# On n'en rajoute pas, hein.
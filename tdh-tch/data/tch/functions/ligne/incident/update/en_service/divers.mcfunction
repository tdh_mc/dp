# On génère un nouvel incident divers
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"divers incidents",deDu:"de ",alAu:"à ",article:"",Article:""}

execute if score @s temp3 matches ..3999 run function tch:ligne/incident/update/en_service/divers/intervention_agents
execute if score @s temp3 matches 4000..6599 run function tch:ligne/incident/update/en_service/divers/deraillement_train
execute if score @s temp3 matches 6600..7199 run function tch:ligne/incident/update/en_service/divers/succession_de_plusieurs_incidents
execute if score @s temp3 matches 7200..7999 run function tch:ligne/incident/update/en_service/divers/travaux
execute if score @s temp3 matches 7999.. run function tch:ligne/incident/update/en_service/divers/succession_de_plusieurs_incidents
# On génère une nouvelle mesure de sécurité
# On a un nombre aléatoire stocké dans @s temp3 (0-9999)

# On enregistre dans nos données le motif générique de l'incident
data modify entity @s Item.tag.tch.incident.generic set value {nom:"mesures de sécurité",deDu:"de l'application de ",alAu:"à l'application des ",article:"des ",Article:"Des "}

execute if score @s temp3 matches ..1999 run function tch:ligne/incident/update/en_service/mesures_de_securite/incendie_pres_des_voies
execute if score @s temp3 matches 2000..4999 run function tch:ligne/incident/update/en_service/mesures_de_securite/bagage_oublie
execute if score @s temp3 matches 5000..6666 run function tch:ligne/incident/update/en_service/mesures_de_securite/animal_sur_les_voies
execute if score @s temp3 matches 6667..7999 run function tch:ligne/incident/update/en_service/mesures_de_securite/manifestation
execute if score @s temp3 matches 8000.. run function tch:ligne/incident/update/en_service/mesures_de_securite/inondation
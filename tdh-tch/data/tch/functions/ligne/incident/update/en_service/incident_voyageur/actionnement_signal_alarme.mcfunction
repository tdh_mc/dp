# ACTIONNEMENT D'UN SIGNAL D'ALARME A BORD - Code incident 43
scoreboard players set @s incident 43

# On enregistre dans nos données le motif précis de l'incident
data modify entity @s Item.tag.tch.incident.precis set value {nom:"actionnement d'un signal d'alarme",deDu:"de l'",alAu:"à l'",article:"l'",Article:"L'"}


# Visibilité ingame? Peut-être si un mec est déjà dans un minecart il tombe sur un villageois devant lui arrêté avec une alarme qui tourne ?
#TODO
# On enregistre le fait qu'on a généré un horaire de reprise
scoreboard players set @s status 2

# On enregistre le jour de reprise ; par défaut, il s'agit d'aujourd'hui
scoreboard players operation @s idJour = #temps idJour

# On choisit ensuite ledit horaire de reprise
# On prend un nombre aléatoire entre 0 et 1000 ticks
execute store result score @s finIncident run data get entity @e[type=#tdh:random,sort=random,limit=1] Rotation[0] 2.77777
scoreboard players set @s temp5 1000
scoreboard players operation @s finIncident %= @s temp5

# En fonction du type d'incident, on aura plus ou moins de différence entre la durée minimum et maximum d'incident
# Par défaut, on conserve 1000, ce qui nous donne un temps d'attente (avant multiplication) de 30-60mn (1000-2000 ticks)
# Si c'est un incident électrique, on a une grande variabilité (300-1300 ticks)
execute if score @s incident matches 30..39 run scoreboard players set @s temp5 300
# Si c'est un malaise voyageur, on a une faible variabilité (2000-3000 ticks)
execute if score @s incident matches 50..59 run scoreboard players set @s temp5 2000
# Si c'est une mesure de sécurité, on a une faible variabilité, sauf si c'est une manifestation
execute if score @s incident matches 70..79 run scoreboard players set @s temp5 2500
execute if score @s incident matches 73 run scoreboard players set @s temp5 400
# Enfin, on ajoute cette valeur au nombre aléatoire
scoreboard players operation @s finIncident += @s temp5

# On a désormais un temps d'attente compris entre 1/2h et 1h avant la reprise.
# On va multiplier ce chiffre par une valeur différente selon le type d'incident
# Par défaut, on multiplie par 2 (donc défaut*défaut= 1-2h d'interruption)
scoreboard players set @s temp5 2
# Si c'est un incident technique, ça mettra assez longtemps (2400-10200t / 1h12-5h12)
execute if score @s incident matches 30..39 run scoreboard players set @s temp5 8
# Si c'est une altercation entre voyageurs ça peut prendre plus longtemps (1h30-3h)
execute if score @s incident matches 42 run scoreboard players set @s temp5 3
# Si c'est un AGP ça peut prendre TRÈS longtemps (5h-7h30)
execute if score @s incident matches 42 run scoreboard players set @s temp5 5
# Pour une panne mécanique, ça peut être BIEN long (5000-10000t / 2h30-5h00)
execute if score @s incident matches 60..69 run scoreboard players set @s temp5 5
# Pour une manifestation, c'est un peu long (4000-14000t / 2-7h)
execute if score @s incident matches 73 run scoreboard players set @s temp5 10
# Pour une difficulté d'exploitation, c'est assez long aussi (4000-8000t / 2-4h)
execute if score @s incident matches 80..89 run scoreboard players set @s temp5 4
# Enfin, on multiplie le nombre aléatoire par cette valeur
scoreboard players operation @s finIncident *= @s temp5


# On a désormais notre délai d'attente dans finIncident ;
# On va simplement le convertir en horaire en ajoutant le tick actuel
scoreboard players operation @s finIncident += #temps currentTdhTick
execute if score @s finIncident >= #temps fullDayTicks run scoreboard players add @s idJour 1
execute if score @s finIncident >= #temps fullDayTicks run scoreboard players operation @s finIncident -= #temps fullDayTicks


scoreboard players operation #store currentHour = @s finIncident
function tdh:store/time
tellraw @a[tag=MetroDebugLog] [{"text":"Debug/ On sait désormais que ","color":"gray"},{"nbt":"Item.tag.tch.incident.generic.article","entity":"@s"},{"nbt":"Item.tag.tch.incident.generic.nom","entity":"@s"},{"text":" sur la ligne "},{"selector":"@s"},{"text":" était précisément "},{"nbt":"Item.tag.tch.incident.precis.article","entity":"@s"},{"nbt":"Item.tag.tch.incident.precis.nom","entity":"@s","color":"yellow"},{"text":". Reprise estimée à "},{"nbt":"time","storage":"tdh:store","interpret":"true","color":"yellow"},{"text":"."}]

# On réinitialise les variables temporaires
scoreboard players reset @s temp5
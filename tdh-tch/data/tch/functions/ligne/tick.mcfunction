# GESTION DES LIGNES DE MÉTRO
# Pour chaque ligne ayant terminé son service, on vérifie si l'heure de début de service a été dépassée
# Cela fonctionne seulement si l'heure actuelle est propice à un début de service (4h-14h)
scoreboard players set #ligne temp 4
scoreboard players operation #ligne temp *= #temps ticksPerHour
scoreboard players set #ligne temp2 14
scoreboard players operation #ligne temp2 *= #temps ticksPerHour

scoreboard players set #ligne temp3 0
execute if score #temps currentTdhTick >= #ligne temp if score #temps currentTdhTick <= #ligne temp2 run scoreboard players set #ligne temp3 1

execute if score #ligne temp3 matches 1 as @e[type=item_frame,tag=tchPC,tag=FinDeService] run function tch:ligne/test_debut_service
# Pour chaque ligne n'ayant pas terminé son service, on vérifie si l'heure de fin de service a été dépassée
# Cela fonctionne seulement si l'heure actuelle est propice à une fin de service (14h-4h)
execute if score #ligne temp3 matches 0 as @e[type=item_frame,tag=tchPC,tag=!FinDeService] run function tch:ligne/test_fin_service

# Gestion des incidents (dans une sous-fonction)
# On ne fait évoluer les incidents que si le temps s'écoule actuellement
execute if score #temps timeFlowing matches 1 run function tch:ligne/incident/tick
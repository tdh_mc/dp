# Configurateur automatique de lignes
# Permet de choisir une ligne à configurer

# On démarre la config et se donne donc le tag Config et ConfigLigne
tag @s add Config
tag @s add ConfigLigne

# On active l'objectif de configuration à trigger (9998 = Choix d'une ligne à paramétrer)
scoreboard players set @s configID 9998
scoreboard players set @s config 0
scoreboard players enable @s config

# On affiche les différentes options
tellraw @s [{"text":"BIENVENUE DANS LE CONFIGURATEUR LIGNE TCH","color":"gold"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez la ligne à modifier :","color":"gray"}]
tellraw @s [{"text":"\u1a20","color":"gray"},{"text":"\u1a21","color":"#ffcd00","clickEvent":{"action":"run_command","value":"/trigger config set 11"}},{"text":" ● "},{"text":"\u1a22","color":"#003ca6","clickEvent":{"action":"run_command","value":"/trigger config set 21"}},{"text":" ● "},{"text":"\u1a23","color":"#6ec4e8","clickEvent":{"action":"run_command","value":"/trigger config set 31"}},{"text":" ● "},{"text":"\u1a24","color":"#cf009e","clickEvent":{"action":"run_command","value":"/trigger config set 41"}},{"text":" ● "},{"text":"\u1a25","color":"#ff7e2e","clickEvent":{"action":"run_command","value":"/trigger config set 51"}},{"text":" ● "},{"text":"\u1a26","color":"#ff7e2e","clickEvent":{"action":"run_command","value":"/trigger config set 55"}},{"text":" ● "},{"text":"\u1a27","color":"#60bb39","clickEvent":{"action":"run_command","value":"/trigger config set 61"}},{"text":" ● "},{"text":"\u1a28","color":"#fa9aba","clickEvent":{"action":"run_command","value":"/trigger config set 71"}},{"text":" ● "},{"text":"\u1a29","color":"#6eca97","clickEvent":{"action":"run_command","value":"/trigger config set 73"}},{"text":" ● "},{"text":"\u1a2a","color":"#e19bdf","clickEvent":{"action":"run_command","value":"/trigger config set 81"}},{"text":" ● "},{"text":"\u1a2b","color":"#345663","clickEvent":{"action":"run_command","value":"/trigger config add 83"}},{"text":" ● "},{"text":"\u1a2c","color":"#345663","clickEvent":{"action":"run_command","value":"/trigger config add 91"}},{"text":" ● "},{"text":"\u1a2d","color":"#345663","clickEvent":{"action":"run_command","value":"/trigger config add 101"}},{"text":" ● "},{"text":"\u1a2e","color":"#345663","clickEvent":{"action":"run_command","value":"/trigger config add 111"}},{"text":" ● "},{"text":"\u1a2f","color":"#345663","clickEvent":{"action":"run_command","value":"/trigger config add 121"}}]
tellraw @s [{"text":"\u1a38","color":"gray"},{"text":"\u1a39","color":"#e2231a","clickEvent":{"action":"run_command","value":"/trigger config set 2100"}},{"text":" ● "},{"text":"\u1a3a","color":"#3c91dc","clickEvent":{"action":"run_command","value":"/trigger config set 2200"}}]
tellraw @s [{"text":"\u1a40","color":"gray"},{"text":"\u1a41","color":"#4b7e99","clickEvent":{"action":"run_command","value":"/trigger config set 4011"}},{"text":" ● "},{"text":"\u1a42","color":"#c39108","clickEvent":{"action":"run_command","value":"/trigger config set 4021"}}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC

# On lance le tick d'attente de configuration
schedule function tdh:config/tick 10t
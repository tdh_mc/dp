# Configurateur automatique de ligne
# Permet de choisir les paramètres de la ligne sélectionnée

# On démarre la config et se donne donc le tag Config et ConfigLigne
tag @s add Config
tag @s add ConfigLigne

# Si on arrive depuis la fonction choix_ligne, on récupère notre idLine
execute if score @s configID matches 9998 run scoreboard players operation #tch idLine = @s config
# Si on a nous-mêmes enregistré une variable idLine, on utilise celle-ci
execute if score @s idLine matches 1.. run scoreboard players operation #tch idLine = @s idLine
# Dans tous les cas, on enregistre nous-mêmes la variable idLine
scoreboard players operation @s idLine = #tch idLine

# On récupère le bon PC selon la valeur de la variable #tch idLine
execute as @e[tag=tchPC] if score @s idLine = #tch idLine run tag @s add ourPC
# On nettoie immédiatement la variable idLine, puisqu'on sait quel est notre PC
scoreboard players reset #tch idLine

# On active l'objectif de configuration à trigger (9999 = Choix d'un paramètre à modifier)
scoreboard players set @s configID 9999
scoreboard players operation @s config = @e[type=item_frame,tag=ourPC,limit=1] idLine
scoreboard players enable @s config

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TCH : LIGNE ","color":"gold"},{"selector":"@e[type=item_frame,tag=ourPC]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le paramètre à modifier :","color":"gray"}]

# Heure de début de service
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] debutService
function tdh:store/time
tellraw @s [{"text":"- ","color":"gray"},{"text":"Heure de début de service","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config add 10000"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Heure de fin de service
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] finService
function tdh:store/time
tellraw @s [{"text":"- ","color":"gray"},{"text":"Heure de fin de service","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config add 20000"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Fréquence en heure creuse
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] frequenceCreuse
function tdh:store/time_delay
tellraw @s [{"text":"- ","color":"gray"},{"text":"Fréquence en heure creuse","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config add 40000"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Fréquence en heure de pointe
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] frequencePointe
function tdh:store/time_delay
tellraw @s [{"text":"- ","color":"gray"},{"text":"Fréquence en heure de pointe","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config add 30000"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Durée de l'heure de pointe du matin
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] finPointe1
scoreboard players operation #store currentHour -= @e[type=item_frame,tag=ourPC,limit=1] debutPointe1
function tdh:store/time_delay
tellraw @s [{"text":"- ","color":"gray"},{"text":"Durée de l'heure de pointe 1","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config add 50000"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Durée de l'heure de pointe du soir
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] finPointe2
scoreboard players operation #store currentHour -= @e[type=item_frame,tag=ourPC,limit=1] debutPointe2
function tdh:store/time_delay
tellraw @s [{"text":"- ","color":"gray"},{"text":"Durée de l'heure de pointe 2","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config add 60000"}},{"text":" (actuellement "},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Service vendredi et samedi soir
scoreboard players set #tch temp -1
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] finServiceWe
execute if score #store currentHour matches ..-1 run scoreboard players operation #store currentHour *= #tch temp
function tdh:store/time_delay
execute if score @e[type=item_frame,tag=ourPC,limit=1] finServiceWe matches 0.. run tellraw @s [{"text":"- ","color":"gray"},{"text":"Service vendredi et samedi soirs","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config add 70000"}},{"text":" (actuellement +"},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
execute if score @e[type=item_frame,tag=ourPC,limit=1] finServiceWe matches ..-1 run tellraw @s [{"text":"- ","color":"gray"},{"text":"Service vendredi et samedi soirs","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config add 70000"}},{"text":" (actuellement -"},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]

# Service samedi et dimanche matin
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe
execute if score #store currentHour matches ..-1 run scoreboard players operation #store currentHour *= #tch temp
function tdh:store/time_delay
execute if score @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe matches 0.. run tellraw @s [{"text":"- ","color":"gray"},{"text":"Service samedi et dimanche matins","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config add 80000"}},{"text":" (actuellement +"},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
execute if score @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe matches ..-1 run tellraw @s [{"text":"- ","color":"gray"},{"text":"Service samedi et dimanche matins","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config add 80000"}},{"text":" (actuellement -"},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
# Probabilité d'incident
tellraw @s [{"text":"- ","color":"gray"},{"text":"Probabilité d'incident","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config add 90000"}},{"text":" (actuellement "},{"score":{"name":"@e[type=item_frame,tag=ourPC,limit=1]","objective":"chancesIncident"}},{"text":" pour 2000)"}]
tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"- Quitter le configurateur","color":"gray","clickEvent":{"action":"run_command","value":"/trigger config set -1"}}]

# Si on n'a pas trouvé de PC à cet ID ligne, on annule
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run function tdh:config/end
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"."}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
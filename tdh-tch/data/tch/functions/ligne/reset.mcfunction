# Initialisation de toutes les lignes avec des valeurs par défaut
scoreboard players set @e[tag=tchPC] debutService 12000
scoreboard players set @e[tag=tchPC] debutServiceAjd 12000
scoreboard players set @e[tag=tchPC] debutServiceWe 0

scoreboard players set @e[tag=tchPC] finService 44000
scoreboard players set @e[tag=tchPC] finServiceAjd 44000
scoreboard players set @e[tag=tchPC] finServiceWe 0

scoreboard players set @e[tag=tchPC] debutPointe1 14500
scoreboard players set @e[tag=tchPC] finPointe1 18500
scoreboard players set @e[tag=tchPC] debutPointe2 36500
scoreboard players set @e[tag=tchPC] finPointe2 40500

scoreboard players set @e[tag=tchPC] frequencePointe 800
scoreboard players set @e[tag=tchPC] frequenceCreuse 1200
scoreboard players set @e[tag=tchPC] chancesIncident 50
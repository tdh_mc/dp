## GÉNÉRATION D'UN NOUVEL HORAIRE DE PASSAGE (LIGNE EN FIN DE SERVICE)
# On choisit aléatoirement un décalage entre l'heure de premier passage et l'heure théorique
# Décalage min: 100 ticks, max: la moitié de leur fréquence creuse

# On récupère un nombre aléatoire
execute store result score @s spawnAt run data get entity @e[type=#tdh:random,sort=random,limit=1] Rotation[0] 6744.4

# On calcule le modulo (la moitié de la fréquence en heure creuse)
scoreboard players set @s temp 2
scoreboard players operation @s temp2 = @e[type=item_frame,tag=ourPC,limit=1] frequenceCreuse
scoreboard players operation @s temp2 /= @s temp
scoreboard players remove @s temp2 100

# On applique le modulo, puis on ajoute 100 pour obtenir le décalage final
scoreboard players operation @s spawnAt %= @s temp2
scoreboard players add @s spawnAt 100

# Enfin, on calcule l'heure réelle de passage en ajoutant l'heure de début de service
scoreboard players operation @s spawnAt += @e[type=item_frame,tag=ourPC,limit=1] debutServiceAjd

# On calcule ensuite le jour où l'on va spawner
# Par défaut c'est ajd, mais si l'heure qu'on a générée est inférieure à l'heure actuelle c'est que ce sera demain
scoreboard players operation @s idJour = #temps idJour
execute if score @s spawnAt <= #temps currentTdhTick run scoreboard players add @s idJour 1

# On réinitialise les variables temporaires
scoreboard players reset @s temp
scoreboard players reset @s temp2

# On supprime le tag "Dernier" au cas où on l'avait (pour ne pas croire demain matin que c'est déjà le dernier, ce serait ballot)
tag @s remove Dernier
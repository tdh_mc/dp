## GÉNÉRATION D'UN NOUVEL HORAIRE DE PASSAGE
# On est une item frame SpawnMetro à sa propre position

# Si on n'a pas de score idStation, on essaie de le déterminer automatiquement
# (seulement si un joueur est suffisamment proche, pour éviter qu'on fasse cette détection avant d'avoir chargé la station la plus proche)
execute unless score @s idStation matches 1.. if entity @p[distance=..100] run function tch:id/station_proche

# On essaie de récupérer notre PC
function tch:tag/pc

# On calcule 12h selon notre durée actuelle du jour
scoreboard players set #ligne temp 12
scoreboard players operation #ligne temp *= #temps ticksPerHour

# Selon l'état de la ligne, on définit une variable temporaire :
# - Par défaut, la ligne est en service (9)
scoreboard players set @s temp 9
# - Si un incident perturbe la ligne ou qu'une interruption est devenue reprise progressive
execute if entity @e[type=item_frame,tag=ourPC,tag=IncidentEnCours,scores={status=4..},limit=1] run scoreboard players set @s temp 3
# - Si un incident est en cours et qu'il a été détecté (et pas encore en reprise progressive, status = 4)
execute if entity @e[type=item_frame,tag=ourPC,tag=IncidentEnCours,scores={status=1..3},limit=1] run scoreboard players set @s temp 2
# - Si la ligne est fermée ou n'existe pas
execute unless entity @e[type=item_frame,tag=ourPC,tag=!HorsService,limit=1] run scoreboard players set @s temp 0
# - Si la ligne est en fin de service (ou qu'on a déjà envoyé le dernier métro, ce qui revient au même)
execute if entity @e[type=item_frame,tag=ourPC,tag=EnService,limit=1] run tag @s[tag=Dernier] add tempFDS
# Si la ligne a le tag "fin de service", on réalise le test d'ouverture pour vérifier si elle l'est vraiment
execute as @e[type=item_frame,tag=ourPC,tag=FinDeService,limit=1] run function tch:ligne/test_debut_service
execute if entity @e[type=item_frame,tag=ourPC,tag=FinDeService,limit=1] run tag @s add tempFDS
# (si on est en fin de service mais qu'on a dépassé l'heure de début de service et qu'il est moins de 12h, on considère qu'on est en service)
execute as @s[tag=tempFDS] if score @e[type=item_frame,tag=ourPC,limit=1] debutServiceAjd < #temps currentTdhTick if score #temps currentTdhTick <= #ligne temp run tag @s remove tempFDS
scoreboard players set @s[tag=tempFDS] temp 1
tag @s remove tempFDS

# On lance ensuite la fonction correspondante
execute if score @s temp matches 0 run function tch:ligne/horaire/gen/hors_service
execute if score @s temp matches 1 run function tch:ligne/horaire/gen/fin_de_service
execute if score @s temp matches 2 run function tch:ligne/horaire/gen/incident
execute if score @s temp matches 3 run function tch:ligne/horaire/gen/trafic_perturbe
execute if score @s temp matches 9 run function tch:ligne/horaire/gen/en_service


# On réinitialise notre PC et la variable temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
#scoreboard players reset @s temp
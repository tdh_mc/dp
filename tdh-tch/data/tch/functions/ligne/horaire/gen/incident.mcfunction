## GÉNÉRATION D'UN NOUVEL HORAIRE DE PASSAGE (LIGNE AVEC INCIDENT)
# On choisit aléatoirement un décalage entre l'heure de fin d'incident et l'heure théorique
# Décalage min: 100 ticks, max: la moitié de la fréquence creuse

# On récupère un nombre aléatoire
execute store result score @s spawnAt run data get entity @e[type=#tdh:random,sort=random,limit=1] Rotation[0] 6744.4

# On calcule le modulo (la moitié de la fréquence en heure creuse)
scoreboard players set @s temp 2
scoreboard players operation @s temp2 = @e[type=item_frame,tag=ourPC,limit=1] frequenceCreuse
scoreboard players operation @s temp2 /= @s temp
scoreboard players remove @s temp2 100

# On applique le modulo, puis on ajoute 100 pour obtenir le décalage final
scoreboard players operation @s spawnAt %= @s temp2
scoreboard players add @s spawnAt 100

# Enfin, on calcule l'heure de passage en ajoutant :
# - l'heure de fin d'incident, si on la connaît déjà
# - le tick actuel si on ne la connaît pas encore
execute if score @e[type=item_frame,tag=ourPC,limit=1] status matches 2.. run scoreboard players operation @s spawnAt += @e[type=item_frame,tag=ourPC,limit=1] finIncident
execute if score @e[type=item_frame,tag=ourPC,limit=1] status matches 1 run scoreboard players operation @s spawnAt += #temps currentTdhTick

# On calcule ensuite le jour où l'on va spawner
# Par défaut c'est ajd, mais si l'heure qu'on a générée est inférieure à l'heure actuelle c'est que ce sera demain
execute if score @e[type=item_frame,tag=ourPC,limit=1] status matches 2.. run scoreboard players operation @s idJour = @e[type=item_frame,tag=ourPC,limit=1] idJour
execute if score @e[type=item_frame,tag=ourPC,limit=1] status matches 1 run scoreboard players operation @s idJour = #temps idJour
execute if score @s spawnAt >= #temps fullDayTicks run scoreboard players add @s idJour 1
execute if score @s spawnAt >= #temps fullDayTicks run scoreboard players operation @s spawnAt -= #temps fullDayTicks

# On réinitialise les variables temporaires
scoreboard players reset @s temp
scoreboard players reset @s temp2
## GÉNÉRATION D'UN NOUVEL HORAIRE DE PASSAGE (LIGNE PERTURBÉE)

# On récupère un nombre aléatoire
execute store result score @s spawnAt run data get entity @e[type=#tdh:random,sort=random,limit=1] Rotation[0] 6744.4

# On stocke la fréquence actuelle pour l'utiliser comme modulo du nombre aléatoire
scoreboard players operation @s temp3 = @e[type=item_frame,tag=ourPC,limit=1] frequenceCreuse
execute if score #temps currentTdhTick >= @e[type=item_frame,tag=ourPC,limit=1] debutPointe1 if score #temps currentTdhTick < @e[type=item_frame,tag=ourPC,limit=1] finPointe1 run scoreboard players operation @s temp3 = @e[type=item_frame,tag=ourPC,limit=1] frequencePointe
execute if score #temps currentTdhTick >= @e[type=item_frame,tag=ourPC,limit=1] debutPointe2 if score #temps currentTdhTick < @e[type=item_frame,tag=ourPC,limit=1] finPointe2 run scoreboard players operation @s temp3 = @e[type=item_frame,tag=ourPC,limit=1] frequencePointe

# On applique le modulo, puis on ajoute la fréquence moyenne
# Le résultat final est donc sA = freq + rand(0,freq) = rand(freq,2freq)
scoreboard players operation @s spawnAt %= @s temp3
scoreboard players operation @s spawnAt += @s temp3
# Début de zone PAS TOUCHER À temp3 !!

# Enfin, on calcule l'heure de passage en ajoutant l'heure actuelle
scoreboard players operation @s spawnAt += #temps currentTdhTick

# On calcule ensuite le jour où l'on va spawner
# Par défaut c'est ajd, mais si l'heure qu'on a générée est inférieure à l'heure max c'est que ce sera demain
scoreboard players operation @s idJour = #temps idJour
execute if score @s spawnAt >= #temps fullDayTicks run scoreboard players add @s idJour 1
execute if score @s spawnAt >= #temps fullDayTicks run scoreboard players operation @s spawnAt -= #temps fullDayTicks

# Si on est après la fin de service (et max 1 fréquence creuse après),
# on met l'horaire de spawn juste avant la fin de service
scoreboard players operation @s temp = @s spawnAt
scoreboard players operation @s temp -= @e[type=item_frame,tag=ourPC,limit=1] finServiceAjd
execute if score @s temp matches 0.. if score @s temp <= @e[type=item_frame,tag=ourPC,limit=1] frequenceCreuse run tag @s add tempFDS
# On retire 1 jour si notre nouvel horaire était après minuit mais que la fin de service était avant minuit
execute as @s[tag=tempFDS] if score @e[type=item_frame,tag=ourPC,limit=1] finServiceAjd matches 24001.. if score @s spawnAt matches ..23999 run scoreboard players remove @s idJour 1
execute as @s[tag=tempFDS] run scoreboard players operation @s spawnAt = @e[type=item_frame,tag=ourPC,limit=1] finServiceAjd
execute as @s[tag=tempFDS] run scoreboard players remove @s spawnAt 21
tag @e[tag=tempFDS] remove tempFDS

# Fin de zone PAS TOUCHER À temp3 !!
# Si on est à moins de 1 fréquence creuse (temp3) de la fin de service, on considère que ce train sera le dernier
scoreboard players operation @s temp = @e[type=item_frame,tag=ourPC,limit=1] finServiceAjd
execute if score @s spawnAt > @s temp run scoreboard players operation @s temp += #temps fullDayTicks
scoreboard players operation @s temp -= @s spawnAt
execute if score @s temp <= @s temp3 run tag @s add Dernier

# On réinitialise les variables temporaires
scoreboard players reset @s temp
scoreboard players reset @s temp3


# On affiche un message sur le quai
function tch:ligne/horaire/gen/message_quai
## SÉLECTION DE LA FRÉQUENCE EN HEURE CREUSE

# On récupère la variable idLine
scoreboard players operation #tch idLine = @s config
scoreboard players remove #tch idLine 40000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On calcule la valeur actuelle
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] frequenceCreuse
function tdh:store/time_delay

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config 0
scoreboard players enable @s config

# On demande de choisir la fréquence en heure creuse
tellraw @s [{"text":"CONFIGURATEUR TCH : LIGNE ","color":"gold"},{"selector":"@e[type=item_frame,tag=ourPC]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez la fréquence en heure creuse :","color":"gray"}]
tellraw @s [{"text":""},{"text":"20mn","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 20"},"hoverEvent":{"action":"show_text","value":"20"}},{"text":" ● ","color":"gray"},{"text":"25mn","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 25"},"hoverEvent":{"action":"show_text","value":"25"}},{"text":" ● ","color":"gray"},{"text":"30mn","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 30"},"hoverEvent":{"action":"show_text","value":"30"}},{"text":" ● ","color":"gray"},{"text":"35mn","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 35"},"hoverEvent":{"action":"show_text","value":"35"}}]
tellraw @s [{"text":""},{"text":"40mn","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 40"},"hoverEvent":{"action":"show_text","value":"40"}},{"text":" ● ","color":"gray"},{"text":"50mn","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 50"},"hoverEvent":{"action":"show_text","value":"50"}},{"text":" ● ","color":"gray"},{"text":"1h","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 100"},"hoverEvent":{"action":"show_text","value":"100"}},{"text":" ● ","color":"gray"},{"text":"1h10","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 110"},"hoverEvent":{"action":"show_text","value":"110"}}]
tellraw @s [{"text":""},{"text":"1h20","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 120"},"hoverEvent":{"action":"show_text","value":"120"}},{"text":" ● ","color":"gray"},{"text":"1h30","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 130"},"hoverEvent":{"action":"show_text","value":"130"}},{"text":" ● ","color":"gray"},{"text":"1h45","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 145"},"hoverEvent":{"action":"show_text","value":"145"}},{"text":" ● ","color":"gray"},{"text":"2h","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 200"},"hoverEvent":{"action":"show_text","value":"200"}}]
tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":")"}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
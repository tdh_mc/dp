# On définit les deux variables temporaires (début et fin de l’heure de pointe)
# Le centre de l’heure de pointe est stocké sous la forme HHMM dans #tch temp
# Les valeurs de retour sont #tch temp et #tch temp2

# On commence par convertir la durée d’une demi-heure de pointe (stockée au format HHMM dans @s config)
function tdh:config/calcul/hhmm_to_ticks
scoreboard players operation #tch temp2 = #config temp

# On calcule ensuite l’horaire du centre de l’heure de pointe, en ticks
scoreboard players operation @s config = #tch temp
function tdh:config/calcul/hhmm_to_ticks

# On ajoute / retire enfin la durée d’une demi-heure de pointe à cet horaire pour obtenir le résultat final
scoreboard players operation #tch temp = #config temp
scoreboard players operation #tch temp -= #tch temp2
scoreboard players operation #tch temp2 += #config temp
## SÉLECTION DE LA DURÉE DE L'HEURE DE POINTE 1

# On récupère la variable idLine
scoreboard players operation #tch idLine = @s config
scoreboard players remove #tch idLine 50000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On calcule la valeur actuelle
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] finPointe1
scoreboard players operation #store currentHour -= @e[type=item_frame,tag=ourPC,limit=1] debutPointe1
function tdh:store/time

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config 0
scoreboard players enable @s config

# On demande de choisir la durée de l'heure de pointe
# Les variables scoreboard correspondent à la moitié de la durée indiquée (1000 ticks pour 1h par exemple) car on ajoute la valeur en arrière ET en avant pour calculer les limites de l'heure de pointe.
tellraw @s [{"text":"CONFIGURATEUR TCH : LIGNE ","color":"gold"},{"selector":"@e[type=item_frame,tag=ourPC]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez la MOITIÉ DE la durée de l'heure de pointe 1 :","color":"gray"}]
tellraw @s [{"text":"Pas d'heure de pointe","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set -1"}}]
scoreboard players set #configHeure min 1
scoreboard players set #configHeure max 4
function tdh:config/select/heure
tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","hoverEvent":{"action":"show_text","value":"Ne pas oublier de diviser par 2 ; par exemple 130 (1h30) pour 3h de pointe"},"clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":")"}]
tellraw @s [{"text":""},{"text":"L'heure de pointe 1 est centrée sur ","color":"gray"},{"text":"8h15","color":"red"},{"text":"."}]

# On nettoie le tag temporaire
scoreboard players reset #tch temp
scoreboard players reset #tch currentHour
scoreboard players reset #tch currentMinute
tag @e[type=item_frame,tag=ourPC] remove ourPC
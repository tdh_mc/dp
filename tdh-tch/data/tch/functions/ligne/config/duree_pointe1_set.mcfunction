## ENREGISTREMENT DE LA DURÉE DE L'HEURE DE POINTE 1

# On définit la durée de l'heure de pointe du matin
# Elle est indiquée par la variable config, et la ligne spécifique est indiquée par configID (50011 = M1, 50021 = M2, etc)

# On enregistre cette variable dans #tch idLine
scoreboard players operation #tch idLine = @s configID
scoreboard players remove #tch idLine 50000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On définit la valeur des variables du PC (si c'est négatif, il n'y a pas d'heure de pointe)
scoreboard players set #tch temp 815
execute if score @s config matches 0.. run function tch:ligne/config/duree_pointe_set
execute if score @s config matches ..-1 run function tch:ligne/config/duree_pointe_disable
scoreboard players operation @e[type=item_frame,tag=ourPC,limit=1] debutPointe1 = #tch temp
scoreboard players operation @e[type=item_frame,tag=ourPC,limit=1] finPointe1 = #tch temp2

# On affiche un message de confirmation
scoreboard players operation #store currentHour = #tch temp
function tdh:store/time
tellraw @s [{"text":"L'heure de pointe du matin commencera à ","color":"gray"},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":" (tick "},{"score":{"name":"#tch","objective":"temp"}},{"text":")"}]
scoreboard players operation #store currentHour = #tch temp2
function tdh:store/time
tellraw @s [{"text":"L'heure de pointe du matin se terminera à ","color":"gray"},{"nbt":"time","storage":"tdh:store","interpret":"true"},{"text":" (tick "},{"score":{"name":"#tch","objective":"temp2"}},{"text":")"}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
# Cette fonction est exécutée par un administrateur
# avec configID = 9999 (menu principal) et config = le menu qu'il veut afficher

# Selon le menu qu'on veut afficher
# - Heure de début de service (10000)
execute as @s[scores={config=10000..19999}] run function tch:ligne/config/debut_service_select
# - Heure de fin de service (20000)
execute as @s[scores={config=20000..29999}] run function tch:ligne/config/fin_service_select
# - Fréquence en heure de pointe (30000)
execute as @s[scores={config=30000..39999}] run function tch:ligne/config/frequence_pointe_select
# - Fréquence en heure creuse (40000)
execute as @s[scores={config=40000..49999}] run function tch:ligne/config/frequence_creuse_select
# - Durée de l'heure de pointe 1 (50000)
execute as @s[scores={config=50000..59999}] run function tch:ligne/config/duree_pointe1_select
# - Durée de l'heure de pointe 2 (60000)
execute as @s[scores={config=60000..69999}] run function tch:ligne/config/duree_pointe2_select
# - Service prolongé le week-end soir (70000)
execute as @s[scores={config=70000..79999}] run function tch:ligne/config/service_week_end_soir_select
# - Service prolongé le week-end matin (80000)
execute as @s[scores={config=80000..89999}] run function tch:ligne/config/service_week_end_matin_select
# - Probabilité d'incident (90000)
execute as @s[scores={config=90000..99999}] run function tch:ligne/config/proba_incident_select

# - Quitter le configurateur (-1)
execute as @s[scores={config=-1}] run function tdh:config/end
## SÉLECTION DE LA MODIFICATION DU SERVICE LE WEEK-END MATIN

# On récupère la variable idLine
scoreboard players operation #tch idLine = @s config
scoreboard players remove #tch idLine 80000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On récupère la valeur actuelle
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe
scoreboard players set #tch temp -1
execute if score #store currentHour matches ..-1 run scoreboard players operation #store currentHour *= #tch temp
function tdh:store/time

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config 0
scoreboard players enable @s config

# On demande de choisir la prolongation du service du matin en week-end
tellraw @s [{"text":"CONFIGURATEUR TCH : LIGNE ","color":"gold"},{"selector":"@e[type=item_frame,tag=ourPC]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez l'heure de début de service le samedi/dimanche matin:","color":"gray"}]
tellraw @s [{"text":"Service réduit: ","color":"gray"},{"text":"+1h","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 100"},"hoverEvent":{"action":"show_text","value":"100"}},{"text":" ● ","color":"gray"},{"text":"+2h","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 200"},"hoverEvent":{"action":"show_text","value":"200"}},{"text":" ● ","color":"gray"},{"text":"+3h","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 300"},"hoverEvent":{"action":"show_text","value":"300"}},{"text":" ● ","color":"gray"},{"text":"+4h","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 400"},"hoverEvent":{"action":"show_text","value":"400"}},{"text":" ● ","color":"gray"},{"text":"+5h","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 500"},"hoverEvent":{"action":"show_text","value":"500"}},{"text":" ● ","color":"gray"},{"text":"+6h","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 600"},"hoverEvent":{"action":"show_text","value":"600"}}]
tellraw @s [{"text":"Service prolongé: ","color":"gray"},{"text":"-30mn","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set -30"},"hoverEvent":{"action":"show_text","value":"-30"}},{"text":" ● ","color":"gray"},{"text":"-1h","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set -100"},"hoverEvent":{"action":"show_text","value":"-100"}},{"text":" ● ","color":"gray"},{"text":"-1h30","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set -130"},"hoverEvent":{"action":"show_text","value":"-130"}},{"text":" ● ","color":"gray"},{"text":"-2h","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set -200"},"hoverEvent":{"action":"show_text","value":"-200"}},{"text":" ● ","color":"gray"},{"text":"-2h30","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set -230"},"hoverEvent":{"action":"show_text","value":"-230"}},{"text":" ● ","color":"gray"},{"text":"-3h","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set -300"},"hoverEvent":{"action":"show_text","value":"-300"}}]
tellraw @s [{"text":"Service normal.","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 1"}}]
tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","hoverEvent":{"action":"show_text","value":"<0 pour service prolongé, >0 pour service réduit"},"clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
execute if score @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe matches 0.. run tellraw @s [{"text":"Conserver le réglage actuel (+","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"storage":"tdh:store","nbt":"time","interpret":true},{"text":")"}]
execute if score @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe matches ..-1 run tellraw @s [{"text":"Conserver le réglage actuel (-","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"storage":"tdh:store","nbt":"time","interpret":true},{"text":")"}]

# On nettoie le tag temporaire
scoreboard players reset #tch temp
tag @e[type=item_frame,tag=ourPC] remove ourPC
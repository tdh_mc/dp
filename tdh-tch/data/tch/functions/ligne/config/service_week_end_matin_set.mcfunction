## ENREGISTREMENT DU MODIFICATEUR D'HEURE DE SERVICE LES MATINS DE WEEK-END

# On définit le modificateur d'heure de fin de service les matins de week-end (samedi et dimanche)
# Il est indiqué par la variable config, et la ligne spécifique est indiquée par configID (80011 = M1, 80021 = M2, etc)

# On enregistre cette variable dans #tch idLine
scoreboard players operation #tch idLine = @s configID
scoreboard players remove #tch idLine 80000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On calcule l’heure en ticks à partir de l’input et on la stocke dans #config temp
# (1 correspond à Pas de changement, puisqu'on ne peut pas avoir config=0)
execute if score @s config matches 1 run scoreboard players set #config temp 0
execute unless score @s config matches 1 run function tdh:config/calcul/hhmm_to_ticks

# On définit la valeur de la variable du PC
scoreboard players operation @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe = #config temp

# On affiche un message de confirmation
scoreboard players operation #store currentHour = #config temp
scoreboard players set #tch temp -1
execute if score #store currentHour matches ..-1 run scoreboard players operation #store currentHour *= #tch temp
function tdh:store/time_delay
execute if score #config temp matches 1.. run tellraw @s [{"text":"Les samedis et dimanches matins, le service commencera ","color":"gray"},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":" plus tard (+"},{"score":{"name":"#config","objective":"temp"}},{"text":" ticks)."}]
execute if score #config temp matches ..-1 run tellraw @s [{"text":"Les samedis et dimanches matins, le service commencera ","color":"gray"},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":" plus tôt ("},{"score":{"name":"#config","objective":"temp"}},{"text":" ticks)."}]
execute if score #config temp matches 0 run tellraw @s [{"text":"Les samedis et dimanches matins, pas de modification d'horaires.","color":"gray"}]
scoreboard players reset #tch temp

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
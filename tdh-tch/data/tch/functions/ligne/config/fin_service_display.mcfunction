## AFFICHAGE DE L'HEURE DE FIN DE SERVICE

# On a plusieurs options pour sélectionner une ligne
# Par défaut, on met la variable à 0 pour pouvoir détecter si on a trouvé ou non
scoreboard players set #tch idLine 0
# Si on a nous-mêmes une variable idLine, on la choisit
execute if score @s idLine matches 1.. run scoreboard players operation #tch idLine = @s idLine
# Si on est à la position d'une entité ayant une variable idLine, on la choisit
execute if score #tch idLine matches 0 as @e[distance=..1,scores={idLine=1..}] run scoreboard players operation #tch idLine = @s idLine

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]



# On calcule la valeur actuelle
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] finService
function tdh:store/time

# On affiche le message
execute if entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"La ligne ","color":"gray"},{"entity":"@e[type=item_frame,tag=ourPC,limit=1]","nbt":"Item.tag.display.line.nom","interpret":true},{"text":" termine à "},{"storage":"tdh:store","nbt":"time","interpret":"true"},{"text":" en semaine."}]


# On calcule la valeur en week-end
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] finService
scoreboard players operation #store currentHour += @e[type=item_frame,tag=ourPC,limit=1] finServiceWe
function tdh:store/time

# On affiche le message
execute if entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"La ligne ","color":"gray"},{"entity":"@e[type=item_frame,tag=ourPC,limit=1]","nbt":"Item.tag.display.line.nom","interpret":true},{"text":" termine à "},{"storage":"tdh:store","nbt":"time","interpret":"true"},{"text":" le week-end."}]



# On réinitialise ce qu'on a créé
data remove storage tdh:store timeWe
tag @e[type=item_frame,tag=ourPC] remove ourPC
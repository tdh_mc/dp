## SÉLECTION DE L'HEURE DE FIN DE SERVICE

# On récupère la variable idLine
scoreboard players operation #tch idLine = @s config
scoreboard players remove #tch idLine 20000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On calcule la valeur actuelle
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] finService
function tdh:store/time

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config 0
scoreboard players enable @s config

# On demande de choisir l'heure de fin de service
tellraw @s [{"text":"CONFIGURATEUR TCH : LIGNE ","color":"gold"},{"selector":"@e[type=item_frame,tag=ourPC]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez l'heure de fin de service :","color":"gray"}]
scoreboard players set #configHeure min 18
scoreboard players set #configHeure max 4
function tdh:config/select/heure
tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":")"}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
## ENREGISTREMENT DE LA FRÉQUENCE EN HEURE DE POINTE

# On définit la fréquence en heure de pointe
# Elle est indiquée par la variable config, et la ligne spécifique est indiquée par configID (30011 = M1, 30021 = M2, etc)

# On enregistre cette variable dans #tch idLine
scoreboard players operation #tch idLine = @s configID
scoreboard players remove #tch idLine 30000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On calcule l’heure en ticks à partir de l’input et on la stocke dans #config temp
function tdh:config/calcul/hhmm_to_ticks

# On définit la valeur de la variable du PC (minimum: 10mn)
scoreboard players operation @e[type=item_frame,tag=ourPC,limit=1] frequencePointe = #config temp

# On affiche un message de confirmation
scoreboard players operation #store currentHour = #config temp
function tdh:store/time_delay
tellraw @s [{"text":"La fréquence en heure de pointe a été définie à ","color":"gray"},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":" ("},{"score":{"name":"#config","objective":"temp"}},{"text":" ticks)"}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
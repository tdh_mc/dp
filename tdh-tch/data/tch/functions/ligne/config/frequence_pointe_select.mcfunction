## SÉLECTION DE LA FRÉQUENCE EN HEURE DE POINTE

# On récupère la variable idLine
scoreboard players operation #tch idLine = @s config
scoreboard players remove #tch idLine 30000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On calcule la valeur actuelle
scoreboard players operation #store currentHour = @e[type=item_frame,tag=ourPC,limit=1] frequencePointe
function tdh:store/time_delay

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config 0
scoreboard players enable @s config

# On demande de choisir la fréquence en heure de pointe
tellraw @s [{"text":"CONFIGURATEUR TCH : LIGNE ","color":"gold"},{"selector":"@e[type=item_frame,tag=ourPC]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez la fréquence en heure de pointe :","color":"gray"}]
tellraw @s [{"text":""},{"text":"12mn","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 12"},"hoverEvent":{"action":"show_text","value":"12"}},{"text":" ● ","color":"gray"},{"text":"16mn","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 16"},"hoverEvent":{"action":"show_text","value":"16"}},{"text":" ● ","color":"gray"},{"text":"20mn","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 20"},"hoverEvent":{"action":"show_text","value":"20"}},{"text":" ● ","color":"gray"},{"text":"25mn","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 25"},"hoverEvent":{"action":"show_text","value":"25"}}]
tellraw @s [{"text":""},{"text":"30mn","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 30"},"hoverEvent":{"action":"show_text","value":"30"}},{"text":" ● ","color":"gray"},{"text":"35mn","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 35"},"hoverEvent":{"action":"show_text","value":"35"}},{"text":" ● ","color":"gray"},{"text":"40mn","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 40"},"hoverEvent":{"action":"show_text","value":"40"}},{"text":" ● ","color":"gray"},{"text":"50mn","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 50"},"hoverEvent":{"action":"show_text","value":"50"}}]
tellraw @s [{"text":""},{"text":"1h","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 100"},"hoverEvent":{"action":"show_text","value":"100"}},{"text":" ● ","color":"gray"},{"text":"1h10","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 110"},"hoverEvent":{"action":"show_text","value":"110"}},{"text":" ● ","color":"gray"},{"text":"1h20","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 120"},"hoverEvent":{"action":"show_text","value":"120"}},{"text":" ● ","color":"gray"},{"text":"1h30","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 130"},"hoverEvent":{"action":"show_text","value":"130"}}]
tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":")"}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
# On tick régulièrement pour lancer les sous-fonctions de prise en compte de la configuration
# Le joueur a une variable configID qui sert à stocker la variable qu'il config, et une variable config qu'il trigger pour communiquer la valeur désirée

# Cette fonction est à destination des administrateurs, donc on peut utiliser execute AS au lieu de AT

# Si on est à l'écran de sélection de ligne, on lance la fonction qui nous lancera sur l'écran d'accueil de la ligne
execute as @s[scores={configID=9998}] unless score @s config matches 0 run function tch:ligne/config
# Si on est à l'écran d'accueil, on lance une des fonctions select selon notre choix
execute as @s[scores={configID=9999}] unless score @s config matches 0 run function tch:ligne/config/select

# Selon la variable qu'on cherche à config
# - Dans tous les cas (10000+), si on veut revenir au menu (config -120047):
execute as @s[scores={configID=10000..,config=-120047}] run function tch:ligne/config
# - Heure de début de service (10000)
execute as @s[scores={configID=10000..19999}] unless score @s config matches ..0 run function tch:ligne/config/debut_service_set
# - Heure de fin de service (20000)
execute as @s[scores={configID=20000..29999}] unless score @s config matches ..0 run function tch:ligne/config/fin_service_set
# - Fréquence en heure de pointe (30000)
execute as @s[scores={configID=30000..39999}] unless score @s config matches ..0 run function tch:ligne/config/frequence_pointe_set
# - Fréquence en heure creuse (40000)
execute as @s[scores={configID=40000..49999}] unless score @s config matches ..0 run function tch:ligne/config/frequence_creuse_set
# - Durée de l'heure de pointe 1 (50000)
execute as @s[scores={configID=50000..59999}] unless score @s config matches ..0 run function tch:ligne/config/duree_pointe1_set
# - Durée de l'heure de pointe 2 (60000)
execute as @s[scores={configID=60000..69999}] unless score @s config matches ..0 run function tch:ligne/config/duree_pointe2_set
# - Service prolongé le week-end (70000)
execute as @s[scores={configID=70000..79999}] unless score @s config matches 0 unless score @s config matches -120047 run function tch:ligne/config/service_week_end_soir_set
# - Service prolongé le week-end (80000)
execute as @s[scores={configID=80000..89999}] unless score @s config matches 0 unless score @s config matches -120047 run function tch:ligne/config/service_week_end_matin_set
# - Probabilité d'incident (90000)
execute as @s[scores={configID=90000..99999}] unless score @s config matches ..0 run function tch:ligne/config/proba_incident_set


# Tous les joueurs qui viennent de choisir une config retournent au menu principal
execute as @s[scores={configID=10000..}] unless score @s config matches 0 unless score @s config matches -120047 run function tch:ligne/config
## ENREGISTREMENT DE L'HEURE DE DEBUT DE SERVICE

# On définit l'heure de début de service
# Elle est indiquée par la variable config, et la ligne spécifique est indiquée par configID (10011 = M1, 10021 = M2, etc)

# On enregistre cette variable dans #tch idLine
scoreboard players operation #tch idLine = @s configID
scoreboard players remove #tch idLine 10000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On calcule l’heure en ticks à partir de l’input et on la stocke dans #config temp
function tdh:config/calcul/hhmm_to_ticks

# On définit la valeur de la variable du PC
scoreboard players operation @e[type=item_frame,tag=ourPC,limit=1] debutService = #config temp

# On affiche un message de confirmation
scoreboard players operation #store currentHour = #config temp
function tdh:store/time
tellraw @s [{"text":"L'heure de début de service a été définie à ","color":"gray"},{"nbt":"time","storage":"tdh:store","interpret":true},{"text":" (tick "},{"score":{"name":"#config","objective":"temp"}},{"text":")"}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
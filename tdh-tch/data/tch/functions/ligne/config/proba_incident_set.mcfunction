## ENREGISTREMENT DE LA PROBABILITÉ D'INCIDENT

# On définit la probabilité d'incident
# Elle est indiquée par la variable config, et la ligne spécifique est indiquée par configID (90011 = M1, 90021 = M2, etc)

# On enregistre cette variable dans #tch idLine
scoreboard players operation #tch idLine = @s configID
scoreboard players remove #tch idLine 90000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On définit la valeur de la variable du PC
execute if score @s config matches 0..2000 run scoreboard players operation @e[type=item_frame,tag=ourPC,limit=1] chancesIncident = @s config
execute if score @s config matches ..-1 run scoreboard players set @e[type=item_frame,tag=ourPC,limit=1] chancesIncident 0
execute if score @s config matches 2001.. run scoreboard players set @e[type=item_frame,tag=ourPC,limit=1] chancesIncident 2000

# On affiche un message de confirmation
execute if score @e[type=item_frame,tag=ourPC,limit=1] debutServiceWe matches 1.. run tellraw @s [{"text":"Les chances d'incident sont désormais de ","color":"gray"},{"score":{"name":"@e[type=item_frame,tag=ourPC,limit=1]","objective":"chancesIncident"},"color":"red"},{"text":" pour 2000."}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
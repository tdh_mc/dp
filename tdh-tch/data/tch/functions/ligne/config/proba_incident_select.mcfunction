## SÉLECTION DE LA PROBABILITÉ D'INCIDENT

# On récupère la variable idLine
scoreboard players operation #tch idLine = @s config
scoreboard players remove #tch idLine 90000

# On récupère le bon PC selon la valeur de la variable #tch idLine
function tch:tag/pc
execute unless entity @e[type=item_frame,tag=ourPC,limit=1] run tellraw @s [{"text":"Impossible de trouver le PC de la ligne ","color":"red"},{"score":{"name":"#tch","objective":"idLine"},"color":"dark_red"},{"text":"..."}]

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config 0
scoreboard players enable @s config

# On demande de choisir la probabilité d'incident
tellraw @s [{"text":"CONFIGURATEUR TCH : LIGNE ","color":"gold"},{"selector":"@e[type=item_frame,tag=ourPC]"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez la probabilité d'incident :","color":"gray"}]
tellraw @s [{"text":""},{"text":"1/2 par an","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 4"},"hoverEvent":{"action":"show_text","value":"4"}},{"text":" ● ","color":"gray"},{"text":"1 par an","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 8"},"hoverEvent":{"action":"show_text","value":"8"}},{"text":" ● ","color":"gray"},{"text":"2 par an","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 12"},"hoverEvent":{"action":"show_text","value":"12"}},{"text":" ● ","color":"gray"},{"text":"6 par an","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 40"},"hoverEvent":{"action":"show_text","value":"40"}}]
tellraw @s [{"text":""},{"text":"1 par mois","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 80"},"hoverEvent":{"action":"show_text","value":"80"}},{"text":" ● ","color":"gray"},{"text":"2 par mois","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 200"},"hoverEvent":{"action":"show_text","value":"200"}},{"text":" ● ","color":"gray"},{"text":"1 par semaine","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 400"},"hoverEvent":{"action":"show_text","value":"400"}},{"text":" ● ","color":"gray"},{"text":"3 par semaine","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 1000"},"hoverEvent":{"action":"show_text","value":"1000"}},{"text":" ● ","color":"gray"},{"text":"1 par jour","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 1950"},"hoverEvent":{"action":"show_text","value":"1950"}}]
tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","hoverEvent":{"action":"show_text","value":"En nombre de chances sur 2000 (par exemple, 1000 = 1 chance sur 2 quotidiennes d'incident)"},"clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -120047"}},{"score":{"name":"@e[type=item_frame,tag=ourPC,limit=1]","objective":"chancesIncident"}},{"text":" pour 2000)"}]

# On nettoie le tag temporaire
tag @e[type=item_frame,tag=ourPC] remove ourPC
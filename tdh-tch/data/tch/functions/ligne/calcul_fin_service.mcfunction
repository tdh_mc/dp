## CALCUL DES HORAIRES (FIN DE SERVICE)
# Cette fonction est exécutée par un PC au début du service pour calculer l'heure de fin de service

# Par défaut, on prend l'horaire standard
scoreboard players operation @s finServiceAjd = @s finService

# On calcule 3h et 14h selon la durée actuelle du jour
scoreboard players set #ligne temp 3
scoreboard players set #ligne temp2 14
scoreboard players operation #ligne temp *= #temps ticksPerHour
scoreboard players operation #ligne temp2 *= #temps ticksPerHour

# Si on est vendredi ou samedi, on ajoute aux horaires de fin de service les modificateurs nécessaires
execute if score #temps jourSemaine matches 5..6 run scoreboard players operation @s finServiceAjd += @s finServiceWe
# Si on est dimanche et que l'horaire de fin de service implique une RÉDUCTION du service, on l'applique également
execute if score #temps jourSemaine matches 7 if score @s finServiceWe matches ..-1 run scoreboard players operation @s finServiceAjd += @s finServiceWe

# La fin de service doit être comprise entre 14h et 3h
execute if score @s finServiceAjd >= #temps fullDayTicks run scoreboard players operation @s finServiceAjd -= #temps fullDayTicks
execute if score @s finServiceAjd matches ..-1 run scoreboard players operation @s finServiceAjd += #temps fullDayTicks
execute if score @s finServiceAjd >= #ligne temp if score @s finServiceAjd <= #ligne temp2 run scoreboard players operation @s finServiceAjd = #ligne temp
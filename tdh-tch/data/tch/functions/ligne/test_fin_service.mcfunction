# Exécuté par une ligne pour tester si elle peut terminer son service
# Ce cas est une douille monstrueuse car l'heure de fin peut être le lendemain à 4h par rapport à la date d'exécution

# On calcule 4h et 14h selon la durée actuelle du jour
scoreboard players set #ligne temp 4
scoreboard players set #ligne temp2 14
scoreboard players operation #ligne temp *= #temps ticksPerHour
scoreboard players operation #ligne temp2 *= #temps ticksPerHour

# Si l'heure actuelle est supérieure à l'heure de fin de service pour aujourd'hui,
# et que l'heure de fin de service pour aujourd'hui est supérieure à 14h (l'heure de fin de service minimum)
# On termine le service
execute if score @s finServiceAjd >= #ligne temp2 if score #temps currentTdhTick > @s finServiceAjd run tag @s add FinDeService

# Si l'heure actuelle est supérieure à l'heure de fin de service pour aujourd'hui,
# et que l'heure de fin de service est inférieure à 14h (l'heure de fin de service minimum),
# et que l'heure actuelle est inférieure à 4h (l'heure de début de service minimum)
# On termine le service
execute if score @s finServiceAjd < #ligne temp2 if score #temps currentTdhTick > @s finServiceAjd if score #temps currentTdhTick < #ligne temp run tag @s add FinDeService


# Si on vient de terminer le service, on calcule l'heure de début de service
execute as @s[tag=FinDeService] run function tch:ligne/calcul_debut_service
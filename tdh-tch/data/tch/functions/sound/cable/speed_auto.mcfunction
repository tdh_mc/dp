# Exécuté en tant qu'un câble à sa position, pour choisir le bon fichier son à diffuser en fonction de sa situation

# Selon notre vitesse max par rapport à notre vitesse :
# Si vitesseMax est inférieure à vitesse, on ralentit
execute if score @s[tag=!ArriveeStation,tag=!Station] vitesseMax > @s vitesse run function tch:sound/cable/speed_auto_d
# Si les 2 sont identiques, on est à vitesse de croisière
execute if score @s[tag=!ArriveeStation,tag=!Station] vitesseMax = @s vitesse run function tch:sound/cable/speed_auto_full
# Si vitesseMax est supérieure à vitesse, on accélère
execute if score @s[tag=!ArriveeStation,tag=!Station] vitesseMax < @s vitesse run function tch:sound/cable/speed_auto_u

# Si on arrive en station, on joue le son de rivet
execute as @s[tag=ArriveeStation,tag=!ArriveeStationDone] run function tch:sound/cable/arrivee
# Si on part d'une station, on joue le son de départ
execute as @s[tag=Station] run function tch:sound/cable/depart
# Selon notre vitesse, on diffuse le bon fichier son

# 1 bloc tous les 6t (140%)
execute if score @s vitesse matches ..3 run function tch:sound/cable/speed140d
# 1 bloc tous les 8t (120%)
execute if score @s vitesse matches 4 run function tch:sound/cable/speed120d
# 1 bloc tous les 10t (100%)
execute if score @s vitesse matches 5 run function tch:sound/cable/speed100d
# 1 bloc tous les 12t (90%)
execute if score @s vitesse matches 6 run function tch:sound/cable/speed90d
# 1 bloc tous les 14t (80%)
execute if score @s vitesse matches 7 run function tch:sound/cable/speed80d
# 1 bloc tous les 16t (70%)
execute if score @s vitesse matches 8 run function tch:sound/cable/speed70d
# 1 bloc tous les 18t (60%)
execute if score @s vitesse matches 9 run function tch:sound/cable/speed60d
# 1 bloc tous les 20t (50%)
execute if score @s vitesse matches 10.. run function tch:sound/cable/speed50d
# Exécuté en tant qu'un RER à sa position, pour diffuser un son de standby tant qu’il ne se remet pas en mouvement

# Pas besoin de tag les joueurs puisqu’on est à l’arrêt, on joue en mono et selon la proximité
# Par contre on sauvegarde notre ID de véhicule pour pouvoir émettre à la position de toutes les voitures
scoreboard players reset #rer idVehicule
scoreboard players operation #rer idVehicule = @s idVehicule

execute as @e[type=armor_stand,tag=RER] if score @s idVehicule = #rer idVehicule at @s run playsound tch:br.rer.outside.station.vit0 block @a[distance=..40] ~ ~ ~ 1 1
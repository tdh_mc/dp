# Exécuté en tant qu'un RER à sa position, pour choisir le bon fichier son à diffuser en fonction de sa situation

# On tag tous les joueurs présents dans notre RER, et toutes les voitures du RER
scoreboard players operation #rer idVehicule = @s idVehicule
execute as @e[type=armor_stand,tag=RER] if score #rer idVehicule = @s idVehicule run tag @s add OurRER
execute at @a if score @p[distance=0] idVehicule = #rer idVehicule run tag @a[distance=0] add SoundTargetTemp

# On joue les sons normaux de vitesse (en fonction de si on est dans un tunnel ou non)
function tch:sound/rer/vitesse/test_tunnel

# Si notre vitesse max est de 1 (on freine pour s’arrêter en station),
# on joue le son de freinage
execute if score @s[tag=!SonFreinageDiffuse] vitesseMax matches 1 run function tch:sound/rer/vitesse/test_freinage
# Si notre vitesse max est > à 1, on retire le tag du son de freinage
tag @s[tag=SonFreinageDiffuse,scores={vitesseMax=2..}] remove SonFreinageDiffuse

# Si on arrive en station, on joue le son de rivet
#execute as @s[tag=ArriveeStation,tag=!ArriveeStationDone] run function tch:sound/cable/arrivee
# Si on part d'une station, on joue le son de départ
#execute as @s[tag=Station] run function tch:sound/cable/depart

# On supprime le tag des joueurs et RER
tag @a[tag=SoundTargetTemp] remove SoundTargetTemp
tag @e[type=armor_stand,tag=OurRER] remove OurRER
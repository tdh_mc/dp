# On joue les sons de tous les RER en mouvement selon leur vitesse
execute as @e[type=armor_stand,tag=RERTete,tag=Mouvement] at @s run function tch:sound/rer/vitesse
# On joue les sons des RER à l’arrêt
execute as @e[type=armor_stand,tag=RERTete,tag=!Mouvement] at @s run function tch:sound/rer/station
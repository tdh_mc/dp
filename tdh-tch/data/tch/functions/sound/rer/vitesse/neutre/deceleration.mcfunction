# Selon notre vitesse, on diffuse le bon fichier son

# Vitesse 2→1 (réduite) : < 7 m/s
execute if score @s vitesse matches ..7 run function tch:sound/rer/vitesse/neutre/deceleration/2_1
# Vitesse 3→2 (moyenne) : 8-18 m/s
execute if score @s vitesse matches 8..18 run function tch:sound/rer/vitesse/neutre/deceleration/3_2
# Vitesse 4→3 (élevée) : > 18 m/s
execute if score @s vitesse matches 19.. run function tch:sound/rer/vitesse/neutre/deceleration/4_3
# Selon notre vitesse, on diffuse le bon fichier son

# Vitesse 1 (réduite) : < 5 m/s
execute if score @s vitesse matches ..5 run function tch:sound/rer/vitesse/neutre/croisiere/1
# Vitesse 2 (moyenne) : 6-14 m/s
execute if score @s vitesse matches 6..14 run function tch:sound/rer/vitesse/neutre/croisiere/2
# Vitesse 3 (élevée) : 15-23 m/s
execute if score @s vitesse matches 15..23 run function tch:sound/rer/vitesse/neutre/croisiere/3
# Vitesse 4 (très élevée) : > 23 m/s
execute if score @s vitesse matches 24.. run function tch:sound/rer/vitesse/neutre/croisiere/4
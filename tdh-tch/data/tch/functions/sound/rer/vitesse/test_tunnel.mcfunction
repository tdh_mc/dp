# Ce test vérifie si on est ou non dans un tunnel pour savoir quel ensemble de sons diffuser
# Il doit y avoir au moins un joueur avec le tag SoundTargetTemp, que l’on utilisera pour le test d’insideness

# Si le joueur est à 15 insideness, on joue les sons de tunnel
execute if entity @p[tag=SoundTargetTemp,scores={insideness=15..}] run function tch:sound/rer/vitesse/tunnel
# Sinon, on joue les sons standard
execute unless entity @p[tag=SoundTargetTemp,scores={insideness=15..}] run function tch:sound/rer/vitesse/neutre
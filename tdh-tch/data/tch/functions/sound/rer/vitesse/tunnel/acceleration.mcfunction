# Selon notre vitesse, on diffuse le bon fichier son

# Vitesse 1→2 (réduite) : < 7 m/s
execute if score @s vitesse matches ..7 run function tch:sound/rer/vitesse/tunnel/acceleration/1_2
# Vitesse 2→3 (moyenne) : 8-18 m/s
execute if score @s vitesse matches 8..18 run function tch:sound/rer/vitesse/tunnel/acceleration/2_3
# Vitesse 3→4 (élevée) : > 18 m/s
execute if score @s vitesse matches 19.. run function tch:sound/rer/vitesse/tunnel/acceleration/3_4
# On trouve la voie qui correspond à notre ID de ligne
function tch:tag/voie

# On se donne un tag pour signifier qu’on a joué le son de freinage (sera retiré si on repasse au-dessus d’une certaine vitesse)
tag @s add SonFreinageDiffuse

# On joue le son de freinage en stéréo à tous les joueurs du RER et en mono à la position de la voie pour tous les autres
playsound tch:br.rer.inside.station.arrivee block @a[tag=SoundTargetTemp] ~ ~ ~ 1 1 1
execute at @e[type=#tch:marqueur/quai,tag=ourVoie,sort=nearest,limit=1] run playsound tch:br.rer.outside.station.arrivee block @a[tag=!SoundTargetTemp] ~ ~ ~ 4 1

# On retire le tag de notre voie
tag @e[type=#tch:marqueur/quai,tag=ourVoie] remove ourVoie
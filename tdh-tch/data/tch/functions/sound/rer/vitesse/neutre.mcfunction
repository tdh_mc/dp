# Selon notre vitesse max par rapport à notre vitesse :
# Si vitesseMax est inférieure à vitesse, on ralentit
execute if score @s vitesseMax < @s vitesse run function tch:sound/rer/vitesse/neutre/deceleration
# Si les 2 sont identiques, on est à vitesse de croisière
execute if score @s vitesseMax = @s vitesse run function tch:sound/rer/vitesse/neutre/croisiere
# Si vitesseMax est supérieure à vitesse, on accélère
execute if score @s vitesseMax > @s vitesse run function tch:sound/rer/vitesse/neutre/acceleration
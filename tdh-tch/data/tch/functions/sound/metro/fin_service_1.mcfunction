# On joue cette annonce à la position de l'armor stand "Direction" d'une ligne correspondante,
# seulement si un joueur est présent à moins de 30 blocs

execute if score @p insideness matches 15.. run playsound tch:annonce.trafic.x.fin_service.1 record @a[distance=..30] ~ ~ ~ 1 1 0.1
execute if score @p insideness matches ..14 run playsound tch:annonce.trafic.x.fin_service.1_clean record @a[distance=..30] ~ ~ ~ 1 1 0.1
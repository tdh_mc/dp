# Désolé mais on ne peut pas encore spécifier de variable dans le sound event d'un playsound
# Le jour où ça sera possible ce code passe de 6000 fonctions à 3...
execute if score @s voix matches ..1 run function tch:sound/arrivee_station2/voix1/jouer
execute if score @s voix matches 2 run function tch:sound/arrivee_station2/voix2/jouer
execute if score @s voix matches 3 run function tch:sound/arrivee_station2/voix3/jouer
execute if score @s voix matches 4 run function tch:sound/arrivee_station2/voix4/jouer
execute if score @s voix matches 5 run function tch:sound/arrivee_station2/voix5/jouer
execute if score @s voix matches 6.. run function tch:sound/arrivee_station2/voix6/jouer
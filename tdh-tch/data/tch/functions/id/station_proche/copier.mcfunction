# Si on n'a pas d'ID de station, on essaie de le récupérer
execute unless score @s idStation matches 1.. run function tch:station/get_id

# On enregistre notre ID de station
scoreboard players operation #tchID idStation = @s idStation
# On exécute cette fonction en tant que n'importe quelle entité, à sa propre position,
# pour trouver la station TCH la plus proche d'elle et copier son score idStation

scoreboard players set #tchID idStation 0
execute as @e[type=#tch:marqueur/station,tag=NomStation,sort=nearest,distance=..200,limit=1] run function tch:id/station_proche/copier
execute unless score #tchID idStation matches 0 run scoreboard players operation @s idStation = #tchID idStation
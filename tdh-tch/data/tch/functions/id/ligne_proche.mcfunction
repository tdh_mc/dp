# On exécute cette fonction en tant que n'importe quelle entité, à sa propre position,
# pour trouver la ligne TCH la plus proche d'elle et copier son score idLine/idLineMax si applicable

scoreboard players set #tchID idLine 0
scoreboard players set #tchID idLineMax 0
execute as @e[type=armor_stand,tag=NumLigne,sort=nearest,distance=..200,limit=1] run function tch:id/ligne_proche/copier
execute unless score #tchID idLine matches 0 run scoreboard players operation @s idLine = #tchID idLine
execute unless score #tchID idLineMax matches 0 run scoreboard players operation @s idLineMax = #tchID idLineMax
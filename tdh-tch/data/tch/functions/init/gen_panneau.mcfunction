# On génère une item frame "TexteTemporaire" à notre position,
# pour matérialiser l'emplacement où on pourra faire apparaître des panneaux temporaires
# pour interpréter du texte NBT et le "fixer" sous forme stockable
execute align xyz run summon item_frame ~.5 ~.03125 ~.5 {Tags:["TexteTemporaire"],Item:{id:"minecraft:spruce_sign",Count:1b,tag:{display:{Name:'"TexteTemporaire"'}}},Fixed:1b,Invulnerable:1b,Facing:1b}
setblock ~ ~ ~ spruce_sign
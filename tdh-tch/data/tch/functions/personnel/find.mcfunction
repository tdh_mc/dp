# Cette fonction est exécutée :
# avec la variable #tchPersonnel temp assignée au nombre d’employéEs dans la liste,
# et la variable #tchPersonnel idAbonnement assignée à l’ID de la personne que l'on recherche

# On stocke l'ID actuel dans temp2
execute store result score #tchPersonnel temp2 run data get storage tch:personnel actifs[0].id
# On retranche l'ID qu'on recherche de temp2 ; si temp2 est égal à 0, alors on aura trouvé l’employéE
scoreboard players operation #tchPersonnel temp2 -= #tchPersonnel idAbonnement

# Si l'ID est le même que celui qu'on génère, on copie les résultats
execute if score #tchPersonnel temp2 matches 0 run function tch:personnel/find/succes
# Sinon, on enregistre l'échec et on continue à itérer
execute unless score #tchPersonnel temp2 matches 0 run function tch:personnel/find/echec
# On copie le premier élément du tableau à la fin
data modify storage tch:personnel actifs append from storage tch:personnel actifs[0]

# On supprime le premier élément du tableau
data remove storage tch:personnel actifs[0]
# On passe les résultats
scoreboard players set #tchPersonnel idAbonnement 1
execute store result score #tchPersonnel niveauAcces run data get storage tch:personnel actifs[0].acces

# On copie les données complètes de l'employéE dans les résultats
data modify storage tch:personnel resultat set from storage tch:personnel actifs[0]
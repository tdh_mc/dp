# On vient de vérifier un ID d’employéE qui n'était pas celui qu'on cherchait

# On enregistre le fait qu'on a vérifié un terme
scoreboard players remove #tchPersonnel temp 1

# On passe à la personne suivante
function tch:personnel/_suivant


# S'il ne reste plus d'itération à faire, on enregistre l'échec
execute unless score #tchPersonnel temp matches 1.. run scoreboard players set #tchPersonnel idAbonnement -1

# Sinon, on continue à itérer
execute if score #tchPersonnel temp matches 1.. run function tch:personnel/find
# On stocke le jour et l'heure de dernière validation
execute store result score #store idJour run data get storage tch:personnel actifs[0].date_debut
function tdh:store/day

# On affiche les données du badge courant
tellraw @s [{"text":" - Badge ","color":"gray"},{"text":"#","color":"gold","extra":[{"nbt":"actifs[0].id","storage":"tch:personnel"}]},{"text":" : accès niveau "},{"nbt":"actifs[0].acces","storage":"tch:personnel","color":"yellow"},{"text":". "},{"text":"Actif depuis le ","italic":"true","extra":[{"nbt":"day","storage":"tdh:store","interpret":true,"color":"yellow"},{"text":"."}]},{"text":" Possesseur : "},{"nbt":"actifs[0].nom","storage":"tch:personnel","interpret":true,"color":"gold"},{"text":"."}]

# On décrémente notre compteur de badges
scoreboard players remove #tch temp 1

# S'il reste des badges, on itère une nouvelle fois
execute if score #tch temp matches 1.. run function tch:personnel/_suivant
execute if score #tch temp matches 1.. run function tch:personnel/afficher/actifs
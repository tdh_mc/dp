# On affiche tous les abonnements actifs au joueur qui l'exécute
tellraw @s [{"text":"--- Badges du personnel TCH actifs :","color":"gold"}]

# On stocke le nombre d'abonnements actifs
execute store result score #tch temp run data get storage tch:personnel actifs

# On itère chaque badge en affichant ses données
execute if score #tch temp matches 1.. run function tch:personnel/afficher/actifs

# On avance une dernière fois pour ne pas décaler la liste des abonnements après l'exécution de cette fonction
# (même si en principe on se fout de l'ordre, ça voudrait dire qu'un /data modify avec un index précis
# ne correspondrait pas à l'index affiché par la dernière fonction d'affichage exécutée... ce qui serait con)
function tch:personnel/_suivant
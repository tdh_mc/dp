# Ajoute une nouvelle entrée pour la personne venant d'entrer chez TCH,
# pour laquelle on a généré un nouvel ID

# Voilà toutes les variables à définir :
# #tchPersonnel idAbonnement = l’ID nouvellement généré
# #tchPersonnel niveauAcces = le niveau d’accès désiré
data modify storage tch:personnel actifs prepend value {id:0,date_debut:0,acces:0}
execute store result storage tch:personnel actifs[0].id int 1 run scoreboard players get #tchPersonnel idAbonnement
execute store result storage tch:personnel actifs[0].acces int 1 run scoreboard players get #tchPersonnel niveauAcces
execute store result storage tch:personnel actifs[0].date_debut int 1 run scoreboard players get #temps idJour
# On l'ajoute aussi comme résultat de la recherche pour en faciliter l'accès
data modify storage tch:personnel resultat set from storage tch:personnel actifs[0]
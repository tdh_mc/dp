# On archive l'employé actuel ; on le retire du fichier du personnel pour le placer dans l'archive du personnel
data modify storage tch:personnel archive append from storage tch:personnel actifs[0]
# On ajoute la date de fin à cet employé
execute store result storage tch:personnel archive[-1].date_fin int 1 run scoreboard players get #temps idJour

# On supprime l’employé du fichier principal
data remove storage tch:personnel actifs[0]
# Cette fonction a vocation à être utilisée depuis d'autres dossiers de TCH
# Elle n'est exécutée par personne, il suffit de définir #tchPersonnel idAbonnement à l'ID qu'on cherche
# Si la fonction trouve l'employéE, elle définira l'idAbonnement à 1 ; sinon, elle le définira à -1
# Si elle trouve l'employé, elle renverra aussi son niveau d’accès dans #tchPersonnel niveauAcces

# Structure du storage du personnel (tch:personnel) :
# {
#	actifs: [
#		{ id: <INT>,
#		  date_debut: <INT>,
#		  acces: <INT> },
#		{ ... },
#		{ ... }
#	],
#	archive: [
#		{ id: <INT>,
#		  date_debut: <INT>,
#		  date_fin: <INT>,
#		  acces: <INT> },
#		{ ... },
#		{ ... }
#	],
#	resultat: {		(peut être absent)
#		id: <INT>,
#		date_debut: <INT>,
#		date_fin: <INT>,
#		acces: <INT>
#	}
# }

# On passe les variables nécessaires pour la fonction find
execute store result score #tchPersonnel temp run data get storage tch:personnel actifs

# On exécute la fonction de recherche
function tch:personnel/find
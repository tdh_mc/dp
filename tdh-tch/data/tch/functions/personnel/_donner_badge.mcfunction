# Cette fonction permet de faire apparaître un badge correspondant à l’ID personnel passé dans #tchPersonnel idAbonnement
# Aucune vérification de la validité de l’ID n’est faite

summon item ~ ~ ~ {Invulnerable:1b,Tags:["tchBadgeTemp"],PickupDelay:0s,Age:0s,Item:{id:"stick",Count:1b,tag:{CustomModelData:1000,display:{Name:'"Badge TCH"'},tch:{type:66b,id_personnel:0}}}}
execute store result entity @e[type=item,tag=tchBadgeTemp,sort=nearest,limit=1] Item.tag.tch.id_personnel int 1 run scoreboard players get #tchPersonnel idAbonnement
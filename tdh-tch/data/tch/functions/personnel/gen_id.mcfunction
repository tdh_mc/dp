# Cette fonction génère un identifiant et le stocke dans #tchPersonnel idAbonnement, puis vérifie s'il existe déjà

# On prend un nombre aléatoire entre 10'000 et 99'999
scoreboard players set #random min 10000
scoreboard players set #random max 100000
function tdh:random

# On stocke tout de suite ce nombre aléatoire dans #tchPersonnel idAbonnement pour chercher s'il existe déjà
scoreboard players operation #tchPersonnel idAbonnement = #random temp

# On vérifie que l'identifiant est bien unique
# On compare chaque ID d'employéE
function tch:personnel/_find

# Si l'ID est bien unique, on stocke à nouveau la variable (l'appel à _find overwrite la variable)
execute if score #tchPersonnel idAbonnement matches -1 run scoreboard players operation #tchPersonnel idAbonnement = #random temp
# Si l'ID n'est pas unique, on en génère un nouveau
execute if score #tchPersonnel idAbonnement matches 1 run function tch:personnel/gen_id
# On copie le dernier élément du tableau au début
data modify storage tch:personnel archive prepend from storage tch:personnel archive[-1]

# On supprime le dernier élément du tableau
data remove storage tch:personnel archive[-1]
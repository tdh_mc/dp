# On copie le premier élément du tableau à la fin
data modify storage tch:personnel archive append from storage tch:personnel archive[0]

# On supprime le premier élément du tableau
data remove storage tch:personnel archive[0]
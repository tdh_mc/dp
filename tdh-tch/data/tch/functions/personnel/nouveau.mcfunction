# On génère un nouveau profil d’employéE
# Si c'est pour un joueur, on doit l'avoir taggé avec le tag tchNouvelEmploye

# On commence par ajouter un profil vierge à l'index
# Un employé commence par défaut au niveau d’accès 1 (minimal)
data modify storage tch:personnel actifs prepend value {id:0,date_debut:0,acces:1}

# On génère un nouvel ID
function tch:personnel/gen_id

# On ajoute ce nouvel ID au profil
execute store result storage tch:personnel actifs[0].id int 1 run scoreboard players get #tchPersonnel idAbonnement
execute store result storage tch:personnel actifs[0].date_debut int 1 run scoreboard players get #temps idJour

# On enregistre le nom du joueur
execute if entity @p[tag=tchNouvelEmploye] at @e[type=item_frame,tag=TexteTemporaire,sort=random,limit=1] run function tch:personnel/sauvegarder_nom

# Enfin, on remplace les derniers résultats par ce nouvel élément, pour faciliter son accès
data modify storage tch:personnel resultat set from storage tch:personnel actifs[0]
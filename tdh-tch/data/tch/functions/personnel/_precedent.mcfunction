# On copie le dernier élément du tableau au début
data modify storage tch:personnel actifs prepend from storage tch:personnel actifs[-1]

# On supprime le dernier élément du tableau
data remove storage tch:personnel actifs[-1]
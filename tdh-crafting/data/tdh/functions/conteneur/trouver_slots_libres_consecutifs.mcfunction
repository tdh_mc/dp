# On exécute cette fonction pour trouver les premiers slots libres consécutifs dans le conteneur situé à sa position d'exécution

# Arguments :
# - #conteneur nombre : le nombre de slots libres consécutifs à trouver

# Valeurs de retour :
# - #conteneur temp : <0 si la recherche a échoué, le numéro du premier slot sinon
# - storage tdh:conteneur resultat (contient une liste de tous les slots demandés)
data remove storage tdh:conteneur resultat
scoreboard players set #conteneur temp -1

# On vérifie que le bloc est bien un conteneur en définissant le max de slots disponibles
scoreboard players set #conteneur max 0
execute if block ~ ~ ~ #tdh:conteneur/5 run scoreboard players set #conteneur max 5
execute if block ~ ~ ~ #tdh:conteneur/9 run scoreboard players set #conteneur max 9
execute if block ~ ~ ~ #tdh:conteneur/27 run scoreboard players set #conteneur max 27

# On n'exécute la suite des commandes que si le bloc est bien un conteneur
execute unless score #conteneur max matches 1.. run scoreboard players set #conteneur temp -10
execute if score #conteneur max matches 1.. run function tdh:conteneur/trouver_slots_libres_consecutifs/init_boucle
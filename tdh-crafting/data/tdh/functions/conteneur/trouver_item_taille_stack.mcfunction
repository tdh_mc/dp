# On exécute cette fonction pour trouver le premier item correspondant à celui qu'on recherche dans le conteneur situé à sa position d'exécution, SI la taille du stack est comprise entre le min et le max spécifiés

# Arguments :
# - #conteneurStack min et #conteneurStack max spécifient le minimum et le maximum du stack (inclusifs)
# - storage tdh:conteneur recherche.item (contient les champs "id" et éventuellement "tag")
#	(il n'y a pas besoin que tag comprenne TOUS les éléments du tag, simplement ceux qu'on veut rechercher)
#	(ne SURTOUT PAS spécifier le tag "Count", car il ne matcherait que cette taille de stack exacte)

# Valeur de retour :
# - #conteneur temp, indique un numéro de slot (ou <=-1 s'il n'y a pas de slot avec l'item qu'on recherche)
# - storage tdh:conteneur resultat (copie de l'item qu'on recherche)
data remove storage tdh:conteneur resultat

# Par défaut, on renvoie -1 (= pas de résultat)
scoreboard players set #conteneur temp -1

# On vérifie que le bloc est bien un conteneur en définissant le max de slots disponibles
scoreboard players set #conteneur max 0
execute if block ~ ~ ~ #tdh:conteneur/5 run scoreboard players set #conteneur max 5
execute if block ~ ~ ~ #tdh:conteneur/9 run scoreboard players set #conteneur max 9
execute if block ~ ~ ~ #tdh:conteneur/27 run scoreboard players set #conteneur max 27

# On n'exécute la suite des commandes que si le bloc est bien un conteneur
execute unless score #conteneur max matches 1.. run scoreboard players set #conteneur temp -10
execute if score #conteneur max matches 1.. run function tdh:conteneur/trouver_item_taille_stack/init_boucle
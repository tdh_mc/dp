# On cherche à insérer un item qui ne se stacke pas

# On essaie de trouver un slot libre dans le conteneur
function tdh:conteneur/trouver_slot_libre

# S'il y a un slot libre, on insère l'objet dans ce slot
execute if score #conteneur temp matches 0.. run function tdh:conteneur/inserer_items/inserer_item/succes/slot_libre
# Sinon, on arrête la boucle
execute if score #conteneur temp matches ..-1 run function tdh:conteneur/inserer_items/echec
# On cherche à insérer un item qui se stacke à hauteur de 64

# On essaie de trouver un slot contenant cet item dans le conteneur, mais n'étant pas encore plein
data modify storage tdh:conteneur recherche.item set value {id:""}
data modify storage tdh:conteneur recherche.item.id set from storage tdh:conteneur recherche.items[0].id
data modify storage tdh:conteneur recherche.item.tag set from storage tdh:conteneur recherche.items[0].tag
scoreboard players set #conteneurStack min 1
scoreboard players set #conteneurStack max 63
function tdh:conteneur/trouver_item_taille_stack

# S'il y a un slot contenant notre item, on le remplit autant que possible
execute if score #conteneur temp matches 0.. run function tdh:conteneur/inserer_items/inserer_item/succes/slot_existant_64
# Sinon, on essaie de trouver plutôt un slot libre
execute if score #conteneur temp matches ..-1 run function tdh:conteneur/inserer_items/inserer_item/non_stackable
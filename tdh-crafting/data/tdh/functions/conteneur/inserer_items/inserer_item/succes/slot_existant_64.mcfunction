# On insère le premier objet de notre liste dans le slot indiqué par la variable #conteneur temp
# Ce slot contient déjà le même item que nous, on fait donc simplement évoluer la taille de son stack

# On calcule la taille totale qu'aurait le stack si on y ajoutait tous les items actuels
execute store result score #conteneur temp7 run data get storage tdh:conteneur recherche.items[0].Count
execute store result score #conteneur temp8 run data get storage tdh:conteneur contenu[0].Count
scoreboard players operation #conteneur temp7 += #conteneur temp8

# Si cette taille est inférieure ou égale à 64, on peut simplement modifier le nombre d'items et supprimer notre item à insérer
execute if score #conteneur temp7 matches ..64 run function tdh:conteneur/inserer_items/inserer_item/succes/slot_existant/total
# Dans le cas contraire, on définit la taille du stack à 64 et on conserve (total-64) items dans notre file d'attente pour les insérer plus loin dans le conteneur
execute if score #conteneur temp7 matches 65.. run function tdh:conteneur/inserer_items/inserer_item/succes/slot_existant/partiel

# On a déjà enregistré le nombre total d'items (existant dans le slot + ceux qu'on va ajouter)
# dans la variable #conteneur temp7

# On calcule dans temp7 le reste (les items qui vont déborder du stack de 64)
scoreboard players set #conteneur temp8 64
scoreboard players operation #conteneur temp7 -= #conteneur temp8
# On a désormais séparé notre variable en deux, avec 64 dans temp8 et le reste dans temp7

# On enregistre que le stack du conteneur fait désormais 64
execute store result storage tdh:conteneur contenu[0].Count byte 1 run scoreboard players get #conteneur temp8
data modify block ~ ~ ~ Items set from storage tdh:conteneur contenu

# On modifie le nombre d'objets restants dans ce slot de notre liste d'objets en attente
execute store result storage tdh:conteneur recherche.items[0].Count byte 1 run scoreboard players get #conteneur temp7

# On ne décrémente PAS notre variable de contrôle, car on n'a pas épuisé ce stack d'items en attente
#scoreboard players remove #conteneurBoucle nombre 0

# On essaie d'insérer ces items restants plus loin dans le conteneur
function tdh:conteneur/inserer_items/inserer_item
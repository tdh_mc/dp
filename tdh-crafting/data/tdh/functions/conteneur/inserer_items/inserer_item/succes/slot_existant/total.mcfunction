# On a déjà enregistré le nombre total d'items (existant dans le slot + ceux qu'on va ajouter)
# dans la variable #conteneur temp7

# On se contente de modifier la taille du stack existant dans le conteneur
execute store result storage tdh:conteneur contenu[0].Count byte 1 run scoreboard players get #conteneur temp7
data modify block ~ ~ ~ Items set from storage tdh:conteneur contenu

# On retire cet objet de notre liste
data remove storage tdh:conteneur recherche.items[0]

# On décrémente notre variable de contrôle
scoreboard players remove #conteneurBoucle nombre 1

# Si on a fini, on donne le résultat et on termine la fonction
execute if score #conteneurBoucle nombre matches ..0 run function tdh:conteneur/inserer_items/succes
# S'il reste des items à insérer, on essaie d'insérer le suivant
execute if score #conteneurBoucle nombre matches 1.. run function tdh:conteneur/inserer_items/inserer_item
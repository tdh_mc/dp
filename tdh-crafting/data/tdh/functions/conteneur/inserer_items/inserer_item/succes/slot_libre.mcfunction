# On insère le premier objet de notre liste dans le slot indiqué par la variable #conteneur temp
execute store result storage tdh:conteneur recherche.items[0].Slot byte 1 run scoreboard players get #conteneur temp
data modify block ~ ~ ~ Items append from storage tdh:conteneur recherche.items[0]

# On retire cet objet de notre liste
data remove storage tdh:conteneur recherche.items[0]

# On décrémente notre variable de contrôle
scoreboard players remove #conteneurBoucle nombre 1

# Si on a fini, on donne le résultat et on termine la fonction
execute if score #conteneurBoucle nombre matches ..0 run function tdh:conteneur/inserer_items/succes
# S'il reste des items à insérer, on essaie d'insérer le suivant
execute if score #conteneurBoucle nombre matches 1.. run function tdh:conteneur/inserer_items/inserer_item
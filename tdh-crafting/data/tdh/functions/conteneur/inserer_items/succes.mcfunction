# On n'a pas besoin de définir la variable résultat (#conteneur temp)
# puisque la dernière opération d'insertion a déjà renvoyé >=0 (sinon elle aurait échoué et nous aussi)

# On enregistre simplement une liste vide comme résultat
data modify storage tdh:conteneur resultat set value []
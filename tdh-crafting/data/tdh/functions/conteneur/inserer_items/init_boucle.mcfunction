# On initialise les données dont on aura besoin pour la boucle

# On enregistre le nombre d'éléments dans cette liste d'items
execute store result score #conteneurBoucle nombre run data get storage tdh:conteneur recherche.items

# On essaie d'insérer le premier item de la liste, puis les autres de manière récursive
execute if score #conteneurBoucle nombre matches 1.. run function tdh:conteneur/inserer_items/inserer_item
# On n'a pas besoin de définir la variable résultat (#conteneur temp)
# puisque la dernière opération d'insertion a déjà renvoyé <0 (sinon elle aurait réussi et nous aussi)

# On enregistre simplement les items qu'on a pas réussi à insérer comme résultat
data modify storage tdh:conteneur resultat set from storage tdh:conteneur recherche.items
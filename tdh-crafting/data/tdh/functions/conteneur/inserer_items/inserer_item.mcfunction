# On a déjà enregistré dans #conteneurBoucle nombre le nombre d'items à ajouter
# Chacun des items est stocké dans tdh:conteneur recherche.items, et on cherche à insérer recherche.items[0]

# On a une version différente de cette fonction selon si on essaie d'insérer un objet avec de la durabilité (outil/arme/armure, qui ne se stacke jamais) ou non
# TODO: Gérer séparément les objets ayant une taille de stack maximale de 16. Mais ça sera pas avant qu'ils aient ajouté une manière de checker des item tags dans un conteneur, parce que je vais pas faire 75 checks différents par insertion d'items.

# Le test exact est :
# - NI un tag Damage (durabilité, appliqué uniquement aux armes/armures/outils, donc tous non stackables)
# - NI un tag Potion ou CustomPotionEffects (appliqués uniquement aux potions, non stackables)
execute store result score #conteneur temp5 unless data storage tdh:conteneur recherche.items[0].tag.Damage unless data storage tdh:conteneur recherche.items[0].tag.Potion unless data storage tdh:conteneur recherche.items[0].tag.CustomPotionEffects

# On tente d'insérer l'objet selon la méthode appropriée à son type
execute if score #conteneur temp5 matches ..0 run function tdh:conteneur/inserer_items/inserer_item/non_stackable
execute if score #conteneur temp5 matches 1.. run function tdh:conteneur/inserer_items/inserer_item/stackable_64
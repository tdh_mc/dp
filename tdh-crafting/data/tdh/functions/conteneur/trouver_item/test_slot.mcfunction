# On copie dans un buffer l'item contenu dans le slot actuel
data modify storage tdh:conteneur buffer set from storage tdh:conteneur contenu[0]
execute store success score #conteneur temp2 run data modify storage tdh:conteneur buffer merge from storage tdh:conteneur recherche.item

# Si on n'a pas pu copier l'item, c'est qu'ils étaient identiques ; on termine donc la boucle
execute unless score #conteneur temp2 matches 1.. run function tdh:conteneur/trouver_item/succes

# Dans le cas contraire, on passe au slot suivant
execute if score #conteneur temp2 matches 1.. run function tdh:conteneur/trouver_item/prochain_slot
# On enregistre l'item résultat dans le storage
data modify storage tdh:conteneur resultat set from storage tdh:conteneur contenu[0]

# On définit la variable résultat
execute store result score #conteneur temp run data get storage tdh:conteneur resultat.Slot
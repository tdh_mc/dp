# On supprime un slot de notre inventaire sauvegardé
data modify storage tdh:conteneur contenu append from storage tdh:conteneur contenu[0]
data remove storage tdh:conteneur contenu[0]

# On fait évoluer notre variable de contrôle
scoreboard players remove #conteneur nombre 1

# On recommence la fonction s'il reste des éléments
execute if score #conteneur nombre matches 1.. run function tdh:conteneur/trouver_item/test_slot
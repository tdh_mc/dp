# On initialise les données dont on aura besoin pour la boucle

# On copie le contenu du conteneur dans un storage temporaire
data modify storage tdh:conteneur contenu set from block ~ ~ ~ Items
# On enregistre le nombre d'items dans le conteneur
execute store result score #conteneur nombre run data get storage tdh:conteneur contenu

# S'il n'y a pas d'item dans le conteneur, il ne peut pas y avoir l'item qu'on recherche ; on renvoie donc -1
#execute if score #conteneur nombre matches ..0 run scoreboard players set #conteneur temp -1
# S'il y a au moins un item, on teste le premier slot (puis les autres par récursion)
execute if score #conteneur nombre matches 1.. run function tdh:conteneur/trouver_item_taille_stack/test_slot
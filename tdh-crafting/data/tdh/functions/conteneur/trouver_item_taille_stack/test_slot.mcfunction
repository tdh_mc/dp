# On copie dans un buffer l'item contenu dans le slot actuel
data modify storage tdh:conteneur buffer set from storage tdh:conteneur contenu[0]
execute store success score #conteneur temp2 run data modify storage tdh:conteneur buffer merge from storage tdh:conteneur recherche.item

# Si on n'a pas pu copier l'item, on vérifie que la taille du stack correspond à ce qu'on recherche
# Dans le cas contraire, cette sous-fonction passera la valeur de #conteneur temp2 à >1 pour activer la condition suivante, et tester les slots ultérieurs
execute unless score #conteneur temp2 matches 1.. run function tdh:conteneur/trouver_item_taille_stack/test_stack

# Si ce slot n'a pas marché, on passe au slot suivant
execute if score #conteneur temp2 matches 1.. run function tdh:conteneur/trouver_item_taille_stack/prochain_slot
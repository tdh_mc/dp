# On enregistre dans une variable temporaire la taille du stack actuel
execute store result score #conteneur temp3 run data get storage tdh:conteneur contenu[0].Count

# Si la taille du stack ne correspond pas, on repasse temp2 à 1 pour passer au prochain slot dans test_slot
execute if score #conteneur temp3 < #conteneurStack min run scoreboard players set #conteneur temp2 1
execute if score #conteneur temp3 > #conteneurStack max run scoreboard players set #conteneur temp2 1

# Si temp2 n'est toujours pas positif, c'est que la taille de ce stack matche ce qu'on recherche, et on s'arrête là
execute if score #conteneur temp2 matches ..0 run function tdh:conteneur/trouver_item_taille_stack/succes
# On exécute cette fonction pour trouver les premiers slots libres dans le conteneur situé à sa position d'exécution

# Arguments :
# - #conteneur nombre : le nombre de slots libres à trouver

# Valeurs de retour :
# - #conteneur temp : <0 si la recherche a échoué, >=0 sinon
# - storage tdh:conteneur resultat (contient une liste de tous les slots demandés)
data remove storage tdh:conteneur resultat
scoreboard players set #conteneur temp -10

# On vérifie que le bloc est bien un conteneur en définissant le max de slots disponibles
scoreboard players set #conteneur max 0
execute if block ~ ~ ~ #tdh:conteneur/5 run scoreboard players set #conteneur max 5
execute if block ~ ~ ~ #tdh:conteneur/9 run scoreboard players set #conteneur max 9
execute if block ~ ~ ~ #tdh:conteneur/27 run scoreboard players set #conteneur max 27

# On n'exécute la suite des commandes que si le bloc est bien un conteneur
execute if score #conteneur max matches 1.. run function tdh:conteneur/trouver_slots_libres/init_boucle
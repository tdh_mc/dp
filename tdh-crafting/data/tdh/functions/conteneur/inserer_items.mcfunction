# On exécute cette fonction pour insérer des items dans le conteneur situé à cette position

# Arguments :
# - storage tdh:conteneur recherche.items (contient une liste d'items avec pour chacun les champs "id", "Count" et éventuellement "tag") (mais pas "Slot")

# Valeurs de retour :
# - #conteneur temp : <0 si l'insertion a échoué, >=0 sinon
# - storage tdh:conteneur resultat (contient le reste de l'opération d'insertion ; identique à l'input s'il a été totalement impossible de l'insérer, 0 blocs d'air s'il a été totalement inséré)
data remove storage tdh:conteneur resultat
scoreboard players set #conteneur temp -10

# On vérifie que le bloc est bien un conteneur en définissant le max de slots disponibles
scoreboard players set #conteneur max 0
execute if block ~ ~ ~ #tdh:conteneur/5 run scoreboard players set #conteneur max 5
execute if block ~ ~ ~ #tdh:conteneur/9 run scoreboard players set #conteneur max 9
execute if block ~ ~ ~ #tdh:conteneur/27 run scoreboard players set #conteneur max 27

# On n'exécute la suite des commandes que si le bloc est bien un conteneur
execute if score #conteneur max matches 1.. run function tdh:conteneur/inserer_items/init_boucle
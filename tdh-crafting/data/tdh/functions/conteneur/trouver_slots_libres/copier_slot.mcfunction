# On copie le slot libre actuel dans nos résultats
data modify storage tdh:conteneur resultat append value {Slot:-1b}
execute store result storage tdh:conteneur resultat[-1].Slot byte 1 run scoreboard players get #conteneur temp

# On décrémente notre variable de contrôle
scoreboard players remove #conteneurBoucle nombre 1

# On passe au slot suivant s'il en reste à trouver
execute if score #conteneurBoucle nombre matches 1.. run function tdh:conteneur/trouver_slots_libres/prochain_slot
# On initialise les données dont on aura besoin pour la boucle

# On copie directement #conteneur nombre dans #conteneurBoucle nombre,
# car la sous-fonction qu'on appelle plus loin se sert de #conteneur nombre
scoreboard players operation #conteneurBoucle nombre = #conteneur nombre

# On initialise les résultats
data modify storage tdh:conteneur resultat set value []

# On trouve le premier slot libre
function tdh:conteneur/trouver_slot_libre

# Si la recherche n'a pas échoué, on copie ce slot et on passe aux suivants
execute unless score #conteneur temp matches ..-1 run function tdh:conteneur/trouver_slots_libres/copier_slot
# On cherche le slot libre suivant
scoreboard players operation #conteneur min = #conteneur temp
scoreboard players add #conteneur min 1
function tdh:conteneur/trouver_slot_libre_apres

# Si on a trouvé un nouveau slot libre, on le copie
execute unless score #conteneur temp matches ..-1 run function tdh:conteneur/trouver_slots_libres/copier_slot
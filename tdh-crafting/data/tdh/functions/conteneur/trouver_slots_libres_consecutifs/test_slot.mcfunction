# On récupère l'ID de slot du premier slot occupé après le nôtre
execute store success score #conteneur temp4 store result score #conteneur temp5 run data get storage tdh:conteneur contenu[0].Slot
# Si l'opération a échoué, c'est qu'il n'y a pas d'items dans le conteneur
# On définit à ce moment-là le dernier slot libre au maximum du conteneur
execute if score #conteneur temp4 matches ..0 run scoreboard players operation #conteneur temp5 = #conteneur max
# On définit aussi le slot libre au max si l'ID de slot est *inférieur* à temp (signifie qu'on a probablement loopé tout autour de l'inventaire)
execute if score #conteneur temp4 matches 1.. if score #conteneur temp5 < #conteneur temp run scoreboard players operation #conteneur temp5 = #conteneur max

# On retranche l'ID du premier slot libre à celui-ci pour connaître le nombre de slots libres entre les 2
scoreboard players operation #conteneur temp5 -= #conteneur temp

# S'il y en a au moins autant que ce qu'on recherche, on s'arrête là
execute if score #conteneur temp5 >= #conteneurBoucle nombre run function tdh:conteneur/trouver_slots_libres_consecutifs/succes

# Sinon, on teste le slot suivant
execute unless score #conteneur temp5 >= #conteneurBoucle nombre run function tdh:conteneur/trouver_slots_libres_consecutifs/prochain_slot
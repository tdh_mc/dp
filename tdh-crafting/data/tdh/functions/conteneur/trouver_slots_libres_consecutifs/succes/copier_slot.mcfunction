# On enregistre le slot correspondant à notre variable temp2
data modify storage tdh:conteneur resultat append value {Slot:-1b}
execute store result storage tdh:conteneur resultat[-1].Slot byte 1 run scoreboard players get #conteneur temp2

# On fait évoluer nos variables de contrôle
scoreboard players add #conteneur temp2 1
scoreboard players remove #conteneurBoucle nombre 1

# S'il reste des numéros de slot à enregistrer, on recommence cette fonction
execute if score #conteneurBoucle nombre matches 1.. run function tdh:conteneur/trouver_slots_libres_consecutifs/succes/copier_slot
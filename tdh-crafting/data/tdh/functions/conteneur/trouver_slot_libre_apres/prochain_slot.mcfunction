# On supprime un slot de notre inventaire sauvegardé
data modify storage tdh:conteneur contenu append from storage tdh:conteneur contenu[0]
data remove storage tdh:conteneur contenu[0]

# On fait évoluer notre variable de contrôle
# (seulement si temp est égal à temp2 ; s'il était inférieur, on ne serait pas dans cette fonction, mais s'il est supérieur, cela indique qu'on est toujours avant notre minimum désiré, et on ne souhaite pas incrémenter temp)
execute if score #conteneur temp = #conteneur temp2 run scoreboard players add #conteneur temp 1
scoreboard players remove #conteneur nombre 1

# On recommence la fonction s'il reste des éléments
execute if score #conteneur nombre matches 1.. run function tdh:conteneur/trouver_slot_libre_apres/test_slot
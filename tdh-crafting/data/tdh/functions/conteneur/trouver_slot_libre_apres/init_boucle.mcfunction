# On initialise les données dont on aura besoin pour la boucle

# On commence par le slot minimal précisé en argument
scoreboard players operation #conteneur temp = #conteneur min

# On copie le contenu du conteneur dans un storage temporaire
data modify storage tdh:conteneur contenu set from block ~ ~ ~ Items
# On enregistre le nombre d'items dans le conteneur
execute store result score #conteneur nombre run data get storage tdh:conteneur contenu

# S'il n'y a pas d'item dans le conteneur, on ne fait rien et on garde "min" comme valeur de retour
# S'il y a au moins un item, on teste le premier slot (puis les autres par récursion)
execute if score #conteneur nombre matches 1.. run function tdh:conteneur/trouver_slot_libre_apres/test_slot

# Si à la fin de l'opération, le maximum de contenance du conteneur est dépassé, on renvoie -1
execute if score #conteneur temp >= #conteneur max run scoreboard players set #conteneur temp -1
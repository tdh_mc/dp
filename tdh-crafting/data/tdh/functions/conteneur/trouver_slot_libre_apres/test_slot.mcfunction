# Si notre variable temporaire est inférieure au numéro de slot,
# c'est qu'on en a sauté (au moins) un avant d'arriver à cet item,
# et qu'il y a donc un slot libre à la valeur de temp actuelle
execute store result score #conteneur temp2 run data get storage tdh:conteneur contenu[0].Slot

# Dans ce cas, on termine la boucle
execute if score #conteneur temp < #conteneur temp2 run function tdh:conteneur/trouver_slot_libre_apres/succes

# Dans le cas contraire, on passe au slot suivant
execute unless score #conteneur temp < #conteneur temp2 run function tdh:conteneur/trouver_slot_libre_apres/prochain_slot
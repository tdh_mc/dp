# On exécute cette fonction pour trouver le premier slot libre (au-delà d'un slot minimum configurable) dans le conteneur situé à sa position d'exécution

# Arguments : #conteneur min, le numéro de slot minimum à rechercher

# Valeurs de retour :
# - #conteneur temp, indique un numéro de slot (ou <=-1 s'il n'y a pas de slot libre)

# On vérifie que le bloc est bien un conteneur en définissant le max de slots disponibles
scoreboard players set #conteneur max 0
execute if block ~ ~ ~ #tdh:conteneur/5 run scoreboard players set #conteneur max 5
execute if block ~ ~ ~ #tdh:conteneur/9 run scoreboard players set #conteneur max 9
execute if block ~ ~ ~ #tdh:conteneur/27 run scoreboard players set #conteneur max 27

# On n'exécute la suite des commandes que si le bloc est bien un conteneur
execute unless score #conteneur max matches 1.. run scoreboard players set #conteneur temp -10
execute if score #conteneur max matches 1.. run function tdh:conteneur/trouver_slot_libre_apres/init_boucle
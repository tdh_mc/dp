# On exécute cette fonction en tant qu'une machine à sa position, pour tester le type de combustible qu'on a
# et éventuellement exécuter la fonction "produire" si tous les blocs sont encore à la bonne place et que la machine est active

# Même si cette fonction n'est pas censée être appelée par une machine ne consommant pas de combustible,
# elle renvoie directement la fonction "Produire" si une telle machine l'appelle tout de même

execute as @s[tag=AtelierAmeliore] run function tdh:craft/machine/produire
execute as @s[tag=Cuisiniere] run function tdh:craft/machine/cuisiniere/test_combustible
execute as @s[tag=TableSandwich] run function tdh:craft/machine/produire
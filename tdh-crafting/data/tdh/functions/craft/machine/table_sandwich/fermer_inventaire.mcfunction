# On appelle cette fonction pour "fermer l'inventaire"
# (= remplacer l'atelier par un bloc différent pendant un très court délai pour sortir le joueur de son UI)

# On remplace l'atelier
setblock ~ ~ ~ oak_planks

# Le tag et le schedule sont gérés par la fonction parente (qu'il faut du coup appeler plutôt que celle-ci : tdh:craft/machine/fermer_inventaire)
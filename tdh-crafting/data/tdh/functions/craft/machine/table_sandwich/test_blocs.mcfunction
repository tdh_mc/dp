# On exécute cette fonction à la position d'un atelier amélioré actif pour vérifier s'il est toujours entier

# On vérifie tous les blocs et on lance la bonne fonction suivant le résultat
# (si la machine est incomplète, on la détruit ; sinon, on produit un item)
execute store result score @s temp2 if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false]

# On ne détruit pas la machine si elle a le tag "InventaireFerme" (= on a remplacé temporairement son bloc "Atelier" par un autre pour forcer la fermeture de l'inventaire)
execute unless score @s[tag=!InventaireFerme] temp2 matches 1.. run function tdh:craft/machine/detruire
# On ne produit l'item que si on a le tag "Active" ; sinon on ne fait rien, et on exécute cette fonction simplement pour vérifier que la machine est toujours fonctionnelle
execute if score @s[tag=Active] temp2 matches 1.. run function tdh:craft/machine/produire

# On réinitialise la variable temporaire
scoreboard players reset @s temp2
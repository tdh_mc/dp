# On exécute cette fonction en tant qu'une entité TableSandwich qui vient d'être utilisée par un joueur,
# pour vérifier si une recette de notre dispenser matche avec les recettes disponibles

# Structure des données de l'entité :
# data: {
#	contenu: {slots: [
#		{Slot:0b,id:"air",Count:1b},	(ou n'importe quel autre type de bloc)
#		{Slot:1b,id:"air",Count:1b},	(avec éventuellement le tag "tag")
#		{Slot:2b,id:"air",Count:1b},
#		{Slot:3b,id:"air",Count:1b},
#		{Slot:4b,id:"air",Count:1b},
#		{Slot:5b,id:"air",Count:1b},
#		{Slot:6b,id:"air",Count:1b},
#		{Slot:7b,id:"air",Count:1b},
#		{Slot:8b,id:"air",Count:1b}
#	] },
#	output: {id:"air", Count:1b},	(N'importe quel bloc à output, avec éventuellement le tag)
#									(Absent si aucun output valide n'a été trouvé)
#	contenu_temp: [
#		(Même structure que ci-dessus, mais sans les blocs d'air)
#		(Temporaire ; ne reste pas après filtration)
#	],
#	recettes_temp: [
#		{
#			output: {id:"air", Count:1b},	(N'importe quel bloc/item à output, avec éventuellement le tag)
#			recipe: {slots: [
#				{id:"air"},	(N'importe quelle recette comme ci-dessus, avec éventuellement le tag)
#				{id:"air"},
#				{id:"air"},
#				{id:"air"},
#				{id:"air"},
#				{id:"air"},
#				{id:"air"},
#				{id:"air"},
#				{id:"air"}
#			] }
#		},
#		{ (etc...) }
#	]
# }

# On s'active (pour ne pas pouvoir être activé par un autre joueur jusqu'à la fin de l'opération)
tag @s add Active

# On commence par enregistrer la recette actuelle dans le storage
execute positioned ^1 ^ ^ run function tdh:craft/machine/copier_contenu
# Une fois cette fonction passée, on a le contenu du bloc dans notre NBT (data.contenu) (de type liste), y compris les blocs d'air

# On initialise l'output (vide par défaut)
data remove entity @s data.output

# On copie la liste des recettes (pour pouvoir les supprimer tranquillement en itérant)
data modify entity @s data.recettes_temp set from storage tdh:craft recettes.table_sandwich

# On teste ensuite toutes les recettes enregistrées une par une, jusqu'à en trouver une qui marche
# On stocke le nombre de recettes
execute store result score @s max run data get entity @s data.recettes_temp
# S'il y en a au moins une, on exécute la suite des opérations (de manière récursive)
execute if score @s max matches 1.. run function tdh:craft/machine/test_recette

# Si on n'a pas de résultat, on affiche un message d'erreur
execute unless data entity @s data.output run function tdh:craft/machine/test_recette/echec
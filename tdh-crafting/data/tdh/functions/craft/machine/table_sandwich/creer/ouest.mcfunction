# S'il y a déjà une machine à cet emplacement, on la détruit
execute align xyz positioned ~.5 ~.5 ~.5 as @e[type=marker,tag=MachineTDH,distance=..0.5] at @s run function tdh:craft/machine/detruire

# On crée une entité "Table à sandwich" à la position d'exécution de la fonction
# Il faut que ce soit la position de la table de craft
execute align xyz positioned ~.5 ~.5 ~.5 run summon marker ~ ~ ~ {Tags:["MachineTDH","TableSandwich"],Rotation:[90f,0f]}

# On affiche un message de confirmation à l'éventuel joueur ayant le tag TestMachineTDH
tellraw @a[tag=TestMachineTDH] [{"text":"Vous venez de construire une ","color":"gray"},{"text":"table à sandwich","color":"gold","bold":"true"},{"text":"."}]
# On teste la direction du dispenser pour savoir dans quel sens positionner l'entité
execute if block ~ ~-1 ~ dispenser[facing=south] run tp @s ~ ~ ~ 0 0
execute if block ~ ~-1 ~ dispenser[facing=north] run tp @s ~ ~ ~ 180 0
execute if block ~ ~-1 ~ dispenser[facing=east] run tp @s ~ ~ ~ -90 0
execute if block ~ ~-1 ~ dispenser[facing=west] run tp @s ~ ~ ~ 90 0
execute if block ~ ~-1 ~ dispenser[facing=up] run tp @s ~ ~ ~ 0 -90
execute if block ~ ~-1 ~ dispenser[facing=down] run tp @s ~ ~ ~ 0 90
# S'il y a déjà une machine à cet emplacement, on la détruit
execute align xyz positioned ~.5 ~.5 ~.5 as @e[type=marker,tag=MachineTDH,distance=..0.5] at @s run function tdh:craft/machine/detruire

# On crée une entité "Atelier amélioré" à la position d'exécution de la fonction
# Il faut que ce soit la position de la table de craft, pas du dispenser
execute align xyz positioned ~.5 ~.5 ~.5 run summon marker ~ ~ ~ {Tags:["MachineTDH","AtelierAmeliore","MachineTDHTemp"]}

# On tourne l'entité dans la bonne direction
execute as @e[type=marker,tag=MachineTDHTemp] at @s run function tdh:craft/machine/atelier_ameliore/creer/test_rotation
tag @e[type=marker,tag=MachineTDHTemp] remove MachineTDHTemp

# On affiche un message de confirmation à l'éventuel joueur ayant le tag TestMachineTDH
tellraw @a[tag=TestMachineTDH] [{"text":"Vous venez de construire un ","color":"gray"},{"text":"atelier amélioré","color":"gold","bold":"true"},{"text":"."}]
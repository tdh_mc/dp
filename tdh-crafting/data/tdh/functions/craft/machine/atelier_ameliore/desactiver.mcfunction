tellraw @a[distance=..10] [{"text":"Désactivation de l'atelier amélioré.","color":"gray"}]

# On restaure l'inventaire du conteneur, et on le déverrouille
data modify block ~ ~-1 ~ Items set from entity @s data.contenu.slots
data remove block ~ ~-1 ~ Lock
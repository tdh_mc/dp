# On appelle cette fonction en tant qu'une entité "MachineTDH",
# en ayant défini le NBT "data.recettes_temp" aux recettes qui correspondent à notre machine,
# et le NBT "data.contenu" au contenu de notre dispenser (blocs d'air inclus),
# pour tester la recette actuelle et passer à la suivante si elle ne matche pas

# On définit une variable temporaire pour vérifier que la condition suivante passe
scoreboard players set #tdhCraft temp9 1

# On teste chaque ingrédient en faisant un merge de celui-ci dans une copie de notre contenu
# Si la seconde commande échoue (temp9=0), c'est que notre contenu est identique à la recette
data modify entity @s data.buffer set from entity @s data.contenu
function tdh:craft/machine/test_recette/0

# Si la recette a matché, on exécute la suite des opérations
execute if score #tdhCraft temp9 matches 0 run function tdh:craft/machine/test_recette/succes
# Sinon, on passe à la recette suivante
execute unless score #tdhCraft temp9 matches 0 run function tdh:craft/machine/test_recette/prochaine
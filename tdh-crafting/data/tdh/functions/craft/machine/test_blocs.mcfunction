# On exécute cette fonction en tant qu'une machine à sa position, pour tester le type de machine qu'on est
# et éventuellement exécuter la fonction "produire" si tous les blocs sont encore à la bonne place et que la machine est active

execute as @s[tag=AtelierAmeliore] run function tdh:craft/machine/atelier_ameliore/test_blocs
execute as @s[tag=Cuisiniere] run function tdh:craft/machine/cuisiniere/test_blocs
execute as @s[tag=TableSandwich] run function tdh:craft/machine/table_sandwich/test_blocs
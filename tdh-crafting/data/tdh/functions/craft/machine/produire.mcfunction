# On fait produire un élément à la machine qui appelle cette fonction
# L'item à output est stocké dans les données de la machine (data.output)

# On invoque l'item à output
function tdh:craft/machine/produire/invoquer_item
# Si la recette donne de l'XP, on invoque aussi l'XP orb
execute if data entity @s data.xp run function tdh:craft/machine/produire/invoquer_xp

# On crée des particules
particle ash ^ ^ ^1 0.1 0.1 0.1 1 20
particle poof ^ ^ ^1 0.2 0.2 0.2 0.05 5

# On joue un son
playsound minecraft:block.dispenser.dispense block @a[distance=..20] ~ ~-1 ~ 0.75 1

# On prend une variable temporaire pour savoir si l'un des items s'épuise
scoreboard players set #tdhCraft temp4 0

# On retranche 1 item de chaque slot de notre "inventaire" sauvegardé
# Slot 0
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[0].Count
execute store result entity @s data.contenu.slots[0].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1
# Slot 1
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[1].Count
execute store result entity @s data.contenu.slots[1].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1
# Slot 2
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[2].Count
execute store result entity @s data.contenu.slots[2].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1
# Slot 3
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[3].Count
execute store result entity @s data.contenu.slots[3].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1
# Slot 4
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[4].Count
execute store result entity @s data.contenu.slots[4].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1
# Slot 5
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[5].Count
execute store result entity @s data.contenu.slots[5].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1
# Slot 6
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[6].Count
execute store result entity @s data.contenu.slots[6].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1
# Slot 7
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[7].Count
execute store result entity @s data.contenu.slots[7].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1
# Slot 8
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.slots[8].Count
execute store result entity @s data.contenu.slots[8].Count byte 1 run scoreboard players remove #tdhCraft temp5 1
execute if score #tdhCraft temp5 matches ..0 run scoreboard players set #tdhCraft temp4 1

# Buffer de combustible
execute store result score #tdhCraft temp5 run data get entity @s data.contenu.combustible.buffer
execute if score #tdhCraft temp5 matches 1.. store result entity @s data.contenu.combustible.buffer byte 1 run scoreboard players remove #tdhCraft temp5 1
# On ne désactive pas la recette si on arrive à 0, car plusieurs machines fonctionnent sans combustible

# Si on a épuisé l'un des éléments de la recette, on se désactive
execute if score #tdhCraft temp4 matches 1.. run function tdh:craft/machine/desactiver

# On supprime les variables temporaires
scoreboard players reset #tdhCraft temp4
scoreboard players reset #tdhCraft temp5
# On détruit la machine qui appelle cette fonction
# On drop au sol tous les objets sauvegardés dans le conteneur
execute unless predicate tdh:craft/machine/empty_slot_0 run function tdh:craft/detruire_machine/invoquer_item/slot_0
execute unless predicate tdh:craft/machine/empty_slot_1 run function tdh:craft/detruire_machine/invoquer_item/slot_1
execute unless predicate tdh:craft/machine/empty_slot_2 run function tdh:craft/detruire_machine/invoquer_item/slot_2
execute unless predicate tdh:craft/machine/empty_slot_3 run function tdh:craft/detruire_machine/invoquer_item/slot_3
execute unless predicate tdh:craft/machine/empty_slot_4 run function tdh:craft/detruire_machine/invoquer_item/slot_4
execute unless predicate tdh:craft/machine/empty_slot_5 run function tdh:craft/detruire_machine/invoquer_item/slot_5
execute unless predicate tdh:craft/machine/empty_slot_6 run function tdh:craft/detruire_machine/invoquer_item/slot_6
execute unless predicate tdh:craft/machine/empty_slot_7 run function tdh:craft/detruire_machine/invoquer_item/slot_7
execute unless predicate tdh:craft/machine/empty_slot_8 run function tdh:craft/detruire_machine/invoquer_item/slot_8
execute unless predicate tdh:craft/machine/empty_slot_fuel run function tdh:craft/detruire_machine/invoquer_item/slot_fuel

# On désactive la machine
execute as @s[tag=Active] run function tdh:craft/machine/desactiver

# On affiche un message aux joueurs alentours
tellraw @a[distance=..10] [{"text":"Destruction de la machine.","color":"gray"}]

# On tue l'entité matérialisant la machine
kill @s
# Ce tick s'exécute plusieurs fois par seconde pour produire un objet d'output dans chaque machine active,
# après avoir testé que les conditions sont toujours respectées

execute as @e[type=marker,tag=MachineTDH,tag=Active] at @s run function tdh:craft/machine/tick_active
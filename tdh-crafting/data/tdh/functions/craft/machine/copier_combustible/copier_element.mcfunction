# On copie le premier élément s'il est dans le bon slot

# On stocke d'abord son numéro de slot
execute store result score #tdhCraft id run data get entity @s data.contenu_temp[0].Slot

# Selon ce numéro de slot, on le copie vers le bon élément du storage final
execute if score #tdhCraft id matches 0 run data modify entity @s data.contenu.combustible.slots[0] set from entity @s data.contenu_temp[0]
execute if score #tdhCraft id matches 1 run data modify entity @s data.contenu.combustible.slots[1] set from entity @s data.contenu_temp[0]
execute if score #tdhCraft id matches 2 run data modify entity @s data.contenu.combustible.slots[2] set from entity @s data.contenu_temp[0]

# On retranche 1 à notre variable de contrôle, et on répète la fonction s'il reste des éléments
data remove entity @s data.contenu_temp[0]
scoreboard players remove #tdhCraft max 1
execute if score #tdhCraft max matches 1.. run function tdh:craft/machine/copier_combustible/copier_element
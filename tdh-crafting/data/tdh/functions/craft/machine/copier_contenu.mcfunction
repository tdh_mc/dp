# On enregistre le contenu du bloc actuel dans le storage de l'entité MachineTDH qui exécute la fonction

# On initialise le storage final
data modify entity @s data.contenu.slots set value [{Slot:0b,id:"minecraft:air",Count:64b},{Slot:1b,id:"minecraft:air",Count:64b},{Slot:2b,id:"minecraft:air",Count:64b},{Slot:3b,id:"minecraft:air",Count:64b},{Slot:4b,id:"minecraft:air",Count:64b},{Slot:5b,id:"minecraft:air",Count:64b},{Slot:6b,id:"minecraft:air",Count:64b},{Slot:7b,id:"minecraft:air",Count:64b},{Slot:8b,id:"minecraft:air",Count:64b}]
# On copie dans un autre noeud du storage le contenu non filtré du bloc actuel
data modify entity @s data.contenu_temp set from block ~ ~ ~ Items

# Ensuite, pour chaque item contenu dans le storage, on le place dans le bon slot
# On enregistre le nombre d'items à traiter
execute store result score #tdhCraft max run data get entity @s data.contenu_temp
# Si le dispenser n'est pas vide, on traite le premier élément (puis les autres de manière récursive)
execute if score #tdhCraft max matches 1.. run function tdh:craft/machine/copier_contenu/copier_element

# Une fois le filtrage terminé, on supprime les items du bloc actuel
# On les replacera à la fin de l'exécution du crafting si celui-ci ne les consomme pas tous
data modify block ~ ~ ~ Items set value []
# On verrouille également le conteneur pour ne pas pouvoir l'ouvrir jusqu'à la fin de l'opération
data modify block ~ ~ ~ Lock set value "Outil de debug ne pouvant pas être créé sans commandes"
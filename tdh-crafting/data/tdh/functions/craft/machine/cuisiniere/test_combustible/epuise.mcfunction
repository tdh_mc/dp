# On exécute cette fonction si on a plus de combustible
# en tant que son entité à sa position
tellraw @a[distance=..5] [{"text":"Le combustible est épuisé !","color":"red"}]

# On désactive la machine
function tdh:craft/machine/desactiver
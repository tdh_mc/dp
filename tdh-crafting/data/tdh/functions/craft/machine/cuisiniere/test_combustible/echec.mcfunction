# On exécute cette fonction si on a pas trouvé de recette correspondant au contenu de la cuisinière,
# en tant que son entité à sa position
tellraw @a[distance=..5] [{"text":"Le combustible ","color":"red"},{"entity":"@s","nbt":"data.contenu.combustible.slots[1].id","italic":"true"},{"text":" n'est pas pris en compte par cette machine."}]

# On désactive la machine
function tdh:craft/machine/desactiver
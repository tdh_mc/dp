# On supprime le combustible à tester actuel du stockage NBT temporaire, et on passe au suivant
data remove entity @s data.combustibles_temp[0]

# On retranche 1 à notre variable de contrôle de boucle
scoreboard players remove @s max 1

# Si on n'est pas encore arrivés à la fin, on réessaye avec la prochaine recette
execute if score @s max matches 1.. run function tdh:craft/machine/cuisiniere/test_combustible/test_item
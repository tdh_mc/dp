# On exécute cette fonction si on a trouvé le combustible correspondant au contenu de la cuisinière,
# en tant que son entité à sa position
# tellraw @a[distance=..5] [{"text":"Combustible trouvé (","color":"green"},{"entity":"@s","nbt":"data.combustibles_temp[0].id","bold":"true"},{"text":")."}]

# On définit le nombre d'utilisations restantes du four à la valeur définie pour ce combustible
data modify entity @s data.contenu.combustible.buffer set from entity @s data.combustibles_temp[0].duration

# On retranche 1 au nombre d'items combustibles restant dans le four
execute store result score @s temp7 run data get entity @s data.contenu.combustible.slots[1].Count
# S'il ne reste plus qu'un seul item combustible (qu'on a du coup consommé), on remplace l'item combustible par de l'air (pour ne plus le détecter comme combustible au tick suivant)
execute if score @s temp7 matches 1 run data modify entity @s data.contenu.combustible.slots[1] set value {Slot:1b,id:"minecraft:air",Count:0b}
# Sinon, s'il en reste plus d'un, on retire simplement 1 à son nombre d'items
execute if score @s temp7 matches 2.. store result entity @s data.contenu.combustible.slots[1].Count byte 1 run scoreboard players remove @s temp7 1

# On enregistre la nouvelle valeur des utilisations restantes dans la variable temporaire de *test_combustible*
execute store result score @s temp3 run data get entity @s data.contenu.combustible.buffer
# On appelle cette fonction en tant qu'une entité "Cuisiniere",
# pour vérifier si le combustible que l'on a enregistré peut fonctionner

# On copie la liste des combustibles (pour pouvoir les supprimer tranquillement en itérant)
data modify entity @s data.combustibles_temp set from storage tdh:craft combustibles

# On teste ensuite tous les combustibles enregistrés un par un, jusqu'à en trouver un qui matche
# On stocke le nombre de combustibles possibles
execute store result score @s max run data get entity @s data.combustibles_temp
# S'il y en a au moins un, on exécute la suite des opérations (de manière récursive)
execute if score @s max matches 1.. run function tdh:craft/machine/cuisiniere/test_combustible/test_item
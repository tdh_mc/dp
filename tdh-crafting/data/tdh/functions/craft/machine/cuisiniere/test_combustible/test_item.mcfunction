# On appelle cette fonction en tant qu'une entité "Cuisiniere",
# pour vérifier si le combustible que l'on a enregistré peut fonctionner

# On définit une variable temporaire pour vérifier que la condition suivante passe
scoreboard players set #tdhCraft temp9 1

# On teste chaque ingrédient en faisant une copie de celui-ci dans une copie de notre contenu
# Si la seconde commande échoue (temp9=0), c'est que notre contenu est identique à la recette
data modify entity @s data.buffer set from entity @s data.contenu.combustible.slots[1]
# On stocke dans temp9 le succès ou l'échec de la copie du combustible actuel
execute store result score #tdhCraft temp9 run data modify entity @s data.buffer.id set from entity @s data.combustibles_temp[0].id

# Si cela échoue, c'est que le combustible présent était identique ; on ajoute donc sa durée de combustion au buffer
execute if score #tdhCraft temp9 matches 0 run function tdh:craft/machine/cuisiniere/test_combustible/succes
# Sinon, on passe à la recette suivante
execute unless score #tdhCraft temp9 matches 0 run function tdh:craft/machine/cuisiniere/test_combustible/prochain
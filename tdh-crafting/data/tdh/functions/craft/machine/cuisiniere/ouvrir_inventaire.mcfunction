# On appelle cette fonction pour "fermer l'inventaire"
# (= remplacer l'atelier par un bloc différent pendant un très court délai pour sortir le joueur de son UI)

# On replace l'atelier
setblock ~ ~ ~ crafting_table

# Le tag est retiré par la fonction parente (qu'il faut du coup appeler plutôt que celle-ci : tdh:craft/machine/ouvrir_inventaire)
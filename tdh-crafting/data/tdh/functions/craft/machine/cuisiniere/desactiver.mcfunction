tellraw @a[distance=..10] [{"text":"Désactivation de la cuisinière.","color":"gray"}]

# On restaure l'inventaire du conteneur, et on le déverrouille
data modify block ^1 ^ ^ Items set from entity @s data.contenu.slots
data remove block ^1 ^ ^ Lock
# Faut voir si ça marche ingame ; sinon il faudra détecter quel élément de l'array correspond au slot 1...
data modify block ^-1 ^ ^ Items set from entity @s data.contenu.combustible.slots
data remove block ^-1 ^ ^ Lock
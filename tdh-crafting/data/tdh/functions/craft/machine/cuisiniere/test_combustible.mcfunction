# On exécute cette fonction à la position d'une cuisinière pour vérifier si elle a toujours du combustible

# On vérifie le niveau de combustible actuel (stocké dans data.contenu.combustible.buffer sous la forme d'un byte représentant le nombre de cuissons restantes)
execute store result score @s temp3 run data get entity @s data.contenu.combustible.buffer

# S'il ne nous reste pas de combustible, on vérifie s'il nous reste des items à brûler (dans data.contenu.combustible.slots[1]), et on en ajoute un au buffer le cas échéant
execute unless score @s temp3 matches 1.. run function tdh:craft/machine/cuisiniere/test_combustible/test_items
# La commande ci-dessus redéfinira temp3 à une valeur >1 si elle trouve du combustible et qu'elle le consomme
execute unless score @s temp3 matches 1.. run function tdh:craft/machine/cuisiniere/test_combustible/epuise
execute if score @s temp3 matches 1.. run function tdh:craft/machine/produire

# On réinitialise la variable temporaire
scoreboard players reset @s temp3
# On copie le premier élément du tableau temporaire du storage vers le bon slot

# On stocke d'abord son numéro de slot
execute store result score #tdhCraft id run data get entity @s data.contenu_temp[0].Slot

# Selon le numéro de slot, on le copie vers le bon élément du storage final
execute if score #tdhCraft id matches 0 run function tdh:craft/machine/copier_contenu/0
execute if score #tdhCraft id matches 1 run function tdh:craft/machine/copier_contenu/1
execute if score #tdhCraft id matches 2 run function tdh:craft/machine/copier_contenu/2
execute if score #tdhCraft id matches 3 run function tdh:craft/machine/copier_contenu/3
execute if score #tdhCraft id matches 4 run function tdh:craft/machine/copier_contenu/4
execute if score #tdhCraft id matches 5 run function tdh:craft/machine/copier_contenu/5
execute if score #tdhCraft id matches 6 run function tdh:craft/machine/copier_contenu/6
execute if score #tdhCraft id matches 7 run function tdh:craft/machine/copier_contenu/7
execute if score #tdhCraft id matches 8 run function tdh:craft/machine/copier_contenu/8

# On retranche 1 à notre variable de contrôle, et on répète la fonction s'il reste des éléments
data remove entity @s data.contenu_temp[0]
scoreboard players remove #tdhCraft max 1
execute if score #tdhCraft max matches 1.. run function tdh:craft/machine/copier_contenu/copier_element
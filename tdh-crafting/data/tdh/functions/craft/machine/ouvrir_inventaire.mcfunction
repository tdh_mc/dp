# On "ouvre l'inventaire" de la machine qui appelle cette fonction
# à savoir replacer la crafting_table au bon endroit après l'avoir remplacée par un autre bloc
# On a des sous-fonctions différentes suivant le type de machine (notamment parce que les conteneurs ne sont pas les mêmes/pas placés pareil)

execute as @s[tag=AtelierAmeliore] run function tdh:craft/machine/atelier_ameliore/ouvrir_inventaire
execute as @s[tag=Cuisiniere] run function tdh:craft/machine/cuisiniere/ouvrir_inventaire
execute as @s[tag=TableSandwich] run function tdh:craft/machine/table_sandwich/ouvrir_inventaire

# Dans tous les cas, on se retire le tag InventaireFerme
tag @s remove InventaireFerme
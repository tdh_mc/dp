# On désactive la machine qui appelle cette fonction
# On a des sous-fonctions différentes suivant le type de machine (notamment parce que les conteneurs ne sont pas les mêmes/pas placés pareil)

# On restaure les contenus des conteneurs avec des fonctions spécifiques à chaque machine
execute as @s[tag=AtelierAmeliore] run function tdh:craft/machine/atelier_ameliore/desactiver
execute as @s[tag=Cuisiniere] run function tdh:craft/machine/cuisiniere/desactiver
execute as @s[tag=TableSandwich] run function tdh:craft/machine/table_sandwich/desactiver

# On supprime les données NBT de la machine
data remove entity @s data.buffer
data remove entity @s data.contenu
data remove entity @s data.output
data remove entity @s data.xp
data remove entity @s data.contenu_temp
data remove entity @s data.recettes_temp
data remove entity @s data.combustibles_temp

# On se désactive
tag @s remove Active
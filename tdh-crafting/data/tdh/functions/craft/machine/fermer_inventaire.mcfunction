# On "ferme l'inventaire" de la machine qui appelle cette fonction
# Cela revient à remplacer brièvement le bloc crafting_table par un autre, pour fermer l'UI dudit bloc
# On a des sous-fonctions différentes suivant le type de machine (notamment parce que les conteneurs ne sont pas les mêmes/pas placés pareil)

# On remplace le/les blocs au bon endroit selon notre type de machine
execute as @s[tag=AtelierAmeliore] run function tdh:craft/machine/atelier_ameliore/fermer_inventaire
execute as @s[tag=Cuisiniere] run function tdh:craft/machine/cuisiniere/fermer_inventaire
execute as @s[tag=TableSandwich] run function tdh:craft/machine/table_sandwich/fermer_inventaire

# On se donne un tag pour se souvenir de le re-remplacer
tag @s add InventaireFerme
# On schedule la fonction de remplacement dans quelques ticks
schedule function tdh:craft/machine/rouvrir_inventaires 5t replace
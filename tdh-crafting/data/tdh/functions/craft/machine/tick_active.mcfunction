# On exécute cette fonction régulièrement pour ticker les machines actives

# Dans un premier temps, on vérifie qu'il y a toujours un joueur à proximité ;
# Si ce n'est pas le cas on n'exécute pas la suite des opérations
execute positioned ~ ~-1 ~ store result score @s temp if entity @p[gamemode=!spectator,distance=..2.5]
execute unless score @s temp matches 1.. run function tdh:craft/machine/desactiver
execute if score @s temp matches 1.. run function tdh:craft/machine/test_blocs

# On réinitialise la variable temporaire
scoreboard players reset @s temp
# On exécute cette fonction si on a pas trouvé de recette correspondant au contenu de la cuisinière,
# en tant que son entité à sa position
tellraw @a[tag=TestMachineTDH] [{"text":"Recette trouvée (","color":"green"},{"entity":"@s","nbt":"data.recettes_temp[0].name","bold":"true"},{"text":")."}]

# Le conteneur a déjà été verrouillé au début de la fonction, on le laisse donc en l'état pour l'instant
# On copie l'item à output
data modify entity @s data.output set from entity @s data.recettes_temp[0].output
data modify entity @s data.xp set from entity @s data.recettes_temp[0].xp
# On ne copie pas le reste de la recette car on a juste besoin de tout décrémenter de 1, blocs d'air compris.

# La suite des opérations se fera régulièrement tant qu'aucun des slots ne sera vide
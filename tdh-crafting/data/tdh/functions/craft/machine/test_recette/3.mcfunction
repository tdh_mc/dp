# On stocke dans temp9 le succès ou l'échec du merge de l'élément
execute store result score #tdhCraft temp9 run data modify entity @s data.buffer.slots[3] merge from entity @s data.recettes_temp[0].recipe.slots[3]

# On définit la variable à 1 (=recette incorrecte) si le tag "vanilla" est présent (signifie qu'on ne veut pas d'items custom du DP/RP pour cet ingrédient) et que l'item du buffer a le tag "CustomModelData" (qui indique un objet override par le RP)
# On ne teste que CustomModelData car on veut tout de même accepter l'objet s'il n'y a que du NBT "bénin" dessus (par exemple un custom name défini par une enclume)
execute if score #tdhCraft temp9 matches 0 unless data entity @s data.recettes_temp[0].recipe.slots[3].tag if data entity @s data.buffer.slots[3].tag.CustomModelData run scoreboard players set #tdhCraft temp9 1

# Si l'ingrédient était identique, on peut vérifier l'ingrédient suivant
execute if score #tdhCraft temp9 matches 0 run function tdh:craft/machine/test_recette/4
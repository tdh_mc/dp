# On exécute cette fonction si on a pas trouvé de recette correspondant au contenu de la machine,
# en tant que son entité à sa position
tellraw @a[tag=TestMachineTDH] [{"text":"Recette non reconnue.","color":"red"}]

# On désactive la machine
function tdh:craft/machine/desactiver
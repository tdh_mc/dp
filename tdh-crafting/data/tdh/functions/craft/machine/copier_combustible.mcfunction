# On enregistre le contenu du bloc actuel dans le storage de l'entité MachineTDH qui exécute la fonction
# Il s'agit d'une version de *copier_contenu* spécifique aux fours utilisés pour leur combustible

# On initialise le storage final
data modify entity @s data.contenu.combustible set value {slots:[{Slot:0b,id:"minecraft:air",Count:0b},{Slot:1b,id:"minecraft:air",Count:0b},{Slot:2b,id:"minecraft:air",Count:0b}],buffer:0b}
# On copie dans un noeud du storage le contenu non filtré du bloc actuel
data modify entity @s data.contenu_temp set from block ~ ~ ~ Items

# Ensuite, pour chaque item contenu dans le storage, on l'enregistre s'il est dans le slot 1 (=combustible)
# On enregistre le nombre d'items à traiter
execute store result score #tdhCraft max run data get entity @s data.contenu_temp
# Si le four n'est pas vide, on traite le premier élément (puis les autres de manière récursive)
execute if score #tdhCraft max matches 1.. run function tdh:craft/machine/copier_combustible/copier_element

# Une fois le filtrage terminé, on verrouille le conteneur pour ne pas pouvoir l'ouvrir jusqu'à la fin de l'opération
data modify block ~ ~ ~ Lock set value "Outil de debug ne pouvant pas être créé sans commandes"
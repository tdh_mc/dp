# On essaie de récupérer la machine la plus proche de la ligne de visée des joueurs ayant activé un atelier récemment (l'entité se trouve toujours au niveau de l'atelier, qui est également la variable d'utilisation qu'on tracke)

# - Tables de craft
execute at @a[gamemode=!spectator,scores={ateliersUtilises=1..}] run function tdh:craft/machine/test_joueurs/table_craft


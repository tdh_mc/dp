# Comme on cherche une entité, on n'a pas besoin de tester tous les blocs alentour
# On peut juste prendre des positions discrètes le long de notre ligne de visée et vérifier si une entité s'y trouve

# On tag le joueur (pour pouvoir lui afficher un message ultérieurement)
tag @p[gamemode=!spectator,scores={ateliersUtilises=1..}] add TestMachineTDH

# On enregistre une variable temporaire (pour ne pas activer plusieurs entités à la suite)
scoreboard players set #tdhCraft temp 0

# On vérifie le long de notre ligne de visée, tous les 1 blocs, si on trouve une entité MachineTDH à 1 bloc de distance
execute positioned ~ ~1.5 ~ positioned ^ ^ ^0.5 as @e[type=marker,tag=MachineTDH,distance=..1.1,limit=1] at @s run function tdh:craft/machine/test_joueurs/succes
execute if score #tdhCraft temp matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^1.5 as @e[type=marker,tag=MachineTDH,distance=..1.1,limit=1] at @s run function tdh:craft/machine/test_joueurs/succes
execute if score #tdhCraft temp matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^2.5 as @e[type=marker,tag=MachineTDH,distance=..1.1,limit=1] at @s run function tdh:craft/machine/test_joueurs/succes
execute if score #tdhCraft temp matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^3.5 as @e[type=marker,tag=MachineTDH,distance=..1.1,limit=1] at @s run function tdh:craft/machine/test_joueurs/succes
execute if score #tdhCraft temp matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^4.5 as @e[type=marker,tag=MachineTDH,distance=..1.1,limit=1] at @s run function tdh:craft/machine/test_joueurs/succes
execute if score #tdhCraft temp matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^5.5 as @e[type=marker,tag=MachineTDH,distance=..1.1,limit=1] at @s run function tdh:craft/machine/test_joueurs/succes

# On réinitialise le scores des joueurs
scoreboard players set @a[tag=TestMachineTDH] ateliersUtilises 0
# On retire le tag temporaire du joueur
tag @a[tag=TestMachineTDH] remove TestMachineTDH
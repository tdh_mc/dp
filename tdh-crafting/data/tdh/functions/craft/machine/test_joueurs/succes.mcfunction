# On exécute cette fonction en tant qu'une entité MachineTDH qui vient d'être activée par un joueur, à sa position

# On enregistre le fait qu'on a trouvé une machine, pour ne pas risquer d'en activer plusieurs à la suite
scoreboard players set #tdhCraft temp 1

# Si on était déjà actif, on affiche un message d'erreur
execute as @s[tag=Active] run tellraw @a[tag=TestMachineTDH] [{"text":"Cette machine est déjà en cours d'utilisation.","color":"red"}]

# On vérifie le type de machine qu'on est, et on exécute la fonction appropriée à notre type
execute as @s[tag=AtelierAmeliore,tag=!Active] run function tdh:craft/machine/atelier_ameliore/utiliser
execute as @s[tag=Cuisiniere,tag=!Active] run function tdh:craft/machine/cuisiniere/utiliser
execute as @s[tag=TableSandwich,tag=!Active] run function tdh:craft/machine/table_sandwich/utiliser

# Dans tous les cas, on ferme immédiatement la fenêtre d'UI affichée au joueur
function tdh:craft/machine/fermer_inventaire
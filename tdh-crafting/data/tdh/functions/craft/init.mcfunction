scoreboard objectives add ateliersPoses minecraft.used:minecraft.crafting_table "Ateliers posés"
scoreboard objectives add ateliersDetruits minecraft.mined:minecraft.crafting_table "Ateliers détruits"
scoreboard objectives add ateliersUtilises minecraft.custom:minecraft.interact_with_crafting_table "Ateliers utilisés"

scoreboard objectives add distributeursPoses minecraft.used:minecraft.dispenser "Distributeurs posés"
scoreboard objectives add distributeursDetruits minecraft.mined:minecraft.dispenser "Distributeurs détruits"

scoreboard objectives add foursPoses minecraft.used:minecraft.furnace "Fours posés"
scoreboard objectives add foursDetruits minecraft.mined:minecraft.furnace "Fours détruits"

scoreboard objectives add trappesFerPosees minecraft.used:minecraft.iron_trapdoor "Trappes en fer posées"
scoreboard objectives add trappesFerDetruites minecraft.mined:minecraft.iron_trapdoor "Trappes en fer détruites"

scoreboard objectives add itemFramesPosees minecraft.used:minecraft.item_frame "Item frames posées"


scoreboard objectives add idMachine dummy "Identifiant de machine"

function tdh:craft/init/combustibles
function tdh:craft/init/recettes/atelier_ameliore
function tdh:craft/init/recettes/cuisiniere
function tdh:craft/init/recettes/table_sandwich
function tdh:craft/init/electricite
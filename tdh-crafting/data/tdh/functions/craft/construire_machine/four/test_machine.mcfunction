# On se situe à la position d'un four,
# et on souhaite tester si le pattern de construction d'une machine est respecté

# Tests pour la cuisinière
# (face à elle, FOUR - TABLE DE CRAFT - DISPENSER, donc l'inverse dans le "sens" de la machine)
execute rotated 0 0 positioned ^1 ^ ^ if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/cuisiniere/creer/sud
execute rotated 90 0 positioned ^1 ^ ^ if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/cuisiniere/creer/ouest
execute rotated 180 0 positioned ^1 ^ ^ if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/cuisiniere/creer/nord
execute rotated 270 0 positioned ^1 ^ ^ if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/cuisiniere/creer/est
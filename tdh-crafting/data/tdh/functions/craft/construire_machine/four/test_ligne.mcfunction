# On teste une ligne (11 blocs dont seule la position en X varie)
# pour vérifier si elle contient des fours

# On traite tous les blocs
execute positioned ~5 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~4 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~3 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~2 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~1 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~ ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~-1 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~-2 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~-3 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~-4 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
execute positioned ~-5 ~ ~ run function tdh:craft/construire_machine/four/test_bloc
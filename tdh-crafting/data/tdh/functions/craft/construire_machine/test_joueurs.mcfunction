# On vérifie les patterns de machines autour des joueurs ayant posé certains types de blocs :

# - Tables de craft
execute at @a[gamemode=!spectator,scores={ateliersPoses=1..}] run function tdh:craft/construire_machine/table_craft
# - Distributeurs
execute at @a[gamemode=!spectator,scores={distributeursPoses=1..}] run function tdh:craft/construire_machine/distributeur
# - Fours
execute at @a[gamemode=!spectator,scores={foursPoses=1..}] run function tdh:craft/construire_machine/four

# - Iron trapdoors
execute at @a[gamemode=!spectator,scores={trappesFerPosees=1..}] run function tdh:craft/construire_machine/trappe_fer



# On vérifie la présence de certaines block entities autour des joueurs (détection avec des advancements)
execute at @a[gamemode=!spectator,advancements={tdh:craft/electricite/generateur/pose=true}] run function tdh:craft/construire_machine/electricite/generateur
execute at @a[gamemode=!spectator,advancements={tdh:craft/electricite/desenchanteur/pose=true}] run function tdh:craft/construire_machine/electricite/desenchanteur
execute at @a[gamemode=!spectator,advancements={tdh:craft/electricite/enchanteur/pose=true}] run function tdh:craft/construire_machine/electricite/enchanteur
execute at @a[gamemode=!spectator,advancements={tdh:craft/electricite/trieuse/pose=true}] run function tdh:craft/construire_machine/electricite/trieuse
# On teste un "plan" (11x11 blocs à l'horizontale)
# pour vérifier s'il contient des tables de craft

# On traite tous les blocs ligne par ligne
execute positioned ~ ~ ~5 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~4 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~3 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~2 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~1 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~ run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~-1 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~-2 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~-3 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~-4 run function tdh:craft/construire_machine/distributeur/test_ligne
execute positioned ~ ~ ~-5 run function tdh:craft/construire_machine/distributeur/test_ligne
# On se situe à la position d'une table de craft,
# et on souhaite tester si le pattern de construction d'une machine est respecté

# Test pour l'atelier amélioré
# (une table de craft posée sur un dispenser)
execute positioned ~ ~1 ~ if block ~ ~ ~ crafting_table unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/atelier_ameliore/creer

# Tests pour la cuisinière
# (face à elle, FOUR - TABLE DE CRAFT - DISPENSER, donc l'inverse dans le "sens" de la machine)
execute rotated 0 0 positioned ^-1 ^ ^ if block ~ ~ ~ crafting_table if block ^-1 ^ ^ furnace unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/cuisiniere/creer/sud
execute rotated 90 0 positioned ^-1 ^ ^ if block ~ ~ ~ crafting_table if block ^-1 ^ ^ furnace unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/cuisiniere/creer/ouest
execute rotated 180 0 positioned ^-1 ^ ^ if block ~ ~ ~ crafting_table if block ^-1 ^ ^ furnace unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/cuisiniere/creer/nord
execute rotated 270 0 positioned ^-1 ^ ^ if block ~ ~ ~ crafting_table if block ^-1 ^ ^ furnace unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/cuisiniere/creer/est

# Tests pour la table à sandwich
# Blocs (vus depuis face à elle) :
# IRON_TRAPDOOR  - (AIR)
# CRAFTING_TABLE - DISPENSER
execute rotated 0 0 positioned ^-1 ^ ^ if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false] if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/table_sandwich/creer/sud
execute rotated 90 0 positioned ^-1 ^ ^ if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false] if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/table_sandwich/creer/ouest
execute rotated 180 0 positioned ^-1 ^ ^ if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false] if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/table_sandwich/creer/nord
execute rotated 270 0 positioned ^-1 ^ ^ if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false] if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/table_sandwich/creer/est
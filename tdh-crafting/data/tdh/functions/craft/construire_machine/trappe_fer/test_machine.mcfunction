# On se situe à la position d'un four,
# et on souhaite tester si le pattern de construction d'une machine est respecté

# Tests pour la table à sandwich
# Blocs (vus depuis face à elle) :
# IRON_TRAPDOOR  - (AIR)
# CRAFTING_TABLE - DISPENSER
execute rotated 0 0 positioned ~ ~-1 ~ if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false] if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/table_sandwich/creer/sud
execute rotated 90 0 positioned ~ ~-1 ~ if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false] if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/table_sandwich/creer/ouest
execute rotated 180 0 positioned ~ ~-1 ~ if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false] if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/table_sandwich/creer/nord
execute rotated 270 0 positioned ~ ~-1 ~ if block ~ ~1 ~ iron_trapdoor[half=bottom,open=false,waterlogged=false] if block ~ ~ ~ crafting_table if block ^1 ^ ^ dispenser unless entity @e[type=marker,tag=MachineTDH,distance=..0.5] run function tdh:craft/machine/table_sandwich/creer/est
# On teste le bloc d'exécution de la fonction pour vérifier s'il contient une trappe en fer
# Si c'est le cas, on exécute une autre fonction qui vérifiera les patterns des différentes machines créables
execute if block ~ ~ ~ iron_trapdoor run function tdh:craft/construire_machine/trappe_fer/test_machine
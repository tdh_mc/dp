# On teste une ligne (11 blocs dont seule la position en X varie)
# pour vérifier si elle contient des trappes en fer

# On traite tous les blocs
execute positioned ~5 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~4 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~3 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~2 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~1 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~ ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~-1 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~-2 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~-3 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~-4 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
execute positioned ~-5 ~ ~ run function tdh:craft/construire_machine/trappe_fer/test_bloc
# On se situe à la position d'une blast furnace,
# et on souhaite tester s'il s'agit bien d'un générateur électrique

# On copie dans un buffer temporaire les données du générateur que l'on doit tester pour vérifier que c'en est un
data modify storage tdh:craft filtres.buffer set from block ~ ~ ~
execute store result score #tdhCraft temp7 run data modify storage tdh:craft filtres.buffer merge from storage tdh:craft filtres.electricite.generateur
data remove storage tdh:craft filtres.buffer

# On n'exécute la suite des opérations que si on est bel et bien un générateur électrique TDH,
# ET qu'il n'y en a pas déjà un à la même position (pour éviter d'override du combustible existant)
execute if score #tdhCraft temp7 matches 0 align xyz positioned ~.5 ~.5 ~.5 unless entity @e[type=marker,tag=GenerateurThermique,distance=..0.5] run function tdh:craft/electricite/generateur/creer
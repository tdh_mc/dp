# On se situe à la position d'un dropper,
# et on souhaite tester s'il s'agit bien d'une trieuse

# On copie dans un buffer temporaire les données du dropper que l'on doit tester pour vérifier si c'est une trieuse
data modify storage tdh:craft filtres.buffer set from block ~ ~ ~
execute store result score #tdhCraft temp7 run data modify storage tdh:craft filtres.buffer merge from storage tdh:craft filtres.electricite.trieuse
data remove storage tdh:craft filtres.buffer

# On n'exécute la suite des opérations que si on est bel et bien une trieuse TDH,
# ET qu'il n'y en a pas déjà une à la même position
execute if score #tdhCraft temp7 matches 0 align xyz positioned ~.5 ~.5 ~.5 unless entity @e[type=marker,tag=Trieuse,distance=..0.5] run function tdh:craft/electricite/trieuse/creer
# Cette fonction est appelée à la position d'un joueur qui vient de poser un enchanteur électrique,
# pour trouver le enchanteur électrique aux alentours

# On tag le joueur ayant posé la table de craft pour pouvoir lui afficher les messages de succès de création d'une machine
tag @a[gamemode=!spectator,distance=0] add TestMachineTDH

# On vérifie les schémas de toutes les tables de craft situées dans un rayon de 5 autour du joueur
# On utilise pour cela des sous-fonctions par "plan" horizontal, puis par ligne
execute positioned ~ ~5 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~4 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~3 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~2 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~1 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~ ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~-1 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~-2 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~-3 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~-4 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan
execute positioned ~ ~-5 ~ run function tdh:craft/construire_machine/electricite/enchanteur/test_plan

# On réinitialise les advancements
advancement revoke @a[tag=TestMachineTDH] only tdh:craft/electricite/enchanteur/pose

# On retire le tag temporaire du joueur
tag @a[tag=TestMachineTDH] remove TestMachineTDH
# Initialisation des filtres NBT pour les machines électriques
data modify storage tdh:craft filtres.electricite.generateur set value {CustomName:'{"text":"Générateur électrique thermique"}',Lock:"Outil de debug ne pouvant pas être créé sans commandes"}
data modify storage tdh:craft filtres.electricite.desenchanteur set value {CustomName:'{"text":"Désenchanteur électrique"}',Lock:"Outil de debug ne pouvant pas être créé sans commandes"}
data modify storage tdh:craft filtres.electricite.enchanteur set value {CustomName:'{"text":"Enchanteur électrique"}',Lock:"Outil de debug ne pouvant pas être créé sans commandes"}
data modify storage tdh:craft filtres.electricite.trieuse set value {CustomName:'{"text":"Trieuse électrique"}',Lock:"Outil de debug ne pouvant pas être créé sans commandes"}

# Initialisation de variables de confirmation pour la manipulation des machines
scoreboard objectives add resetTrieuse trigger
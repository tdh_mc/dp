# Initialisation des combustibles (pour les recettes custom avec cuisson)
data modify storage tdh:craft combustibles set value []
data modify storage tdh:craft combustibles append value {id:"minecraft:coal_block",duration:80b}
data modify storage tdh:craft combustibles append value {id:"minecraft:coal",duration:8b}
data modify storage tdh:craft combustibles append value {id:"minecraft:charcoal",duration:8b}
data modify storage tdh:craft combustibles append value {id:"minecraft:lava_bucket",duration:100b}
data modify storage tdh:craft combustibles append value {id:"minecraft:dried_kelp_block",duration:20b}
data modify storage tdh:craft combustibles append value {id:"minecraft:blaze_rod",duration:12b}
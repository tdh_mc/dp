# Initialisation des recettes custom
data modify storage tdh:craft recettes.atelier_ameliore set value []

# Recettes de l'atelier amélioré
# Bundle (bourse) =
# FIL  | ---- | FIL
# CUIR | ---- | CUIR
# ---- | CUIR | ----
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Bourse",output:{id:"minecraft:bundle",Count:1b},recipe:{slots:[{id:"minecraft:string"},{id:"minecraft:air"},{id:"minecraft:string"},{id:"minecraft:leather"},{id:"minecraft:air"},{id:"minecraft:leather"},{id:"minecraft:air"},{id:"minecraft:leather"},{id:"minecraft:air"}]}}


# Composants électriques
# Électroaimant =
# CUIVRE| CUIVRE |CUIVRE
# CUIVRE|BLOC_FER|CUIVRE
# CUIVRE|REDSTONE|CUIVRE
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Électroaimant",output:{id:"minecraft:raw_copper",Count:1b,tag:{display:{Name:'"Électroaimant"'},CustomModelData:450,ingredient:10450}},recipe:{slots:[{id:"minecraft:copper_ingot"},{id:"minecraft:copper_ingot"},{id:"minecraft:copper_ingot"},{id:"minecraft:copper_ingot"},{id:"minecraft:iron_block"},{id:"minecraft:copper_ingot"},{id:"minecraft:copper_ingot"},{id:"minecraft:redstone"},{id:"minecraft:copper_ingot"}]}}

# Turbine =
# FER | FER | FER
# FER |BLOC_FER| FER
# FER | FER | FER
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Turbine",output:{id:"minecraft:raw_iron",Count:1b,tag:{display:{Name:'"Turbine"'},CustomModelData:480,ingredient:10480}},recipe:{slots:[{id:"minecraft:iron_ingot"},{id:"minecraft:iron_ingot"},{id:"minecraft:iron_ingot"},{id:"minecraft:iron_ingot"},{id:"minecraft:iron_block"},{id:"minecraft:iron_ingot"},{id:"minecraft:iron_ingot"},{id:"minecraft:iron_ingot"},{id:"minecraft:iron_ingot"}]}}

# Générateur électrique =
# TURBINE|IRON_BARS|ELECTROAIMANT
# SB_WALL| ------- | BLOC_CUIVRE
# FURNACE| ------- |BLOC_REDSTONE
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Générateur électrique",output:{id:"minecraft:blast_furnace",Count:1b,tag:{display:{Name:'{"text":"Générateur électrique thermique"}'},BlockEntityTag:{CustomName:'{"text":"Générateur électrique thermique"}',Lock:"Outil de debug ne pouvant pas être créé sans commandes"}}},recipe:{slots:[{id:"minecraft:raw_iron",tag:{CustomModelData:480,ingredient:10480}},{id:"minecraft:iron_bars"},{id:"minecraft:raw_copper",tag:{CustomModelData:450,ingredient:10450}},{id:"minecraft:stone_brick_wall"},{id:"minecraft:air"},{id:"minecraft:copper_block"},{id:"minecraft:furnace"},{id:"minecraft:air"},{id:"minecraft:redstone_block"}]}}

# Désenchanteur électrique =
# BLAZE_ROD|ELECTROAIMANT|BLAZE_ROD
#   CUIVRE |TABLE_ENCHANT| CUIVRE
#   CUIVRE |  OBSIDIENNE | CUIVRE
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Désenchanteur électrique",output:{id:"minecraft:brewing_stand",Count:1b,tag:{display:{Name:'{"text":"Désenchanteur électrique"}'},BlockEntityTag:{CustomName:'{"text":"Désenchanteur électrique"}',Lock:"Outil de debug ne pouvant pas être créé sans commandes"}}},recipe:{slots:[{id:"minecraft:blaze_rod"},{id:"minecraft:raw_copper",tag:{CustomModelData:450,ingredient:10450}},{id:"minecraft:blaze_rod"},{id:"minecraft:copper_ingot"},{id:"minecraft:enchanting_table"},{id:"minecraft:copper_ingot"},{id:"minecraft:copper_ingot"},{id:"minecraft:obsidian"},{id:"minecraft:copper_ingot"}]}}

# Enchanteur électrique =
# ELECTROAIMANT|BLAZE_ROD|ELECTROAIMANT
#   CUIVRE |TABLE_ENCHANT| CUIVRE
#   CUIVRE |  OBSIDIENNE | CUIVRE
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Enchanteur électrique",output:{id:"minecraft:brewing_stand",Count:1b,tag:{display:{Name:'{"text":"Enchanteur électrique"}'},BlockEntityTag:{CustomName:'{"text":"Enchanteur électrique"}',Lock:"Outil de debug ne pouvant pas être créé sans commandes"}}},recipe:{slots:[{id:"minecraft:raw_copper",tag:{CustomModelData:450,ingredient:10450}},{id:"minecraft:blaze_rod"},{id:"minecraft:raw_copper",tag:{CustomModelData:450,ingredient:10450}},{id:"minecraft:copper_ingot"},{id:"minecraft:enchanting_table"},{id:"minecraft:copper_ingot"},{id:"minecraft:copper_ingot"},{id:"minecraft:obsidian"},{id:"minecraft:copper_ingot"}]}}

# Trieuse électrique =
# CUIVRE |ELECTROAIMANT| CUIVRE
# HOPPER |   DROPPER   | HOPPER
# CUIVRE |    HOPPER   | CUIVRE
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Trieuse électrique",output:{id:"minecraft:dropper",Count:1b,tag:{display:{Name:'{"text":"Trieuse électrique"}'},BlockEntityTag:{CustomName:'{"text":"Trieuse électrique"}',Lock:"Outil de debug ne pouvant pas être créé sans commandes"}}},recipe:{slots:[{id:"minecraft:copper_ingot"},{id:"minecraft:raw_copper",tag:{CustomModelData:450,ingredient:10450}},{id:"minecraft:copper_ingot"},{id:"minecraft:hopper"},{id:"minecraft:dropper"},{id:"minecraft:hopper"},{id:"minecraft:copper_ingot"},{id:"minecraft:hopper"},{id:"minecraft:copper_ingot"}]}}
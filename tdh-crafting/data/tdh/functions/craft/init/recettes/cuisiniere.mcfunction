# Initialisation des recettes custom
data modify storage tdh:craft recettes.cuisiniere set value []

# Recettes de la cuisinière
# Tarte aux fruits x2 =
# FRUIT| FRUIT| FRUIT
# SUCRE| OEUF | SUCRE
# BLE  | BLE  | BLE 
# Tarte aux baies
data modify storage tdh:craft recettes.cuisiniere append value {name:"Tarte aux baies",xp:2s,output:{id:"minecraft:pumpkin_pie",Count:2b,tag:{CustomModelData:50,display:{Name:'"Tarte aux baies"'}}},recipe:{slots:[{id:"minecraft:sweet_berries"},{id:"minecraft:sweet_berries"},{id:"minecraft:sweet_berries"},{id:"minecraft:sugar"},{id:"minecraft:egg"},{id:"minecraft:sugar"},{id:"minecraft:wheat"},{id:"minecraft:wheat"},{id:"minecraft:wheat"}]}}
# Tarte aux baies lumineuses
data modify storage tdh:craft recettes.cuisiniere append value {name:"Tarte aux baies lumineuses",xp:2s,output:{id:"minecraft:pumpkin_pie",Count:2b,tag:{CustomModelData:100,display:{Name:'"Tarte aux baies lumineuses"'}}},recipe:{slots:[{id:"minecraft:glow_berries"},{id:"minecraft:glow_berries"},{id:"minecraft:glow_berries"},{id:"minecraft:sugar"},{id:"minecraft:egg"},{id:"minecraft:sugar"},{id:"minecraft:wheat"},{id:"minecraft:wheat"},{id:"minecraft:wheat"}]}}
# Tarte aux pommes
data modify storage tdh:craft recettes.cuisiniere append value {name:"Tarte aux pommes",xp:2s,output:{id:"minecraft:pumpkin_pie",Count:2b,tag:{CustomModelData:200,display:{Name:'"Tarte aux pommes"'}}},recipe:{slots:[{id:"minecraft:apple"},{id:"minecraft:apple"},{id:"minecraft:apple"},{id:"minecraft:sugar"},{id:"minecraft:egg"},{id:"minecraft:sugar"},{id:"minecraft:wheat"},{id:"minecraft:wheat"},{id:"minecraft:wheat"}]}}
# Tarte au sucre x1 =
# SUCRE| SUCRE| SUCRE
# SUCRE| OEUF | SUCRE
# BLE  | BLE  | BLE 
data modify storage tdh:craft recettes.cuisiniere append value {name:"Tarte au sucre",xp:1s,output:{id:"minecraft:pumpkin_pie",Count:1b,tag:{CustomModelData:1000,display:{Name:'"Tarte au sucre"'}}},recipe:{slots:[{id:"minecraft:sugar"},{id:"minecraft:sugar"},{id:"minecraft:sugar"},{id:"minecraft:sugar"},{id:"minecraft:egg"},{id:"minecraft:sugar"},{id:"minecraft:wheat"},{id:"minecraft:wheat"},{id:"minecraft:wheat"}]}}

# Omelette x1 =
# OEUF|OEUF|OEUF
# OEUF|OEUF|OEUF
# OEUF|OEUF|OEUF
data modify storage tdh:craft recettes.cuisiniere append value {name:"Omelette",xp:1s,output:{id:"minecraft:cooked_porkchop",Count:1b,tag:{CustomModelData:10000,display:{Name:'"Omelette"'}}},recipe:{slots:[{id:"minecraft:egg"},{id:"minecraft:egg"},{id:"minecraft:egg"},{id:"minecraft:egg"},{id:"minecraft:egg"},{id:"minecraft:egg"},{id:"minecraft:egg"},{id:"minecraft:egg"},{id:"minecraft:egg"}]}}
# On exécute cette fonction en tant qu'une machine électrique à sa position,
# pour vérifier si la block entity correspondante est toujours à notre position

execute as @s[tag=GenerateurThermique] at @s run function tdh:craft/electricite/generateur/test_bloc
execute as @s[tag=Desenchanteur] at @s run function tdh:craft/electricite/desenchanteur/test_bloc
execute as @s[tag=Enchanteur] at @s run function tdh:craft/electricite/enchanteur/test_bloc
execute as @s[tag=Trieuse] at @s run function tdh:craft/electricite/trieuse/test_bloc
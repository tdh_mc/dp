# On remplace notre tag "TempRepeteurValide" par un tag "TempRepeteurUtilise" pour ne pas tester en boucle les mêmes répéteurs
tag @s remove TempRepeteurValide
tag @s add TempRepeteurUtilise

# On tag les générateurs à moins de 7 blocs de nous (sur les axes X/Z) mais seulement 3 blocs en hauteur
# On ne prend pas en compte les générateurs ayant un status <= 0 (signifie une absence de combustible)
execute positioned ~-7.5 ~-3.5 ~-7.5 run tag @e[type=marker,tag=Generateur,dx=15,dy=7,dz=15,scores={status=1..}] add TempGenerateurValide

# S'il n'y a pas de générateur valide, on cherche les répéteurs de circuit selon les mêmes critères
execute unless entity @e[type=marker,tag=TempGenerateurValide] run function tdh:craft/electricite/trouver_generateur/test_repeteurs
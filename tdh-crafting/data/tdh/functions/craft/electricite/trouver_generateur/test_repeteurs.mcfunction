# On tag tous les répéteurs valides
execute positioned ~-7.5 ~-3.5 ~-7.5 run tag @e[type=marker,tag=Repeteur,tag=!TempRepeteurUtilise,dx=15,dy=7,dz=15] add TempRepeteurValide

# On exécute un test de générateur à la position de chaque répéteur valide
execute as @e[type=marker,tag=TempRepeteurValide] at @s run function tdh:craft/electricite/trouver_generateur/test_repeteur_valide
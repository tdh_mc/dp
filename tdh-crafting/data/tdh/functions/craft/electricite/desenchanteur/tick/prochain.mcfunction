# On veut passer à l'opération suivante, mais on doit commencer par placer dans le conteneur en-dessous de nous les items résultats de l'opération précédente

# On vérifie s'il y a deux places disponibles dans le coffre en-dessous de nous
scoreboard players set #conteneur nombre 2
execute positioned ~ ~-1 ~ run function tdh:conteneur/trouver_slots_libres

# S'il n'y a pas de place, on met un message d'erreur comme quoi le coffre est plein
# mais on ne reset pas notre progression (on reste en attente jusqu'à ce qu'il y ait de la place)
execute if score #conteneur temp matches ..-10 run function tdh:craft/electricite/desenchanteur/tick/echec/pas_de_coffre
execute if score #conteneur temp matches -9..-1 run function tdh:craft/electricite/desenchanteur/tick/echec/pas_de_place
execute if score #conteneur temp matches 0.. run function tdh:craft/electricite/desenchanteur/tick/prochain/succes
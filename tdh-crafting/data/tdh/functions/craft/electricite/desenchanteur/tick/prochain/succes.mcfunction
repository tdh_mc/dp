# On a réussi à trouver 2 slots pour placer notre item et notre livre ;
# ces 2 slots sont enregistrés dans notre storage tdh:conteneur resultat

# On calcule notre output
# On copie dans l'output notre outil enchanté avec le bon numéro de slot
data modify entity @s data.output.item set from entity @s data.contenu.queue[0].item
data modify entity @s data.output.item.Slot set from storage tdh:conteneur resultat[0].Slot
# On copie dans l'output notre livre non enchanté avec le bon numéro de slot,
# en le convertissant en livre enchanté (mais en conservant le reste de son NBT)
data modify entity @s data.output.book set from entity @s data.contenu.queue[0].book
data modify entity @s data.output.book.id set value "minecraft:enchanted_book"
data modify entity @s data.output.book.Slot set from storage tdh:conteneur resultat[1].Slot
# On remplace les enchantements de l'outil par des enchantements stockés sur le livre
data modify entity @s data.output.book.tag.StoredEnchantments set from entity @s data.output.item.tag.Enchantments
data remove entity @s data.output.item.tag.Enchantments

# On ajoute au conteneur ces 2 items
data modify block ~ ~-1 ~ Items append from entity @s data.output.item
data modify block ~ ~-1 ~ Items append from entity @s data.output.book

# On les supprime de nos données
data remove entity @s data.output
data remove entity @s data.contenu.queue[0]

# On passe à l'item suivant
function tdh:craft/electricite/desenchanteur/tick/prochain/succes/test_queue
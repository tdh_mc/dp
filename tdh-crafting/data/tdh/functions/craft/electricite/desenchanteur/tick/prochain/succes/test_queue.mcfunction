# On exécute cette fonction après le succès d'une opération de désenchantement,
# pour vérifier s'il reste d'autres opérations en attente

# On enregistre la taille actuelle de la queue
execute store result score #tdhCraft temp7 run data get entity @s data.contenu.queue

# S'il n'y a pas d'éléments en attente, on se désactive
execute if score #tdhCraft temp7 matches ..0 run function tdh:craft/electricite/desenchanteur/desactiver
# S'il y en a, on modifie l'élément affiché au-dessus de nous
execute if score #tdhCraft temp7 matches 1.. run function tdh:craft/electricite/desenchanteur/reinitialiser_statut
# S'il y a déjà une machine à cet emplacement, on la détruit
execute as @e[type=marker,tag=MachineElectriqueTDH,distance=..0.5] run function tdh:craft/electricite/detruire

# On crée une entité "Désenchanteur électrique" à la position d'exécution de la fonction,
# qui doit également être celle du brewing stand
summon marker ~ ~ ~ {Tags:["MachineElectriqueTDH","Desenchanteur","MachineTDHTemp"]}

# On initialise correctement l'entité
execute as @e[type=marker,tag=MachineTDHTemp] at @s run function tdh:craft/electricite/desenchanteur/creer/initialiser

# On affiche un message de confirmation à l'éventuel joueur ayant le tag TestMachineTDH
tellraw @a[tag=TestMachineTDH] [{"text":"Ce ","color":"gray"},{"text":"désenchanteur","color":"gold","bold":"true"},{"text":" a été initialisé avec succès."}]
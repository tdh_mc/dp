# On exécute cette fonction en tant qu'une entité Desenchanteur qui vient d'être utilisée par un joueur,
# pour vérifier si l'objet tenu en main par le joueur est compatible avec le désenchanteur

# Structure des données de l'entité :
# data: {
#	contenu: {
#		queue: [	(une liste où le premier élément entré, l'élément 0, sera traité avant les suivants)
#			{
#				enchantability:15b,
#				item: {id:"minecraft:iron_sword",Count:1b,tag:{ (etc) }},
#				book: {id:"minecraft:book",Count:1b,tag:{ (etc) }},
#			},
#			{
#				enchantability:10b,
#				item: {id:"minecraft:diamond_pickaxe",Count:1b,tag:{ (etc) }},
#				book: {id:"minecraft:book",Count:1b,tag:{ (etc) }},
#			},
#			(...)
#		],
#		coffre_temp: [
#			(inventaire du coffre en-dessous, seulement présent pendant l'opération de recherche)
#		]
#	},
#	output: {
#		book: {id:"minecraft:enchanted_book",Count:1b,tag:{ (...) }},
#		item: {id:"minecraft:iron_sword",Count:1b,tag:{ (...) }}
#	},							(Absent si aucun output valide n'a été trouvé)
#	output_temp: {id:"minecraft:book} (présent seulement pendant l'opération de recherche)
# }

# On enregistre le fait qu'on a trouvé une machine (pour ne pas en activer X à la suite)
scoreboard players set #tdhCraft status 1

# Si on a déclenché l'interaction avec une main vide, on tente de retirer un item de la file
scoreboard players set @s temp5 0
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/desenchanteur/interaction={air=true}}] run scoreboard players set @s temp5 -100
execute if score @s temp5 matches -100 run function tdh:craft/electricite/desenchanteur/utiliser/test_objet/0

# Sinon, on procède avec les tests normaux
execute unless score @s temp5 matches -100 run function tdh:craft/electricite/desenchanteur/utiliser/test_conteneur


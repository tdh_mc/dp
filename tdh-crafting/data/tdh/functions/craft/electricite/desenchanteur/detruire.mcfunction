# On se désactive
function tdh:craft/electricite/desenchanteur/desactiver

# On vide tous nos slots
execute if data entity @s data.contenu.queue[0] run function tdh:craft/electricite/desenchanteur/vider


tellraw @a[distance=..10] [{"text":"Destruction du désenchanteur."}]
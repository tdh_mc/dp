# On vient d'échouer à l'opération d'utilisation du désenchanteur
# (la file d'attente est pleine)

# On restaure l'item dans la main du joueur
item replace entity @p[tag=TestMachineTDH] weapon.mainhand from block ~ ~ ~ container.0
# On vide le slot 0 du brewing stand
item replace block ~ ~ ~ container.0 with air

# On nettoie nos données temporaires
data remove entity @s data.contenu.coffre_temp
data remove entity @s data.output_temp

# On affiche un message d'erreur
title @a[tag=TestMachineTDH] actionbar [{"text":"La file d'attente est pleine!","color":"red"}]
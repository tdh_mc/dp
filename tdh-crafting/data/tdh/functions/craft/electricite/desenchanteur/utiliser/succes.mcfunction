# On a déjà défini :
# - #tdhCraft temp = le nombre d'items du tag enchantable présents sur le joueur avant l'opération
# - #tdhCraft temp2 = le nombre d'items ajoutés par le joueur
# - #tdhCraft temp4 = l'enchantabilité de l'item

# On ajoute l'item à la queue
data modify entity @s data.contenu.queue append value {enchantability:0b,book:{id:"minecraft:book",Count:1b},item:{id:"minecraft:air",Count:1b}}
execute store result entity @s data.contenu.queue[-1].enchantability byte 1 run scoreboard players get #tdhCraft temp4
data modify entity @s data.contenu.queue[-1].item.id set from block ~ ~ ~ Items[0].id
data modify entity @s data.contenu.queue[-1].item.tag set from block ~ ~ ~ Items[0].tag

# On copie le conteneur temporaire stocké dans nos données sur la block entity du conteneur, puis on supprime notre version des données
data modify entity @s data.contenu.queue[-1].book set from entity @s data.output_temp
data modify block ~ ~-1 ~ Items set from entity @s data.contenu.coffre_temp
data remove entity @s data.contenu.coffre_temp
data remove entity @s data.output_temp

# S'il y avait plus de 1 item dans ce stack, on rend les autres au joueur
execute if score #tdhCraft temp2 matches 2.. run function tdh:craft/electricite/desenchanteur/utiliser/rendre_excedent

# On vide le slot 0 du brewing stand
item replace block ~ ~ ~ container.0 with air

# Si on n'est pas actif, on lance la routine de démarrage
execute as @s[tag=!Active] run function tdh:craft/electricite/desenchanteur/activer

# On affiche un message de succès
title @a[tag=TestMachineTDH] actionbar [{"text":"Ajout de ","color":"gray"},{"text":"1 ","color":"gold","extra":[{"entity":"@s","nbt":"data.contenu.queue[-1].item.id"}]},{"text":" au désenchanteur."}]
# Ce mode est un peu particulier, car il signifie qu'on a cliqué sur le brewing stand avec une main vide
# Cela permet de retirer le dernier élément ajouté et de le reprendre dans sa main

# Le livre est également /give, mais pas spécifiquement dans la main, afin qu'il se stacke avec les éventuels livres déjà présents dans l'inventaire du joueur

# On vérifie qu'il y a au moins un item dans la queue
execute store result score #tdhCraft temp8 if data entity @s data.contenu.queue[0]
execute if score #tdhCraft temp8 matches ..0 run function tdh:craft/electricite/desenchanteur/utiliser/echec/queue_vide
# Si la queue n'est pas vide, on vérifie que la main du joueur est bien vide
execute if score #tdhCraft temp8 matches 1.. run function tdh:craft/electricite/desenchanteur/utiliser/test_main/0
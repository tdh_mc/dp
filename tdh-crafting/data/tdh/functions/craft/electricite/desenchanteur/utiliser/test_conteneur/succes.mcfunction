# On a réussi à trouver un livre dans le conteneur
# On copie son ID et son tag (au cas où il ait un nom, par exemple)
data modify entity @s data.output_temp set from storage tdh:conteneur resultat
data modify entity @s data.output_temp.Count set value 1b

# On retranche 1 au nombre d'items contenus dans ce slot du conteneur
data modify entity @s data.contenu.coffre_temp set from storage tdh:conteneur contenu
execute store result score @s temp8 run data get entity @s data.contenu.coffre_temp[0].Count
execute store result entity @s data.contenu.coffre_temp[0].Count byte 1 run scoreboard players remove @s temp8 1

# On teste ensuite l'objet tenu en main par le joueur
function tdh:craft/electricite/desenchanteur/utiliser/test_objet

# On teste d'abord s'il y a un livre non enchanté dans le bloc en-dessous de nous
data modify storage tdh:conteneur recherche.item set value {id:"minecraft:book"}
execute positioned ~ ~-1 ~ run function tdh:conteneur/trouver_item

# Selon le retour de la fonction ci-dessus, on affiche un message d'erreur ou on exécute les prochains tests
execute if score #conteneur temp matches ..-10 run function tdh:craft/electricite/desenchanteur/utiliser/echec/pas_de_conteneur
execute if score #conteneur temp matches -9..-1 run function tdh:craft/electricite/desenchanteur/utiliser/echec/pas_de_livre
execute if score #conteneur temp matches 0.. run function tdh:craft/electricite/desenchanteur/utiliser/test_conteneur/succes
# On vient d'échouer à l'opération d'utilisation du désenchanteur
# (on tenait un objet en main alors qu'on a activé le mode "retirer un objet")

# On affiche un message d'erreur
title @a[tag=TestMachineTDH] actionbar [{"text":"Votre main doit être libre pour retirer un objet!","color":"red"}]
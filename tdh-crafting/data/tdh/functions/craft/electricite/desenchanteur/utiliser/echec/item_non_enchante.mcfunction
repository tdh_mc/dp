# On vient d'échouer à l'opération d'utilisation du désenchanteur
# (on tenait un objet en main qui était enchantable, mais pas enchanté)

# On restaure l'item dans la main du joueur
item replace entity @p[tag=TestMachineTDH] weapon.mainhand from block ~ ~ ~ container.0
# On vide le slot 0 du brewing stand
item replace block ~ ~ ~ container.0 with air

# On nettoie nos données temporaires
data remove entity @s data.contenu.coffre_temp
data remove entity @s data.output_temp

# On affiche un message d'erreur
title @a[tag=TestMachineTDH] actionbar [{"text":"Cet objet n'est pas enchanté!","color":"red"}]
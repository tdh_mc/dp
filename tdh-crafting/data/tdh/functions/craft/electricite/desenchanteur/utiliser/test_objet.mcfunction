
# On copie l'objet tenu en main par le joueur dans le storage de notre machine
item replace block ~ ~ ~ container.0 from entity @p[tag=TestMachineTDH] weapon.mainhand

# Selon la condition passée par le joueur pour obtenir l'advancement, on vérifie ce qu'il tient en main :
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/desenchanteur/interaction={5=true}}] run function tdh:craft/electricite/desenchanteur/utiliser/test_objet/5
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/desenchanteur/interaction={10=true}}] run function tdh:craft/electricite/desenchanteur/utiliser/test_objet/10
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/desenchanteur/interaction={15=true}}] run function tdh:craft/electricite/desenchanteur/utiliser/test_objet/15
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/desenchanteur/interaction={25=true}}] run function tdh:craft/electricite/desenchanteur/utiliser/test_objet/25
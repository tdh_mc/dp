# On a déjà défini :
# - #tdhCraft temp = le nombre d'items du tag enchantable présents sur le joueur avant l'opération
# - #tdhCraft temp2 = le nombre d'items en mainhand du joueur (qu'on s'apprête à retirer)
# - #tdhCraft temp4 = l'enchantabilité de l'item

# On vérifie si l'item sauvegardé dans le brewing stand est bien enchanté
execute unless data block ~ ~ ~ Items[0].tag.Enchantments run scoreboard players set #tdhCraft temp3 -500

# S'il n'y avait pas d'enchantements, on annule l'opération
execute if score #tdhCraft temp3 matches -500 run function tdh:craft/electricite/desenchanteur/utiliser/echec/item_non_enchante

# Sinon, on vérifie que la queue n'est pas pleine
execute unless score #tdhCraft temp3 matches -500 run function tdh:craft/electricite/desenchanteur/utiliser/test_queue
# On vient d'échouer à l'opération d'utilisation du désenchanteur
# (pas de conteneur sous le désenchanteur)

# On nettoie le storage temporaire du conteneur
data remove entity @s data.contenu.coffre_temp

# On affiche un message d'erreur
title @a[tag=TestMachineTDH] actionbar [{"text":"Soit il n'y a pas de conteneur, soit il ne contient pas de livres.","color":"red"}]
# Ce mode est un peu particulier, car il signifie qu'on a cliqué sur le brewing stand avec une main vide
# Cela permet de retirer le dernier élément ajouté et de le reprendre dans sa main

# Le livre est également /give, mais pas spécifiquement dans la main, afin qu'il se stacke avec les éventuels livres déjà présents dans l'inventaire du joueur

# On vérifie que le joueur ne tient pas d'item dans sa main, car on va faire un item replace
# donc on ne veut pas risquer d'override quelque chose d'autre
execute store result score #tdhCraft temp9 if data entity @p[tag=TestMachineTDH] SelectedItem
execute if score #tdhCraft temp9 matches 1.. run function tdh:craft/electricite/desenchanteur/utiliser/echec/item_en_main
# Si la main est bien vide, on vérifie qu'il y a bien au moins un item dans la queue
execute if score #tdhCraft temp9 matches ..0 run function tdh:craft/electricite/desenchanteur/utiliser/retirer_item
# On rend les items excédentaires d'un stack >1 au joueur
# On a déjà défini :
# - #tdhCraft temp = le nombre d'items du tag enchantable présents sur le joueur avant l'opération
# - #tdhCraft temp2 = le nombre d'items ajoutés par le joueur
# - #tdhCraft temp4 = l'enchantabilité de l'item

# On retranche 1 au nombre d'items ajoutés par le joueur, et on stocke cette valeur dans le conteneur
execute store result block ~ ~ ~ Items[0].Count byte 1 run scoreboard players remove #tdhCraft temp2 1
# On restaure l'item dans la main du joueur
item replace entity @p[tag=TestMachineTDH] weapon.mainhand from block ~ ~ ~ container.0
# On vient d'échouer à l'opération d'utilisation du désenchanteur
# (on tenait un objet en main alors qu'on a activé le mode "retirer un objet")

# On affiche un message d'erreur
title @a[tag=TestMachineTDH] actionbar [{"text":"La file d\'attente est vide!","color":"red"}]
# On a déjà défini :
# - #tdhCraft temp = le nombre d'items du tag enchantable présents sur le joueur avant l'opération
# - #tdhCraft temp2 = le nombre d'items en mainhand du joueur (qu'on s'apprête à retirer)
# - #tdhCraft temp4 = l'enchantabilité de l'item

# On vérifie la longueur de la queue et on l'empêche de grandir si elle est >=13
execute store result score #tdhCraft temp9 run data get entity @s data.contenu.queue

# S'il y a trop d'objets, on annule l'opération
execute if score #tdhCraft temp9 matches 13.. run function tdh:craft/electricite/desenchanteur/utiliser/echec/queue_pleine

# Sinon, l'opération réussit
execute unless score #tdhCraft temp9 matches 13.. run function tdh:craft/electricite/desenchanteur/utiliser/succes
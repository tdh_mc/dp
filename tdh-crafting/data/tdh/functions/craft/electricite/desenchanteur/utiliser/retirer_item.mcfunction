# On retire le dernier item placé (donc celui à la fin de la liste) pour le placer dans la main du joueur

# On met dans la main du joueur l'outil
data modify entity @s data.contenu.queue[-1].item.Slot set value 0b
data modify block ~ ~ ~ Items prepend from entity @s data.contenu.queue[-1].item
item replace entity @p[tag=TestMachineTDH] weapon.mainhand from block ~ ~ ~ container.0
data remove block ~ ~ ~ Items[0]

# On summon à sa position le livre pour qu'il le récupère instantanément
execute at @p[tag=TestMachineTDH] run summon item ~ ~ ~ {Item:{id:"minecraft:stone",Count:1b},PickupDelay:0s,Age:0s,Tags:["TempOutputMachineTDH"]}
data modify entity @e[type=item,tag=TempOutputMachineTDH,limit=1] Item set from entity @s data.contenu.queue[-1].book
data modify entity @e[type=item,tag=TempOutputMachineTDH,limit=1] Owner set from entity @p[tag=TestMachineTDH] UUID

# On retire le dernier élément de la queue
data remove entity @s data.contenu.queue[-1]

# S'il n'y a plus d'élément dans la queue, on désactive la machine
execute unless data entity @s data.contenu.queue[0] run function tdh:craft/electricite/desenchanteur/desactiver

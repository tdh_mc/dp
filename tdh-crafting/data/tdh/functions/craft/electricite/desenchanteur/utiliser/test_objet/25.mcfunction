# On enregistre le nombre d'items de ce tier d'enchantement pourraient être retirés du joueur au maximum
execute store result score #tdhCraft temp run clear @p[tag=TestMachineTDH] #tdh:craft/enchantable/25 0
# On enregistre également le nombre d'items présents dans la main du joueur
execute store result score #tdhCraft temp2 run data get block ~ ~ ~ Items[0].Count

# On définit une variable de contrôle pour enregistrer le passage ou non des conditions ci-dessous
scoreboard players set #tdhCraft temp9 0

# Si le nombre d'items dans la main du joueur est supérieur au nombre d'items enchantables détectés,
# c'est qu'on ne tient plus le bon objet, et on annule l'opération
execute if score #tdhCraft temp2 > #tdhCraft temp run scoreboard players set #tdhCraft temp9 -1

# Sinon (= il est inférieur ou égal au nombre d'items enchantables pouvant être retirés),
# on exécute la suite des opérations
execute if score #tdhCraft temp9 matches 0 run function tdh:craft/electricite/desenchanteur/utiliser/test_main/25
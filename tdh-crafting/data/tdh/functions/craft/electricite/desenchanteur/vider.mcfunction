# On vide tous les items en attente du désenchanteur

# On vide le premier slot de la queue
summon item ~ ~ ~ {Tags:["TempItem"],Item:{id:"minecraft:cobblestone",Count:1b}}
data modify entity @e[type=item,tag=TempItem,distance=0,limit=1] Item set from entity @s data.contenu.queue[0].item
tag @e[type=item,tag=TempItem] remove TempItem

summon item ~ ~ ~ {Tags:["TempItem"],Item:{id:"minecraft:cobblestone",Count:1b}}
data modify entity @e[type=item,tag=TempItem,distance=0,limit=1] Item set from entity @s data.contenu.queue[0].book
tag @e[type=item,tag=TempItem] remove TempItem

# On supprime ce slot de la queue
data remove entity @s data.contenu.queue[0]

# S'il reste des slots à la queue, on recommence cette fonction
execute if data entity @s data.contenu.queue[0] run function tdh:craft/electricite/desenchanteur/vider
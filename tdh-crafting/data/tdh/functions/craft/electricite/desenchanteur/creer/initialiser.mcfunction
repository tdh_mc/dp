# On initialise le statut du générateur (= temps de désenchantement)
# et son maximum (= temps total pour désenchanter un objet)
# Les deux valeurs sont exprimées en ticks du système électrique (par défaut 4 ticks par seconde)
# Il faut donc 20 secondes + 1 log de bois (ou 1/4 de charbon) pour désenchanter 1 objet
scoreboard players set @s status 0
scoreboard players set @s max 80

# On supprime le tag ayant servi à nous sélectionner
tag @s remove MachineTDHTemp
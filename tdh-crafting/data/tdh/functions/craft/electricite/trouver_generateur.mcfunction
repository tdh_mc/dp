# On exécute cette fonction en tant qu'une machine électrique à sa position,
# pour trouver un générateur valide aux alentours

# On tag les générateurs à moins de 7 blocs de nous (sur les axes X/Z) mais seulement 3 blocs en hauteur
# On ne prend pas en compte les générateurs ayant un status <= 0 (signifie une absence de combustible)
execute positioned ~-7.5 ~-3.5 ~-7.5 run tag @e[type=marker,tag=Generateur,dx=15,dy=7,dz=15,scores={status=1..}] add TempGenerateurValide

# S'il n'y a pas de générateur valide, on cherche les répéteurs de circuit selon les mêmes critères
# (récursif de répéteur en répéteur jusqu'à trouver un générateur OU épuiser les répéteurs)
execute unless entity @e[type=marker,tag=TempGenerateurValide] run function tdh:craft/electricite/trouver_generateur/test_repeteurs

# Si à ce stade on n'a pas d'entité ayant le tag TempGenerateurValide,
# c'est qu'il n'y a aucune énergie accessible dans ce réseau
#execute unless entity @e[type=marker,tag=TempGenerateurValide] run tag @s add 

# On choisit le générateur valide le plus proche comme "résultat" de la fonction
tag @e[type=marker,tag=TempGenerateurValide,sort=nearest,limit=1] add GenerateurValide

# On retire tous les tags temporaires
tag @e[type=marker,tag=TempGenerateurValide] remove TempGenerateurValide
tag @e[type=marker,tag=TempRepeteurValide] remove TempRepeteurValide
tag @e[type=marker,tag=TempRepeteurUtilise] remove TempRepeteurUtilise
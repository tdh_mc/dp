# Un désenchanteur inactif vérifie périodiquement s'il s'est mis en pause par manque de combustible,
# et si tel est le cas il vérifie s'il y a désormais du combustible disponible aux alentours

execute as @s[tag=NoEnergie] run function tdh:craft/electricite/enchanteur/tick/test_combustible

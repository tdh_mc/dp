# On exécute cette fonction pour désactiver manuellement l'enchanteur
# (potentiellement pendant une opération d'enchantement en cours)

# On vérifie s'il y a un coffre en-dessous de nous, et s'il contient au moins 2 slots libres consécutifs
scoreboard players set #conteneur nombre 2
execute positioned ~ ~-1 ~ run function tdh:conteneur/trouver_slots_libres_consecutifs

# Si on a trouvé 2 slots libres consécutifs, on y replace les items contenus dans l'enchanteur
execute unless score #conteneur temp matches ..-1 run function tdh:craft/electricite/enchanteur/utiliser/desactiver/succes
# Sinon, on affiche un message d'erreur sur l'item
execute if score #conteneur temp matches ..-10 run function tdh:craft/electricite/enchanteur/utiliser/echec/aucun_conteneur
execute if score #conteneur temp matches -9..-1 run function tdh:craft/electricite/enchanteur/utiliser/echec/aucune_place_consecutive
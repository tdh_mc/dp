# On replace les items de la queue dans le conteneur de l'enchanteur
# On a les numéros de slot dans le storage tdh:conteneur resultat, sous forme d'une liste de compounds avec un seul tag ({Slot:Xb})

# On modifie les items dans le storage
data modify storage tdh:conteneur contenu append from entity @s data.contenu.queue[0].item
data modify storage tdh:conteneur contenu[-1].Slot set from storage tdh:conteneur resultat[0].Slot

data modify storage tdh:conteneur contenu append from entity @s data.contenu.queue[0].book
data modify storage tdh:conteneur contenu[-1].Slot set from storage tdh:conteneur resultat[1].Slot

# On réassigne l'inventaire du conteneur
data modify block ~ ~-1 ~ Items set from storage tdh:conteneur contenu

# On désactive l'enchanteur
function tdh:craft/electricite/enchanteur/desactiver
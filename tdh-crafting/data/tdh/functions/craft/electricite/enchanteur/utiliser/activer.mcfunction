# On exécute cette fonction pour activer l'enchanteur
# (ou pour changer d'item après la fin de l'opération précédente)

# On trouve le premier livre enchanté présent dans le coffre
data modify storage tdh:conteneur recherche.item set value {id:"minecraft:enchanted_book"}
execute positioned ~ ~-1 ~ run function tdh:conteneur/trouver_item

# Si on n'a pas trouvé de livre enchanté, on affiche une erreur "pas de livre"
execute if score #conteneur temp matches ..-10 run function tdh:craft/electricite/enchanteur/utiliser/echec/aucun_conteneur
execute if score #conteneur temp matches -9..-1 run function tdh:craft/electricite/enchanteur/utiliser/echec/aucun_livre_enchante
# Sinon, on vérifie si on trouve un item dans un slot adjacent au livre enchanté
execute if score #conteneur temp matches 0.. run function tdh:craft/electricite/enchanteur/utiliser/activer/test_item
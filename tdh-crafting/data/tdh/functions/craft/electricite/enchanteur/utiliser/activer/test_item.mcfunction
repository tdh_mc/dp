# On a trouvé un livre enchanté (dans le slot #conteneur temp)
# Ce livre est stocké dans le storage conteneur resultat, et conteneur contenu contient tous les items de l'inventaire du coffre en-dessous de nous

# On teste le slot précédent
execute store result score #tdhCraft temp9 run data get storage tdh:conteneur contenu[-1].Slot
scoreboard players add #tdhCraft temp9 1

# Si le slot précédent est bien juste à côté du slot actuel (contenant le livre enchanté),
# alors on les ajoute tous les deux à la queue
execute if score #tdhCraft temp9 = #conteneur temp run function tdh:craft/electricite/enchanteur/utiliser/activer/succes
# Sinon, on affiche une autre erreur
execute unless score #tdhCraft temp9 = #conteneur temp run function tdh:craft/electricite/enchanteur/utiliser/echec/aucun_item
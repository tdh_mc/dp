# On enregistre les items contenus dans le slot actuel et le slot précédent
data modify entity @s data.contenu.queue append value {item:{id:"minecraft:air",Count:1b},book:{id:"minecraft:enchanted_book",Count:1b}}
data modify entity @s data.contenu.queue[0].item set from storage tdh:conteneur contenu[-1]
data modify entity @s data.contenu.queue[0].book set from storage tdh:conteneur contenu[0]

# On supprime ces deux items de l'inventaire du coffre
data remove storage tdh:conteneur contenu[-1]
data remove storage tdh:conteneur contenu[0]
# Puis on replace cet inventaire modifié dans le coffre d'origine
data modify block ~ ~-1 ~ Items set from storage tdh:conteneur contenu

# On active l'enchanteur
execute as @s[tag=Active] run function tdh:craft/electricite/enchanteur/reinitialiser_statut
execute as @s[tag=!Active] run function tdh:craft/electricite/enchanteur/activer
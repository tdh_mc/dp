# On exécute cette fonction en tant qu'une entité Enchanteur qui vient d'être utilisée par un joueur,
# pour vérifier si l'objet tenu en main par le joueur est compatible avec le désenchanteur

# Structure des données de l'entité :
# data: {
#	contenu: {
#		queue: [	(une liste qui ne contiendra jamais apriori qu'un élément)
#			{
#				enchantability:15b,
#				item: {id:"minecraft:iron_sword",Count:1b,tag:{ (etc) }},
#				book: {id:"minecraft:book",Count:1b,tag:{ (etc) }},
#			}
#		],
#		coffre_temp: [
#			(inventaire du coffre en-dessous, seulement présent pendant l'opération de recherche)
#		]
#	},
#	output: {
#		item: {id:"minecraft:iron_sword",Count:1b,tag:{ (...) }}
#	},							(Absent si aucun output valide n'a été trouvé)
#	output_temp: {id:"minecraft:book} (présent seulement pendant l'opération de recherche)
# }

# On enregistre le fait qu'on a trouvé une machine (pour ne pas en activer X à la suite)
scoreboard players set #tdhCraft status 1

# Selon si la machine est actuellement activée ou désactivée, on tente de la désactiver ou de l'activer
execute store result score @s temp5 if entity @s[tag=Active]
execute if score @s temp5 matches 1.. run function tdh:craft/electricite/enchanteur/utiliser/desactiver
execute unless score @s temp5 matches 1.. run function tdh:craft/electricite/enchanteur/utiliser/activer

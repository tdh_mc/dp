# On se désactive
function tdh:craft/electricite/enchanteur/desactiver

# On vide tous nos slots
# On utilise la fonction du désenchanteur car nos données ont la même structure
execute if data entity @s data.contenu.queue[0] run function tdh:craft/electricite/desenchanteur/vider


tellraw @a[distance=..10] [{"text":"Destruction de l'enchanteur."}]
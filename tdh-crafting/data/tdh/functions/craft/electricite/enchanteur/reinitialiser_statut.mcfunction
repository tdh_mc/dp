
# On réinitialise le statut de l'opération de désenchantement
scoreboard players set @s status 0

# On réinitialise les informations de notre item
execute positioned ~ ~.5 ~ run data modify entity @e[type=item,tag=ObjetEnchante,distance=..0.1,limit=1] CustomName set value '[{"text":"Progression: ","color":"gray"},{"text":"0%","color":"gold"}]'
execute positioned ~ ~.5 ~ run data modify entity @e[type=item,tag=ObjetEnchante,distance=..0.1,limit=1] Item set from entity @s data.contenu.queue[0].item
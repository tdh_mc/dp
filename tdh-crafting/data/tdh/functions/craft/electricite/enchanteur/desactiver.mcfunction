tellraw @a[distance=..10] [{"text":"Désactivation de l'enchanteur."}]

title @p[tag=TestMachineTDH] actionbar [{"text":"Enchanteur désactivé!","color":"green"}]

# On se retire le tag
tag @s remove Active

# On définit notre progression dans la tâche en cours à 0
scoreboard players set @s status 0

# On tue l'item temporaire éventuellement présent au-dessus de nous
execute positioned ~ ~.5 ~ run kill @e[type=item,distance=..0.1,tag=ObjetEnchante]
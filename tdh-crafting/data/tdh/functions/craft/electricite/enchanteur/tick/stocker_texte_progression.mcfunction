# On exécute cette fonction en tant qu'un item dont on veut changer le nom pour afficher la progression
# à la position de l'item frame TexteTemporaire (ou simplement celle de n'importe quel panneau)

# On stocke dans le panneau la phrase à rendre
data modify block ~ ~ ~ Text1 set value '[{"text":"Progression: ","color":"gray"},{"score":{"name":"#tdhCraft","objective":"status"},"color":"gold","extra":[{"text":"%"}]}]'

# On copie cette phrase du panneau à notre CustomName
data modify entity @s CustomName set from block ~ ~ ~ Text1
# On veut passer à l'opération suivante, mais on doit commencer par placer dans le conteneur en-dessous de nous les items résultats de l'opération précédente

# On vérifie s'il y a une place disponible dans le coffre en-dessous de nous
execute positioned ~ ~-1 ~ run function tdh:conteneur/trouver_slot_libre

# S'il n'y a pas de place, on met un message d'erreur comme quoi le coffre est plein
# mais on ne reset pas notre progression (on reste en attente jusqu'à ce qu'il y ait de la place)
execute if score #conteneur temp matches ..-10 run function tdh:craft/electricite/enchanteur/tick/echec/pas_de_coffre
execute if score #conteneur temp matches -9..-1 run function tdh:craft/electricite/enchanteur/tick/echec/pas_de_place
execute if score #conteneur temp matches 0.. run function tdh:craft/electricite/enchanteur/tick/prochain/succes
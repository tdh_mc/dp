# On copie le premier enchantement
data modify entity @s data.output.item.tag.Enchantments append from entity @s data.contenu.queue[0].book.tag.StoredEnchantments[0]

# On le supprime du livre d'origine
data remove entity @s data.contenu.queue[0].book.tag.StoredEnchantments[0]

# S'il reste des enchantements, on copie le suivant
scoreboard players remove #tdhCraft max 1
execute if score #tdhCraft max matches 1.. run function tdh:craft/electricite/enchanteur/tick/prochain/copier_enchantement
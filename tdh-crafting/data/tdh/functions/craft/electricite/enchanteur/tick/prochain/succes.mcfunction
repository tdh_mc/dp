# On a réussi à trouver un slot pour placer l'item enchanté
# ce slot est enregistré dans #conteneur temp

# On calcule notre output
# On copie dans l'output notre outil avec le bon numéro de slot
data modify entity @s data.output.item set from entity @s data.contenu.queue[0].item
execute store result entity @s data.output.item.Slot byte 1 run scoreboard players get #conteneur temp
# On enchante l'outil selon les enchantements stockés sur le livre
execute store result score #tdhCraft max run data get entity @s data.contenu.queue[0].book.tag.StoredEnchantments
execute if score #tdhCraft max matches 1.. run function tdh:craft/electricite/enchanteur/tick/prochain/copier_enchantement

# On ajoute au conteneur l'outil enchanté
data modify block ~ ~-1 ~ Items append from entity @s data.output.item

# On les supprime de nos données
data remove entity @s data.output
data remove entity @s data.contenu.queue[0]

# On passe à l'item suivant
function tdh:craft/electricite/enchanteur/utiliser/activer

# On progresse d'un tick dans l'opération d'enchantement en cours
scoreboard players add @s status 1

# On consomme 1 unité de combustible dans notre générateur
scoreboard players remove @e[type=marker,tag=GenerateurValide,limit=1] status 1

# On affiche cette information sur notre item
# On calcule le pourcentage de progression
scoreboard players set #tdhCraft status 100
scoreboard players operation #tdhCraft status *= @s status
scoreboard players operation #tdhCraft status /= @s max

# On exécute la fonction en tant que l'item dont on veut changer le nom,
# à la position de l'item frame TexteTemporaire
execute positioned ~ ~.5 ~ as @e[type=item,tag=ObjetEnchante,distance=..0.1,limit=1] at @e[type=item_frame,tag=TexteTemporaire,limit=1] run function tdh:craft/electricite/enchanteur/tick/stocker_texte_progression
# On vérifie si on peut trouver un générateur électrique proche qui nous fournisse de l'énergie
# On trouve le générateur le plus proche (hors générateurs vides d'énergie)
function tdh:craft/electricite/trouver_generateur
# Le résultat est donné sous la forme du tag "GenerateurValide" donné à un marqueur au maximum

# Si on a trouvé un générateur valide, on rallume la machine
execute if entity @e[type=marker,tag=GenerateurValide] run function tdh:craft/electricite/enchanteur/activer

# On supprime le tag GenerateurValide
tag @e[type=marker,tag=GenerateurValide] remove GenerateurValide
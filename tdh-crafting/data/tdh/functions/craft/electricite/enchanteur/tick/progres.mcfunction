

# Si on n'est pas déjà au maximum, on incrémente notre statut
execute if score @s status < @s max run function tdh:craft/electricite/enchanteur/tick/incrementer_statut

# Si on est arrivés au maximum, on passe à l'opération suivante
execute if score @s status >= @s max run function tdh:craft/electricite/enchanteur/tick/prochain
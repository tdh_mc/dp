# On se désactive
tag @s remove Active
tag @s add NoEnergie

# On réinitialise la progression de l'opération en cours
scoreboard players set @s status 0

# On supprime l'éventuel item invoqué au-dessus de la machine
execute positioned ~ ~.5 ~ run kill @e[type=item,distance=..0.1,tag=ObjetEnchante]

# On affiche un message d'erreur aux joueurs alentour
tellraw @a[distance=..7.5] [{"text":"L'enchanteur est à court de combustible!","color":"red"}]
# S'il y a déjà une machine à cet emplacement, on la détruit
execute as @e[type=marker,tag=MachineElectriqueTDH,distance=..0.5] run function tdh:craft/electricite/detruire

# On crée une entité "Enchanteur électrique" à la position d'exécution de la fonction,
# qui doit également être celle du brewing stand
summon marker ~ ~ ~ {Tags:["MachineElectriqueTDH","Enchanteur","MachineTDHTemp"]}

# On initialise correctement l'entité
execute as @e[type=marker,tag=MachineTDHTemp] at @s run function tdh:craft/electricite/enchanteur/creer/initialiser

# On affiche un message de confirmation à l'éventuel joueur ayant le tag TestMachineTDH
tellraw @a[tag=TestMachineTDH] [{"text":"Cet ","color":"gray"},{"text":"enchanteur","color":"gold","bold":"true"},{"text":" a été initialisé avec succès."}]
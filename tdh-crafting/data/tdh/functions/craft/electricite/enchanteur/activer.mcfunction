# On enregistre le fait qu'on est actif
tag @s add Active
# On supprime les éventuels tags temporaires donnés précédemment
tag @s remove NoEnergie

title @p[tag=TestMachineTDH] actionbar [{"text":"Enchanteur activé!","color":"green"}]

# On détruit tout item précédent qui n'aurait pas été correctement retiré
execute positioned ~ ~.5 ~ run kill @e[type=item,distance=..0.1,tag=ObjetEnchante]

# On crée un nouvel item temporaire
summon item ~ ~.5 ~ {Tags:["ObjetEnchante"],Item:{id:"minecraft:cobblestone",Count:1b},PickupDelay:32767s,Age:-32768s,NoGravity:1b,Invulnerable:1b,CustomNameVisible:1b,CustomName:'[{"text":"Progression: ","color":"gray"},{"text":"0%","color":"gold"}]'}
# On lui transmet l'apparence de l'item en cours de traitement
data modify entity @e[type=item,tag=ObjetEnchante,distance=..1,limit=1] Item merge from entity @s data.contenu.queue[0].item
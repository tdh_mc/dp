# On exécute cette fonction régulièrement pour ticker les machines électriques

# On a une fonction différente selon le type de machine
execute as @s[tag=GenerateurThermique] run function tdh:craft/electricite/generateur/tick
execute as @s[tag=Desenchanteur] run function tdh:craft/electricite/desenchanteur/tick
execute as @s[tag=Enchanteur] run function tdh:craft/electricite/enchanteur/tick
# La trieuse n'est pas ici car elle ticke plus lentement pour éviter de faire crasher le serveur avec ses 20000 opérations NBT
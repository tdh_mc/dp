# On vérifie si des joueurs ont interagi avec des dispositifs électriques

# - Générateurs
execute at @a[gamemode=!spectator,advancements={tdh:craft/electricite/generateur/remplissage_combustible=true}] run function tdh:craft/electricite/generateur/test_joueur

# - Désenchanteurs
execute at @a[gamemode=!spectator,advancements={tdh:craft/electricite/desenchanteur/interaction=true}] run function tdh:craft/electricite/desenchanteur/test_joueur

# - Enchanteurs
execute at @a[gamemode=!spectator,advancements={tdh:craft/electricite/enchanteur/interaction=true}] run function tdh:craft/electricite/enchanteur/test_joueur

# - Trieuses
execute at @a[gamemode=!spectator,advancements={tdh:craft/electricite/trieuse/interaction=true}] run function tdh:craft/electricite/trieuse/test_joueur

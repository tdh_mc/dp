# On exécute cette fonction en tant qu'une trieuse ayant le tag TrieuseAReset,
# pour vérifier si le joueur a fait son choix

# On a une sous-fonction différente pour chaque valeur possible de la variable
# n'importe quelle valeur inconnue (sauf 0) = annuler l'opération
execute unless score @p[tag=TrieuseAReset] resetTrieuse matches 0 unless score @p[tag=TrieuseAReset] resetTrieuse matches 101..105 run function tdh:craft/electricite/trieuse/utiliser/effacer/annuler
# 101-104 = nord/est/sud/ouest
execute if score @p[tag=TrieuseAReset] resetTrieuse matches 101 run function tdh:craft/electricite/trieuse/utiliser/nord/effacer/succes
execute if score @p[tag=TrieuseAReset] resetTrieuse matches 102 run function tdh:craft/electricite/trieuse/utiliser/est/effacer/succes
execute if score @p[tag=TrieuseAReset] resetTrieuse matches 103 run function tdh:craft/electricite/trieuse/utiliser/sud/effacer/succes
execute if score @p[tag=TrieuseAReset] resetTrieuse matches 104 run function tdh:craft/electricite/trieuse/utiliser/ouest/effacer/succes
# 105 = effacer tout
execute if score @p[tag=TrieuseAReset] resetTrieuse matches 105 run function tdh:craft/electricite/trieuse/utiliser/haut/effacer/succes

# On retire le tag temporaire de notre trieuse
tag @e[type=marker,tag=OurTrieuseAReset] remove OurTrieuseAReset
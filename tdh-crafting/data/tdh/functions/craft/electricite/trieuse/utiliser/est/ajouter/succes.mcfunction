# On ajoute l'item à nos filtres
data modify entity @s data.filtres.est append from entity @s data.item

# On affiche un message de succès
tellraw @p[tag=TestMachineTDH] [{"text":"[Trieuse]","color":"gold"},{"text":" Le filtre ","color":"gray"},{"text":"est"},{"text":" matchera désormais ","color":"gray"},{"nbt":"data.item.id","entity":"@s","color":"yellow","extra":[{"nbt":"data.item.tag","entity":"@s"}]},{"text":"."}]
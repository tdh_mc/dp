# On souhaite ajouter l'item tenu en main à la liste des filtres de ce côté de la trieuse

# On copie l'item tenu en main dans nos données
# On ne prend que les tags "id" et quelques éléments spécifiques du "tag"
# (on ne peut par exemple pas filtrer par type d'enchantement, ou selon d'autres tags de type liste)
data modify entity @s data.item set value {id:""}
data modify entity @s data.item.id set from entity @p[tag=TestMachineTDH] SelectedItem.id
data modify entity @s data.item.tag.CustomModelData set from entity @p[tag=TestMachineTDH] SelectedItem.tag.CustomModelData
data modify entity @s data.item.tag.BlockEntityTag set from entity @p[tag=TestMachineTDH] SelectedItem.tag.BlockEntityTag
data modify entity @s data.item.tag.EntityTag set from entity @p[tag=TestMachineTDH] SelectedItem.tag.EntityTag
data modify entity @s data.item.tag.Potion set from entity @p[tag=TestMachineTDH] SelectedItem.tag.Potion

# On vérifie s'il y a déjà des filtres de ce côté
execute store result score @s temp7 run data get entity @s data.filtres.ouest
# S'il n'y en a pas encore, on peut simplement ajouter cet item à la liste (vide)
execute if score @s temp7 matches ..0 run function tdh:craft/electricite/trieuse/utiliser/ouest/ajouter/succes
# S'il y a déjà des items dans le filtre, on teste chaque item (pour vérifier que celui-là n'y est pas déjà)
execute if score @s temp7 matches 1.. run function tdh:craft/electricite/trieuse/utiliser/ouest/ajouter/loop
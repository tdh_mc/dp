# On exécute cette fonction à la position d'un joueur ayant le tag TrieuseAReset,
# pour vérifier s'il a fait un choix concernant le vidage des filtres

# On vérifie d'abord s'il y a une trieuse ayant le tag à proximité
execute as @e[type=marker,tag=TrieuseAReset,distance=..10] if score @s idMachine = @p[tag=TrieuseAReset] idMachine run tag @s add OurTrieuseAReset

# S'il n'y en a pas, on annule automatiquement l'opération
execute store result score #trieuse temp if entity @e[type=marker,tag=OurTrieuseAReset]
execute if score #trieuse temp matches ..0 run function tdh:craft/electricite/trieuse/utiliser/effacer/annuler
# Sinon, on vérifie les variables du joueur
execute if score #trieuse temp matches 1.. as @e[type=marker,tag=OurTrieuseAReset] at @s run function tdh:craft/electricite/trieuse/utiliser/effacer/test_variable

scoreboard players reset #trieuse temp

# On retire le tag temporaire de notre trieuse
tag @e[type=marker,tag=OurTrieuseAReset] remove OurTrieuseAReset
# On exécute cette fonction lorsque le joueur a confirmé qu'il souhaitait effacer l'intégralité des filtres
data modify entity @s data.filtres set value {nord:[],est:[],sud:[],ouest:[]}

scoreboard players reset @s idMachine
scoreboard players reset @p[tag=TrieuseAReset] idMachine
scoreboard players reset @p[tag=TrieuseAReset] resetTrieuse

# On affiche un message de confirmation
tellraw @p[tag=TrieuseAReset] [{"text":"[Trieuse]","color":"gold"},{"text":" Tous les filtres ont été ","color":"gray"},{"text":"réinitialisés","color":"red"},{"text":".","color":"gray"}]

tag @s remove TrieuseAReset
tag @p[tag=TrieuseAReset] remove TrieuseAReset
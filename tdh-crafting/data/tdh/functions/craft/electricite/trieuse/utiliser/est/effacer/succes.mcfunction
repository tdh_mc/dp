# On exécute cette fonction lorsque le joueur a confirmé qu'il souhaitait effacer les filtres du côté est
data modify entity @s data.filtres.est set value []

scoreboard players reset @s idMachine
scoreboard players reset @p[tag=TrieuseAReset] idMachine
scoreboard players reset @p[tag=TrieuseAReset] resetTrieuse

# On affiche un message de confirmation
tellraw @p[tag=TrieuseAReset] [{"text":"[Trieuse]","color":"gold"},{"text":" Le filtre ","color":"gray"},{"text":"est"},{"text":" ne matche plus ","color":"gray"},{"text":"aucun objet","color":"red"},{"text":"."}]

tag @s remove TrieuseAReset
tag @p[tag=TrieuseAReset] remove TrieuseAReset
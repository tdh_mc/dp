# On décrémente notre variable de contrôle
scoreboard players remove @s temp7 1
# On déplace le premier filtre à la fin de la liste
data modify entity @s data.filtres.est append from entity @s data.filtres.est[0]
data remove entity @s data.filtres.est[0]

# Si on a testé tous les éléments sans succès, on peut simplement ajouter cet item à la liste
execute if score @s temp7 matches ..0 run function tdh:craft/electricite/trieuse/utiliser/est/ajouter/succes
# S'il reste des éléments à tester, on recommence le loop
execute if score @s temp7 matches 1.. run function tdh:craft/electricite/trieuse/utiliser/est/ajouter/loop
# On annule l'opération
scoreboard players reset @s idMachine
scoreboard players reset @p[tag=TrieuseAReset,distance=..10] idMachine
scoreboard players reset @p[tag=TrieuseAReset,distance=..10] resetTrieuse

tellraw @p[tag=TrieuseAReset,distance=..10] [{"text":"[Trieuse]","color":"gold"},{"text":" La réinitialisation du filtre a été ","color":"gray"},{"text":"annulée","color":"yellow"},{"text":"."}]

tag @s remove TrieuseAReset
tag @p[tag=TrieuseAReset,distance=..10] remove TrieuseAReset
# On souhaite effacer la liste des filtres, mais en raison des risques de missclick cela se fait en 2 étapes

# On active simplement la variable à trigger par le joueur pour confirmer la réinitialisation des filtres
scoreboard players set @p[tag=TestMachineTDH] resetTrieuse 0
scoreboard players enable @p[tag=TestMachineTDH] resetTrieuse

# On enregistre que c'est cette machine qui doit être reset
tag @s add TrieuseAReset
tag @p[tag=TestMachineTDH] add TrieuseAReset
# On prend une variable aléatoire pour servir d'ID partagé entre le joueur et la trieuse
scoreboard players set #random min 1
scoreboard players set #random max 99999
function tdh:random
scoreboard players operation @s idMachine = #random temp
scoreboard players operation @p[tag=TestMachineTDH] idMachine = #random temp

# On affiche un message au joueur
tellraw @p[tag=TestMachineTDH] [{"text":"[Trieuse]","color":"gold"},{"text":" Voulez-vous vraiment réinitialiser ","color":"gray"},{"text":"l'intégralité des filtres","color":"red","bold":"true"},{"text":" ?\n","color":"gray"},{"text":"Cliquez ici pour confirmer.","color":"red","italic":"true","clickEvent":{"action":"run_command","value":"/trigger resetTrieuse set 105"},"hoverEvent":{"action":"show_text","contents":[{"text":"Confirmer la réinitialisation de ","color":"gray"},{"text":"l'ensemble des filtres","color":"gold"},{"text":" de la trieuse."}]}},{"text":"\n"},{"text":"Cliquez ici pour annuler.","color":"green","italic":"true","clickEvent":{"action":"run_command","value":"/trigger resetTrieuse set -1"},"hoverEvent":{"action":"show_text","contents":[{"text":"Conserver les filtres actuels.","color":"gray"}]}}]

# On schedule la fonction qui teste cette variable
# (elle devrait s'exécuter si peu fréquemment que ce n'est pas la peine d'en faire une membre d'un tdh-tick)
schedule function tdh:craft/electricite/trieuse/utiliser/effacer/tick 1s
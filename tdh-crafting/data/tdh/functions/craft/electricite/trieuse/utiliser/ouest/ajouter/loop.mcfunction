# On vérifie si le premier item de la liste des filtres matche celui qu'on tient en main
data modify entity @s data.buffer set from entity @s data.item
execute store result score @s temp6 run data modify entity @s data.buffer set from entity @s data.filtres.ouest[0]

# Si la copie a échoué, c'est que cet item est identique, donc on va le RETIRER de la liste
execute if score @s temp6 matches ..0 run function tdh:craft/electricite/trieuse/utiliser/ouest/ajouter/retirer
# Sinon, on passe à l'item suivant
execute if score @s temp6 matches 1.. run function tdh:craft/electricite/trieuse/utiliser/ouest/ajouter/prochain_filtre
# On retire le filtre actuel de la liste des filtres
data remove entity @s data.filtres.ouest[0]

# On affiche un message de succès
tellraw @p[tag=TestMachineTDH] [{"text":"[Trieuse]","color":"gold"},{"text":" Le filtre ","color":"gray"},{"text":"ouest"},{"text":" ne matche plus ","color":"gray"},{"nbt":"data.item.id","entity":"@s","color":"red","extra":[{"nbt":"data.item.tag","entity":"@s"}]},{"text":"."}]
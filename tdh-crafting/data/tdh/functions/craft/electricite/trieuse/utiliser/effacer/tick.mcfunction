# On exécute ce tick pour vérifier si les joueurs qui doivent faire un choix
# (confirmer ou annuler l'effacement d'un filtre de leur trieuse) l'ont fait

execute at @a[tag=TrieuseAReset] run function tdh:craft/electricite/trieuse/utiliser/effacer/tick_joueur

# On re-schedule cette fonction s'il reste des joueurs ayant le tag
execute if entity @p[tag=TrieuseAReset] run schedule function tdh:craft/electricite/trieuse/utiliser/effacer/tick 10t
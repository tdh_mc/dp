# On vérifie si on tient ou non un item en main ;
# Si on n'en tient pas, on veut effacer les filtres de ce côté
# Si on en tient un, on essaie de l'ajouter à notre liste de filtres

execute store result score @s temp8 if data entity @p[tag=TestMachineTDH] SelectedItem
execute if score @s temp8 matches ..0 run function tdh:craft/electricite/trieuse/utiliser/sud/effacer
execute if score @s temp8 matches 1.. run function tdh:craft/electricite/trieuse/utiliser/sud/ajouter
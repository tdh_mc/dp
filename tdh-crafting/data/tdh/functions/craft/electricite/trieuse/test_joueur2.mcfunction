# On enregistre une variable temporaire (pour ne pas activer plusieurs entités à la suite)
scoreboard players set #tdhCraft status 0

# On vérifie le long de notre ligne de visée, tous les 1 blocs, si on trouve une entité Trieuse à 1 bloc de distance
execute positioned ~ ~1.5 ~ positioned ^ ^ ^0.5 as @e[type=marker,tag=Trieuse,distance=..1.1,limit=1] at @s run function tdh:craft/electricite/trieuse/utiliser
execute if score #tdhCraft status matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^1.5 as @e[type=marker,tag=Trieuse,distance=..1.1,limit=1] at @s run function tdh:craft/electricite/trieuse/utiliser
execute if score #tdhCraft status matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^2.5 as @e[type=marker,tag=Trieuse,distance=..1.1,limit=1] at @s run function tdh:craft/electricite/trieuse/utiliser
execute if score #tdhCraft status matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^3.5 as @e[type=marker,tag=Trieuse,distance=..1.1,limit=1] at @s run function tdh:craft/electricite/trieuse/utiliser
execute if score #tdhCraft status matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^4.5 as @e[type=marker,tag=Trieuse,distance=..1.1,limit=1] at @s run function tdh:craft/electricite/trieuse/utiliser
execute if score #tdhCraft status matches 0 positioned ~ ~1.5 ~ positioned ^ ^ ^5.5 as @e[type=marker,tag=Trieuse,distance=..1.1,limit=1] at @s run function tdh:craft/electricite/trieuse/utiliser
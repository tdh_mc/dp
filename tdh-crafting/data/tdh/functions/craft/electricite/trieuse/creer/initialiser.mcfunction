# On initialise le statut de la filtreuse (= timer actuel)
# et son maximum (= temps total avant de lancer une opération de filtrage)
# Les deux valeurs sont exprimées en ticks du système électrique (par défaut 4 ticks par seconde)
# Il s'écoule donc 30 secondes entre 2 filtrations
#scoreboard players set @s status 0
#scoreboard players set @s max 120
# En fait on ne va pas définir ces timers ici ; on va simplement avoir un tick beaucoup plus lent pour le filtrage, en se servant d'un des ticks lents de tdh-tick

data modify entity @s data set value {filtres:{nord:[],est:[],sud:[],ouest:[]}}
tag @s add Active

# On supprime le tag ayant servi à nous sélectionner
tag @s remove MachineTDHTemp
# S'il y a déjà une machine à cet emplacement, on la détruit
execute as @e[type=marker,tag=MachineElectriqueTDH,distance=..0.5] run function tdh:craft/electricite/detruire

# On crée une entité "Trieuse électrique" à la position d'exécution de la fonction,
# qui doit également être celle du dropper
# Le tag "Lent" signifie qu'on utilise le tick "lent" (1x/16s) au lieu du "rapide" (1x/.25s)
summon marker ~ ~ ~ {Tags:["MachineElectriqueTDH","Trieuse","MachineTDHTemp","Lent"]}

# On initialise correctement l'entité
execute as @e[type=marker,tag=MachineTDHTemp] at @s run function tdh:craft/electricite/trieuse/creer/initialiser

# On affiche un message de confirmation à l'éventuel joueur ayant le tag TestMachineTDH
tellraw @a[tag=TestMachineTDH] [{"text":"Cette ","color":"gray"},{"text":"trieuse","color":"gold","bold":"true"},{"text":" a été initialisée avec succès."}]
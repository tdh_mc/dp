# On insère le premier item dans le premier slot libre (enregistré dans @s temp)
data modify entity @s data.contenu append from storage tdh:conteneur resultat[0]
execute store result entity @s data.contenu[-1].Slot byte 1 run scoreboard players add @s temp 1

# On retire cet item de la liste des résultats
data remove storage tdh:conteneur resultat[0]

# S'il reste des items à insérer, on répète la fonction
scoreboard players remove #filtre temp4 1
execute if score #filtre temp4 matches 1.. run function tdh:craft/electricite/trieuse/tick/insertion/echec_loop
# On définit la valeur de #filtre temp à >0 pour enregistrer le fait qu'un filtre a matché
scoreboard players set #filtre temp 4

# On ajoute l'item à la liste des insertions à réaliser à l'ouest
data modify entity @s data.insertion.ouest append from entity @s data.contenu[0]
data remove entity @s data.insertion.ouest[-1].Slot
# On ne supprime pas l'item de data.contenu car ce sera fait dans la fonction filtrage/loop parente
# (puisque ce doit être fait même si on ne matche aucun filtre)

# On sépare du "contenu" principal tous les items devant être insérés dans une des 4 directions horizontales

# On initialise les 5 contenus secondaires
data modify entity @s data.insertion set value {nord:[],est:[],sud:[],ouest:[],bas:[]}

# On a déjà initialisé la variable @s max au nombre d'items au total dans la fonction progres
# On peut donc simplement filtrer le premier item de notre liste "contenu"
function tdh:craft/electricite/trieuse/tick/filtrage/loop
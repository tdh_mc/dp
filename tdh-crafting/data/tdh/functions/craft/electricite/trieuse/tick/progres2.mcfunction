# On filtre tous les items de notre conteneur
function tdh:craft/electricite/trieuse/tick/filtrage/init

# À la fin de cette opération, on devrait avoir 5 listes d'items à insérer (data.insertion.nord, sud, est, ouest, bas)

# On définit une variable temporaire pour enregistrer le slot actuel de ré-insertion dans notre conteneur
# (= tous les items n'ayant pas la place d'être stockés dans les conteneurs attenants)
scoreboard players set @s temp -1
# (C'est -1 au lieu de 0 car on incrémente en assignant et qu'on ne peut pas faire de post-incrément dans minecraft)

# On insère d'abord dans le coffre en-dessous de nous les données correspondant aux items NON FILTRÉS
execute if data entity @s data.insertion.bas[0] run function tdh:craft/electricite/trieuse/tick/insertion/bas
# On insère ensuite les listes d'items filtrées, qui seront ré-insérées dans data.contenu s'il n'y a pas la place
execute if data entity @s data.insertion.nord[0] run function tdh:craft/electricite/trieuse/tick/insertion/nord
execute if data entity @s data.insertion.est[0] run function tdh:craft/electricite/trieuse/tick/insertion/est
execute if data entity @s data.insertion.sud[0] run function tdh:craft/electricite/trieuse/tick/insertion/sud
execute if data entity @s data.insertion.ouest[0] run function tdh:craft/electricite/trieuse/tick/insertion/ouest

# Enfin, on ré-insère dans notre propre conteneur tout ce qui est encore dans data.contenu (ce qui n'a eu la place d'être stocké nulle part)
# Y compris s'il n'y a rien dans data.contenu d'ailleurs, car c'est ce qu'on espère : avoir réussi à tout rentrer dans les conteneurs alentour, et ne plus avoir d'item dans data.contenu
function tdh:craft/electricite/trieuse/tick/insertion/reste

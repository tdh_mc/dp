# On exécute cette fonction régulièrement pour trier les items situés dans notre conteneur
# Comme on est un dropper, notre maximum d'items est toujours de 9

# On copie l'ensemble de nos items dans notre storage, et on les compte
data remove entity @s data.contenu
data modify entity @s data.contenu set from block ~ ~ ~ Items
execute store result score @s max run data get entity @s data.contenu

# On calcule l'énergie consommée par le filtrage de ces items
# (1/16e de charbon par stack d'items filtré)
scoreboard players set @s status 20
scoreboard players operation @s status *= @s max
# On consomme cette énergie sur notre générateur
scoreboard players operation @e[type=marker,tag=GenerateurValide,limit=1] status -= @s status
scoreboard players set @s status 0

# S'il y a au moins 1 item dans cette liste, on exécute l'opération (récursive) de filtrage
execute if score @s max matches 1.. run function tdh:craft/electricite/trieuse/tick/progres2
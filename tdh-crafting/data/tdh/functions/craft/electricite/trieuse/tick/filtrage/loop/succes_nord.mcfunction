# On définit la valeur de #filtre temp à >0 pour enregistrer le fait qu'un filtre a matché
scoreboard players set #filtre temp 1

# On ajoute l'item à la liste des insertions à réaliser au nord
data modify entity @s data.insertion.nord append from entity @s data.contenu[0]
data remove entity @s data.insertion.nord[-1].Slot
# On ne supprime pas l'item de data.contenu car ce sera fait dans la fonction filtrage/loop parente
# (puisque ce doit être fait même si on ne matche aucun filtre)

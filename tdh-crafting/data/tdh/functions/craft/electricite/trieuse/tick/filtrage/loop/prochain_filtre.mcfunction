# On retire le premier filtre de la liste
data remove entity @s data.filtres.buffer[0]

scoreboard players remove #filtre max 1
execute if score #filtre max matches 1.. run function tdh:craft/electricite/trieuse/tick/filtrage/loop/test_filtre
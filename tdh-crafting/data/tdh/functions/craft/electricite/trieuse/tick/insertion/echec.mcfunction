# On exécute cette fonction après une opération d'insertion,
# pour remettre les items non insérés dans notre propre liste data.contenu,
# afin de les réinsérer ultérieurement dans notre conteneur

# On enregistre le nombre d'items non insérés
execute store result score #filtre temp4 run data get storage tdh:conteneur resultat

# S'il y en a au moins 1, on exécute la suite des opérations
execute if score #filtre temp4 matches 1.. run function tdh:craft/electricite/trieuse/tick/insertion/echec_loop
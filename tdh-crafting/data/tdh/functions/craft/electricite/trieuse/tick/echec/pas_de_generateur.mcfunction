# On se désactive temporairement
tag @s remove Active

# On affiche un message d'erreur aux joueurs alentour
tellraw @a[distance=..7.5] [{"text":"La trieuse est à court de combustible!","color":"red"}]
# On sépare du "contenu" principal tous les items devant être insérés dans une des 4 directions horizontales

# On passe successivement l'item à travers chaque filtre,
# et on définit #filtre temp à 1 dès qu'il en passe un
scoreboard players set #filtre temp 0
function tdh:craft/electricite/trieuse/tick/filtrage/loop/nord
execute if score #filtre temp matches 0 run function tdh:craft/electricite/trieuse/tick/filtrage/loop/est
execute if score #filtre temp matches 0 run function tdh:craft/electricite/trieuse/tick/filtrage/loop/sud
execute if score #filtre temp matches 0 run function tdh:craft/electricite/trieuse/tick/filtrage/loop/ouest
# Si la valeur temporaire vaut toujours 0, c'est que l'item n'a passé aucun filtre ;
# par conséquent on le place dans la liste "bas" avec les autres items non filtrés
execute if score #filtre temp matches 0 run function tdh:craft/electricite/trieuse/tick/filtrage/loop/succes_bas

# Dans tous les cas, on retire l'item 0 de la liste pour passer au suivant
data remove entity @s data.contenu[0]
scoreboard players remove @s max 1

# S'il reste des items à filtrer, on exécute à nouveau cette fonction
execute if score @s max matches 1.. run function tdh:craft/electricite/trieuse/tick/filtrage/loop
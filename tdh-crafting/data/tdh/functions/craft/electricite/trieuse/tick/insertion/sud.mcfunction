# On insère dans le bon conteneur tous les items à y insérer

data modify storage tdh:conteneur recherche.items set from entity @s data.insertion.sud
execute positioned ~ ~ ~1 run function tdh:conteneur/inserer_items

# Si la fonction inserer_items a renvoyé un nombre négatif (= elle n'a pas pu insérer l'intégralité des items), on replace chacun de ces items dans notre conteneur
execute if score #conteneur temp matches ..-1 run function tdh:craft/electricite/trieuse/tick/insertion/echec
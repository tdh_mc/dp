# On compare successivement cet item à tous les éléments du filtre ouest

# On stocke d'abord le nombre d'items stockés dans le filtre ouest
data modify entity @s data.filtres.buffer set from entity @s data.filtres.ouest
execute store result score #filtre max run data get entity @s data.filtres.buffer

# S'il y a au moins un item dans ce filtre, on tente de matcher l'item avec chacun d'entre eux récursivement
execute if score #filtre max matches 1.. run function tdh:craft/electricite/trieuse/tick/filtrage/loop/test_filtre

# Si après la fonction test_filtre, #filtre temp2 est égale à 0, c'est qu'on a réussi à passer un des filtres
# (et qu'on s'est arrêtés là car il ne sert à rien de tester les autres)
execute if score #filtre temp2 matches 0 run function tdh:craft/electricite/trieuse/tick/filtrage/loop/succes_ouest
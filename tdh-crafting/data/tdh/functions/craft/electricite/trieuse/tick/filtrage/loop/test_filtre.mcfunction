# On compare le premier item avec le premier filtre de notre liste de filtres
data modify entity @s data.buffer set from entity @s data.contenu[0]
execute store result score #filtre temp2 run data modify entity @s data.buffer merge from entity @s data.filtres.buffer[0]

# S'il ne matche pas, on passe au filtre suivant
execute if score #filtre temp2 matches 1.. run function tdh:craft/electricite/trieuse/tick/filtrage/loop/prochain_filtre
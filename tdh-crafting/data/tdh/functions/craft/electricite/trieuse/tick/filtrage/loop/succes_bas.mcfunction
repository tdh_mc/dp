# On ajoute l'item à la liste des insertions à réaliser en bas
data modify entity @s data.insertion.bas append from entity @s data.contenu[0]
data remove entity @s data.insertion.bas[-1].Slot
# On ne supprime pas l'item de data.contenu car ce sera fait dans la fonction filtrage/loop parente
# (puisque ce doit être fait même si on ne matche aucun filtre)

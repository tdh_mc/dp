# On enregistre le fait qu'on est actif
tag @s add Active
# On supprime les éventuels tags temporaires donnés précédemment
tag @s remove NoEnergie

title @p[tag=TestMachineTDH] actionbar [{"text":"Trieuse activée!","color":"green"}]
# On exécute régulièrement cette fonction pour faire trier les items à la trieuse
# Ce tick n'est exécuté que lorsqu'elle est active (tag Active)

# Si on a le tag "TrieuseAReset" mais qu'aucun joueur n'ayant le même tag n'est à proximité,
# on se le retire
execute as @s[tag=TrieuseAReset] unless entity @p[tag=TrieuseAReset,distance=..10] run function tdh:craft/electricite/trieuse/utiliser/effacer/annuler

# On vérifie si on peut trouver un générateur électrique proche qui nous fournisse de l'énergie
# On trouve le générateur le plus proche (hors générateurs vides d'énergie)
function tdh:craft/electricite/trouver_generateur
# Le résultat est donné sous la forme du tag "GenerateurValide" donné à un marqueur au maximum

# Si on n'a pas trouvé de générateur valide, on éteint la machine
execute unless entity @e[type=marker,tag=GenerateurValide] run function tdh:craft/electricite/trieuse/tick/echec/pas_de_generateur
# Si on n'a pas activé l'échec ci-dessus, on a toujours le tag Active, qu'on peut donc tester pour savoir si on exécute la suite des opérations
execute as @s[tag=Active] run function tdh:craft/electricite/trieuse/tick/progres
# On réactive automatiquement la machine pour retester le générateur dans 16 secondes
tag @s add Active

# On supprime le tag GenerateurValide
tag @e[type=marker,tag=GenerateurValide] remove GenerateurValide
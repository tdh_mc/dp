# On exécute cette fonction en tant qu'une entité Trieuse qui vient d'être utilisée par un joueur,
# pour vérifier comment elle doit réagir à l'interaction d'un joueur avec elle

# Structure des données de l'entité :
# data: {
#	filtres: {
#		nord: [ {id:"minecraft:stone"}, {id:"minecraft:cobblestone"}, {id:"minecraft:mossy_cobblestone} ],
#		sud: [ {id:"minecraft:andesite"} ]
#		est: [ {id:"minecraft:diorite"} ]
#		ouest: [ {id:"minecraft:diamond_sword",tag:{CustomModelData:400}} ]
#			(n'importe quels items sont autorisés, mais SANS les tags Count ni Slot)
#	},
#	item: {id:"minecraft:stone",tag:{ (éventuellement absent) }},
#		(item est seulement présent pendant l'ajout d'un item aux filtres)
#	contenu: [
#		{Slot:0b,id:"minecraft:stone",Count:64b},
#		{Slot:1b,id:"minecraft:mossy_cobblestone_stairs",Count:47b},
#		{Slot:2b,id:"minecraft:diamond_sword",Count:1b,tag:{Damage:0}}
#			(une liste de tous les items contenus dans notre conteneur ;
#			 seulement présente pendant l'exécution du filtrage)
#	],
#	insertion:{
#		nord:	[ (une liste avec la même syntaxe que ci-dessus) ],
#		est:	[ (idem) ],
#		sud:	[ (idem) ],
#		ouest:	[ (idem) ],
#		bas:	[ (idem) ]
#		(ces listes sont vides en-dehors de l'opération de filtrage)
#	}
# }

# On enregistre le fait qu'on a trouvé une machine (pour ne pas en activer X à la suite)
scoreboard players set #tdhCraft status 1

# Selon notre orientation par rapport à la machine, on exécute une sous-fonction différente
# (1 = Nord, 2 = Est, 3 = Sud, 4 = Ouest)
# (c'est inversé par rapport au joueur puisqu'on cherche à sélectionner la face de la machine lui faisant face)
scoreboard players set @s temp9 3
execute if entity @p[tag=TestMachineTDH,y_rotation=-135..-45] run scoreboard players set @s temp9 4
execute if entity @p[tag=TestMachineTDH,y_rotation=-45..45] run scoreboard players set @s temp9 1
execute if entity @p[tag=TestMachineTDH,y_rotation=45..135] run scoreboard players set @s temp9 2
# (5 = Haut, 6 = Bas)
execute if entity @p[tag=TestMachineTDH,x_rotation=45..90] run scoreboard players set @s temp9 5
execute if entity @p[tag=TestMachineTDH,x_rotation=-90..-45] run scoreboard players set @s temp9 6

execute if score @s temp9 matches 1 run function tdh:craft/electricite/trieuse/utiliser/nord
execute if score @s temp9 matches 2 run function tdh:craft/electricite/trieuse/utiliser/est
execute if score @s temp9 matches 3 run function tdh:craft/electricite/trieuse/utiliser/sud
execute if score @s temp9 matches 4 run function tdh:craft/electricite/trieuse/utiliser/ouest
execute if score @s temp9 matches 5 run function tdh:craft/electricite/trieuse/utiliser/haut
execute if score @s temp9 matches 6 run function tdh:craft/electricite/trieuse/utiliser/bas

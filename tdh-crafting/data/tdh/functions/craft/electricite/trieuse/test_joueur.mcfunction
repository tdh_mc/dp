# Comme on cherche une entité, on n'a pas besoin de tester tous les blocs alentour
# On peut juste prendre des positions discrètes le long de notre ligne de visée et vérifier si une entité s'y trouve

# On tag le joueur (pour pouvoir lui afficher un message ultérieurement)
tag @p[gamemode=!spectator,distance=0] add TestMachineTDH

# On n'exécute la suite des opérations que si on n'est PAS en train de sneak
execute at @p[tag=TestMachineTDH,predicate=!tdh:is_sneaking] run function tdh:craft/electricite/trieuse/test_joueur2

# On réinitialise les advancements
advancement revoke @a[tag=TestMachineTDH] only tdh:craft/electricite/trieuse/interaction
# On retire le tag temporaire du joueur
tag @a[tag=TestMachineTDH] remove TestMachineTDH
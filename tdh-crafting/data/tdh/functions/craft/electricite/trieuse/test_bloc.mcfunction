# Si le bloc dans lequel on se trouve n'est plus la machine nous correspondant,
# on se détruit

# On copie dans un buffer temporaire les données du bloc
data modify storage tdh:craft filtres.buffer set from block ~ ~ ~
execute store result score #tdhCraft temp7 run data modify storage tdh:craft filtres.buffer merge from storage tdh:craft filtres.electricite.trieuse
data remove storage tdh:craft filtres.buffer

# On se détruit si on n'est pas une trieuse
execute unless score #tdhCraft temp7 matches 0 run function tdh:craft/electricite/detruire
# On détruit la machine électrique qui appelle cette fonction

execute as @s[tag=GenerateurThermique] at @s run function tdh:craft/electricite/generateur/detruire
execute as @s[tag=Desenchanteur] at @s run function tdh:craft/electricite/desenchanteur/detruire
execute as @s[tag=Enchanteur] at @s run function tdh:craft/electricite/enchanteur/detruire
execute as @s[tag=Trieuse] at @s run function tdh:craft/electricite/trieuse/detruire


kill @s
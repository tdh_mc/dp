# S'il y a déjà une machine à cet emplacement, on la détruit
execute as @e[type=marker,tag=MachineElectriqueTDH,distance=..0.5] run function tdh:craft/electricite/detruire

# On crée une entité "Générateur électrique" à la position d'exécution de la fonction,
# qui doit également être celle de la blast furnace "Générateur électrique thermique"
summon marker ~ ~ ~ {Tags:["MachineElectriqueTDH","Generateur","GenerateurThermique","MachineTDHTemp"]}

# On initialise correctement l'entité
execute as @e[type=marker,tag=MachineTDHTemp] at @s run function tdh:craft/electricite/generateur/creer/initialiser

# On affiche un message de confirmation à l'éventuel joueur ayant le tag TestMachineTDH
tellraw @a[tag=TestMachineTDH] [{"text":"Ce ","color":"gray"},{"text":"générateur","color":"gold","bold":"true"},{"text":" a été initialisé avec succès."}]
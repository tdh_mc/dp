# On exécute cette fonction en tant qu'une MachineTDHElectrique de type GenerateurThermique,
# lorsqu'on vient d'être activés par un joueur qui tient un combustible en main gauche
# Le joueur en question a le tag TestMachineTDH

# On enregistre le fait qu'on a trouvé un générateur et qu'on ne peut pas en activer d'autre
scoreboard players set #tdhCraft status 1

# On copie l'objet tenu en main par le joueur dans le storage de notre machine
item replace block ~ ~ ~ container.0 from entity @p[tag=TestMachineTDH] weapon.mainhand

# Selon la condition passée par le joueur pour obtenir l'advancement, on vérifie ce qu'il tient en main :
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/generateur/remplissage_combustible={1=true}}] run function tdh:craft/electricite/generateur/remplissage/test_objet/1
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/generateur/remplissage_combustible={2=true}}] run function tdh:craft/electricite/generateur/remplissage/test_objet/2
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/generateur/remplissage_combustible={6=true}}] run function tdh:craft/electricite/generateur/remplissage/test_objet/6
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/generateur/remplissage_combustible={8=true}}] run function tdh:craft/electricite/generateur/remplissage/test_objet/8
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/generateur/remplissage_combustible={12=true}}] run function tdh:craft/electricite/generateur/remplissage/test_objet/12
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/generateur/remplissage_combustible={20=true}}] run function tdh:craft/electricite/generateur/remplissage/test_objet/20
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/generateur/remplissage_combustible={80=true}}] run function tdh:craft/electricite/generateur/remplissage/test_objet/80
execute if entity @p[tag=TestMachineTDH,advancements={tdh:craft/electricite/generateur/remplissage_combustible={100=true}}] run function tdh:craft/electricite/generateur/remplissage/test_objet/100
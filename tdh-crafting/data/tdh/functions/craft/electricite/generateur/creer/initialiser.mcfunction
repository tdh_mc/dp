# On a 0 combustible par défaut
scoreboard players set @s status 0

# Un générateur est toujours actif (pour pouvoir afficher ses particules)
tag @s add Active

# On supprime le tag ayant servi à nous sélectionner
tag @s remove MachineTDHTemp
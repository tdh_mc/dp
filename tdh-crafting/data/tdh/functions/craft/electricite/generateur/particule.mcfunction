# On affiche une particule d'une couleur différente selon le combustible restant

# Rouge sombre (<1mn·machine)
execute if score @s status matches ..239 run particle dust 0.5 0.0 0.0 1 ~ ~0.6 ~ 0.05 0.05 0.05 1 1 force @a[distance=..15]
# Rouge (1-2mn·machine)
execute if score @s status matches 240..479 run particle dust 1.0 0.0 0.0 1 ~ ~0.6 ~ 0.05 0.05 0.05 1 1 force @a[distance=..15]
# Orange (2-3mn·machine)
execute if score @s status matches 480..719 run particle dust 1.0 0.5 0.0 1 ~ ~0.6 ~ 0.05 0.05 0.05 1 1 force @a[distance=..15]
# Jaune (3-4mn·machine)
execute if score @s status matches 720..959 run particle dust 1.0 1.0 0.0 1 ~ ~0.6 ~ 0.05 0.05 0.05 1 1 force @a[distance=..15]
# Vert clair (4-5mn·machine)
execute if score @s status matches 960..1199 run particle dust 0.5 1.0 0.0 1 ~ ~0.6 ~ 0.05 0.05 0.05 1 1 force @a[distance=..15]
# Vert (5mn·machine+)
execute if score @s status matches 1200.. run particle dust 0.0 1.0 0.0 1 ~ ~0.6 ~ 0.05 0.05 0.05 1 1 force @a[distance=..15]
# On exécute régulièrement cette fonction pour faire évoluer le générateur

# On ne fait pas évoluer le combustible ici, il est ajouté par *test_joueur* lorsqu'un joueur clique droit sur le four avec du combustible, et automatiquement retiré par les machines qui en ont besoin

# S'il y a au moins 1 de combustible, on affiche une particule au-dessus de la machine
execute if score @s status matches 1.. run function tdh:craft/electricite/generateur/particule
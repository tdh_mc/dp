# On a déjà défini :
# - #tdhCraft temp = le nombre d'items du tag combustible présents sur le joueur avant l'opération
# - #tdhCraft temp2 = le nombre d'items combustibles ajoutés par le joueur

# On modifie la valeur du combustible stocké dans la machine
scoreboard players set #tdhCraft temp4 4000

# On exécute la suite (générique) des opérations
function tdh:craft/electricite/generateur/remplissage/succes
# On vient d'échouer à l'opération de remplissage
# (on tenait un objet en main qui n'était pas un combustible valide)

# On restaure l'item dans la main du joueur
item replace entity @p[tag=TestMachineTDH] weapon.mainhand from block ~ ~ ~ container.0
# On vide le slot 0 du four
item replace block ~ ~ ~ container.0 with air

# On affiche un message d'erreur
title @a[tag=TestMachineTDH] actionbar [{"text":"Combustible non valide!","color":"red"}]
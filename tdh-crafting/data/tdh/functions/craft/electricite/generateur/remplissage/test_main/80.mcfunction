# On a déjà défini :
# - #tdhCraft temp = le nombre d'items du tag combustible présents sur le joueur avant l'opération
# - #tdhCraft temp2 = le nombre d'items en mainhand du joueur (qu'on s'apprête à retirer)
# - #tdhCraft temp9 = 0, la variable de contrôle

# On retire l'item en mainhand du joueur (pas d'inquiétude, on en a une copie dans l'output du four)
item replace entity @p[tag=TestMachineTDH] weapon.mainhand with air

# On compte à nouveau le nombre d'items possédés par le joueur qui matchent le tag combustible
execute store result score #tdhCraft temp3 run clear @p[tag=TestMachineTDH] #tdh:craft/combustible/80 0
# On ajoute le nombre d'items qu'on vient de retirer (temp2)
scoreboard players operation #tdhCraft temp3 += #tdhCraft temp2

# Si le total n'est pas égal à temp, c'est qu'on avait changé d'item, et on annule l'opération
execute unless score #tdhCraft temp3 = #tdhCraft temp run function tdh:craft/electricite/generateur/remplissage/echec
# Sinon, on prend en compte l'ajout du combustible
execute if score #tdhCraft temp3 = #tdhCraft temp run function tdh:craft/electricite/generateur/remplissage/succes/80
# On a déjà défini :
# - #tdhCraft temp = le nombre d'items du tag combustible présents sur le joueur avant l'opération
# - #tdhCraft temp2 = le nombre d'items combustibles ajoutés par le joueur
# - #tdhCraft temp4 = la valeur en unités d'énergie de chaque item combustible

# On calcule la valeur totale du combustible, et on l'ajoute à notre statut
scoreboard players operation #tdhCraft temp2 *= #tdhCraft temp4
scoreboard players operation @s status += #tdhCraft temp2

# On vide le slot 0 du four
item replace block ~ ~ ~ container.0 with air

# On affiche un message de succès
title @a[tag=TestMachineTDH] actionbar [{"text":"Remplissage du générateur avec ","color":"gray"},{"score":{"name":"#tdhCraft","objective":"temp2"},"color":"gold","extra":[{"text":" unités d'énergie"}]},{"text":" (total : "},{"score":{"name":"@s","objective":"status"},"color":"yellow"},{"text":")."}]
# On exécute cette fonction régulièrement pour ticker les machines électriques inactives

# On a une fonction différente selon le type de machine
# Certaines machines ne tickent pas lorsqu'elles sont inactives (c'est le cas du générateur thermique)
#execute as @s[tag=GenerateurThermique] run function tdh:craft/electricite/generateur/tick_inactive
execute as @s[tag=Desenchanteur] run function tdh:craft/electricite/desenchanteur/tick_inactive
execute as @s[tag=Enchanteur] run function tdh:craft/electricite/enchanteur/tick_inactive
#execute as @s[tag=Trieuse] run function tdh:craft/electricite/trieuse/tick_inactive
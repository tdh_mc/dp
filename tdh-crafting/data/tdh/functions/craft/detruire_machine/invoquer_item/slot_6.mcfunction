# On appelle cette fonction pour invoquer ingame l'item contenu dans le slot 6 d'une machine qu'on s'apprête à détruire

summon item ~ ~ ~ {Tags:["ItemRestaureTemp"],Item:{id:"stick",Count:1b}}
data modify entity @e[type=item,tag=ItemRestaureTemp,limit=1] Item set from entity @s data.contenu.slots[6]
tag @e[type=item,tag=ItemRestaureTemp] remove ItemRestaureTemp
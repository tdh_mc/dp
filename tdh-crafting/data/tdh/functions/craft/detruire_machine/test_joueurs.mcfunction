# On vérifie les patterns de machines autour des joueurs ayant détruit certains types de blocs :

# - Tables de craft
execute at @a[gamemode=!spectator,scores={ateliersDetruits=1..}] run function tdh:craft/detruire_machine/test_joueur
# - Distributeurs
execute at @a[gamemode=!spectator,scores={distributeursDetruits=1..}] run function tdh:craft/detruire_machine/test_joueur
# - Fours
execute at @a[gamemode=!spectator,scores={foursDetruits=1..}] run function tdh:craft/detruire_machine/test_joueur
# - Fours
execute at @a[gamemode=!spectator,scores={trappesFerDetruites=1..}] run function tdh:craft/detruire_machine/test_joueur


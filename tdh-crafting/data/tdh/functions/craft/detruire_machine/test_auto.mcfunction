# On vérifie périodiquement toutes les machines pour vérifier qu'elles matchent toujours ;
# Dans le cas contraire, elles sont détruites

# On ne teste pas les machines actives car elles re-testent de toute façon systématiquement au moment du craft
execute as @e[type=marker,tag=MachineTDH,tag=!Active] at @s run function tdh:craft/machine/test_blocs

execute as @e[type=marker,tag=MachineElectriqueTDH] at @s run function tdh:craft/electricite/test_blocs
# Cette fonction est appelée à la position d'un joueur qui vient de détruire une table de craft,
# pour vérifier s'il vient de détruire une machine en comprenant une

# On vérifie toutes les entités MachineTDH dans un rayon de 10 alentour
execute as @e[type=marker,tag=MachineTDH,distance=..10] at @s run function tdh:craft/machine/test_blocs

# On réinitialise tous les scores des joueurs à cette position
scoreboard players set @a[distance=0] ateliersDetruits 0
scoreboard players set @a[distance=0] distributeursDetruits 0
scoreboard players set @a[distance=0] foursDetruits 0
scoreboard players set @a[distance=0] trappesFerDetruites 0
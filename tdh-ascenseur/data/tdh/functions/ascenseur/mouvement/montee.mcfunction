# On exécute cette fonction en tant qu'un ascenseur à notre position, pour lancer la montée
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"text":"-- Début de la montée --","color":"gray"}]

tag @s[tag=!Descente] add Montee
tag @s add Mouvement

# On referme les portes à l'étage où on se trouve
function tdh:ascenseur/mouvement/depart/test_etage
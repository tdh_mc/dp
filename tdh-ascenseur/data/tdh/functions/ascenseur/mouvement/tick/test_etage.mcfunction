# On exécute cette fonction en tant qu'un ascenseur à notre position, pour vérifier si on a atteint un étage

# On tag l'étage le plus proche au même niveau que nous s'il en existe un
execute positioned ~-6 ~ ~-6 as @e[type=item_frame,tag=EtageAscenseur,dx=12,dy=0,dz=12] run tag @s add NotreEtageTemp
tag @e[type=item_frame,tag=NotreEtageTemp,sort=nearest,limit=1] add NotreEtage
tag @e[type=item_frame,tag=NotreEtageTemp] remove NotreEtageTemp

# Si c'est le cas, on enregistre le fait qu'on est arrivés à un étage, sauf si notre destination n'est pas cet étage
execute unless score @s destination matches -999.. if entity @e[type=item_frame,tag=NotreEtage,distance=..20,limit=1] run function tdh:ascenseur/mouvement/tick/etage
execute if score @s[tag=!RetourEnCours] destination = @e[type=item_frame,tag=NotreEtage,distance=..20,limit=1] etage run function tdh:ascenseur/mouvement/tick/etage
execute if score @s[tag=RetourEnCours,tag=!Appel] destination = @e[type=item_frame,tag=NotreEtage,distance=..20,limit=1] etage run function tdh:ascenseur/mouvement/tick/etage_retour
execute if score @s[tag=RetourEnCours,tag=Appel] destination = @e[type=item_frame,tag=NotreEtage,distance=..20,limit=1] etage run function tdh:ascenseur/mouvement/tick/etage

# On supprime le tag temporaire
tag @e[type=item_frame,tag=NotreEtage] remove NotreEtage
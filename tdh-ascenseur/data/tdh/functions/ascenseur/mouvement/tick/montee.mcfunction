# On exécute cette fonction en tant qu'un ascenseur à notre position, pour se déplacer d'un bloc vers le haut

# On déplace d'abord les personnes et les entités
tp @s ~ ~1 ~
execute positioned ~-1.9 ~0 ~-1.9 as @e[dx=2.8,dy=3,dz=2.8,type=#tdh:ascenseur_deplacable] at @s run tp @s ~ ~1 ~ ~ ~
execute positioned ~-1.9 ~0 ~-1.9 at @a[dx=2.8,dy=3,dz=2.8,gamemode=!spectator] if block ~ ~-0.49 ~ #tdh:ascenseur run tp @p[gamemode=!spectator] ~ ~1.01 ~ ~ ~

# On joue le bruitage
execute positioned ~ ~4.5 ~ run function tdh:sound/ascenseur/montee

# On déplace ensuite les blocs
clone ~-1.4 ~-1 ~-1.4 ~1.4 ~3 ~1.4 ~-1.4 ~0 ~-1.4 filtered #tdh:ascenseur move

# On vérifie si on est arrivés à un étage
execute at @s run function tdh:ascenseur/mouvement/tick/test_etage

# Si la voie au-dessus de nous n'est pas libre, on s'arrête
execute at @s[tag=Mouvement] unless block ~ ~4 ~ air run function tdh:ascenseur/mouvement/arret/chemin_bloque
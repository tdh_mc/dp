# On récupère l'appel enregistré dans nos données pour connaître notre nouvelle direction
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"On applique notre appel.","color":"green"}]
# On nettoie nos tags actuels
tag @s remove Montee
tag @s remove Descente

# Si on avait un appel pour monter, on se met en montée, vice versa pour la descente
execute as @s[tag=AppelMontee] run tag @s add Montee
execute as @s[tag=AppelDescente] run tag @s add Descente

# On supprime les tags d'appel
tag @s remove AppelDescente
tag @s remove AppelMontee
tag @s remove Appel
scoreboard players reset @s destination
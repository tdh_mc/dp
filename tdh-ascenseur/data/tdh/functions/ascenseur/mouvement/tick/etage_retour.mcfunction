# On exécute cette fonction en tant qu'un ascenseur à notre position, pour enregistrer qu'on a atteint un étage
# Il y a déjà une item_frame "NotreEtage" à laquelle on est arrivés, et il n'y a pas besoin de la nettoyer ici (c'est fait dans test_etage)
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Étage atteint: ","color":"green"},{"selector":"@e[type=item_frame,tag=NotreEtage,limit=1]"}]

# S'il n'y a pas de numéro d'étage, on s'arrête par précaution
execute unless score @e[type=item_frame,tag=NotreEtage,limit=1] etage matches -999999.. run function tdh:ascenseur/mouvement/arret/aucun_numero_etage
# Sinon, on enregistre le numéro d'étage
execute if score @e[type=item_frame,tag=NotreEtage,limit=1] etage matches -999999.. run scoreboard players operation @s etage = @e[type=item_frame,tag=NotreEtage,limit=1] etage

# On s’arrête
function tdh:ascenseur/mouvement/arret/terminus
scoreboard players reset @s destination

# On ouvre PAS les portes
tag @s remove PortesOuvertes

# On diffuse un son
function tdh:sound/ascenseur/arrivee
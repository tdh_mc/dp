# On exécute cette fonction en tant qu'un ascenseur à notre position, pour enregistrer qu'on a atteint un étage
# Il y a déjà une item_frame "NotreEtage" à laquelle on est arrivés, et il n'y a pas besoin de la nettoyer ici (c'est fait dans test_etage)
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Étage atteint: ","color":"green"},{"selector":"@e[type=item_frame,tag=NotreEtage,limit=1]"}]

# S'il n'y a pas de numéro d'étage, on s'arrête par précaution
execute unless score @e[type=item_frame,tag=NotreEtage,limit=1] etage matches -999999.. run function tdh:ascenseur/mouvement/arret/aucun_numero_etage
# Sinon, on enregistre le numéro d'étage
execute if score @e[type=item_frame,tag=NotreEtage,limit=1] etage matches -999999.. run scoreboard players operation @s etage = @e[type=item_frame,tag=NotreEtage,limit=1] etage

# On se met en pause
function tdh:ascenseur/mouvement/arret/pause
scoreboard players reset @s destination

# On ouvre les portes
execute at @e[type=item_frame,tag=NotreEtage,limit=1] facing entity @s feet run fill ^-2 ^0 ^1 ^2 ^3 ^1 air replace iron_bars

# On diffuse un son
function tdh:sound/ascenseur/arrivee

# On affiche un message
execute as @e[type=item_frame,tag=NotreEtage,limit=1] run title @a[distance=..2.1] actionbar [{"text":"Étage ","color":"gray"},{"score":{"name":"@s","objective":"etage"},"color":"gold"},{"text":" : "},{"selector":"@s","color":"gold"}]
execute as @s[tag=Appel] at @e[type=item_frame,tag=NotreEtage,limit=1] run tellraw @a[distance=..6] [{"text":"Votre ascenseur est ","color":"gray"},{"text":"arrivé","color":"gold"},{"text":"."}]
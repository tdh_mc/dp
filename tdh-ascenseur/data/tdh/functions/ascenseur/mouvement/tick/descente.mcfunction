# On exécute cette fonction en tant qu'un ascenseur à notre position, pour se déplacer d'un bloc vers le haut

# On déplace d'abord les blocs
clone ~-1.4 ~-1 ~-1.4 ~1.4 ~3 ~1.4 ~-1.4 ~-2 ~-1.4 filtered #tdh:ascenseur move

# On rallonge les chaînes qui nous soutiennent
fill ~-0.9 ~3 ~-0.9 ~-0.9 ~3 ~-0.9 chain[axis=y] keep
fill ~0.9 ~3 ~-0.9 ~0.9 ~3 ~-0.9 chain[axis=y] keep
fill ~0.9 ~3 ~0.9 ~0.9 ~3 ~0.9 chain[axis=y] keep
fill ~-0.9 ~3 ~0.9 ~-0.9 ~3 ~0.9 chain[axis=y] keep

# On joue le bruitage
execute positioned ~ ~2.5 ~ run function tdh:sound/ascenseur/descente

# On déplace ensuite les personnes et les entités
tp @s ~ ~-1 ~
execute positioned ~-1.9 ~0 ~-1.9 as @e[dx=2.8,dy=3,dz=2.8,type=#tdh:ascenseur_deplacable] at @s run tp @s ~ ~-1 ~ ~ ~
execute positioned ~-1.9 ~0 ~-1.9 at @a[dx=2.8,dy=3,dz=2.8,gamemode=!spectator] if block ~ ~-0.99 ~ air run tp @p[gamemode=!spectator] ~ ~-0.99 ~ ~ ~

# On vérifie si on est arrivés à un étage
execute at @s run function tdh:ascenseur/mouvement/tick/test_etage

# Si la voie en-dessous de nous n'est pas libre, on s'arrête
execute at @s unless block ~ ~-2 ~ air run function tdh:ascenseur/mouvement/arret/chemin_bloque
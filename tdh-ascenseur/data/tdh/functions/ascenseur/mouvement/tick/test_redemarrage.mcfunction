# On décrémente notre tick de 1
scoreboard players remove @s remainingTicks 1

# Si notre tick est à 0, on redémarre
execute if score @s remainingTicks matches ..0 run function tdh:ascenseur/mouvement/tick/redemarrage
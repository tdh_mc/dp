# On continue notre mouvement (si on va s'arrêter, on ne redémarrera pas et peut donc retirer Pause tranquillement)
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Redémarrage de l'ascenseur.","color":"green"}]
tag @s remove Pause

# Si on a atteint notre destination, on démarre dans la direction désirée
execute as @s[tag=Appel] run function tdh:ascenseur/mouvement/tick/appliquer_appel

# On vérifie s'il reste des joueurs dans l'ascenseur ; dans le cas contraire, on s'arrête
execute as @s[tag=!Mouvement] run tag @s add PortesOuvertes
execute as @s[tag=Mouvement] positioned ~-1.9 ~0 ~-1.9 unless entity @p[dx=2.8,dy=3,dz=2.8,gamemode=!spectator] run function tdh:ascenseur/mouvement/arret/plus_personne

# Si on a atteint le maximum ou minimum, mais qu'il reste des joueurs, on se remet en pause
execute if score @s[tag=Montee] etage >= @s max run function tdh:ascenseur/mouvement/arret/terminus
execute if score @s[tag=Descente] etage <= @s min run function tdh:ascenseur/mouvement/arret/terminus

# Si on se met en mouvement, on referme les portes
execute as @s[tag=Mouvement] run function tdh:ascenseur/mouvement/depart/test_etage
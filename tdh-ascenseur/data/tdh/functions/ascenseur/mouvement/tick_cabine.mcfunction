# On vérifie si un ascenseur à l'arrêt peut refermer ses portes
execute at @s[tag=PortesOuvertes] run function tdh:ascenseur/mouvement/tick/test_fermeture_portes

# On vérifie si un ascenseur en pause peut redémarrer
execute at @s[tag=Pause] run function tdh:ascenseur/mouvement/tick/test_redemarrage

# On fait monter tous les ascenseurs qui montent
execute at @s[tag=Montee,tag=!Pause] run function tdh:ascenseur/mouvement/tick/montee
# On fait descendre tous les ascenseurs qui descendent
execute at @s[tag=Descente,tag=!Pause] run function tdh:ascenseur/mouvement/tick/descente
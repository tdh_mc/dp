# On exécute cette fonction en tant qu'un ascenseur à notre position, pour vérifier si on a atteint un étage
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"text":"-- Test départ étage --","color":"gray"}]

# On tag l'étage le plus proche au même niveau que nous s'il en existe un
execute positioned ~-6 ~ ~-6 as @e[type=item_frame,tag=EtageAscenseur,dx=12,dy=0,dz=12] run tag @s add NotreEtageTemp
tag @e[type=item_frame,tag=NotreEtageTemp,sort=nearest,limit=1] add NotreEtage
tag @e[type=item_frame,tag=NotreEtageTemp] remove NotreEtageTemp
# Si c'est le cas, on referme les portes
execute at @e[type=item_frame,tag=NotreEtage,distance=..20] run function tdh:ascenseur/mouvement/depart/etage

# On supprime le tag temporaire
tag @e[type=item_frame,tag=NotreEtage] remove NotreEtage
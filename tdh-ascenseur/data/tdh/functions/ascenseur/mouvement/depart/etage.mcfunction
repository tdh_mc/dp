# On supprime le tag Pause si on le possède
tag @s remove Pause
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Départ de l'étage.","color":"green"}]

# On ferme les portes
execute facing entity @s feet run fill ^-2 ^0 ^1 ^2 ^3 ^1 iron_bars keep

# On diffuse le son de fermeture des portes
execute facing entity @s feet positioned ^ ^ ^2 run function tdh:sound/ascenseur/depart

# On pousse les joueurs se trouvant sur la porte
execute facing entity @s feet positioned as @p[gamemode=!spectator,distance=..2] if block ~ ~1 ~ iron_bars run tp @p[gamemode=!spectator,distance=0] ^ ^ ^-1
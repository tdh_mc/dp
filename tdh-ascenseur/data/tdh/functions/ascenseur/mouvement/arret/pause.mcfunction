# 20x6t = 12s avant de redémarrer
scoreboard players set @s remainingTicks 20

# On enregistre le fait qu'on est en pause
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Mise en pause de l'ascenseur.","color":"gold"}]
tag @s add Pause
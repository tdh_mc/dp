# Il n'y a plus personne dans l'ascenseur et on arrête donc notre mouvement
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"On referme les portes, car il n'y a plus personne.","color":"gold"}]
tag @s remove Mouvement
tag @s remove Montee
tag @s remove Descente
tag @s remove RetourEnCours
tag @s remove PortesOuvertes

# On referme les portes
function tdh:ascenseur/mouvement/depart/test_etage

# Si on a le tag qui spécifie qu'on retourne automatiquement à un étage, on le fait
execute unless score @s[tag=RetourAuto] etage = @s etageOrigine run function tdh:ascenseur/appel/retour_auto
# On est arrivés au "terminus" de l'ascenseur et on arrête donc notre mouvement
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Terminus de l'ascenseur.","color":"gold"}]
tag @s remove Mouvement
tag @s remove Montee
tag @s remove Descente
tag @s remove RetourEnCours
tag @s add PortesOuvertes
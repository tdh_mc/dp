# La route est bloquée et on arrête donc notre mouvement
tag @s remove Mouvement
tag @s remove Montee
tag @s remove Descente
tag @s remove RetourEnCours

# On affiche un message
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Arrêt d'urgence: ","bold":"true","color":"red"},{"text":"Chemin bloqué.","color":"red"}]
tellraw @a[distance=..2.1] [{"text":"Arrêt de l'ascenseur : ","color":"gray"},{"text":"Chemin bloqué.","color":"red"}]

# On joue le son correspondant
function tdh:sound/ascenseur/chemin_bloque
# On ajoute le tag NotreAppel à l'ascenseur le plus proche correspondant à notre requête s'il en existe un
function tdh:ascenseur/appel/tag_ascenseur

# Si on a trouvé un ascenseur, on l'appelle
# Ne PAS ajouter "at @s" ici car on veut rester à la position du CB
execute if entity @e[type=armor_stand,tag=NotreAppel] as @e[type=item_frame,tag=EtageAscenseur,distance=..15,sort=nearest,limit=1] run function tdh:ascenseur/appel/descente

# Si on n'a pas trouvé d'ascenseur, on affiche un message d'erreur
execute unless entity @e[type=armor_stand,tag=NotreAppel] run function tdh:ascenseur/appel/erreur/aucun_ascenseur

# On supprime le tag temporaire
tag @e[type=armor_stand,tag=NotreAppel] remove NotreAppel
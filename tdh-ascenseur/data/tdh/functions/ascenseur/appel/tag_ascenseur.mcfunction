# On tag les ascenseurs pouvant correspondre à cette requête (+-8 blocs dans toutes les directions)
execute positioned ~-8 0 ~-8 run tag @e[type=armor_stand,dx=16,dy=256,dz=16,tag=Ascenseur] add NotreAppelTemp

# On ne peut sélectionner que des ascenseurs sans le tag Mouvement, SAUF s'il vient déjà à notre étage
# (auquel cas on affichera un message d'erreur pour ne pas réserver plusieurs ascenseurs)
execute as @e[type=armor_stand,tag=Ascenseur,tag=NotreAppelTemp,tag=Mouvement] unless score @s destination = @e[type=item_frame,tag=EtageAscenseur,distance=..15,sort=nearest,limit=1] etage run tag @s remove NotreAppelTemp

# On choisit ensuite le plus proche pour lui envoyer la requête
tag @e[type=armor_stand,tag=Ascenseur,tag=NotreAppelTemp,sort=nearest,limit=1] add NotreAppel
tag @e[type=armor_stand,tag=NotreAppelTemp] remove NotreAppelTemp
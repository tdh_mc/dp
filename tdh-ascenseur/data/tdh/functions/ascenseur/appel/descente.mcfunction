# On exécute cette fonction en tant qu'un étage à la position du command block exécutant l'appel,
# en ayant défini une armor_stand Ascenseur "NotreAppel" qui correspond à l'ascenseur qu'on veut appeler
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"text":"","color":"gray"},{"text":"--- Appel descente↓ ---","bold":"true"},{"text":"\nÉtage à desservir: "},{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gold"}]

# Si l'ascenseur a déjà le tag "Appel", on affiche un message d'erreur
execute if entity @e[type=armor_stand,tag=NotreAppel,tag=Appel] run function tdh:ascenseur/appel/erreur/deja_reserve

# Si l'ascenseur n'a pas encore le tag "Appel", on réserve
execute at @s as @e[type=armor_stand,tag=NotreAppel,tag=!Appel] run function tdh:ascenseur/appel/succes/descente

# On diffuse le son d'appel
function tdh:sound/ascenseur/appel
# On exécute cette fonction en tant que l'ascenseur à appeler, à la position de l'étage, si l'ascenseur est déjà là
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Ascenseur déjà à cet étage.","color":"green"}]

# On enregistre le fait qu'on est en mouvement
tag @s add Mouvement

# On joue le son d'arrivée de l'ascenseur
execute facing entity @s feet at @s positioned ^ ^ ^-2 run function tdh:sound/ascenseur/arrivee

# On se met en pause et on ouvre les portes
execute at @s run function tdh:ascenseur/mouvement/tick/test_etage
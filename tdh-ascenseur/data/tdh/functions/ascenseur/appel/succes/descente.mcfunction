# On exécute cette fonction en tant que l'ascenseur à appeler, à la position de l'étage
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Appel pour descendre enregistré.","color":"green"}]

# On enregistre le fait qu'on a été appelés
tag @s add Appel
tag @s add AppelDescente

# On enregistre le numéro de l'étage à atteindre
scoreboard players operation @s destination = @e[type=item_frame,tag=EtageAscenseur,distance=..1,sort=nearest,limit=1] etage

# Si c'est plus bas que notre étage actuel, on monte ; sinon, on descend
execute if score @s destination < @s etage at @s run function tdh:ascenseur/mouvement/descente
execute if score @s destination > @s etage at @s run function tdh:ascenseur/mouvement/montee
# Dans les deux cas, on affiche un message d'information à l'étage ayant appelé
execute unless score @s destination = @s etage run tellraw @a[distance=..6] [{"text":"Votre ascenseur est ","color":"gray"},{"text":"en route","color":"gold"},{"text":"."}]

# Si on est déjà au bon étage, on ouvre les portes
execute if score @s destination = @s etage run function tdh:ascenseur/appel/succes/deja_la
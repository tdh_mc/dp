# On exécute cette fonction en tant que l'ascenseur à appeler, à la position de l'étage
execute positioned ~-20 0 ~-20 run tellraw @a[dx=40,dz=40,dy=256,tag=AscenseurDebugLog] [{"nbt":"CustomName","entity":"@s","interpret":"true","color":"gray"},{"text":" : "},{"text":"Retour automatique à l'étage ","color":"green"},{"score":{"name":"@s","objective":"etageOrigine"},"color":"green","bold":"true"}]

# On enregistre le fait qu'on fait un retour auto
tag @s add RetourEnCours

# On enregistre le numéro de l'étage à atteindre (notre étage par défaut)
scoreboard players operation @s destination = @s etageOrigine

# Si c'est plus bas que notre étage actuel, on monte ; sinon, on descend
execute if score @s destination < @s etage at @s run function tdh:ascenseur/mouvement/descente
execute if score @s destination > @s etage at @s run function tdh:ascenseur/mouvement/montee
# On initialise avec un temps calme
weather clear
scoreboard players set #meteo on 1
scoreboard players operation #meteo actuelle = #meteo_calme actuelle

# On génère 2 semaines de prévisions (l'actuelle + la prochaine)
function tdh:meteo/auto/prochaine_semaine
function tdh:meteo/auto/prochaine_semaine
# puis on lance une fois prochaine_journee pour calculer toutes les probas manquantes
function tdh:meteo/auto/prochaine_journee

# Enfin, on lance le tick
function tdh:meteo/auto/tick
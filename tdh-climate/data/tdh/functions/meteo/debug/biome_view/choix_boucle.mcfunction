# On choisit la bonne boucle à exécuter pour afficher les informations requises
# (on a plusieurs boucles pour éviter de refaire le test du tag du joueur à chaque itération)

execute if entity @p[tag=BiomeViewPlayerTemp,tag=BiomeViewEau] run function tdh:meteo/debug/biome_view/boucle/eau
execute if entity @p[tag=BiomeViewPlayerTemp,tag=BiomeViewCaverne] run function tdh:meteo/debug/biome_view/boucle/caverne
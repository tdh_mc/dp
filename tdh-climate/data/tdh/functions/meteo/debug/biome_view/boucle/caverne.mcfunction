# On vérifie si notre position actuelle contient un biome de caverne
execute store result score #biomeView temp if predicate tdh:biome/caverne

# On affiche une particule :
# - Rouge = Pas caverne
execute if score #biomeView temp matches 0 if score #biomeView temp2 matches 0 run particle dust 1 0.2 0.2 10 ~ ~ ~ 0 0 0 0 3 force @a[tag=BiomeViewPlayerTemp]
# - Vert = Caverne
execute if score #biomeView temp matches 1 run particle dust 0.2 1 0.2 10 ~ ~ ~ 0 0 0 0 3 force @a[tag=BiomeViewPlayerTemp]

# On passe à la position suivante
function tdh:meteo/debug/biome_view/boucle/x
execute if score #biomeView posY < #biomeView yMax at @s run function tdh:meteo/debug/biome_view/boucle/caverne
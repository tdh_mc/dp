# On incrémente nos Z
scoreboard players operation #biomeView posZ = #biomeView zMin
scoreboard players add #biomeView posY 1

# Si on n'a pas dépassé le maximum en Z, on se déplace sur cet axe
execute if score #biomeView posY < #biomeView yMax run tp @s ~-128 ~16 ~-128
# Sinon, on termine l'exécution
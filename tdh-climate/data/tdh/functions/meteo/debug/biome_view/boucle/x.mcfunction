# On incrémente nos X
scoreboard players add #biomeView posX 1

# Si on n'a pas dépassé le maximum en X, on se déplace sur cet axe
execute if score #biomeView posX < #biomeView xMax run tp @s ~16 ~ ~
# Sinon, on revient à 0 sur cet axe et on se déplace sur l'axe des Z
execute unless score #biomeView posX < #biomeView xMax run function tdh:meteo/debug/biome_view/boucle/z
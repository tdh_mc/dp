# On incrémente nos Z
scoreboard players operation #biomeView posX = #biomeView xMin
scoreboard players add #biomeView posZ 1

# Si on n'a pas dépassé le maximum en Z, on se déplace sur cet axe
execute if score #biomeView posZ < #biomeView zMax run tp @s ~-128 ~ ~16
# Sinon, on revient à 0 sur cet axe et on se déplace sur l'axe des Y
execute unless score #biomeView posZ < #biomeView zMax run function tdh:meteo/debug/biome_view/boucle/y
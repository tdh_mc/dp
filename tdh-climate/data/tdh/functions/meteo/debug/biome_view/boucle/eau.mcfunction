# On vérifie si notre position actuelle contient un biome d'eau
# On sépare ça en océan / rivière pour afficher une couleur différente de particule
execute store result score #biomeView temp if predicate tdh:biome/ocean
execute if score #biomeView temp matches 0 store result score #biomeView temp2 if predicate tdh:biome/riviere

# On affiche une particule :
# - Rouge = Ni océan, ni rivière
execute if score #biomeView temp matches 0 if score #biomeView temp2 matches 0 run particle dust 1 0.2 0 10 ~ ~ ~ 0 0 0 0 3 force @a[tag=BiomeViewPlayerTemp]
# - Violet = Rivière
execute if score #biomeView temp matches 0 if score #biomeView temp2 matches 1 run particle dust 1 0.2 1 10 ~ ~ ~ 0 0 0 0 3 force @a[tag=BiomeViewPlayerTemp]
# - Cyan = Océan
execute if score #biomeView temp matches 1 run particle dust 0.2 1 1 10 ~ ~ ~ 0 0 0 0 3 force @a[tag=BiomeViewPlayerTemp]

# On passe à la position suivante
function tdh:meteo/debug/biome_view/boucle/x
execute if score #biomeView posY < #biomeView yMax at @s run function tdh:meteo/debug/biome_view/boucle/eau
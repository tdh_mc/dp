# On donne un tag temporaire au joueur sélectionné
tag @p[tag=BiomeView] add BiomeViewPlayerTemp

# Définition de variables
# Le nombre d'itérations en X, Y, et Z
scoreboard players set #biomeView xMin 0
scoreboard players set #biomeView yMin 0
scoreboard players set #biomeView zMin 0
scoreboard players set #biomeView xMax 9
scoreboard players set #biomeView yMax 9
scoreboard players set #biomeView zMax 9

scoreboard players set #biomeView posX 0
scoreboard players set #biomeView posY 0
scoreboard players set #biomeView posZ 0


# On peut ensuite invoquer une entité et tester toutes les positions environnantes
summon marker ~-64 ~-64 ~-64 {Tags:["BiomeViewMarkerTemp"],CustomName:'"Marqueur de BiomeView"'}
execute as @e[type=marker,tag=BiomeViewMarkerTemp,limit=1] at @s run function tdh:meteo/debug/biome_view/choix_boucle
kill @e[type=marker,tag=BiomeViewMarkerTemp]

# On supprime le tag temporaire
tag @a[tag=BiomeViewPlayerTemp] remove BiomeViewPlayerTemp
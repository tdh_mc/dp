# On exécute cette fonction en tant qu'une snowball SnowUpdate à "sa" position (en fait celle du bloc en dessous)
# pour vérifier si la hauteur maximale de neige est atteinte

# On se donne un score spécifique selon l'altitude à laquelle on se trouve
# Par défaut on est supérieurs à 210
scoreboard players set @s altitudeFromZero 225
# Si on est inférieurs à 210
execute if blocks ~ ~46 ~ ~ ~46 ~ ~ ~47 ~ all run scoreboard players set @s altitudeFromZero 195
# Si on est inférieurs à 180
execute if blocks ~ ~76 ~ ~ ~76 ~ ~ ~77 ~ all run scoreboard players set @s altitudeFromZero 165
# Si on est inférieurs à 150
execute if blocks ~ ~106 ~ ~ ~106 ~ ~ ~107 ~ all run scoreboard players set @s altitudeFromZero 135
# Si on est inférieurs à 120
execute if blocks ~ ~136 ~ ~ ~136 ~ ~ ~137 ~ all run scoreboard players set @s altitudeFromZero 105
# Si on est inférieurs à 90
execute if blocks ~ ~166 ~ ~ ~166 ~ ~ ~167 ~ all run scoreboard players set @s altitudeFromZero 75


# Selon ce score, on vérifie si on peut ajouter un niveau de neige
execute if score @s altitudeFromZero matches ..89 run function tdh:meteo/effects/neige/blocs/test_hauteur_neige/1m
execute if score @s altitudeFromZero matches 90..119 run function tdh:meteo/effects/neige/blocs/test_hauteur_neige/1m50
execute if score @s altitudeFromZero matches 120..149 run function tdh:meteo/effects/neige/blocs/test_hauteur_neige/2m
execute if score @s altitudeFromZero matches 150..179 run function tdh:meteo/effects/neige/blocs/test_hauteur_neige/3m
execute if score @s altitudeFromZero matches 180..209 run function tdh:meteo/effects/neige/blocs/test_hauteur_neige/4m
execute if score @s altitudeFromZero matches 210.. run function tdh:meteo/effects/neige/blocs/test_hauteur_neige/5m

# On réinitialise notre score
scoreboard players reset @s altitudeFromZero
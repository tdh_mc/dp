# On exécute cette fonction en tant qu'une snowball SnowUpdate à "sa" position (en fait celle du bloc en dessous)
# pour vérifier quel est le niveau d'humidité du biome dans lequel on se trouve,
# et ajuster en conséquence la probabilité de faire monter la neige de 1

# Il y a 5 niveaux d'humidité/précipitation car on ne peut pas directement récupérer la valeur par biome définie dans la configuration (worldgen/biomes/)
# Du coup on a séparé les précipitations en :
# [1] Très faible (0.01-0.24)
# [2] Faible (0.01-0.24)
# [3] Normal (0.25-0.49)
# [4] Élevé (0.5-0.74)
# [5] Très élevé (0.75+)

# On définit le maximum de la variable aléatoire selon ce niveau d'humidité
# On a 100% de chances de monter au niveau 5, 80% au niveau 4, etc linéairement
scoreboard players set #random max 0
execute if predicate tdh:biome/precipitations_1 run scoreboard players set #random max 500
execute if score #random max matches 0 if predicate tdh:biome/precipitations_2 run scoreboard players set #random max 400
execute if score #random max matches 0 if predicate tdh:biome/precipitations_3 run scoreboard players set #random max 300
execute if score #random max matches 0 if predicate tdh:biome/precipitations_4 run scoreboard players set #random max 200
execute if score #random max matches 0 if predicate tdh:biome/precipitations_5 run scoreboard players set #random max 100

# On prend une variable aléatoire, et si elle matche, on fait monter le niveau de la neige
scoreboard players set #random min 0
function tdh:random

execute if score #random temp matches ..99 run function tdh:meteo/effects/neige/blocs/succes

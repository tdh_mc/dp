# On calcule le tick du prochain update
scoreboard players operation #neige jourUpdate = #temps idJour
scoreboard players operation #neige tickUpdate = #temps currentTdhTick
scoreboard players operation #neige tickUpdate += #neige frequenceUpdate
execute if score #neige tickUpdate >= #temps fullDayTicks run function tdh:meteo/effects/neige/blocs/tick/prochain_update_demain
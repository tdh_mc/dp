# On doit rester sous les 1m + 5 couches de neige
# On est à la position d'un bloc de neige
# Donc si le bloc en dessous contient de la neige aussi et qu'on est à 5 blocs ou plus, on s'arrête
execute if block ~ ~-1 ~ snow run tag @s add Stop
execute as @s[tag=Stop] unless block ~ ~ ~ snow[layers=5] unless block ~ ~ ~ snow[layers=6] unless block ~ ~ ~ snow[layers=7] unless block ~ ~ ~ snow[layers=8] run tag @s remove Stop

# Dans le cas contraire on peut ajouter de la neige
execute as @s[tag=!Stop] run function tdh:meteo/effects/neige/blocs/succes
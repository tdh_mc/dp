# Tick de la montée de la neige
# Exécuté s'il neige, à chaque tick,
# pour vérifier si le tick d'update a été dépassé

execute if score #temps idJour = #neige jourUpdate if score #temps currentTdhTick >= #neige tickUpdate run function tdh:meteo/effects/neige/blocs/tick
execute if score #temps idJour > #neige jourUpdate run function tdh:meteo/effects/neige/blocs/tick
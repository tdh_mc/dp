# On doit rester sous les 2m + 1 couches de neige
# On est à la position d'un bloc de neige
# Donc si les 2 blocs en dessous contiennent de la neige aussi, on s'arrête
execute if block ~ ~-1 ~ snow if block ~ ~-2 ~ snow run tag @s add Stop

# Dans le cas contraire on peut ajouter de la neige
execute as @s[tag=!Stop] run function tdh:meteo/effects/neige/blocs/succes
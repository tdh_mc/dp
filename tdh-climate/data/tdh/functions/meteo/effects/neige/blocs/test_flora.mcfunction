# On vérifie si le bloc du dessous contient également un bloc de flore ;
# Si oui, c'est celui-ci que l'on remplacera
execute store result score @s temp if block ~ ~-1 ~ #tdh:snow_replaceable
execute if score @s temp matches 0 if block ~ ~-1 ~ #tdh:solid run function tdh:meteo/effects/neige/blocs/test_biome
execute if score @s temp matches 0 positioned ~ ~-1 ~ if block ~ ~ ~ snow run function tdh:meteo/effects/neige/blocs/test_biome
execute if score @s temp matches 1 positioned ~ ~-1 ~ run function tdh:meteo/effects/neige/blocs/test_flora
# On exécute cette fonction en tant qu'une snowball tempSnowball à sa position,
# pour augmenter la hauteur de la neige sur laquelle elle est posée

execute if block ~ ~ ~ snow[layers=8] run setblock ~ ~1 ~ snow[layers=1]
execute if block ~ ~ ~ snow[layers=7] run setblock ~ ~ ~ snow[layers=8]
execute if block ~ ~ ~ snow[layers=6] run setblock ~ ~ ~ snow[layers=7]
execute if block ~ ~ ~ snow[layers=5] run setblock ~ ~ ~ snow[layers=6]
execute if block ~ ~ ~ snow[layers=4] run setblock ~ ~ ~ snow[layers=5]
execute if block ~ ~ ~ snow[layers=3] run setblock ~ ~ ~ snow[layers=4]
execute if block ~ ~ ~ snow[layers=2] run setblock ~ ~ ~ snow[layers=3]
execute if block ~ ~ ~ snow[layers=1] run setblock ~ ~ ~ snow[layers=2]
execute if block ~ ~ ~ #tdh:snow_replaceable run setblock ~ ~ ~ snow[layers=1]
execute if block ~ ~ ~ air run setblock ~ ~ ~ snow[layers=1]

tellraw @a[tag=MeteoDetailLog] [{"text":"[ChuteNeige]","color":"green"},{"text":" Succès de la montée de la neige! Position ","color":"gray","extra":[{"entity":"@s","nbt":"Pos"}]}]
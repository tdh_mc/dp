# On doit rester sous les 1m + 1 couche de neige
# On est à la position d'un bloc de neige
# Donc si le bloc en dessous contient de la neige aussi, on s'arrête

# Dans le cas contraire on peut ajouter de la neige
execute unless block ~ ~-1 ~ snow run function tdh:meteo/effects/neige/blocs/succes
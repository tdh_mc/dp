# Tick de la montée de la neige
# Exécuté à chaque tick d'update des chutes de neige

# On exécute le tick à la position de chaque joueur chez qui il neige :
# concrètement, on va invoquer des items temporaires et les spread autour de chaque joueur

# On calcule d'abord la variable temp qui contient le nombre d'éléments à invoquer par joueur
scoreboard players operation #neige temp = #meteo intensite
scoreboard players operation #neige temp -= #neige intensiteMin
scoreboard players operation #neige temp *= #neige vitesse

# On exécute ensuite la fonction pour chaque joueur ayant le tag AllowSpreadplayers
# (sans lequel on risque de faire crasher le serveur à cause du bug MC-186963)
execute at @a[tag=AllowSpreadplayers] run function tdh:meteo/effects/neige/blocs/tick_joueur
# Si l'actualisation automatique du monde est activée, et qu'aucun joueur n'est en ligne,
# on actualise la couche de neige autour des entités temporaires d'actualisation
execute if score #meteoActuAuto on matches 1 unless score #temps playersOnline > #meteoActuAuto playersOnline as @e[type=marker,tag=MeteoActuAuto] at @s run function tdh:meteo/effects/neige/blocs/tick_entite

# On vérifie si on peut faire monter la neige à chacun des endroits testés
execute as @e[type=snowball,tag=SnowUpdate] at @s positioned ~ ~-1 ~ run function tdh:meteo/effects/neige/blocs/test_bloc

# Message de debug
execute store result score #neigeSpawn temp if entity @e[type=snowball,tag=SnowUpdate]
tellraw @a[tag=MeteoDetailLog] [{"text":"[ChuteNeige]","color":"green"},{"text":" On a tenté de faire monter la neige à ","color":"gray"},{"score":{"name":"#neigeSpawn","objective":"temp"}},{"text":" positions.","color":"gray"}]

# On élimine les entités temporaires
kill @e[type=snowball,tag=SnowUpdate]

# On calcule le tick du prochain update
execute unless score #neige frequenceUpdate matches 1 run function tdh:meteo/effects/neige/blocs/tick/calcul_prochain_update
# Si le predicate passe, on passe *tout de suite* au succès sans faire le test humidité
# Ainsi on évite de tester de manière répétée ce predicate assez lourd
execute positioned ~ ~16 ~ if predicate tdh:biome/neige positioned ~ ~-16 ~ run function tdh:meteo/effects/neige/blocs/succes
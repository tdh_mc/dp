# On exécute cette fonction en tant qu'une snowball SnowUpdate à "sa" position (en fait celle du bloc en-dessous)

# On assigne une variable temporaire pour éviter de faire plusieurs checks de bloc si le premier passe
scoreboard players set #neige temp2 0
# On vérifie si notre position contient de la neige (ou des végétaux) et pas de joueur
execute if block ~ ~ ~ snow run scoreboard players set #neige temp2 1
execute if score #neige temp2 matches 0 if block ~ ~ ~ #tdh:snow_replaceable run scoreboard players set #neige temp2 2
# On permet également de faire monter la neige s'il n'y a pas de neige ou de flore à cette position ;
# en effet, les entités d'actualisation automatique ne déclenchent pas de random tick vanilla à proximité,
# par conséquent on a besoin de tenter de faire monter la neige également si on trouve un bloc solide
execute if score #neige temp2 matches 0 if block ~ ~ ~ #tdh:solid run scoreboard players set #neige temp2 3
execute if score #neige temp2 matches 0 if block ~ ~ ~ #stairs[half=top] run scoreboard players set #neige temp2 3
execute if score #neige temp2 matches 0 if block ~ ~ ~ #slabs[type=top] run scoreboard players set #neige temp2 3

# Selon la valeur de la variable temporaire, on exécute la bonne fonction
execute if score #neige temp2 matches 1 run function tdh:meteo/effects/neige/blocs/test_neige
execute if score #neige temp2 matches 2 run function tdh:meteo/effects/neige/blocs/test_flora
execute if score #neige temp2 matches 3 positioned ~ ~1 ~ run function tdh:meteo/effects/neige/blocs/test_biome
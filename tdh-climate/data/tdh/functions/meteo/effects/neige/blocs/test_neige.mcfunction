# On appelle cette fonction à la position d'un bloc dont on a détecté qu'il s'agit de neige,
# pour savoir s'il neige effectivement à cet endroit en cette saison

scoreboard players set #neige temp3 0
execute if predicate tdh:biome/neige run scoreboard players set #neige temp3 1

# S'il neige effectivement, on effectue la suite des tests
execute if score #neige temp3 matches 1 run function tdh:meteo/effects/neige/blocs/test_humidite
# S'il ne neige pas en cette saison, on fait fondre instantanément la neige à cette position
execute unless score #neige temp3 matches 1 run function tdh:meteo/effects/beau_temps/fonte_neige/totale
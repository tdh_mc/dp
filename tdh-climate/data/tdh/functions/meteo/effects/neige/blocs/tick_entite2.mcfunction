# Tick de la montée de la neige
# Exécuté à la position d'une entité quand il neige, à chaque tick d'update des chutes de neige

# On invoque toujours 10 items quelle que soit l'intensité, pour accélérer le processus
scoreboard players set #neigeSpawn temp 10
execute at @e[type=item_frame,tag=SpawnMonde,limit=1] run function tdh:meteo/effects/neige/blocs/invoquer_entites

# On pose cet item à un endroit aléatoire autour du joueur 
# Différentes fonctions servent à prendre en compte la distance paramétrable
# On cap la distance paramétrée à 90 quelle que soit le paramétrage ;
# cela permet de ne pas dépasser des chunks /forceloadés
function tdh:meteo/effects/neige/blocs/spread/20_80

# Message de debug
execute store result score #neigeSpawn temp if entity @e[type=snowball,tag=SnowTemp]
tellraw @a[tag=MeteoDetailLog] [{"text":"[ChuteNeige]","color":"green"},{"text":" On a invoqué ","color":"gray"},{"score":{"name":"#neigeSpawn","objective":"temp"}},{"text":" entités SnowUpdate autour de ","color":"gray"},{"selector":"@s"},{"text":".","color":"gray"}]

# On retire le tag temporaire des entités invoquées
tag @e[type=snowball,tag=SnowTemp] remove SnowTemp
# On invoque 8 particules de neige par tick
particle minecraft:firework ^2.1 ^8 ^8 0.03 -1 0.11 1.0 0 normal @a[distance=..10]
particle minecraft:firework ^0.6 ^9.1 ^-7 0.17 -1 -0.1 0.93 0 normal @a[distance=..10]
particle minecraft:firework ^-1.6 ^8.3 ^7.5 0.16 -1 -0.17 1.04 0 normal @a[distance=..10]
particle minecraft:firework ^1.4 ^7.9 ^-6.2 -0.22 -1 0 1.09 0 normal @a[distance=..10]
particle minecraft:firework ^2.1 ^9.2 ^3.7 -0.1 -1 0.17 0.86 0 normal @a[distance=..10]
particle minecraft:firework ^0.55 ^8.6 ^-9.5 0.03 -1 -0.08 0.81 0 normal @a[distance=..10]
particle minecraft:firework ^-0.6 ^8.4 ^-5.5 -0.13 -1 -0.03 1.11 0 normal @a[distance=..10]
particle minecraft:firework ^4.6 ^8.2 ^-2.2 0.02 -1 -0.1 0.8 0 normal @a[distance=..10]
# On invoque 4 particules de neige par tick
particle minecraft:firework ^ ^8 ^10 0.03 -1 -0.06 1.0 0 normal @a[distance=..10]
particle minecraft:firework ^ ^7.6 ^-10 0.01 -1 0.09 0.94 0 normal @a[distance=..10]
particle minecraft:firework ^-1.8 ^6.9 ^8 0.12 -1 -0.03 1.06 0 normal @a[distance=..10]
particle minecraft:firework ^1.6 ^8.1 ^-8 -0.03 -1 -0.03 1.03 0 normal @a[distance=..10]
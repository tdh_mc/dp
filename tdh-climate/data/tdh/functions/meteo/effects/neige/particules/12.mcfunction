# On invoque 12 particules de neige par tick
particle minecraft:firework ^4.3 ^9.2 ^8 0.15 -1 -0.3 1.0 0 normal @a[distance=..10]
particle minecraft:firework ^-1.3 ^9.3 ^7.5 -0.3 -1 0.17 0.87 0 normal @a[distance=..10]
particle minecraft:firework ^0.8 ^8.2 ^-7.2 -0.11 -1 -0.11 1.19 0 normal @a[distance=..10]
particle minecraft:firework ^2.1 ^9.3 ^4.7 0 -1 0.3 1.23 0 normal @a[distance=..10]
particle minecraft:firework ^0.65 ^7.8 ^-10.5 -0.2 -1 0 1.51 0 normal @a[distance=..10]
particle minecraft:firework ^-0.6 ^8.9 ^-4.5 0.6 -1 0.03 1.09 0 normal @a[distance=..10]
particle minecraft:firework ^4.4 ^9.3 ^-2.2 0.17 -1 0.25 1.18 0 normal @a[distance=..10]
particle minecraft:firework ^-2.6 ^9.3 ^-3.5 0.26 -1 0.19 1.01 0 normal @a[distance=..10]
particle minecraft:firework ^2.4 ^8.6 ^4.2 -0.23 -1 -0.44 1.31 0 normal @a[distance=..10]
particle minecraft:firework ^-6.1 ^8 ^3.7 0.19 -1 -0.14 1.27 0 normal @a[distance=..10]
particle minecraft:firework ^-0.55 ^7.7 ^10.5 0.17 -1 -0.4 1.13 0 normal @a[distance=..10]
particle minecraft:firework ^0.7 ^9.3 ^6.5 0.13 -1 0.06 0.89 0 normal @a[distance=..10]
particle minecraft:firework ^3.4 ^8.9 ^5.2 0 -1 0 0.99 0 normal @a[distance=..10]
particle minecraft:firework ^-1.1 ^8.6 ^0.6 0.19 -1 -0.21 0.87 0 normal @a[distance=..10]
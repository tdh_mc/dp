# On téléporte chaque joueur d'une courte distance dans la direction du vent
tp @p[gamemode=!spectator,distance=0] ^ ^ ^0.019
# tp @p[gamemode=!spectator,distance=0,scores={insideness=1}] ^ ^ ^0.07
# tp @p[gamemode=!spectator,distance=0,scores={insideness=2}] ^ ^ ^0.06
# tp @p[gamemode=!spectator,distance=0,scores={insideness=3}] ^ ^ ^0.052
# tp @p[gamemode=!spectator,distance=0,scores={insideness=4}] ^ ^ ^0.044
# tp @p[gamemode=!spectator,distance=0,scores={insideness=5}] ^ ^ ^0.036
# tp @p[gamemode=!spectator,distance=0,scores={insideness=6}] ^ ^ ^0.02
# tp @p[gamemode=!spectator,distance=0,scores={insideness=7}] ^ ^ ^0.03
# tp @p[gamemode=!spectator,distance=0,scores={insideness=8}] ^ ^ ^0.02
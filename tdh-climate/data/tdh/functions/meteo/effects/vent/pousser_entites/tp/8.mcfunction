# On téléporte chaque joueur d'une très courte distance dans la direction du vent
tp @p[gamemode=!spectator,distance=0] ^ ^ ^0.0125
# tp @p[gamemode=!spectator,distance=0,scores={insideness=1}] ^ ^ ^0.045
# tp @p[gamemode=!spectator,distance=0,scores={insideness=2}] ^ ^ ^0.04
# tp @p[gamemode=!spectator,distance=0,scores={insideness=3}] ^ ^ ^0.035
# tp @p[gamemode=!spectator,distance=0,scores={insideness=4}] ^ ^ ^0.03
# tp @p[gamemode=!spectator,distance=0,scores={insideness=5}] ^ ^ ^0.025
# tp @p[gamemode=!spectator,distance=0,scores={insideness=6}] ^ ^ ^0.02
# tp @p[gamemode=!spectator,distance=0,scores={insideness=7}] ^ ^ ^0.015
# tp @p[gamemode=!spectator,distance=0,scores={insideness=8}] ^ ^ ^0.01
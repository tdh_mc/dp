# On téléporte chaque joueur d'une courte distance dans la direction du vent
tp @p[gamemode=!spectator,distance=0] ^ ^ ^0.025
# tp @p[gamemode=!spectator,distance=0,scores={insideness=1}] ^ ^ ^0.09
# tp @p[gamemode=!spectator,distance=0,scores={insideness=2}] ^ ^ ^0.08
# tp @p[gamemode=!spectator,distance=0,scores={insideness=3}] ^ ^ ^0.07
# tp @p[gamemode=!spectator,distance=0,scores={insideness=4}] ^ ^ ^0.06
# tp @p[gamemode=!spectator,distance=0,scores={insideness=5}] ^ ^ ^0.05
# tp @p[gamemode=!spectator,distance=0,scores={insideness=6}] ^ ^ ^0.04
# tp @p[gamemode=!spectator,distance=0,scores={insideness=7}] ^ ^ ^0.03
# tp @p[gamemode=!spectator,distance=0,scores={insideness=8}] ^ ^ ^0.02
# On exécute cette fonction à la position d'un joueur ayant une insideness inférieure à 9,
# à la rotation de l'armor stand DirectionVent pendant un temps d'intensité 3

# On fait des tests sur les blocs qui nous entourent pour checker la force du vent
scoreboard players set #vent temp 0
execute if block ^ ^ ^-10 #tdh:passable if block ^ ^ ^10 #tdh:passable run scoreboard players add #vent temp 1
execute if block ^ ^ ^-20 #tdh:passable if block ^ ^ ^20 #tdh:passable run scoreboard players add #vent temp 1
execute if block ^ ^ ^-30 #tdh:passable if block ^ ^ ^30 #tdh:passable run scoreboard players add #vent temp 1
execute if block ^ ^ ^-40 #tdh:passable if block ^ ^ ^40 #tdh:passable run scoreboard players add #vent temp 1

execute if block ^ ^4 ^-10 #tdh:passable if block ^ ^2 ^10 #tdh:passable run scoreboard players add #vent temp 1
execute if block ^ ^6 ^-20 #tdh:passable if block ^ ^3 ^20 #tdh:passable run scoreboard players add #vent temp 1
execute if block ^ ^8 ^-30 #tdh:passable if block ^ ^4 ^30 #tdh:passable run scoreboard players add #vent temp 1
execute if block ^ ^10 ^-40 #tdh:passable if block ^ ^5 ^40 #tdh:passable run scoreboard players add #vent temp 1

# Selon la force calculée du vent, on applique un effet plus ou moins puissant
execute if score #vent temp matches 1 run function tdh:meteo/effects/vent/pousser_entites/tp/1
execute if score #vent temp matches 2 run function tdh:meteo/effects/vent/pousser_entites/tp/2
execute if score #vent temp matches 3 run function tdh:meteo/effects/vent/pousser_entites/tp/3
execute if score #vent temp matches 4 run function tdh:meteo/effects/vent/pousser_entites/tp/4
execute if score #vent temp matches 5 run function tdh:meteo/effects/vent/pousser_entites/tp/5
execute if score #vent temp matches 6 run function tdh:meteo/effects/vent/pousser_entites/tp/6
execute if score #vent temp matches 7 run function tdh:meteo/effects/vent/pousser_entites/tp/7
execute if score #vent temp matches 8 run function tdh:meteo/effects/vent/pousser_entites/tp/8
# Tick des effets du vent
# Exécuté à chaque tick ingame, s'il y a du vent

# On exécute l'update tous les N ticks (décidé au hasard selon les réglages dans la fonction update elle-même)
scoreboard players remove #vent currentTick 1
execute if score #vent currentTick matches ..-1 run function tdh:meteo/effects/vent/update

# Si le vent est intense, on applique un mouvement forcé aux joueurs
#execute if score #meteo intensite matches 3 at @e[type=armor_stand,tag=DirectionVent,limit=1] positioned as @a[gamemode=!spectator,gamemode=!creative,scores={insideness=..11}] if block ^ ^ ^0.7 #tdh:passable if block ^ ^ ^-0.7 #tdh:passable if block ^ ^ ^-3 #tdh:passable run function tdh:meteo/effects/vent/pousser_entites/3
#execute if score #meteo intensite matches 4.. at @e[type=armor_stand,tag=DirectionVent,limit=1] positioned as @a[gamemode=!spectator,gamemode=!creative,scores={insideness=..11}] if block ^ ^ ^0.7 #tdh:passable if block ^ ^ ^-0.7 #tdh:passable if block ^ ^ ^-3 #tdh:passable run function tdh:meteo/effects/vent/pousser_entites/4
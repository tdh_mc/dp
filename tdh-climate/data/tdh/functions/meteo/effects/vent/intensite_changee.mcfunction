# On enregistre le réglage correspondant à l'intensité actuelle de la météo dans #orage probaChgmt
# Il permettra de déterminer quelles sont les chances d'avoir un éclair (sur 10'000)
execute if score #meteo intensite matches 0 run function tdh:meteo/effects/vent/intensite_changee/0
execute if score #meteo intensite matches 1 run function tdh:meteo/effects/vent/intensite_changee/1
execute if score #meteo intensite matches 2 run function tdh:meteo/effects/vent/intensite_changee/2
execute if score #meteo intensite matches 3 run function tdh:meteo/effects/vent/intensite_changee/3
execute if score #meteo intensite matches 4 run function tdh:meteo/effects/vent/intensite_changee/4
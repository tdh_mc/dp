# On fait évoluer la direction du vent si nécessaire
execute as @e[type=armor_stand,tag=DirectionVent,limit=1] at @s run function tdh:meteo/effects/vent/changer_direction

# On fait évoluer la vitesse du vent si nécessaire
scoreboard players set #random min 0
scoreboard players set #random max 20
function tdh:random
execute if score #vent vitesse > #vent vitesseMin if score #random temp < #meteo intensite run scoreboard players remove #vent vitesse 1
scoreboard players remove #random temp 10
execute if score #vent vitesse < #vent vitesseMax if score #random temp matches 0.. if score #random temp < #meteo intensite run scoreboard players add #vent vitesse 1

# On réinitialise le timer de ce calcul,
# avec une durée aléatoire dépendant de nos réglages actuels
scoreboard players operation #random min = #vent dureeMin
scoreboard players operation #random max = #vent dureeMax
function tdh:random
scoreboard players operation #vent currentTick = #random temp
# On change la direction du vent,
# et la fonction est exécutée par l'armor_stand DirectionVent (au spawn) à sa position et rotation
# Elle va évoluer de +/- X° de rotation aléatoirement, X dépendant des réglages actuels

# On stocke une variable aléatoire que l'on module pour avoir +/- [notre réglage]
scoreboard players operation #random max = #vent rotationMax
scoreboard players set #random min 0
scoreboard players operation #random min -= #random max
function tdh:random

# On enregistre la rotation actuelle de l'armor stand DirectionVent
execute store result score #vent rotation run data get entity @s Rotation[0]
# On ajoute la variable aléatoire
scoreboard players operation #vent rotation += #random temp
# On prend le mod360 puisque c'est une rotation, et on enregistre le résultat dans l'armor stand
scoreboard players set #vent temp 360
execute store result entity @s Rotation[0] float 1 run scoreboard players operation #vent rotation %= #vent temp

# On réinitialise la variable temporaire
scoreboard players reset #vent temp
# On invoque une entité à la position du spawn, puis on la place dans un chunk déchargé pour le forceload

# Sont stockés dans notre configuration :
# #meteoActuAuto playersOnline	- le nombre de joueurs au maximum (au-delà duquel on n'actualise pas)
# #meteoActuAuto nombre		- le nombre maximal d'entités d'actualisation simultanées
# #meteoActuAuto duree		- la durée de vie d'une entité d'actualisation
# #meteoActuAuto xMin/xMax	- les bornes sur l'axe des X de la position aléatoire des entités d'actualisation
# #meteoActuAuto zMin/zMax	- les bornes sur l'axe des Z de la position aléatoire des entités d'actualisation

execute at @e[type=item_frame,tag=SpawnMonde,limit=1] run summon marker ~ ~ ~ {CustomName:'"Actualisateur du système météo"',Tags:["MeteoActuAuto","MeteoActuTemp"]}

# On calcule une position dans les bornes précisées dans notre configuration,
# puis on la stocke dans un storage
data modify storage tdh:meteo actu_auto.position set value [0d,64d,0d]
scoreboard players operation #random min = #meteoActuAuto xMin
scoreboard players operation #random max = #meteoActuAuto xMax
function tdh:random
execute store result storage tdh:meteo actu_auto.position[0] double 1 run scoreboard players get #random temp
scoreboard players operation #random min = #meteoActuAuto zMin
scoreboard players operation #random max = #meteoActuAuto zMax
function tdh:random
execute store result storage tdh:meteo actu_auto.position[2] double 1 run scoreboard players get #random temp


# Message de debug
execute if score #temps playersOnline matches 1.. run tellraw @a[tag=MeteoDebugLog] [{"text":"[ActuAuto]","color":"green"},{"text":" On tente de placer la nouvelle entité ","color":"gray"},{"selector":"@e[type=marker,tag=MeteoActuTemp,limit=1]"},{"text":" à la position ","color":"gray"},{"storage":"tdh:meteo","nbt":"actu_auto.position"},{"text":".","color":"gray"}]
execute unless score #temps playersOnline matches 1.. run say [ActuAuto] On tente de placer la nouvelle entité @s à la position calculée

# On définit la position de l'entité
# On doit le faire dans une sous-fonction avec "execute as" pour éviter le "bug" MC-227930
execute as @e[type=marker,tag=MeteoActuTemp,limit=1] run function tdh:meteo/effects/actu_auto/positionner_entite

# On ne récurse pas (on n'invoque pas d'autre entité) pour éviter de tenter de charger plein de chunks en
# même temps et de créer des ralentissements. 1 forceload toutes les quelques secondes sera bien suffisant.
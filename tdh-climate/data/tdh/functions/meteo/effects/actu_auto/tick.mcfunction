# Cette fonction est exécutée régulièrement par l'un des tdh-ticks,
# et invoque de nouvelles entités temporaires pour actualiser la couche de neige
# à des positions aléatoires, si le système est activé et qu'aucun joueur n'est en ligne

# L'actualisation à proprement parler se déroule dans les fonctions d'origine de gestion de la chute et de la fonte de la neige, dans tdh:meteo/effects/neige/blocs/ et tdh:meteo/effects/beau_temps/fonte_neige/

# Sont stockés dans notre configuration :
# #meteoActuAuto playersOnline	- le nombre de joueurs au maximum (au-delà duquel on n'actualise pas)
# #meteoActuAuto nombre		- le nombre maximal d'entités d'actualisation simultanées
# #meteoActuAuto duree		- la durée de vie d'une entité d'actualisation
# #meteoActuAuto xMin/xMax	- les bornes sur l'axe des X de la position aléatoire des entités d'actualisation
# #meteoActuAuto zMin/zMax	- les bornes sur l'axe des Z de la position aléatoire des entités d'actualisation

# Si le système est activé et qu'il n'y a aucun joueur en ligne, on ticke les entités existantes
scoreboard players set #meteoActuAuto temp 0
execute if score #meteoActuAuto on matches 1 unless score #temps playersOnline > #meteoActuAuto playersOnline run scoreboard players set #meteoActuAuto temp 1
execute if score #meteoActuAuto temp matches 1 run function tdh:meteo/effects/actu_auto/tick_entites

# Sinon (soit il y a un joueur en ligne, soit on a désactivé le système), on tente de supprimer les entités existantes
execute unless score #meteoActuAuto temp matches 1 run function tdh:meteo/effects/actu_auto/nettoyer_entites
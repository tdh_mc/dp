# On réinitialise tous nos scores, on dé-forceload le chunk, puis on se tue
scoreboard players reset @s
execute if score #temps playersOnline matches 1.. run tellraw @a[tag=MeteoDebugLog] [{"text":"[ActuAuto]","color":"red"},{"text":" On détruit l'entité ","color":"gray"},{"selector":"@s","hoverEvent":{"action":"show_text","contents":[{"entity":"@s","nbt":"Pos"}]}},{"text":".","color":"gray"}]
execute unless score #temps playersOnline matches 1.. run say [ActuAuto] On détruit l'entité @s

# On affiche un message d'erreur si la commande forceload renvoie 0 (= impossible de décharger le chunk),
# car cela peut signifier que le chunk est resté forceload en raison d'une erreur dans le code
execute store result score #meteoActuAuto temp3 at @s run forceload remove ~-96 ~-96 ~96 ~96
execute unless score #meteoActuAuto temp3 matches 1.. if score #temps playersOnline matches 1.. run tellraw @a[tag=MeteoDebugLog] [{"text":"[ActuAuto]","color":"red"},{"text":" L'entité ","color":"gray"},{"selector":"@s","hoverEvent":{"action":"show_text","contents":[{"entity":"@s","nbt":"Pos"}]}},{"text":" n'a pas réussi à dé-forceloader son chunk de résidence avant de se détruire.","color":"gray"}]
execute unless score #meteoActuAuto temp3 matches 1.. unless score #temps playersOnline matches 1.. run say [ActuAuto] [Erreur] Une entité n'a pas réussi à dé-forceloader son chunk de résidence avant de se détruire.

kill @s
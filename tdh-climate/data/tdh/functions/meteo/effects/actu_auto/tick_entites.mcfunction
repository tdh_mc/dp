# On exécute régulièrement cette fonction pour ticker les entités existantes,
# et en générer de nouvelles s'il n'y en a pas assez

# Sont stockés dans notre configuration :
# #meteoActuAuto playersOnline	- le nombre de joueurs au maximum (au-delà duquel on n'actualise pas)
# #meteoActuAuto nombre		- le nombre maximal d'entités d'actualisation simultanées
# #meteoActuAuto duree		- la durée de vie d'une entité d'actualisation
# #meteoActuAuto xMin/xMax	- les bornes sur l'axe des X de la position aléatoire des entités d'actualisation
# #meteoActuAuto zMin/zMax	- les bornes sur l'axe des Z de la position aléatoire des entités d'actualisation

# On ticke les entités existantes
execute as @e[type=marker,tag=MeteoActuAuto] at @s run function tdh:meteo/effects/actu_auto/tick_entite

# On compte les entités existantes
execute store result score #meteoActuAuto temp2 if entity @e[type=marker,tag=MeteoActuAuto]

# S'il y en a moins que le maximum, on en invoque de nouvelles
execute if score #meteoActuAuto temp2 < #meteoActuAuto nombre run function tdh:meteo/effects/actu_auto/invoquer_entite
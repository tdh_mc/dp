# On positionne l'entité qui exécute cette fonction à la position indiquée dans le storage,
# puis on forceload le chunk dans lequel on se retrouve

# Sont stockés dans notre configuration :
# #meteoActuAuto playersOnline	- le nombre de joueurs au maximum (au-delà duquel on n'actualise pas)
# #meteoActuAuto nombre		- le nombre maximal d'entités d'actualisation simultanées
# #meteoActuAuto duree		- la durée de vie d'une entité d'actualisation
# #meteoActuAuto xMin/xMax	- les bornes sur l'axe des X de la position aléatoire des entités d'actualisation
# #meteoActuAuto zMin/zMax	- les bornes sur l'axe des Z de la position aléatoire des entités d'actualisation

# On définit nos scores
scoreboard players set @s duree 0
# On retire notre tag temporaire
tag @s remove MeteoActuTemp

# On se place à la bonne position
data modify entity @s Pos set from storage tdh:meteo actu_auto.position
execute if score #temps playersOnline matches 1.. run tellraw @a[tag=MeteoDebugLog] [{"text":"[ActuAuto]","color":"green"},{"text":" On a placé l'entité ","color":"gray"},{"selector":"@s"},{"text":" à la position ","color":"gray"},{"entity":"@s","nbt":"Pos"},{"text":".","color":"gray"}]
execute if score #temps playersOnline matches ..0 run say [ActuAuto] Entité déplacée avec succès

# On forceload le chunk où on se trouve
execute store result score #meteoActuAuto temp3 at @s run forceload add ~-96 ~-96 ~96 ~96
execute unless score #meteoActuAuto temp3 matches 1.. if score #temps playersOnline matches 1.. run tellraw @a[tag=MeteoDebugLog] [{"text":"[ActuAuto]","color":"red"},{"text":" L'entité ","color":"gray"},{"selector":"@s","hoverEvent":{"action":"show_text","contents":[{"entity":"@s","nbt":"Pos"}]}},{"text":" n'a pas réussi à forceloader son chunk de résidence.","color":"gray"}]
execute unless score #meteoActuAuto temp3 matches 1.. unless score #temps playersOnline matches 1.. run say [ActuAuto] [Erreur] L'entité @s n'a pas réussi à forceloader son chunk de résidence.

# Si on n'a pas réussi à le forceload, on se détruit
execute unless score #meteoActuAuto temp3 matches 1.. at @s run function tdh:meteo/effects/actu_auto/detruire_entite

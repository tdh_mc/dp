# On exécute cette fonction régulièrement pour tick l'entité MeteoActuAuto qui l'exécute
# On doit impérativement être à la position de l'entité pour pouvoir décharger proprement le chunk

# Sont stockés dans notre configuration :
# #meteoActuAuto playersOnline	- le nombre de joueurs au maximum (au-delà duquel on n'actualise pas)
# #meteoActuAuto nombre		- le nombre maximal d'entités d'actualisation simultanées
# #meteoActuAuto duree		- la durée de vie d'une entité d'actualisation
# #meteoActuAuto xMin/xMax	- les bornes sur l'axe des X de la position aléatoire des entités d'actualisation
# #meteoActuAuto zMin/zMax	- les bornes sur l'axe des Z de la position aléatoire des entités d'actualisation

# On augmente notre durée de vie effective
scoreboard players add @s duree 1

# Si on a dépassé la durée de vie maximale, on se détruit
execute if score @s duree > #meteoActuAuto duree run function tdh:meteo/effects/actu_auto/detruire_entite
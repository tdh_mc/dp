# Tick des effets de pluie
# Exécuté à chaque tick où il pleut (meteo actuelle = 2 ou 3) à la position de chaque joueur chez qui il pleut

# On lance les fonctions appropriées pour les joueurs qui sont dans les biomes où il pleut, puis celles pour ceux qui sont où il neige
execute at @a[scores={biomeType=2..3,insideness=..11}] rotated ~ 0 positioned ^ ^ ^5 rotated as @e[sort=random,type=#tdh:random,limit=1] run function tdh:meteo/effects/pluie/particules
execute at @a[scores={biomeType=1,insideness=..11}] rotated ~ 0 positioned ^ ^ ^5 rotated as @e[sort=random,type=#tdh:random,limit=1] run function tdh:meteo/effects/neige/particules
# On invoque 16 particules de pluie par tick
particle minecraft:falling_water ^ ^16 ^4 3.2 0.2 3.2 1.5 16 normal @a[distance=..10]
particle minecraft:falling_water ^ ^16 ^-4 3.2 0.2 3.2 1.5 16 normal @a[distance=..10]

particle minecraft:falling_water ^ ^14 ^9 3.2 0.2 3.2 1.5 8 normal @a[distance=..10]
particle minecraft:falling_water ^ ^14 ^-9 3.2 0.2 3.2 1.5 8 normal @a[distance=..10]

particle minecraft:falling_water ^ ^12 ^14 3.2 0.2 3.2 1.5 4 normal @a[distance=..10]
particle minecraft:falling_water ^ ^12 ^-14 3.2 0.2 3.2 1.5 4 normal @a[distance=..10]
# On invoque 4 particules de pluie par tick
particle minecraft:falling_water ^ ^16 ^5 4 0.2 4 1 4 normal @a[distance=..10]
particle minecraft:falling_water ^ ^16 ^-5 4 0.2 4 1 4 normal @a[distance=..10]

particle minecraft:falling_water ^ ^14 ^10 4 0.2 4 1 2 normal @a[distance=..10]
particle minecraft:falling_water ^ ^14 ^-10 4 0.2 4 1 2 normal @a[distance=..10]

particle minecraft:falling_water ^ ^12 ^15 4 0.2 4 1 1 normal @a[distance=..10]
particle minecraft:falling_water ^ ^12 ^-15 4 0.2 4 1 1 normal @a[distance=..10]
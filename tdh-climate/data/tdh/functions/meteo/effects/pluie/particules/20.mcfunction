# On invoque 20 particules de pluie par tick
particle minecraft:falling_water ^ ^14 ^3 2.6 0.2 2.6 2 20 normal @a[distance=..10]
particle minecraft:falling_water ^ ^14 ^-3 2.6 0.2 2.6 2 20 normal @a[distance=..10]
particle minecraft:falling_water ^-3 ^13 ^ 2.6 0.2 2.6 2 8 normal @a[distance=..10]
particle minecraft:falling_water ^3 ^13 ^ 2.6 0.2 2.6 2 8 normal @a[distance=..10]

particle minecraft:falling_water ^ ^12 ^8 2.6 0.2 2.6 2 10 normal @a[distance=..10]
particle minecraft:falling_water ^ ^12 ^-8 2.6 0.2 2.6 2 10 normal @a[distance=..10]
particle minecraft:falling_water ^-8 ^11 ^ 2.6 0.2 2.6 2 4 normal @a[distance=..10]
particle minecraft:falling_water ^8 ^11 ^ 2.6 0.2 2.6 2 4 normal @a[distance=..10]

particle minecraft:falling_water ^ ^10 ^13 2.6 0.2 2.6 2 5 normal @a[distance=..10]
particle minecraft:falling_water ^ ^10 ^-13 2.6 0.2 2.6 2 5 normal @a[distance=..10]
particle minecraft:falling_water ^-13 ^9 ^ 2.6 0.2 2.6 2 2 normal @a[distance=..10]
particle minecraft:falling_water ^13 ^9 ^ 2.6 0.2 2.6 2 2 normal @a[distance=..10]
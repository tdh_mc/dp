# On invoque 12 particules de pluie par tick
particle minecraft:falling_water ^ ^16 ^4 3.2 0.2 3.2 1.5 12 normal @a[distance=..10]
particle minecraft:falling_water ^ ^16 ^-4 3.2 0.2 3.2 1.5 12 normal @a[distance=..10]

particle minecraft:falling_water ^ ^14 ^9 3.2 0.2 3.2 1.5 6 normal @a[distance=..10]
particle minecraft:falling_water ^ ^14 ^-9 3.2 0.2 3.2 1.5 6 normal @a[distance=..10]

particle minecraft:falling_water ^ ^12 ^14 3.2 0.2 3.2 1.5 3 normal @a[distance=..10]
particle minecraft:falling_water ^ ^12 ^-14 3.2 0.2 3.2 1.5 3 normal @a[distance=..10]
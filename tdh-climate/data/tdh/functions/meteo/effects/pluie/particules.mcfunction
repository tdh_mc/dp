# Particules de pluie (selon l'intensité)
execute if score #pluieParticules intensite matches 1..3 run function tdh:meteo/effects/pluie/particules/2
execute if score #pluieParticules intensite matches 4..5 run function tdh:meteo/effects/pluie/particules/4
execute if score #pluieParticules intensite matches 6..7 run function tdh:meteo/effects/pluie/particules/6
execute if score #pluieParticules intensite matches 8..9 run function tdh:meteo/effects/pluie/particules/8
execute if score #pluieParticules intensite matches 10..11 run function tdh:meteo/effects/pluie/particules/10
execute if score #pluieParticules intensite matches 12..14 run function tdh:meteo/effects/pluie/particules/12
execute if score #pluieParticules intensite matches 15..18 run function tdh:meteo/effects/pluie/particules/16
execute if score #pluieParticules intensite matches 19..22 run function tdh:meteo/effects/pluie/particules/20
execute if score #pluieParticules intensite matches 23..27 run function tdh:meteo/effects/pluie/particules/25
execute if score #pluieParticules intensite matches 28.. run function tdh:meteo/effects/pluie/particules/30
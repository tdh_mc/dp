# On enregistre le réglage correspondant à l'intensité actuelle de la météo dans #pluieParticules intensite
# Il permettra de déterminer combien de particules générer lorsqu'on subit des précipitations
execute if score #meteo intensite matches 0 run scoreboard players set #pluieParticules intensite 0
execute if score #meteo intensite matches 1 run scoreboard players operation #pluieParticules intensite = #pluieParticules_1 intensite
execute if score #meteo intensite matches 2 run scoreboard players operation #pluieParticules intensite = #pluieParticules_2 intensite
execute if score #meteo intensite matches 3 run scoreboard players operation #pluieParticules intensite = #pluieParticules_3 intensite
execute if score #meteo intensite matches 4 run scoreboard players operation #pluieParticules intensite = #pluieParticules_4 intensite
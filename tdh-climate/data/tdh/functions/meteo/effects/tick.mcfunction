# Tick des effets météo exécutés à chaque tick ingame

tellraw @a[tag=MeteoDetailLog] [{"text":"=== DÉBUT DU TICK DES EFFETS MÉTÉO ===","color":"gray","bold":"true"}]

# On fait fondre la neige si c'est activé, et qu'il fait beau
# On ne vérifie pas s'il fait jour car l'update auto doit se faire même de nuit
execute if score #fonteNeige on matches 1.. if score #meteo actuelle matches 1 run function tdh:meteo/effects/beau_temps/fonte_neige/tick_test
# On affiche les effets de pluie/neige s'il pleut et que l'intensité est suffisamment élevée
execute if score #pluieParticules on matches 1.. if score #meteo actuelle matches 2..3 run function tdh:meteo/effects/pluie/tick
# On augmente la hauteur de la couche de neige si c'est activé et que la météo est pluvieuse
execute if score #neige on matches 1.. if score #meteo actuelle matches 2..3 if score #meteo intensite >= #neige intensiteMin run function tdh:meteo/effects/neige/blocs/tick_test
# On affiche les éclairs supplémentaires s'il y a de l'orage pour les joueurs chez qui il y a des précipitations
# et s'ils sont activés
execute if score #orage on matches 1.. if score #meteo actuelle matches 3 run function tdh:meteo/effects/orage/tick
# On applique les effets du vent s'il y a du vent et qu'ils sont activés
execute if score #vent on matches 1.. if score #meteo intensite matches 1.. run function tdh:meteo/effects/vent/tick

tellraw @a[tag=MeteoDetailLog] [{"text":"=== FIN DU TICK DES EFFETS MÉTÉO ===","color":"gray","bold":"true"}]
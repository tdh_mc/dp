# On exécute cette fonction à la position d'un joueur, pour vérifier *sérieusement* si on risque d'entraîner
# le bug de spreadplayers. Car si on ne vérifie qu'à notre propre position, il arrive qu'on tombe sur des
# morceaux de biome terrestre (style "désert") en plein milieu de l'océan et sans blocs solides à la surface
# Par conséquent, on va tester le predicate à la fois à notre position, mais également aux 4 "coins" d'un
# carré centré sur notre position, et si 2 de ces predicates échouent on désactive spreadplayers par sécurité

scoreboard players set #meteoActuAuto temp8 5
execute positioned ~ 96 ~ if predicate tdh:allow_spreadplayers run scoreboard players remove #meteoActuAuto temp8 1
execute positioned ~-48 96 ~ if predicate tdh:allow_spreadplayers run scoreboard players remove #meteoActuAuto temp8 1
execute positioned ~48 96 ~ if predicate tdh:allow_spreadplayers run scoreboard players remove #meteoActuAuto temp8 1
execute positioned ~ 96 ~-48 if predicate tdh:allow_spreadplayers run scoreboard players remove #meteoActuAuto temp8 1
execute positioned ~ 96 ~48 if predicate tdh:allow_spreadplayers run scoreboard players remove #meteoActuAuto temp8 1

# Si on a un score de 2 ou + (2 predicates ou + n'ont pas passé), on retire le tag ;
# dans le cas contraire, on se donne le tag nous autorisant à utiliser spreadplayers
execute if score #meteoActuAuto temp8 matches ..1 run tag @a[distance=0] add AllowSpreadplayers
execute unless score #meteoActuAuto temp8 matches ..1 run tag @a[distance=0] remove AllowSpreadplayers
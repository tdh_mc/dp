execute as @s[tag=Birch] run setblock ~ ~ ~ birch_sapling
execute as @s[tag=DarkOak] run setblock ~ ~ ~ dark_oak_sapling
execute as @s[tag=Acacia] run setblock ~ ~ ~ acacia_sapling
execute as @s[tag=Jungle] run setblock ~ ~ ~ jungle_sapling
execute as @s[tag=Spruce] run setblock ~ ~ ~ spruce_sapling
execute as @s[tag=Oak] run setblock ~ ~ ~ oak_sapling
# On récupère 2 nombres aléatoires entre -400 et 400, que l'on stocke dans le mouvement de l'objet
scoreboard players set #random min -400
scoreboard players set #random max 400
function tdh:random
execute store result entity @s Motion[0] double 0.001 run scoreboard players get #random temp
function tdh:random
execute store result entity @s Motion[2] double 0.001 run scoreboard players get #random temp
# Avec un facteur 0.001, [-400,400[ devient [-0.4,0.4[ : la valeur est en blocs/tick

# On définit une vitesse verticale positive pour que l'objet s'en aille assez loin
# (entre 0.2 et 0.5 blocs/tick)
scoreboard players set #random min 200
scoreboard players set #random max 500
function tdh:random
execute store result entity @s Motion[1] double 0.001 run scoreboard players get #random temp


# On affiche des particules à l'endroit d'où on part
particle dust 0 .4 .1 .8 ~ ~ ~ .2 .1 .2 1 20
particle poof ~ ~ ~ .1 0 .1 .05 3
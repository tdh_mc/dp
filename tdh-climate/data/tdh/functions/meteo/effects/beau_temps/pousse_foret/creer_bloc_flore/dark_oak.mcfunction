# On prend un nombre aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# Selon la valeur de ce nombre, on génère une fleur correspondant au biome dark forest
execute if score #random temp matches 0..14 run setblock ~ ~ ~ dandelion
execute if score #random temp matches 15..24 run setblock ~ ~ ~ poppy
execute if score #random temp matches 25..39 run setblock ~ ~ ~ lily_of_the_valley
execute if score #random temp matches 40..59 run setblock ~ ~ ~ brown_mushroom
execute if score #random temp matches 60..69 run setblock ~ ~ ~ red_mushroom
execute if score #random temp matches 70..79 run function tdh:setblock/lilac
execute if score #random temp matches 80..89 run function tdh:setblock/rose_bush
execute if score #random temp matches 90..99 run function tdh:setblock/peony
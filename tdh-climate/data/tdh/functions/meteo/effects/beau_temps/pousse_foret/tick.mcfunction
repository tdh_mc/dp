# Tick du replantage automatique d'arbres en forêt
# Exécuté toutes les 10 secondes

# La pousse des forêts doit être activée, la météo actuelle être du beau temps, et la saison être le printemps ou l'été
execute if score #pousseForets on matches 1.. if score #meteo actuelle matches 1 if score #temps saison matches 1..2 run function tdh:meteo/effects/beau_temps/pousse_foret/update
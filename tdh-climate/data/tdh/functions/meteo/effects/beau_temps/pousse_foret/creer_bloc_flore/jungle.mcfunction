# On prend un nombre aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# Selon la valeur de ce nombre, on génère une fleur correspondant au biome jungle
execute if score #random temp matches 0..14 run setblock ~ ~ ~ dandelion
execute if score #random temp matches 15..29 run setblock ~ ~ ~ poppy
execute if score #random temp matches 30..39 run setblock ~ ~ ~ blue_orchid
execute if score #random temp matches 40..49 run setblock ~ ~ ~ azure_bluet
execute if score #random temp matches 50..53 run setblock ~ ~ ~ red_tulip
execute if score #random temp matches 54..57 run setblock ~ ~ ~ orange_tulip
execute if score #random temp matches 58..59 run setblock ~ ~ ~ pink_tulip
execute if score #random temp matches 60..73 run function tdh:setblock/lilac
execute if score #random temp matches 74..87 run function tdh:setblock/rose_bush
execute if score #random temp matches 88..99 run function tdh:setblock/peony
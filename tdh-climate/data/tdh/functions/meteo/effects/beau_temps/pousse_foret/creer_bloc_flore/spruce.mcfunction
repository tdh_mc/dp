# On prend un nombre aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# Selon la valeur de ce nombre, on génère une fleur correspondant au biome alpin / spruce
execute if score #random temp matches 0..9 run setblock ~ ~ ~ dandelion
execute if score #random temp matches 10..19 run setblock ~ ~ ~ poppy
execute if score #random temp matches 20..49 run setblock ~ ~ ~ allium
execute if score #random temp matches 50..69 run setblock ~ ~ ~ sweet_berry_bush
execute if score #random temp matches 70..79 run setblock ~ ~ ~ azure_bluet
execute if score #random temp matches 80..89 run setblock ~ ~ ~ oxeye_daisy
execute if score #random temp matches 90..99 run setblock ~ ~ ~ cornflower
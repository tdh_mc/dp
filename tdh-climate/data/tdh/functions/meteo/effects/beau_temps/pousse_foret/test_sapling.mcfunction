# On exécute cette fonction en tant qu'un NouveauSapling,
# venant d'être placé à une position aléatoire et souhaitant tester s'il se trouve bien sur un arbre

# Selon le type d'arbre sur lequel on se trouve :
execute if block ~ ~-1 ~ spruce_leaves run function tdh:meteo/effects/beau_temps/pousse_foret/set_pousse/spruce
execute if block ~ ~-1 ~ birch_leaves run function tdh:meteo/effects/beau_temps/pousse_foret/set_pousse/birch
execute if block ~ ~-1 ~ oak_leaves run function tdh:meteo/effects/beau_temps/pousse_foret/set_pousse/oak
execute if block ~ ~-1 ~ dark_oak_leaves run function tdh:meteo/effects/beau_temps/pousse_foret/set_pousse/dark_oak
execute if block ~ ~-1 ~ acacia_leaves run function tdh:meteo/effects/beau_temps/pousse_foret/set_pousse/acacia
execute if block ~ ~-1 ~ jungle_leaves run function tdh:meteo/effects/beau_temps/pousse_foret/set_pousse/jungle

# Si on n'a pas trouvé de type d'arbre correspondant, on se tue
execute as @s[tag=!Spruce,tag=!Birch,tag=!Oak,tag=!DarkOak,tag=!Acacia,tag=!Jungle] run kill @s

# Sinon, on se donne un mouvement aléatoire
execute as @s[tag=Sapling] run function tdh:meteo/effects/beau_temps/pousse_foret/mouvement_aleatoire
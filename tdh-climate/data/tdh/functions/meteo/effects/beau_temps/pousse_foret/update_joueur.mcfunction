# Tick du replantage automatique d'arbres en forêt
# Exécuté à chaque update, à la position d'un joueur se trouvant en forêt

# On invoque une entité matérialisant un sapling
execute at @e[type=item_frame,tag=SpawnMonde,limit=1] run function tdh:meteo/effects/beau_temps/pousse_foret/invoquer_entites

# On répartit aléatoirement cette ou ces entités autour du joueur
spreadplayers ~ ~ 3 34 false @e[type=item,tag=NouveauSapling]

# On traite ensuite chacun des saplings
execute as @e[type=item,tag=NouveauSapling] at @s run function tdh:meteo/effects/beau_temps/pousse_foret/test_sapling

# On retire le tag temporaire de chacun des saplings
tag @e[type=item,tag=NouveauSapling] remove NouveauSapling
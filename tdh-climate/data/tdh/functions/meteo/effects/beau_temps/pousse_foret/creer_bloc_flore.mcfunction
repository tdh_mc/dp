execute as @s[tag=Birch] run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_flore/birch
execute as @s[tag=DarkOak] run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_flore/dark_oak
execute as @s[tag=Acacia] run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_flore/acacia
execute as @s[tag=Jungle] run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_flore/jungle
execute as @s[tag=Spruce] run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_flore/spruce
execute as @s[tag=Oak] run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_flore/oak
# On génère un nombre aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On module les probabilités qui vont suivre selon le biome dans lequel on se trouve
# Dans les biomes savane, on n'a que des hautes herbes
execute as @s[tag=Acacia] run scoreboard players add #random temp 50
# Dans les biomes jungle et dark forest, on a plus de chances d'avoir des hautes herbes
execute as @s[tag=Jungle] run scoreboard players add #random temp 35
execute as @s[tag=DarkOak] run scoreboard players add #random temp 20
# Dans les biomes forest et birch forest, on a plus de chances d'avoir de l'herbe normale
execute as @s[tag=Oak] run scoreboard players remove #random temp 15
execute as @s[tag=Birch] run scoreboard players remove #random temp 25

# On a une chance sur deux de générer une herbe normale, et une chance sur deux de générer une herbe haute
execute if score #random temp matches ..49 run setblock ~ ~ ~ grass
execute if score #random temp matches 50.. run function tdh:setblock/tall_grass
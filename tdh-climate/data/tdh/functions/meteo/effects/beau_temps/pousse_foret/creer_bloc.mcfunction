# On exécute cette fonction en tant qu'un item Sapling à sa position,
# pour faire apparaître un bloc correspondant à ce sapling

# On prend un nombre aléatoire pour savoir si on fait poper de l'herbe/flore ou un vrai sapling
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random
scoreboard players operation #pousseForets temp = #random temp

# Si notre variable est inférieure à 15, on invoque un sapling
execute if score #pousseForets temp matches ..14 run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_sapling_test
# Si elle est inférieure à 70, on invoque de l'herbe
execute if score #pousseForets temp matches 15..69 run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_herbe
# Sinon, on invoque de la flore
execute if score #pousseForets temp matches 70.. run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_flore


# On affiche des particules à l'endroit où le bloc apparaît
particle poof ~ ~ ~ 0 0 0 .1 10
particle happy_villager ~ ~0.2 ~ .1 .03 .1 1 5
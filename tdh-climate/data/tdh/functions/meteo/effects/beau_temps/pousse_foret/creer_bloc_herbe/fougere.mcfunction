# On génère un nombre aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On a une chance sur deux de générer une fougère normale, et une chance sur deux de générer une fougère haute
execute if score #random temp matches ..49 run setblock ~ ~ ~ fern
execute if score #random temp matches 50.. run function tdh:setblock/large_fern
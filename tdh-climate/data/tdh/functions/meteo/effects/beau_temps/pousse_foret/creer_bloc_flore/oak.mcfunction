# On prend un nombre aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# Selon la valeur de ce nombre, on génère une fleur correspondant au biome standard (oak)
execute if score #random temp matches 0..29 run setblock ~ ~ ~ dandelion
execute if score #random temp matches 30..49 run setblock ~ ~ ~ poppy
execute if score #random temp matches 50..59 run setblock ~ ~ ~ azure_bluet
execute if score #random temp matches 60..64 run setblock ~ ~ ~ red_tulip
execute if score #random temp matches 65..69 run setblock ~ ~ ~ pink_tulip
execute if score #random temp matches 70..74 run setblock ~ ~ ~ white_tulip
execute if score #random temp matches 75..79 run setblock ~ ~ ~ orange_tulip
execute if score #random temp matches 80..89 run setblock ~ ~ ~ oxeye_daisy
execute if score #random temp matches 90..96 run setblock ~ ~ ~ cornflower
execute if score #random temp matches 97 run setblock ~ ~ ~ blue_orchid
execute if score #random temp matches 98..99 run function tdh:setblock/sunflower
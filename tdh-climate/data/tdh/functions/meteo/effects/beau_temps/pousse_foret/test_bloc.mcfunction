# On exécute cette fonction en tant qu'un item Sapling à sa position,
# pour vérifier s'il se trouve bien sur un bloc propice à la pousse d'arbres

# On vérifie si l'item est bien posé sur un bloc correct et s'il y a de l'air au-dessus de nous
execute if block ~ ~ ~ #tdh:passable if block ~ ~1 ~ #tdh:passable if block ~ ~2 ~ #tdh:passable if block ~ ~-1 ~ #tdh:sapling_base run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc

# Dans tous les cas (qu'on ait réussi à spawner ou non), on se tue après l'opération
kill @s
# Tick de la pousse des forêts
# Exécuté toutes les 10s lorsque les conditions météo sont appropriées

# On traite les saplings déjà existants
execute as @e[type=item,tag=Sapling,tag=!NouveauSapling] at @s run function tdh:meteo/effects/beau_temps/pousse_foret/test_bloc

# On exécute la fonction d'invocation à la position de chaque joueur se trouvant en forêt
execute at @a[predicate=tdh:biome/foret] run function tdh:meteo/effects/beau_temps/pousse_foret/update_joueur
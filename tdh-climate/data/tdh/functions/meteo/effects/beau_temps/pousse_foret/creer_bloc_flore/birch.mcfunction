# On prend un nombre aléatoire
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# Selon la valeur de ce nombre, on génère une fleur correspondant au biome birch forest
execute if score #random temp matches 0..19 run setblock ~ ~ ~ dandelion
execute if score #random temp matches 20..39 run setblock ~ ~ ~ poppy
execute if score #random temp matches 40..59 run setblock ~ ~ ~ lily_of_the_valley
execute if score #random temp matches 60..73 run function tdh:setblock/lilac
execute if score #random temp matches 74..87 run function tdh:setblock/rose_bush
execute if score #random temp matches 88..99 run function tdh:setblock/peony
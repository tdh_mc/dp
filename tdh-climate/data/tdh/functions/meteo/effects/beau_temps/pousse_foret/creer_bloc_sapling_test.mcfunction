# Si les 8 blocs autour de nous sont passables, on crée un sapling
scoreboard players set #pousseForets temp2 0
execute if block ~-1 ~ ~ #tdh:passable if block ~1 ~ ~ #tdh:passable if block ~ ~ ~-1 #tdh:passable if block ~ ~ ~1 #tdh:passable if block ~-1 ~ ~-1 #tdh:passable if block ~-1 ~ ~1 #tdh:passable if block ~1 ~ ~-1 #tdh:passable if block ~1 ~ ~1 #tdh:passable run scoreboard players set #pousseForets temp2 1

execute if score #pousseForets temp2 matches 1 run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_sapling
# Sinon, on crée un bloc d'herbe
execute unless score #pousseForets temp2 matches 1 run function tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc_herbe
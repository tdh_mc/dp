# Tick de la fonte de la neige
# Exécuté à la position d'un joueur chez qui il neige en temps normal,
# s'il fait beau actuellement, à chaque tick d'update de la fonte de la neige

# On invoque un item au spawn (voire + si la météo est intense)
# (on copie d'abord le nombre d'items à invoquer vers la variable de travail #neigeSpawn temp)
scoreboard players operation #neigeSpawn temp = #fonteNeige temp
execute at @e[type=item_frame,tag=SpawnMonde,limit=1] run function tdh:meteo/effects/beau_temps/fonte_neige/invoquer_entites

# On pose cet item à un endroit aléatoire autour du joueur 
# Différentes fonctions servent à prendre en compte la distance paramétrable
execute if score #fonteNeige distance matches ..89 run function tdh:meteo/effects/beau_temps/fonte_neige/spread/20_80
execute if score #fonteNeige distance matches 90.. run function tdh:meteo/effects/beau_temps/fonte_neige/spread/90_160

# Message de debug
execute store result score #neigeSpawn temp if entity @e[type=snowball,tag=SnowTemp]
tellraw @a[tag=MeteoDetailLog] [{"text":"[FonteNeige]","color":"green"},{"text":" On a invoqué ","color":"gray"},{"score":{"name":"#neigeSpawn","objective":"temp"}},{"text":" entités SnowUpdate.","color":"gray"}]

# On retire le tag temporaire des entités invoquées
tag @e[type=snowball,tag=SnowTemp] remove SnowTemp
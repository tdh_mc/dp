# Tick de la baisse de la neige
# Exécuté à la position d'un joueur chez qui il neige en temps normal par beau temps, à chaque tick
# concrètement, on va invoquer des items temporaires et les spread autour de chaque joueur

# On calcule d'abord la variable temp qui contient le nombre d'éléments à invoquer par joueur
scoreboard players operation #fonteNeige temp = #fonteNeige vitesse

# On exécute ensuite la fonction pour chaque joueur
# Pas forcément uniquement les joueurs chez qui il neige car on a pu changer de saison depuis...
# En revanche on utilise le tag AllowSpreadplayers (de meteo/effects/bug_spreadplayers) pour éviter le crash
execute if score #temps timeOfDay matches 1 at @a[tag=AllowSpreadplayers] run function tdh:meteo/effects/beau_temps/fonte_neige/tick_joueur
# Si l'actualisation automatique du monde est activée, et qu'aucun joueur n'est en ligne,
# on actualise la couche de neige autour des entités temporaires d'actualisation
execute if score #meteoActuAuto on matches 1 unless score #temps playersOnline > #meteoActuAuto playersOnline as @e[type=marker,tag=MeteoActuAuto] at @s run function tdh:meteo/effects/beau_temps/fonte_neige/tick_entite

# On vérifie si on peut faire monter la neige à chacun des endroits testés
execute as @e[type=snowball,tag=SnowUpdate] at @s positioned ~ ~-1 ~ run function tdh:meteo/effects/beau_temps/fonte_neige/test_bloc

# Message de debug
execute store result score #neigeSpawn temp if entity @e[type=snowball,tag=SnowUpdate]
tellraw @a[tag=MeteoDetailLog] [{"text":"[FonteNeige]","color":"green"},{"text":" On a tenté de faire monter la neige à ","color":"gray"},{"score":{"name":"#neigeSpawn","objective":"temp"}},{"text":" positions.","color":"gray"}]

# On élimine les entités temporaires
kill @e[type=snowball,tag=SnowUpdate]

# On calcule le tick du prochain update
execute unless score #fonteNeige frequenceUpdate matches 1 run function tdh:meteo/effects/beau_temps/fonte_neige/tick/calcul_prochain_update
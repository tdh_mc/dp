# On exécute cette fonction en tant qu'une snowball SnowUpdate à "sa" position (en fait celle du bloc en dessous)
# pour vérifier si la hauteur minimale de neige est atteinte

# On est à moyenne altitude (120-160)
# La neige fond totalement en été
execute if score #temps saison matches 2 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/0
# printemps ou automne : fait fondre la neige, mais laisse tout de même une couche de 1/3
execute if score #temps saison matches 1 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/1
execute if score #temps saison matches 3 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/3
# décembre/janvier : la neige ne fond pas
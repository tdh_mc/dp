# Tick de la fonte de la neige
# Exécuté s'il fait beau, à chaque tick,
# pour vérifier si le tick d'update a été dépassé

execute if score #temps idJour = #fonteNeige jourUpdate if score #temps currentTdhTick >= #fonteNeige tickUpdate run function tdh:meteo/effects/beau_temps/fonte_neige/tick
execute if score #temps idJour > #fonteNeige jourUpdate run function tdh:meteo/effects/beau_temps/fonte_neige/tick
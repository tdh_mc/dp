# On exécute cette fonction en tant qu'une snowball SnowUpdate à sa position,
# pour diminuer la hauteur de la neige sur laquelle elle est posée

execute if block ~ ~ ~ snow[layers=1] if block ~ ~-1 ~ snow run setblock ~ ~ ~ air
execute if block ~ ~ ~ snow[layers=2] if block ~ ~-1 ~ snow run setblock ~ ~ ~ snow[layers=1]
execute if block ~ ~ ~ snow[layers=3] run setblock ~ ~ ~ snow[layers=2]
execute if block ~ ~ ~ snow[layers=4] run setblock ~ ~ ~ snow[layers=3]
execute if block ~ ~ ~ snow[layers=5] run setblock ~ ~ ~ snow[layers=4]
execute if block ~ ~ ~ snow[layers=6] run setblock ~ ~ ~ snow[layers=5]
execute if block ~ ~ ~ snow[layers=7] run setblock ~ ~ ~ snow[layers=6]
execute if block ~ ~ ~ snow[layers=8] run setblock ~ ~ ~ snow[layers=7]
# On exécute cette fonction en tant qu'une snowball SnowUpdate à "sa" position (en fait celle du bloc en dessous),
# SI sa position contient de la neige !
# pour vérifier si la hauteur minimale de neige est atteinte

# On se donne un score spécifique selon l'altitude à laquelle on se trouve
execute store result score @s altitudeFromZero run data get entity @s Pos[1]

# Selon ce score, on vérifie si on peut retirer un niveau de neige
execute if score @s altitudeFromZero matches ..119 run function tdh:meteo/effects/beau_temps/fonte_neige/test_bloc/1
execute if score @s altitudeFromZero matches 120..159 run function tdh:meteo/effects/beau_temps/fonte_neige/test_bloc/2
execute if score @s altitudeFromZero matches 160..199 run function tdh:meteo/effects/beau_temps/fonte_neige/test_bloc/3
execute if score @s altitudeFromZero matches 200.. run function tdh:meteo/effects/beau_temps/fonte_neige/test_bloc/4

# On réinitialise notre score
scoreboard players reset @s altitudeFromZero
# On exécute cette fonction en tant qu'une snowball SnowUpdate à "sa" position (en fait celle du bloc en dessous)
# pour vérifier si la hauteur minimale de neige est atteinte

# On est à très haute altitude (200+)
# La neige ne fond jamais totalement et ne fond un peu qu'en été (laisse tout de même une couche de 7)
execute if score #temps saison matches 2 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/7
# octobre/avril : la neige ne fond pas
# On calcule le tick du prochain update
scoreboard players operation #fonteNeige jourUpdate = #temps idJour
scoreboard players operation #fonteNeige tickUpdate = #temps currentTdhTick
scoreboard players operation #fonteNeige tickUpdate += #fonteNeige frequenceUpdate
execute if score #fonteNeige tickUpdate >= #temps fullDayTicks run function tdh:meteo/effects/beau_temps/fonte_neige/tick/prochain_update_demain
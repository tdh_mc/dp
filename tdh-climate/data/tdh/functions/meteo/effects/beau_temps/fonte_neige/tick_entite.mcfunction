# C'est exactement la même chose que pour un joueur ; simplement, on veut la SUPPRIMER si on ne passe pas le tag allow_spreadplayers, car contrairement à un joueur elle ne peut pas bouger

# On aimerait bien pouvoir tester ça directement au moment où elle spawn, mais comme le chunk charge de manière asynchrone on ne peut pas le faire

execute if entity @s[tag=!AllowSpreadplayers] run function tdh:meteo/effects/actu_auto/test_entite
execute if entity @s[tag=AllowSpreadplayers] run function tdh:meteo/effects/beau_temps/fonte_neige/tick_entite2
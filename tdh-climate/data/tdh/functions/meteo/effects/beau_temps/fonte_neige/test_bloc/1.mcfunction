# On exécute cette fonction en tant qu'une snowball SnowUpdate à "sa" position (en fait celle du bloc en dessous)
# pour vérifier si la hauteur minimale de neige est atteinte

# On est à basse altitude (<120)
# La neige fond totalement du printemps à l'automne et ne s'arrête jamais totalement de fondre
execute if score #temps saison matches 1..3 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/0
# hiver : fait fondre la neige, mais laisse tout de même une couche de 2
execute unless score #temps saison matches 1..3 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/2
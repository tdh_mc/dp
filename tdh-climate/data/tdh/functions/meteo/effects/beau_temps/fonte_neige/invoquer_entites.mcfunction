# On invoque une ou plusieurs entités servant à faire descendre la neige,
# selon le paramètre #neigeSpawn temp (qui détermine le nombre d'entités invoquées)

# Cette fonction étant récursive, on se contente d'invoquer une seule entité
summon snowball ~ ~ ~ {NoGravity:1b,Tags:["SnowUpdate","SnowTemp"]}

# Ensuite, s'il y en a d'autres à invoquer, on répète la fonction
scoreboard players remove #neigeSpawn temp 1
execute if score #neigeSpawn temp matches 1.. run function tdh:meteo/effects/beau_temps/fonte_neige/invoquer_entites
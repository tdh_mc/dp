# On exécute cette fonction en tant qu'une snowball SnowUpdate à "sa" position (en fait celle du bloc en dessous)
# pour vérifier si la hauteur minimale de neige est atteinte

# On est à haute altitude (160-200)
# La neige ne fond pas totalement en été ; il reste une couche de 1
execute if score #temps saison matches 2 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/1
# printemps ou automne : fait fondre la neige, mais laisse tout de même une couche de 5/7
execute if score #temps saison matches 1 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/5
execute if score #temps saison matches 3 run function tdh:meteo/effects/beau_temps/fonte_neige/succes/7
# novembre/février : la neige ne fond pas
# On invoque un objet temporaire au spawn pour déterminer la position de l'éclair
execute at @e[type=item_frame,tag=SpawnMonde,limit=1] run summon item ~ ~ ~ {Tags:["PositionEclair"],Item:{id:"polished_blackstone_button",Count:1b},PickupDelay:32767s,Age:5999s}

# On le place à une position aléatoire autour du joueur, dans une direction aléatoire
# (entre 0 et 80 blocs de distance, avec un max de probabilités à 30 blocs de distance)
execute rotated as @e[type=#tdh:random,sort=random,limit=1] positioned ^ ^ ^30 run spreadplayers ~ ~ 1 50 false @e[type=item,tag=PositionEclair]

# On invoque un éclair à la position de l'item (sauf s'il risque de détruire une entité protégée)
execute at @e[type=item,tag=PositionEclair] unless entity @e[type=#tdh:immunite_foudre,distance=..5] run function tdh:meteo/effects/orage/eclair/invoquer_eclair
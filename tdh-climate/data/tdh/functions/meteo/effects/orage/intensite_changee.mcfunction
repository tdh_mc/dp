# On enregistre le réglage correspondant à l'intensité actuelle de la météo dans #orage probaChgmt
# Il permettra de déterminer quelles sont les chances d'avoir un éclair (sur 10'000)
execute if score #meteo intensite matches 0 run scoreboard players set #orage probaChgmt 0
execute if score #meteo intensite matches 1 run scoreboard players operation #orage probaChgmt = #orage_1 probaChgmt
execute if score #meteo intensite matches 2 run scoreboard players operation #orage probaChgmt = #orage_2 probaChgmt
execute if score #meteo intensite matches 3 run scoreboard players operation #orage probaChgmt = #orage_3 probaChgmt
execute if score #meteo intensite matches 4 run scoreboard players operation #orage probaChgmt = #orage_4 probaChgmt
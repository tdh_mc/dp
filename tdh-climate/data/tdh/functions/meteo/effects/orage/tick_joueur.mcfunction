# Tick des effets d'orage
# Exécuté à chaque tick où il y a de l'orage (meteo actuelle = 3) à la position de chaque joueur chez qui il pleut ou neige

# Éclairs supplémentaires (selon l'intensité)

# On prend un nombre aléatoire
function tdh:random

# Si le nombre aléatoire est plus petit que nos chances d'avoir un éclair, on invoque un éclair
execute if score #random temp < #orage probaChgmt run function tdh:meteo/effects/orage/eclair
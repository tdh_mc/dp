# Tick des effets d'orage
# Exécuté à chaque tick où il y a de l'orage (meteo actuelle = 3) à la position de chaque joueur chez qui il pleut ou neige

# Éclairs supplémentaires (selon l'intensité)

# On enregistre les paramètres du nombre aléatoire (entre 0 et 9999)
scoreboard players set #random min 0
scoreboard players set #random max 10000

# Pour chaque joueur se trouvant dans un biome approprié, on fait un test
# En raison de MC-186963 (https://bugs.mojang.com/browse/MC-186963), on désactive ce test si on est dans un biome océan non gelé
execute at @a[scores={biomeType=1..3}] if predicate tdh:allow_spreadplayers run function tdh:meteo/effects/orage/tick_joueur
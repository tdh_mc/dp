# On désactive l'écoulement "naturel" de la météo
gamerule doWeatherCycle false

# On désactive tous les packs saisonniers de la météo à l'exception de l'éventuel pack actuellement choisi
execute unless score #saison actuelle matches ..0 run datapack disable "file/tdh-climate-s01"
execute unless score #saison actuelle matches 1 run datapack disable "file/tdh-climate-s02"
execute unless score #saison actuelle matches 2 run datapack disable "file/tdh-climate-s03"
execute unless score #saison actuelle matches 3 run datapack disable "file/tdh-climate-s04"
execute unless score #saison actuelle matches 4 run datapack disable "file/tdh-climate-s05"
execute unless score #saison actuelle matches 5 run datapack disable "file/tdh-climate-s06"
execute unless score #saison actuelle matches 6 run datapack disable "file/tdh-climate-s07"
execute unless score #saison actuelle matches 7 run datapack disable "file/tdh-climate-s08"
execute unless score #saison actuelle matches 8 run datapack disable "file/tdh-climate-s09"
execute unless score #saison actuelle matches 9 run datapack disable "file/tdh-climate-s10"
execute unless score #saison actuelle matches 10 run datapack disable "file/tdh-climate-s11"
execute unless score #saison actuelle matches 11.. run datapack disable "file/tdh-climate-s12"

# Si la météo n'est pas activée actuellement,
# on lance la séquence de démarrage (génération de prévisions + définition de variables)
execute unless score #meteo on matches 1 run function tdh:meteo/start
# On crée toutes les variables du module
function tdh:meteo/init/variables
# Si le storage ne contient pas déjà les définitions météorologiques, on les crée
function tdh:meteo/init/storage
# S'il n'y a pas encore de cabine au spawn, on la crée
execute unless entity @e[type=item_frame,tag=SpawnMonde] run function tdh:meteo/init/spawn

# Si la météo n'est pas activée actuellement,
# on lance la séquence de démarrage (génération de prévisions + définition de variables)
execute unless score #meteo on matches 1 run function tdh:meteo/start
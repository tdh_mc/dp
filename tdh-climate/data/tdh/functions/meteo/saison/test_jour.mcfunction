# On exécute cette fonction quotidiennement,
# pour calculer le temps écoulé depuis le solstice
# et éventuellement modifier le datapack climatique actuellement chargé

# On calcule dans une variable temporaire le temps écoulé depuis le solstice d'hiver
scoreboard players operation #saison suivante = #temps jourAnnee
scoreboard players operation #saison suivante -= #solsticeHiver jourAnnee
scoreboard players operation #saison suivante %= #temps jourAnneeMax

# On divise ce temps x le nombre total d'intervalles saisonniers par le nombre de jours dans l'année (365)
# pour obtenir notre ID de datapack saisonnier entre 0 et (le nombre total d'intervalles saisonniers - 1)
# (ce n'est pas à proprement parler le "nombre de saisons" mais le nombre de datapacks actifs dans une année ; par défaut 12 (TODO: inclure cette variable dans la config))
scoreboard players operation #saison suivante *= #saison nombre
scoreboard players operation #saison suivante /= #temps jourAnneeMax

# Si le nouvel ID calculé est différent du précédent, on l'enregistre
# On passera aussi cette fonction si on n'a jamais enregistré d'ID (si #saison actuelle n'existe pas)
execute unless score #saison suivante = #saison actuelle run function tdh:meteo/saison/prochain_pack
# On exécute cette fonction lorsqu'on doit changer le datapack saisonnier chargé
# Le pack précédent (s'il existe) est stocké dans #saison actuelle
# Le nouveau pack à charger est stocké dans #saison suivante

# On modifie la saison actuelle
scoreboard players operation #saison actuelle = #saison suivante

# On exécute l'opération en 2 fois : on active d'abord le nouveau pack juste après tdh-climate
# (donc AVANT l'ancien pack, de sorte à ne rien changer),
# puis on désactive l'ancien pack avec un schedule (plusieurs secondes après pour éviter les cafouillages)
# (lorsqu'on exécute plusieurs commandes /datapack enable ou /datapack disable trop rapprochées,
# il arrive que certaines opérations ne soient juste pas prises en compte...)
execute if score #saison actuelle matches ..0 run function tdh:meteo/saison/prochain_pack/activer/01
execute if score #saison actuelle matches 1 run function tdh:meteo/saison/prochain_pack/activer/02
execute if score #saison actuelle matches 2 run function tdh:meteo/saison/prochain_pack/activer/03
execute if score #saison actuelle matches 3 run function tdh:meteo/saison/prochain_pack/activer/04
execute if score #saison actuelle matches 4 run function tdh:meteo/saison/prochain_pack/activer/05
execute if score #saison actuelle matches 5 run function tdh:meteo/saison/prochain_pack/activer/06
execute if score #saison actuelle matches 6 run function tdh:meteo/saison/prochain_pack/activer/07
execute if score #saison actuelle matches 7 run function tdh:meteo/saison/prochain_pack/activer/08
execute if score #saison actuelle matches 8 run function tdh:meteo/saison/prochain_pack/activer/09
execute if score #saison actuelle matches 9 run function tdh:meteo/saison/prochain_pack/activer/10
execute if score #saison actuelle matches 10 run function tdh:meteo/saison/prochain_pack/activer/11
execute if score #saison actuelle matches 11.. run function tdh:meteo/saison/prochain_pack/activer/12
# En fait pas besoin de /schedule la désactivation,
# puisqu'on désactive automatiquement tous les packs qui ne correspondent PAS à la #saison actuelle,
# et ce à chaque reload (on est obligés sinon tous les packs se réactivent lors d'un reboot serveur...)
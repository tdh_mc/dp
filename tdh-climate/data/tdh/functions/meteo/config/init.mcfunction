# On démarre la config et se donne donc le tag Config
tag @s add Config
tag @s add ConfigMeteo

# On active l'objectif de configuration à trigger (9999 = Choix d'un paramètre à modifier)
scoreboard players set @s configID 9999
scoreboard players set @s config -2000000000
scoreboard players enable @s config
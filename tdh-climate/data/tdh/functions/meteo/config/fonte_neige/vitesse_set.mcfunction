## ENREGISTREMENT DE LA VITESSE D'ACTUALISATION DE LA FONTE DE LA NEIGE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (pas de min/max)
execute if score @s config matches 1.. run scoreboard players operation #fonteNeige vitesse = @s config

# On affiche un message de confirmation
tellraw @s [{"text":"La fonte de la neige actualisera désormais ","color":"gray"},{"score":{"name":"#fonteNeige","objective":"vitesse"},"color":"yellow","extra":[{"text":" blocs"}]},{"text":" à chaque update."}]
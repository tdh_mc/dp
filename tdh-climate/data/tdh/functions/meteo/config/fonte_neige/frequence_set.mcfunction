## ENREGISTREMENT DE L'INTERVALLE D'ACTUALISATION DE LA FONTE DE LA NEIGE
# Il est indiqué par la variable config

# On définit la valeur de la variable
execute if score @s config matches 1.. run scoreboard players operation #fonteNeige frequenceUpdate = @s config

# On affiche un message de confirmation
execute if score #fonteNeige frequenceUpdate matches 1 run tellraw @s [{"text":"La fonte de la neige sera désormais updatée à ","color":"gray"},{"text":"chaque tick","color":"yellow"},{"text":"."}]
execute unless score #fonteNeige frequenceUpdate matches 1 run tellraw @s [{"text":"La fonte de la neige sera désormais updatée tous les ","color":"gray"},{"score":{"name":"#fonteNeige","objective":"frequenceUpdate"},"color":"yellow","extra":[{"text":" ticks"}]},{"text":"."}]
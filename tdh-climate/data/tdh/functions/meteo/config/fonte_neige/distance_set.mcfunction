## ENREGISTREMENT DE LA DISTANCE D'ACTUALISATION DE LA FONTE DE LA NEIGE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (minimum 16, maximum 170)
execute if score @s config matches 16..170 run scoreboard players operation #fonteNeige distance = @s config
execute if score @s config matches 0..15 run scoreboard players set #fonteNeige distance 16
execute if score @s config matches 171.. run scoreboard players set #fonteNeige distance 170

# On affiche un message de confirmation
tellraw @s [{"text":"La distance d'actualisation de la fonte de la neige est désormais de ","color":"gray"},{"score":{"name":"#fonteNeige","objective":"distance"},"color":"yellow","extra":[{"text":" blocs"}]},{"text":"."}]
## SÉLECTION DU NOMBRE DE PARTICULES DE PLUIE PAR TICK À L'INTENSITÉ 4

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config -2000000000
scoreboard players enable @s config


tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE","color":"gold"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le nombre de particules de pluie par tick à l'intensité 4 :","color":"gray"}]
tellraw @s [{"text":"","color":"gray"},{"text":"2","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 2"},"hoverEvent":{"action":"show_text","value":"2 particules par tick"}},{"text":" ● "},{"text":"4","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 4"},"hoverEvent":{"action":"show_text","value":"4 particules par tick"}},{"text":" ● "},{"text":"6","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 6"},"hoverEvent":{"action":"show_text","value":"6 particules par tick"}},{"text":" ● "},{"text":"8","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 8"},"hoverEvent":{"action":"show_text","value":"8 particules par tick"}},{"text":" ● "},{"text":"10","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 10"},"hoverEvent":{"action":"show_text","value":"10 particules par tick"}},{"text":" ● "},{"text":"12","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 12"},"hoverEvent":{"action":"show_text","value":"12 particules par tick"}}]
tellraw @s [{"text":"","color":"gray"},{"text":"16","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 16"},"hoverEvent":{"action":"show_text","value":"16 particules par tick"}},{"text":" ● "},{"text":"20","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 20"},"hoverEvent":{"action":"show_text","value":"20 particules par tick"}},{"text":" ● "},{"text":"25","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 25"},"hoverEvent":{"action":"show_text","value":"25 particules par tick"}},{"text":" ● "},{"text":"30","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 30"},"hoverEvent":{"action":"show_text","value":"30 particules par tick"}}]

#tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","hoverEvent":{"action":"show_text","value":[{"text":""}]},"clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -1999999999"}},{"score":{"name":"#pluieParticules_4","objective":"intensite"}},{"text":")"}]
## ENREGISTREMENT DU NOMBRE DE PARTICULES DE PLUIE PAR TICK À L'INTENSITÉ 3
# Il est indiqué par la variable config

# On définit la valeur de la variable
execute if score @s config matches 1..50 run scoreboard players operation #pluieParticules_3 intensite = @s config
execute if score @s config matches 51.. run scoreboard players set #pluieParticules_3 intensite 50

# On affiche un message de confirmation
tellraw @s [{"text":"À l'intensité 3, il y aura désormais ","color":"gray"},{"score":{"name":"#pluieParticules_3","objective":"intensite"},"color":"yellow","extra":[{"text":" particules"}]},{"text":" de pluie par tick."}]

# On recalcule le nombre de particules actuel
function tdh:meteo/effects/pluie/intensite_changee
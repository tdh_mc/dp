# On prend le scénario situé tout à la fin de la liste, et on le place au début
data modify storage tdh:meteo index.scenario prepend from storage tdh:meteo index.scenario[-1]
data remove storage tdh:meteo index.scenario[-1]

# On actualise l'affichage du configurateur
function tdh:meteo/config/scenarios
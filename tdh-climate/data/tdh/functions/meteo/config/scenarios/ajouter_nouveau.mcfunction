# On ajoute un scénario en tête de liste, avec des valeurs par défaut
data modify storage tdh:meteo index.scenario prepend value {nom:"Nouveau scénario",poids:{calme:{pluie:{min:0,max:0},orage:{min:0,max:0},intensiteMin:{min:0,max:0},intensiteMax:{min:0,max:0}},pluie:{calme:{min:0,max:0},orage:{min:0,max:0},intensiteMin:{min:0,max:0},intensiteMax:{min:0,max:0}},orage:{calme:{min:0,max:0},pluie:{min:0,max:0},intensiteMin:{min:0,max:0},intensiteMax:{min:0,max:0}}}}

# On réaffiche le menu de configuration
function tdh:meteo/config/scenarios
# On prend le scénario situé au début de la liste, et on le renvoie à la fin
data modify storage tdh:meteo index.scenario append from storage tdh:meteo index.scenario[0]
data remove storage tdh:meteo index.scenario[0]

# On actualise l'affichage du configurateur
function tdh:meteo/config/scenarios
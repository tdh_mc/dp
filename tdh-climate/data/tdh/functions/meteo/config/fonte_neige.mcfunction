# Configurateur du climat
# Sous-menu de la fonte de la neige

# On démarre la config
function tdh:meteo/config/init

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE / ","color":"gold"},{"text":"Fonte de la neige","bold":"true"},{"text":"\n——————————————————————————","color":"gray"}]

# Distance d'actualisation (31100)
tellraw @s [{"text":"Distance d'actualisation : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Distance la plus éloignée (d'un joueur ou d'un chunk forceloadé) à laquelle la neige peut fondre.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 31100"}},{"score":{"name":"#fonteNeige","objective":"distance"},"color":"gold","extra":[{"text":" blocs"}]}]
# Intervalle d'actualisation (31200)
execute if score #fonteNeige frequenceUpdate matches 1 run tellraw @s [{"text":"Intervalle d'actualisation : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de ticks où le datapack essaiera de faire fondre de la neige lorsqu'il fait beau.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 31200"}},{"text":"chaque tick","color":"gold"}]
execute unless score #fonteNeige frequenceUpdate matches 1 run tellraw @s [{"text":"Intervalle d'actualisation : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de ticks où le datapack essaiera de faire fondre de la neige lorsqu'il fait beau.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 31200"}},{"text":"1 tick sur ","color":"gold","extra":[{"score":{"name":"#fonteNeige","objective":"frequenceUpdate"}}]}]
# Vitesse d'actualisation (31300)
tellraw @s [{"text":"Vitesse de la fonte : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de positions autour de chaque joueur où le datapack essaiera de faire fondre la neige, à chaque update.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 31300"}},{"score":{"name":"#fonteNeige","objective":"vitesse"},"color":"gold"}]

# Paramètres génériques
tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"Réinitialiser les paramètres","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Tous les paramètres de la fonte de la neige seront définis à leurs valeurs par défaut.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/fonte_neige/reset"}},{"text":" • ","color":"gray"},{"text":"Retour au menu racine","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Affiche le menu principal de tdh-climate.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/_config"}}]
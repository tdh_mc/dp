# Configurateur du climat
# Sous-menu de l'actualisation automatique

# On démarre la config
function tdh:meteo/config/init

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE / ","color":"gold"},{"text":"Actualisation auto","bold":"true"},{"text":"\n——————————————————————————","color":"gray"}]

# Nombre maximal de joueurs (50300)
tellraw @s [{"text":"Nombre maximal de joueurs en ligne : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de joueurs au-delà duquel l'actualisation automatique des chunks s'interrompra.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 50300"}},{"score":{"name":"#meteoActuAuto","objective":"playersOnline"},"color":"gold"}]
# Nombre maximal d'entités (50100)
tellraw @s [{"text":"Nombre maximal d'entités : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de positions différentes du monde qu'on essaiera de charger en même temps au maximum.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 50100"}},{"score":{"name":"#meteoActuAuto","objective":"nombre"},"color":"gold"}]
# Durée de vie d'une entité (50200)
scoreboard players set #store currentHour 600
scoreboard players operation #store currentHour *= #meteoActuAuto duree
function tdh:store/realtime
tellraw @s [{"text":"Durée de vie d'une entité : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre d'updates du système d'actualisation automatique avant de détruire une entité et passer à une autre position.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 50200"}},{"storage":"tdh:store","nbt":"realtime","interpret":"true","color":"gold"}]
# Bornes X (50400/410)
tellraw @s [{"text":"La position X peut être comprise entre ","color":"gray"},{"score":{"name":"#meteoActuAuto","objective":"xMin"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Le minimum des positions aléatoires sur l'axe des X.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 50400"}},{"text":" et "},{"score":{"name":"#meteoActuAuto","objective":"xMax"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Le maximum des positions aléatoires sur l'axe des X.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 50410"}},{"text":"."}]
# Bornes Z (50500/510)
tellraw @s [{"text":"La position Z peut être comprise entre ","color":"gray"},{"score":{"name":"#meteoActuAuto","objective":"zMin"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Le minimum des positions aléatoires sur l'axe des Z.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 50500"}},{"text":" et "},{"score":{"name":"#meteoActuAuto","objective":"zMax"},"color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Le maximum des positions aléatoires sur l'axe des Z.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 50510"}},{"text":"."}]

# Paramètres génériques
tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"Réinitialiser les paramètres","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Tous les paramètres de l'actualisation automatique seront définis à leurs valeurs par défaut.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/actu_auto/reset"}},{"text":" • ","color":"gray"},{"text":"Retour au menu racine","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Affiche le menu principal de tdh-climate.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/_config"}}]
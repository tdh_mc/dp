# Configurateur du climat
# Sous-menu des particules de la pluie

# On démarre la config
function tdh:meteo/config/init

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE / ","color":"gold"},{"text":"Particules de pluie/neige","bold":"true"},{"text":"\n——————————————————————————","color":"gray"}]

# Nombre de particules pour chaque niveau d'intensité (32101/2/3/4)
tellraw @s [{"text":"Intensité 1 : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de particules de précipitations spawnées par tick lorsque l'intensité météo est de 1.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 32101"}},{"score":{"name":"#pluieParticules_1","objective":"intensite"},"color":"gold"}]
tellraw @s [{"text":"Intensité 2 : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de particules de précipitations spawnées par tick lorsque l'intensité météo est de 2.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 32102"}},{"score":{"name":"#pluieParticules_2","objective":"intensite"},"color":"gold"}]
tellraw @s [{"text":"Intensité 3 : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de particules de précipitations spawnées par tick lorsque l'intensité météo est de 3.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 32103"}},{"score":{"name":"#pluieParticules_3","objective":"intensite"},"color":"gold"}]
tellraw @s [{"text":"Intensité 4 : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de particules de précipitations spawnées par tick lorsque l'intensité météo est de 4.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 32104"}},{"score":{"name":"#pluieParticules_4","objective":"intensite"},"color":"gold"}]

# Paramètres génériques
tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"Réinitialiser les paramètres","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Tous les paramètres des particules de pluie/neige seront définis à leurs valeurs par défaut.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/particules_pluie/reset"}},{"text":" • ","color":"gray"},{"text":"Retour au menu racine","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Affiche le menu principal de tdh-climate.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/_config"}}]
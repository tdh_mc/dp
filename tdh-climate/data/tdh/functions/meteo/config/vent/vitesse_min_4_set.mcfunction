## ENREGISTREMENT DE LA VITESSE MINIMALE DU VENT À L'INTENSITÉ 4
# Elle est indiquée par la variable config

# On définit la valeur de la variable (pas de min/max)
scoreboard players operation #vent_4 vitesseMin = @s config
# On recalcule la valeur du maximum de l'intensité inférieure
scoreboard players operation #vent_3 vitesseMax = @s config
scoreboard players remove #vent_3 1

# On affiche un message de confirmation
tellraw @s [{"text":"Le vent sera désormais d'au-moins ","color":"gray"},{"score":{"name":"#vent_4","objective":"vitesseMin"},"color":"yellow","extra":[{"text":" m/s"}]},{"text":" à l'intensité 4."}]
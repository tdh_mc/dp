## ENREGISTREMENT DE LA VITESSE MINIMALE DU VENT À L'INTENSITÉ 2
# Elle est indiquée par la variable config

# On définit la valeur de la variable (pas de min/max)
scoreboard players operation #vent_2 vitesseMin = @s config
# On recalcule la valeur du maximum de l'intensité inférieure
scoreboard players operation #vent_1 vitesseMax = @s config
scoreboard players remove #vent_1 1

# On affiche un message de confirmation
tellraw @s [{"text":"Le vent sera désormais d'au-moins ","color":"gray"},{"score":{"name":"#vent_2","objective":"vitesseMin"},"color":"yellow","extra":[{"text":" m/s"}]},{"text":" à l'intensité 2."}]
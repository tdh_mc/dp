## ENREGISTREMENT DE LA ROTATION MAXIMALE DU VENT PAR UPDATE À L'INTENSITÉ 4
# Elle est indiquée par la variable config

# On définit la valeur de la variable (min 0, max 90)
execute if score @s config matches ..90 run scoreboard players operation #vent_4 rotationMax = @s config
execute if score @s config matches 91.. run scoreboard players set #vent_4 rotationMax 90

# On affiche un message de confirmation
tellraw @s [{"text":"Le vent pourra tourner au maximum de ","color":"gray"},{"score":{"name":"#vent_4","objective":"rotationMax"},"color":"yellow","extra":[{"text":"°"}]},{"text":" par update à l'intensité 4."}]
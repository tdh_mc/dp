scoreboard players set #vent vitesse 0
# Intensité 0 (vent 0m/s, 3mn par check)
scoreboard players set #vent_0 vitesseMin 0
scoreboard players set #vent_0 vitesseMax 0
scoreboard players set #vent_0 dureeMin 3600
scoreboard players set #vent_0 dureeMax 3600
scoreboard players set #vent_0 rotationMax 0
# Intensité 1 (vent 1-6m/s, 25-100s par check pour rotation <= 4°)
scoreboard players set #vent_1 vitesseMin 1
scoreboard players set #vent_1 vitesseMax 6
scoreboard players set #vent_1 dureeMin 500
scoreboard players set #vent_1 dureeMax 2000
scoreboard players set #vent_1 rotationMax 4
# Intensité 2 (vent 7-13m/s, 15-40s par check pour rotation <= 6°)
scoreboard players set #vent_2 vitesseMin 7
scoreboard players set #vent_2 vitesseMax 13
scoreboard players set #vent_2 dureeMin 300
scoreboard players set #vent_2 dureeMax 800
scoreboard players set #vent_2 rotationMax 6
# Intensité 3 (vent 14-24m/s, 10-25s par check pour rotation <= 10°)
scoreboard players set #vent_3 vitesseMin 14
scoreboard players set #vent_3 vitesseMax 24
scoreboard players set #vent_3 dureeMin 200
scoreboard players set #vent_3 dureeMax 500
scoreboard players set #vent_3 rotationMax 10
# Intensité 4 (vent 25-40m/s, 3-15s par check pour rotation <= 15°)
scoreboard players set #vent_4 vitesseMin 25
scoreboard players set #vent_4 vitesseMax 40
scoreboard players set #vent_4 dureeMin 60
scoreboard players set #vent_4 dureeMax 300
scoreboard players set #vent_4 rotationMax 15

function tdh:meteo/effects/vent/intensite_changee
function tdh:meteo/config/vent
## ENREGISTREMENT DE LA VITESSE MINIMALE DU VENT À L'INTENSITÉ 4
# Elle est indiquée par la variable config

# On définit la valeur de la variable (pas de min/max)
scoreboard players operation #vent_4 vitesseMax = @s config

# On affiche un message de confirmation
tellraw @s [{"text":"Le maximum global de la vitesse du vent est désormais de ","color":"gray"},{"score":{"name":"#vent_4","objective":"vitesseMax"},"color":"yellow","extra":[{"text":" m/s"}]},{"text":"."}]
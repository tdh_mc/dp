# Cette fonction est exécutée par un administrateur
# avec configID = 9999 (menu principal) et config = le menu qu'il veut afficher

# Selon le menu qu'on veut afficher

# - Options des scénarios (10000) :
execute as @s[scores={config=10000}] run function tdh:meteo/config/scenarios
# - Options des mois (20000) :
execute as @s[scores={config=20000}] run function tdh:meteo/config/mois

# Accumulation de la neige (30xxx)
# - Activer/désactiver (30010/30011)
execute as @s[scores={config=30010}] run function tdh:meteo/config/chute_neige/enable
execute as @s[scores={config=30011}] run function tdh:meteo/config/chute_neige/disable
# - Distance d'actualisation (30100)
execute as @s[scores={config=30100}] run function tdh:meteo/config/chute_neige/distance_select
# - Intervalle d'actualisation (30200)
execute as @s[scores={config=30200}] run function tdh:meteo/config/chute_neige/frequence_select
# - Vitesse d'actualisation (30300)
execute as @s[scores={config=30300}] run function tdh:meteo/config/chute_neige/vitesse_select
# - Intensité minimale (30400)
execute as @s[scores={config=30400}] run function tdh:meteo/config/chute_neige/intensite_min_select

# Fonte de la neige (31xxx)
# - Activer/désactiver (31010/31011)
execute as @s[scores={config=31010}] run function tdh:meteo/config/fonte_neige/enable
execute as @s[scores={config=31011}] run function tdh:meteo/config/fonte_neige/disable
# - Distance d'actualisation (31100)
execute as @s[scores={config=31100}] run function tdh:meteo/config/fonte_neige/distance_select
# - Intervalle d'actualisation (31200)
execute as @s[scores={config=31200}] run function tdh:meteo/config/fonte_neige/frequence_select
# - Vitesse d'actualisation (31300)
execute as @s[scores={config=31300}] run function tdh:meteo/config/fonte_neige/vitesse_select

# Particules de pluie/neige (32xxx)
# - Activer/désactiver (32010/32011)
execute as @s[scores={config=32010}] run function tdh:meteo/config/particules_pluie/enable
execute as @s[scores={config=32011}] run function tdh:meteo/config/particules_pluie/disable
# - Particules à l'intensité 1/2/3/4 (32101/2/3/4)
execute as @s[scores={config=32101}] run function tdh:meteo/config/particules_pluie/particules_1_select
execute as @s[scores={config=32102}] run function tdh:meteo/config/particules_pluie/particules_2_select
execute as @s[scores={config=32103}] run function tdh:meteo/config/particules_pluie/particules_3_select
execute as @s[scores={config=32104}] run function tdh:meteo/config/particules_pluie/particules_4_select

# Éclairs selon l'intensité (33xxx)
# - Activer/désactiver (33010/33011)
execute as @s[scores={config=33010}] run function tdh:meteo/config/eclairs/enable
execute as @s[scores={config=33011}] run function tdh:meteo/config/eclairs/disable
# - Probabilité à l'intensité 1/2/3/4 (33101/2/3/4)
execute as @s[scores={config=33101}] run function tdh:meteo/config/eclairs/frequence_1_select
execute as @s[scores={config=33102}] run function tdh:meteo/config/eclairs/frequence_2_select
execute as @s[scores={config=33103}] run function tdh:meteo/config/eclairs/frequence_3_select
execute as @s[scores={config=33104}] run function tdh:meteo/config/eclairs/frequence_4_select

# Vent (34xxx)
# - Activer/désactiver (34010/34011)
execute as @s[scores={config=34010}] run function tdh:meteo/config/vent/enable
execute as @s[scores={config=34011}] run function tdh:meteo/config/vent/disable
# - Minimums de vitesse du vent aux intensités 1/2/3/4 (34101/2/3/4)
execute as @s[scores={config=34101}] run function tdh:meteo/config/vent/vitesse_min_1_select
execute as @s[scores={config=34102}] run function tdh:meteo/config/vent/vitesse_min_2_select
execute as @s[scores={config=34103}] run function tdh:meteo/config/vent/vitesse_min_3_select
execute as @s[scores={config=34104}] run function tdh:meteo/config/vent/vitesse_min_4_select
# - Maximum de vitesse du vent à l'intensité 4 (34105)
execute as @s[scores={config=34105}] run function tdh:meteo/config/vent/vitesse_max_4_select
# - Rotation maximale par update à l'intensité 1/2/3/4 (34201/2/3/4)
execute as @s[scores={config=34201}] run function tdh:meteo/config/vent/rotation_max_1_select
execute as @s[scores={config=34202}] run function tdh:meteo/config/vent/rotation_max_2_select
execute as @s[scores={config=34203}] run function tdh:meteo/config/vent/rotation_max_3_select
execute as @s[scores={config=34204}] run function tdh:meteo/config/vent/rotation_max_4_select

# Propagation des arbres (40xxx)
# - Activer/désactiver (40010/40011)
execute as @s[scores={config=40010}] run function tdh:meteo/config/pousse_foret/enable
execute as @s[scores={config=40011}] run function tdh:meteo/config/pousse_foret/disable

# Actualisation automatique (50xxx)
# - Activer/désactiver (50010/50011)
execute as @s[scores={config=50010}] run function tdh:meteo/config/actu_auto/enable
execute as @s[scores={config=50011}] run function tdh:meteo/config/actu_auto/disable
# - Nombre maximal d'entités invoquées (50100)
execute as @s[scores={config=50100}] run function tdh:meteo/config/actu_auto/max_entites_select
# - Durée de vie d'une entité d'actualisation (50200)
execute as @s[scores={config=50200}] run function tdh:meteo/config/actu_auto/duree_entite_select
# - Nombre maximum de joueurs en ligne (50300)
execute as @s[scores={config=50300}] run function tdh:meteo/config/actu_auto/max_joueurs_select
# - Bornes X et Z d'actualisation (50400/410 / 50500/510)
execute as @s[scores={config=50400}] run function tdh:meteo/config/actu_auto/x_min_select
execute as @s[scores={config=50410}] run function tdh:meteo/config/actu_auto/x_max_select
execute as @s[scores={config=50500}] run function tdh:meteo/config/actu_auto/z_min_select
execute as @s[scores={config=50510}] run function tdh:meteo/config/actu_auto/z_max_select

# - Quitter le configurateur (-1)
execute as @s[scores={config=-1}] run function tdh:config/end
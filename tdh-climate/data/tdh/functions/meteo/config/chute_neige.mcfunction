# Configurateur du climat
# Sous-menu des chutes de neige

# On démarre la config
function tdh:meteo/config/init

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE / ","color":"gold"},{"text":"Chutes de neige","bold":"true"},{"text":"\n——————————————————————————","color":"gray"}]

# Distance d'actualisation (30100)
tellraw @s [{"text":"Distance d'actualisation : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Distance la plus éloignée d'un joueur à laquelle la neige peut s'accumuler.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 30100"}},{"score":{"name":"#neige","objective":"distance"},"color":"gold","extra":[{"text":" blocs"}]}]
# Intervalle d'actualisation (30200)
execute if score #neige frequenceUpdate matches 1 run tellraw @s [{"text":"Intervalle d'actualisation : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de ticks où le datapack essaiera de poser de la neige lorsqu'il neige.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 30200"}},{"text":"chaque tick","color":"gold"}]
execute unless score #neige frequenceUpdate matches 1 run tellraw @s [{"text":"Intervalle d'actualisation : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de ticks où le datapack essaiera de poser de la neige lorsqu'il neige.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 30200"}},{"text":"1 tick sur ","color":"gold","extra":[{"score":{"name":"#neige","objective":"frequenceUpdate"}}]}]
# Vitesse d'actualisation (30300)
tellraw @s [{"text":"Vitesse de l'élévation : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Nombre de positions autour de chaque joueur où le datapack essaiera de faire monter la neige, à chaque update.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 30300"}},{"score":{"name":"#neige","objective":"vitesse"},"color":"gold"}]
# Intensité minimale (30400)
tellraw @s [{"text":"Intensité minimale : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Minimum d'intensité de la météo, au-dessous duquel la neige ne s'accumule pas quand il neige.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 30400"}},{"score":{"name":"#neige","objective":"intensiteMin"},"color":"gold"}]

# Paramètres génériques
tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"Réinitialiser les paramètres","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Tous les paramètres des chutes de neige seront définis à leurs valeurs par défaut.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/chute_neige/reset"}},{"text":" • ","color":"gray"},{"text":"Retour au menu racine","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Affiche le menu principal de tdh-climate.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/_config"}}]
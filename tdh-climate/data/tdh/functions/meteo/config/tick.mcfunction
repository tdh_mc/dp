# On tick régulièrement pour lancer les sous-fonctions de prise en compte de la configuration
# Le joueur a une variable configID qui sert à stocker la variable qu'il config, et une variable config qu'il trigger pour communiquer la valeur désirée

# Cette fonction est à destination des administrateurs, donc on peut utiliser execute AS au lieu de AT

# Si on est à l'écran d'accueil, on lance une des fonctions select selon notre choix
execute as @s[scores={configID=9999}] unless score @s config matches 0 run function tdh:meteo/config/select

# Selon la variable qu'on cherche à config
# - Dans tous les cas (10000+), si on veut revenir au menu (config -1999999999):
#	(pas possible avec Scénario/Mois car on doit spécifier si on conserve ou annule nos modifications)
execute as @s[scores={configID=30000..,config=-1999999999}] run function tdh:meteo/_config


# Options des scénarios :
execute as @s[scores={configID=10000}] unless score @s config matches ..0 run function tdh:meteo/config/scenarios/tick

# Options des mois :
execute as @s[scores={configID=20000}] unless score @s config matches ..0 run function tdh:meteo/config/mois/tick

# Options de l'accumulation de la neige :
# - Distance d'actualisation (30100)
execute as @s[scores={configID=30100}] unless score @s config matches ..0 run function tdh:meteo/config/chute_neige/distance_set
# - Intervalle d'actualisation (30200)
execute as @s[scores={configID=30200}] unless score @s config matches ..0 run function tdh:meteo/config/chute_neige/frequence_set
# - Vitesse d'actualisation (30300)
execute as @s[scores={configID=30300}] unless score @s config matches ..0 run function tdh:meteo/config/chute_neige/vitesse_set
# - Intensité minimale (30400)
execute as @s[scores={configID=30400}] unless score @s config matches ..0 run function tdh:meteo/config/chute_neige/intensite_min_set

# Options de la fonte de la neige :
# - Distance d'actualisation (31100)
execute as @s[scores={configID=31100}] unless score @s config matches ..0 run function tdh:meteo/config/fonte_neige/distance_set
# - Intervalle d'actualisation (31200)
execute as @s[scores={configID=31200}] unless score @s config matches ..0 run function tdh:meteo/config/fonte_neige/frequence_set
# - Vitesse d'actualisation (31300)
execute as @s[scores={configID=31300}] unless score @s config matches ..0 run function tdh:meteo/config/fonte_neige/vitesse_set

# Options des particules de pluie/neige :
# - Particules à chaque niveau d'intensité (32101/2/3/4)
execute as @s[scores={configID=32101}] unless score @s config matches ..0 run function tdh:meteo/config/particules_pluie/particules_1_set
execute as @s[scores={configID=32102}] unless score @s config matches ..0 run function tdh:meteo/config/particules_pluie/particules_2_set
execute as @s[scores={configID=32103}] unless score @s config matches ..0 run function tdh:meteo/config/particules_pluie/particules_3_set
execute as @s[scores={configID=32104}] unless score @s config matches ..0 run function tdh:meteo/config/particules_pluie/particules_4_set

# Options des éclairs :
# - Probabilité à l'intensité 1/2/3/4 (33101/2/3/4)
execute as @s[scores={configID=33101}] unless score @s config matches ..0 run function tdh:meteo/config/eclairs/frequence_1_set
execute as @s[scores={configID=33102}] unless score @s config matches ..0 run function tdh:meteo/config/eclairs/frequence_2_set
execute as @s[scores={configID=33103}] unless score @s config matches ..0 run function tdh:meteo/config/eclairs/frequence_3_set
execute as @s[scores={configID=33104}] unless score @s config matches ..0 run function tdh:meteo/config/eclairs/frequence_4_set

# Options du vent :
# - Minimums de vitesse du vent à chaque niveau d'intensité (34101/2/3/4)
execute as @s[scores={configID=34101}] unless score @s config matches ..0 run function tdh:meteo/config/vent/vitesse_min_1_set
execute as @s[scores={configID=34102}] unless score @s config matches ..0 run function tdh:meteo/config/vent/vitesse_min_2_set
execute as @s[scores={configID=34103}] unless score @s config matches ..0 run function tdh:meteo/config/vent/vitesse_min_3_set
execute as @s[scores={configID=34104}] unless score @s config matches ..0 run function tdh:meteo/config/vent/vitesse_min_4_set
# - Maximum de vitesse du vent à l'intensité max (34105)
execute as @s[scores={configID=34105}] unless score @s config matches ..0 run function tdh:meteo/config/vent/vitesse_max_4_set
# - Rotation max par update à chaque niveau d'intensité (34201/2/3/4)
execute as @s[scores={configID=34201}] unless score @s config matches ..0 run function tdh:meteo/config/vent/rotation_max_1_set
execute as @s[scores={configID=34202}] unless score @s config matches ..0 run function tdh:meteo/config/vent/rotation_max_2_set
execute as @s[scores={configID=34203}] unless score @s config matches ..0 run function tdh:meteo/config/vent/rotation_max_3_set
execute as @s[scores={configID=34204}] unless score @s config matches ..0 run function tdh:meteo/config/vent/rotation_max_4_set

# Options de l'actualisation automatique :
# - Nombre maximal d'entités invoquées (50100)
execute as @s[scores={configID=50100}] unless score @s config matches ..0 run function tdh:meteo/config/actu_auto/max_entites_set
# - Durée de vie d'une entité d'actualisation (50200)
execute as @s[scores={configID=50200}] unless score @s config matches ..0 run function tdh:meteo/config/actu_auto/duree_entite_set
# - Nombre maximum de joueurs en ligne (50300)
execute as @s[scores={configID=50300}] unless score @s config matches ..-1 run function tdh:meteo/config/actu_auto/max_joueurs_set
# - Bornes X et Z d'actualisation (50400/410 / 50500/510)
execute as @s[scores={configID=50400}] unless score @s config matches ..-1999999999 run function tdh:meteo/config/actu_auto/x_min_set
execute as @s[scores={configID=50410}] unless score @s config matches ..-1999999999 run function tdh:meteo/config/actu_auto/x_max_set
execute as @s[scores={configID=50500}] unless score @s config matches ..-1999999999 run function tdh:meteo/config/actu_auto/z_min_set
execute as @s[scores={configID=50510}] unless score @s config matches ..-1999999999 run function tdh:meteo/config/actu_auto/z_max_set

# Tous les joueurs qui viennent de choisir une config retournent au menu principal
execute as @s[scores={configID=30000..}] unless score @s config matches -2000000000 run function tdh:meteo/_config
## ENREGISTREMENT DE LA VITESSE D'ACTUALISATION DES CHUTES DE NEIGE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (pas de min/max)
execute if score @s config matches 1.. run scoreboard players operation #neige vitesse = @s config

# On affiche un message de confirmation
tellraw @s [{"text":"Les chutes de neige actualiseront désormais ","color":"gray"},{"score":{"name":"#neige","objective":"vitesse"},"color":"yellow","extra":[{"text":" blocs"}]},{"text":" à chaque update."}]
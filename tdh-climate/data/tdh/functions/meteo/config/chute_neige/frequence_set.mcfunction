## ENREGISTREMENT DE L'INTERVALLE D'ACTUALISATION DES CHUTES DE NEIGE
# Il est indiqué par la variable config

# On définit la valeur de la variable (minimum 16, maximum 170)
execute if score @s config matches 1.. run scoreboard players operation #neige frequenceUpdate = @s config

# On affiche un message de confirmation
execute if score #neige frequenceUpdate matches 1 run tellraw @s [{"text":"Les chutes de neige seront désormais updatées à ","color":"gray"},{"text":"chaque tick","color":"yellow"},{"text":"."}]
execute unless score #neige frequenceUpdate matches 1 run tellraw @s [{"text":"Les chutes de neige seront désormais updatées tous les ","color":"gray"},{"score":{"name":"#neige","objective":"frequenceUpdate"},"color":"yellow","extra":[{"text":" ticks"}]},{"text":"."}]
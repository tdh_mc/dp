## ENREGISTREMENT DE L'INTENSITÉ MINIMALE DES CHUTES DE NEIGE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (minimum 0, maximum 4)
execute if score @s config matches 1..4 run scoreboard players operation #neige intensiteMin = @s config
execute if score @s config matches 5.. run scoreboard players set #neige intensiteMin 0

# On affiche un message de confirmation
execute unless score #neige intensiteMin matches 1.. run tellraw @s [{"text":"Les chutes de neige seront désormais calculées quelle que soit l'intensité.","color":"gray"}]
execute if score #neige intensiteMin matches 1.. run tellraw @s [{"text":"L'intensité minimale pour calculer les chutes de neige est désormais de ","color":"gray"},{"score":{"name":"#neige","objective":"intensiteMin"},"color":"yellow"},{"text":"."}]
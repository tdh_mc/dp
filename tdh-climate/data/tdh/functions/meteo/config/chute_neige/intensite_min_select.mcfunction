## SÉLECTION DE L'INTENSITÉ MINIMUM POUR ACTIVER LES CHUTES DE NEIGE

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config -2000000000
scoreboard players enable @s config


tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE","color":"gold"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez l'intensité météo minimale requise pour les chutes de neige :","color":"gray"}]
tellraw @s [{"text":"","color":"gray"},{"text":"0","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 999"},"hoverEvent":{"action":"show_text","value":"Pas de minimum."}},{"text":" ● "},{"text":"1","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 1"},"hoverEvent":{"action":"show_text","value":"L'update des chutes de neige sera désactivé quand l'intensité est de 0."}},{"text":" ● "},{"text":"2","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 2"},"hoverEvent":{"action":"show_text","value":"L'update des chutes de neige sera désactivé quand l'intensité est de 0 ou 1."}},{"text":" ● "},{"text":"3","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 3"},"hoverEvent":{"action":"show_text","value":"L'update des chutes de neige sera désactivé quand l'intensité est de 0, 1 ou 2."}},{"text":" ● "},{"text":"4","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 4"},"hoverEvent":{"action":"show_text","value":"L'update des chutes de neige sera désactivé quand l'intensité n'est pas de 4."}}]

#tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","hoverEvent":{"action":"show_text","value":[{"text":""}]},"clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -1999999999"}},{"score":{"name":"#neige","objective":"intensiteMin"}},{"text":")"}]
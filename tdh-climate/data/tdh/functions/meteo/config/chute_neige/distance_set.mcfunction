## ENREGISTREMENT DE LA DISTANCE D'ACTUALISATION DES CHUTES DE NEIGE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (minimum 16, maximum 170)
execute if score @s config matches 16..170 run scoreboard players operation #neige distance = @s config
execute if score @s config matches 0..15 run scoreboard players set #neige distance 16
execute if score @s config matches 171.. run scoreboard players set #neige distance 170

# On affiche un message de confirmation
tellraw @s [{"text":"La distance d'actualisation des chutes de neige est désormais de ","color":"gray"},{"score":{"name":"#neige","objective":"distance"},"color":"yellow","extra":[{"text":" blocs"}]},{"text":"."}]
# Configurateur du climat
# Sous-menu des éclairs

# On démarre la config
function tdh:meteo/config/init

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE / ","color":"gold"},{"text":"Éclairs","bold":"true"},{"text":"\n——————————————————————————","color":"gray"}]

# Nombre de particules pour chaque niveau d'intensité (33101/2/3/4)
tellraw @s [{"text":"Intensité 1 : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Chances par tick, sur 10'000, d'invoquer un éclair lorsque l'intensité météo est de 1.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 33101"}},{"score":{"name":"#orage_1","objective":"probaChgmt"},"color":"gold"}]
tellraw @s [{"text":"Intensité 2 : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Chances par tick, sur 10'000, d'invoquer un éclair lorsque l'intensité météo est de 2.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 33102"}},{"score":{"name":"#orage_2","objective":"probaChgmt"},"color":"gold"}]
tellraw @s [{"text":"Intensité 3 : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Chances par tick, sur 10'000, d'invoquer un éclair lorsque l'intensité météo est de 3.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 33103"}},{"score":{"name":"#orage_3","objective":"probaChgmt"},"color":"gold"}]
tellraw @s [{"text":"Intensité 4 : ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Chances par tick, sur 10'000, d'invoquer un éclair lorsque l'intensité météo est de 4.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 33104"}},{"score":{"name":"#orage_4","objective":"probaChgmt"},"color":"gold"}]

# Paramètres génériques
tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"Réinitialiser les paramètres","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Tous les paramètres des éclairs seront définis à leurs valeurs par défaut.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/eclairs/reset"}},{"text":" • ","color":"gray"},{"text":"Retour au menu racine","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Affiche le menu principal de tdh-climate.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/_config"}}]
## ENREGISTREMENT DES CHANCES D'AVOIR UN ÉCLAIR PAR TICK À L'INTENSITÉ 2
# Elles sont indiquées par la variable config

# On définit la valeur de la variable
execute if score @s config matches 0..10000 run scoreboard players operation #orage_2 probaChgmt = @s config
execute if score @s config matches 10001.. run scoreboard players set #orage_2 probaChgmt 10000

# On affiche un message de confirmation
tellraw @s [{"text":"À l'intensité 2, les chances d'avoir un éclair sont désormais de ","color":"gray"},{"score":{"name":"#orage_2","objective":"probaChgmt"},"color":"yellow","extra":[{"text":"/10'000"}]},{"text":" par tick."}]

# On recalcule les chances actuelles
function tdh:meteo/effects/orage/intensite_changee
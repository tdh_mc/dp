## DÉFINITION DES POIDS MENSUELS DES SCÉNARIOS

# On enregistre l'état original de la config des mois
#  (sauf si on en a déjà sauvegardé un sans le supprimer)
execute unless data storage tdh:meteo index_backup.mois run data modify storage tdh:meteo index_backup.mois set from storage tdh:meteo index.mois

# On active l'objectif de configuration à trigger
scoreboard players set @s configID 20000
scoreboard players set @s config -2000000000
scoreboard players enable @s config


tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE","color":"gold"}]
tellraw @s [{"text":"——————————————————————————\nCe menu n'est pas encore disponible car il est assez ","color":"gray"},{"text":"relou","bold":"true","color":"red"},{"text":" à mettre en place. Vous pouvez toutefois :\n- Voir les réglages de l'ensemble des mois avec "},{"text":"cette commande","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Afficher les réglages de tous les mois."}]},"clickEvent":{"action":"suggest_command","value":"/data get storage tdh:meteo index.mois"}},{"text":"\n- Voir les réglages d'un mois spécifique avec "},{"text":"celle-ci","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Afficher les réglages d'un mois spécifique."}]},"clickEvent":{"action":"suggest_command","value":"/data get storage tdh:meteo index.mois[ ID_MOIS ]"}},{"text":"\n- Afficher la "},{"text":"liste des scénarios disponibles","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Afficher tous les noms des scénarios enregistrés."}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/scenarios/liste"}},{"text":"\n- "},{"text":" [Afficher l'aide]","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Chaque semaine, le système météo choisit un scénario aléatoirement, parmi les différents choix possibles pour le mois actuel. Les paramètres de chacun des mois contiennent deux champs :\n- "},{"text":"poids","color":"gold"},{"text":" = le poids total de l'ensemble des scénarios possibles,\n -"},{"text":"scenarios","color":"gold"},{"text":" = une liste de noms de scénarios ("},{"text":"scenarios[N].nom","color":"gold"},{"text":") auxquels sont associés des poids ("},{"text":"scenarios[N].poids","color":"yellow"},{"text":") qui déterminent les probabilités d'obtenir ce scénario plutôt qu'un autre."}]}}]

tellraw @s [{"text":"——————————————————————————","color":"gray"}]

tellraw @s [{"text":"Valider la configuration actuelle","color":"gold","hoverEvent":{"action":"show_text","contents":"Sauvegarde toutes les modifications apportées aux mois, et retourne au menu principal."},"clickEvent":{"action":"run_command","value":"/trigger config set 2"}}]
tellraw @s [{"text":"Annuler toutes les modifications","color":"red","hoverEvent":{"action":"show_text","contents":"Annule toutes les modifications apportées aux mois, et retourne au menu principal."},"clickEvent":{"action":"run_command","value":"/trigger config set 3"}}]
# Configurateur du climat
# Sous-menu du vent

# On démarre la config
function tdh:meteo/config/init

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE / ","color":"gold"},{"text":"Vent","bold":"true"},{"text":"\n——————————————————————————","color":"gray"}]

# Cutoff de vitesse du vent pour chaque niveau d'intensité (34101/2/3/4/5),
# le 5 correspondant à la valeur *max* de l'intensité 4 (les autres max sont calculés à partir des minimums, sauf pour l'intensité 0 qui n'est pas paramétrable)
tellraw @s [{"text":"Le vent est d'intensité 1 au-dessus de ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Vitesse du vent minimale lorsque l'intensité météo est de 1.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34101"}},{"score":{"name":"#vent_1","objective":"vitesseMin"},"color":"gold"},{"text":" m/s."}]
tellraw @s [{"text":"Le vent est d'intensité 2 au-dessus de ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Vitesse du vent minimale lorsque l'intensité météo est de 2.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34102"}},{"score":{"name":"#vent_2","objective":"vitesseMin"},"color":"gold"},{"text":" m/s."}]
tellraw @s [{"text":"Le vent est d'intensité 3 au-dessus de ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Vitesse du vent minimale lorsque l'intensité météo est de 3.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34103"}},{"score":{"name":"#vent_3","objective":"vitesseMin"},"color":"gold"},{"text":" m/s."}]
tellraw @s [{"text":"Le vent est d'intensité 4 au-dessus de ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Vitesse du vent minimale lorsque l'intensité météo est de 4.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34104"}},{"score":{"name":"#vent_4","objective":"vitesseMin"},"color":"gold"},{"text":" m/s."}]
tellraw @s [{"text":"Le vent ne peut pas dépasser ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Vitesse du vent maximale lorsque l'intensité météo est de 4.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34105"}},{"score":{"name":"#vent_4","objective":"vitesseMax"},"color":"gold"},{"text":" m/s."}]

# On ne peut pas customiser la durée d'actualisation par période pour l'instant (TODO)
# On peut simplement modifier la vitesse de rotation (34201/2/3/4)
tellraw @s [{"text":"À l'intensité 1, le vent peut tourner au maximum de ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Rotation maximale du vent, à chaque update du vent, lorsque l'intensité météo est de 1.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34201"}},{"score":{"name":"#vent_1","objective":"rotationMax"},"color":"gold"},{"text":" degrés à chaque update."}]
tellraw @s [{"text":"À l'intensité 2, le vent peut tourner au maximum de ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Rotation maximale du vent, à chaque update du vent, lorsque l'intensité météo est de 2.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34202"}},{"score":{"name":"#vent_2","objective":"rotationMax"},"color":"gold"},{"text":" degrés à chaque update."}]
tellraw @s [{"text":"À l'intensité 3, le vent peut tourner au maximum de ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Rotation maximale du vent, à chaque update du vent, lorsque l'intensité météo est de 3.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34203"}},{"score":{"name":"#vent_3","objective":"rotationMax"},"color":"gold"},{"text":" degrés à chaque update."}]
tellraw @s [{"text":"À l'intensité 4, le vent peut tourner au maximum de ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"Rotation maximale du vent, à chaque update du vent, lorsque l'intensité météo est de 4.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 34204"}},{"score":{"name":"#vent_4","objective":"rotationMax"},"color":"gold"},{"text":" degrés à chaque update."}]

# Paramètres génériques
tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"Réinitialiser les paramètres","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Tous les paramètres du vent seront définis à leurs valeurs par défaut.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/vent/reset"}},{"text":" • ","color":"gray"},{"text":"Retour au menu racine","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Affiche le menu principal de tdh-climate.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/_config"}}]
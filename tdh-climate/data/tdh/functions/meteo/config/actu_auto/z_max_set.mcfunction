## ENREGISTREMENT DU MAXIMUM EN Z DE L'ACTUALISATION AUTOMATIQUE
# Il est indiqué par la variable config

# On définit la valeur de la variable
scoreboard players operation #meteoActuAuto zMax = @s config

# On affiche un message de confirmation
tellraw @s [{"text":"La borne supérieure en Z du système d'actualisation automatique est désormais à ","color":"gray"},{"score":{"name":"#meteoActuAuto","objective":"zMax"},"color":"yellow"},{"text":"."}]
## SÉLECTION DU NOMBRE MAXIMAL D'ENTITÉS D'ACTUALISATION AUTOMATIQUE

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config -2000000000
scoreboard players enable @s config


tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE","color":"gold"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le nombre maximal de positions à actualiser simultanément :","color":"gray"}]
tellraw @s [{"text":"","color":"gray"},{"text":"1","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 1"},"hoverEvent":{"action":"show_text","value":"1 positions simultanées"}},{"text":" ● "},{"text":"2","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 2"},"hoverEvent":{"action":"show_text","value":"2 positions simultanées"}},{"text":" ● "},{"text":"3","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 3"},"hoverEvent":{"action":"show_text","value":"3 positions simultanées"}},{"text":" ● "},{"text":"4","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 4"},"hoverEvent":{"action":"show_text","value":"4 positions simultanées"}},{"text":" ● "},{"text":"5","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 5"},"hoverEvent":{"action":"show_text","value":"5 positions simultanées"}},{"text":" ● "},{"text":"6","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 6"},"hoverEvent":{"action":"show_text","value":"6 positions simultanées"}},{"text":" ● "},{"text":"7","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 7"},"hoverEvent":{"action":"show_text","value":"7 positions simultanées"}},{"text":" ● "},{"text":"8","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 8"},"hoverEvent":{"action":"show_text","value":"8 positions simultanées"}}]

tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -1999999999"}},{"score":{"name":"#meteoActuAuto","objective":"nombre"}},{"text":")"}]
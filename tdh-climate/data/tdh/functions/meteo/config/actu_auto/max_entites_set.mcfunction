## ENREGISTREMENT DU NOMBRE MAX D'ENTITÉS D'ACTUALISATION AUTOMATIQUE
# Il est indiqué par la variable config

# On définit la valeur de la variable (minimum 1, maximum 9)
# On pourrait théoriquement en mettre largement plus (autant que de joueurs sur un serveur),
# mais cette valeur évite de définir par erreur une valeur beaucoup trop élevée suite à une faute de frappe
# Elle pourra évidemment être relevée ultérieurement s'il s'avère que le système fonctionne bien
execute if score @s config matches 1..9 run scoreboard players operation #meteoActuAuto nombre = @s config
execute if score @s config matches ..0 run scoreboard players set #meteoActuAuto nombre 1
execute if score @s config matches 10.. run scoreboard players set #meteoActuAuto nombre 9

# On affiche un message de confirmation
tellraw @s [{"text":"Le nombre maximal d'entités d'actualisation automatique est désormais de ","color":"gray"},{"score":{"name":"#meteoActuAuto","objective":"nombre"},"color":"yellow"},{"text":"."}]
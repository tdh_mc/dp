## ENREGISTREMENT DU NOMBRE MAX DE JOUEURS POUR L'ACTUALISATION AUTOMATIQUE
# Il est indiqué par la variable config

# On définit la valeur de la variable
scoreboard players operation #meteoActuAuto playersOnline = @s config

# On affiche un message de confirmation
tellraw @s [{"text":"Le nombre maximal de joueurs en ligne pour l'actualisation automatique est désormais de ","color":"gray"},{"score":{"name":"#meteoActuAuto","objective":"playersOnline"},"color":"yellow"},{"text":"."}]
## ENREGISTREMENT DE LA DURÉE D'EXISTENCE D'UNE ENTITÉ D'ACTUALISATION AUTOMATIQUE
# Elle est indiquée par la variable config

# On définit la valeur de la variable (pas de filtres ; on a un minimum de 1 de toute façon)
scoreboard players operation #meteoActuAuto duree = @s config

# On calcule cette valeur en temps réel
scoreboard players set #store currentHour 600
scoreboard players operation #store currentHour *= #meteoActuAuto duree
function tdh:store/realtime

# On affiche un message de confirmation
tellraw @s [{"text":"La durée de vie d'une entité d'actualisation automatique est désormais de ","color":"gray"},{"storage":"tdh:store","nbt":"realtime","interpret":"true","color":"yellow"},{"text":"."}]
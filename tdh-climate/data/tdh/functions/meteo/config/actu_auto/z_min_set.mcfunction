## ENREGISTREMENT DU MINIMUM EN Z DE L'ACTUALISATION AUTOMATIQUE
# Il est indiqué par la variable config

# On définit la valeur de la variable
scoreboard players operation #meteoActuAuto zMin = @s config

# On affiche un message de confirmation
tellraw @s [{"text":"La borne inférieure en Z du système d'actualisation automatique est désormais à ","color":"gray"},{"score":{"name":"#meteoActuAuto","objective":"zMin"},"color":"yellow"},{"text":"."}]
## SÉLECTION DU X MAXIMUM DE L'ACTUALISATION AUTOMATIQUE

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config -2000000000
scoreboard players enable @s config


tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE","color":"gold"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le maximum en X des positions d'actualisation :","color":"gray"}]

tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -1999999999"}},{"score":{"name":"#meteoActuAuto","objective":"xMax"}},{"text":")"}]
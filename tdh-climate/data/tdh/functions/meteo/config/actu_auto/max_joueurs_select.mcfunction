## SÉLECTION DU NOMBRE MAXIMAL DE JOUEURS POUR L'ACTUALISATION AUTOMATIQUE

# On active l'objectif de configuration à trigger
scoreboard players operation @s configID = @s config
scoreboard players set @s config -2000000000
scoreboard players enable @s config


tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE","color":"gold"}]
tellraw @s [{"text":"——————————————————————————\nChoisissez le nombre maximal de joueurs en ligne, au-delà duquel l'actualisation automatique s'interrompra :","color":"gray"}]
tellraw @s [{"text":"","color":"gray"},{"text":"0","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 0"},"hoverEvent":{"action":"show_text","value":"Aucun joueur ne doit être présent"}},{"text":" ● "},{"text":"1","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 1"},"hoverEvent":{"action":"show_text","value":"1 joueur maximum"}},{"text":" ● "},{"text":"2","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 2"},"hoverEvent":{"action":"show_text","value":"2 joueurs maximum"}},{"text":" ● "},{"text":"3","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 3"},"hoverEvent":{"action":"show_text","value":"3 joueurs maximum"}},{"text":" ● "},{"text":"4","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 4"},"hoverEvent":{"action":"show_text","value":"4 joueurs maximum"}},{"text":" ● "},{"text":"5","color":"yellow","clickEvent":{"action":"run_command","value":"/trigger config set 5"},"hoverEvent":{"action":"show_text","value":"5 joueurs maximum"}},{"text":" ● "},{"text":"6","color":"gold","clickEvent":{"action":"run_command","value":"/trigger config set 6"},"hoverEvent":{"action":"show_text","value":"6 joueurs maximum"}}]

tellraw @s [{"text":"Entrer une valeur précise","color":"#ff7738","clickEvent":{"action":"suggest_command","value":"/trigger config set "}}]
tellraw @s [{"text":"Conserver le réglage actuel (","color":"red","clickEvent":{"action":"run_command","value":"/trigger config set -1999999999"}},{"score":{"name":"#meteoActuAuto","objective":"playersOnline"}},{"text":")"}]
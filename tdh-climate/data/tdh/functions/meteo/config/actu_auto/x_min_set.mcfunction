## ENREGISTREMENT DU MINIMUM EN X DE L'ACTUALISATION AUTOMATIQUE
# Il est indiqué par la variable config

# On définit la valeur de la variable
scoreboard players operation #meteoActuAuto xMin = @s config

# On affiche un message de confirmation
tellraw @s [{"text":"La borne inférieure en X du système d'actualisation automatique est désormais à ","color":"gray"},{"score":{"name":"#meteoActuAuto","objective":"xMin"},"color":"yellow"},{"text":"."}]
## ENREGISTREMENT DU MAXIMUM EN X DE L'ACTUALISATION AUTOMATIQUE
# Il est indiqué par la variable config

# On définit la valeur de la variable
scoreboard players operation #meteoActuAuto xMax = @s config

# On affiche un message de confirmation
tellraw @s [{"text":"La borne supérieure en X du système d'actualisation automatique est désormais à ","color":"gray"},{"score":{"name":"#meteoActuAuto","objective":"xMax"},"color":"yellow"},{"text":"."}]

# execute if score #meteo_pluie probaCalme matches ..1999 if score #meteo_calme probaPluie matches 1000.. run function tdh:meteo/auto/prevision/pluie_stable

execute if score #meteo_pluie intensiteMax matches 1 run function tdh:meteo/auto/prevision/message/pluie_faible
execute if score #meteo_pluie intensiteMax matches 2 run function tdh:meteo/auto/prevision/message/pluie_moyenne
execute if score #meteo_pluie intensiteMax matches 3 run function tdh:meteo/auto/prevision/message/pluie_forte
execute if score #meteo_pluie intensiteMax matches 4 run function tdh:meteo/auto/prevision/message/pluie_diluvienne
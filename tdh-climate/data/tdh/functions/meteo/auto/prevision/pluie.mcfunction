
# execute if score #meteo_calme probaPluie matches 101.. if score #meteo_calme probaOrage matches ..99 run function tdh:meteo/auto/prevision/pluie

execute if score #meteo_pluie probaCalme matches 2000.. run function tdh:meteo/auto/prevision/averse
execute if score #meteo_pluie probaCalme matches ..1999 if score #meteo_calme probaPluie matches ..999 run function tdh:meteo/auto/prevision/message/rares_averses
execute if score #meteo_pluie probaCalme matches ..1999 if score #meteo_calme probaPluie matches 1000.. run function tdh:meteo/auto/prevision/pluie_stable
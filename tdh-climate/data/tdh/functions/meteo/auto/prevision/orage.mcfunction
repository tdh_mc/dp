
# execute if score #meteo_calme probaPluie matches 101.. if score #meteo_calme probaOrage matches 100.. run function tdh:meteo/auto/prevision/orage

execute if score #meteo_pluie probaOrage matches ..399 run function tdh:meteo/auto/prevision/message/risque_orage
execute if score #meteo_pluie probaOrage matches 400.. run function tdh:meteo/auto/prevision/message/orages
execute if score #meteo_pluie probaOrage matches 400.. if score #meteo_pluie probaCalme matches 1500.. if score #meteo_orage probaCalme matches 1500.. run function tdh:meteo/auto/prevision/message/averses_orageuses
execute if score #meteo_pluie probaOrage matches 400.. if score #meteo_orage probaCalme matches ..499 if score #meteo_orage probaPluie matches ..499 if score #meteo_orage intensiteMax matches 3.. run function tdh:meteo/auto/prevision/message/orages_violents
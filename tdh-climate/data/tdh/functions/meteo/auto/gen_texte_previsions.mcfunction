
# Prévisions pour cette semaine
# Si la proba de pluie est de moins de 1%, on prévoit du beau temps
# (sauf si la proba d'orage est supérieure à 1% auquel cas on avertit des risques d'orage)
execute if score #meteo_calme probaPluie matches ..100 if score #meteo_calme probaOrage matches ..99 run function tdh:meteo/auto/prevision/message/beau_temps
execute if score #meteo_calme probaPluie matches ..100 if score #meteo_calme probaOrage matches 100.. run function tdh:meteo/auto/prevision/message/risque_orage

# Si la proba de pluie est de plus de 1% et le risque d'orage faible, on exécute les checks pour la pluie
execute if score #meteo_calme probaPluie matches 101.. if score #meteo_calme probaOrage matches ..99 run function tdh:meteo/auto/prevision/pluie
execute if score #meteo_calme probaPluie matches 101.. if score #meteo_calme probaOrage matches 100.. run function tdh:meteo/auto/prevision/orage
# On fait évoluer légèrement les probabilités en direction de celles de la semaine suivante
scoreboard players set #meteo temp2 30
scoreboard players operation #meteo temp2 /= #temps jourSemaine

# On prend la différence hebdomadaire entre chaque variable, et on ajoute une fraction de cette différence à la valeur de cette semaine
scoreboard players operation #meteo temp = #meteoSP_calme probaPluie
scoreboard players operation #meteo temp -= #meteo_calme probaPluie
scoreboard players operation #meteo temp /= #meteo temp2
scoreboard players operation #meteo_calme probaPluie += #meteo temp

scoreboard players operation #meteo temp = #meteoSP_calme probaOrage
scoreboard players operation #meteo temp -= #meteo_calme probaOrage
scoreboard players operation #meteo temp /= #meteo temp2
scoreboard players operation #meteo_calme probaOrage += #meteo temp


scoreboard players operation #meteo temp = #meteoSP_pluie probaCalme
scoreboard players operation #meteo temp -= #meteo_pluie probaCalme
scoreboard players operation #meteo temp /= #meteo temp2
scoreboard players operation #meteo_pluie probaCalme += #meteo temp

scoreboard players operation #meteo temp = #meteoSP_pluie probaOrage
scoreboard players operation #meteo temp -= #meteo_pluie probaOrage
scoreboard players operation #meteo temp /= #meteo temp2
scoreboard players operation #meteo_pluie probaOrage += #meteo temp


scoreboard players operation #meteo temp = #meteoSP_orage probaCalme
scoreboard players operation #meteo temp -= #meteo_orage probaCalme
scoreboard players operation #meteo temp /= #meteo temp2
scoreboard players operation #meteo_orage probaCalme += #meteo temp

scoreboard players operation #meteo temp = #meteoSP_orage probaPluie
scoreboard players operation #meteo temp -= #meteo_orage probaPluie
scoreboard players operation #meteo temp /= #meteo temp2
scoreboard players operation #meteo_orage probaPluie += #meteo temp

# On réinitialise les variables temporaires
scoreboard players reset #meteo temp
scoreboard players reset #meteo temp2
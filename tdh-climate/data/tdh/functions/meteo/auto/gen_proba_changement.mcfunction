
# Pour chaque météo, les probabilités de changement sont égales à la somme des deux probabilités
# Ex: pour temps calme, les probabilités de changement = probas de pluie + probas d'orage
scoreboard players operation #meteo_calme probaChgmt = #meteo_calme probaPluie
scoreboard players operation #meteo_calme probaChgmt += #meteo_calme probaOrage

scoreboard players operation #meteo_pluie probaChgmt = #meteo_pluie probaCalme
scoreboard players operation #meteo_pluie probaChgmt += #meteo_pluie probaOrage

scoreboard players operation #meteo_orage probaChgmt = #meteo_orage probaPluie
scoreboard players operation #meteo_orage probaChgmt += #meteo_orage probaCalme
# Génération de la prochaine semaine de prévisions

# On récupère d'abord dans le storage (tdh:meteo calcul.mois) le mois correspondant
execute if score #temps dateMois matches 1 run function tdh:meteo/auto/mois/01
execute if score #temps dateMois matches 2 run function tdh:meteo/auto/mois/02
execute if score #temps dateMois matches 3 run function tdh:meteo/auto/mois/03
execute if score #temps dateMois matches 4 run function tdh:meteo/auto/mois/04
execute if score #temps dateMois matches 5 run function tdh:meteo/auto/mois/05
execute if score #temps dateMois matches 6 run function tdh:meteo/auto/mois/06
execute if score #temps dateMois matches 7 run function tdh:meteo/auto/mois/07
execute if score #temps dateMois matches 8 run function tdh:meteo/auto/mois/08
execute if score #temps dateMois matches 9 run function tdh:meteo/auto/mois/09
execute if score #temps dateMois matches 10 run function tdh:meteo/auto/mois/10
execute if score #temps dateMois matches 11 run function tdh:meteo/auto/mois/11
execute if score #temps dateMois matches 12 run function tdh:meteo/auto/mois/12
execute if score #temps dateMois matches 13 run function tdh:meteo/auto/mois/13
execute if score #temps dateMois matches 14 run function tdh:meteo/auto/mois/14
execute if score #temps dateMois matches 15 run function tdh:meteo/auto/mois/15
execute if score #temps dateMois matches 16 run function tdh:meteo/auto/mois/16
execute if score #temps dateMois matches 17 run function tdh:meteo/auto/mois/17
execute if score #temps dateMois matches 18 run function tdh:meteo/auto/mois/18
execute if score #temps dateMois matches 19 run function tdh:meteo/auto/mois/19
execute if score #temps dateMois matches 20 run function tdh:meteo/auto/mois/20
execute if score #temps dateMois matches 21 run function tdh:meteo/auto/mois/21
execute if score #temps dateMois matches 22 run function tdh:meteo/auto/mois/22
execute if score #temps dateMois matches 23 run function tdh:meteo/auto/mois/23
execute if score #temps dateMois matches 24 run function tdh:meteo/auto/mois/24


# On récupère un nombre aléatoire d'après le poids total spécifié dans les paramètres du mois
scoreboard players set #random min 0
execute store result score #random max run data get storage tdh:meteo calcul.mois.poids
function tdh:random
scoreboard players operation #meteo temp = #random temp

# Ensuite, on va itérer la liste des scénarios possibles pour ce mois ;
# Pour chaque scénario, on va retrancher son poids au nombre aléatoire ;
# Tant que notre nombre aléatoire est supérieur ou égal à 0, on passe au scénario suivant
function tdh:meteo/auto/gen_semaine/next
# Lorsqu'on sort de cette fonction, le storage tdh:meteo calcul.mois a été supprimé,
# et le storage tdh:meteo calcul.scenario contient le scénario considéré


# On va rechercher le scénario correspondant à l'ID (contenu dans tdh:meteo calcul.scenario.nom)
data modify storage tdh:meteo recherche.nom set from storage tdh:meteo calcul.scenario.nom
function tdh:meteo/auto/recherche/scenario
# Après cette fonction le scénario se trouve dans tdh:meteo recherche.resultat

# On copie toutes les données de ce scénario vers le scoreboard, en appliquant l'aléatoire requis
function tdh:meteo/auto/gen_semaine/copy
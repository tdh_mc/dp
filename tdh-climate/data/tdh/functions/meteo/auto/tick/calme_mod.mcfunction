# On récupère 1 variable aléatoire entre 0 et 99 dans #random temp
scoreboard players set #random min 0
scoreboard players set #random max 100
function tdh:random

# On module la puissance du vent si le nombre aléatoire correspond
execute if score #meteo intensite < #meteo intensiteMax if score #meteo temp matches ..19 run scoreboard players add #meteo intensite 1
execute if score #meteo intensite > #meteo intensiteMin if score #meteo temp matches 80.. run scoreboard players remove #meteo intensite 1
# On affiche un message
execute if score #meteo actuelle = #meteo_calme actuelle run tellraw @a [{"nbt":"texte.transition.calme_pluie","storage":"tdh:meteo","interpret":"true","color":"gray"}]
execute if score #meteo actuelle = #meteo_orage actuelle run tellraw @a [{"nbt":"texte.transition.orage_pluie","storage":"tdh:meteo","interpret":"true","color":"gray"}]

# On change la météo
weather rain

# On modifie les variables correspondantes
scoreboard players operation #meteo suivante = #meteo_pluie actuelle
scoreboard players operation #meteo intensiteMin = #meteo_pluie intensiteMin
scoreboard players operation #meteo intensiteMax = #meteo_pluie intensiteMax
function tdh:meteo/auto/tick/gen_intensite

# On modifie le storage
data modify storage tdh:meteo actuelle.nom set value "Pluie"
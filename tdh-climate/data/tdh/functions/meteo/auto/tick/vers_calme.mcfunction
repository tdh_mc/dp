# On affiche un message
execute if score #meteo actuelle = #meteo_pluie actuelle run tellraw @a [{"nbt":"texte.transition.pluie_calme","storage":"tdh:meteo","interpret":"true","color":"gray"}]
execute if score #meteo actuelle = #meteo_orage actuelle run tellraw @a [{"nbt":"texte.transition.orage_calme","storage":"tdh:meteo","interpret":"true","color":"gray"}]

# On change la météo
weather clear

# On modifie les variables correspondantes
scoreboard players operation #meteo suivante = #meteo_calme actuelle
scoreboard players operation #meteo intensiteMin = #meteo_calme intensiteMin
scoreboard players operation #meteo intensiteMax = #meteo_calme intensiteMax
function tdh:meteo/auto/tick/gen_intensite

# On modifie le storage
data modify storage tdh:meteo actuelle.nom set value "Beau temps"
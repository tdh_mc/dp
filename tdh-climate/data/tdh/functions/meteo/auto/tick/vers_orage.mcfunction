# On affiche un message
execute if score #meteo actuelle = #meteo_calme actuelle run tellraw @a [{"nbt":"texte.transition.calme_orage","storage":"tdh:meteo","interpret":"true","color":"gray"}]
execute if score #meteo actuelle = #meteo_pluie actuelle run tellraw @a [{"nbt":"texte.transition.pluie_orage","storage":"tdh:meteo","interpret":"true","color":"gray"}]

# On change la météo
weather thunder

# On modifie les variables correspondantes
scoreboard players operation #meteo suivante = #meteo_orage actuelle
scoreboard players operation #meteo intensiteMin = #meteo_orage intensiteMin
scoreboard players operation #meteo intensiteMax = #meteo_orage intensiteMax
function tdh:meteo/auto/tick/gen_intensite

# On modifie le storage
data modify storage tdh:meteo actuelle.nom set value "Orage"
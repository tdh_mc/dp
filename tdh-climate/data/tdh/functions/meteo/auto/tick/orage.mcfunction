# Si le nombre aléatoire est inférieur à la probabilité de pluie, alors c'est pluie
execute if score #random temp < #meteo_orage probaPluie run function tdh:meteo/auto/tick/vers_pluie

# Si le nombre aléatoire est inférieur à la probabilité de changement, alors c'est temps clair
execute if score #random temp >= #meteo_orage probaPluie if score #random temp < #meteo_orage probaChgmt run function tdh:meteo/auto/tick/vers_calme

# Sinon, si le nombre aléatoire est supérieur à la probabilité de changement, on modifie éventuellement l'intensité
execute if score #random temp >= #meteo_orage probaChgmt run function tdh:meteo/auto/tick/orage_mod
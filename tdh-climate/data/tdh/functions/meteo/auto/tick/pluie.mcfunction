# Si le nombre aléatoire est inférieur à la probabilité d'orage, alors c'est orage
execute if score #random temp < #meteo_pluie probaOrage run function tdh:meteo/auto/tick/vers_orage

# Si le nombre aléatoire est inférieur à la probabilité de changement, alors c'est temps clair
execute if score #random temp >= #meteo_pluie probaOrage if score #random temp < #meteo_pluie probaChgmt run function tdh:meteo/auto/tick/vers_calme

# Sinon, si le nombre aléatoire est supérieur à la probabilité de changement, on modifie éventuellement l'intensité
execute if score #random temp >= #meteo_pluie probaChgmt run function tdh:meteo/auto/tick/pluie_mod
# On récupère 1 variable aléatoire entre 0 et 4 dans #random temp
scoreboard players set #random min 0
scoreboard players set #random max 5
function tdh:random

# On assigne cette valeur aléatoire à l'intensité
scoreboard players operation #meteo intensite = #random temp

# On modifie l'intensité si elle dépasse du min/max
execute if score #meteo intensite < #meteo intensiteMin run scoreboard players operation #meteo intensite = #meteo intensiteMin
execute if score #meteo intensite > #meteo intensiteMax run scoreboard players operation #meteo intensite = #meteo intensiteMax
# Si le nombre aléatoire est inférieur à la probabilité d'orage, alors c'est orage
execute if score #random temp < #meteo_calme probaOrage run function tdh:meteo/auto/tick/vers_orage

# Si le nombre aléatoire est inférieur à la probabilité de changement, alors c'est pluie
execute if score #random temp >= #meteo_calme probaOrage if score #random temp < #meteo_calme probaChgmt run function tdh:meteo/auto/tick/vers_pluie

# Sinon, si le nombre aléatoire est supérieur à la probabilité de changement, on modifie éventuellement l'intensité
execute if score #random temp >= #meteo_calme probaChgmt run function tdh:meteo/auto/tick/calme_mod
# On copie les données de la semaine prochaine dans la semaine actuelle
scoreboard players operation #meteo_calme probaPluie = #meteoSP_calme probaPluie
scoreboard players operation #meteo_calme probaOrage = #meteoSP_calme probaOrage
scoreboard players operation #meteo_calme probaChgmt = #meteoSP_calme probaChgmt
scoreboard players operation #meteo_calme intensiteMin = #meteoSP_calme intensiteMin
scoreboard players operation #meteo_calme intensiteMax = #meteoSP_calme intensiteMax

scoreboard players operation #meteo_pluie probaCalme = #meteoSP_pluie probaCalme
scoreboard players operation #meteo_pluie probaOrage = #meteoSP_pluie probaOrage
scoreboard players operation #meteo_pluie probaChgmt = #meteoSP_pluie probaChgmt
scoreboard players operation #meteo_pluie intensiteMin = #meteoSP_pluie intensiteMin
scoreboard players operation #meteo_pluie intensiteMax = #meteoSP_pluie intensiteMax

scoreboard players operation #meteo_orage probaCalme = #meteoSP_orage probaCalme
scoreboard players operation #meteo_orage probaPluie = #meteoSP_orage probaPluie
scoreboard players operation #meteo_orage probaChgmt = #meteoSP_orage probaChgmt
scoreboard players operation #meteo_orage intensiteMin = #meteoSP_orage intensiteMin
scoreboard players operation #meteo_orage intensiteMax = #meteoSP_orage intensiteMax

# On copie les prévisions de la semaine prochaine dans la semaine actuelle
data modify storage tdh:meteo prevision set from storage tdh:meteo previsionSP


# On génère de nouvelles données pour la semaine prochaine
function tdh:meteo/auto/gen_semaine

# On en déduit les prévisions pour la semaine prochaine
function tdh:meteo/auto/gen_texte_previsions_sp
# Si la proba de pluie est de moins de 1%, on prévoit du beau temps
# (sauf si la proba d'orage est supérieure à 1% auquel cas on avertit des risques d'orage)
execute if score #meteoSP_calme probaPluie matches ..100 if score #meteoSP_calme probaOrage matches ..99 run function tdh:meteo/auto/prevision/message/semaine_prochaine/beau_temps
execute if score #meteoSP_calme probaPluie matches ..100 if score #meteoSP_calme probaOrage matches 100.. run function tdh:meteo/auto/prevision/message/semaine_prochaine/risque_orage

# Si la proba de pluie ou d'orage est de plus de 1%
execute if score #meteoSP_calme probaPluie matches 101.. if score #meteoSP_calme probaOrage matches ..99 run function tdh:meteo/auto/prevision/message/semaine_prochaine/pluie
execute if score #meteoSP_calme probaPluie matches 101.. if score #meteoSP_calme probaOrage matches 100.. run function tdh:meteo/auto/prevision/message/semaine_prochaine/orages
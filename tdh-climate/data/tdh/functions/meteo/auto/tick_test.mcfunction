# Si on a dépassé l'horaire de prochain update météorologique, on exécute le "vrai" tick
execute if score #temps idJour = #meteo jourUpdate if score #temps currentTdhTick >= #meteo tickUpdate run function tdh:meteo/auto/tick
execute if score #temps idJour > #meteo jourUpdate run function tdh:meteo/auto/tick
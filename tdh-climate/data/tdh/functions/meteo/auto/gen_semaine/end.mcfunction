# On sélectionne le scénario actuellement en tête de liste
data modify storage tdh:meteo calcul.scenario set from storage tdh:meteo calcul.mois.scenarios[0]

# On supprime les données du mois actuel du storage, puisqu'on a terminé le calcul
data remove storage tdh:meteo calcul.mois

# On nettoie les variables temporaires
scoreboard players reset #meteo temp
scoreboard players reset #meteo temp2
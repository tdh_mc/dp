# Dans cette fonction récursive, on va à chaque fois retrancher le poids du scénario actuel (passé dans #meteo temp),
# et si ce nombre passe au-dessous de 0, cela signifie que le scénario actuel est celui qu'on a choisi

# On enregistre le poids de ce scénario dans #meteo temp2
execute store result score #meteo temp2 run data get storage tdh:meteo calcul.mois.scenarios[0].poids

# S'il ne reste qu'un seul scénario, quel que soit son poids, on passe le paramètre à -1 pour forcer sa sélection
execute unless data storage tdh:meteo calcul.mois.scenarios[1] run scoreboard players set #meteo temp -1

# Si le nombre n'est pas encore négatif, on retranche le poids du scénario actuel
execute if score #meteo temp matches 0.. run scoreboard players operation #meteo temp -= #meteo temp2

# Si le nombre est maintenant négatif (que ce soit à cause du manque de scénarios ou de la soustraction ci-dessus),
# on termine la récursion et on sélectionne le scénario actuel
execute if score #meteo temp matches ..-1 run function tdh:meteo/auto/gen_semaine/end
# Dans le cas contraire, on continue à itérer
execute if score #meteo temp matches 0.. run data remove storage tdh:meteo calcul.mois.scenarios[0]
execute if score #meteo temp matches 0.. run function tdh:meteo/auto/gen_semaine/next
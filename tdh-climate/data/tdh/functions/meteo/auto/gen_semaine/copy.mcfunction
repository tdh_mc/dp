# Cette fonction est exécutée à la toute fin de gen_semaine,
# et copie les données du scénario trouvé par la fonction de recherche dans des variables scoreboard

# Ces données sont enregistrées pour la prochaine période (SP = semaine prochaine),
# car on génère toujours avec une période d'avance pour afficher 2 prévisions successives (période actuelle + suivante)

# Pour chaque paramètre, on procède comme suit :
# - enregistrement du #random min et #random max depuis le storage
# - génération d'un nombre aléatoire
# - copie du nombre aléatoire vers la bonne variable

# Après chaque ensemble de paramètres (calme / pluie / orage),
# on vérifie l'intégrité des données (somme des 2 probas < 10000, et intensiteMin <= intensiteMax)

## DONNÉES POUR TEMPS CLAIR
# Probabilité de pluie
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.calme.pluie.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.calme.pluie.max
function tdh:random
scoreboard players operation #meteoSP_calme probaPluie = #random temp
# Probabilité d'orage
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.calme.orage.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.calme.orage.max
function tdh:random
scoreboard players operation #meteoSP_calme probaOrage = #random temp
# Intensité minimale
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.calme.intensiteMin.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.calme.intensiteMin.max
function tdh:random
scoreboard players operation #meteoSP_calme intensiteMin = #random temp
# Intensité maximale
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.calme.intensiteMax.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.calme.intensiteMax.max
function tdh:random
scoreboard players operation #meteoSP_calme intensiteMax = #random temp

# On vérifie les paramètres
scoreboard players operation #meteoSP_calme intensiteMin < #meteoSP_calme intensiteMax
scoreboard players operation #meteoSP_calme probaChgmt = #meteoSP_calme probaPluie
scoreboard players operation #meteoSP_calme probaChgmt += #meteoSP_calme probaOrage
execute if score #meteoSP_calme probaChgmt matches 10001.. run scoreboard players set #meteoSP_calme probaPluie 10000
execute if score #meteoSP_calme probaChgmt matches 10001.. run scoreboard players operation #meteoSP_calme probaPluie -= #meteoSP_calme probaOrage
execute if score #meteoSP_calme probaChgmt matches 10001.. run scoreboard players set #meteoSP_calme probaChgmt 10000


## DONNÉES POUR TEMPS PLUVIEUX
# Probabilité de temps clair
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.pluie.calme.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.pluie.calme.max
function tdh:random
scoreboard players operation #meteoSP_pluie probaCalme = #random temp
# Probabilité d'orage
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.pluie.orage.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.pluie.orage.max
function tdh:random
scoreboard players operation #meteoSP_pluie probaOrage = #random temp
# Intensité minimale
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.pluie.intensiteMin.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.pluie.intensiteMin.max
function tdh:random
scoreboard players operation #meteoSP_pluie intensiteMin = #random temp
# Intensité maximale
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.pluie.intensiteMax.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.pluie.intensiteMax.max
function tdh:random
scoreboard players operation #meteoSP_pluie intensiteMax = #random temp

# On vérifie les paramètres
scoreboard players operation #meteoSP_pluie intensiteMin < #meteoSP_pluie intensiteMax
scoreboard players operation #meteoSP_pluie probaChgmt = #meteoSP_pluie probaCalme
scoreboard players operation #meteoSP_pluie probaChgmt += #meteoSP_pluie probaOrage
execute if score #meteoSP_pluie probaChgmt matches 10001.. run scoreboard players set #meteoSP_pluie probaCalme 10000
execute if score #meteoSP_pluie probaChgmt matches 10001.. run scoreboard players operation #meteoSP_pluie probaCalme -= #meteoSP_pluie probaOrage
execute if score #meteoSP_pluie probaChgmt matches 10001.. run scoreboard players set #meteoSP_pluie probaChgmt 10000


## DONNÉES POUR TEMPS ORAGEUX
# Probabilité de temps clair
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.orage.calme.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.orage.calme.max
function tdh:random
scoreboard players operation #meteoSP_orage probaCalme = #random temp
# Probabilité de pluie
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.orage.pluie.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.orage.pluie.max
function tdh:random
scoreboard players operation #meteoSP_orage probaPluie = #random temp
# Intensité minimale
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.orage.intensiteMin.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.orage.intensiteMin.max
function tdh:random
scoreboard players operation #meteoSP_orage intensiteMin = #random temp
# Intensité maximale
execute store result score #random min run data get storage tdh:meteo recherche.resultat.poids.orage.intensiteMax.min
execute store result score #random max run data get storage tdh:meteo recherche.resultat.poids.orage.intensiteMax.max
function tdh:random
scoreboard players operation #meteoSP_orage intensiteMax = #random temp

# On vérifie les paramètres
scoreboard players operation #meteoSP_orage intensiteMin < #meteoSP_orage intensiteMax
scoreboard players operation #meteoSP_orage probaChgmt = #meteoSP_orage probaPluie
scoreboard players operation #meteoSP_orage probaChgmt += #meteoSP_orage probaOrage
execute if score #meteoSP_orage probaChgmt matches 10001.. run scoreboard players set #meteoSP_orage probaPluie 10000
execute if score #meteoSP_orage probaChgmt matches 10001.. run scoreboard players operation #meteoSP_orage probaPluie -= #meteoSP_orage probaOrage
execute if score #meteoSP_orage probaChgmt matches 10001.. run scoreboard players set #meteoSP_orage probaChgmt 10000
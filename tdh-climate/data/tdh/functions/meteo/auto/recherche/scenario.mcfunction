# On recherche un scénario dans le storage
# Le nom du scénario à rechercher est contenu dans le storage tdh:meteo recherche.nom,
# et le résultat éventuel se trouvera après la recherche dans tdh:meteo recherche.resultat

# On enregistre le nombre de scénarios dans le storage
execute store result score #meteoRecherche max run data get storage tdh:meteo index.scenario

# Pour chaque scénario, on vérifie si le nom correspond
function tdh:meteo/auto/recherche/scenario/next
# Après cette fonction, le champ tdh:meteo recherche.resultat contient un scénario correspondant à la recherche (si ce n'est pas le cas, un message d'erreur est affiché)
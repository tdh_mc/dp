# S'il ne reste plus d'itérations disponibles, on affiche en debug un warning
execute if score #meteoRecherche max matches ..0 run tellraw @a[tag=MeteoDebugLog] [{"text":"","color":"gray"},{"text":"Warning tdh-climate : ","color":"gold","bold":"true"},{"text":"Impossible de trouver le scénario "},{"nbt":"recherche.nom","storage":"tdh:meteo","color":"gold","italic":"true"},{"text":" dans l'index. Le scénario sélectionné sera incorrect. Merci de vérifier les "},{"text":"paramètres","color":"gold","hoverEvent":{"action":"show_text","contents":[{"text":"Cliquer ici pour lancer le configurateur ","color":"gray"},{"text":"tdh-climate","color":"gold"},{"text":"."}]},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/_config"}},{"text":" pour le mois "},{"nbt":"date.mois.deDu","storage":"tdh:datetime"},{"nbt":"date.mois.min","storage":"tdh:datetime"},{"text":"."}]

# On enregistre le scénario actuellement en tête de liste comme résultat de la recherche
data modify storage tdh:meteo recherche.resultat set from storage tdh:meteo index.scenario[0]

# On réinitialise les variables temporaires
scoreboard players reset #meteoRecherche temp
scoreboard players reset #meteoRecherche max
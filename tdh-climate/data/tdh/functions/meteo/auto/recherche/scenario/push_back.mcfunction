# On renvoie l'élément actuel à la fin du tableau
data modify storage tdh:meteo index.scenario append from storage tdh:meteo index.scenario[0]
data remove storage tdh:meteo index.scenario[0]

# On retranche 1 au maximum d'itérations
scoreboard players remove #meteoRecherche max 1

# S'il ne reste plus d'itération possible, on termine la fonction
execute if score #meteoRecherche max matches ..0 run function tdh:meteo/auto/recherche/scenario/end
# Sinon, on relance la fonction d'itération
execute if score #meteoRecherche max matches 1.. run function tdh:meteo/auto/recherche/scenario/next
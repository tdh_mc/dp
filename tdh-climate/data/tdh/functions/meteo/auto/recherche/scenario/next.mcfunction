# On copie dans un buffer le nom que l'on recherche
data modify storage tdh:meteo recherche.buffer set from storage tdh:meteo recherche.nom

# Message d'erreur si l'élément actuel ne contient pas de nom
execute unless data storage tdh:meteo index.scenario[0].nom run tellraw @a[tag=MeteoDebugLog] [{"text":"","color":"gray"},{"text":"Erreur tdh-climate : ","color":"red","bold":"true"},{"text":"Un des éléments du storage "},{"text":"tdh:meteo/index.scenario","color":"gold","italic":"true"},{"text":" ne contient pas de nom. Cela entraîne des problèmes lors de la recherche, qui renverra presque toujours ce scénario de manière incorrecte."}]

# On copie dans le même buffer le nom du scénario actuellement en tête de liste,
# et on enregistre le succès ou l'échec de la commande dans #meteoRecherche temp
execute store success score #meteoRecherche temp run data modify storage tdh:meteo recherche.buffer set from storage tdh:meteo index.scenario[0].nom

# Si la commande échoue, c'est que le scénario actuel est le bon
execute if score #meteoRecherche temp matches ..0 run function tdh:meteo/auto/recherche/scenario/end
# Si elle a réussi, alors on renvoie l'élément à la fin et on continue à itérer
execute if score #meteoRecherche temp matches 1.. run function tdh:meteo/auto/recherche/scenario/push_back
# Lorsqu'on change de journée
# Si on est jeudi ou +, on ajuste légèrement les probabilités de pluie/orage en fonction des probabilités de la semaine suivante
execute if score #temps jourSemaine matches 4.. run function tdh:meteo/auto/prochaine_journee_evolution

# Si on est lundi, on passe aux probabilités de la nouvelle semaine (et on génère la semaine prochaine)
execute if score #temps jourSemaine matches 1 run function tdh:meteo/auto/prochaine_semaine

# On génère les probabilités de changement (utilisées pour le calcul du tick météo)
function tdh:meteo/auto/gen_proba_changement

# On génère ensuite les textes des prévisions de la journée dans le storage
function tdh:meteo/auto/gen_texte_previsions
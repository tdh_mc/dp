# Ce "tick" est exécuté une fois toutes les 2h20 environ, et lance les autres fonctions,
# entraînant le changement de la météo et des prévisions

# On récupère 1 variable aléatoire entre 0 et 9999 dans #random temp
scoreboard players set #random min 0
scoreboard players set #random max 10000
function tdh:random

# On enregistre la météo suivante (par défaut, la même qu'actuellement) et l'intensité actuelle
scoreboard players operation #meteo suivante = #meteo actuelle
scoreboard players operation #meteo temp5 = #meteo intensite

# En fonction de la météo actuelle, on lance une des sous-fonctions
execute if score #meteo actuelle matches 1 run function tdh:meteo/auto/tick/calme
execute if score #meteo actuelle matches 2 run function tdh:meteo/auto/tick/pluie
execute if score #meteo actuelle matches 3 run function tdh:meteo/auto/tick/orage

# On récupère la nouvelle météo actuelle (si elle a changé, on envoie l'event correspondant)
execute unless score #meteo actuelle = #meteo suivante run function #tdh:meteo/on_weather_changed
scoreboard players operation #meteo actuelle = #meteo suivante

# Si l'intensité a changé, on envoie l'event correspondant
execute unless score #meteo temp5 = #meteo intensite run function #tdh:meteo/on_intensity_changed
scoreboard players reset #meteo temp5


# On calcule le prochain horaire d'update de la météo
# On calcule le tick d'update en ajoutant la fréquence d'update au tick actuel
scoreboard players operation #meteo tickUpdate = #temps currentTdhTick
scoreboard players operation #meteo tickUpdate += #meteo frequenceUpdate
# Si le résultat de l'addition est supérieur à une journée complète, alors on ajoute un jour
scoreboard players operation #meteo jourUpdate = #temps idJour
execute if score #meteo tickUpdate >= #temps fullDayTicks run scoreboard players add #meteo jourUpdate 1
execute if score #meteo tickUpdate >= #temps fullDayTicks run scoreboard players operation #meteo tickUpdate -= #temps fullDayTicks
# On génère une salle en bedrock à notre position
fill ~-2 ~-1 ~-2 ~2 ~3 ~2 bedrock hollow

# On place à l'intérieur de cette salle les différentes entités nécessaires
execute unless entity @e[type=item_frame,tag=SpawnMonde] run summon item_frame ~ ~ ~ {Tags:["SpawnMonde"],Fixed:1b,Invisible:1b,Invulnerable:1b,Facing:1b,Item:{Count:1b,id:"minecraft:carrot_on_a_stick",tag:{display:{Name:'"Spawn TDH"'}}}}
execute unless entity @e[type=armor_stand,tag=DirectionVent] run summon armor_stand ~1 ~ ~1 {Tags:["DirectionVent"],Invulnerable:1b,NoGravity:1b,CustomName:'"Direction du vent"'}
# On enregistre les phrases de transition
data modify storage tdh:meteo texte.transition.calme_pluie set value '[{"text":"Le temps se gâte...","color":"gray"}]'
data modify storage tdh:meteo texte.transition.calme_orage set value '[{"text":"Le tonnerre gronde...","color":"gray"}]'
data modify storage tdh:meteo texte.transition.pluie_calme set value '[{"text":"La pluie semble s\'éloigner.","color":"gray"}]'
data modify storage tdh:meteo texte.transition.pluie_orage set value '[{"text":"Le tonnerre gronde...","color":"gray"}]'
data modify storage tdh:meteo texte.transition.orage_calme set value '[{"text":"Le ciel semble se découvrir.","color":"gray"}]'
data modify storage tdh:meteo texte.transition.orage_pluie set value '[{"text":"L\'orage semble se calmer.","color":"gray"}]'
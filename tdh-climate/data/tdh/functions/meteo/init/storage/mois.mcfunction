# On initialise une liste vide dans le storage météo pour stocker les semaines-types possibles, chaque mois
# On supporte maximum 24 mois dans tdh-time, on initialise donc 24 éléments dans le storage pour chacun de ces mois
data modify storage tdh:meteo index.mois set value [{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]

# Mois de janvier. Peu de précipitations fortes mais peu de beau temps aussi
# 30% beau temps, 20% rares averses, 50% pluie (24% faible / 16% normale / 10% forte)
data modify storage tdh:meteo index.mois[0] set value {poids:50,scenarios:[{poids:15,nom:"Beau temps"},{poids:10,nom:"Rares averses"},{poids:12,nom:"Pluie faible"},{poids:8,nom:"Pluie"},{poids:5,nom:"Pluie forte"}]}
# Mois de février. Plus de beau temps qu'en janvier
# 50% beau temps, 20% rares averses, 30% pluie (10% faible, 10% normale, 10% forte)
data modify storage tdh:meteo index.mois[1] set value {poids:50,scenarios:[{poids:25,nom:"Beau temps"},{poids:10,nom:"Rares averses"},{poids:5,nom:"Pluie faible"},{poids:5,nom:"Pluie"},{poids:5,nom:"Pluie forte"}]}
# Mois de mars. Premier mois où peuvent se produire des giboulées
# 44% beau temps, 20% giboulées, 6% pluies éparses, 16% averses, 24% pluie (10% faible / 4% normale / 10% forte)
data modify storage tdh:meteo index.mois[2] set value {poids:50,scenarios:[{poids:22,nom:"Beau temps"},{poids:10,nom:"Giboulées"},{poids:3,nom:"Pluies éparses"},{poids:3,nom:"Rares averses"},{poids:5,nom:"Pluie faible"},{poids:2,nom:"Pluie"},{poids:5,nom:"Pluie forte"}]}
# Mois d'avril, le coeur des giboulées
# 34% beau temps, 34% giboulées, 18% averses (dont 6% orageuses), 14% pluie (dont 10% forte)
data modify storage tdh:meteo index.mois[3] set value {poids:50,scenarios:[{poids:17,nom:"Beau temps"},{poids:17,nom:"Giboulées"},{poids:3,nom:"Pluies éparses"},{poids:3,nom:"Averses orageuses"},{poids:3,nom:"Rares averses"},{poids:2,nom:"Pluie"},{poids:5,nom:"Pluie forte"}]}
# Mois de mai, beaucoup de beau temps et rarement des précipitations
# 60% beau temps, 10% averses, 20% pluie (dont 4% forte et 6% avec orage), 10% orage (6% petit et 4% gros)
data modify storage tdh:meteo index.mois[4] set value {poids:50,scenarios:[{poids:30,nom:"Beau temps"},{poids:3,nom:"Pluies éparses"},{poids:2,nom:"Rares averses"},{poids:5,nom:"Pluie"},{poids:2,nom:"Pluie forte"},{poids:3,nom:"Pluie et orage"},{poids:3,nom:"Petit orage"},{poids:2,nom:"Gros orage"}]}
# Mois de juin, encore + de beau temps et d'orage
# 70% beau temps, 10% averses, 12% pluie (dont 2% faible et 6% avec orage), 8% orage (dont 6% gros)
data modify storage tdh:meteo index.mois[5] set value {poids:50,scenarios:[{poids:35,nom:"Beau temps"},{poids:2,nom:"Pluies éparses"},{poids:3,nom:"Rares averses"},{poids:2,nom:"Pluie"},{poids:1,nom:"Pluie faible"},{poids:3,nom:"Pluie et orage"},{poids:1,nom:"Petit orage"},{poids:3,nom:"Gros orage"}]}
# Mois de juillet, soit du beau temps (beaucoup) soit (rarement) de très gros orages
# 70% beau temps, 30% orages (dont 6% averses, 10% petit, 14% gros)
data modify storage tdh:meteo index.mois[6] set value {poids:50,scenarios:[{poids:35,nom:"Beau temps"},{poids:3,nom:"Averses orageuses"},{poids:5,nom:"Petit orage"},{poids:7,nom:"Gros orage"}]}
# Mois d'août, soit du beau temps (beaucoup) soit (rarement) de très gros orages
# 76% beau temps, 24% orages (dont 6% averses, 8% petit, 10% gros)
data modify storage tdh:meteo index.mois[7] set value {poids:50,scenarios:[{poids:38,nom:"Beau temps"},{poids:3,nom:"Averses orageuses"},{poids:4,nom:"Petit orage"},{poids:5,nom:"Gros orage"}]}
# Mois de septembre, moins de beau temps et pas mal de pluie faible
# 50% beau temps, 16% averses, 28% pluie (dont 16% faible et 4% forte), 6% orage (4% petit et 2% gros)
data modify storage tdh:meteo index.mois[8] set value {poids:50,scenarios:[{poids:25,nom:"Beau temps"},{poids:5,nom:"Rares averses"},{poids:3,nom:"Pluies éparses"},{poids:8,nom:"Pluie faible"},{poids:4,nom:"Pluie"},{poids:2,nom:"Pluie forte"},{poids:2,nom:"Petit orage"},{poids:1,nom:"Gros orage"}]}
# Mois d'octobre, avec pas mal de flotte
# 30% beau temps, 28% averses (dont 2% orageuses), 40% pluie (dont 12% faible, 10% forte), 2% orage (petit)
data modify storage tdh:meteo index.mois[9] set value {poids:50,scenarios:[{poids:15,nom:"Beau temps"},{poids:6,nom:"Rares averses"},{poids:7,nom:"Pluies éparses"},{poids:1,nom:"Averses orageuses"},{poids:6,nom:"Pluie faible"},{poids:9,nom:"Pluie"},{poids:5,nom:"Pluie forte"},{poids:1,nom:"Petit orage"}]}
# Mois de novembre, encore pire qu'octobre
# 20% beau temps, 20% averses, 60% pluie (dont 30% faible, 10% forte)
data modify storage tdh:meteo index.mois[10] set value {poids:50,scenarios:[{poids:10,nom:"Beau temps"},{poids:5,nom:"Rares averses"},{poids:5,nom:"Pluies éparses"},{poids:15,nom:"Pluie faible"},{poids:10,nom:"Pluie"},{poids:5,nom:"Pluie forte"}]}
# Mois de décembre, le retour d'un peu de beaux jours avec toutefois quelques précipitations
# 40% beau temps, 16% averses, 44% pluie (dont 12% faible, 4% forte)
data modify storage tdh:meteo index.mois[11] set value {poids:50,scenarios:[{poids:20,nom:"Beau temps"},{poids:5,nom:"Rares averses"},{poids:3,nom:"Pluies éparses"},{poids:6,nom:"Pluie faible"},{poids:14,nom:"Pluie"},{poids:2,nom:"Pluie forte"}]}
# Cette fonction ne doit PAS être appelée avec un "execute at" ou "execute positioned as"
# car elle a besoin d'être à la position du spawn pour fonctionner

# On se place à l'altitude 1, et on vérifie si toutes les positions qu'occuperait la "cabine" sont bien en blocs naturels
execute positioned ~ 1 ~ run function tdh:meteo/init/spawn/test_placement
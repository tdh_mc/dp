# Clear = 1, Rain = 2, Storm = 3
scoreboard objectives add actuelle dummy
scoreboard objectives add suivante dummy

scoreboard players set #meteo_calme actuelle 1
scoreboard players set #meteo_pluie actuelle 2
scoreboard players set #meteo_orage actuelle 3

scoreboard objectives add duree dummy "Durée"
scoreboard objectives add dureeMin dummy "Durée minimale"
scoreboard objectives add dureeMax dummy "Durée maximale"

execute unless score #meteo frequenceUpdate matches 1.. run scoreboard players set #meteo frequenceUpdate 24
# La fréquence d'update est déterminée par journée
# Avec une fréquence d'update de 24, on màj la météo précisément 1x/heure
# Avec une fréquence d'update de 48, ce serait 2x / heure

# Les probabilités sont données sur 10'000
scoreboard objectives add probaPluie dummy "Probabilité de pluie"
scoreboard objectives add probaOrage dummy "Probabilité d'orage"
scoreboard objectives add probaCalme dummy "Probabilité de temps calme"
scoreboard objectives add probaChgmt dummy "Probabilité de changement"

# L'intensité de la pluie va de 1 (petite pluie) à 4 (cataclysme)
scoreboard objectives add intensiteMin dummy "Intensité minimale"
scoreboard objectives add intensite dummy "Intensité"
scoreboard objectives add intensiteMax dummy "Intensité maximale"

# On active tous les systèmes s'ils n'ont pas encore d'état d'activation
# Par défaut les forêts peuvent se propager ; 
execute unless score #pousseForets on matches 0..1 run scoreboard players set #pousseForets on 1
# Par défaut, la neige est activée ; elle s'actualise jusqu'à une distance de 96 ; et s'actualise tous les 1 ticks
execute unless score #neige on matches 0..1 run scoreboard players set #neige on 1
execute unless score #neige distance matches 20.. run scoreboard players set #neige distance 96
execute unless score #neige vitesse matches 1.. run scoreboard players set #neige vitesse 1
execute unless score #neige frequenceUpdate matches 1.. run scoreboard players set #neige frequenceUpdate 1
execute unless score #neige jourUpdate matches 1.. run scoreboard players set #neige jourUpdate 1
execute unless score #neige tickUpdate matches 0.. run scoreboard players set #neige tickUpdate 0
execute unless score #neige intensiteMin matches 0.. run scoreboard players set #neige intensiteMin 1
# Par défaut la neige peut fondre ; elle le fait jusqu'à une distance de 96, et s'actualise tous les 1 ticks
execute unless score #fonteNeige on matches 0..1 run scoreboard players set #fonteNeige on 1
execute unless score #fonteNeige distance matches 20.. run scoreboard players set #fonteNeige distance 96
execute unless score #fonteNeige vitesse matches 1.. run scoreboard players set #fonteNeige vitesse 1
execute unless score #fonteNeige frequenceUpdate matches 1.. run scoreboard players set #fonteNeige frequenceUpdate 1
execute unless score #fonteNeige jourUpdate matches 1.. run scoreboard players set #fonteNeige jourUpdate 1
execute unless score #fonteNeige tickUpdate matches 0.. run scoreboard players set #fonteNeige tickUpdate 0
# Par défaut la pluie peut émettre des particules à partir de l'intensité 2/4, avec (4/12/20) particules/tick selon l'intensité
execute unless score #pluieParticules on matches 0..1 run scoreboard players set #pluieParticules on 1
execute unless score #pluieParticules intensite matches 0.. run scoreboard players set #pluieParticules intensite 0
execute unless score #pluieParticules_1 intensite matches 0.. run scoreboard players set #pluieParticules_1 intensite 0
execute unless score #pluieParticules_2 intensite matches 0.. run scoreboard players set #pluieParticules_2 intensite 4
execute unless score #pluieParticules_3 intensite matches 0.. run scoreboard players set #pluieParticules_3 intensite 12
execute unless score #pluieParticules_4 intensite matches 0.. run scoreboard players set #pluieParticules_4 intensite 20
# Par défaut un orage aura davantage d'éclairs à partir de l'intensité 2, 
execute unless score #orage on matches 0..1 run scoreboard players set #orage on 1
execute unless score #orage probaChgmt matches 0.. run scoreboard players set #orage probaChgmt 0
execute unless score #orage_1 probaChgmt matches 0.. run scoreboard players set #orage_1 probaChgmt 0
execute unless score #orage_2 probaChgmt matches 0.. run scoreboard players set #orage_2 probaChgmt 2
execute unless score #orage_3 probaChgmt matches 0.. run scoreboard players set #orage_3 probaChgmt 8
execute unless score #orage_4 probaChgmt matches 0.. run scoreboard players set #orage_4 probaChgmt 32

# Réglage des paramètres de la direction du vent
execute unless score #vent on matches 0..1 run scoreboard players set #vent on 1
execute unless score #vent vitesse matches 0.. run scoreboard players set #vent vitesse 0
# Intensité 0 (vent 0m/s, 3mn par check)
execute unless score #vent_0 vitesseMin matches 0.. run scoreboard players set #vent_0 vitesseMin 0
execute unless score #vent_0 vitesseMax matches 0.. run scoreboard players set #vent_0 vitesseMax 0
execute unless score #vent_0 dureeMin matches 1.. run scoreboard players set #vent_0 dureeMin 3600
execute unless score #vent_0 dureeMax matches 1.. run scoreboard players set #vent_0 dureeMax 3600
execute unless score #vent_0 rotationMax matches 1..180 run scoreboard players set #vent_0 rotationMax 0
# Intensité 1 (vent 1-6m/s, 25-100s par check pour rotation <= 4°)
execute unless score #vent_1 vitesseMin matches 1.. run scoreboard players set #vent_1 vitesseMin 1
execute unless score #vent_1 vitesseMax matches 1.. run scoreboard players set #vent_1 vitesseMax 6
execute unless score #vent_1 dureeMin matches 1.. run scoreboard players set #vent_1 dureeMin 500
execute unless score #vent_1 dureeMax matches 1.. run scoreboard players set #vent_1 dureeMax 2000
execute unless score #vent_1 rotationMax matches 1..180 run scoreboard players set #vent_1 rotationMax 4
# Intensité 2 (vent 7-13m/s, 15-40s par check pour rotation <= 6°)
execute unless score #vent_2 vitesseMin matches 1.. run scoreboard players set #vent_2 vitesseMin 7
execute unless score #vent_2 vitesseMax matches 1.. run scoreboard players set #vent_2 vitesseMax 13
execute unless score #vent_2 dureeMin matches 1.. run scoreboard players set #vent_2 dureeMin 300
execute unless score #vent_2 dureeMax matches 1.. run scoreboard players set #vent_2 dureeMax 800
execute unless score #vent_2 rotationMax matches 1..180 run scoreboard players set #vent_2 rotationMax 6
# Intensité 3 (vent 14-24m/s, 10-25s par check pour rotation <= 10°)
execute unless score #vent_3 vitesseMin matches 1.. run scoreboard players set #vent_3 vitesseMin 14
execute unless score #vent_3 vitesseMax matches 1.. run scoreboard players set #vent_3 vitesseMax 24
execute unless score #vent_3 dureeMin matches 1.. run scoreboard players set #vent_3 dureeMin 200
execute unless score #vent_3 dureeMax matches 1.. run scoreboard players set #vent_3 dureeMax 500
execute unless score #vent_3 rotationMax matches 1..180 run scoreboard players set #vent_3 rotationMax 10
# Intensité 4 (vent 25-40m/s, 3-15s par check pour rotation <= 15°)
execute unless score #vent_4 vitesseMin matches 1.. run scoreboard players set #vent_4 vitesseMin 25
execute unless score #vent_4 vitesseMax matches 1.. run scoreboard players set #vent_4 vitesseMax 40
execute unless score #vent_4 dureeMin matches 1.. run scoreboard players set #vent_4 dureeMin 60
execute unless score #vent_4 dureeMax matches 1.. run scoreboard players set #vent_4 dureeMax 300
execute unless score #vent_4 rotationMax matches 1..180 run scoreboard players set #vent_4 rotationMax 15

# Réglage des paramètres de l'actualisation automatique
# Par défaut l'actualisation automatique est DÉSACTIVÉE
execute unless score #meteoActuAuto on matches 0..1 run scoreboard players set #meteoActuAuto on 0
# Par défaut on n'actualise que s'il n'y a aucun joueur présent
execute unless score #meteoActuAuto playersOnline matches 0.. run scoreboard players set #meteoActuAuto playersOnline 0
# Par défaut on peut forceload 3 positions à la fois, pour une durée de 5 minutes
# (l'actualisation du timer se fait toutes les ~30 secondes)
execute unless score #meteoActuAuto nombre matches 1.. run scoreboard players set #meteoActuAuto nombre 3
execute unless score #meteoActuAuto duree matches 1.. run scoreboard players set #meteoActuAuto duree 10
# Par défaut, on se centre sur la position (0,0) et on inclut 4 chunks
# Il va sans dire qu'il faudra config ingame de sorte à ce que ça soit + étendu, et adapté au monde
execute unless score #meteoActuAuto xMin matches -999999.. run scoreboard players set #meteoActuAuto xMin -16
execute unless score #meteoActuAuto xMax matches -999999.. run scoreboard players set #meteoActuAuto xMax 16
execute unless score #meteoActuAuto zMin matches -999999.. run scoreboard players set #meteoActuAuto zMin -16
execute unless score #meteoActuAuto zMax matches -999999.. run scoreboard players set #meteoActuAuto zMax 16

# Réglages des saisons
# Nombre de datapacks subdivisant une année (par défaut il y en a 1 par mois)
execute unless score #saison nombre matches 1.. run scoreboard players set #saison nombre 12


# Variables obsolètes à retirer
scoreboard objectives remove CurrentWeather
scoreboard objectives remove PreviousWeather
scoreboard objectives remove CustomWeatherOn
scoreboard objectives remove RainOdds
scoreboard objectives remove StormOdds
scoreboard objectives remove MeanRainCycles
scoreboard objectives remove MeanStormCycles
scoreboard objectives remove RainIntensity
scoreboard objectives remove MaxRainIntensity
scoreboard objectives remove CyclesElapsed
scoreboard objectives remove SummonedStands
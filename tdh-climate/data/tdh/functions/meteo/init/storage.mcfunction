# On stocke les scénarios si le storage n'en contient pas encore
execute unless data storage tdh:meteo index.scenario[0] run function tdh:meteo/init/storage/semaines

# On stocke les configurations des mois si le storage n'en contient pas encore
execute unless data storage tdh:meteo index.mois[0].poids run function tdh:meteo/init/storage/mois

# On stocke les configurations des textes à afficher si le storage n'en contient pas encore
execute unless data storage tdh:meteo texte run function tdh:meteo/init/storage/texte
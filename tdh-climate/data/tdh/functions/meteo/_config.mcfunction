# Configurateur du climat
# Permet de choisir les paramètres de la météo selon les mois de l'année
# et d'activer/désactiver des fonctionnalités optionnelles du module

# On démarre la config
function tdh:meteo/config/init

# On affiche les différentes options
tellraw @s [{"text":"CONFIGURATEUR TDH-CLIMATE","color":"gold"}]
tellraw @s [{"text":"Voici la configuration actuelle de ","color":"gray"},{"text":"tdh-climate","bold":"true"},{"text":" :\n——————————————————————————"}]

# -- Programmation des scénarios et des mois --
# Liste des scénarios (10000) (la config se passe dans un sous-menu)
execute store result score #meteo temp run data get storage tdh:meteo index.scenario
tellraw @s [{"text":"Il y a ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"[Avancé]","color":"red"},{"text":" Modification des scénarios hebdomadaires.\n","color":"white"},{"text":"Par précaution, il est préférable de mettre ","color":"yellow","italic":"true","extra":[{"text":"tdh-time","bold":"true","color":"gold"},{"text":" en "},{"text":"pause","bold":"true"},{"text":" pendant la modification des scénarios."}]}]},"clickEvent":{"action":"run_command","value":"/trigger config set 10000"}},{"score":{"name":"#meteo","objective":"temp"},"color":"gold"},{"text":" scénarios","color":"yellow"},{"text":" enregistrés."}]
# Configuration des mois (20000) (la config se passe dans un sous-menu)
tellraw @s [{"text":"Accéder au ","color":"gray","hoverEvent":{"action":"show_text","contents":[{"text":"[Avancé]","color":"red"},{"text":" Modification des poids mensuels de chaque scénario.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/trigger config set 20000"}},{"text":" configurateur des mois","color":"gold"},{"text":"."}]

tellraw @s [{"text":"——————————————————————————","color":"gray"}]

# -- Options météo/précipitations --
# Accumulation de la neige (30000)
# Activer/désactiver (30010/30011) + accès au sous-menu par une fonction
execute unless score #neige on matches 1 run tellraw @s [{"text":"La neige ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour activer l'accumulation de la neige."},"clickEvent":{"action":"run_command","value":"/trigger config set 30010"}},{"text":"ne s'accumule pas","color":"red"},{"text":" au-delà d'une seule épaisseur."}]
execute if score #neige on matches 1 run tellraw @s [{"text":"La neige ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour désactiver l'accumulation de la neige."},"clickEvent":{"action":"run_command","value":"/trigger config set 30011"}},{"text":"s'accumule","color":"green"},{"text":". "},{"text":"(","color":"gray","hoverEvent":{"action":"show_text","contents":"Accéder aux paramètres avancés de l'accumulation de la neige."},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/chute_neige"},"extra":[{"text":"paramètres","color":"yellow","italic":"true"},{"text":")"}]}]

# Fonte de la neige (31000)
# Activer/désactiver (31010/31011) + accès au sous-menu par une fonction
execute unless score #fonteNeige on matches 1 run tellraw @s [{"text":"La neige ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour activer la fonte de la neige."},"clickEvent":{"action":"run_command","value":"/trigger config set 31010"}},{"text":"ne fond pas","color":"red"},{"text":"."}]
execute if score #fonteNeige on matches 1 run tellraw @s [{"text":"La neige ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour désactiver la fonte de la neige."},"clickEvent":{"action":"run_command","value":"/trigger config set 31011"}},{"text":"fond normalement","color":"green"},{"text":". "},{"text":"(","color":"gray","hoverEvent":{"action":"show_text","contents":"Accéder aux paramètres avancés de la fonte de la neige."},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/fonte_neige"},"extra":[{"text":"paramètres","color":"yellow","italic":"true"},{"text":")"}]}]

# Particules de pluie/neige (32000)
# Activer/désactiver (32010/32011) + accès au sous-menu par une fonction
execute unless score #pluieParticules on matches 1 run tellraw @s [{"text":"Les particules des précipitations sont ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour activer les particules des précipitations."},"clickEvent":{"action":"run_command","value":"/trigger config set 32010"}},{"text":"désactivées","color":"red"},{"text":"."}]
execute if score #pluieParticules on matches 1 run tellraw @s [{"text":"Les particules des précipitations sont ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour désactiver les particules des précipitations."},"clickEvent":{"action":"run_command","value":"/trigger config set 32011"}},{"text":"activées","color":"green"},{"text":". "},{"text":"(","color":"gray","hoverEvent":{"action":"show_text","contents":"Accéder aux paramètres avancés des particules de pluie et de neige."},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/particules_pluie"},"extra":[{"text":"paramètres","color":"yellow","italic":"true"},{"text":")"}]}]

# Éclairs selon l'intensité (33000)
# Activer/désactiver (33010/33011) + accès au sous-menu par une fonction
execute unless score #orage on matches 1 run tellraw @s [{"text":"Seuls les ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour activer les éclairs supplémentaires selon l'intensité de la météo."},"clickEvent":{"action":"run_command","value":"/trigger config set 33010"}},{"text":"éclairs du jeu vanilla","color":"red"},{"text":" sont activés."}]
execute if score #orage on matches 1 run tellraw @s [{"text":"Les éclairs selon l'intensité météo sont ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour conserver uniquement les éclairs du jeu vanilla."},"clickEvent":{"action":"run_command","value":"/trigger config set 33011"}},{"text":"activés","color":"green"},{"text":". "},{"text":"(","color":"gray","hoverEvent":{"action":"show_text","contents":"Accéder aux paramètres avancés des éclairs supplémentaires."},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/eclairs"},"extra":[{"text":"paramètres","color":"yellow","italic":"true"},{"text":")"}]}]

# Vent (34000)
# Activer/désactiver (34010/34011) + accès au sous-menu par une fonction
execute unless score #vent on matches 1 run tellraw @s [{"text":"Le vent ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour activer le vent."},"clickEvent":{"action":"run_command","value":"/trigger config set 34010"}},{"text":"est désactivé","color":"red"},{"text":"."}]
execute if score #vent on matches 1 run tellraw @s [{"text":"Le vent ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour désactiver le vent."},"clickEvent":{"action":"run_command","value":"/trigger config set 34011"}},{"text":"est activé","color":"green"},{"text":". "},{"text":"(","color":"gray","hoverEvent":{"action":"show_text","contents":"Accéder aux paramètres avancés du vent."},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/direction_vent"},"extra":[{"text":"paramètres","color":"yellow","italic":"true"},{"text":")"}]}]

# -- Options de la végétation --
# Propagation des arbres (40000)
# Activer/désactiver (40010/40011) + accès au sous-menu par une fonction
execute unless score #pousseForets on matches 1 run tellraw @s [{"text":"La propagation des forêts est ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour activer la propagation des forêts."},"clickEvent":{"action":"run_command","value":"/trigger config set 40010"}},{"text":"désactivée","color":"red"},{"text":"."}]
execute if score #pousseForets on matches 1 run tellraw @s [{"text":"La propagation des forêts est ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour désactiver la propagation des forêts."},"clickEvent":{"action":"run_command","value":"/trigger config set 40011"}},{"text":"activée","color":"green"},{"text":". "},{"text":"(","color":"gray","hoverEvent":{"action":"show_text","contents":"Accéder aux paramètres avancés de la propagation des forêts."},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/pousse_arbres"},"extra":[{"text":"paramètres","color":"yellow","italic":"true"},{"text":")"}]}]

# -- Options de l'actualisation automatique --
# Actualisation automatique (50000)
# Activer/désactiver (50010/50011) + accès au sous-menu par une fonction
execute unless score #meteoActuAuto on matches 1 run tellraw @s [{"text":"L'actualisation automatique des chunks déchargés est ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour activer l'actualisation automatique."},"clickEvent":{"action":"run_command","value":"/trigger config set 50010"}},{"text":"désactivée","color":"red"},{"text":"."}]
execute if score #meteoActuAuto on matches 1 run tellraw @s [{"text":"L'actualisation automatique des chunks déchargés est ","color":"gray","hoverEvent":{"action":"show_text","contents":"Cliquer pour désactiver l'actualisation automatique."},"clickEvent":{"action":"run_command","value":"/trigger config set 50011"}},{"text":"activée","color":"green"},{"text":". "},{"text":"(","color":"gray","hoverEvent":{"action":"show_text","contents":"Accéder aux paramètres avancés de l'actualisation automatique."},"clickEvent":{"action":"run_command","value":"/function tdh:meteo/config/actu_auto"},"extra":[{"text":"paramètres","color":"yellow","italic":"true"},{"text":")"}]}]


#tellraw @s [{"text":"Vérifier l'intégrité de la date","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Cliquer ici si la date affichée ci-dessus semble incorrecte.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:time/config/date_check"}},{"text":" • ","color":"gray"},{"text":"Avancer d'une journée","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"La date sera avancée d'une journée, et l'heure sera définie au lever du soleil.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:time/config/prochaine_journee"}}]
#tellraw @s [{"text":"Réinitialiser l'heure","color":"gold","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"L'heure sera définie au début de la journée (tick 0).","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:time/reset_heure"}},{"text":" • ","color":"gray"},{"text":"Actualiser l'affichage","color":"yellow","italic":"true","hoverEvent":{"action":"show_text","contents":[{"text":"Affiche à nouveau ce menu.","color":"white"}]},"clickEvent":{"action":"run_command","value":"/function tdh:time/config"}}]


tellraw @s [{"text":"——————————————————————————","color":"gray"}]
tellraw @s [{"text":"- Quitter le configurateur","color":"gray","clickEvent":{"action":"run_command","value":"/trigger config set -1"}}]
scoreboard players operation @s temp = #temps currentTick
scoreboard players operation @s temp %= fakePlayer number100

execute if score @s temp matches 0..5 run function tdh:give/deco/dossier/0
execute if score @s temp matches 6..11 run function tdh:give/deco/dossier/blanc
execute if score @s temp matches 12..17 run function tdh:give/deco/dossier/bleu
execute if score @s temp matches 18..23 run function tdh:give/deco/dossier/bleuc
execute if score @s temp matches 24..29 run function tdh:give/deco/dossier/brun
execute if score @s temp matches 30..35 run function tdh:give/deco/dossier/cyan
execute if score @s temp matches 36..41 run function tdh:give/deco/dossier/gris
execute if score @s temp matches 42..47 run function tdh:give/deco/dossier/grisc
execute if score @s temp matches 48..52 run function tdh:give/deco/dossier/jaune
execute if score @s temp matches 53..58 run function tdh:give/deco/dossier/noir
execute if score @s temp matches 59..64 run function tdh:give/deco/dossier/orange
execute if score @s temp matches 65..70 run function tdh:give/deco/dossier/rose
execute if score @s temp matches 71..76 run function tdh:give/deco/dossier/rouge
execute if score @s temp matches 77..82 run function tdh:give/deco/dossier/vert
execute if score @s temp matches 83..88 run function tdh:give/deco/dossier/vertc
execute if score @s temp matches 89..94 run function tdh:give/deco/dossier/violet
execute if score @s temp matches 95..99 run function tdh:give/deco/dossier/violetc
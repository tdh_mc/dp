scoreboard players operation @s temp = #temps currentTick
scoreboard players operation @s temp %= fakePlayer number100

execute if score @s temp matches 0..13 run function tdh:give/deco/alchimie/goldencarrot
execute if score @s temp matches 14..28 run function tdh:give/deco/alchimie/magmacream
execute if score @s temp matches 29..42 run function tdh:give/deco/alchimie/netherwart
execute if score @s temp matches 43..57 run function tdh:give/deco/alchimie/phantom
execute if score @s temp matches 58..70 run function tdh:give/deco/alchimie/pufferfish
execute if score @s temp matches 71..84 run function tdh:give/deco/alchimie/spidereye
execute if score @s temp matches 85..99 run function tdh:give/deco/alchimie/sucre
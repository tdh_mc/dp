scoreboard players operation @s temp = #temps currentTick
scoreboard players operation @s temp %= fakePlayer number100

execute if score @s temp matches 0..14 run function tdh:give/deco/minerai/charbon
execute if score @s temp matches 15..29 run function tdh:give/deco/minerai/diamant
execute if score @s temp matches 30..39 run function tdh:give/deco/minerai/emeraude
execute if score @s temp matches 40..54 run function tdh:give/deco/minerai/fer
execute if score @s temp matches 55..64 run function tdh:give/deco/minerai/lapis
execute if score @s temp matches 65..79 run function tdh:give/deco/minerai/or
execute if score @s temp matches 80..84 run function tdh:give/deco/minerai/random
execute if score @s temp matches 85..99 run function tdh:give/deco/minerai/redstone
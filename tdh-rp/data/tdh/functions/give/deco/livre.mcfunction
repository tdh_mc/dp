scoreboard players operation @s temp = #temps currentTick
scoreboard players operation @s temp %= fakePlayer number100

execute if score @s temp matches 0..25 run function tdh:give/deco/livre1h
execute if score @s temp matches 26..28 run function tdh:give/deco/livre1v

execute if score @s temp matches 29..53 run function tdh:give/deco/livre2h

execute if score @s temp matches 54..74 run function tdh:give/deco/livre3h
execute if score @s temp matches 75..78 run function tdh:give/deco/livre3v

execute if score @s temp matches 79..91 run function tdh:give/deco/livre4h
execute if score @s temp matches 92..99 run function tdh:give/deco/livre4v
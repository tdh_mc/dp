tellraw @s {"text":" --------------------","color":"gold"}
tellraw @s {"text":"Commandes disponibles dans /tgived2 minerai :","color":"gold"}
tellraw @s [{"text":"- /tgived2 minerai ","color":"gold"},{"text":"charbon","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/minerai/charbon"}}]
tellraw @s [{"text":"- /tgived2 minerai ","color":"gold"},{"text":"diamant","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/minerai/diamant"}}]
tellraw @s [{"text":"- /tgived2 minerai ","color":"gold"},{"text":"emeraude","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/minerai/emeraude"}}]
tellraw @s [{"text":"- /tgived2 minerai ","color":"gold"},{"text":"fer","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/minerai/fer"}}]
tellraw @s [{"text":"- /tgived2 minerai ","color":"gold"},{"text":"lapis","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/minerai/lapis"}}]
tellraw @s [{"text":"- /tgived2 minerai ","color":"gold"},{"text":"or","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/minerai/or"}}]
tellraw @s [{"text":"- /tgived2 minerai ","color":"gold"},{"text":"redstone","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/minerai/redstone"}}]
tellraw @s [{"text":"- /tgived2 minerai ","color":"gold"},{"text":"random","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/minerai/random"}},{"text":" (minerais non présents dans le jeu de base)","color":"gold"}]
tellraw @s {"text":" --------------------","color":"gold"}
#tellraw @s [{"text":"Pour afficher les détails de chacun des sous-dossiers, taper ","color":"gold"},{"text":"/tgived2 ","color":"blue"},{"text":"<dossier>","color":"aqua"},{"text":" help","color":"blue"}]
#tellraw @s {"text":" --------------------","color":"gold"}
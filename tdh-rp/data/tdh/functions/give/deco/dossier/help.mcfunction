tellraw @s {"text":" --------------------","color":"gold"}
tellraw @s {"text":"Commandes disponibles dans /tgived2 dossier :","color":"gold"}
tellraw @s [{"text":"- Couleurs par défaut","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/0"}}]
tellraw @s [{"text":"- Blanc","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/blanc"}}]
tellraw @s [{"text":"- Bleu","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/bleu"}}]
tellraw @s [{"text":"- Bleu clair","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/bleuc"}}]
tellraw @s [{"text":"- Brun","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/brun"}}]
tellraw @s [{"text":"- Cyan","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/cyan"}}]
tellraw @s [{"text":"- Gris","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/gris"}}]
tellraw @s [{"text":"- Gris clair","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/grisc"}}]
tellraw @s [{"text":"- Jaune","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/jaune"}}]
tellraw @s [{"text":"- Noir","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/noir"}}]
tellraw @s [{"text":"- Orange","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/orange"}}]
tellraw @s [{"text":"- Rose","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/rose"}}]
tellraw @s [{"text":"- Rouge","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/rouge"}}]
tellraw @s [{"text":"- Vert","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/vert"}}]
tellraw @s [{"text":"- Vert clair","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/vertc"}}]
tellraw @s [{"text":"- Violet","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/violet"}}]
tellraw @s [{"text":"- Violet clair","color":"gold","clickEvent":{"action":"run_command","value":"/function tdh:give/deco/dossier/violetc"}}]
tellraw @s {"text":" --------------------","color":"gold"}
#tellraw @s [{"text":"Pour afficher les détails de chacun des sous-dossiers, taper ","color":"gold"},{"text":"/tgived2 ","color":"blue"},{"text":"<dossier>","color":"aqua"},{"text":" help","color":"blue"}]
#tellraw @s {"text":" --------------------","color":"gold"}
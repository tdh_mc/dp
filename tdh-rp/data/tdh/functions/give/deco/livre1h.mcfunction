scoreboard players operation @s temp = #temps currentTick
scoreboard players operation @s temp %= fakePlayer number100

execute if score @s temp matches 0..9 run function tdh:give/deco/livre/1h1
execute if score @s temp matches 10..19 run function tdh:give/deco/livre/1h2
execute if score @s temp matches 20..29 run function tdh:give/deco/livre/1h3
execute if score @s temp matches 30..39 run function tdh:give/deco/livre/1h4
execute if score @s temp matches 40..43 run function tdh:give/deco/livre/1h5_blue
execute if score @s temp matches 44..47 run function tdh:give/deco/livre/1h5_gold
execute if score @s temp matches 48..51 run function tdh:give/deco/livre/1h5_grn
execute if score @s temp matches 52..55 run function tdh:give/deco/livre/1h5_mag
execute if score @s temp matches 56..60 run function tdh:give/deco/livre/1h5_purp
execute if score @s temp matches 61..64 run function tdh:give/deco/livre/1h5_red
execute if score @s temp matches 65..69 run function tdh:give/deco/livre/1h5_yel
execute if score @s temp matches 70..79 run function tdh:give/deco/livre/1h6
execute if score @s temp matches 80..89 run function tdh:give/deco/livre/1h7
execute if score @s temp matches 90..99 run function tdh:give/deco/livre/1h8
tellraw @s {"text":" --------------------","color":"gold"}
tellraw @s {"text":"Commandes disponibles dans /tgivei (cliquables) :","color":"gold"}
tellraw @s [{"text":"- /tgivei ","color":"gold"},{"text":"acces_interdit","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/info/acces_interdit"}}]
tellraw @s [{"text":"- /tgivei ","color":"gold"},{"text":"chenal_ferme","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/info/chenal_ferme"}}]
tellraw @s [{"text":"- /tgivei ","color":"gold"},{"text":"chenal_ouvert","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/info/chenal_ouvert"}}]
tellraw @s [{"text":"- /tgivei ","color":"gold"},{"text":"feu_orange","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/info/feu_orange"}}]
tellraw @s [{"text":"- /tgivei ","color":"gold"},{"text":"feu_rouge","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/info/feu_rouge"}}]
tellraw @s [{"text":"- /tgivei ","color":"gold"},{"text":"feu_vert","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/info/feu_vert"}}]
tellraw @s [{"text":"- /tgivei ","color":"gold"},{"text":"haut_parleur","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/info/haut_parleur"}}]
tellraw @s [{"text":"- /tgivei ","color":"gold"},{"text":"sens_interdit","color":"red","clickEvent":{"action":"run_command","value":"/function tdh:give/info/sens_interdit"}}]
tellraw @s {"text":" --------------------","color":"gold"}
#tellraw @s [{"text":"Pour afficher les détails de chacun des sous-dossiers, taper ","color":"gold"},{"text":"/tgivei2 ","color":"blue"},{"text":"<dossier>","color":"aqua"},{"text":" help","color":"blue"}]
#tellraw @s {"text":" --------------------","color":"gold"}
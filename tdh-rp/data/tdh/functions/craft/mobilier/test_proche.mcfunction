# On exécute cette fonction à la position d'un joueur qui a récemment posé une item frame "MobilierTDH",
# pour traiter ladite entité (invoquer un marqueur à sa position, éventuellement ajouter une barrier si c'est une table ou assimilé...)

# On exécute une sous-fonction pour chaque item frame "MobilierTDH" non traitée précédemment par cette fonction
execute as @e[type=item_frame,tag=MobilierTDH,tag=!Traite,distance=..15] at @s run function tdh:craft/mobilier/test_item_frame
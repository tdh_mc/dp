# On exécute cette fonction en tant qu'un marqueur de mobilier TDH à sa position,
# lorsque l'item frame a disparu/été retirée, pour invoquer son objet

# On enregistre le fait qu'on a pris en compte la disparition de notre item frames
scoreboard players set @s temp 2

# Selon le cas, on va avoir une action différente :
# - Si un item existe, on modifie ses données
data modify entity @e[type=item,distance=..2,nbt={Item:{id:"minecraft:item_frame"}},limit=1] Item set from entity @s data.item
# - Si l'item n'existe pas/plus, on ne fait rien
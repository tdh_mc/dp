# On exécute cette fonction en tant qu'un marqueur de mobilier TDH à sa position,
# pour détruire son item frame et invoquer son objet

# On déclare le fait qu'on a détruit l'item frame
scoreboard players set @s temp 1

# On tue l'item frame
kill @e[type=item_frame,tag=MobilierTDH,distance=0]

# On invoque l'item correspondant
summon item ~ ~ ~ {Tags:["TempItemMobilierTDH"],Item:{id:"minecraft:item_frame",Count:1b},PickupDelay:10s}
data modify entity @e[type=item,tag=TempItemMobilierTDH,limit=1] Item set from entity @s data.item
tag @e[type=item,tag=TempItemMobilierTDH] remove TempItemMobilierTDH
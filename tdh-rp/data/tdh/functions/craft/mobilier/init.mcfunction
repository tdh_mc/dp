# Recettes custom de l'atelier amélioré
# Chaise (osier) =
# STICK| ---- | ----
# STICK| SLAB | SLAB
# STICK| ---- | STICK
# Spruce
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Chaise (osier)",output:{id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Chaise en osier"'},CustomModelData:100101,EntityTag:{Tags:["MobilierTDH","Chaise","Spruce","Osier"],Item:{id:"minecraft:spruce_stairs",Count:1b,tag:{CustomModelData:101}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}},recipe:{slots:[{id:"minecraft:stick"},{id:"minecraft:air"},{id:"minecraft:air"},{id:"minecraft:stick"},{id:"minecraft:spruce_slab"},{id:"minecraft:spruce_slab"},{id:"minecraft:stick"},{id:"minecraft:air"},{id:"minecraft:stick"}]}}

# Table (1x1) =
# SLAB | SLAB | SLAB
# STICK| ---- | STICK
# STICK| ---- | STICK
# Spruce
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Table en sapin (1x1)",output:{id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Table en sapin (1x1)"'},CustomModelData:201100,EntityTag:{Tags:["MobilierTDH","Table","Spruce","1x1"],Item:{id:"minecraft:scaffolding",Count:1b,tag:{CustomModelData:1100}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}},recipe:{slots:[{id:"minecraft:spruce_slab"},{id:"minecraft:spruce_slab"},{id:"minecraft:spruce_slab"},{id:"minecraft:stick"},{id:"minecraft:air"},{id:"minecraft:stick"},{id:"minecraft:stick"},{id:"minecraft:air"},{id:"minecraft:stick"}]}}

# Table (2x1) =
# TABLE| TABLE| ----
# ---- | ---- | ----
# ---- | ---- | ----
# Spruce
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Table en sapin (2x1)",output:{id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Table en sapin (2x1)"'},CustomModelData:202100,EntityTag:{Tags:["MobilierTDH","Table","Spruce","2x1"],Item:{id:"minecraft:scaffolding",Count:1b,tag:{CustomModelData:2100}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}},recipe:{slots:[{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:air"},{id:"minecraft:air"},{id:"minecraft:air"},{id:"minecraft:air"},{id:"minecraft:air"},{id:"minecraft:air"},{id:"minecraft:air"}]}}

# Table (2x2) =
# TABLE| TABLE| ----
# TABLE| TABLE| ----
# ---- | ---- | ----
# Spruce
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Table en sapin (2x2)",output:{id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Table en sapin (2x2)"'},CustomModelData:202200,EntityTag:{Tags:["MobilierTDH","Table","Spruce","2x2"],Item:{id:"minecraft:scaffolding",Count:1b,tag:{CustomModelData:2200}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}},recipe:{slots:[{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:air"},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:air"},{id:"minecraft:air"},{id:"minecraft:air"},{id:"minecraft:air"}]}}

# Table (3x2) =
# TABLE| TABLE| TABLE
# TABLE| TABLE| TABLE
# ---- | ---- | ----
# Spruce
data modify storage tdh:craft recettes.atelier_ameliore append value {name:"Table en sapin (3x2)",output:{id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Table en sapin (3x2)"'},CustomModelData:203200,EntityTag:{Tags:["MobilierTDH","Table","Spruce","3x2"],Item:{id:"minecraft:scaffolding",Count:1b,tag:{CustomModelData:3200}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}},recipe:{slots:[{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:air"},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:air"},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:scaffolding",tag:{CustomModelData:201100}},{id:"minecraft:air"}]}}


# Données supplémentaires pour les item frames
# Mobilier
# Chaises
data modify storage tdh:craft objets.mobilier.chaise.spruce.osier set value {id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Chaise en osier"'},CustomModelData:100101,EntityTag:{Tags:["MobilierTDH","Chaise","Spruce","Osier"],Item:{id:"minecraft:spruce_stairs",Count:1b,tag:{CustomModelData:101}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}}
data modify storage tdh:craft objets.mobilier.chaise.spruce.osier_dossier set value {id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Chaise en osier avec dossier"'},CustomModelData:100201,EntityTag:{Tags:["MobilierTDH","Chaise","Spruce","Osier","Dossier"],Item:{id:"minecraft:spruce_stairs",Count:1b,tag:{CustomModelData:201}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}}
data modify storage tdh:craft objets.mobilier.chaise.spruce.jaune set value {id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Chaise en sapin"'},CustomModelData:100100,EntityTag:{Tags:["MobilierTDH","Chaise","Spruce","Jaune"],Item:{id:"minecraft:spruce_stairs",Count:1b,tag:{CustomModelData:100}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}}
data modify storage tdh:craft objets.mobilier.chaise.spruce.jaune_dossier set value {id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Chaise en sapin avec dossier"'},CustomModelData:100200,EntityTag:{Tags:["MobilierTDH","Chaise","Spruce","Jaune","Dossier"],Item:{id:"minecraft:spruce_stairs",Count:1b,tag:{CustomModelData:200}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}}

# Tables
data modify storage tdh:craft objets.mobilier.table.spruce.1x1 set value {id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Table en sapin (1x1)"'},CustomModelData:201100,EntityTag:{Tags:["MobilierTDH","Table","Spruce","1x1"],Item:{id:"minecraft:scaffolding",Count:1b,tag:{CustomModelData:1100}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}}
data modify storage tdh:craft objets.mobilier.table.spruce.2x1 set value {id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Table en sapin (2x1)"'},CustomModelData:202100,EntityTag:{Tags:["MobilierTDH","Table","Spruce","2x1"],Item:{id:"minecraft:scaffolding",Count:1b,tag:{CustomModelData:2100}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}}
data modify storage tdh:craft objets.mobilier.table.spruce.2x2 set value {id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Table en sapin (2x2)"'},CustomModelData:202200,EntityTag:{Tags:["MobilierTDH","Table","Spruce","2x2"],Item:{id:"minecraft:scaffolding",Count:1b,tag:{CustomModelData:2200}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}}
data modify storage tdh:craft objets.mobilier.table.spruce.3x2 set value {id:"minecraft:item_frame",Count:1b,tag:{display:{Name:'"Table en sapin (3x2)"'},CustomModelData:203200,EntityTag:{Tags:["MobilierTDH","Table","Spruce","3x2"],Item:{id:"minecraft:scaffolding",Count:1b,tag:{CustomModelData:3200}},ItemDropChance:0.0f,Invisible:1b,Facing:1b}}}
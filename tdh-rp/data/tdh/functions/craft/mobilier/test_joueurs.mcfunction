# On exécute cette fonction régulièrement pour vérifier quels joueurs ont posé des item frames,
# et configurer correctement les meubles placés par lesdites item frames

# L'advancement tdh:craft/mobilier/pose ne se trigger que lorsqu'on pose une item frame avec le tag "MobilierTDH"
execute at @a[advancements={tdh:craft/mobilier/pose=true}] run function tdh:craft/mobilier/test_proche

# On reset l'advancement de tous les joueurs
advancement revoke @a only tdh:craft/mobilier/pose
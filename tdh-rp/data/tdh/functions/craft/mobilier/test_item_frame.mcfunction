# On exécute cette fonction en tant qu'une item frame "MobilierTDH" à sa propre position,
# lorsqu'elle vient d'être posée par un joueur (=pas de tag "Traite")

# Selon notre type de mobilier, on exécute une sous-fonction différente
execute as @s[tag=Table] run function tdh:craft/mobilier/table/creer
execute as @s[tag=Chaise] run function tdh:craft/mobilier/chaise/creer
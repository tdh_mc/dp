# On exécute cette fonction régulièrement pour vérifier si du mobilier TDH a été retiré,
# afin de détruire les marqueurs correspondant à ce mobilier

execute as @e[type=marker,tag=MarqueurMobilierTDH] at @s run function tdh:craft/mobilier/test_destruction
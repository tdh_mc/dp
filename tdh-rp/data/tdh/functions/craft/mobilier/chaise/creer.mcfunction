# On est une item frame "MobilierTDH" de type "Chaise" à sa propre position

# On crée un marqueur à la position de l'item frame
summon marker ~ ~ ~ {Tags:["MarqueurMobilierTDH","Chaise","MarqueurTemp"]}

# On cherche à s'initialiser, mais on a une fonction différente selon nos tags
execute as @s[tag=Spruce] run function tdh:craft/mobilier/chaise/creer/spruce

# On supprime le tag temporaire du marqueur
tag @e[type=marker,tag=MarqueurTemp] remove MarqueurTemp

# On enregistre le fait qu'on a traité ce mobilier
tag @s add Traite
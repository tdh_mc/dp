# On a une fonction différente selon le type de chaise
execute as @s[tag=Osier,tag=!Dossier] run function tdh:craft/mobilier/chaise/creer/spruce/osier
execute as @s[tag=Osier,tag=Dossier] run function tdh:craft/mobilier/chaise/creer/spruce/osier_dossier
execute as @s[tag=Jaune,tag=!Dossier] run function tdh:craft/mobilier/chaise/creer/spruce/jaune
execute as @s[tag=Jaune,tag=Dossier] run function tdh:craft/mobilier/chaise/creer/spruce/jaune_dossier
# On exécute cette fonction en tant qu'un marqueur de mobilier TDH à sa position, pour vérifier si l'item frame est toujours en place

# On déclare une variable de contrôle pour détecter lorsqu'une condition passe
scoreboard players set @s temp 2

# Si l'item frame existe toujours, on définit la variable de contrôle à 0
execute if entity @e[type=item_frame,tag=MobilierTDH,distance=0] run scoreboard players set @s temp 0
# Si l'item frame existe mais qu'elle ne contient pas d'item, on la détruit et on spawn l'item approprié
# (cette fonction définira aussi la variable de contrôle à 1)
execute if score @s temp matches 0 unless data entity @e[type=item_frame,tag=MobilierTDH,distance=0,limit=1] Item.id run function tdh:craft/mobilier/destruction/item_frame
# Si l'item frame n'existe plus, on va vérifier si un item droppé existe déjà pour remplacer ses données
execute if score @s temp matches 2 run function tdh:craft/mobilier/destruction/no_item_frame

# Si la variable de contrôle est supérieure à 0, on se détruit
execute if score @s temp matches 1.. run function tdh:craft/mobilier/destruction
# On a une fonction différente selon la taille de la table
execute as @s[tag=1x1] run function tdh:craft/mobilier/table/creer/spruce/1x1
execute as @s[tag=2x1] run function tdh:craft/mobilier/table/creer/spruce/2x1
execute as @s[tag=2x2] run function tdh:craft/mobilier/table/creer/spruce/2x2
execute as @s[tag=3x2] run function tdh:craft/mobilier/table/creer/spruce/3x2

# On donne dans tous les cas au marqueur le tag Spruce
tag @e[type=marker,tag=MarqueurTemp,limit=1] add Spruce
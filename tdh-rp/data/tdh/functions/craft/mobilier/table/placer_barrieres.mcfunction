# On exécute cette fonction en tant qu'un MarqueurMobilierTDH pour poser les barrières invisibles au bon endroit sous la table MobilierTDH située à la même position
execute as @s[tag=1x1] run function tdh:craft/mobilier/table/placer_barrieres/1x1
execute as @s[tag=2x1] run function tdh:craft/mobilier/table/placer_barrieres/2x1
execute as @s[tag=2x2] run function tdh:craft/mobilier/table/placer_barrieres/2x2
execute as @s[tag=3x2] run function tdh:craft/mobilier/table/placer_barrieres/3x2
# On commence par retirer les barrières (selon notre ancienne rotation)
# mais seulement si on possédait *déjà* une rotation
execute if score @s rotation matches 0..7 run function tdh:craft/mobilier/table/retirer_barrieres

# On oriente ensuite le marqueur selon notre nouvelle rotation
scoreboard players operation @s rotation = @s temp
execute if score @s rotation matches 0 run tp @s ~ ~ ~ -90 0
execute if score @s rotation matches 1 run tp @s ~ ~ ~ -45 0
execute if score @s rotation matches 2 run tp @s ~ ~ ~ 0 0
execute if score @s rotation matches 3 run tp @s ~ ~ ~ 45 0
execute if score @s rotation matches 4 run tp @s ~ ~ ~ 90 0
execute if score @s rotation matches 5 run tp @s ~ ~ ~ 135 0
execute if score @s rotation matches 6 run tp @s ~ ~ ~ 180 0
execute if score @s rotation matches 7 run tp @s ~ ~ ~ 225 0

# On replace les barrières selon notre nouvelle rotation
execute at @s run function tdh:craft/mobilier/table/placer_barrieres
# On exécute cette fonction régulièrement en tant qu'un MarqueurMobilierTDH de type Table,
# pour vérifier si la rotation de l'item frame correspond toujours à la nôtre

# On récupère la rotation actuelle de l'item frame
execute store result score @s temp run data get entity @e[type=item_frame,tag=MobilierTDH,distance=0,limit=1] ItemRotation
scoreboard players set @s rotationMax 8
scoreboard players operation @s temp %= @s rotationMax

# Si cette rotation n'est PAS identique à notre score déjà enregistré, on change les barrières de place
execute unless score @s temp = @s rotation run function tdh:craft/mobilier/table/verifier_barrieres/modifier_rotation
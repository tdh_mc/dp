# On est une item frame "MobilierTDH" de type "Table" à sa propre position

# On déplace l'item frame d'un bloc vers le haut
tp @s ~ ~1 ~

# On crée un marqueur à la position de l'item frame
execute at @s run summon marker ~ ~ ~ {Tags:["MarqueurMobilierTDH","Table","MarqueurTemp"]}

# On cherche à s'initialiser, mais on a une fonction différente selon nos tags
execute at @s[tag=Spruce] run function tdh:craft/mobilier/table/creer/spruce

# On initialise les barrières sous notre table
execute as @e[type=marker,tag=MarqueurTemp,limit=1] at @s run function tdh:craft/mobilier/table/verifier_barrieres

# On supprime le tag temporaire du marqueur
tag @e[type=marker,tag=MarqueurTemp] remove MarqueurTemp

# On enregistre le fait qu'on a traité ce mobilier
tag @s add Traite
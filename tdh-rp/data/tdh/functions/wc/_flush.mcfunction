# Cette fonction est appelée par un command block à sa position,
# pour tirer la chasse des toilettes les plus proches

# On tag les toilettes les plus proches
tag @e[type=item_frame,tag=WC,distance=..12,sort=nearest,limit=1] add OurWC

# Si les toilettes sont déjà fermées, on leur donne un tag pour s'en souvenir un peu plus bas
tag @e[type=item_frame,tag=OurWC,tag=Ferme,limit=1] add DejaFerme

# Si les toilettes les plus proches sont ouvertes, on tire la chasse et on schedule l'ouverture
execute as @e[type=item_frame,tag=OurWC,tag=!Ferme,limit=1] at @s run function tdh:wc/flush

# Si les toilettes les plus proches étaient déjà fermées, on les rouvre
execute as @e[type=item_frame,tag=OurWC,tag=DejaFerme,limit=1] at @s run function tdh:wc/ouvrir

# On supprime le tag temporaire des toilettes les plus proches
tag @e[type=item_frame,tag=WC,tag=OurWC] remove OurWC
tag @e[type=item_frame,tag=WC,tag=DejaFerme] remove DejaFerme
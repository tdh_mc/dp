# Cette fonction est appelée par une item frame "WC" à sa position,
# pour fermer et flush des toilettes ouvertes

# On calcule notre heure/date d'ouverture
scoreboard players operation @s idJour = #temps idJour
scoreboard players operation @s tickUpdate = #temps currentTdhTick
scoreboard players add @s tickUpdate 100
execute if score @s tickUpdate >= #temps fullDayTicks run scoreboard players add @s idJour 1
execute if score @s tickUpdate >= #temps fullDayTicks run scoreboard players operation @s tickUpdate -= #temps fullDayTicks

# On se donne le tag de fermeture
tag @s add Ferme

# On ferme les toilettes (si le modèle était bien ouvert)
execute store result score #rpWC temp run data get entity @s Item.tag.CustomModelData
execute if score #rpWC temp matches ..999 store result entity @s Item.tag.CustomModelData int 1 run scoreboard players add #rpWC temp 1000
scoreboard players reset #rpWC temp

# On joue un bruitage (TODO)
playsound tdh:br.wc.flush block @a[distance=..15] ~ ~ ~ 1 1
playsound tdh:br.wc.close block @a[distance=..15] ~ ~ ~ 1 1
# Ce tick tourne uniquement lorsqu'il y a des toilettes à rouvrir prochainement
# Dans le cas contraire, il se désactivera à la fin de l'itération actuelle

# Il n'est appelé par personne ni à aucune position précise

# On rouvre tous les WC qui ont dépassé leur heure de réouverture
execute as @e[type=item_frame,tag=WC,tag=Ferme] if score @s idJour < #temps idJour at @s run function tdh:wc/ouvrir
execute as @e[type=item_frame,tag=WC,tag=Ferme] if score @s idJour = #temps idJour if score @s tickUpdate <= #temps currentTdhTick at @s run function tdh:wc/ouvrir
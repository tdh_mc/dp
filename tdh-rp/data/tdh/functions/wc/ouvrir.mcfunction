# Cette fonction est appelée par une item frame "WC" à sa position,
# pour rouvrir des toilettes qui étaient fermées

# On supprime notre heure/date d'ouverture
scoreboard players reset @s idJour
scoreboard players reset @s tickUpdate

# On supprime notre tag de fermeture
tag @s remove Ferme

# On ouvre les toilettes (si le modèle était bien fermé)
execute store result score #rpWC temp run data get entity @s Item.tag.CustomModelData
execute if score #rpWC temp matches 1000.. store result entity @s Item.tag.CustomModelData int 1 run scoreboard players remove #rpWC temp 1000
scoreboard players reset #rpWC temp

# On joue un bruitage (TODO)
playsound tdh:br.wc.open block @a[distance=..15] ~ ~ ~ 1 1
# On copie le bon objet dans le storage tdh:joueur objet_main_gauche
data modify storage tdh:joueur objet_main_gauche set from entity @p[distance=0,gamemode=!spectator] Inventory[-1]

# On fait ensuite les calculs sur l'objet
# Tous les objets dynamiques seront composés de la manière suivante :
# Item: {
#	- id:"<l'id de l'objet>",
#	- Count:"<le nombre d'objets>",
#	- tag:{
#		- dynamique:<identifiant d'objet dynamique> (0 ou absent si l'objet n'est pas dynamique)
#	  }
# }

# On a donc le tag "dynamique" qui peut être testé, et ne renverra un nombre différent/supérieur à 0 QUE si on tient un objet dynamique : un seul test NBT a donc besoin d'être réalisé à chaque itération pour vérifier si le joueur tient un objet
execute store result score #joueurObjet temp run data get storage tdh:joueur objet_main_gauche.tag.dynamique

# Si on a un type d'objet dynamique différent de 0, on vérifie de quel objet il s'agit
execute if score #joueurObjet temp matches 1.. run function tdh:player/objets_main_gauche/objet

# On réinitialise la variable temporaire et le storage
scoreboard players reset #joueurObjet temp
data remove storage tdh:joueur objet_main_gauche
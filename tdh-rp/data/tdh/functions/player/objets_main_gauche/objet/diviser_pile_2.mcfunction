data modify entity @s Item set from storage tdh:joueur objet_main_gauche
data modify entity @s Owner set from entity @p[gamemode=!spectator,distance=0] UUID
# On décrémente le nombre d'objets de 1, car on en gardera 1 dans la main
execute store result entity @s Item.Count byte 1 run scoreboard players remove #joueurObjet temp6 1

# On supprime le tag temporaire de l'objet
tag @s remove ObjetTemporaire
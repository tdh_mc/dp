# On enregistre le nombre d'objets qu'on tient actuellement en main
execute store result score #joueurObjet temp6 run data get storage tdh:joueur objet_main_gauche.Count

# S'il y en a plus de 1, on divise le stack
execute if score #joueurObjet temp6 matches 2.. run function tdh:player/objets_main_gauche/objet/diviser_pile
scoreboard players reset #joueurObjet temp6
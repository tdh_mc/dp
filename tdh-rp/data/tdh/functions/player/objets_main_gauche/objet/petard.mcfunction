# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient un juin en main (#joueurObjet temp = 250..299 du moins au plus combustionné) DE LA MAIN GAUCHE

# Si on est au maximum, on supprime le juin car il est terminé
execute if score #joueurObjet temp matches 299 run function tdh:player/objets_main_gauche/objet/petard/combustion_terminee

# Sinon, on est en cours de combustion, et on consume le juin normalement
execute if score #joueurObjet temp matches 251..298 run function tdh:player/objets_main_gauche/objet/petard/combustion_avancee

# Si l'ID est exactement de 200, on tient un juin éteint, et on va donc vérifier si on a un briquet dans notre autre main
execute if score #joueurObjet temp matches 250 run function tdh:player/objets_main_gauche/objet/petard/test_allumage
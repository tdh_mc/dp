# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient un juin en fin de combustion en main (#joueurObjet temp = 299),
# et on va supprimer le juin

# On enregistre le fait qu'on ne tient plus d'objet dynamique
scoreboard players set #joueurObjet temp 0

# On supprime l'objet du joueur
item replace entity @p[distance=0,gamemode=!spectator] weapon.offhand with air

# On jette un mégot au sol, qui ne peut être ramassé par personne et met longtemps à dépop
summon item ^0.35 ^0.85 ^0.65 {Tags:["Megot"],Age:-30000s,Invulnerable:1b,PickupDelay:32767s,Item:{id:"minecraft:stick",Count:1b,tag:{CustomModelData:1299,display:{Name:'"Mégot illicite"'}}}}
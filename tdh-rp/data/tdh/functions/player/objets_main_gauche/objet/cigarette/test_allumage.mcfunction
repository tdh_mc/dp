# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une cigarette non allumée en main (#joueurObjet temp = 200),
# et on va tester si on a un briquet dans l'autre main

# Si la condition passe, on allume la cigarette
execute if entity @p[distance=0,gamemode=!spectator,nbt={SelectedItem:{id:"minecraft:flint_and_steel"}}] run function tdh:player/objets_main_gauche/objet/cigarette/allumage
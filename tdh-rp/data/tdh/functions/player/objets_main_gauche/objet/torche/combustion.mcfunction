# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une torche avec temps de combustion en main (#joueurObjet temp = 100..199 du moins au plus combustionné),
# et on va ajouter un niveau de combustion à la torche

# On vérifie s'il faut diviser le stack (= si on tient 2 objets ou +)
#function tdh:player/objets_main_gauche/objet/test_diviser_pile

# Si on a atteint 199, on supprime la torche et on en place une éteinte à la place
execute if score #joueurObjet temp matches 199 run function tdh:player/objets_main_gauche/objet/torche/combustion_terminee
# On définira dans cette fonction le score #joueurObjet temp à 0 pour ne pas activer la condition précédente

# Si on n'a pas passé la condition précédente, on diminue le niveau de combustion de la torche de 1
execute if score #joueurObjet temp matches 100..199 run function tdh:player/objets_main_gauche/objet/torche/combustion_avancee
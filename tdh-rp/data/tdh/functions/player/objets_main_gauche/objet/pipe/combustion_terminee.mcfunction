# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient un juin en fin de combustion en main (#joueurObjet temp = 299),
# et on va supprimer le juin

# On enregistre le fait qu'on ne tient plus d'objet dynamique
scoreboard players set #joueurObjet temp 0

# On donne une pipe vide au joueur
item replace entity @p[distance=0,gamemode=!spectator] weapon.offhand with stick{CustomModelData:1300,dynamique:300,display:{Name:'"Pipe"'}}
# On vérifie s'il faut diviser le stack (= si on tient 2 objets ou +)
function tdh:player/objets_main_gauche/objet/test_diviser_pile

# On remplace l'item tenu en main
item replace entity @p[distance=0,gamemode=!spectator] weapon.offhand with redstone_torch{CustomModelData:352,dynamique:352,display:{Name:'"Pipe allumée"'}}

# On joue un effet sonore
function tdh:sound/objet/briquet
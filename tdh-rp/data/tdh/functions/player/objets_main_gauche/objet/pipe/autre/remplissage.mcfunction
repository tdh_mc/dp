# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une pipe vide en main (#joueurObjet temp = 300),
# et qu'on a dans l'autre main du juin (nether_wart avec tag ingredient=250)

# On vérifie s'il faut diviser le stack (= si on tient 2 objets ou +)
function tdh:player/objets_main_gauche/objet/test_diviser_pile

# On supprime une feuille de juin
execute store result score #joueurObjet temp6 run clear @p[distance=0,gamemode=!spectator] nether_wart{ingredient:250} 1

# Si on a réussi à le faire, on se donne une pipe remplie
execute if score #joueurObjet temp6 matches 1.. run item replace entity @p[distance=0,gamemode=!spectator] weapon.offhand with stick{CustomModelData:1351,dynamique:351,display:{Name:'"Pipe remplie"'}}

# On réinitialise la variable temporaire
scoreboard players reset #joueurObjet temp6
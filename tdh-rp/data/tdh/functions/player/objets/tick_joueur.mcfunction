## TICK DES OBJETS DYNAMIQUES TENUS PAR LE JOUEUR
# Exécuté 1 fois par seconde en principe, à la position de chaque joueur pas en spectateur

# Tous les objets dynamiques seront composés de la manière suivante :
# Item: {
#	- id:"<l'id de l'objet>",
#	- Count:"<le nombre d'objets>",
#	- tag:{
#		- dynamique:<identifiant d'objet dynamique> (0 ou absent si l'objet n'est pas dynamique)
#	  }
# }

# On a donc le tag "dynamique" qui peut être testé, et ne renverra un nombre différent/supérieur à 0 QUE si on tient un objet dynamique : un seul test NBT a donc besoin d'être réalisé à chaque itération pour vérifier si le joueur tient un objet
execute store result score #joueurObjet temp run data get entity @p[distance=0,gamemode=!spectator] SelectedItem.tag.dynamique

# Si on a un type d'objet dynamique différent de 0, on vérifie de quel objet il s'agit
execute if score #joueurObjet temp matches 1.. run function tdh:player/objets/objet

# On réinitialise la variable temporaire
scoreboard players reset #joueurObjet temp
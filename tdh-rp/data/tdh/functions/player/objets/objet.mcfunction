# On a différents types d'objets dynamiques, l'ID étant stocké dans #joueurObjet temp
# et l'effet appliqué au joueur le plus proche, distance=0 n'étant pas en spectateur

# 100 à 199 = la torche qui se consume de manière plus ou moins régulière
# (avec une variable aléatoire pour savoir si elle se consume ou non à ce tick)
# À 100, elle est comme neuve ; elle se consumera jusqu'à atteindre 199, le dernier niveau de combustion (après quoi elle se transformera en torche éteinte, non dynamique)
execute if score #joueurObjet temp matches 100..199 run function tdh:player/objets/objet/torche

# 200 à 249 = la cigarette qui se consume de manière très régulière (une fois chaque tick, donc 50 ticks de 1 seconde en tout)
# À 200 elle est neuve ; elle se consumera jusqu'à atteindre 249, le dernier niveau de combustion (après quoi on la supprime)
execute if score #joueurObjet temp matches 200..249 run function tdh:player/objets/objet/cigarette

# 250 à 299 = le pétard qui se consume de manière assez régulière (1 chance sur 2 à chaque tick)
# À 250 il est neuf ; il se consumera jusqu'à atteindre 249, le dernier niveau de combustion (après quoi on le supprime)
execute if score #joueurObjet temp matches 250..299 run function tdh:player/objets/objet/petard

# 300 à 349 = la pipe se consume de manière assez régulière (1 chance sur 2 à chaque tick)
# À 300 elle est remplie ; elle se consume jusqu'à atteindre 349, le dernier niveau de combustion (après quoi on donne une pipe vide qui pourra être re-remplie)
execute if score #joueurObjet temp matches 300..399 run function tdh:player/objets/objet/pipe
# Variables relatives aux objets custom
scoreboard objectives add torchesCraftees minecraft.crafted:minecraft.torch "Torches fabriquées"
scoreboard objectives add torchesRamassees minecraft.picked_up:minecraft.torch "Torches ramassées"
scoreboard objectives add torchesDetruites minecraft.mined:minecraft.torch "Torches détruites"
scoreboard objectives add torchesDroppees minecraft.dropped:minecraft.torch "Torches droppées"

# Données associées aux objets custom
# Torches
data modify storage tdh:craft objets.outils.torche set value {id:"minecraft:torch",Count:1b,tag:{CustomModelData:100,dynamique:100,tdhTorche:1b,display:{Lore:["'Elle ne durera pas cent ans.'"]}}}
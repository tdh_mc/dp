# On vérifie s'il faut diviser le stack (= si on tient 2 objets ou +)
function tdh:player/objets/objet/test_diviser_pile

# On remplace l'item tenu en main
item replace entity @p[distance=0,gamemode=!spectator] weapon.mainhand with redstone_torch{CustomModelData:201,dynamique:201,display:{Name:'"Cigarette allumée"'}}

# On joue un effet sonore
function tdh:sound/objet/briquet
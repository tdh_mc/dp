data modify entity @s Item set from entity @p[gamemode=!spectator,distance=0] SelectedItem
data modify entity @s Owner set from entity @p[gamemode=!spectator,distance=0] UUID
# On décrémente le nombre d'objets de 1, car on en gardera 1 dans la main
execute store result entity @s Item.Count byte 1 run scoreboard players remove #joueurObjet temp6 1

tag @s remove ObjetTemporaire
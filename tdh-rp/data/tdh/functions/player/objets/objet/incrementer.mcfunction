# Fonction générique exécutée par un joueur tenant un objet dynamique
# pour passer au statut suivant (l'actuel est stocké dans #joueurObjet temp)
# et modifier en conséquence le CustomModelData et le statut de l'item tenu en main

# On incrémente le niveau de combustion de 1
# On stocke direct cette valeur dans le storage dont a besoin l'item modifier ci-après
execute store result storage tdh:joueur objet_dynamique.statut int 1 run scoreboard players add #joueurObjet temp 1

# On stocke le nouveau niveau de combustion dans l'objet dynamique en main droite
item modify entity @p[distance=0,gamemode=!spectator] weapon.mainhand tdh:objet_dynamique/copier
# On appelle cette fonction à la position du joueur à qui donner les torches,
# avec la variable #joueurObjet max définie au nombre de torches à donner

# On définit une variable temporaire à la valeur de max
scoreboard players operation #joueurObjet temp9 = #joueurObjet max
# Si la variable dépasse la taille max d'un stack, on la réduit
execute if score #joueurObjet temp9 matches 65.. run scoreboard players set #joueurObjet temp9 64

# On se donne des torches custom
summon item ~ ~ ~ {Tags:["TorcheTemporaire"],Item:{id:"torch",Count:1b,tag:{CustomModelData:100,dynamique:100,tdhTorche:1b,display:{Lore:['"Elle ne durera pas cent ans."']}}},PickupDelay:0s,Age:0s}
execute store result entity @e[type=item,tag=TorcheTemporaire,distance=0,limit=1] Item.Count byte 1 run scoreboard players get #joueurObjet temp9
data modify entity @e[type=item,tag=TorcheTemporaire,distance=0,limit=1] Owner set from entity @p[gamemode=!spectator,distance=0] UUID
tag @e[type=item,distance=0,tag=TorcheTemporaire,limit=1] remove TorcheTemporaire

# On vérifie si on a fini ou non
scoreboard players operation #joueurObjet max -= #joueurObjet temp9
execute if score #joueurObjet max matches 1.. run function tdh:player/objets/objet/torche/remplacer/donner

# On réinitialise les variables temporaires
scoreboard players reset #joueurObjet temp9
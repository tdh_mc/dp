# On enregistre dans une variable scoreboard le numéro du slot actuel
execute store result score #torche temp2 run data get storage tdh:joueur inventaire[0].Slot

# Selon le slot, on exécute la fonction correspondante
execute if score #torche temp2 matches 0..8 run function tdh:player/objets/objet/torche/remplacer/filtrer/0_8
execute if score #torche temp2 matches 9..17 run function tdh:player/objets/objet/torche/remplacer/filtrer/9_17
execute if score #torche temp2 matches 18..26 run function tdh:player/objets/objet/torche/remplacer/filtrer/18_26
execute if score #torche temp2 matches 27..35 run function tdh:player/objets/objet/torche/remplacer/filtrer/27_35
execute unless score #torche temp2 matches 0..35 run function tdh:player/objets/objet/torche/remplacer/filtrer/100_106
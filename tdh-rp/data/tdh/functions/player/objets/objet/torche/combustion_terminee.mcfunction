# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une torche en fin de combustion en main (#joueurObjet temp = 199),
# et on va définir la torche comme éteinte

# On stocke le nombre actuel de torches
execute store result score #joueurObjet nombre run data get entity @p[distance=0,gamemode=!spectator] SelectedItem.Count

# On enregistre le fait qu'on ne tient plus d'objet dynamique
scoreboard players set #joueurObjet temp 0

# On stocke le nouvel objet (torche éteinte) dans le joueur
# Il n'est plus dynamique, donc on ajoute simplement un CustomModelData
item replace entity @p[distance=0,gamemode=!spectator] weapon.mainhand with stick{CustomModelData:1100,display:{Name:'"Torche consumée"'}}
# On stocke le nombre de torches consumées
item modify entity @p[distance=0,gamemode=!spectator] weapon.mainhand tdh:objet_dynamique/copier_nombre
# On copie l'inventaire du joueur dans un storage
data modify storage tdh:joueur inventaire set from entity @p[gamemode=!spectator,distance=0] Inventory

# S'il y a au moins un élément dans l'inventaire, on exécute la récursion
execute if data storage tdh:joueur inventaire[0] run function tdh:player/objets/objet/torche/remplacer/filtrer/recursion
# On réinitialise la variable temporaire de récursion
scoreboard players reset #torche temp
scoreboard players reset #torche temp2
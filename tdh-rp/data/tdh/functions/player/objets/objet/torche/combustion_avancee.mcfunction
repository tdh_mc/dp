# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une torche avec temps de combustion en main (#joueurObjet temp = 100..199 du moins au plus combustionné),
# et on va ajouter un niveau de combustion à la torche

# On incrémente le niveau de combustion de 1
function tdh:player/objets/objet/incrementer
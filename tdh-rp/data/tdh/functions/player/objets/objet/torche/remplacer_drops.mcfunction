# On exécute cette fonction à la position d'un joueur pas en spectateur qui a récemment droppé ou cassé des torches,
# pour remplacer les items "torche" aux alentours par des items custom de TDH

execute as @e[type=item,distance=..20,nbt={Item:{id:"minecraft:torch"}}] unless data entity @s Item.tag.tdhTorche run data modify entity @s Item.tag merge from storage tdh:craft objets.outils.torche.tag
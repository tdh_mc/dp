# On supprime toutes les torches de notre inventaire
clear @p[gamemode=!spectator,distance=0] torch

# On se donne autant de torches custom qu'on avait de torches à la base
scoreboard players operation #joueurObjet max = #joueurObjet temp
function tdh:player/objets/objet/torche/remplacer/donner
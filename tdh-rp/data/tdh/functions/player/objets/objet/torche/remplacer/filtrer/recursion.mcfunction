# On a enregistré dans tdh:joueur inventaire l'inventaire du joueur actuellement traité
# On a vérifié qu'il y avait au minimum 1 élément dans l'inventaire

# On vérifie si le slot actuel contient une torche
# Si c'est le cas, cette opération ne changera rien, donc renverra 0
execute store success score #torche temp run data modify storage tdh:joueur inventaire[0].id set value "minecraft:torch"
# Cependant, s'il contient déjà une tdhTorche, on ne souhaite rien modifier
execute if score #torche temp matches 0 store result score #torche temp run data get storage tdh:joueur inventaire[0].tag.tdhTorche

# Si la variable vaut toujours 0, on a bien affaire à une torche vanilla,
# et on la remplace dans l'inventaire du joueur
execute if score #torche temp matches 0 run function tdh:player/objets/objet/torche/remplacer/filtrer/test_slot

# On supprime le slot actuel
data remove storage tdh:joueur inventaire[0]

# S'il reste des slots, on continue la récursion
execute if data storage tdh:joueur inventaire[0] run function tdh:player/objets/objet/torche/remplacer/filtrer/recursion
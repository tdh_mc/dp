# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient un juin en main (#joueurObjet temp = 201..248 du moins au plus combustionné),
# et on va ajouter un niveau de combustion au juin

# On vérifie s'il faut diviser le stack (= si on tient 2 objets ou +)
function tdh:player/objets/objet/test_diviser_pile

# On incrémente le niveau de combustion de 1
function tdh:player/objets/objet/incrementer

# On calcule le mod2 de la variable pour avoir une particule "aléatoire"
scoreboard players operation #joueurObjet temp4 = #joueurObjet temp
scoreboard players set #joueurObjet temp5 2
scoreboard players operation #joueurObjet temp4 %= #joueurObjet temp5

# On émet une particule de fumée alternativement à la bouche du joueur et sur le juin
execute if score #joueurObjet temp4 matches 0 run function tdh:player/objets/objet/petard/particule/fumee_main
execute if score #joueurObjet temp4 matches 1 run function tdh:player/objets/objet/petard/particule/fumee_bouche

# On émet des particules de cendre qui tombent
function tdh:player/objets/objet/petard/particule/cendre

# On réinitialise les variables temporaires
scoreboard players reset #joueurObjet temp4
scoreboard players reset #joueurObjet temp5
# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient un juin non allumé en main (#joueurObjet temp = 250),
# et on va tester si on a un briquet dans l'autre main

# Si la condition passe, on allume le pétard
execute if entity @p[distance=0,gamemode=!spectator,nbt={Inventory:[{Slot:-106b,id:"minecraft:flint_and_steel"}]}] run function tdh:player/objets/objet/petard/allumage
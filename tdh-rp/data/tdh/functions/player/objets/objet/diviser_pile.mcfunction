# On invoque les objets excédentaires au sol
summon item ~ ~ ~ {Tags:["ObjetTemporaire"],Item:{id:"dark_oak_button",Count:1b},Age:0s,PickupDelay:1s}
execute as @e[type=item,tag=ObjetTemporaire,sort=nearest,limit=1] run function tdh:player/objets/objet/diviser_pile_2

# On définit la taille de la pile actuelle à 1
item modify entity @p[gamemode=!spectator,distance=0] weapon.mainhand tdh:objet_dynamique/destack
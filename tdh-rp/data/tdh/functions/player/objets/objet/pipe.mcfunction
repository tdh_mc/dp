# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une pipe en main (#joueurObjet temp = 301..349 ou 351..399 du moins au plus combustionné, selon qu'elle est remplie de juin ou de tabac)

# Si on est au maximum, on remplace la pipe par une pipe vide
execute if score #joueurObjet temp matches 349 run function tdh:player/objets/objet/pipe/combustion_terminee
execute if score #joueurObjet temp matches 399 run function tdh:player/objets/objet/pipe/combustion_terminee

# Sinon, on est en cours de combustion, et on consume le juin normalement
execute if score #joueurObjet temp matches 302..348 run function tdh:player/objets/objet/pipe/tabac/combustion_avancee
execute if score #joueurObjet temp matches 352..398 run function tdh:player/objets/objet/pipe/autre/combustion_avancee

# Si l'ID est exactement de 301 ou 351, on tient une pipe pleine et éteinte, et on va donc vérifier si on a un briquet dans notre autre main
execute if score #joueurObjet temp matches 301 run function tdh:player/objets/objet/pipe/tabac/test_allumage
execute if score #joueurObjet temp matches 351 run function tdh:player/objets/objet/pipe/autre/test_allumage

# Si la pipe est vide, on vérifie si on a un truc à remplir correspondant dans notre autre main
execute if score #joueurObjet temp matches 300 run function tdh:player/objets/objet/pipe/test_remplissage
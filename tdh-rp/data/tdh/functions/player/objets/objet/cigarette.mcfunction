# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une cigarette en main (#joueurObjet temp = 200..249 du moins au plus combustionné)

# Si on est au maximum, on supprime la cigarette car elle est terminée
execute if score #joueurObjet temp matches 249 run function tdh:player/objets/objet/cigarette/combustion_terminee

# Sinon, on est en cours de combustion, et on consume la cigarette normalement
execute if score #joueurObjet temp matches 201..248 run function tdh:player/objets/objet/cigarette/combustion_avancee

# Si l'ID est exactement de 200, on tient une cigarette éteinte, et on va donc vérifier si on a un briquet dans notre autre main
execute if score #joueurObjet temp matches 200 run function tdh:player/objets/objet/cigarette/test_allumage
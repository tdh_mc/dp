# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une pipe en main (#joueurObjet temp = 302..348 du moins au plus combustionné),
# et on va ajouter un niveau de combustion à la pipe

# On vérifie s'il faut diviser le stack (= si on tient 2 objets ou +)
function tdh:player/objets/objet/test_diviser_pile

# On incrémente le niveau de combustion de 1
function tdh:player/objets/objet/incrementer

# On émet une particule de fumée
function tdh:player/objets/objet/pipe/tabac/particule/fumee
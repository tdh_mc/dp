# De la fumée devant le visage
execute rotated ~ 0 run particle poof ^ ^1.5 ^0.5 0.01 0.01 0.01 0.02 45
execute rotated ~ 0 run particle smoke ^ ^1.5 ^0.5 0.01 0.01 0.01 0.02 5

# De la fumée au dessus de la pipe
execute rotated ~ 0 run particle poof ^-0.3 ^1.0 ^0.7 0.03 0.05 0.03 0.02 30
execute rotated ~ 0 run particle campfire_signal_smoke ^-0.3 ^1.0 ^0.7 0 0.05 0 1 0
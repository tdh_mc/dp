# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une pipe vide en main (#joueurObjet temp = 300),
# et on va tester si on a dans l'autre main de quoi la remplir

# Si la condition passe, on remplit la pipe
execute if entity @p[distance=0,gamemode=!spectator,nbt={Inventory:[{Slot:-106b,tag:{ingredient:200}}]}] run function tdh:player/objets/objet/pipe/tabac/remplissage
execute if entity @p[distance=0,gamemode=!spectator,nbt={Inventory:[{Slot:-106b,tag:{ingredient:250}}]}] run function tdh:player/objets/objet/pipe/autre/remplissage
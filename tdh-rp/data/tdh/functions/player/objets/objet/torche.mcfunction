# On exécute cette fonction à la position d'un joueur pas en spectateur,
# si on tient une torche avec temps de combustion en main (#joueurObjet temp = 100..199 du moins au plus combustionné)

# On vérifie s'il faut diviser le stack (= si on tient 2 objets ou +)
# Désactivé car c'était trop relou à poser
# À la place on diminue les chances du stack de se consumer de manière linéaire en fonction du nombre de torches composant ce stack
#function tdh:player/objets/objet/test_diviser_pile

# On prend une variable aléatoire pour déterminer si la torche se consume ou non
scoreboard players set #random min 0
scoreboard players set #random max 100
execute store result score #random temp run data get entity @p[gamemode=!spectator,distance=0] SelectedItem.Count
scoreboard players operation #random max *= #random temp
function tdh:random

# La torche a 1 chance sur (4 x nombre de torches) de se consumer d'un niveau à chaque seconde
execute if score #random temp matches ..24 run function tdh:player/objets/objet/torche/combustion

# Elle émet dans tous les cas des particules
# On récupère une variable aléatoire pour le choix de la particule
scoreboard players set #random max 4
function tdh:random

# On émet une particule de fumée
execute if score #random temp matches 0 run function tdh:player/objets/objet/torche/particule/fumee1
execute if score #random temp matches 1 run function tdh:player/objets/objet/torche/particule/fumee2
execute if score #random temp matches 2 run function tdh:player/objets/objet/torche/particule/fumee3
execute if score #random temp matches 3 run function tdh:player/objets/objet/torche/particule/fumee4

# On émet des particules de cendre qui tombent
function tdh:player/objets/objet/torche/particule/cendre
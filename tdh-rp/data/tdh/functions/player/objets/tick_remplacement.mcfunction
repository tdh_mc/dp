# ---- CALCUL DES OBJETS TENUS PAR LE JOUEUR ----
# On vérifie si des objets auxquels on doit ajouter du NBT ont été craftés
execute at @a[gamemode=!spectator,scores={torchesCraftees=1..}] run function tdh:player/objets/objet/torche/remplacer
execute at @a[gamemode=!spectator,scores={torchesRamassees=1..}] run function tdh:player/objets/objet/torche/remplacer

# Tous les joueurs qui ont droppé ou cassé des torches testent les items alentour pour remplacer les torches par des torches TDH
execute at @a[gamemode=!spectator,scores={torchesDroppees=1..}] run function tdh:player/objets/objet/torche/remplacer_drops
execute at @a[gamemode=!spectator,scores={torchesDetruites=1..}] run function tdh:player/objets/objet/torche/remplacer_drops

# On réinitialise les scores de tous les joueurs
scoreboard players reset @a torchesCraftees
scoreboard players reset @a torchesRamassees
scoreboard players reset @a torchesDroppees
scoreboard players reset @a torchesDetruites
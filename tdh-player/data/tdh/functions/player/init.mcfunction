# Variables relatives aux compétences des joueurs
scoreboard objectives add vie health "Vie"

scoreboard objectives add eloquence dummy "Compétence Éloquence"
scoreboard objectives add eloquenceXp dummy "Expérience en Éloquence"
scoreboard objectives add force dummy "Compétence Force"
scoreboard objectives add forceXp dummy "Expérience en Force"
scoreboard objectives add agilite dummy "Compétence Agilité"
scoreboard objectives add agiliteXp dummy "Expérience en Agilité"
scoreboard objectives add defense dummy "Compétence Défense"
scoreboard objectives add defenseXp dummy "Expérience en Défense"

# Variables de tracking des actions des joueurs pour leurs gains d'expérience quotidiens
scoreboard objectives add xpGrimper minecraft.custom:minecraft.climb_one_cm "XP due à l'escalade"
scoreboard objectives add xpPlonger minecraft.custom:minecraft.swim_one_cm "XP due à la plongée"
scoreboard objectives add xpPecher minecraft.custom:minecraft.fish_caught "XP due aux poissons pêchés"
scoreboard objectives add xpDmgFaits minecraft.custom:minecraft.damage_dealt "XP due aux dommages infligés"
scoreboard objectives add xpDmgSubis minecraft.custom:minecraft.damage_taken "XP due aux dommages subis"
scoreboard objectives add xpDmgBloques minecraft.custom:minecraft.damage_blocked_by_shield "XP due aux dommages bloqués"
scoreboard objectives add xpCiblesTouchees minecraft.custom:minecraft.target_hit "XP due aux cibles touchées"
scoreboard objectives add xpMobsTues minecraft.custom:minecraft.mob_kills "XP due aux mobs tués"
scoreboard objectives add xpJoueursTues minecraft.custom:minecraft.player_kills "XP due aux joueurs tués"
# On réinitialise la vitesse du joueur (= on la met au niveau par défaut, qui sera changé à la prochaine itération du calcul vitesse)
scoreboard players set @a[distance=0] vitesse 500
attribute @p[gamemode=!spectator,distance=0] minecraft:generic.movement_speed base set 0.07
attribute @p[gamemode=spectator,distance=0] minecraft:generic.movement_speed base set 0.07
# Calcul de la vitesse actuelle du joueur s'il ne vole pas et n'est pas en spectateur
# Les différents effets à appliquer sont :
# - altitude au-dessus de 100
# - précipitations (pluie ou orage)
# - intensité de la météo (=vent)
# On attend une sortie entre 0 et 500, avec 0 = on ne bouge pas et 500 = vitesse normale

# On enregistre l'altitude dans une variable temporaire
scoreboard players operation #joueur temp = @p[distance=0,gamemode=!spectator] altitudeFromZero
# On retranche 100 (avec un minimum à 0)
scoreboard players set #joueur temp2 100
scoreboard players operation #joueur temp > #joueur temp2
scoreboard players operation #joueur temp -= #joueur temp2

# On a désormais une valeur entre 0 et 156 d'altitude en "trop"
# S'il y a des précipitations, on ajoute une valeur fixe
execute if score #meteo actuelle matches 2 run scoreboard players add #joueur temp 40
execute if score #meteo actuelle matches 3 run scoreboard players add #joueur temp 100
# Si la météo est intense, on ajoute une valeur fixe
execute if score #meteo intensite matches 1 run scoreboard players add #joueur temp 8
execute if score #meteo intensite matches 2 run scoreboard players add #joueur temp 24
execute if score #meteo intensite matches 3 run scoreboard players add #joueur temp 64
execute if score #meteo intensite matches 4.. run scoreboard players add #joueur temp 192

# On va la multiplier par -1 pour avoir une valeur de ralentissement entre 0 et -500
scoreboard players set #joueur temp2 -1
scoreboard players operation #joueur temp *= #joueur temp2

# On multiplie par (1 - l'insideness) ; plus on est à l'intérieur, moins la réduction est importante
# On utilise ce nombre au carré pour n'avoir une vraie réduction de la vitesse que quand on est proche de 100% extérieur
scoreboard players set #joueur temp2 256
scoreboard players set #joueur temp3 16
scoreboard players operation #joueur temp3 -= @p[distance=0,gamemode=!spectator] insideness
scoreboard players operation #joueur temp3 *= #joueur temp3
scoreboard players operation #joueur temp *= #joueur temp3
scoreboard players operation #joueur temp /= #joueur temp2

# On ajoute 500 pour avoir une valeur comprise entre 0 et 500
scoreboard players add #joueur temp 500

# Si la nouvelle vitesse est différente de l'ancienne, on change notre attribut
execute unless score #joueur temp = @p[distance=0,gamemode=!spectator] vitesse run function tdh:player/scores/vitesse/changer

# On vérifie si on doit bénéficier du boost de vitesse lié au déplacement sur route,
# mais pas s'il y a de la neige dans le bloc au-dessus (on ne veut pas profiter du bonus si la route est enneigée)
scoreboard players set #joueur temp2 0
execute if block ~ ~-0.4 ~ #tdh:route unless block ~ ~0.6 ~ snow run scoreboard players set #joueur temp2 1
execute if block ~ ~-1.4 ~ #tdh:route unless block ~ ~-0.4 ~ snow run scoreboard players set #joueur temp2 1
# On vérifie avant d'appliquer l'effet si le joueur a déjà le tag,
# pour éviter de l'appliquer en boucle si l'état ne change pas
execute if score #joueur temp2 matches 0 if entity @p[distance=0,gamemode=!spectator,tag=BonusRoute] run function tdh:player/scores/vitesse/bonus/route_off
execute if score #joueur temp2 matches 1 if entity @p[distance=0,gamemode=!spectator,tag=!BonusRoute] run function tdh:player/scores/vitesse/bonus/route_on

# On vérifie si on doit bénéficier du boost de vitesse lié au sprint
scoreboard players set #joueur temp2 0
execute at @p[distance=0,gamemode=!spectator,predicate=tdh:is_sprinting] run scoreboard players set #joueur temp2 1

# On vérifie avant d'appliquer l'effet si le joueur a déjà le tag,
# pour éviter de l'appliquer en boucle si l'état ne change pas
execute if score #joueur temp2 matches 0 if entity @p[distance=0,gamemode=!spectator,tag=BonusCourse] run function tdh:player/scores/vitesse/bonus/course_off
execute if score #joueur temp2 matches 1 if entity @p[distance=0,gamemode=!spectator,tag=!BonusCourse] run function tdh:player/scores/vitesse/bonus/course_on

# On réinitialise la variable temporaire
scoreboard players reset #joueur temp
scoreboard players reset #joueur temp2
scoreboard players reset #joueur temp3
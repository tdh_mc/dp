# On ajoute le modifier de vitesse (x1.5)
attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 9a77add9-d744-4811-9059-5056088486a6 "Bonus de course" 0.5 multiply_base
# On ajoute le modifier de résistance au knockback (+50% additifs)
attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 36a44d2b-3ce8-4db2-a651-169a45fc5279 "Bonus de course" 0.5 add
# On ajoute le modifier de vitesse d'attaque (x0.5)
attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add 66a1ceb3-248e-4e6e-8540-fcdad8a267a0 "Bonus de course" -0.5 multiply_base

# On enregistre le fait qu'on a ajouté le bonus
tag @p[distance=0,gamemode=!spectator] add BonusCourse
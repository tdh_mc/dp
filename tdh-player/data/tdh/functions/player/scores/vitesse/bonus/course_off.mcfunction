# On supprime les modifiers
attribute @p[distance=0,gamemode=!spectator,tag=BonusCourse] generic.movement_speed modifier remove 9a77add9-d744-4811-9059-5056088486a6
attribute @p[distance=0,gamemode=!spectator,tag=BonusCourse] generic.knockback_resistance modifier remove 36a44d2b-3ce8-4db2-a651-169a45fc5279
attribute @p[distance=0,gamemode=!spectator,tag=BonusCourse] generic.attack_speed modifier remove 66a1ceb3-248e-4e6e-8540-fcdad8a267a0

# On enregistre le fait qu'on a supprimé le bonus
tag @p[distance=0,gamemode=!spectator,tag=BonusCourse] remove BonusCourse
# On supprime le modifier
attribute @p[distance=0,gamemode=!spectator,tag=BonusRoute] generic.movement_speed modifier remove 8b60ae09-070d-48a9-b913-d4a4154e6177

# On enregistre le fait qu'on a supprimé le bonus
tag @p[distance=0,gamemode=!spectator,tag=BonusRoute] remove BonusRoute
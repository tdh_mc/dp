# On ajoute le modifier de vitesse
attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8b60ae09-070d-48a9-b913-d4a4154e6177 "Bonus de déplacement sur route" 0.19 multiply_base

# On enregistre le fait qu'on a ajouté le bonus
tag @p[distance=0,gamemode=!spectator] add BonusRoute
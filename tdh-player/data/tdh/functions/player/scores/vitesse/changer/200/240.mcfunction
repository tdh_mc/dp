# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 240..241 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03850
execute if score #joueur temp matches 242..243 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03880
execute if score #joueur temp matches 244..245 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03910
execute if score #joueur temp matches 246..247 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03940
execute if score #joueur temp matches 248..249 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03970
execute if score #joueur temp matches 250..251 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04000
execute if score #joueur temp matches 252..253 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04030
execute if score #joueur temp matches 254..255 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04060
execute if score #joueur temp matches 256..257 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04090
execute if score #joueur temp matches 258..259 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04120
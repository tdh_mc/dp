# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 100..101 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01750
execute if score #joueur temp matches 102..103 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01780
execute if score #joueur temp matches 104..105 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01810
execute if score #joueur temp matches 106..107 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01840
execute if score #joueur temp matches 108..109 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01870
execute if score #joueur temp matches 110..111 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01900
execute if score #joueur temp matches 112..113 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01930
execute if score #joueur temp matches 114..115 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01960
execute if score #joueur temp matches 116..117 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01990
execute if score #joueur temp matches 118..119 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02020
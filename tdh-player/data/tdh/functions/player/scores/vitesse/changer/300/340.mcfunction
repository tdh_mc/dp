# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 340..341 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05250
execute if score #joueur temp matches 342..343 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05275
execute if score #joueur temp matches 344..345 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05300
execute if score #joueur temp matches 346..347 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05325
execute if score #joueur temp matches 348..349 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05350
execute if score #joueur temp matches 350..351 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05375
execute if score #joueur temp matches 352..353 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05400
execute if score #joueur temp matches 354..355 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05425
execute if score #joueur temp matches 356..357 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05450
execute if score #joueur temp matches 358..359 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05475
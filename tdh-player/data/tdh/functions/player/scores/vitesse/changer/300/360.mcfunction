# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 360..361 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05500
execute if score #joueur temp matches 362..363 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05525
execute if score #joueur temp matches 364..365 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05550
execute if score #joueur temp matches 366..367 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05575
execute if score #joueur temp matches 368..369 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05600
execute if score #joueur temp matches 370..371 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05625
execute if score #joueur temp matches 372..373 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05650
execute if score #joueur temp matches 374..375 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05675
execute if score #joueur temp matches 376..377 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05700
execute if score #joueur temp matches 378..379 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05725
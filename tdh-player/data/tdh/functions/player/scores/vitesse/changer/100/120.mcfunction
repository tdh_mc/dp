# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 120..121 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02050
execute if score #joueur temp matches 122..123 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02080
execute if score #joueur temp matches 124..125 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02110
execute if score #joueur temp matches 126..127 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02140
execute if score #joueur temp matches 128..129 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02170
execute if score #joueur temp matches 130..131 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02200
execute if score #joueur temp matches 132..133 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02230
execute if score #joueur temp matches 134..135 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02260
execute if score #joueur temp matches 136..137 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02290
execute if score #joueur temp matches 138..139 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02320
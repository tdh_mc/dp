# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 320..321 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05000
execute if score #joueur temp matches 322..323 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05025
execute if score #joueur temp matches 324..325 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05050
execute if score #joueur temp matches 326..327 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05075
execute if score #joueur temp matches 328..329 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05100
execute if score #joueur temp matches 330..331 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05125
execute if score #joueur temp matches 332..333 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05150
execute if score #joueur temp matches 334..335 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05175
execute if score #joueur temp matches 336..337 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05200
execute if score #joueur temp matches 338..339 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.05225
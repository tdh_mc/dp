# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 100..119 run function tdh:player/scores/vitesse/changer/100/100
execute if score #joueur temp matches 120..139 run function tdh:player/scores/vitesse/changer/100/120
execute if score #joueur temp matches 140..159 run function tdh:player/scores/vitesse/changer/100/140
execute if score #joueur temp matches 160..179 run function tdh:player/scores/vitesse/changer/100/160
execute if score #joueur temp matches 180..199 run function tdh:player/scores/vitesse/changer/100/180
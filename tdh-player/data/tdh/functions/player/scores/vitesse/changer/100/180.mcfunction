# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 180..181 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02950
execute if score #joueur temp matches 182..183 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02980
execute if score #joueur temp matches 184..185 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03010
execute if score #joueur temp matches 186..187 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03040
execute if score #joueur temp matches 188..189 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03070
execute if score #joueur temp matches 190..191 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03100
execute if score #joueur temp matches 192..193 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03130
execute if score #joueur temp matches 194..195 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03160
execute if score #joueur temp matches 196..197 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03190
execute if score #joueur temp matches 198..199 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03220
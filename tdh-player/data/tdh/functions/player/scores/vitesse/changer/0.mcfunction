# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 0..19 run function tdh:player/scores/vitesse/changer/0/0
execute if score #joueur temp matches 20..39 run function tdh:player/scores/vitesse/changer/0/20
execute if score #joueur temp matches 40..59 run function tdh:player/scores/vitesse/changer/0/40
execute if score #joueur temp matches 60..79 run function tdh:player/scores/vitesse/changer/0/60
execute if score #joueur temp matches 80..99 run function tdh:player/scores/vitesse/changer/0/80
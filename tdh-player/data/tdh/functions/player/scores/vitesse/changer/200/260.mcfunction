# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 260..261 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04150
execute if score #joueur temp matches 262..263 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04180
execute if score #joueur temp matches 264..265 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04210
execute if score #joueur temp matches 266..267 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04240
execute if score #joueur temp matches 268..269 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04270
execute if score #joueur temp matches 270..271 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04300
execute if score #joueur temp matches 272..273 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04330
execute if score #joueur temp matches 274..275 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04360
execute if score #joueur temp matches 276..277 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04390
execute if score #joueur temp matches 278..279 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04420
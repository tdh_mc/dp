# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 40..41 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00850
execute if score #joueur temp matches 42..43 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00880
execute if score #joueur temp matches 44..45 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00910
execute if score #joueur temp matches 46..47 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00940
execute if score #joueur temp matches 48..49 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00970
execute if score #joueur temp matches 50..51 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01000
execute if score #joueur temp matches 52..53 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01030
execute if score #joueur temp matches 54..55 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01060
execute if score #joueur temp matches 56..57 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01090
execute if score #joueur temp matches 58..59 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01120
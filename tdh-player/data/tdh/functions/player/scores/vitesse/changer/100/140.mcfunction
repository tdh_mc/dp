# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 140..141 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02350
execute if score #joueur temp matches 142..143 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02380
execute if score #joueur temp matches 144..145 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02410
execute if score #joueur temp matches 146..147 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02440
execute if score #joueur temp matches 148..149 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02470
execute if score #joueur temp matches 150..151 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02500
execute if score #joueur temp matches 152..153 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02530
execute if score #joueur temp matches 154..155 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02560
execute if score #joueur temp matches 156..157 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02590
execute if score #joueur temp matches 158..159 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.02620
# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 280..281 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04450
execute if score #joueur temp matches 282..283 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04480
execute if score #joueur temp matches 284..285 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04510
execute if score #joueur temp matches 286..287 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04540
execute if score #joueur temp matches 288..289 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04570
execute if score #joueur temp matches 290..291 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04600
execute if score #joueur temp matches 292..293 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04630
execute if score #joueur temp matches 294..295 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04660
execute if score #joueur temp matches 296..297 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04690
execute if score #joueur temp matches 298..299 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04720
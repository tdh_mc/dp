# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 220..221 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03550
execute if score #joueur temp matches 222..223 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03580
execute if score #joueur temp matches 224..225 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03610
execute if score #joueur temp matches 226..227 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03640
execute if score #joueur temp matches 228..229 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03670
execute if score #joueur temp matches 230..231 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03700
execute if score #joueur temp matches 232..233 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03730
execute if score #joueur temp matches 234..235 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03760
execute if score #joueur temp matches 236..237 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03790
execute if score #joueur temp matches 238..239 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03820
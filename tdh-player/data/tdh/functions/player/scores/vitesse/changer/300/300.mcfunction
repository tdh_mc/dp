# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 300..301 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04750
execute if score #joueur temp matches 302..303 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04775
execute if score #joueur temp matches 304..305 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04800
execute if score #joueur temp matches 306..307 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04825
execute if score #joueur temp matches 308..309 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04850
execute if score #joueur temp matches 310..311 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04875
execute if score #joueur temp matches 312..313 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04900
execute if score #joueur temp matches 314..315 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04925
execute if score #joueur temp matches 316..317 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04950
execute if score #joueur temp matches 318..319 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.04975
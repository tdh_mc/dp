# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 200..201 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03250
execute if score #joueur temp matches 202..203 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03280
execute if score #joueur temp matches 204..205 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03310
execute if score #joueur temp matches 206..207 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03340
execute if score #joueur temp matches 208..209 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03370
execute if score #joueur temp matches 210..211 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03400
execute if score #joueur temp matches 212..213 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03430
execute if score #joueur temp matches 214..215 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03460
execute if score #joueur temp matches 216..217 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03490
execute if score #joueur temp matches 218..219 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.03520
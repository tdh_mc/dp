# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 20..21 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00550
execute if score #joueur temp matches 22..23 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00580
execute if score #joueur temp matches 24..25 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00610
execute if score #joueur temp matches 26..27 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00640
execute if score #joueur temp matches 28..29 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00670
execute if score #joueur temp matches 30..31 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00700
execute if score #joueur temp matches 32..33 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00730
execute if score #joueur temp matches 34..35 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00760
execute if score #joueur temp matches 36..37 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00790
execute if score #joueur temp matches 38..39 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00820
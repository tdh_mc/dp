# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 80..81 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01450
execute if score #joueur temp matches 82..83 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01480
execute if score #joueur temp matches 84..85 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01510
execute if score #joueur temp matches 86..87 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01540
execute if score #joueur temp matches 88..89 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01570
execute if score #joueur temp matches 90..91 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01600
execute if score #joueur temp matches 92..93 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01630
execute if score #joueur temp matches 94..95 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01660
execute if score #joueur temp matches 96..97 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01690
execute if score #joueur temp matches 98..99 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01720
# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 60..61 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01150
execute if score #joueur temp matches 62..63 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01180
execute if score #joueur temp matches 64..65 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01210
execute if score #joueur temp matches 66..67 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01240
execute if score #joueur temp matches 68..69 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01270
execute if score #joueur temp matches 70..71 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01300
execute if score #joueur temp matches 72..73 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01330
execute if score #joueur temp matches 74..75 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01360
execute if score #joueur temp matches 76..77 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01390
execute if score #joueur temp matches 78..79 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.01420
# On a plusieurs checks selon la valeur de la variable
execute if score #joueur temp matches 0..1 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00250
execute if score #joueur temp matches 2..3 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00280
execute if score #joueur temp matches 4..5 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00310
execute if score #joueur temp matches 6..7 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00340
execute if score #joueur temp matches 8..9 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00370
execute if score #joueur temp matches 10..11 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00400
execute if score #joueur temp matches 12..13 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00430
execute if score #joueur temp matches 14..15 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00460
execute if score #joueur temp matches 16..17 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00490
execute if score #joueur temp matches 18..19 run attribute @p[gamemode=!spectator,distance=0] generic.movement_speed base set 0.00520
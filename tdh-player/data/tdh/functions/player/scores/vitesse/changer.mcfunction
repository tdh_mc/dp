# Calcul de la nouvelle vitesse du joueur
# On a la variable #joueur temp définie à la valeur de notre vitesse, de 0 (aucun mouvement) à 500 (vitesse normale)

# On enregistre la nouvelle vitesse
scoreboard players operation @p[distance=0,gamemode=!spectator] vitesse = #joueur temp

# Si on est à vitesse normale, on la définit
execute if score #joueur temp matches 500 run attribute @p[distance=0,gamemode=!spectator] minecraft:generic.movement_speed base set 0.07

# Sinon, on fait un check par centaine
execute if score #joueur temp matches ..99 run function tdh:player/scores/vitesse/changer/0
execute if score #joueur temp matches 100..199 run function tdh:player/scores/vitesse/changer/100
execute if score #joueur temp matches 200..299 run function tdh:player/scores/vitesse/changer/200
execute if score #joueur temp matches 300..399 run function tdh:player/scores/vitesse/changer/300
execute if score #joueur temp matches 400..499 run function tdh:player/scores/vitesse/changer/400
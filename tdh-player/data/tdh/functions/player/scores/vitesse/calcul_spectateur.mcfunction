# Calcul de la vitesse actuelle du joueur s'il est en spectateur
# Toujours la même, 500 / 500 (le max)
scoreboard players set #joueur temp 500

# Si la nouvelle vitesse est différente de l'ancienne, on change notre attribut
execute unless score #joueur temp = @p[distance=0,gamemode=spectator] vitesse run attribute @p[distance=0,gamemode=spectator] minecraft:generic.movement_speed base set 0.07

# On change notre vitesse
scoreboard players operation @p[distance=0,gamemode=spectator] vitesse = #joueur temp

# On réinitialise la variable temporaire
scoreboard players reset #joueur temp
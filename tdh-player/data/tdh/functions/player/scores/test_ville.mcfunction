# On a une sous-fonction différente selon si on est déjà considérés comme en ville, ou non
execute if entity @p[distance=0,tag=InTown] run function tdh:player/scores/test_ville/en_ville
execute if entity @p[distance=0,tag=InVillage] run function tdh:player/scores/test_ville/en_village
execute if entity @p[distance=0,tag=!InTown,tag=!InVillage] run function tdh:player/scores/test_ville/hors_ville
# On enregistre le type de biome dans lequel se trouve le joueur
# 3: DEFAULT (il pleut)
# 1: FROID (il neige) / 2 (il neige mais plus haut que notre altitude)
# 4: CHAUD (pas de précipitations)

# - Par défaut, il pleut
scoreboard players set @a[distance=0] biomeType 3
# - Si on est dans un biome désertique ou enneigé, on se donne le type correspondant
execute if predicate tdh:biome/desert run scoreboard players set @a[distance=0] biomeType 4
execute if score @p biomeType matches 3 if predicate tdh:biome/neige run scoreboard players set @a[distance=0] biomeType 1
execute if score @p biomeType matches 3 if predicate tdh:biome/neigeux run scoreboard players set @a[distance=0] biomeType 2
# Calcul de la vitesse actuelle du joueur

# On a une sous fonction différente selon notre gamemode et si on vole ou non
execute if entity @p[distance=0,gamemode=!spectator,tag=IsFlying] run function tdh:player/scores/vitesse/calcul_vol
execute if entity @p[distance=0,gamemode=!spectator,tag=!IsFlying] run function tdh:player/scores/vitesse/calcul
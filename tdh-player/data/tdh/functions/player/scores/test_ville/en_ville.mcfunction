# On est à la position d'un joueur qui a le tag InTown
# On veut vérifier s'il y a toujours une ville proche de nous

# On vérifie si le joueur est à portée d'une ville, et on quitte la ville si on n'en trouve pas
scoreboard players set #joueur temp5 0
execute positioned ~-150 0 ~-150 unless entity @e[type=armor_stand,tag=NomVille,dx=300,dy=256,dz=300] run scoreboard players set #joueur temp5 1
execute if score #joueur temp5 matches 1 run function tdh:player/scores/test_ville/sortie_ville
scoreboard players reset #joueur temp5
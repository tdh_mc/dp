# On est à la position d'un joueur qui a le tag InVillage
# On veut vérifier s'il y a toujours un village proche de nous

# On vérifie si le joueur est à portée d'un village, et on quitte le village si on n'en trouve pas
scoreboard players set #joueur temp5 0
execute positioned ~-90 0 ~-90 unless entity @e[type=armor_stand,tag=NomVillage,dx=180,dy=256,dz=180] run scoreboard players set #joueur temp5 1
execute if score #joueur temp5 matches 1 run function tdh:player/scores/test_ville/sortie_village
scoreboard players reset #joueur temp5
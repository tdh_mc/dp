# On est à la position d'un joueur qui n'a ni le tag InTown, ni le tag InVillage
# On veut vérifier s'il y a une ville proche de nous

# On vérifie si le joueur est à portée d'une ville ou d'un village, et on stocke le résultat dans une variable temporaire
scoreboard players set #joueur temp6 0
execute positioned ~-60 0 ~-60 if entity @e[type=armor_stand,tag=NomVillage,dx=120,dy=256,dz=120] run scoreboard players set #joueur temp6 1
execute positioned ~-120 0 ~-120 if entity @e[type=armor_stand,tag=NomVille,dx=240,dy=256,dz=240] run scoreboard players set #joueur temp6 2

# Si on a un score différent de 0, on exécute la fonction d'entrée correspondante
execute if score #joueur temp6 matches 1 run function tdh:player/scores/test_ville/entree_village
execute if score #joueur temp6 matches 2 run function tdh:player/scores/test_ville/entree_ville

# On réinitialise la variable temporaire
scoreboard players reset #joueur temp6
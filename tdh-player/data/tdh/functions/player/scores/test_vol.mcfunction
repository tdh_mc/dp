# On réinitialise notre tag
tag @p[distance=0,gamemode=!spectator] remove IsFlying

# Si le bloc sur lequel on se trouve et les 2 du dessous sont tous vides, on est en train de voler
execute if blocks ~ ~ ~ ~ ~-2 ~ ~ 253 ~ masked run tag @p[distance=0,gamemode=!spectator] add IsFlying
# Si un joueur pas encore en combat vient d'attaquer ou d'être attaqué, il passe en mode combat
execute at @a[advancements={tdh:combat_check={player_hurt=true}},tag=!EnCombat] run function tdh:player/combat/start
execute at @a[advancements={tdh:combat_check={player_attack=true}},tag=!EnCombat] run function tdh:player/combat/start

# Si un joueur déjà en combat vient d'attaquer ou d'être attaqué, on réinitialise son compteur (depuis le dernier dommage)
execute at @a[advancements={tdh:combat_check={player_hurt=true}},tag=EnCombat] run function tdh:player/combat/hurt
execute at @a[advancements={tdh:combat_check={player_attack=true}},tag=EnCombat] run function tdh:player/combat/hurt

# Si un joueur en combat vient de tuer un mob, on actualise le nombre de mobs en combat
execute at @a[advancements={tdh:combat_check={player_kill=true}},tag=EnCombat] run function tdh:player/combat/kill

# On affiche à tous les joueurs en combat le titre en bas de l'écran, si le combat dure depuis 1 seconde ou +
#execute at @a[tag=EnCombat,scores={combatDuration=20..}] run function tdh:player/combat/actionbar

# Si un joueur en combat est tué, on le sort du mode combat
execute at @a[advancements={tdh:combat_check={player_killed=true}},tag=EnCombat] run function tdh:player/combat/defaite

# S'il ne reste plus d'ennemis, c'est probablement qu'ils ont tous fui
execute at @a[tag=EnCombat] unless entity @e[type=#tdh:hostile,tag=EnCombat,distance=..47] run function tdh:player/combat/fuite

# S'il n'y a pas eu de dommages pendant 30 secondes et qu'aucun ennemi n'est à 16, on sort du combat
execute at @a[tag=EnCombat,scores={timeSinceLastDmg=600..}] positioned ~-16 ~-4 ~-16 unless entity @e[type=!player,dx=32,dy=36,dz=32,tag=EnCombat] positioned ~16 ~4 ~16 run function tdh:player/combat/fuite

# On réinitialise l'advancement
advancement revoke @a only tdh:combat_check

# On incrémente les compteurs
execute at @a[tag=EnCombat] run scoreboard players add @p combatDuration 1
execute at @a[tag=EnCombat] run scoreboard players add @p timeSinceLastDmg 1
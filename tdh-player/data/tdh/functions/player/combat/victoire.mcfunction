# En cas de victoire, tout le monde gagne dans un rayon de 16
tag @a[distance=..16] remove EnCombat
execute at @a[distance=..16] run scoreboard players reset @p combatDuration
execute at @a[distance=..16] run scoreboard players reset @p timeSinceLastDmg
execute at @a[distance=..16] run scoreboard players reset @p nombreEnnemis

# On désactive le bonus de vitesse de déplacement des gens qui ont une bonne compétence Agilité
execute at @a[distance=..16] run function tdh:player/xp/agilite/desactiver_bonus

# On affiche un message
title @a[distance=..16] actionbar [{"text":"Vous avez remporté le combat. ","color":"gray"},{"text":"C'est choueeette!","color":"green","bold":"true"}]
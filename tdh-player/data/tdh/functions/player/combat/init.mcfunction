# Variables relatives au combat
scoreboard objectives add timeSinceLastDmg dummy "Derniers dommages"
scoreboard objectives add combatDuration dummy "Durée du combat"
scoreboard objectives add nombreEnnemis dummy "Nombre d'ennemis"
scoreboard objectives add deces minecraft.custom:minecraft.deaths "Nombre de décès"
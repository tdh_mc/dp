# En cas de défaite (décès), c'est juste la personne morte qui perd, pas tt le monde à la fois
tag @p remove EnCombat
scoreboard players reset @p combatDuration
scoreboard players reset @p timeSinceLastDmg
scoreboard players reset @p nombreEnnemis

# On désactive le bonus de vitesse de déplacement des gens qui ont une bonne compétence Agilité
function tdh:player/xp/agilite/desactiver_bonus

# On affiche un message humiliant
title @p actionbar [{"text":"Vous avez perdu le combat. ","color":"gray"},{"text":"C'est con!","color":"red","bold":"true"}]
# Exécuté en tant qu'entité en combat, autre que le joueur, pour vérifier si elle est tjrs en combat
execute at @s[type=!phantom] positioned ~-32 ~-6 ~-32 unless entity @p[dx=64,dy=8,dz=64,tag=EnCombat] run tag @s remove EnCombat
execute at @s[type=phantom] unless entity @p[distance=..32,tag=EnCombat] run tag @s remove EnCombat

# On ajuste le nom de l'entité
data merge entity @s[tag=EnCombat,tag=!CombatName] {CustomName:"{\"text\":\"⚔\",\"color\":\"red\"}",CustomNameVisible:1b}
data remove entity @s[tag=!EnCombat,tag=CombatName] CustomName
data remove entity @s[tag=!EnCombat,tag=CombatName] CustomNameVisible

tag @s[tag=EnCombat] add CombatName
tag @s[tag=!EnCombat] remove CombatName
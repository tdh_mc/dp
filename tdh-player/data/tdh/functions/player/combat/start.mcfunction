tag @a[distance=..16] add EnCombat
function tdh:player/combat/aggro
execute if entity @e[tag=EnCombat,type=!player,distance=..9] run function tdh:player/combat/aggro_big
execute at @a[distance=..16] run scoreboard players set @p combatDuration 0
execute at @a[distance=..16] run function tdh:player/xp/agilite/activer_bonus
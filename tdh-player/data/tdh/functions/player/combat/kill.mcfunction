# Les ennemis à proximité rejoignent le combat
function tdh:player/combat/aggro
scoreboard players set @p timeSinceLastDmg 0

# S'il ne reste pas d'ennemis, on vient d'obtenir une victoire
execute at @p unless entity @e[type=#tdh:hostile,tag=EnCombat,distance=..47] run function tdh:player/combat/victoire
# En cas de fuite, tout le monde gagne dans un rayon de 16
tag @a[distance=..16] remove EnCombat

# On désactive le bonus de vitesse de déplacement des gens qui ont une bonne compétence Agilité
execute at @a[distance=..16] run function tdh:player/xp/agilite/desactiver_bonus

# On affiche un message si le combat était un "vrai" combat (+ de 10 secondes)
execute if score @p combatDuration matches 200.. run title @a[distance=..16] actionbar [{"text":"Tous vos ennemis... semblent avoir ","color":"gray"},{"text":"disparu","color":"gray","bold":"true"},{"text":" disparu","color":"gray"},{"text":" ᵈᶦˢᵖᵃʳᵘ","color":"gray"}]

execute at @a[distance=..16] run scoreboard players reset @p combatDuration
execute at @a[distance=..16] run scoreboard players reset @p timeSinceLastDmg
execute at @a[distance=..16] run scoreboard players reset @p nombreEnnemis
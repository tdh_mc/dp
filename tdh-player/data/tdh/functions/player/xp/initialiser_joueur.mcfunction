# On se donne les niveaux par défaut dans toutes les compétences
scoreboard players set @p[distance=0] eloquence 1
scoreboard players set @p[distance=0] eloquenceXp 0

scoreboard players set @p[distance=0] force 1
scoreboard players set @p[distance=0] forceXp 0

scoreboard players set @p[distance=0] agilite 1
scoreboard players set @p[distance=0] agiliteXp 0

scoreboard players set @p[distance=0] defense 1
scoreboard players set @p[distance=0] defenseXp 0
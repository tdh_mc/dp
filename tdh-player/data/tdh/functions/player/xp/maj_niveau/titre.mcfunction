# On enregistre le nombre de titres à afficher
execute store result score #xp temp run data get storage tdh:joueur level_up

# On a un title différent selon le nombre de titres
title @p[gamemode=!spectator,distance=0] times 10 80 10
execute if score #xp temp matches 1 run title @p[gamemode=!spectator,distance=0] subtitle [{"storage":"tdh:joueur","nbt":"level_up[0]","interpret":"true"}]
execute if score #xp temp matches 2 run title @p[gamemode=!spectator,distance=0] subtitle [{"storage":"tdh:joueur","nbt":"level_up[0]","interpret":"true"},{"text":", "},{"storage":"tdh:joueur","nbt":"level_up[1]","interpret":"true"}]
execute if score #xp temp matches 3 run title @p[gamemode=!spectator,distance=0] subtitle [{"storage":"tdh:joueur","nbt":"level_up[0]","interpret":"true"},{"text":", "},{"storage":"tdh:joueur","nbt":"level_up[1]","interpret":"true"},{"text":", "},{"storage":"tdh:joueur","nbt":"level_up[2]","interpret":"true"}]
execute if score #xp temp matches 4.. run title @p[gamemode=!spectator,distance=0] subtitle [{"storage":"tdh:joueur","nbt":"level_up[0]","interpret":"true"},{"text":", "},{"storage":"tdh:joueur","nbt":"level_up[1]","interpret":"true"},{"text":", "},{"storage":"tdh:joueur","nbt":"level_up[2]","interpret":"true"},{"text":", "},{"storage":"tdh:joueur","nbt":"level_up[3]","interpret":"true"}]
execute if score #xp temp matches 1.. run title @p[gamemode=!spectator,distance=0] title [{"text":"Niveau atteint","color":"aqua"}]

# Selon le nombre de level ups simultanés, on diffuse un son différent
execute if score #xp temp matches 1 run playsound tdh:player.levelup_small player @p[gamemode=!spectator,distance=0]
execute if score #xp temp matches 2 run playsound tdh:player.levelup_med player @p[gamemode=!spectator,distance=0]
execute if score #xp temp matches 3.. run playsound tdh:player.levelup_big player @p[gamemode=!spectator,distance=0]

# Si on a bien level up, on stoppe la musique et on en laisse une nouvelle reprendre naturellement
execute if score #xp temp matches 1.. run function tdh:sound/music/tick/reset_level_up
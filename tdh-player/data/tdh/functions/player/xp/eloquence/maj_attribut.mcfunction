# On a différents paliers qui augmentent nos compétences en matière de dommages et de vie

# On définit une variable temporaire pour éviter de faire 846 checks de sélecteurs
scoreboard players operation #xp temp9 = @p[distance=0,gamemode=!spectator] eloquence

# On supprime les bonus éventuels déjà offerts par les autres niveaux
attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier remove 5a408c85-8c30-42e1-9fcd-d6529a04628b

# Chance (additif)
# +0 au niveau 1-14
# +1 au niveau 15-19
execute if score #xp temp9 matches 50..69 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 1.0 add
# +2 au niveau 20-24
execute if score #xp temp9 matches 70..89 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 2.0 add
# +3 au niveau 25-29
execute if score #xp temp9 matches 90..109 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 3.0 add
# +4 au niveau 30-34
execute if score #xp temp9 matches 110..129 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 4.0 add
# +5 au niveau 35-39
execute if score #xp temp9 matches 130..149 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 5.0 add
# +6 au niveau 40-44
execute if score #xp temp9 matches 150..174 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 6.0 add
# +8 au niveau 45-49
execute if score #xp temp9 matches 175..199 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 8.0 add
# +10 au niveau 50-59
execute if score #xp temp9 matches 200..224 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 10.0 add
# +15 au niveau 60-74
execute if score #xp temp9 matches 225..249 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 15.0 add
# +20 au niveau 75-99
execute if score #xp temp9 matches 250..299 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 20.0 add
# +25 au niveau 100-149
execute if score #xp temp9 matches 100..149 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 25.0 add
# +37 au niveau 150-199
execute if score #xp temp9 matches 100..149 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 37.5 add
# +50 au niveau 200-249
execute if score #xp temp9 matches 100..149 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 50.0 add
# +75 au niveau 250-299
execute if score #xp temp9 matches 100..149 run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 75.0 add
# +100 au niveau 300+
execute if score #xp temp9 matches 300.. run attribute @p[distance=0,gamemode=!spectator] generic.luck modifier add 5a408c85-8c30-42e1-9fcd-d6529a04628b "Bonus de compétence Éloquence" 100.0 add


# On réinitialise la variable temporaire
scoreboard players reset #xp temp9
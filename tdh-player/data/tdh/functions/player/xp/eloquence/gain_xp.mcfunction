# On appelle cette fonction à la position d'un joueur (pas en spectateur) qui a gagné de l'XP d'éloquence
# et on va vérifier si on a atteint un niveau supérieur

# L'éloquence gagnée est stockée dans #joueur eloquenceXp
# On affiche un titre au joueur
title @p[gamemode=!spectator,distance=0] actionbar [{"text":"","color":"aqua"},{"text":"Éloquence: ","color":"gold"},{"text":"+"},{"score":{"name":"#joueur","objective":"eloquenceXp"}},{"text":"xp","color":"aqua"}]
# On ajoute la valeur à notre xp précédente
scoreboard players operation @p[gamemode=!spectator,distance=0] eloquenceXp += #joueur eloquenceXp
scoreboard players reset #joueur eloquenceXp
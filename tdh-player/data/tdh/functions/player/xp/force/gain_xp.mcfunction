# On appelle cette fonction à la position d'un joueur (pas en spectateur) qui a gagné de l'XP de force
# et on va vérifier si on a atteint un niveau supérieur

# La force gagnée est stockée dans #joueur forceXp
# On affiche un titre au joueur
title @p[gamemode=!spectator,distance=0] actionbar [{"text":"","color":"aqua"},{"text":"Force: ","color":"gold"},{"text":"+"},{"score":{"name":"#joueur","objective":"forceXp"}},{"text":"xp","color":"aqua"}]
# On ajoute la valeur à notre xp précédente
scoreboard players operation @p[gamemode=!spectator,distance=0] forceXp += #joueur forceXp
scoreboard players reset #joueur forceXp
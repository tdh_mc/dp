# On a différents paliers qui augmentent nos compétences en matière de dommages et de vie

# On définit une variable temporaire pour éviter de faire 846 checks de sélecteurs
scoreboard players operation #xp temp9 = @p[distance=0,gamemode=!spectator] force

# On supprime les bonus éventuels déjà offerts par les autres niveaux
attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier remove a56824c5-cb97-450f-bb2b-887ca98c8979
attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier remove d36af8b9-5820-417f-b806-a5a98298c718
attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier remove 81c7af3e-8188-438d-a2d9-c1f96260227b

# Dommages d'attaque
# +0 au niveau 1-2
# +0.1 au niveau 3-6
execute if score #xp temp9 matches 3..6 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.1 add
# +0.2 au niveau 7-11
execute if score #xp temp9 matches 7..11 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.2 add
# +0.3 au niveau 12-16
execute if score #xp temp9 matches 12..16 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.3 add
# +0.4 au niveau 17-21
execute if score #xp temp9 matches 17..21 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.4 add
# +0.5 au niveau 22-29
execute if score #xp temp9 matches 22..29 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.5 add
# +0.6 au niveau 30-44
execute if score #xp temp9 matches 30..44 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.6 add
# +0.7 au niveau 45-59
execute if score #xp temp9 matches 45..59 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.7 add
# +0.8 au niveau 60-74
execute if score #xp temp9 matches 60..74 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.8 add
# +0.9 au niveau 75-89
execute if score #xp temp9 matches 75..89 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 0.9 add
# +1.0 au niveau 90-104
execute if score #xp temp9 matches 90..104 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 1.0 add
# +1.5 au niveau 105+
execute if score #xp temp9 matches 105.. run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add d36af8b9-5820-417f-b806-a5a98298c718 "Bonus de compétence Force" 1.5 add

# *1 au niveau 1-29
# *1.04 au niveau 30-44
execute if score #xp temp9 matches 30..44 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add 81c7af3e-8188-438d-a2d9-c1f96260227b "Multiplicateur de compétence Force" 0.04 multiply
# *1.08 au niveau 45-59
execute if score #xp temp9 matches 45..59 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add 81c7af3e-8188-438d-a2d9-c1f96260227b "Multiplicateur de compétence Force" 0.08 multiply
# *1.12 au niveau 60-74
execute if score #xp temp9 matches 60..74 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add 81c7af3e-8188-438d-a2d9-c1f96260227b "Multiplicateur de compétence Force" 0.12 multiply
# *1.16 au niveau 75-87
execute if score #xp temp9 matches 75..87 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add 81c7af3e-8188-438d-a2d9-c1f96260227b "Multiplicateur de compétence Force" 0.16 multiply
# *1.20 au niveau 88-99
execute if score #xp temp9 matches 88..99 run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add 81c7af3e-8188-438d-a2d9-c1f96260227b "Multiplicateur de compétence Force" 0.20 multiply
# *1.30 au niveau 100+
execute if score #xp temp9 matches 100.. run attribute @p[distance=0,gamemode=!spectator] generic.attack_damage modifier add 81c7af3e-8188-438d-a2d9-c1f96260227b "Multiplicateur de compétence Force" 0.30 multiply


# Santé maximum (additif)
# +0 au niveau 1-49
# +2 au niveau 50-69
execute if score #xp temp9 matches 50..69 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 2.0 add
# +3 au niveau 70-89
execute if score #xp temp9 matches 70..89 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 3.0 add
# +4 au niveau 90-109
execute if score #xp temp9 matches 90..109 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 4.0 add
# +5 au niveau 110-129
execute if score #xp temp9 matches 110..129 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 5.0 add
# +6 au niveau 130-149
execute if score #xp temp9 matches 130..149 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 6.0 add
# +7 au niveau 150-174
execute if score #xp temp9 matches 150..174 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 7.0 add
# +8 au niveau 175-199
execute if score #xp temp9 matches 175..199 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 8.0 add
# +9 au niveau 200-224
execute if score #xp temp9 matches 200..224 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 9.0 add
# +10 au niveau 225-249
execute if score #xp temp9 matches 225..249 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 10.0 add
# +11 au niveau 250-299
execute if score #xp temp9 matches 250..299 run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 11.0 add
# +12 au niveau 300+
execute if score #xp temp9 matches 300.. run attribute @p[distance=0,gamemode=!spectator] generic.max_health modifier add a56824c5-cb97-450f-bb2b-887ca98c8979 "Bonus de compétence Force" 12.0 add


# On réinitialise la variable temporaire
scoreboard players reset #xp temp9
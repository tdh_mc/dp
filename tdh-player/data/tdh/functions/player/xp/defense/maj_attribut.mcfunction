# On a différents paliers qui augmentent nos compétences en matière de puissance de l'armure et de résistance au knockback

# On définit une variable temporaire pour éviter de faire 846 checks de sélecteurs
scoreboard players operation #xp temp9 = @p[distance=0,gamemode=!spectator] defense

# On supprime les bonus éventuels déjà offerts par les autres niveaux
attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier remove 24b931c0-e6bc-4b9a-9c0b-be601f8abf76
attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier remove 001dbb14-2835-4530-bafc-17df8eafa1e4
attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier remove a7d7e8ea-479c-44aa-bfa9-a579fa39938f

# Puissance de l'armure (toughness)
# +0 au niveau 1
# +0.1 au niveau 2-3
execute if score #xp temp9 matches 2..3 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.1 add
# +0.2 au niveau 4-6
execute if score #xp temp9 matches 4..6 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.2 add
# +0.3 au niveau 7-10
execute if score #xp temp9 matches 7..10 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.3 add
# +0.4 au niveau 11-15
execute if score #xp temp9 matches 11..15 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.4 add
# +0.5 au niveau 16-20
execute if score #xp temp9 matches 16..20 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.5 add
# +0.6 au niveau 21-25
execute if score #xp temp9 matches 21..25 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.6 add
# +0.7 au niveau 26-30
execute if score #xp temp9 matches 26..30 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.7 add
# +0.8 au niveau 31-35
execute if score #xp temp9 matches 31..35 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.8 add
# +0.9 au niveau 36-40
execute if score #xp temp9 matches 36..40 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 0.9 add
# +1.0 au niveau 41-50
execute if score #xp temp9 matches 41..50 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 1.0 add
# +1.1 au niveau 51-60
execute if score #xp temp9 matches 51..60 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 1.1 add
# +1.2 au niveau 61-70
execute if score #xp temp9 matches 61..70 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 1.2 add
# +1.3 au niveau 71-80
execute if score #xp temp9 matches 71..80 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 1.3 add
# +1.4 au niveau 81-90
execute if score #xp temp9 matches 81..90 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 1.4 add
# +1.5 au niveau 91-99
execute if score #xp temp9 matches 91..99 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 1.5 add
# +2.0 au niveau 100+
execute if score #xp temp9 matches 100.. run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add 001dbb14-2835-4530-bafc-17df8eafa1e4 "Bonus de compétence Défense" 2.0 add

# *1 au niveau 1-19
# *1.05 au niveau 20-37
execute if score #xp temp9 matches 20..37 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add a7d7e8ea-479c-44aa-bfa9-a579fa39938f "Multiplicateur de compétence Défense" 0.05 multiply
# *1.1 au niveau 38-54
execute if score #xp temp9 matches 38..54 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add a7d7e8ea-479c-44aa-bfa9-a579fa39938f "Multiplicateur de compétence Défense" 0.1 multiply
# *1.2 au niveau 55-67
execute if score #xp temp9 matches 55..67 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add a7d7e8ea-479c-44aa-bfa9-a579fa39938f "Multiplicateur de compétence Défense" 0.2 multiply
# *1.3 au niveau 68-83
execute if score #xp temp9 matches 68..83 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add a7d7e8ea-479c-44aa-bfa9-a579fa39938f "Multiplicateur de compétence Défense" 0.3 multiply
# *1.4 au niveau 84-96
execute if score #xp temp9 matches 84..96 run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add a7d7e8ea-479c-44aa-bfa9-a579fa39938f "Multiplicateur de compétence Défense" 0.4 multiply
# *1.5 au niveau 97+
execute if score #xp temp9 matches 97.. run attribute @p[distance=0,gamemode=!spectator] generic.armor_toughness modifier add a7d7e8ea-479c-44aa-bfa9-a579fa39938f "Multiplicateur de compétence Défense" 0.5 multiply


# Résistance au knockback
# +0 au niveau 1-2
# +0.02 au niveau 3-4
execute if score #xp temp9 matches 3..4 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.02 add
# +0.04 au niveau 5-8
execute if score #xp temp9 matches 5..8 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.04 add
# +0.06 au niveau 9-13
execute if score #xp temp9 matches 9..13 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.06 add
# +0.08 au niveau 14-18
execute if score #xp temp9 matches 14..18 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.08 add
# +0.10 au niveau 19-22
execute if score #xp temp9 matches 19..22 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.10 add
# +0.12 au niveau 23-27
execute if score #xp temp9 matches 23..27 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.12 add
# +0.14 au niveau 28-32
execute if score #xp temp9 matches 28..32 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.14 add
# +0.16 au niveau 33-37
execute if score #xp temp9 matches 33..37 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.16 add
# +0.18 au niveau 38-45
execute if score #xp temp9 matches 38..45 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.18 add
# +0.20 au niveau 46-55
execute if score #xp temp9 matches 46..55 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.20 add
# +0.22 au niveau 56-65
execute if score #xp temp9 matches 56..65 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.22 add
# +0.24 au niveau 66-75
execute if score #xp temp9 matches 66..75 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.24 add
# +0.26 au niveau 76-85
execute if score #xp temp9 matches 76..85 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.26 add
# +0.28 au niveau 86-95
execute if score #xp temp9 matches 86..95 run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.28 add
# +0.30 au niveau 96+
execute if score #xp temp9 matches 96.. run attribute @p[distance=0,gamemode=!spectator] generic.knockback_resistance modifier add 24b931c0-e6bc-4b9a-9c0b-be601f8abf76 "Bonus de compétence Défense" 0.30 add


# On réinitialise la variable temporaire
scoreboard players reset #xp temp9
# On appelle cette fonction à la position d'un joueur (pas en spectateur) qui a gagné de l'XP de défense
# et on va vérifier si on a atteint un niveau supérieur

# La défense gagnée est stockée dans #joueur defenseXp
# On affiche un titre au joueur
title @p[gamemode=!spectator,distance=0] actionbar [{"text":"","color":"aqua"},{"text":"Défense: ","color":"gold"},{"text":"+"},{"score":{"name":"#joueur","objective":"defenseXp"}},{"text":"xp","color":"aqua"}]
# On ajoute la valeur à notre xp précédente
scoreboard players operation @p[gamemode=!spectator,distance=0] defenseXp += #joueur defenseXp
scoreboard players reset #joueur defenseXp
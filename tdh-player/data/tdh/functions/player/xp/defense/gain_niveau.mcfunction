# On appelle cette fonction à la position d'un joueur (pas en spectateur) qui a gagné un niveau de défense

# On a déjà #xp max = le palier d'XP pour ce niveau
# On le retranche à nos points d'XP
scoreboard players operation @p[gamemode=!spectator,distance=0] defenseXp -= #xp max
# On gagne un niveau
scoreboard players add @p[gamemode=!spectator,distance=0] defense 1

# On modifie les attributs correspondants
function tdh:player/xp/defense/maj_attribut

# On se tag si on a gagné un niveau
tag @p[gamemode=!spectator,distance=0] add GainNiveau

# S'il nous reste des points d'XP, on vérifie si on a gagné d'autres niveaux
execute if score @p[gamemode=!spectator,distance=0] defenseXp matches 1.. run function tdh:player/xp/defense/calcul_xp
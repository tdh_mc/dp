# On calcule le maximum d'XP pour notre niveau actuel (Niveau * 100):
scoreboard players operation #xp max = @p[gamemode=!spectator,distance=0] defense
scoreboard players set #xp temp 100
scoreboard players operation #xp max *= #xp temp

# Si on a dépassé le palier, on gagne un niveau
execute if score @p[gamemode=!spectator,distance=0] defenseXp >= #xp max run function tdh:player/xp/defense/gain_niveau

# On réinitialise la variable temporaire
scoreboard players reset #xp max
scoreboard players reset #xp temp

# Si on a gagné un niveau, on enregistre un titre à afficher
execute if entity @p[gamemode=!spectator,tag=GainNiveau,distance=0] run data modify storage tdh:joueur level_up append value '[{"text":"","color":"aqua"},{"text":"Défense ","color":"gold"},{"text":"("},{"score":{"name":"@p[gamemode=!spectator,distance=0]","objective":"defense"}},{"text":")","color":"aqua"}]'
tag @p[gamemode=!spectator,distance=0] remove GainNiveau
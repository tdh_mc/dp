# Au début d'un combat, on ajoute le bonus de vitesse de déplacement
# On définit une variable temporaire pour éviter de faire 846 checks de sélecteurs
scoreboard players operation #xp temp9 = @p[distance=0,gamemode=!spectator] agilite

# +0 au niveau 1-11
# +0.005 au niveau 14-23
execute if score #xp temp9 matches 14..23 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 28788248-20d2-4981-a32b-97e8474d513a "Bonus de compétence Agilité en combat" 0.005 add
# +0.008 au niveau 24-34
execute if score #xp temp9 matches 24..34 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 28788248-20d2-4981-a32b-97e8474d513a "Bonus de compétence Agilité en combat" 0.008 add
# +0.012 au niveau 35-49
execute if score #xp temp9 matches 35..49 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 28788248-20d2-4981-a32b-97e8474d513a "Bonus de compétence Agilité en combat" 0.012 add
# +0.016 au niveau 50-64
execute if score #xp temp9 matches 50..64 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 28788248-20d2-4981-a32b-97e8474d513a "Bonus de compétence Agilité en combat" 0.016 add
# +0.020 au niveau 65-79
execute if score #xp temp9 matches 65..79 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 28788248-20d2-4981-a32b-97e8474d513a "Bonus de compétence Agilité en combat" 0.020 add
# +0.025 au niveau 80-94
execute if score #xp temp9 matches 80..94 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 28788248-20d2-4981-a32b-97e8474d513a "Bonus de compétence Agilité en combat" 0.025 add
# +0.030 au niveau 95+
execute if score #xp temp9 matches 95.. run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 28788248-20d2-4981-a32b-97e8474d513a "Bonus de compétence Agilité en combat" 0.030 add


# On réinitialise la variable temporaire
scoreboard players reset #xp temp9
# On appelle cette fonction à la position d'un joueur (pas en spectateur) qui a gagné de l'XP d'agilité
# et on va vérifier si on a atteint un niveau supérieur

# L'agilité gagnée est stockée dans #joueur agiliteXp
# On affiche un titre au joueur
title @p[gamemode=!spectator,distance=0] actionbar [{"text":"","color":"aqua"},{"text":"Agilité: ","color":"gold"},{"text":"+"},{"score":{"name":"#joueur","objective":"agiliteXp"}},{"text":"xp","color":"aqua"}]
# On ajoute la valeur à notre xp précédente
scoreboard players operation @p[gamemode=!spectator,distance=0] agiliteXp += #joueur agiliteXp
scoreboard players reset #joueur agiliteXp
# On a différents paliers qui augmentent nos compétences en matière de vitesse de déplacement et d'attaque

# On définit une variable temporaire pour éviter de faire 846 checks de sélecteurs
scoreboard players operation #xp temp9 = @p[distance=0,gamemode=!spectator] agilite

# On supprime les bonus éventuels déjà offerts par les autres niveaux
attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier remove d97d0135-fbfc-49c8-916a-4563bc5f9d69
attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier remove 8cfefa64-699a-4dca-a973-e77f105c1e3a

# Vitesse de déplacement (multiplicatif ; le modifier additif est ajouté uniquement pendant un combat)
# *1 au niveau 1-4
# *1.02 au niveau 5-9
execute if score #xp temp9 matches 5..9 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.02 multiply
# *1.04 au niveau 10-14
execute if score #xp temp9 matches 10..14 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.04 multiply
# *1.06 au niveau 15-19
execute if score #xp temp9 matches 15..19 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.06 multiply
# *1.08 au niveau 20-24
execute if score #xp temp9 matches 20..24 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.08 multiply
# *1.1 au niveau 25-39
execute if score #xp temp9 matches 25..39 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.1 multiply
# *1.15 au niveau 40-54
execute if score #xp temp9 matches 40..54 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.15 multiply
# *1.2 au niveau 55-69
execute if score #xp temp9 matches 55..69 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.2 multiply
# *1.25 au niveau 70-84
execute if score #xp temp9 matches 70..84 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.25 multiply
# *1.3 au niveau 85-99
execute if score #xp temp9 matches 85..99 run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.3 multiply
# *1.4 au niveau 100+
execute if score #xp temp9 matches 100.. run attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier add 8cfefa64-699a-4dca-a973-e77f105c1e3a "Multiplicateur de compétence Agilité" 0.4 multiply


# Vitesse d'attaque (additif)
# +0 au niveau 1-2
# +0.1 au niveau 3-6
execute if score #xp temp9 matches 3..6 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.1 add
# +0.2 au niveau 7-11
execute if score #xp temp9 matches 7..11 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.2 add
# +0.3 au niveau 12-16
execute if score #xp temp9 matches 12..16 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.3 add
# +0.4 au niveau 17-21
execute if score #xp temp9 matches 17..21 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.4 add
# +0.5 au niveau 22-29
execute if score #xp temp9 matches 22..29 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.5 add
# +0.6 au niveau 30-44
execute if score #xp temp9 matches 30..44 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.6 add
# +0.7 au niveau 45-59
execute if score #xp temp9 matches 45..59 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.7 add
# +0.8 au niveau 60-74
execute if score #xp temp9 matches 60..74 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.8 add
# +0.9 au niveau 75-89
execute if score #xp temp9 matches 75..89 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 0.9 add
# +1.0 au niveau 90-104
execute if score #xp temp9 matches 90..104 run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 1.0 add
# +1.25 au niveau 105+
execute if score #xp temp9 matches 105.. run attribute @p[distance=0,gamemode=!spectator] generic.attack_speed modifier add d97d0135-fbfc-49c8-916a-4563bc5f9d69 "Bonus de compétence Agilité" 1.25 add


# On réinitialise la variable temporaire
scoreboard players reset #xp temp9
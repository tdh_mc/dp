# À la fin d'un combat, on perd le bonus de vitesse de déplacement associé
attribute @p[distance=0,gamemode=!spectator] generic.movement_speed modifier remove 28788248-20d2-4981-a32b-97e8474d513a
attribute @p[distance=0,gamemode=spectator] generic.movement_speed modifier remove 28788248-20d2-4981-a32b-97e8474d513a
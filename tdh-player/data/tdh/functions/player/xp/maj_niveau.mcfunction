# On exécute cette fonction à la position d'un joueur en train de dormir,
# pour prendre en compte ses gains d'expérience précédents et appliquer son/ses nouveaux niveaux

# On s'ajoute l'expérience liée aux activités récemment pratiquées (depuis le dernier sommeil)
scoreboard players set #joueur agiliteXp 0
scoreboard players set #joueur defenseXp 0
#scoreboard players set #joueur eloquenceXp 0
scoreboard players set #joueur forceXp 0
# On multiplie nos stats "brutes" par différents facteurs
# Les compétences Agilité :
# - Grimper (=> +1 AGI tous les 5m)
scoreboard players set #joueur temp 500
scoreboard players operation @p[gamemode=!spectator,distance=0] xpGrimper /= #joueur temp
scoreboard players operation #joueur agiliteXp += @p[gamemode=!spectator,distance=0] xpGrimper
scoreboard players reset @p[gamemode=!spectator,distance=0] xpGrimper
# - Plonger (=> +1 AGI tous les 20m)
scoreboard players set #joueur temp 2000
scoreboard players operation @p[gamemode=!spectator,distance=0] xpPlonger /= #joueur temp
scoreboard players operation #joueur agiliteXp += @p[gamemode=!spectator,distance=0] xpPlonger
scoreboard players reset @p[gamemode=!spectator,distance=0] xpPlonger
# - Pêcher (=> +2 AGI pour chaque poisson pêché)
scoreboard players set #joueur temp 2
scoreboard players operation @p[gamemode=!spectator,distance=0] xpPecher *= #joueur temp
scoreboard players operation #joueur agiliteXp += @p[gamemode=!spectator,distance=0] xpPecher
scoreboard players reset @p[gamemode=!spectator,distance=0] xpPecher
# - Cibles touchées (=> +1 AGI pour chaque cible touchée)
#scoreboard players set #joueur temp 1
#scoreboard players operation @p[gamemode=!spectator,distance=0] xpCiblesTouchees *= #joueur temp
scoreboard players operation #joueur agiliteXp += @p[gamemode=!spectator,distance=0] xpCiblesTouchees
scoreboard players reset @p[gamemode=!spectator,distance=0] xpCiblesTouchees
# Les compétences Défense :
# - Dommages subis (=> +1 DEF à chaque coeur de dommages subis)
scoreboard players set #joueur temp 20
scoreboard players operation @p[gamemode=!spectator,distance=0] xpDmgSubis /= #joueur temp
scoreboard players operation #joueur defenseXp += @p[gamemode=!spectator,distance=0] xpDmgSubis
scoreboard players reset @p[gamemode=!spectator,distance=0] xpDmgSubis
# - Dommages bloqués (=> +5 DEF à chaque coeur de dommages bloqués)
scoreboard players set #joueur temp 4
scoreboard players operation @p[gamemode=!spectator,distance=0] xpDmgBloques /= #joueur temp
scoreboard players operation #joueur defenseXp += @p[gamemode=!spectator,distance=0] xpDmgBloques
scoreboard players reset @p[gamemode=!spectator,distance=0] xpDmgBloques
# Les compétences Force :
# - Dommages faits (=> +1 FOR à chaque coeur de dommages infligés)
scoreboard players set #joueur temp 20
scoreboard players operation @p[gamemode=!spectator,distance=0] xpDmgFaits /= #joueur temp
scoreboard players operation #joueur forceXp += @p[gamemode=!spectator,distance=0] xpDmgFaits
scoreboard players reset @p[gamemode=!spectator,distance=0] xpDmgFaits
# - Mobs tués (=> +10 FOR à chaque mob tué)
scoreboard players set #joueur temp 10
scoreboard players operation @p[gamemode=!spectator,distance=0] xpMobsTues *= #joueur temp
scoreboard players operation #joueur forceXp += @p[gamemode=!spectator,distance=0] xpMobsTues
scoreboard players reset @p[gamemode=!spectator,distance=0] xpMobsTues
# - Joueurs tués (=> +20 FOR à chaque joueur tué)
scoreboard players set #joueur temp 20
scoreboard players operation @p[gamemode=!spectator,distance=0] xpJoueursTues *= #joueur temp
scoreboard players operation #joueur forceXp += @p[gamemode=!spectator,distance=0] xpJoueursTues
scoreboard players reset @p[gamemode=!spectator,distance=0] xpJoueursTues
# On ajoute l'XP calculée au joueur
function tdh:player/xp/agilite/gain_xp
function tdh:player/xp/defense/gain_xp
function tdh:player/xp/eloquence/gain_xp
function tdh:player/xp/force/gain_xp

# Pour chaque compétence, on vérifie si on a atteint le palier d'XP
function tdh:player/xp/agilite/calcul_xp
function tdh:player/xp/defense/calcul_xp
function tdh:player/xp/eloquence/calcul_xp
function tdh:player/xp/force/calcul_xp

# Si on a gagné un niveau, on l'affiche
execute if data storage tdh:joueur level_up run function tdh:player/xp/maj_niveau/titre

# On réinitialise le storage
data remove storage tdh:joueur level_up
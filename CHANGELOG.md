## [Version de développement - Augmentation de la distance max des modèles de signaux]	(Current)	- 2024-05-13
### Modifications :
 - Les modèles de signaux sont désormais modifiés jusqu’à 60 blocs de distance au lieu de 50 (`tch:signal/tick/orange`, `/tick/rouge` et `/reserver/signal`).

## [Version de développement - Réparation de la détection Station ouverte des valideurs]	(4b40d81f)	- 2024-05-12
### Réparations :
 - La détection de station ouverte des valideurs Gold et Silver (`tch:station/valideur/gold/test_station` et `/silver/test_station`) fonctionne désormais correctement lorsque l’armor stand `NomStation` a le tag `{Marker:1b}` (cela ne fonctionnait pas lorsque la distance était égale à 0, on cherche désormais une distance inférieure à 0.1).

## [Version de développement - Ajout des signaux au hgive (#302)]	(f10ab494)	- 2024-05-08
### Ajouts :
 - Il est désormais possible d’obtenir les modèles de signaux depuis **hgive** (`tch:give/give_main` et `tch:give/signal/main`). (#302)

## [Version de développement - Prise en compte des modèles des signaux (#302)]	(2fa24106)	- 2024-05-07
### Modifications :
 - Le code des signaux (`tch:signal/`) modifie désormais le modèle affiché par les item_frames `SignalModel` lorsqu’un signal change d’état. Les fonctions de changement de modèle (qui modifient aussi le niveau de lumière local) sont situées dans `tch:signal/modele/`, et appelées par `tch:signal/tick/orange`, `/tick/rouge` et `/reserver/signal`.

## [Version de développement - Les ascenseurs 2 × 3 et 2 × 2 posent bien leur câble (#319)]	(abea326a)	- 2024-05-07
### Réparations :
 - Les ascenseurs 2 × 2 et 2 × 3 posent désormais bien un câble dans la fonction `tdh:ascenseur/mouvement/tick/descente`. (#319)

## [Version de développement - Incidents des distributeurs]	(88d3fcba)	- 2024-05-07
### Ajouts :
 - Un système d’incident a été ajouté au code du distributeur TCH (`tch:station/distributeur/`). La probabilité qu’un distributeur dysfonctionne dépend des `chancesIncident` définies par le configurateur de station (`tch:station/config/`), et comme pour les valideurs un distributeur hors-service sera réparé le lendemain à 3 h 30 du matin.

## [Version de développement - Affichage des infos sur les badges du personnel TCH]	(76724683)	- 2024-05-06
### Ajouts :
 - Un système d’affichage des badges actifs / désactivés est désormais disponible dans `tch:personnel/`, sur le même modèle que les fonctions équivalentes de `tch:abonnement/`. Il est possible d’afficher les badges actifs avec `tch:personnel/afficher/_actifs` et les badges désactivés avec `tch:personnel/afficher/_archive`.
 - Les fonctions permettant de se déplacer dans l’archive des badges du personnel TCH (`tch:personnel/archive/_precedent` et `_suivant`) ont été ajoutées.
### Modifications :
 - Les fonctions d’affichage des abonnements actifs / archivés (dans `tch:abonnement/afficher/`) fonctionnent désormais correctement s’il y a 0 abonnement actif / archivé.

## [Version de développement - Simplification du hgive station]	(b6ba011d)	- 2024-05-06
### Modifications :
 - Le hgive station (`tch:give/station/`) utilise désormais les données du système d’itinéraire pour détecter la station la plus proche, ainsi que générer la liste des stations. *Pour cette raison, il n’est plus possible d’obtenir le panneau nom_station de son choix en un clic depuis la liste ; il faut une première commande suggérée à remplir soi-même avec l’ID station (indiqué dans la liste), puis cliquer sur une seconde commande.*
### Suppressions :
 - Toutes les stations hardcodées dans `tch:give/station/` ont été supprimées (par exemple `tch:give/station/alwin_onyx`).

## [Version de développement - Réparation du configurateur de ligne TCH]	(6037991f)	- 2024-05-06
### Modifications :
 - Les fonctions de `tch:ligne/config/` utilisent désormais la fonction `tch:tag/pc` pour récupérer leur PC.
 - La fonction de **tdh-core** `tdh:config/calcul/hhmm_to_ticks` utilise désormais des sous-fonctions `/negative_init` et `/negative_end` pour accepter et calculer correctement les inputs négatifs (par exemple -230 pour - 2 h 30).
### Réparations :
 - Les fonctions de `tch:ligne/config/` attendent désormais des inputs temporels sous la forme **hhmm** (par exemple 1645 pour 16 h 45) plutôt que directement en ticks, pour permettre une configuration harmonieuse quelles que soient les durées d’une journée ou d’une heure.

## [Version de développement - Sélecteur d’horaire générique dans le configurateur TDH]	(6278d1cb)	- 2024-05-06
### Modifications :
 - Le code permettant de choisir un horaire est désormais générique (il a été extrait de `tch:station/config/` du pack **tdh-tch** et se trouve désormais dans `tdh:config/select/heure` et ses sous-fonctions, du pack **tdh-core**). Pour l’utiliser, il faut simplement définir un horaire minimal et maximal (avec `#configHeure min` et `max`) et appeler la fonction `tdh:config/select/heure`.
 - Le code permettant de calculer un tick exact à partir d’un horaire au format HHMM *(par exemple 1645 pour 16 h 45)* fait désormais partie de **tdh-core**, dans la fonction `tdh:config/calcul/hhmm_to_ticks`. Cette fonction attend un paramètre `@s config` et renvoie le résultat dans `#config temp`.

## [Version de développement - Ajout de l’ID de la station Morgenne]	(6f47e7d6)	- 2024-05-06
### Modifications :
 - La station **Morgenne** existe désormais dans l’index `tch:station/get_id`.

## [Version de développement - Réparation de l’affichage des horaires d’ouverture / fermeture des lignes]	(28f0f4b4)	- 2024-05-05
### Réparations :
 - Les fonctions `tch:ligne/config/debut_service_display` et `fin_service_display` affichent désormais bien les horaires d’ouverture / fermeture des lignes lorsqu’elles sont appelées par les fonctions de `tch:station/config/`.

## [Version de développement - Réparation du configurateur de station TCH]	(f3906a59)	- 2024-05-05
### Modifications :
 - Diverses commandes du configurateur de station TCH ont été déplacées dans leurs propres sous-fonctions pour éviter les répétitions de code (`tch:station/config/liste_heure_fermeture_select`, `liste_heure_ouverture_select` et `calcul_heure`).
 - Le configurateur de station TCH ne restreint plus les horaires d’ouverture / fermeture autorisés.
 - Les tellraw de `tch:station/config/` utilisant le paramètre `"interpret"="true"` l’ont remplacé par `"interpret"=true`, qui a le même effet mais fonctionnera dans les versions récentes de Minecraft (alors que `"true"` ne sera plus reconnu).
### Réparations :
 - Le configurateur de station TCH prend désormais correctement en compte les réglages de durée du jour et de nombre d’heures par jour / minutes par heure définis dans **tdh-time**.

## [Version de développement - Simplification de la syntaxe de l’affichage des lieux et sujets]	(13894222)	- 2024-05-05
### Modifications :
 - L’affichage des **lieux** et **sujets débloqués** a été largement simplifié.

## [Version de développement - Changement de la syntaxe du paramètre `interpret` pour les dialogues]	(bfa56fb7)	- 2024-05-05
### Modifications :
 - Toutes les lignes de dialogue et d’affichage utilisant le paramètre `"interpret"="true"` l’ont remplacé par `"interpret"=true`, qui a le même effet mais fonctionnera dans les versions récentes de Minecraft (alors que `"true"` ne sera plus reconnu).

## [Version de développement - Optimisation de l’affichage des dialogues]	(868e409c)	- 2024-05-05
### Modifications :
 - Le tag `NotreJoueur` est désormais donné et retiré au joueur dans la fonction principale de l’affichage des dialogues (`tdh:dialogue/affichage`) plutôt que de manière répétée dans les diverses sous-fonctions.
 - Dans la même fonction, une commande `at @p` a été remplacée par `if entity @p` puisqu’on est déjà à la position du joueur. Dans les sous-fonctions, plusieurs `at` (ou `as @s`) ont été remplacés par `positioned as` puisqu’on se fichait de la rotation et qu’on avait besoin de la position.
 - L’affichage des options (dans `tdh:dialogue/affichage/options`) a été massivement simplifié grâce à la découverte d’une syntaxe permettant d’afficher plusieurs éléments d’un tableau en une seule commande.

## [Version de développement - Ralentissement de la réparation des dialogues]	(81b32483)	- 2024-05-05
### Modifications :
 - À des fins d’optimisation, la logique de la « réparation » des dialogues des villageois (qui se trouvait jusqu’alors dans `tdh:dialogue/tick`) a été déplacée dans `tdh:dialogue/reparer_dialogues`, et est désormais exécutée une fois toutes les 2 secondes (au lieu de 8 fois par seconde précédemment).

## [Version de développement - Désactivation des passagers RER]	(16a808b7)	- 2024-03-27
### Modifications :
 - Pour des raisons de performance, les RER ne spawnent pour l’instant plus de passagers (le code qui les fait spawner est toujours présent, on a simplement défini la probabilité de spawn à 0 dans les fonctions `tch:rer/spawner/spawn/voiture/passagers/test_assis` et `test_debout).

## [Version de développement - Réparation de la déréservation PID]	(03ffb542)	- 2024-03-25
### Réparations :
 - La déréservation des PID (déverrouillage du SACD lui permettant d’afficher autre chose que **00**) a été réparée. Depuis quelques commits elle ne se faisait plus *hormis* lors d’un terminus avec retournement, ce qui n’était pas très pratique.

## [Version de développement - Réparation d’un sélecteur du fix de la rotation des RER]	(6ccc28a4)	- 2024-03-25
### Réparations :
 - Un sélecteur avec une distance un peu trop faible (dans `tch:rer/rame/arret`) empêchait la réparation de la rotation des voitures autres que la voiture de tête.

## [Version de développement - Réparation d’un sélecteur de l’arrivée en gare des RER]	(684f790a)	- 2024-03-25
### Réparations :
 - Un sélecteur avec une distance un peu trop faible empêchait le joueur de revenir à sa position d’origine lors de l’arrivée d’un RER en station (dans la fonction `tch:rer/rame/en_station/arrivee/retirer_joueur`).

## [Version de développement - Optimisation des sélecteurs du RER]	(27bd679f)	- 2024-03-25
### Modifications :
 - De très nombreux sélecteurs `@e` du code du RER (`tch:rer/`) incluent désormais un check `distance=..X`, car il se trouve que bien que ça rajoute un check supplémentaire, il est vérifié **après** les tags du sélecteur mais filtre la liste d’entités **avant** (uniquement les entités des types demandés étant dans un des chunks dans le rayon de cette distance sont sélectionnées). Les premiers tests de performance semblent très concluants (le tick du RER est passé de 9,1 % à 4,1 % du maximum dans un environnement de test, de 17 h à 01 h à Sablons–Brimiel).
 - La fonction `tch:rer/rame/detruire` dispose désormais d’une sous-fonction exécutée en tant que la voiture de tête de l’ID recherché, pour limiter le nombre de sélecteurs `@e`.
### Réparations :
 - Un sélecteur incorrect (`@e` au lieu de `@a`) a été corrigé dans `tch:rer/spawner/spawn/message_quai`.

## [Version de développement - Optimisation du mouvement du RER]	(6b139e41)	- 2024-03-25
### Modifications :
 - Le mouvement des RER est désormais géré voiture par voiture, ce qui permet de tagger tous les passagers, caméras et autres entités correspondant à notre voiture *sans* avoir à checker en plus leur score `idVoiture` à chaque fois.
   Le tag `OurPassager` n’est plus donné dans `tch:rer/rame/mouvement` mais dans `tch:rer/rame/mouvement/voiture`, qui est exécutée pour chaque voiture, encapsule les fonctions de mouvement (`tch:rer/rame/mouvement/1_16`, `17_32`…) et définit en plus le tag `OurCamera`.
 - Les fonctions de mouvement proprement dites (`tch:rer/rame/mouvement/avancer/1` à `32`) sont bien plus compactes vu qu’on peut simplement utiliser les tags `OurPassager` et `OurCamera` pour appliquer le même mouvement à l’ensemble de notre voiture. C’est en particulier avantageux pour la partie déplaçant les passagers, car auparavant on détectait l’entité OurRER la plus proche de **chaque passager** pour s’aligner dessus !!
 - La fonction de retournement hors station est la seule à ne plus utiliser le tag `OurPassager` et à repasser à un check via score. Mais comme elle ne s’exécute qu’une fois et pas à chaque tick, je pense qu’on y gagne au change. (`tch:rer/rame/mouvement/retournement`)
 - Une partie des checks d’ID line / ID line max ont été déplacés de `tch:rer/rame/mouvement/voiture/aligner` à `/aligner/sauvegarder_id_line`, principalement pour des raisons de lisibilité de la première fonction.
 - La fonction `tch:rer/rame/mouvement/voiture/aligner_precedente` doit maintenant supprimer les tags `OurPassager` et `OurCamera` et les réappliquer aux nouvelles entités correspondantes puisqu’elles sont désormais uniques à chaque voiture.
### Réparations :
 - La variable `#rer idVehicule` était définie deux fois par tick pour chaque RER, la seconde définition (dans `tch:rer/rame/mouvement`) a donc été retirée car il y en avait déjà une dans `tch:rer/rame/tick_rame`.
### Suppressions :
 - Les fonctions de mouvement spécifiques aux caméras (`tch:rer/rame/mouvement/camera/1` à `32`) ont été retirées car elles ont pu être intégrées directement aux fonctions de mouvement génériques `tch:rer/rame/mouvement/avancer/1` à `32`.

## [Version de développement - Divers bugfixes et optimisations des ascenseurs]	(1ff4e036)	- 2024-03-25
### Modifications :
 - Chaque cabine d’ascenseur, dans le `tdh:ascenseur/mouvement/tick`, exécute désormais la fonction `tdh:ascenseur/mouvement/tick_cabine` pour limiter le nombre de sélecteurs utilisés dans `tick`.
### Réparations :
 - L’ascenseur ne peut plus déplacer n’importe quel type d’entité, mais uniquement une liste prédéfinie (dans le tag `#tdh:ascenseur_deplacable`) incluant les entités des tags `#tdh:hostile`, `#tdh:neutre` et `#tdh:amical`, ainsi que les items et les flèches. Cela n’inclut PLUS les armor stands (elle se déplace elle-même, mais pas les autres).
 - Un ascenseur `RetourAuto` en cours de retour n’enregistre plus le tag `Appel` générique, mais `RetourEnCours`. Cela lui permet d’éviter d’ouvrir les portes ou de se mettre en pause en arrivant à l’étage (en utilisant un check dans la fonction `tdh:ascenseur/mouvement/tick/test_etage` pour déclencher `/tick/etage_retour` au lieu de `/tick/etage`). Cela permet également d’enregistrer un appel pour un ascenseur en cours de retour (il utilisera, dans ce cas, la fonction `etage` au lieu de `etage_retour` et ouvrira correctement les portes).
 - Le volume de déplacement / détection des entités passagères de l’ascenseur a été réduit, pour tenir compte du bug [MC-123441](https://bugs.mojang.com/browse/MC-123441).

## [Version de développement - Réactivation des annonces sonores PID pour les RER]	(479502b7)	- 2024-03-24
### Modifications :
 - Les annonces PID ont été réactivées pour les RER (`tch:station/annonce/pid/gen_id`).
### Réparations :
 - Les annonces PID prennent désormais en compte l’ID de prochaine ligne (`tch:station/annonce/pid/diffuser`, `diffuser/metro/dogo/station_from_line_u` et `/metro/info/station_from_line_u`).

## [Version de développement - Le retournement en station ne bloque plus le PID à 00]	(6d2fedef)	- 2024-03-24
### Réparations :
 - Le retournement des trains en station déréserve également le 00 du PID pour éviter qu’il ne reste bloqué ensuite. *(Idéalement on réglera mieux ce souci lorsqu’on aura réécrit le code du PID)*
 - Le retournement des trains en station se fait désormais 5 ticks avant le départ plutôt que 4.

## [Version de développement - Bugfix des retournements en station pour MC-103800]	(5da9e190)	- 2024-03-24
### Réparations :
 - Le retournement des trains en station se fait désormais 4 ticks avant le départ pour éviter de déclencher le bug [MC-103800](https://bugs.mojang.com/browse/MC-103800).

## [Version de développement - Rotations plus faibles pour le bugfix MC-103800]	(617b5cee)	- 2024-03-24
### Modifications :
 - La rotation temporaire appliquée aux RER en station pour « corriger » leur rotation occasionnellement incorrecte due au bug [MC-103800](https://bugs.mojang.com/browse/MC-103800) est désormais de 2° plutôt que 5° précédemment. J’ai testé avec 1° et ça ne fonctionnait malheureusement plus, 2° me semble être le minimum. (`tch:rer/rame/arret/reparer_rotation2` et `reparer_rotation3`).
### Réparations :
 - Une astérisque surnuméraire a été retirée de ce `CHANGELOG.md` dans les détails du commit fixant originellement ce bug (c35d1d3b).

## [Version de développement - Simplification du calcul du prochain jour du spawner RER]	(79e9569f)	- 2024-03-24
### Modifications :
 - Le calcul du prochain jour dans le code du spawner RER (`tch:rer/spawner/ajuster_multiples` et `/spawner/spawn/decaler_horaire`) utilise désormais une seule condition plutôt que deux.

## [Version de développement - Extension des SACD (#306)]	(d825765a)	- 2024-03-24
### Modifications :
 - Le temps d’attente maximal affichable par un SACD est désormais de **59** minutes (contre **99** auparavant). Ensuite, entre **60** et **120** minutes il affiche **60**/**++** alternativement, et au-delà il affiche **XX** (fin de service). (`tch:station/pid/update`)
 - Les messages sur le quai pour informer qu’un train arrive dans X minutes ne sont désormais affichés que jusqu’à 60 minutes (contre 90 auparavant).
### Réparations :
 - Le calcul des minutes d’attente (`tch:station/pid/update/calcul_minutes`) prend désormais en compte l’ID de jour du spawner plutôt que de se baser sur son propre temps d’attente.

## [Version de développement - Impression ticket par ticket pour les distributeurs (#351)]	(b904862a)	- 2024-03-24
### Modifications :
 - Les distributeurs n’impriment plus tous les tickets instantanément, mais l’un après l’autre (assez rapidement pour l’instant mais ça pourra être réduit au besoin) : pour ce faire ils invoquent une entité temporaire (un marker) avec le tag `DistributeurImpression` et bloquent les interactions jusqu’à la fin de l’impression en se donnant le tag `ImpressionEnCours`. (#351)
 - Une bonne partie de la logique autour de l’impression des tickets (jingle de succès, message tellraw…) est désormais « générique » et calculé dans une fonction `tch:station/distributeur/ticket/debut_impression` plutôt que dans `fer`, `or` et `diamant` du même dossier.
### Réparations :
 - L’accueil du distributeur affichait incorrectement 1 diamant = 10 tickets (alors que c’est 20).

## [Version de développement - Lecteurs de badge TCH]	(66b16a2e)	- 2024-03-24
### Ajouts :
 - Des lecteurs de badge ont été ajoutés pour les zones d’accès restreint du réseau TCH. Il suffit de coller une item frame contre une porte et de lui donner, presque comme pour un valideur Gold, les tags `ValideurBadge` et `Nord`, `Sud`, `Est` ou `Ouest`, ainsi qu’un score `niveauAcces` correspondant à la restriction d’accès désirée (plus le nombre est élevé, plus l’accès est restreint).
   Ils fonctionnent avec la même logique que le valideur Gold, et sont donc branchés dans `tch:station/tick` (avec toute la logique dans les sous-fonctions de `tch:station/valideur/badge/`).
 - Une base de données du personnel de TCH a été créée sur la même logique que le fichier des abonnements (avec les mêmes fonctions du genre `_find` pour faire une recherche et `nouveau` ou `add_generated_id` pour ajouter des entrées à la base de données). Elle se trouve dans le dossier `tch:personnel/`.
   La fonction `tch:personnel/_donner_badge` permet d’obtenir un badge ayant l’ID `#tchPersonnel idAbonnement` (variable à définir au préalable).
### Modifications :
 - L’initialisation de TCH `tch:init` définit désormais la variable `niveauAcces`.

## [Version de développement - Les entités écrasées ou électrocutées ne droppent plus de loot]	(4104d161)	- 2024-03-23
### Modifications :
 - Les entités écrasées (`tch:rer/rame/mouvement/ecraser_entite`), électrocutées (`tch:metro/rail/electrocution_entite`) ou simplement supprimées par les différents nettoyages d’entités du système RER (`tch:rer/rame/detruire/detruire_entite` et `tch:rer/rame/en_station/arrivee/supprimer_mob`) appellent désormais `tdh:kill_lootless` au lieu de se contenter d’un simple `minecraft:kill`, et ne droppent donc plus d’objets lors de leur mort.

## [Version de développement - Kill d’entités sans laisser de loot]	(b3f53b70)	- 2024-03-23
### Ajouts :
 - Dans **tdh-core**, la fonction `tdh:kill_lootless` permet de tuer une entité sans faire apparaître de loot (plus précisément, on détruit en même temps tous les items très proches de l’entité tuée étant apparus au même tick).

## [Version de développement - Réparation de la détection des RER dans le même bloc signalisé que nous]	(f5b69111)	- 2024-03-23
### Réparations :
 - Les RER ne se basaient que sur l’état du *prochain* signal pour redémarrer lorsqu’ils avaient rencontré un signal rouge. Ils vérifient désormais également si un RER est déjà présent dans le même bloc signalisé qu’eux (dans `tch:signal/tick_vehicule`).

## [Version de développement - Assignation correcte de prochaineLigne à toutes les voitures des RER]	(6d0ba738)	- 2024-03-23
### Réparations :
 - Les armor stands `ProchaineLigne` n’assignaient la variable `prochaineLigne` qu’aux voitures de tête, ce qui causait des bivoies au niveau des aiguillages lorsqu’ils filtraient par `prochaineLigne`. On assigne désormais cette variable à toutes les armor stands du RER dans `tch:rer/rame/mouvement/voiture/aligner/sauvegarder_tags`.

## [Version de développement - Ajustement du volume d’écrasement des RER]	(88d7324f)	- 2024-03-23
### Réparations :
 - Le volume d’écrasement des RER a été ajusté en fonction du bug [MC-123441](https://bugs.mojang.com/browse/MC-123441) dans la fonction `tch:rer/rame/mouvement/ecraser_entites`. La fonction est également appelée par **chaque** voiture plutôt que seulement la voiture de tête, pour éviter des soucis si les voitures ont différentes rotations.

## [Version de développement - Poussage des entités lors de la fermeture des portes]	(da79548f)	- 2024-03-23
### Ajouts :
 - Un système permet de « pousser » les joueurs et voyageurs vers l’intérieur ou l’extérieur de la voiture s’ils sont trop proches de la porte au moment de la fermeture de celle-ci. Il se trouve dans `tch:rer/rame/en_station/fermeture_portes/pousser_entites_x` et `pousser_entites_z`, et est déclenché par les voitures de RER dans `tch:rer/rame/en_station/fermeture_portes/test_entite`.

## [Version de développement - Sons d’arrivée et de mouvement extérieur du RER]	(0b3cbb82)	- 2024-03-23
### Ajouts :
 - Les sons d’arrivée du RER de la dernière version du RP sont désormais joués (par la fonction `tch:sound/rer/vitesse` pour la première partie, pré-arrêt, qui est diffusée lorsque le RER a pour vitesse max 1 et une vitesse inférieure ou égale à 8 ; et par la fonction `tch:rer/rame/en_station/arrivee` pour la partie post-arrêt).
 - Les sons de mouvement du RER sont désormais également diffusés en version mono pour les personnes hors du RER en question (dans `tch:sound/rer/vitesse` et ses sous-fonctions).
### Réparations :
 - Les sons de standby du RER arrêté (dans `tch:sound/rer/station/`) sont désormais bien les sons `tch:br.rer.outside.` plutôt qu’`inside` car ils ont été renommés.

## [Version de développement - Les caméras forcées définissent correctement un ID de voiture]	(665ba50f)	- 2024-03-23
### Réparations :
 - Jusqu’à présent, les caméras forcées ne définissaient pas d’ID de voiture, et conservaient donc la valeur précédente, ce qui dans certains cas pouvait envoyer la caméra dans un mur (cela dépendait de la caméra précédemment utilisée et était donc assez aléatoire). Désormais lorsqu’on force une caméra, on définit l’ID de voiture de cette caméra à celui de la voiture la plus proche d’elle. (`tch:rer/rame/mouvement/voiture/aligner/forcer_camera/teleporter`)

## [Version de développement - Remplacement du son d’écrasement]	(657cfa1a)	- 2024-03-23
### Modifications :
 - Le son d’écrasement `tch:br.rer.collision` est désormais joué à la place de `tdh:br.electrocution` dans la fonction `tch:rer/rame/mouvement/ecrasement_particules`.

## [Version de développement - Début de sonorisation des RER à l’arrêt]	(b4f4e127)	- 2024-03-23
### Ajouts :
 - Les RER à l’arrêt sont désormais sonorisés par la fonction `tch:sound/rer/station` (elle-même déclenchée par `tch:sound/rer/vitesse_tick`).

## [Version de développement - Début de sonorisation des RER en mouvement]	(557d22fd)	- 2024-03-23
### Ajouts :
 - Les RER en mouvement sont désormais sonorisés (sans prise en compte pour l’instant de contextes spécifiques comme l’arrêt/départ de station, à l’exception du son `signal_depart` qui est déjà joué depuis belle lurette). L’émission des sons est faite toutes les 2 secondes par la fonction `tch:sound/rer/vitesse_tick`, qui appelle `vitesse` du même dossier (puis les fonctions du sous-dossier `vitesse/`).

## [Version de développement - Infos de debug des destinations des passagers]	(23d60d97)	- 2024-03-23
### Ajouts :
 - Le statut et le score `idMarqueur` des item frames `tchPassagerDest` peuvent désormais être affichés au-dessus d’eux (comme pour les infos des tracés de RER) par les fonctions de `tch:station/passager/debug/`. Le système est actif si le score `#passagerDestDebug on` vaut 1, et peut être activé / désactivé avec `tch:station/passager/debug/on` et `off`.

## [Version de développement - Suppression de la limite de distance des PID]	(6e69b528)	- 2024-03-21
### Réparations :
 - Les PID sont désormais update même s’ils sont à plus de 150 blocs de leur spawner (la condition datait de l’époque où les PID n’enregistraient pas d’ID station et devaient se baser sur la proximité).

## [Version de développement - Fix temporaire du bug des rotations RER MC-103800]	(c35d1d3b)	- 2024-03-21
### Ajouts :
 - Un système permettant de « réparer » la rotation des RER a été mis en place. Ce souci existe à cause du bug [MC-103800](https://bugs.mojang.com/browse/MC-103800), et le fix est très imparfait mais améliore déjà la situation. Le fix n’est mis en place qu’au moment de l’arrêt d’un RER donc des rotations incorrectes pendant les trajets peuvent toujours se produire, mais au moins les modèles seront toujours alignés lorsqu’ils sont à quai. (`tch:rer/rame/arret` et `arret/reparer_rotation`).

## [Version de développement - Angles de caméra fixes et forcés pour le RER (#318)]	(bb559554)	- 2024-03-21
### Ajouts :
 - Il est désormais possible de forcer l’utilisation de certains angles de caméra pour le RER. Il suffit pour ce faire de donner le tag `ForceCamera` à une des entités de son tracé, ainsi qu’une variable `idCamera`. Le ou les angles de caméra forcés doivent de leur côté être des `armor_stand`, avoir le tag `RERCameraPosition` et le même `idCamera`. *(`tch:rer/rame/camera/`, `tch:rer/rame/mouvement/voiture/aligner/copier_tags` et `sauvegarder_tags`)*
 - Il est possible de faire en sorte que des positions forcées de caméras soient **fixes**. Pour cela, il faut simplement donner à l’entité `RERCameraPosition` le tag `FixedCamera`, et elle restera en place tout en suivant le mouvement de la voiture de tête (elle peut également suivre celui de la voiture de queue si on lui adjoint le tag `RegarderQueue`). *(`tch:rer/rame/camera/test_fixe` et `fin_fixe`, `recentrer` et `recentrer_queue`, ainsi que `tch:rer/rame/passager/tick_camera` ; mais cela a également nécessité la modification des 32 fonctions `tch:rer/rame/mouvement/avancer/1` à `32` pour éviter que les caméras fixes n’avancent avec le RER)*
 - Quatre nouveaux angles de caméra par défaut ont été ajoutés à `tch:rer/rame/camera/update` : `tch:rer/rame/camera/update/colle_arriere_droite`, `colle_arriere_gauche`, `interieur_cockpit` et `pantographe_avant`.
### Modifications :
 - Plusieurs angles de caméra par défaut ont été modifiés pour les rendre plus naturels : il s’agit de `tch:rer/rame/camera/update/americain_droite`, `americain_gauche`, `cote_droit`, `cote_gauche`, `face`, `interieur_queue` et `interieur_tete`. (#318)
 - Les tags `ForceCamera`, `ClearCamera` et `RegarderQueue` ont été ajoutés à la visualisation des infos de debug des tracés du RER. (`tch:rer/trace/debug/`)

## [Version de développement - Les RER peuvent spawner en pente]	(4b0b3cc2)	- 2024-03-20
### Réparations :
 - Les RER ne réinitialisent plus leur propre rotation verticale à 0° lors du spawn (dans `tch:rer/spawner/spawn/test_signal`). Ils peuvent donc désormais spawner en pente sans provoquer de glitch visuel.

## [Version de développement - Les signaux nouvellement créés affichent bien une particule de debug]	(4d7ef638)	- 2024-03-20
### Réparations :
 - Les signaux n’ayant pas encore de `status` affichent désormais correctement une particule verte plutôt qu’aucune particule (dans `tch:signal/debug/tick`).

## [Version de développement - Moins de flood dans SignalDebugLog]	(d9ac1b3e)	- 2024-03-20
### Modifications :
 - Les messages qui informent qu’un signal n’a PAS changé d’état et qu’il est resté rouge / orange ont été déplacés de `SignalDebugLog` à `SignalDetailLog` pour éviter le flood intempestif.

## [Version de développement - Davantage d’infos de debug pour les signaux]	(7d966614)	- 2024-03-20
### Ajouts :
 - Ajout d’une visualisation sous forme de particules de l’état actuel des signaux (invoque de la `dust` rouge, orange ou verte à l’emplacement de chacune des entités `Signal` proches). Le code se trouve dans `tch:signal/debug/tick_test` et `tick`, et est exécuté toutes les secondes (`#tdh:tick/1`).
### Modifications :
 - Il y a désormais deux versions de la réservation de signal `tch:signal/reserver` ; `tch:signal/reserver/actuel` qui ne change l’état que du signal actuel (et des autres signaux partageant son ID) et ne permet de filtrer que selon leur statut, et `tch:signal/reserver/complet` qui change l’état du signal actuel et de tous les signaux précédents qui l’ont comme signal suivant (et permet de filtrer selon le statut des signaux précédents, actuels et suivants). Cette réservation de signal donne désormais un tag `ReservationReussie` à l’entité qui l’appelle (il doit être retiré par la fonction appelante). *(modifie `tch:rer/rame/mouvement/voiture/aligner/copier_signal` et `tch:rer/spawner/spawn/test_signal`)*
 - Les infos de debug des tracés du RER affichent désormais la `prochaineLigne` d’un tracé conditionnel, en plus de l’`idLine` / `idLineMax`.
 - Un signal dont on a déjà vérifié s’il pouvait changer de statut à ce tick reçoit désormais un tag `SignalCalcule` qui lui évite d’être à nouveau vérifié à ce tick. Ce tag est ensuite retiré dans `tch:signal/tick`.
 - De nombreux nouveaux textes de debug sont affichés aux joueurs ayant le tag `SignalDetailLog`.
 - La fonction `tch:signal/tick/test_vehicule` a été séparée en deux parties, une pour le signal actuel (`tch:signal/tick/test_signal`) et une autre pour le prochain signal (`tch:signal/tick/test_prochain_signal`).
### Réparations :
 - L’horaire de prochain spawn des spawners RER se réinitialise désormais correctement (dans `tch:rer/spawner/spawn/test_signal`) lorsqu’un spawn a réussi.
 - Les infos de debug des tracés du RER affichent désormais bien l’`idStation` des entités `ProchaineStation`, plutôt que leur `prochaineStation` (qui n’était jamais présente dans leurs variables). *(`tch:rer/trace/debug/tick/prochaine_station` et `prochaine_station_texte`)*
 - Les fonctions `tch:signal/tick/orange` et `rouge` ont été séparées en plusieurs sous-fonctions (ajout de `orange2`, `orange3` et `rouge2`) qui doivent être exécutées par des ensembles d’entités. Cela a pour but de vérifier qu’on sélectionne bien TOUS les signaux de cet ID au moment de les passer au rouge.

## [Version de développement - Second bugfix de l’affichage des infos de debug des tracés de RER]	(ca903405)	- 2024-03-18
### Réparations :
 - Le bugfix précédent était ENCORE buggé car je suis stupide. Cordialement.

## [Version de développement - Bugfix de l’affichage des infos de debug des tracés de RER]	(ed2e0b2b)	- 2024-03-18
### Réparations :
 - Des tags incorrectement retirés empêchaient les infos de debug des tracés de RER de s’afficher correctement.

## [Version de développement - Item frames TexteTemporaire multiples]	(1225d7b3)	- 2024-03-18
### Modifications :
 - Il est désormais possible pour plusieurs item frames `TexteTemporaire` de coexister, et la sélection entre les différentes entités par une fonction se fait de manière aléatoire.

## [Version de développement - Ajout des signaux (#302)]	(04b0775c)	- 2024-03-18
### Ajouts :
 - Les signaux ont été ajoutés au pack **tdh-tch**. Ils ont été pensés pour être compatibles avec métro, RER et câble, mais ne prennent pour l’instant en compte que le RER (les connexions avec les autres parties du code devraient arriver rapidement) :
   - Un signal peut être une `armor_stand` (`TraceCable` ou `TraceRER`), ou bien un `marker` (qui sera utilisé pour les métros).
   - Chaque signal doit enregistrer un ID pour lui-même (`idSignal`), et un autre pour le prochain (`prochainSignal`).
   - Chaque signal enregistre une variable `status` qui vaut soit `0` (vert), soit `1` (orange), soit `2` (rouge). Vert signifie que la voie est libre ; orange signifie que le bloc suivant est rouge et que le train doit limiter sa vitesse ; rouge signifie que le bloc suivant est occupé et qu’il faut effectuer un freinage d’urgence.
   - Les RER qui rencontrent un `TraceRER` étant également un `Signal` vérifient l’état de ce signal avant de continuer, et limitent leur vitesse ou s’arrêtent en cas de besoin.
   - Un signal repasse automatiquement à l’orange quand le RER qui l’a déclenché se situe 2 signaux plus loin. Le signal repasse automatiquement au vert s’il se situe 3 signaux plus loin (ou que le RER a déspawné).
   La majorité du code relatif à ces signaux se trouve dans le dossier `tch:signal/`, mais certaines parties de la détection et du traitement des signaux RER se trouvent également dans `tch:rer/` (`rame/mouvement/voiture/aligner/copier_tags`, `sauvegarder_tags`, `copier_signal` ; `spawner/spawn/spawn`, `test_ligne`, `test_voie` et également `test_signal` qui a été créé pour l’occasion et constitue la 4ème étape des checks de spawn du RER). `test_voie` ne teste d’ailleurs plus vraiment l’état de la voie (puisque c’est les signaux qui s’en occupent désormais), mais vérifie simplement qu’au moins un joueur est relativement proche du quai de ce RER.
   Les signaux actifs (oranges / rouges) sont tickés par la fonction `tch:signal/tick` (appelée toutes les 2 secondes par `#tdh:tick/2`).
 - De nouvelles fonctions de tag permettent de donner un tag :
    - au signal actuel (`tch:tag/signal`, correspondant à notre `idSignal`) : donne le tag `ourSignal`
	- au prochain signal (`tch:tag/prochain_signal`, correspondant à notre `prochainSignal`) : donne le tag `ourNextSignal`
	- aux *précédents* signaux (`tch:tag/precedents_signaux`, tous les signaux ayant notre `idSignal` comme `prochainSignal`) : donne le tag `ourPreviousSignal`
 - De nouveaux types d’entité définissent les types pouvant être des signaux (pour l’instant `marker` et `armor_stand`), ainsi que les types pouvant être des véhicules (`minecart` et `armor_stand`). (`#tch:marqueur/signal` et `#tch:vehicule`).
 - Les RER peuvent désormais effectuer un freinage / arrêt d’urgence. Ils sont déclenchés par `tch:rer/rame/mouvement/arret_urgence` lorsqu’un signal rouge est rencontré ; le RER freinera alors jusqu’à atteindre l’arrêt et se donnera le tag `ArretUrgence`. Une fois dans cet état, un RER vérifie régulièrement l’état du signal qui l’a déclenché, et se remettra en marche dès que le signal **suivant** sera vert ou orange.
 - L’arrêt et le redémarrage d’un RER se font désormais dans des fonctions dédiées (`tch:rer/rame/arret` et `depart`) plutôt que dans les fonctions d’arrivée / départ de station (`tch:rer/rame/en_station/arrivee` et `depart`).
### Modifications :
 - Un RER ralentit 2 fois plus vite s’il est en train de faire un freinage d’urgence (-1 m / s tous les 5 ticks plutôt que 10). Il peut également s’arrêter totalement si sa vitesse tombe à 0. (`tch:rer/rame/mouvement/moduler_vitesse/ralentir`)
 - Les spawners RER peuvent désormais avoir un score `vitesse` (en plus de `vitesseMax`) qui permet de donner aux RER nouvellement invoqués une vitesse de départ différente de la vitesse maximale du tronçon. (`tch:rer/spawner/spawn/spawn`)
 - Les spawners RER ne réinitialisent plus l’horaire de spawn lorsque la voie n’est pas libre, mais le décalent simplement de 5 minutes (`tch:rer/spawner/spawn/test_ligne`, `test_voie`, `test_signal`, `decaler_horaire`)
 - Les spawners des différents modes de transport donnent correctement le tag `AllowSignals` aux entités invoquées (`tch:cable/spawner/spawn/spawn`, `tch:rer/spawner/spawn/spawn` et `tch:metro/spawner/spawn/spawn`).
 - Les signaux sont pris en compte par l’affichage de debug des tracés de RER (`tch:rer/trace/debug/tick`). Plusieurs autres tags affichent désormais davantage d’informations en utilisant une item frame TexteTemporaire (`ProchaineLigne` et `ProchaineStation` affichent la variable du même nom, `Direction` et les aiguillages standard affichent leur `idLine` / `idLineMax`, tout comme les `SpawnRER` qui peuvent également afficher leur `prochaineLigne`).
### Suppressions :
 - Plusieurs appels à fonctions ont été retirés de la logique de spawn des RER (les réservations de voie et autres test_reservation_fantome), puisqu’on ne « réserve » plus de voies mais qu’on utilise le système de signaux. (`tch:rer/spawner/spawn/test_voie`)
 - La fonction `tch:rer/spawner/spawn/voiture/vitesse_spawn` a été supprimée, la définition de la vitesse est désormais faite dans la même fonction que les autres scores (`tch:rer/spawner/spawn/voiture/sauvegarder_variables`).

## [Version de développement - Modification des ID de ligne des RER]	(28681c62)	- 2024-03-17
### Réparations :
 - Quelques ID de ligne de RER oubliés dans le commit précédent sont également passés de XX**01** à XX**00** (`tch:give/siel/pid/_near` et `tch:ligne/_choix_ligne`).

## [Version de développement - Modification des ID de ligne des RER]	(b42a9dd5)	- 2024-03-17
### Réparations :
 - Les ID de ligne des RER commencent désormais à XX**00** plutôt que XX**01**, pour éviter des soucis de non-détection du PC lorsqu’on est un quai avec un ID générique (par exemple 2100-2149 pour un quai « Sud » générique du RER A). Les fonctions suivantes ont été modifiées : `tch:give/acces_quais/_near`, `/corresp/_near` et `/logo/_near` ; `tch:ligne/init` ; ainsi que l’initialisation du système d’itinéraire dans `tch:itineraire/init/lignes` et `/init/stations/connexions`.

## [Version de développement - Retournement des RER aux terminus]	(43cd9bab)	- 2024-03-17
### Ajouts :
 - Il est désormais possible pour un RER de se retourner (partir dans la direction opposée). Ce retournement peut être déclenché en station (en ajoutant le tag `Retournement` à l’armor stand `Direction`), soit hors d’une station (en ajoutant le même tag `Retournement` à une armor stand du tracé). Les fonctions correspondantes se trouvent dans `tch:rer/rame/en_station/retournement` *(déclenché dans `tch:rer/rame/en_station/depart` et `tch:rer/rame/mouvement/voiture/aligner/sauvegarder_tags`)* et `tch:rer/rame/mouvement∕retournement` *(également déclenché dans `tch:rer/rame/mouvement/voiture/aligner/sauvegarder_tags`)*.
 - Il est désormais possible pour un RER d’enregistrer un **prochain ID de ligne**, pour savoir dans quelle direction repartir une fois arrivé au terminus. Cet enregistrement se déroule au moment du spawn (dans la fonction `tch:rer/spawner/spawn/spawn` et `tch:rer/spawner/spawn/voiture/sauvegarder_variables`), ou lorsque le RER rencontre une armor stand de son tracé ayant le tag `ProchaineLigne`, et a lieu à la fin de la fonction `tch:rer/rame/mouvement/voiture/aligner/copier_tags` *(dans les sous-fonctions `/aligner/copier_ligne` et `copier_ligne_random`)*.
 - Deux nouvelles fonctions de tag permettent d’utiliser une variable prochaineLigne à la place de l’idLine usuel. Il s’agit hormis cela de copies conformes des fonctions `tch:tag/pc` et `tch:tag/terminus_pc` ; elles s’appellent `tch:tag/pc_next` et `tch:tag/terminus_pc_next` et permettent de tag le PC de ligne ou de terminus correspondant à notre **prochain ID de ligne**.
### Modifications :
 - On déréserve désormais la voie au début de la fonction `tch:rer/rame/en_station/depart`, car un éventuel retournement modifierait l’ID de ligne et empêcherait la déréservation correcte de la voie.
 - Les tracés du RER peuvent désormais filtrer les RER selon leur **prochain ID de ligne**. Cela permet, par exemple, de séparer les directions Procyon et Cyséal à la gare de Venise, alors même que tous les RER qui arrivent en gare ont le même ID de ligne : 2154, correspondant à Venise. Ce filtrage se fait dans les fonctions `tch:rer/rame/mouvement/voiture/aligner` et `/aligner/sauvegarder_id_line`.
 - Le nommage automatique des quais et spawners (`tch:station/debug/update/direction`) détecte désormais la présence d’un **prochain ID de ligne** et l’utilise en lieu et place de l’ID line pour le nommage des entités. *(Par exemple, un quai d’ID line 2153 et de prochaineLigne 2102 sera renommé correctement __Procyon__ plutôt que __Beurrieux–Valdemont__.)*
 - Le signal envoyé aux PID (`tch:station/pid/update`) détecte désormais la présence d’un **prochain ID de ligne** et l’utilise en lieu et place de l’ID line pour sélectionner les bons SACD.
 - La fonction d’affichage de debug des infos des tracés du RER (`tch:rer/trace/debug/tick`) prend en compte les nouveaux tags `Retournement` et `ProchaineLigne`.
 - La fonction `tch:tag/voie` permet désormais de filtrer selon le **prochain ID de ligne** si celui-ci est présent en plus de l’ID line.

## [Version de développement - Mise à jour des noms des terminus]	(34d5f2b3)	- 2024-03-16
### Modifications :
 - Les noms des terminus qui n’avaient qu’un nom temporaire ont été mis à jour dans `tch:auto/get_id_line`.

## [Version de développement - Mise à jour du hgive des PID/AQ du RER A]	(0779b9d7)	- 2023-04-06
### Modifications :
 - Le fichier `tch:give/siel/pid/rera` a été modifié pour permettre d'obtenir les nouveaux PID combinant 2 directions. Les sous-fonctions de `/rera/` ont également été renommées du mode textuel (`/rera/cyseal`) au mode numérique (`/rera/2101`), et celles qui n'existaient pas ont été ajoutées.
 - Le fichier `tch:give/acces_quais/rera` a été modifié pour permettre d'obtenir l'intégralité des nouveaux accès aux quais du RER A. Les sous-fonctions de `/rera/` ont également été renommées du mode textuel (`/rera/cyseal`) au mode numérique (`/rera/2101`), et celles qui n'existaient pas ont été ajoutées.
### Réparations :
 - La plupart des `tellraw` des fichiers de `tch:give/acces_quais/` et `/siel/pid/` précisent désormais correctement le *préfixe* des terminus, et non plus seulement le nom "raccourci".

## [Version de développement - Renommage des stations M9-M12 et bateau dans station/get_id]	(7d1cfbea)	- 2023-03-27
### Modifications :
 - Les stations au nom temporaire (XXVillageYY et autres Tante Astruc) ont été renommées dans le fichier *tch:station/get_id* pour toutes celles ayant été nommées (desservies soit par le RER A, soit par un métro ou un bateau).

## [Version de développement - Implémentation des nouveaux SACD (rp#160)]	(fd581edb)	- 2023-03-20
### Modifications :
 - Puisqu'il existe désormais 2 modèles de SACD, la fonction d'update des PID (`tch:station/pid/update_pid`) vérifie désormais le type de SACD avant d'assigner un nouveau modèle.
 - La fonction de tag des joueurs par les PID (pour affichage des textes appropriés) a été déplacée dans des sous-fonctions `tch:station/pid/tag_joueur` et `tag_joueur2`, chacune étant dédiée à l'un des deux types de SACD (respectivement principal et secondaire).
 - L'infostation des SACD (`tch:station/debug/info/sacd` et `sacd2`) a été séparé de la fonction des PID (`tch:station/debug/info/pid`), cette dernière ne s'occupant plus que de l'affichage du texte généré par les sous-fonctions `sacd` et `sacd2` et choisissant entre ces deux-là en fonction du type du SACD.

## [Version de développement - hgive spécifique pour le métro 12 (rp#160)]	(1ba3f526)	- 2023-03-20
### Modifications :
 - Le hgive accès aux quais du métro 12 (`tch:give/acces_quais/m12`) propose désormais de sélectionner les trois panneaux "double direction" (2 des 3 terminus de la ligne) ajoutés dans rp#160.
 - Le hgive PID du métro 12 (`tch:give/siel/pid/m12`) propose désormais le PID bidirectionnel pour les 2 directions allant "dans le même sens" (122 et 123) dont le PID a été ajouté dans rp#160.
 - Le hgive PID (`tch:give/siel/pid/_main`) propose désormais le don d'un SACD soit principal, soit secondaire, pour utilisation sur les PID bidirectionnels.

## [Version de développement - RPgive des tables et chaises multi-teintes]	(0aadbc62)	- 2023-03-18
### Modifications :
 - Les nouvelles tables et chaises du RP ont été ajoutées au rpgive (**tdh-rp** : `tdh:give/rp/mobilier/chaise` et `table`).
### Réparations :
 - Un fichier oublié du commit précédent (**tdh-tch** : `tch:itineraire/init/stations/noms`) a été ajouté, car je suis complètement débile.

## [Version de développement - Nommage des nouvelles stations M9-M12]	(d289e3ef)	- 2023-03-18
### Modifications :
 - Toutes les stations du système d'itinéraire ayant un nom temporaire (commençant par `XX`) a été renommé dans toutes ses occurences (commentaires et code "réel") des fichiers `tch:itineraire/init/stations/connexions` et `noms`.

## [Version de développement - Bugfix du remplacement de la flore par de la neige]	(27abc5dc)	- 2023-03-12
### Réparations :
 - On teste désormais bien le biome actuel lorsqu'on tente de remplacer de la flore par de la neige, puisqu'on ne teste plus la présence de neige adjacente... (dans `tdh:meteo/effects/neige/blocs/test_flora`, on appelle bien `test_biome`).
 - Les blocs `azalea` et `flowering_azalea` ne font plus partie du block tag `#tdh:snow_replaceable` de **tdh-climate**. Ils ne sont plus inclus non plus dans le tag de **tdh-core** `#tdh:passable_flora` ; ils y étaient restés par erreur depuis leur ajout au block tag de Minecraft `#flowers`, dont on n'inclut désormais plus à `passable_flora` que les sous-tags `#small_flowers` et `#tall_flowers`.

## [Version de développement - Bugfix du déspawn occasionnel de piglins dans le RER (#317)]	(dbf86d95)	- 2023-03-11
### Modifications :
 - Des messages de debug supplémentaires ont été ajoutés à la génération du `piglin` placeholder du départ du RER (dans `tch:rer/rame/en_station/depart`, `depart/ajouter_joueur`, `ajouter_joueur/copier_donnees`, et `tch:rer/rame/en_station/arrivee/supprimer_mob`). Ils sont affichés aux joueurs ayant le tag `RERDebugLog`.
### Réparations :
 - Les piglins invoqués par la putain de fonction `tch:rer/rame/en_station/depart/ajouter_joueur/invoquer_mob` de merde ont désormais le tag NBT `PersistenceRequired:1b` pour éviter qu'ils ne *déspawnent* leurs morts !! (#317)
 - Une ligne de `tch:rer/rame/en_station/depart`, qui avait le même objectif que l'ajout du commit 8a43b2dc mais n'avait pas été correctement écrite (et donc ne fonctionnait pas), a été retirée de ce fichier.
 - Les réservations sont à nouveau exécutées dans la bonne station par les câbles et RER qui exécuteraient les fonctions de `tch:voie/reservation/`. Elles avaient été cassées par le commit 65f7e26f, qui a comme pour les métros remplacé la plupart des utilisations de la variable `idStation` par les RER et câbles par `prochaineStation`.

## [Version de développement - Détection des ticks écourtés dans le tdh-tick (#343 #349)]	(c6ea92a7)	- 2023-03-11
### Modifications :
 - Les ticks écourtés (= dont le nombre de commandes dépasse la longueur de la gamerule `maxCommandChainLength` et dont l'exécution est donc interrompue) sont désormais détectés par le système `tdh:tick` de **tdh-core**, avec une méthode assez simpliste : on définit une variable temporaire au début de l'exécution, et on la reset après avoir fini ladite exécution, ce qui permet, si la variable est encore définie au début d'une itération du `tdh:tick`, de déduire que le tick précédent n'a pas été terminé proprement et d'en informer tous les joueurs ayant le tag `TickDebugLog`. Dans ce but, un nouveau fichier a été créé pour chacune des "fréquences" du `tdh:tick` (par exemple `tdh:tick/3`, `tdh:tick/1_16`...), et le `tdh:tick` appelle ces fichiers plutôt que le function tag directement. (#343)
 - La fonction `tdh:tick` de **tdh-core** stocke désormais la variable `#quickTick duree`, qui spécifie l'intervalle entre deux exécutions du tick actuel. Cela permet aux fonctions appelées par `tdh:tick` de pouvoir en permanence connaître le nombre approximatif de ticks écoulés depuis leur dernière exécution. (#349)

## [Version de développement - Bugfix du comptage des joueurs au départ du RER]	(8a43b2dc)	- 2023-03-11
### Réparations :
 - Un bug du système RER (dans `tch:rer/rame/en_station/depart`) faisait en sorte que les caméras de tous les RER étaient systématiquement mises à jour, alors qu'on était supposés compter le nombre de joueurs présents à bord pour vérifier si on les updatait ou non ; en fait, la variable de comptage des joueurs n'était jamais remise à zéro, et considérait en permanence qu'il y avait des joueurs à bord.

## [Version de développement - Annonces d'arrivée en station génériques (#346)]	(65f7e26f)	- 2023-03-11
### Ajouts :
 - La fonction `tch:metro/spawner/_force_spawn` permet de forcer un spawner à envoyer son métro maintenant ; il sélectionne automatiquement le spawner le plus proche et le force à faire spawner un métro, mais seulement si la voie de destination est libre.
### Modifications :
 - Les fonctions de **tdh-tch** `tch:sound/alerte_arrivee_station_auto` et `arrivee_station_auto` ont été rendues génériques, et sont désormais respectivement accessibles via `tch:sound/arrivee_station1/jouer` et `tch:sound/arrivee_station2/jouer`. Au lieu de définir dans le datapack chaque couple ID de station / nom de sound event à chaque fois que l'on rajoute une station, il suffira désormais de les rajouter dans le RP en les nommant par leur ID de station plutôt que leur nom complet : le code du datapack est capable de jouer n'importe quel sound event pour des IDs de station allant de 0 à 9999.
   Pour permettre aux différentes voix des métros faisant correspondance dans la même station de se jouer correctement, on a dupliqué le code 6 fois avec 6 "voix" différentes, ce qui permet d'avoir maximum 6 lignes en correspondance dans la même station si l'on se débrouille bien pour répartir les différentes lignes du réseau sur les différentes voix. (Il est toujours possible d'en rajouter davantage, mais l'impossibilité d'utiliser des variables dans les noms de sound events rend le code *très* lourd et répétitif, par conséquent j'ai choisi 6 comme maximum car l'ensemble du réseau TCH actuel rentre dans 5 voix).
   Voici un exemple de nom de sound event avec cette nomenclature : `tch:station.voix5.1980_1`. On a comme expliqué précédemment 6 voix disponibles, de `voix1` à `voix6` ; on a également 10'000 ID de station disponibles (de 0 à 9999, bien qu'il soit probablement impossible d'utiliser 0 en pratique), à chaque fois en 2 versions (`_1` ou `_2`, qui correspondent en principe aux tons respectivement ascendant et descendant).
   Ces sound events sont automatiquement diffusés par le SISVE des câbles, RER et métros, à partir de leurs variables `prochaineStation` et `voix`. L'initialisation des lignes de `tch:itineraire/_init` enregistre désormais la variable `voix` pour chaque PC de ligne.
 - Les fonctions de terminus de **tdh-tch** (`tch:sound/terminus`, `terminus_en` et `terminus_jingle`) ont été rendues plus génériques. Les sons de terminus du RP doivent désormais être spécifiés selon une nomenclature stricte : chaque mode de transport ayant sa catégorie (`rer`, `metro` ou `cable`), on aura par exemple le second terminus du RER qui aura pour sound events `tch:annonce.rer.terminus.2_0` (jingle), `2_1` (fr) et `2_2` (en). Cela afin de permettre de modifier aisément les langues ou les messages sans toucher au code du datapack. Les nouvelles fonctions terminus se trouvent dans les dossiers `tch:sound/metro`, `rer` et `cable`, à chaque fois aux chemins d'accès `/annonce/terminus`, `terminus_2` et `terminus_jingle`.
   Les lignes correspondant à chaque ID de terminus ou de jingle ne sont plus hardcodées. Il faudra désormais définir ces combinaisons dans le RP (il est possible de créer plusieurs sound events nommés différemment qui jouent le même fichier son, notamment utile pour dupliquer les jingles sur plusieurs sons de terminus) avec les noms génériques de sound events spécifiés, et donner à chaque PC une variable `idTerminus` qui détermine l'ID du son de terminus que les véhicules de sa ligne diffusent. Jusqu'à 20 ID uniques (de 1 à 20) sont supportés par ce nouveau système.
   L'initialisation des lignes dans `tch:itineraire/_init` enregistre désormais cette variable pour chaque PC de ligne.
 - Le tag `SISVEPassagerRER` du `tch:sisve/rer/` a été renommé `SISVEPassager` et généralisé à l'ensemble des SISVE (Métro, câble et RER). Les annonces `arrivee_station1` et `arrivee_station2`, ainsi que `terminus`, diffusent désormais leurs sons (toujours stéréo) à tous les passagers ayant le tag `SISVEPassager`, comme le faisaient les fonctions exclusives au RER pour l'instant, plutôt que d'avoir une condition de distance/position différente pour les câbles et métros.
 - La fonction `tch:itineraire/_init` de **tdh-tch** a été séparée en plusieurs sous-fonctions, pour permettre d'initialiser certains champs du système d'itinéraire sans toucher au reste (surtout utile sur TDH pour éviter de reset le timer du SISVE calculé automatiquement, stocké dans le système d'itinéraire, en attendant de trouver une solution plus propre...). On a donc créé les fichiers `tch:itineraire/init/lignes` ; `/init/stations/noms`, `connexions` et `give` ; `/init/lieux_dits/noms` et `connexions`.

## [Version de développement - Empêchement des sons d'insecte sur des blocs non appropriés]	(9d9e26d8)	- 2023-03-08
### Modifications :
 - Le block tag `#tdh:base_insecte` de **tdh-rp-sound** ne contient plus les blocs d'air.
### Réparations :
 - Les émetteurs de sons d'insecte sont détruits dès leur spawn s'ils ne sont pas à maximum 5 blocs au-dessus d'un bloc du tag `#tdh:base_insecte`. Par conséquent, ils ne devraient plus se manifester dans les stations, au milieu des rues des villes et dans d'autres endroits artificialisés. (En revanche, ce n'est pas le cas pour les oiseaux.)

## [Version de développement - Correction des fonctions type_filtre_set de la config des aiguillages]	(0c5890e2)	- 2023-03-08
### Réparations :
 - Les fonctions `tch:metro/aiguillage/config/type_filtreX_set` définissent désormais le bon élément NBT dans les données d'entité de l'aiguillage.

## [Version de développement - Correction de l'actualisation des chunks déchargés]	(90883192)	- 2023-03-08
### Modifications :
 - Les entités d'actualisation automatique ne testent plus 2 fois plus de positions à chaque tick, car cela générait un nombre extrêmement élevé d'entités temporaires aux hauts niveaux d'intensité météo. Au lieu de cela, on a simplement une valeur fixe de 10 entités temporaires par entité d'actualisation automatique quel que soit le niveau d'intensité.
 - Le `tdh:meteo/effects/neige/blocs/test_flora` ne checke plus s'il y a de la neige aux alentours, mais vérifie en revanche qu'on replace bien le bloc de flore le plus bas (car par défaut on part de la position la plus haute : désormais il descend récursivement jusqu'à trouver le sol ou un bloc invalide style air.

## [Version de développement - Accélération de l'actualisation des chunks déchargés]	(3e242aaf)	- 2023-03-08
### Modifications :
 - Les entités d'actualisation automatique de **tdh-climate** testent désormais 2 fois plus de positions autour d'elles à chaque tick.

## [Version de développement - Plus de neige sur les routes]	(9266d4eb)	- 2023-03-08
### Modifications :
 - Le block tag `#tdh:ground` contient désormais le bloc `soul_sand`.
### Réparations :
 - De la neige ne peut plus se déposer sur les routes (`dirt_path`). Elle le faisait en raison de la présence dans le block tag `#tdh:ground` du `dirt_path`. Comme le retirer de `#tdh:ground` n'était pas souhaitable, on a préféré copier l'intégralité de `#tdh:ground` dans `#tdh:solid` (qui détermine quels blocs peuvent recevoir de la neige), hormis le `dirt_path`.

## [Version de développement - Test plus fiable pour le bug MC-186963 de spreadplayers]	(75a96570)	- 2023-03-08
### Ajouts :
 - Un tick (`tdh:meteo/effects/bug_spreadplayers/tick`) donne ou retire régulièrement un tag `AllowSpreadplayers` aux joueurs selon le biome dans lequel ils se trouvent, afin de ne pas avoir à faire le test à chaque tick. On a également rendu le test plus robuste ; au lieu de tester une seule fois le predicate à la position du joueur, on le teste 5 fois : une fois à sa position, et 4 fois à ±48 blocs sur chacun des deux axes horizontaux. Si au moins 2 de ces predicates tombent dans un biome aquatique (océan ou rivière), on n'autorise pas l'utilisation de `spreadplayers`. Par conséquent, on a déplacé le code des `tick_joueur2` dans `tick_joueur`, et le rôle de `tick_joueur` est rempli uniquement par une sélection des joueurs via le tag `AllowSpreadplayers`.
### Modifications :
 - Le test du tag `AllowSpreadplayers` pour les entités d'actualisation automatique a été déplacé de `tdh:meteo/effects/beau_temps/fonte_neige/` et `/neige/fonte` à `tdh:meteo/effects/actu_auto/test_entite`. Il teste, de manière similaire au code des joueurs, 5 positions différentes plutôt qu'une seule.
### Réparations :
 - Le tag `tdh:allow_spreadplayers` ne teste plus si on est dans un biome océan gelé ou non, et renvoie false dans tous les cas si on est dans un biome océan. En effet, autour des entités d'actualisation automatique, l'eau ne gèle pas ; par conséquent, on risque d'avoir des problèmes si on spawn dans un océan supposément gelé mais ne l'étant pas réellement. L'altitude de la couche de neige *sur un océan* n'étant pas fondamentale, on s'en passera sans trop de regrets.
 - Les `tick_joueur` de `tdh:meteo/effects/beau_temps/fonte_neige/` et `/neige/fonte/` s'exécutent désormais à la position de tous les joueurs, et non plus uniquement s'ils sont dans un biome neige. En effet, depuis la mise en place des saisons, on pouvait se trouver dans un biome contenant encore de la neige et ne pas la voir fondre car il n'y neigeait plus...
 - Les entités `SnowTemp` servant à calculer la montée/descente de la couche de neige (dans les fonctions `tdh:meteo/effects/beau_temps/fonte_neige/spread/` et `/neige/blocs/spread/`) sont désormais correctement placées autour de *chacune* des entités et joueurs valides, et non plus uniquement autour de la dernière entité à exécuter le spread. Le but du renommage des tags `SnowDown`/`SnowUp` en `SnowUpdate`+`SnowTemp` était déjà de réparer ce bug, mais il se trouve que je suis con...

## [Version de développement - Système d'aiguillage multi-filtres]	(cb44c993)	- 2023-03-08
### Ajouts :
 - Un système d'aiguillage pour les métros a été ajouté à **tdh-tch** dans le but de remplacer les vieillissantes fonctions `tch:auto/sort_minecart_` et d'ajouter des fonctionnalités de filtrage avancé, similaires à celles déjà existantes sur le RER, qui seront requises par certaines lignes en projet. Les aiguillages sont désormais des entités de type `marker` de tag `tchAiguillage` positionnées à l'emplacement du rail dont on souhaite définir l'orientation, et tous les paramètres de filtrage sont stockés dans les données de ladite entité :
    - Dans les scores, les deux variables `idLine` et `idLineMax` définissent les bornes (inclusives) des ID de ligne concernés par cet aiguillage.
	- Dans les données, la liste située dans `data.filtres` contient une série de filtres contenant les 3 tags suivants :
	    - `type`, de type *byte*, détermine le type de filtre : 1b signifie qu'on filtre selon la présence/absence d'un joueur à bord, 2b selon l'ID de ligne/direction du métro, et 3b est une sorte de "choix par défaut", en principe placé uniquement en toute fin de liste (pour servir de valeur "fallback" au cas où aucune autre ne matcherait)
		- `valeur`, de type *int*, détermine la valeur recherchée par le filtre : si le type est 1b, on a simplement le choix entre 0 (métro vide) et 1 (métro plein). Si le type est 2b, il s'agit de l'ID de ligne recherché. Si le type est 3b, cette valeur est inutilisée et peut ne pas être spécifiée.
		- `rail`, de type *byte*, détermine le type de rail à placer si la condition passe. Dans l'ordre, de 1b à 6b, on a `north_south`, `east_west`, `north_east`, `north_west`, `south_east` et `south_west`.
	  L'ordre des éléments de cette liste est important : les filtres sont testés dans l'ordre dans lequel ils sont spécifiés, ce qui permet de combiner différents types de filtres en conservant un résultat prévisible (par exemple, on envoie d'abord tous les métros vides se faire détruire ; puis on discrimine entre 2 ID de ligne différents ; puis on envoie tous ceux qui ne matchent pas sur l'une de ces deux directions, où un second aiguillage se chargera de filtrer les ID de ligne incorrects pour les envoyer sur une voie de garage).
   Ce système se trouve dans `tch:metro/aiguillage/` et son tick tourne une fois toutes les 2 secondes grâce au function tag `#tdh:tick/1_2`.
 - Pour éviter d'avoir à configurer tout ça manuellement, de surcroît sans pouvoir visualiser la position de l'entité, une fonction de configuration des aiguillages (`tdh:metro/aiguillage/_config`) a été ajoutée à tous les emplacements nécessaires : les function tags `#tdh:config/tick` et `end_all`, ainsi que `prompts` qui lui permet d'être proposée par `tdh:_config`. Elle permet de configurer l'entité `tchAiguillage` la plus proche du joueur qui l'appelle, et affiche également les ID de ligne sous forme complète (ID de ligne + nom du terminus).

## [Version de développement - Flexibilisation de la recherche de connexion du système d'itinéraire]	(6b75c2ff)	- 2023-03-07
### Modifications :
 - La fonction `tch:itineraire/noeud/trouver_connexion` a vu ses arguments légèrement modifiés pour la rendre plus flexible. Auparavant, elle ne pouvait trouver une connexion que si on connaissait les 3 variables la caractérisant (`idStation`, `idLine` et `idSortie`) : désormais, on peut signaler que la valeur d'une ou plusieurs de ces variables n'importe pas ou est inconnue, en la réinitialisant (`scoreboard reset`) ou en lui donnant une valeur négative. Par conséquent, la recherche de connexion de `tch:metro/rame/en_station/prochaine_station` utilise désormais cette fonction, et ses deux sous-fonctions `connexion` et `recursion` ont été supprimées.
 - L'infostation (`tch:station/debug/info`) n'actualise désormais plus les armor stands `ProchaineStation`, qui ont été rendues obsolètes par le commit 9f0177f0 ; sa sous-fonction `tch:station/debug/info/lignes` affiche désormais des informations de prochaine station extraites du système d'itinéraire, ainsi qu'une ligne d'avertissement pour chaque armor stand `ProchaineStation`.
 - Le code de génération des textes de correspondance de **tdh-tch** (`tch:itineraire/init/correspondances`) a été complètement réécrit et simplifié, car il était parfaitement nul à chier. La fonction de recherche n'itère plus sur l'ensemble des ID de lignes existant sur TDH, mais utilise l'opération `<` de `scoreboard` pour trouver le minimum et ajouter son ID de ligne à la liste des correspondances : le maximum arbitraire de 8 lignes en correspondance dans la même station a donc été supprimé.
 - Le code de génération des textes de correspondance de **tdh-tch** (`tch:itineraire/init/correspondances`) affiche désormais les lignes en correspondance dans une station liée à la première par une liaison intérieure. Pour éviter de trop alourdir le code, on se contente du cas simple où seuls 2 PC de station sont en liaison intérieure l'un avec l'autre (sur TDH ce n'est déjà le cas qu'avec Le Hameau RG/RD).
### Réparations :
 - Le nom automatiquement généré pour certains PC du système d'itinéraire (`tch:itineraire/init/nom_pc/quai`, `sortie` et `station`) interprète désormais correctement le texte NBT du panneau servant de stockage intermédiaire.
 - La limite arbitraire de 20 blocs de distance de la fonction `tch:itineraire/init/details` a été retirée.
 - La condition d'un message d'erreur de la fonction `tch:metro/rame/en_station/prochaine_station` a été corrigée, car celui-ci n'était jamais affiché (même lorsqu'il aurait dû l'être).

## [Version de développement - Actualisation automatique de la couche de neige des chunks déchargés]	(afb0cfcf)	- 2023-03-07
### Ajouts : 
 - Le système des effets météo de **tdh-climate** contient désormais un système d'actualisation automatique des chunks (`tdh:meteo/effects/actu_auto`), configurable via `tdh:meteo/_config`. Il permet, lorsque le nombre de joueurs sur le serveur est inférieur à un seuil (configurable), d'invoquer un certain nombre (configurable) de marqueurs temporaires qui forceloadent des chunks au hasard dans le monde pour actualiser leur couverture neigeuse.
 - De nouveaux block tags ont été ajoutés à **tdh-core** : `#tdh:concrete`, `#tdh:concrete_powder`, `#tdh:copper_blocks`, `#tdh:glass`, `#tdh:glass_pane`, `#tdh:ores` et `#tdh:solid`.
 - Un visualiseur de biomes de debug a été ajouté à **tdh-climate**, dans `tdh:meteo/debug/biome_view/`. Il n'a pas d'interface de config pour l'instant : il suffit d'activer le système (`#biomeView on = 1`) et de se donner 2 tags (`BiomeView` + `BiomeViewEau` **ou** `BiomeViewCaverne`).
### Modifications :
 - Le configurateur de **tdh-climate** `tdh:meteo/_config` utilise désormais un entier négatif à la valeur très élevée à la fois comme valeur "neutre" (anciennement 0) et comme valeur "annuler" (anciennement -120047), pour éviter d'interférer avec la plage de valeurs possibles pour les bornes en X et Z de la zone à actualiser (qui peut être n'importe quelle coordonnée de bloc, positive comme négative).
 - Les boules de neige invoquées par les fonctions de `tdh:meteo/effects/beau_temps/fonte_neige/` et `/effects/neige/blocs/` partagent désormais le même tag (`SnowUpdate`) au lieu d'avoir chacun le leur (`SnowUp`/`SnowDown`).
 - De la neige peut désormais tomber même s'il n'y a pas une première couche de neige préexistante, pour permettre l'actualisation sans joueurs alentour (les joueurs étant nécessaires à la survenue du random tick). La fonction `tdh:meteo/effects/neige/blocs/test_biome` a été ajoutée dans ce but.
 - Le predicate `tdh:allow_spreadplayers` contournant le bug MC-186963 est désormais exécuté par les joueurs dans la sous-fonction `tick_joueur` plutôt que directement dans le `tick`. Par conséquent, la fonction `tick_joueur` a été renommée `tick_joueur2`, et est appelée lors du succès du predicate dans `tick_joueur`. Il a également été déplacé de **tdh-climate** à **tdh-core**.
 - Plusieurs block tags de **tdh-core** ont été complétés, notamment `#tdh:ground` qui intègre de nombreux nouveaux blocs des dernières mises à jour.
### Réparations :
 - On ne peut plus faire monter automatiquement (avec **tdh-climate**) la hauteur d'une couche de neige s'il ne neige pas en ce moment à cet endroit (on n'avait pas pensé à l'époque de l'écriture de ce code aux saisons). On a ajouté à ces fins la fonction `tdh:meteo/effects/neige/blocs/test_neige`. Une couche de neige qui tente de s'élever à un endroit où il pleut sera même supprimée d'une seul coup.

## [Version de développement - Description des datapacks dans le readme]	(a5205adb)	- 2023-03-04
### Modifications :
 - Le `README.md` a été étoffé : il comprend désormais une description des fonctionnalités de chacun des packs et de leur niveau de stabilité, et les instructions d'installation ont été mises à jour.

## [Version de développement - Merge de tdh-climate-seasons dans dev]	(2855753d)	- 2023-03-04
 - Cette branche, qui a dérivé pas mal de temps, comprend à la fois les ajouts de la branche *tdh-crafting* et ceux de la branche *tdh-climate-seasons*, et ajoute donc des versions (très) incomplètes, mais néanmoins relativement fonctionnelles, de l'ébauche de système de crafting amélioré et de machinerie, ainsi que les datapacks saisonniers. Il a paru logique de la merger après le contournement du bug MC-186963, qui est assez critique et, bien que touchant majoritairement à **tdh-climate** vu le nombre de `spreadplayers` que le datapack contient, s'appliquait aussi à **tdh-rp-sound**.

## [Version de développement - Moins de sons d'insectes dans les villes]	(3d96d07a)	- 2023-03-04
### Réparations :
 - Les sons d'insectes, qui étaient rares en ville avec `spreadplayers` en raison du manque de blocs valides sur lesquels spawner, sont omniprésents avec la nouvelle version du code qui les autorise à spawner en l'air. Par conséquent, on divise par 4 le nombre maximum d'émetteurs d'insectes lorsqu'on est en ville, et par 2 lorsqu'on est dans un village.

## [Version de développement - Contournement du bug MC-186963 de spreadplayers]	(e9e259dd)	- 2023-03-04
### Réparations :
 - En raison du bug [MC-186963](https://bugs.mojang.com/browse/MC-186963), un lag extrêmement intense peut se manifester lors d'une commande `spreadplayers` si aucun ou presque aucun bloc solide n'est à portée de ladite commande. Toutes les occurences de `spreadplayers` dans le datapack ont été modifiées : soit (c'est le cas pour les effets météo de **tdh-climate**) on utilise le predicate `tdh:allow_spreadplayers`, soit (c'est le cas pour les sons d'insectes et d'oiseaux de **tdh-rp-sound**) on cesse d'utiliser `spreadplayers`, et on définit des positions aléatoires autour du joueur à l'aide du storage et de commandes scoreboard.
### Ajouts :
 - Un predicate de **tdh-climate**, `tdh:allow_spreadplayers`, détermine si on doit désactiver les appels à spreadplayers autour de la position d'où il est appelé ; il vérifie simplement si on est dans un biome océan (avec le predicate `tdh:biome/ocean`) et échoue (= interdit l'utilisation de spreadplayers) si on est bien dans un biome océan et qu'on ne passe pas également le predicate `tdh:biome/neige_0` (qui indique que l'eau peut geler à toute altitude).
### Modifications :
 - L'écran de configuration de **tdh-climate** indique désormais l'existence du bug MC-186963, et donne quelques conseils pour limiter ses conséquences (`tdh:meteo/config/chute_neige/distance_select` et `fonte_neige/distance_select`). La plage de valeurs cliquables a été décalée (elle passe de 16-104 à 64-152).
 - Les tags de bloc de **tdh-rp-sound** `base_insecte` et `base_oiseau` incluent désormais également les blocs d'air, pour ne pas leur interdire de spawner à distance du sol. Il faudra ajuster un peu l'effet (car de nombreuses entités se retrouvent en l'air même quand on est en haut d'une colline/d'un arbre...), mais ça a le mérite d'être plus varié que le système précédent (et d'éviter d'utiliser `spreadplayers`).
 - Quelques messages de debug ont été ajoutés à la fonte/montée de la neige, accessibles en utilisant le tag *MeteoDetailLog* ; et aux sons d'insectes et d'oiseaux de **tdh-rp-sound**, accessibles avec le tag *InsecteDebugLog*.
 - Le tag *InsecteDebugLog* permet également d'afficher une particule à l'emplacement de chaque diffuseur de son d'insecte ou d'oiseau, en définissant également le score `#insecteDebug on` à 1. (On peut le retirer ou changer sa valeur après coup pour le désactiver)

## [Version de développement - Calcul automatique du retard du SISVE / Prochaine station des métros récupérée dans le système d'itinéraire (9f0177f0) - 2023-03-03 :
### Ajouts :
 - 3 nouvelles fonctions de tag ont été ajoutées, dans `tch:tag/` : `prochaine_station_pc`, `prochain_quai_pc` et `prochaine_voie`. Elles fonctionnent exactement comme les fonctions `station_pc`, `quai_pc` et `voie` du même répertoire (et donnent le même tag `ourStationPC`/`ourQuaiPC`/`ourVoie`), mais utilisent comme source de l'ID de station la variable `prochaineStation` de l'entité qui les appelle plutôt qu'`idStation`.
 - De nouvelles fonctions permettent à un métro de calculer lui-même le décalage entre l'arrivée en station "prévue" par le SISVE et l'arrivée en station réelle, et de l'enregistrer dans le système d'itinéraire pour corriger le décalage aux prochains passages sur le même interstation (`tch:metro/rame/en_station/corriger_timer_sisve`). Ce décalage du timer du SISVE, enregistré dans les données de connexion des `tchItinNode` du système d'itinéraire, est récupéré et appliqué par les métros au déclenchement du pré-station, dans la fonction `tch:sisve/metro/pre_station/debut`.
 - Quelques fonctions génériques permettant de sélectionner (placer en tête de la liste des connexions) une connexion spécifique dans un `tchItinNode` ont été ajoutées dans `tch:itineraire/noeud/`. Il suffit de définir les variables `#tchItineraire idStation`, `idLine` et `idSortie` aux valeurs recherchées (O si on ne souhaite pas rechercher cette valeur), et d'appeler la fonction `tch:itineraire/noeud/trouver_connexion`. L'entité l'ayant appelée aura le tag `ConnexionTrouvee` si la connexion recherchée a bien été trouvée.
### Modifications :
 - Les métros n'utilisent plus l'armor stand `ProchaineStation` ; ils récupèrent à chaque arrivée en gare la prochaine station de leur itinéraire, en fonction de leur ID de ligne, en utilisant les données du système d'itinéraire. La logique de ce check de prochaine station se trouve dans `tch:metro/rame/en_station/prochaine_station` et ses sous-fonctions.
 - La manière dont les métros stockent la prochaine station a été modifiée. Précédemment, ils stockaient uniquement une variable `idStation`, qui contenait l'ID de la station actuelle lorsqu'ils étaient en station, et l'ID de la prochaine station à partir du moment où ils redémarraient. Désormais, ils stockent 2 variables distinctes, `idStation` et `prochaineStation`, qui contiennent toujours respectivement l'ID de la station précédente (ou actuelle) et celui de la prochaine station. De nombreux fichiers ont par conséquent été modifiés (dans `tch:voie/reservation/`, `tch:sisve/metro/`, `tch:metro/rame/`, `tch:sound/arrivee_station/` et `alerte_arrivee_station/`) ; parfois (lorsqu'il s'agissait de fichiers concernant à la fois métro et RER/câble) des checks ont été ajoutés pour discriminer les deux car je n'avais pas le courage de m'occuper tout de suite du code (similaire) à écrire pour les câbles et RER.
 - Lorsque le temps avant départ est supérieur à 5 secondes, le SISVE métro affiche désormais le nom de la station actuelle plutôt que ce temps d'attente avant le départ. (`tch:sisve/metro/en_station/tick` et `nom_station`)
 - Le type d'entité item_frame pour les PorteQuai est désormais précisé explicitement dans les sélecteurs qui l'ouvrent et la referment. (`tch:metro/rame/en_station/tick` et `arret`)

## [Version de développement - Nommage automatique des item frames du système d'itinéraire (1aaac26a) - 2023-03-03 :
### Ajouts :
 - Une nouvelle fonction a été créée pour compiler et enregistrer dans les données *extra* du storage de dialogue une liste ordonnée des lignes ouvertes, et une autre pour les lignes fermées : il s'agit de la fonction *tdh:dialogue/calcul/lignes_ouvertes_fermees* et de ses sous-fonctions.
### Modifications :
 - Jusqu'à présent, les item frames du système d'itinéraire n'étaient pas nommées, ce qui ne simplifiait pas les choses pour les identifier dans les commandes. Désormais, l'initialisation du système d'itinéraire (*tch:itineraire/init/textes*) copie automatiquement le nom de l'item dans le nom de l'item frame ; c'est le cas pour les PC de ligne, de quai, de station et de sortie.
 - Le dialogue (**tdh-dialogue**) du topic "lignes" de l'agent TCH (*tdh:dialogue/topics/tch/lignes/agent_tch*) a été modifié, de sorte à utiliser les noms de ligne stockés dans les données d'item (Item.tag.display.line.nom) plutôt que le CustomName de l'entité.

## [Version de développement - Messages de debug plus précis et bugfixes du système d'itinéraire (61ab2e21) - 2023-03-03 :
### Ajouts :
 - En parallèle de l'itinéraire "condensé" (avec un nouveau champ pour chaque changement de ligne/sortie d'une station uniquement, dans le but d'être affichable presque en l'état par un agent TCH), on stocke désormais un itinéraire "debug", dont les données se trouvent pour chaque entité tchItinNode dans **Item.tag.tch.itineraire.chemin_debug** (*tch:itineraire/calcul/init/noeud*, *depart* ; *tch:itineraire/calcul/voisins/itineraire* et sa sous-fonction */itineraire/ajout_noeud_debug*).
 - De nouveaux messages de debug plus précis ont été ajoutés au système de recherche d'itinéraire, et sont affichés aux joueurs ayant le tag **ItinDetailLog**.
### Réparations :
 - Un bug bien difficile à cerner a été corrigé : la fonction *tch:itineraire/calcul/voisins/trouver*, lorsqu'elle recherchait une station par ID de station (dans le cas d'une liaison urbaine TCH de station à station), sélectionnait aussi **les sorties de cette station**, qui n'arrivaient pas à copier l'itinéraire déjà emprunté : tout se passait alors comme si on recommençait le calcul d'itinéraire à partir de cette sortie qui, n'ayant enregistré ni distance ni durée du trajet écoulé, était ultra favorisée par le système de sélection du prochain noeud (qui choisit toujours le noeud le plus proche encore non visité).
 - Le tag **nouvelleEntree** est correctement retiré dans la fonction qui le donne, *tch:itineraire/calcul/voisins/update*, plutôt que dans la fonction *tch:itineraire/calcul/voisins/itineraire*, pour éviter les cas où nouvelleEntree n'est pas correctement retiré et persiste.
 - Également à ces fins, on a ajouté le tag **nouvelleEntree** aux tags automatiquement supprimés pour tous les tchItinNode par la fonction *tch:itineraire/calcul/init/noeud*.
 - Les "fausses correspondances" sur le réseau métro ont en principe été corrigées par la fonction *tch:itineraire/calcul/voisins/update/fausse_correspondance* appelée par *tch:itineraire/calcul/voisins/update*, avec une méthode un peu différente des "fausses correspondances" RER car leur raison n'est pas la même : dans le cas générique, cela se produit quand on emprunte une correspondance en direction d'une ligne mais que l'on ne souhaite pas la prendre dans la direction 1. On enregistre alors d'abord la direction 1 (puisqu'il s'agit de l'ID de la correspondance), puis la direction 2 (puisque c'est la connexion que l'on emprunte) ; le bugfix fait que le système d'itinéraire retirera au moment de l'ajout d'une direction l'étape précédente, si et seulement si elle avait pour destination la station d'arrivée de l'étape qui la précédait (ou la station de départ de l'itinéraire si aucune étape ne la précédait).

## [Version de développement - Gestion des directions d'une même ligne dans le système d'info itinéraire (6725450c) - 2023-03-03 :
### Modifications :
 - Deux nouvelles fonctions du debug d'itinéraire permettent de choisir l'ID de ligne ou de station respectivement sur le PC Quai (*tch:itineraire/debug/_ligne_pc_proche*) et sur le PC station (*_station_pc_proche*) le plus proche, au lieu d'un "vrai" quai ou d'une "vraie" station dans les fonctions d'origine (*_ligne_proche* et *_station_proche*).
### Réparations :
 - Le système d'info itinéraire (*tch:itineraire/debug/*) prend désormais en compte les modifications apportées par les commits précédents ; il affiche l'ID de direction associé à chaque connexion (si elle en a un), et le système d'info de ligne (*tch:itineraire/debug/ligne*) se place à chaque terminus et tente de tracer un chemin en direction de chacun des autres terminus (cela fait quelques lignes inutiles/orphelines mais au moins c'est exhaustif).

## [Version de développement - Gestion des directions d'une même ligne dans les résultats de recherche d'itinéraire (8fb0227d) - 2023-03-02 :
### Modifications :
 - Les passages par des hubs de station ne sont plus affichés par les agents TCH lorsqu'ils donnent un résultat de recherche d'itinéraire. (**tdh-dialogue**/*tdh:dialogue/en_attente/itineraire/resultat/etape*)
 - L'agent TCH fournit désormais la direction, en plus de la ligne, lorsqu'il conseille d'en emprunter une. (**tdh-dialogue**/*tdh:dialogue/en_attente/itineraire/resultat/etape/ligne*)
 - Le système de recherche d'itinéraire enregistre bien la direction exacte, et non plus l'ID global de ligne, pour chaque étape de l'itinéraire. (*tch:itineraire/calcul/voisins/itineraire/ajout_noeud* et *update_noeud*) Il stocke également le nom du terminus, avec la couleur de la ligne, grâce à la fonction *tch:itineraire/calcul/voisins/itineraire/calcul_terminus*.

## [Version de développement - Amélioration de l'approximation des correspondances du système d'itinéraire (2879d6d8) - 2023-03-02 :
### Modifications :
 - Un "battement" de correspondance a été ajouté à la fonction *tch:itineraire/calcul/voisins/update* ; 10 mètres + 20 minutes ingame sont ajoutés, pour éviter au calcul d'itinéraire de proposer des correspondances inutiles. La valeur précise pourra peut-être être ajustée, mais les résultats observés avec +20mn ne sont pas mauvais.
### Réparations :
 - Le calcul d'itinéraire n'affiche plus une ligne supplémentaire de correspondance allant vers la même station (cela était dû au fait que la correspondance va pointer vers la 1ère direction de la ligne, par exemple 11 ; si on prend alors la ligne direction 12, il considérait cela comme une correspondance) : on l'ignore désormais explicitement dans la fonction de calcul du tag nouvelleEntree (elle-même déplacée de *tch:itineraire/calcul/voisins/itineraire* à son "parent" *tch:itineraire/calcul/voisins/update* pour permettre l'ajout du battement de correspondance).
 - Une nouvelle fonction (*tch:itineraire/calcul/voisins/update/fausse_correspondance_rer*) appelée par le calcul du tag nouvelleEntree (dans *tch:itineraire/calcul/voisins/update*) permet de changer de direction sur une ligne de RER sans afficher explicitement la précédente dans le trajet enregistré, si les deux directions vont bien dans la même direction "globale" (par exemple le RER A direction Procyon ou Cyséal sur le tronçon central de la ligne). Cela permet d'éviter d'avoir à calculer à l'avance quelle est la bonne direction à emprunter sur un tronçon où plusieurs directions cohabitent, puisqu'hormis cela leurs distances et durées sont parfaitement identiques...

## [Version de développement - Accélération du calcul d'itinéraire (6fce6ac5) - 2023-03-02 :
### Modifications :
 - Le calcul d'itinéraire est désormais capable de calculer plusieurs nœuds par tick (actuellement 8, contre 1 seul précédemment). La récursion ne se passe plus directement dans *tch:itineraire/calcul/prochain_noeud*, mais dans sa sous-fonction */prochain_noeud/recursion*, qui décide entre schedule la fonction dans 1 tick ou la réexécuter immédiatement en fonction du nombre d'opérations effectuées à ce tick.

## [Version de développement - Spécification explicite des directions des lignes dans les connexions du système d'itinéraire (92e8c9d5) - 2023-03-02 :
### Modifications :
 - L'ID de ligne est désormais spécifié dans les données des connexions du système d'itinéraire (*tch:itineraire/_init*) qui empruntent une ligne de transport, en plus de la station de destination. Cela ne concerne pas les connexions entre stations réalisées à pied (comme Hameau RG—RD, ou n'importe quelle sortie vers un lieu-dit).
 - Lorsqu'on calcule un itinéraire, lors du calcul d'une connexion (*tch:itineraire/calcul/voisins/itineraire* et */voisins/itineraire/ajout_noeud*), on ne compare plus l'ID de ligne de l'entité à laquelle cette connexion est rattachée avec celui de l'entité vers laquelle pointe la connexion ; on compare l'ID nouvellement spécifié dans cette connexion avec l'ID **de la connexion précédente**, enregistré en première position dans le chemin de l'entité actuelle. Cela permet de différencier les ID de direction les uns des autres au sein de la même ligne.
### Réparations :
 - En raison de la spécification explicite des directions dans les infos des connexions, le souci qui faisait que l'agent TCH ne précisait pas lorsqu'on devait faire correspondance sur la même ligne (par exemple, pour faire Procyon—Grenat en RER A) a été corrigé. (En revanche, vu les modifications substantielles apportées aux résultats du calcul d'itinéraire, la manière qu'a l'agent TCH de présenter lesdits résultats est pour le moins chaotique, à régler dans un prochain commit)
 - Une commande affichant un message de debug de *tch:itineraire/calcul/voisins/trouver* a été légèrement modifiée, car elle affichait parfois des messages incohérents et utilisait bien plus de sélecteurs complexes que nécessaire.
 - De nouvelles conditions ont été ajoutées à *tch:itineraire/calcul/voisins/trouver*, ce qui permet d'une part de discriminer entre trajets en transport et trajets à pied, et d'autre part (enfin!) d'emprunter les trajets à pied extérieurs aux stations lors des recherches d'itinéraire (et donc, par exemple, de rejoindre des lieux-dits comme le Donjon des Ténèbres ou la citadelle de Gzor).

## [Version de développement - Les sous-fonctions d'infostation ne renomment plus les entités (793b9cc8) - 2023-03-02 :
### Réparations :
 - Correction d'un souci (pas à proprement parler un bug) : puisque chacune des sous-fonctions d'infostation renommait les entités qui avaient le bon tag en suivant une certaine convention de nommage, il était impossible de se rendre compte a posteriori en regardant l'infostation qu'on avait par erreur donné deux tags différents (et incompatibles, comme PID/tchHP) à la même entité, puisque dans chaque sous-fonction elle était renommée selon la convention du tag de cette sous-fonction ! Désormais les noms des entités ne sont plus actualisés que tous ensemble, lors de l'appel de la fonction infostation principale (*tch:station/debug/info*).

## [Version de développement - Correction de définitions erronées du système d'itinéraire (fca46215) - 2023-03-02 :
### Modifications :
 - Les PC de sorties TCH copient désormais bien le nom de leur station dans leurs données.
### Réparations :
 - Les données du quai de la ligne 4 à Glandebruine Nord étaient malencontreusement assignés à la ligne 2, et empêchaient les deux dites lignes de passer par Glandebruine Nord lors d'une recherche d'itinéraire.

## [Version de développement - Affichage de lignes complètes dans le debug d'itinéraire (1207d257) - 2023-03-02 :
### Ajouts :
 - Un système d'affichage des tracés complets des lignes est désormais disponible dans le debug du système d'itinéraire (*tch:itineraire/debug/ligne*) ; il détecte tous les terminus de cette ligne, et tente de suivre leur tracé jusqu'à un autre terminus. Il ne fonctionne pas correctement pour les lignes à branche (actuellement RER A et M12), mais offre déjà une aide précieuse pour les lignes "classiques" (et ne devrait pas être trop compliqué à améliorer pour faire en sorte qu'en cas de branche, il suive séparément chacune des branches au lieu d'en "choisir" une). Il est accessible soit en définissant #tchItinDebug idLine et en appelant *tch:itineraire/debug/ligne*, soit en utilisant la fonction *tch:itineraire/debug/_ligne_proche* qui détecte l'armor stand NumLigne la plus proche et utilise son ID de ligne comme input de la fonction.

## [Version de développement - Infos de debug du système d'itinéraire (af21e828) - 2023-03-01 :
### Ajouts :
 - Un système d'affichage des informations du système d'itinéraire a été mis en place. Il se trouve dans *tch:itineraire/debug/station* et ses sous-fonctions, et est accessible soit par infostation (*tch:station/debug/info*), soit directement par la commande *tch:itineraire/debug/_station_proche* (dans les deux cas, on récupère l'ID de la station la plus proche, puis on affiche ses données).

## [Version de développement - Infostation pour les portes de quai (635760dc) - 2023-03-01 :
### Modifications :
 - Tous les noms automatiques des entités secondaires des stations sont désormais calculés automatiquement (même s'ils ne sont pas affichés) au début de la fonction infostation (*tch:station/debug/info*), au lieu d'être simplement calculés au moment où leur affichage est nécessaire.
 - La fonction infostation affiche désormais les portes de quai (ainsi qu'un message d'erreur si elles sont manquantes sur un ou plusieurs quais d'une station), et une nouvelle sous-fonction leur est dédiée (*tch:station/debug/info/portes_quai*).
 - La fonction *tch:station/debug/info/id_station* ajoute également le nom de la station (récupéré sur le PC de station) au storage, au lieu d'avoir une copie de cet ajout au storage dans de multiples fonctions de l'infostation (*tch:station/debug/info/hp*, *pid*, *distributeurs*, *valideurs*...).
 
## [Version de développement - Recalcul des timings de l'itinéraire de la plupart des métros (b7efb26f) - 2023-03-01 :
### Modifications :
 - Toutes les lignes de métro, à l'exception des lignes 3 et 4 qui avaient déjà été recalculées récemment, ont été mesurées et enregistrées dans le système d'itinéraire (*tch:itineraire/_init*). Cela a conduit à réduire légèrement les durées de trajet pour la plupart d'entre elles.
 - Un métro qui part d'une station sera simplement supprimé (au lieu d'être relancé) s'il n'y a aucun joueur dans un rayon de 60 blocs à la ronde. (*tch:metro/rame/en_station/depart*)
 - 3 nouvelles entités peuvent être électrocutées par des rails : les abeilles, les poulets et les cave spider (*tch/tags/entity_types/electrocutable.json*)

## [Version de développement - Réparation d'un message non affiché dans infostation:acces (546ff39b) - 2023-03-01 :
### Réparations :
 - Une condition inutile tirée d'un copier-coller a été retirée de *tch:station/debug/info/acces/sortie*, qui affiche désormais correctement les sorties orphelines dans la sous-section d'infostation.

## [Version de développement - Ajout des codes 3 lettres des stations (46df53ce) - 2023-02-28 :
### Modifications :
 - Le système d'itinéraire définit désormais un champ "Item.tag.display.station.code" contenant le code station à 3 lettres de chaque station du réseau TCH.
 - Les noms générés automatiquement pour les entités secondaires des stations (HP, PID, valideurs, distributeurs...) utilisent désormais le code station plutôt que le nom complet, pour raccourcir les listes desdites entités.
### Réparations :
 - Le système infostation ne tentera désormais plus de recalculer l'ID d'une ligne s'il n'a pas trouvé au moins 2 directions proches. (dans *tch:station/debug/update/maj_id/num_ligne*)

## [Version de développement - Réécriture d'infostation (2c688fde) - 2023-02-28 :
### Modifications :
 - Le système d'infostation a été réécrit, et déplacé de *tch:auto/debug/infostation* à *tch:station/debug/_info*. De nombreuses nouvelles informations sont affichées, notamment sur les haut-parleurs, les PID, les valideurs et les distributeurs. La plupart des informations sont désormais dans des sous-menus pour éviter de générer un énorme mur de texte, et seules des infos condensées sont affichées sur la "page d'accueil" de l'infostation.
### Réparations :
 - Un spawner ne peut désormais plus déclencher des messages de PID sur un PID qui ne fait pas partie de la même station que lui.

## [Version de développement - Ajout de la station Le Hameau–Port au système d'itinéraire (5f0cb822) - 2023-02-27 :
### Modifications :
 - La station Le Hameau–Port a été ajoutée au système d'itinéraire (*tch:itineraire/_init*).

## [Version de développement - Amélioration des dialogues de l'agent TCH en recherche d'itinéraire (69838670) - 2023-02-27 :
### Modifications :
 - La temporisation de l'agent TCH lors d'une recherche d'itinéraire (**tdh-dialogue**/*tdh:dialogue/en_attente/itineraire/temporiser*) est désormais moins fréquente (toutes les 5-10 secondes) pour ne pas spammer le chat inutilement.
 - Les distances affichées par l'agent TCH en même temps que les résultats d'une recherche d'itinéraire (*tdh:dialogue/en_attente/itineraire/resultat/mesures*) sont désormais multipliées par le facteur de temps (relativement au temps réel). Par exemple, avec une durée du jour de 40mn IRL (36 jours par jour), les distances seront multipliées par 36.
### Réparations :
 - Une ligne de dialogue de l'agent TCH lors d'une recherche d'itinéraire, qui disait "Vous voulez aller de <destination>" au lieu de "Vous voulez aller **à** <destination>", a été corrigée. (*tdh:dialogue/topics/tch/itineraire/agent_tch/debut_calcul/reponse/en_cours*)

## [Version de développement - Mise à jour du hgive logo pour les lignes M9-12 (c7be0339) - 2023-02-27 :
### Modifications :
 - Le hgive logo (*tch:give/logo/*) liste désormais les logos des lignes de métro 9 à 12, et les autodétecte.

## [Version de développement - Correction d'un bug d'initialisation du système d'itinéraire (ff74e367) - 2023-02-27 :
### Réparations :
 - Le commit précédent avait malencontreusement supprimé les informations de connexion (correspondance/sortie) des item frames terminus d'une ligne lors de l'initialisation des infos de mise en page du hgive totem : on écrasait par erreur l'intégralité du tag *Item.tag.tch* au lieu de simplement écrire dans *Item.tag.tch.give*. Les commandes incriminées ont été modifiées.

## [Version de développement - Mise à jour du hgive des totems de lignes (a317eaf6) - 2023-02-26 :
### Modifications :
 - Le hgive des totems de lignes (*tch:give/plan/lignes/totem/*) a été réécrit et est désormais beaucoup plus simple et dynamique (dans la limite du possible/raisonnable). Il détecte désormais automatiquement la station la plus proche et les lignes en faisant partie, et calcule automatiquement l'ID de panneau en le donnant au joueur. Le résultat est un peu lourd (et fait un usage intensif de l'interprétation de texte JSON via le panneau TexteTemporaire) mais permet de réduire considérablement le travail futur de mise à jour de ce système (puisque toutes les données sont fournies par le système d'itinéraire).
 - L'initialisation du système d'itinéraire (*tch:itineraire/_init*) enregistre désormais dans les données NBT de chaque terminus un texte JSON (dans **Item.tag.tch.give.totem.texte**) servant à composer les textes cliquables du hgive des totems de lignes.

## [Version de développement - Mise à jour du hgive des panneaux accès aux quais Métro/Câble (b51de01d) - 2023-02-25 :
### Modifications :
 - Les fonctions de *tch:give/acces_quais/* détectent désormais automatiquement le nom des terminus dans le système d'itinéraire, et affichent toujours des données à jour.

## [Version de développement - Mise à jour du hgive des panneaux corresp Métro/Câble (d55fbd83) - 2023-02-25 :
### Modifications :
 - Les fonctions de *tch:give/corresp/* détectent désormais automatiquement le nom des terminus dans le système d'itinéraire, et affichent toujours des données à jour.
### Réparations :
 - La ligne 12 a bien 3 terminus au lieu de 2 dans le système *tch:give/siel/pid/*.

## [Version de développement - Mise à jour automatique des noms station/terminus par infostation (f673bc46) - 2023-02-25 :
### Modifications
 - La fonction infostation (*tch:auto/debug/infostation*) met désormais à jour automatiquement les noms de terminus de chaque ligne, ainsi que les noms de station de la station actuelle et des prochaines stations de chaque ligne. (La fonction *tch:itineraire/_init*) a été modifiée pour permettre de générer les combinaisons nom de station + couleur de ligne.)

## [Version de développement - Mise à jour du hgive des PID Métro/Câble]	(3f1e99e3)	- 2023-02-25
### Modifications :
 - La fonction *tch:tag/terminus_pc* peut désormais être appelée par une entité n'ayant pas d'ID line, en spécifiant manuellement la variable **#tch idTerminus**.
 - Les fonctions de *tch:give/siel/pid/* détectent désormais automatiquement le nom du terminus dans le système d'itinéraire, et affichent donc toujours des données à jour dudit système (plus besoin d'update manuellement le hgive PID à part pour ajouter de nouvelles lignes).

## [Version de développement - Ajout à l'itinéraire de Grenat–Port et des timings du RER A2/M3]	(652708ab)	- 2023-02-25
### Modifications :
 - Le système d'itinéraire (*tch:itineraire/_init*) comprend désormais la station "Grenat–Port", au nord de Grenat–Ville sur les lignes de métro 1 et 5a. Son ID (1102) a été ajouté à *tch:station/get_id*.
 - Les distances et durées de trajet de la branche A2 du RER A (Le Hameau—Procyon), de l'extension Sud du métro 3 (Villonne—Tolbrok) et des câbles A et B dans le système d'itinéraire correspondent désormais à leurs valeurs réelles.
 - Ajout des noms définitifs des stations Sablons–Brimiel (XXRERAx00), Gléry (XXVillageBR), Touissy (XXVillageBC), La Béhue (XXVillageBU), Malarân (XXMetro3xA), Tilia (XXVillageCN), Montagne du Destin (MontagneGigantesque) dans les fichiers *tch:station/get_id*, *tch:auto/get_id_line*, *tch:itineraire/_init* et *tch:metro/spawner/spawn/nom_voiture*.

## [Version de développement - Mise à jour des timings de la ligne 4 dans itineraire]	(72f1a329)	- 2023-02-22
### Modifications :
 - Les distances et durées des connexions du nouveau tracé de la ligne 4 dans le système d'itinéraire (*tch:itineraire/_init*) ont désormais les valeurs réelles (mesurées ingame).
 - Duplication des lignes dédiées au métro du fichier *tch:auto/get_id_line*, pour permettre de poser des spawners métro sans devoir se demander de quelle armor stand NumLigne ils sont le plus proches ; les spawners métro pourront désormais également être détectés avec le tag **SpawnMx** (SpawnM1, SpawnM5b...).

## [Version de développement - Correction de quelques connexions du système d'itinéraire]	(404df9b5)	- 2023-02-22
### Réparations :
 - Quelques connexions, qui avaient encore par erreur une distance de 0m et un temps de 0t, ont été corrigées (dans *tch:itineraire/_init*).

## [Version de développement - Mise à jour des timings dans le système d'itinéraire]	(d97d96ca)	- 2023-02-22
### Modifications :
 - De nombreuses connexions de *tch:itineraire/_init* ont vu leur durée changer drastiquement (elle était auparavant spécifiée en minutes TDH, qu'on a ramené **très approximativement** à une valeur en ticks Minecraft). De nombreuses autres qui n'avaient ni durée ni distance ont été calculées pour la première fois (mais ce n'est pas encore le cas des interstations en transport, dont la plupart ont une valeur "standard" assez irréaliste de 666m/1999t).
### Réparations :
 - Le terminus de la ligne 5a est correctement devenu Asvaard-Rudelieu dans *tch:auto/get_id_line*.
 - Les noms de terminus ont été corrigés dans *tch:metro/spawner/spawn/nom_voiture*.

## [Version de développement - Mise à jour des lignes de métro et de RER dans le système d'itinéraire]	(9706c1b6)	- 2023-02-21
### Modifications :
 - La fonction *tch:auto/get_id_line* permet désormais de détecter les lignes de métro 9, 10, 11 et 12. Elle intègre également les nouveaux terminus du RER A, et de différentes lignes de métro dont le tracé a subi une extension.
 - L'initialisation du système d'itinéraire (*tch:itineraire/_init*) intègre désormais les nouvelles stations desservies par les modes ferrés (les bateaux sont encore TODO) : les métros 9/10/11/12, les extensions des lignes 2/3/4/5a/8 (et les stations supprimées ou tracés modifiés, pour les lignes 4, 7 et RER A).
 - Les lignes peuvent désormais être sélectionnées 24h/24 par la recherche d'itinéraire, même si elles sont fermées : la fonction responsable de désactiver les lignes hors-service n'est plus appelée dans *tch:itineraire/calcul*. À voir si cette modification sera temporaire ou restera définitivement ; il faut dire qu'en l'état, comme cette désactivation est faite sur la base de l'horaire *actuel* (alors qu'une ligne empruntée dans 3h aura peut-être eu le temps d'ouvrir... ou de fermer!), cela empêche plus d'obtenir des itinéraires pertinents qu'autre chose.
 - Le rayon de détection des nœuds proches par le système d'itinéraire (dans *tch:itineraire/calcul/voisins/trouver*) a désormais une portée de 18 blocs, contre 11 précédemment ; certains des nouveaux interstations du RER A n'étaient pas correctement détectés avec l'ancien maximum.
 - Les messages de debug de la fonction *tch:itineraire/calcul/voisins/trouver* affichent désormais également l'ID de ligne du nœud actuel, pour aider à différencier entre les quais des différentes lignes d'une même station (qui, pour des raisons d'affichage du SISVE, ont uniquement comme nom celui de leur station).
 - L'interface permettant de choisir une ligne TCH à configurer (*tch:ligne/_choix_ligne*) comprend désormais les PC des lignes de métro 9 à 12.
 - L'initialisation des lignes (*tch:ligne/init*) initialise désormais les PC des lignes de métro 9 à 12.
 - La fonction assignant aux voitures du RER A un CustomName correspondant à sa direction (*tch:rer/spawner/spawn/voiture/sauvegarder_nom*) intègre désormais les deux nouveaux terminus des branches A3 et A4.
 - Toutes les nouvelles stations (y compris celles n'étant desservies que par des lignes de bateau, et y compris les nouvelles villes n'ayant pas encore de station prévue) se sont vu assigner des ID station, définis comme les autres dans la fonction *tch:station/get_id*.

## [Version de développement - Fix d'un predicate de tdh-climate-s11]	(074b3ffe)	- 2022-09-15
### Réparations :
 - Une virgule de trop a été supprimée dans le predicate de **tdh-climate-s11** `tdh:biome/neige_248`.

## [Version de développement - Predicates de précipitations saisonniers (#143)]	(dde27e09)	- 2022-09-14
### Ajouts :
 - Des predicates de **tdh-climate-s01** à **s12**, `tdh:biome/precipitations_1` à `precipitations_5`, permettent de checker l'humidité du biome situé à la position où ils sont appelés. C'est une solution un peu foireuse mais il n'est pas possible de récupérer la "vraie" valeur d'humidité via une commande similaire à `/data`, donc on se débrouille comme on peut.
 - Un nouveau block tag de **tdh-climate**, `#tdh:snow_replaceable`, définit tous les blocs pouvant être remplacés par de la neige lorsqu'elle tombe.
### Modifications :
 - Dans la fonction de **tdh-climate** `tdh:meteo/effects/neige/blocs/test_flora`, on peut désormais remplacer avec succès une plante si un des blocs attenants *en diagonale* contient de la neige, et plus seulement s'il est directement adjacent.
 - La neige ne monte plus systématiquement lorsqu'on teste une position ; on a désormais une chance de la faire monter qui dépend de la valeur d'humidité/précipitations du biome (20% à 1, 40% à 2, etc jusqu'à 100% à 5/5 d'humidité). La fonction de **tdh-climate** qui la teste est `tdh:meteo/effects/neige/blocs/test_humidite`.
### Réparations :
 - Une virgule de trop a été supprimée dans le predicate de **tdh-climate-s10** `tdh:biome/desert`.

## [Version de développement - Predicates de biome saisonniers (#143)]	(a1ff5291)	- 2022-09-01
### Ajouts :
 - Plusieurs predicates de biome de TDH (`tdh:biome/desert`, `glace`, `neige_XXX`, `neige_plus_haut`, `neigeux` et `plaine_sans_neige`) sont désormais redéfinis dans chaque datapack saisonnier **tdh-climate-s01** à **s12**, pour permettre aux effets météo et aux bruitages d'oiseaux et d'insectes de rester cohérents au fil des saisons. (#143)
### Modifications :
 - La fonction d'enregistrement du biomeType (`tdh:player/scores/biome` de **tdh-player**) ne définit plus le type de biome à "enneigé" si on est dans un biome froid où il n'y a pas de précipitations (c'est le cas de certains biomes considérés comme "chauds" en hiver/saison sèche, qui matchent à la fois le predicate `tdh:biome/desert` et `tdh:biome/neige`).

## [Version de développement - Ajout des saisons (#143)]	(ba7ca98e)	- 2022-08-31
### Ajouts :
 - 12 nouveaux datapacks contenant des définitions de biomes (`data/minecraft/worldgen/biome/`) ont été ajoutés, et sont nommés **tdh-climate-s01** à **s12**. Ils correspondent grosso modo aux 12 mois de l'année, mais sont alignés sur les solstices et équinoxes, et commencent/terminent donc autour du 21 de chaque mois. Chaque biome voit ses propriétés (température/humidité) varier au cours de l'année, la neige tombe plus bas en hiver qu'en été, et il y a un certain retard sur les saisons astronomiques qui varie en fonction du biome et s'inspire de biomes similaires IRL. Les définitions des couleurs des biomes (de l'eau/des feuillages) n'ont pas encore été modifiées, et sont identiques au jeu de base et tout au long de l'année. (#143)
 - Ces 12 datapacks sont automatiquement activés et désactivés par le pack **tdh-climate**, dont la fonction *tdh:meteo/on_load* a été modifiée pour désactiver automatiquement tous les datapacks lors d'un reload ou reboot (car Minecraft active tous les datapacks du dossier `datapacks` par défaut à chaque reboot...).
 - En plus du reload, une fonction (`tdh:meteo/saison/test_jour`) s'exécute tous les jours à 4h du matin pour vérifier s'il faut changer de datapack saisonnier.
### Modifications :
 - Il y a désormais deux fois moins de chances qu'un sapling tombé d'un arbre ne replante un autre sapling (30% -> 15% ; les pourcentages de chances vacants ont été redistribués à l'herbe).
 - Il n'est plus possible pour un sapling de se planter de cette manière s'il est adjacent (horizontalement ou en diagonale) à un bloc solide ; il sera là aussi remplacé par un bloc d'herbe.

## [Version de développement - Ajout de la filtreuse d'objets électrique]	(da7687d3)	- 2022-08-29
### Ajouts :
 - Une loot table a été ajoutée au dropper pour permettre de conserver son tag Lock lors de sa destruction.
 - Une *trieuse électrique* a été ajoutée à **tdh-crafting**. Cette machine permet, comme son nom l'indique, de trier des objets et de les placer dans différents conteneurs autour d'elle. Elle fonctionne de la manière suivante :
	- Toutes les 10 secondes, chaque trieuse vérifie si elle contient des items (il est impossible de l'ouvrir "manuellement", mais on peut simplement la remplir en se servant d'un hopper situé *au-dessus d'elle* et pointant vers le bas).
	- Si elle contient au moins un item, elle vérifie les filtres dans l'ordre suivant : *nord*, *est*, *sud*, *ouest*. Le premier filtre qui contient cet item le "consomme" et l'ajoute à une liste spécifique à cette orientation (`data.insertion.nord`, `est`, `sud` ou `ouest`).
	- Tous les items qui n'ont passé aucun des 4 filtres directionnels sont placés dans une cinquième liste, `data.insertion.bas`.
	- La trieuse tente ensuite d'insérer chaque liste d'items dans le conteneur situé dans la direction correspondante.
	- Tous les items qui n'ont pas pu être insérés (manque de place ou absence de conteneur) sont replacés dans l'inventaire de la filtreuse d'objets.
 - Pour ajouter ou retirer des items à filtrer, il suffit de faire un clic droit sur la filtreuse d'objets en tenant l'item qu'on veut ajouter ou retirer. Le filtre à modifier est choisi selon la direction du joueur, et non selon la face de la filtreuse sur laquelle on clique ; cela permet de modifier le filtre même si on a placé un bloc solide (par exemple un tonneau) dans cette direction. Par exemple, pour modifier les filtres *est*, le joueur doit avoir une rotation horizontale comprise entre +45 et +135° (ce qui signifie qu'il regarde vers l'*ouest*, donc dans la direction opposée au filtre).
 - Il est possible de réinitialiser un filtre en faisant un clic droit sur la filtreuse dans la direction correspondante, sans tenir d'objet dans la main. Pour réinitialiser entièrement la filtreuse, il suffit de faire un clic droit dessus en la regardant par le dessus (à la verticale).
### Réparations :
 - Le commentaire de documentation de la fonction `tdh:conteneur/inserer_items` précise désormais qu'il faut également spécifier le tag `Count`.

## [Version de développement - Fonction d'ajout automatique d'items à un conteneur]	(8b8cfa6b)	- 2022-08-28
### Ajouts :
 - La fonction `tdh:conteneur/trouver_item_taille_stack` est similaire à la fonction existante `tdh:conteneur/trouver_item`, mais accepte deux arguments (`#conteneurStack min` et `max`) qui définissent la taille minimum/maximum du stack à rechercher. Par exemple, avec min=1 et max=63, on cherche n'importe quel stack ayant moins de 64 items.
 - La fonction `tdh:conteneur/inserer_items` permet d'insérer une liste d'items dans un conteneur. Le seul paramètre est à spécifier dans le storage `tdh:conteneur recherche.items`, et chaque item sera ajouté séquentiellement jusqu'à tomber sur un item ne pouvant pas être ajouté. La fonction renverra 0 ou plus si elle s'exécute avec succès, et une valeur inférieure à 0 si elle échoue (mais dans ce cas, elle pourra avoir tout de même déjà ajouté des éléments au conteneur). Elle renvoie dans `tdh:conteneur resultat` la liste des items *qu'elle n'a pas réussi à ajouter*, donc une liste vide si la fonction réussit.

## [Version de développement - Ajout de l'omelette]	(78cf1903)	- 2022-08-27
### Ajouts :
 - Une recette d'omelette a été ajoutée à `tdh:craft/init/recettes/cuisiniere`. En attendant d'avoir implémenté toutes les machines permettant de transformer le lait, elle se crafte uniquement avec des œufs.

## [Version de développement - Code du désenchanteur plus générique]	(1c196b7c)	- 2022-08-26
### Modifications :
 - Le désenchanteur électrique utilise désormais pour ses tests de conteneur les fonctions génériques de *tdh:conteneur/*. Plusieurs fonctions devenues inutiles ont été supprimées.

## [Version de développement - Ajout de l'enchanteur électrique]	(df06916e)	- 2022-08-26
### Ajouts : 
 - Un enchanteur électrique a été implémenté (`tdh:craft/electricite/enchanteur/`) et une recette pour le crafter, très similaire à celle du désenchanteur, a été ajoutée dans `tdh:craft/init/recettes/atelier_ameliore`. Il doit lui aussi être posé sur un conteneur quelconque, contenant en ordre séquentiel des objets à enchanter et leurs livres enchantés (dans l'ordre item -> livre, dans des slots d'inventaire consécutifs, pour éviter tout accident). Un clic droit toggle le statut de la machine (il lance l'enchantement si elle est désactivée, et le stoppe si elle est activée).
 - Deux advancements ont été ajoutés dans `tdh:craft/electricite/enchanteur/` pour détecter les interactions des joueurs avec les enchanteurs.
 - De nombreuses fonctions génériques ont été ajoutées dans `tdh:conteneur/` et permettent de manipuler les conteneurs depuis des fonctions de manière un peu moins brainfuckesque.
   - La fonction `trouver_slot_libre` renvoie dans `#conteneur temp` l'ID du premier slot libre du conteneur situé dans le bloc actuel, et un nombre négatif si elle n'en trouve pas (-1 s'il n'y a pas de slot et -10 s'il n'y a pas de conteneur).
   - La fonction `trouver_slot_libre_apres` fonctionne de la même manière, mais attend un argument (`#conteneur min`), qui est l'ID de slot minimum que l'on désire trouver.
   - Les fonctions `trouver_slots_libres` et `trouver_slots_libres_consecutifs` permettent d'enchaîner plusieurs des fonctions précédentes pour trouver une suite de slots libres, consécutifs ou non. Elles attendent un argument scoreboard, `#conteneur nombre`, qui spécifie le nombre de slots libres à trouver. Le résultat est fourni dans le storage sous la forme d'une liste de slots (dans `tdh:conteneur resultat`, les éléments de la liste sont de la forme `{Slot:0b}`), et on retourne aussi `#conteneur temp` selon les mêmes règles que les fonctions précédentes.
   - La fonction `trouver_item` permet de trouver un item spécifique dans un conteneur. Elle attend un argument dans le storage, `tdh:conteneur recherche.item`, qui doit respecter la structure d'un item avec les tags `id` et éventuellement `tag`. Elle renvoie à la fois l'ID du slot (dans `#conteneur temp`, ou un nombre négatif si elle ne le trouve pas) et le slot entier dans le storage `tdh:conteneur resultat`.
   Toutes ces fonctions conservent également dans leur storage le contenu complet du conteneur (dans `tdh:conteneur contenu`), avec le slot trouvé (ou le premier slot suivant le slot trouvé) en position 0.
### Réparations :
 - Le renard drop désormais bien de la viande de renard (au lieu de viande de chèvre).
### Suppressions :
 - Une fonction non utilisée du désenchanteur, `tdh:craft/electricite/desenchanteur/particule`, a été supprimée.

## [Version de développement - Ajout du désenchanteur électrique]	(84fa8c5c)	- 2022-08-22
### Ajouts :
 - Un désenchanteur électrique a été implémenté (*tdh:craft/electricite/desenchanteur/*) et une recette pour le crafter ajoutée dans *tdh:craft/init/recettes/atelier_ameliore*. Il doit être posé au-dessus d'un conteneur quelconque contenant des livres non enchantés, et à proximité d'un générateur alimenté en énergie ; une fois ces conditions réunies, il suffit de cliquer droit avec un item enchanté pour ajouter ce dernier à la file d'attente des items à désenchanter. Lorsque l'opération sera terminée, l'objet désenchanté et le livre nouvellement enchanté seront placés dans le conteneur.
 - Deux advancements ont été ajoutés dans *tdh:craft/electricite/desenchanteur* pour détecter les interactions des joueurs avec les désenchanteurs.
 - Une loot table a été ajoutée au brewing stand pour permettre de conserver son tag Lock lors de sa destruction.
 - De nombreux item tags ont été ajoutés : tous les types d'outils (*tdh:craft/outils/*) et d'armures (*tdh:craft/armures/*) classés par matériau, ainsi que les tags *tdh:craft/enchantable/5*, *10*, *15* et *25* qui classent tous ces objets en fonction de leur enchantabilité (cette statistique ne change rien à l'opération de désenchantement pour l'instant, mais est tout de même prise en compte et enregistrée par le code du désenchanteur).
 - Trois nouveaux block tags (*tdh:conteneur/5*, *9* et *27*) permettent de stocker tous les types de blocs pouvant contenir des slots d'inventaire, selon leur capacité maximale (par exemple `5` contient le hopper).
 - Une nouvelle fonction, *tdh:craft/electricite/trouver_generateur*, permet de trouver le générateur le plus proche de la machine qui l'exécute et de lui donner un tag "GenerateurValide". Elle cherche dans un rayon de 7 blocs autour de la machine les générateurs, et répète l'opération récursivement pour chaque entité "Répéteur électrique" (pas encore implémentée) des alentours jusqu'à trouver un générateur ou épuiser tous les répéteurs disponibles.
### Modifications :
 - Le générateur électrique a été renommé "Générateur électrique thermique", et possède désormais à la fois le tag `Generateur` et le tag `GenerateurThermique`, pour permettre à d'autres types de générateur d'être sélectionnés par le tag `Generateur`.
 - La génération d'une machine vérifie désormais systématiquement s'il existe déjà une machine à cet emplacement, et annule l'opération si tel est le cas. Si la création d'une machine est malgré tout forcée à cette position, les machines précédentes subsistant à cet emplacement sont préalablement détruites.
 - La valeur d'un combustible en unités d'énergie a été augmentée (elle était désormais comptée en nombre d'items craftables avec, soit 8 pour un charbon ; elle est désormais comptée en ticks du système électrique, soit (avec 4 ticks par seconde) 320 pour un charbon).
 - La fonction *tdh:craft/electricite/tick* du commit précédent a été renommée *tick_active*, puisqu'on a ajouté une fonction *tick_inactive* tickant les machines électriques désactivées à un rythme beaucoup plus faible, pour vérifier si elles doivent être réactivées.
### Réparations :
 - Il n'est désormais plus possible d'activer plusieurs générateurs d'affilée (ou plusieurs fois le même générateur) lors des itérations successives de la commande *tdh:craft/electricite/generateur/remplissage* dans *tdh:craft/electricite/generateur/test_joueur*.

## [Version de développement - Ajout du générateur électrique]	(de51dea3)	- 2022-08-21
### Ajouts :
 - Un générateur électrique a été ajouté à **tdh-crafting** dans *tdh:craft/electricite/generateur/*. Il accepte pour l'instant la plupart des types de combustible du four du jeu de base (sauf divers objets faits à base de bois ; il ne prend parmi ceux-ci que les troncs, les planches, les dalles et les escaliers). Une recette pour le crafter a été ajoutée à l'atelier amélioré.
 - Plusieurs item tags ont été ajoutés (dans **tdh-crafting**, *#tdh:craft/combustible/1*, *2*, *6*, *8*, *12*, *20*, *80* et *100*) pour contenir tous les items pouvant servir de combustible, avec des valeurs identiques à Minecraft vanilla. Les (très nombreux) items fournissant 1.5 cuissons dans le jeu de base ont reçu soit 2 cuissons (les troncs d'arbre) soit 1 (tous les autres).
 - Des recettes pour crafter une turbine et un électroaimant ont été ajoutées à l'atelier amélioré. Ces deux items custom sont requis pour la recette du générateur électrique.
 - Une loot table custom a été ajoutée à la *blast_furnace* dans **tdh-crafting** pour qu'elle copie son tag "Lock", en plus de son nom, lorsqu'elle est détruite. Cela permet de récupérer tels quels les générateurs électriques TDH.
### Modifications :
 - La réinitialisation des scores et advancements dans divers ticks du système **tdh-crafting** a été déplacée dans les sous-fonctions appropriées, pour n'être exécutées que pour les joueurs qui remplissaient la condition et non pour tout le monde tout le temps.

## [Version de développement - Non-déstackage des torches tenues en main]	(450768e6)	- 2022-08-20
### Ajouts :
 - Un nouvel item modifier de **tdh-rp**, *tdh:objet_dynamique/copier_nombre*, permet d'assigner à un stack le nombre d'items contenu dans la variable #joueurObjet nombre, pour permettre de faire évoluer les torches en changeant l'item (allumé -> éteint) sans changer leur nombre.
### Modifications :
 - Les torches dynamiques ne sont plus déstackées automatiquement, et peuvent être tenues en main quel que soit leur nombre. En revanche, elles ont proportionnellement moins de chances de se consumer en fonction du nombre contenu dans le stack (2 torches se consument 2 fois moins vite qu'une torche, 3 torches 3 fois moins vite, etc).
 - Les torches droppées au sol en tant qu'items sont périodiquement remplacées par des torches TDH.
 - L'item modifier de **tdh-rp** *tdh:objet_dynamique/torche* copie désormais le NBT depuis un storage au lieu de le hardcoder.
 - Diverses initialisations de variables liées aux torches TDH ont été déplacées de **tdh-core** à **tdh-rp**.
 - Une partie des initialisations de recettes de l'atelier amélioré (tables et chaises) ont été déplacées de **tdh-crafting** à **tdh-rp**, ainsi que le code d'update du mobilier TDH ajouté dans le commit précédent.

## [Version de développement - Ajout des recettes de tables et de chaises]	(692d8953)	- 2022-08-20
### Ajouts :
 - Des recettes de tables et de chaises ont été ajoutées à l'atelier amélioré.
 - Un système de placement assisté des tables et des chaises a été mis en place. Au lieu de give des items override divers et variés du RP, on donne simplement au joueur une item frame qu'il peut placer et qui contient automatiquement le bon modèle avec le bon CustomModelData. L'advancement *tdh:craft/mobilier/pose* détecte le placement d'une de ces item frames, et la fonction *tdh:craft/mobilier/test_joueurs* détecte autour des joueurs qui trigger l'advancement quels mobiliers viennent d'être placés. Une item frame "MobilierTDH" est automatiquement doublée d'un marqueur "MarqueurMobilierTDH", qui permet de détecter aisément les modifications apportées par le joueur (rotation/destruction) et d'y réagir en conséquence. L'item frame ne droppe jamais son contenu, c'est le marqueur qui s'occupe de dropper l'item approprié (pour permettre de le reposer ultérieurement) puis de détruire l'item frame et lui-même.
### Modifications :
 - L'initialisation de **tdh-crafting** (*tdh:craft/init*) a été séparée en plusieurs sous-fonctions pour chacun des noeuds principaux du storage tdh:craft.

## [Version de développement - Ajout de la table à sandwiches]	(adb62ddb)	- 2022-08-20
### Ajouts :
 - La table à sandwich a été implémentée et permet de crafter les recettes de sandwich ajoutées dans le commit précédent.
 - De nouvelles recettes de sandwich ont été ajoutées et permettent d'utiliser diverses viandes cuites custom ajoutées dans le commit précédent. Il n'est pas encore possible de cuire celles-ci manuellement (à part dans un four standard, qui les transformera en un item "normal" sans NBT...) mais on y travaille.
### Modifications :
 - Un ingrédient inclus dans une recette qui ne comprend *pas* le tag `tag` refuse désormais les items custom du RP overridant cet item.
### Réparations :
 - Correction d'une erreur dans le code de spawn des cuisinières qui n'assignait pas correctement leur rotation et menait à leur destruction si elles étaient orientées autrement que vers le sud.

## [Version de développement - Ajout de la cuisinière et de nouvelles recettes]	(3092f194)	- 2022-08-19
### Ajouts :
 - De nombreuses loot tables ont été ajoutées à **tdh-crafting** ; elles overrident les loot tables par défaut de nombreuses entités du jeu de base pour leur ajouter des drops de viande. Les viandes en question ont des CustomModelData définis, mais n'existent pas encore dans le RP.
 - La cuisinière a été ajoutée comme machine TDH : elle permet pour l'instant de fabriquer quelques tartes (baies, pommes, ou sucre).
 - Des recettes de sandwich ont été ajoutées au storage *tdh:craft* dans *tdh:craft/init*, mais la table à sandwich servant à les crafter n'est pas encore implémentée.
 - Les machines TDH peuvent désormais consommer du combustible, et plusieurs fonctions génériques ont été ajoutées pour faire des tests de présence de combustible (*tdh:craft/machine/test_combustible*). C'est pour l'instant le cas de la cuisinière, qui a pour l'instant les mêmes valeurs de durée de combustion que Minecraft pour les principaux combustibles utilisés (lava bucket, coal, charcoal, coal block, dried kelp et blaze rod).
### Modifications :
 - Diverses méthodes implémentées dans *tdh:craft/machine/atelier_ameliore/* ont été déplacées dans le dossier parent *tdh:craft/machine/*, car elles étaient suffisamment génériques pour cela (c'est le cas de *test_recette* et diverses méthodes liées à celle-ci). Plusieurs morceaux de fonctions spécifiques à *atelier_ameliore* ont été déplacés dans leurs parents génériques.
### Réparations :
 - Les fonctions qui manquaient dans *tdh:craft/detruire_machine/invoquer_item/* ont été ajoutées, et les predicates de *tdh:craft/machine/* ont été corrigés pour ne pas matcher les entités MachineTDH désactivées (donc sans NBT "contenu").

## [Version de développement - Ajout de l'atelier amélioré]	(61431219)	- 2022-08-18
### Ajouts :
 - L'atelier amélioré se construit en posant un atelier sur un dispenser. Il permet de crafter des recettes custom avec du NBT (contrairement aux recettes de base de Minecraft), et servira de base pour améliorer le système de crafting avec de nouvelles machines et recettes à base d'items custom.
 - Pas mal de messages de confirmation, dont certains pouvant être considérés comme intempestifs, s'affichent actuellement lors de l'utilisation de l'atelier amélioré, pour vérifier que tout fonctionne bien. La plupart ont vocation à être retirés à terme.
 - Les machines sont symbolisées par des entités "marker". Elles sont générées conditionnellement lorsqu'un joueur pose certains blocs (actuellement on checke table de craft et dispenser), et se détruisent automatiquement si leurs blocs ne correspondent plus à leurs prérequis (une vérification est faite soit lorsqu'un joueur en survie ou aventure les détruit, soit lorsqu'un joueur essaie de les activer, soit automatiquement et une fois toutes les 15 secondes pour toutes les machines chargées).
 - Pour crafter des objets avec un atelier amélioré (mais le fonctionnement sera similaire pour les autres machines), il suffit de composer le bon schéma dans le dispenser, puis d'utiliser l'atelier. La recette sera exécutée 4 fois par seconde tant que le joueur ne s'éloigne pas de la machine et qu'il reste des ingrédients dans le dispenser.
 - Pour l'instant la seule recette disponible est celle du bundle (qui a été retirée en vanilla) ; mais il est d'ores et déjà très facile d'ajouter des recettes en modifiant le storage *tdh:craft recettes.atelier_ameliore* (s'inspirer de l'élément existant pour configurer les suivants).

## [Version de développement - Recette de torches moins coûteuse]	(95ae7c8a)	- 2022-08-16
### Modifications :
 - La recette des torches custom de **tdh-rp** (*minecraft:recipes/torch.json*) donne désormais 4 torches plutôt qu'une seule, comme dans le jeu vanilla.

## [Version de développement - Suppression des réveils nocturnes]	(4e722614)	- 2022-07-30
### Modifications :
 - La gamerule *playersSleepingPercentage* ayant été ajoutée à Minecraft, l'initialisation de **tdh-time** (*tdh:time/init/variables*) définit désormais cette variable à 150% pour empêcher les joueurs de se relever la nuit (puisqu'on ne la skip pas avec le datapack, on l'accélère simplement).

## [Version de développement - Amélioration des scénarios mensuels de tdh-climate]	(b2f0990a)	- 2022-07-30
### Modifications :
 - Les scénarios climatiques mensuels de **tdh-climate** (dans *tdh:meteo/init/storage/mois*) sont désormais plus réalistes (pluies plus fréquentes l'hiver, orages uniquement autour de l'été).

## [Version de développement - Correction de l'électrocution TCH]	(8a0b9dd9)	- 2022-07-29
### Réparations :
 - L'advancement de détection des rails de **tdh-tch** (*tdh:bloc/rail*) qui n'était plus fonctionnel en 1.18.2 a été réparé, et détecte également les rails activateurs.
 - Les joueurs qui marchent sur des rails en creative ou spectateur ne sont plus électrocutés lorsqu'ils repassent en survie ou aventure.

## [Version de développement - Modernisation de tdh-rp-sound]	(e7f43b93)	- 2022-07-29
### Ajouts :
 - Deux nouveaux block tags de **tdh-rp-sound**, *#tdh:base_insecte* et *base_oiseau*, stockent séparément tous les blocs sur lesquels peuvent spawner des (sons d')insectes ou des (sons d')oiseaux.
### Modifications :
 - Dans **tdh-core**, l'initialisation du panneau "TexteTemporaire" lui donne également le tag "SpawnMonde".
 - Le prédicat *tdh:biome/glace* de **tdh-core** matche désormais également le biome *frozen_ocean*.
 - Les tags *#tdh:flora*, *passable* et *tree* de **tdh-core** ont été modifiés pour prendre en compte de nombreuses plantes ajoutées par les dernières versions de Minecraft. Le tag *#tdh:ground* a de son côté été modifié pour inclure tous les nouveaux blocs souterrains.
 - Les sons de craquement de glace aléatoires ne tickent plus chaque tick (avec le reste du *tdh:sound/ambient/tick* de **tdh-rp-sound**) mais toutes les 2 secondes. Leur probabilité a été augmentée pour compenser. Ils sont désormais dans un sous-dossier *tdh:sound/ambient/iceberg/* au lieu de tout concentrer dans la fonction *tdh:sound/ambient/iceberg_random*.
 - Le type de biome 9 (*ice_spikes*) a été retiré des valeurs possibles dans *tdh:sound*.
 - Toutes les entités temporaires invoquées par les systèmes *tdh:sound/insecte/* et *tdh:sound/oiseau/* sont désormais des markers plutôt que des items/item frames.
 - Le bruit de la pluie est désormais totalement inaudible lorsqu'on est à moins de 34 d'altitude.

## [Version de développement - Modernisation de tdh-rp]	(05dec04e)	- 2022-07-29
### Ajouts :
 - Plusieurs item modifiers ont été ajoutés à **tdh-rp** dans *tdh:objet_dynamique/* pour contribuer à plusieurs fonctions du code des objets dynamiques (modification de l'ID dynamique et du CustomModelData, séparation en 2 stacks d'un stack comprenant plus d'un item...).
### Modifications :
 - Une partie du code des objets dynamiques (l'incrémentation de l'ID et le changement du CustomModelData de l'item dynamique tenu par le joueur) a été rendue générique (*tdh:player/objets/objet/incrementer* et *objets_main_gauche/objet/incrementer*) et remplace les nombreuses fonctions replaceitem obligatoires en 1.16 et inférieur.
 - Le système de filtrage des torches (*tdh:player/objets/objet/torche/remplacer/*) a été rendu plus générique et utilise moins de sélecteurs : il copie désormais l'inventaire dans un storage pour éviter de tester séparément chaque slot. Les deux fonctions *tdh:player/objets/objet/torche/remplacer/toutes* et *donner* ne sont plus utilisées puisque tout passe par le système de filtration, dans le but de toujours laisser les torches dans le même slot d'inventaire.
 - Le code des objets main gauche s'exécute désormais au même tick que celui des objets (1 fois toutes les 3 secondes).

## [Version de développement - Bugfix du rounding d'updatemaps]	(4e7f76bf)	- 2022-07-28
### Réparations :
 - Correction de l'arrondi de *cgc:cartographie/debut* qui prenait toujours une map supplémentaire autour de nous pour peu qu'on ne tombe pas pile sur le centre d'une carte. Il prend désormais toujours le centre de carte le plus proche de notre input.

## [Version de développement - Modernisation de tdh-player (#327)]	(3aa1fd0a)	- 2022-07-27
### Ajouts :
 - Ajout de 3 prédicats *tdh:is_sprinting*, *tdh:is_swimming* et *tdh:is_sneaking* dans **tdh-core**. Ils permettent, appliqués à une entité (probablement un joueur) de tester s'il est en train de sprinter, nager ou s'il est accroupi.
 - Ajout de nombreux prédicats (dans **tdh-core**, *tdh:biome/*) permettant de tester le type de biome actuel. Ils sont désormais organisés par catégorie pour limiter le nombre de conditions ; par exemple, en étant dans une taïga enneigée, on passera à la fois le prédicat *tdh:biome/foret* et *tdh:biome/neige_0*. Les prédicats *neige_X* sont un peu particuliers : ils ne réussissent que si on est dans un biome où il neige au-dessus de X blocs, **si on l'exécute au-dessus de X blocs d'altitude**. Le prédicat *neige* regroupe tous les prédicats *neige_X* et passe si on est actuellement à un endroit où il peut neiger.
 - Ajout de nombreux tags de biome (dans **tdh-core**, *tdh:tags/worldgen/biome/*) qui permettront (on l'espère) dans une future version de tester avec moins de conditions les prédicats listés précédemment. Pour l'instant les prédicats sont de type *alternative* avec un sous-prédicat pour chaque biome, mais le jour où les tags de biome seront supportés dans les prédicats on pourra simplifier ça (et les tags seront déjà prêts).
### Modifications :
 - Plusieurs fonctions de scores du joueur de **tdh-core** ont été déplacés dans **tdh-player**, car il n'y avait pas de raison de tout garder dans core : *tdh:player/scores/vitesse*, *test_ville*, *biome*, *test_vol*.
 - La vitesse du joueur est désormais multipliée par 1.5 lorsqu'il est en train de courir. (le test se trouve dans **tdh-player**). (#327)
 - Toutes les fonctions qui utilisaient les variables générées par la fonction *tdh:player/scores/biome* (currentBiome et biomeType) appellent désormais un des prédicats de biome de **tdh-core** (*tdh:biome/*) au lieu d'utiliser lesdites variables, ce qui permet désormais de tester correctement le biome *à la position d'exécution* sans se reposer sur la présence ou l'absence d'un joueur à cet emplacement. La fonction *tdh:player/scores/biome* existe toujours, mais a réduit les checks au minimum : la variable biomeType renseigne désormais uniquement le type de précipitations actuel (1 = neige, 2 = pluie, 3 = désertique).
### Réparations :
 - Une erreur de copier-coller qui empêchait la neige de se retirer depuis l'implémentation du configurateur **tdh-climate** a été corrigée.
 - Le check des titres du joueur fonctionnent à nouveau. Ils sont désormais exécutés 1 fois par seconde au lieu de 4.
 - Un souci dans le calcul de la prochaine carte qui entraînait la non-génération de certaines cartes (dans **tdh-updatemaps**) a été corrigé.
### Suppressions :
 - L'advancement *tdh:biomeinfo* a été supprimé.

## [Version de développement - Modernisation de tdh-updatemaps (#326)]	(ffddff92)	- 2022-07-25
### Ajouts :
 - Un système de scripts au format .bat dans **tdh-updatemaps**/*map_dat_generator* permet de générer jusqu'à 4 millions de cartes au format map.dat (d'ID 1000000 à 4999999), définis comme suit :
    + 1xxxzzz = Carte n° [+XXX ; +ZZZ]
	+ 2xxxzzz = Carte n° [+XXX ; -ZZZ]
	+ 3xxxzzz = Carte n° [-XXX ; +ZZZ]
	+ 4xxxzzz = Carte n° [-XXX ; -ZZZ]
    L'origine se situe en 0,0 (par exemple la carte centrée en 128,-256 aura pour ID 2001002). Il est possible de générer une portion plus petite des cartes (par défaut 40'000 environ), voir le fichier README.txt situé dans ledit dossier.
### Modifications :
 - Grâce à la nouvelle commande *item*, le code de **tdh-updatemaps** est désormais plus générique. Il suffit de définir les 4 variables #cartographieX min et max, et #cartographieZ min et max ; ces variables exprimées en coordonnées de bloc définissent les limites de la zone à rendre. Il faut également qu'un joueur ait le tag CGCCartographie. Ensuite, lancer la fonction *cgc:cartographie/debut*, qui vérifie que toutes les variables sont correctes avant de lancer la génération. Si le joueur se déconnecte, la génération se met automatiquement en pause et reprend lorsqu'il se reconnecte. Il est toujours possible d'annuler la génération en cours en lançant la fonction *cgc:cartographie/fin*. (#326)
### Suppressions :
 - Tout l'ancien code de **tdh-updatemaps** a été supprimé, ce qui n'est pas dommage.

## [Version de développement - Conversion du pack pour Minecraft 1.18]	(db6383bd)	- 2022-07-22
### Modifications :
 - Tous les packs (*pack.mcmeta*) ont vu leur identifiant de format passer de 6 (1.16.5) à 9 (1.18.2).
### Réparations :
 - Toutes les références à *grass_path* ont été renommées *dirt_path* pour correspondre à ce changement dans Minecraft.
 - Tous les appels à *replaceitem* ont été remplacés par des fonctions *item replace* équivalentes. Il faudra néanmoins songer à faire un refactoring de nombreuses fonctions, en premier lieu les objets dynamiques (#326) pour tenir compte des nouvelles possibilités d'item replace par rapport à replaceitem.
 - Les variables scoreboard de type *minecraft.custom:minecraft.play_one_minute* ont vu ce type renommé *play_time* pour correspondre à l'évolution du nom dans Minecraft.
 - Un setblock de *cauldron* dans **tdh-specifics** a été renommé *water_cauldron* pour correspondre à ce changement dans Minecraft.

## [Version de développement - Refonte des annonces PID (#156 #153)]	(38c2b3c8)	- 2022-07-22
### Ajouts :
 - Une nouvelle fonction, *tch:tag/spawner*, permet de tagger le spawner qui correspond à l'idLine et idStation de l'entité qui l'exécute.
 - Deux nouveaux tags de type d'entité, *#tch:marqueur/quai* et *spawner*, stockent respectivement les entités pouvant posséder respectivement : un des tags Direction/NumLigne/ProchaineStation, ou un des tags SpawnCable/SpawnRER/SpawnMetro. (#153, #55)
 - Le code des annonces PID a été totalement réécrit, et la nouvelle version se trouve dans *tch:station/annonce/*. Le tick utilisé par le PID tourne à chaque tick, contrairement à celui des annonces, puisqu'il doit être testé par toutes les entités compatibles et non une seule fois globalement ; il s'agit du fichier *tch:station/annonce/tick_rapide*. Les fonctions du PID se trouvent elles dans le dossier *tch:station/annonce/pid/*.
### Modifications :
 - Le nouveau code générique du PID, précédemment ajouté dans *tch:pid/*, a été déplacé dans *tch:station/pid/* pour éviter de démultiplier les dossiers à la racine, d'autant plus que le PID se trouve forcément dans une station.
 - Les fonctions *tch:tag/quai*, *station* et *voie* utilisent désormais les tags de type d'entité *#tch:marqueur/quai*, *spawner* et *station* pour définir les entités qu'elles recherchent. (#153, #186)
 - Les playsound des annonces PID se trouvent désormais dans *tch:sound/annonce/*. Si l'entité (en principe le haut-parleur) qui l'exécute n'a pas le tag Clean, seuls les joueurs disposant du tag tchInside (donc ayant passé une entrée/sortie de station) entendront l'annonce, ce qui permet d'éviter que l'on entende lesdites annonces PID depuis la surface lorsqu'on se trouve au-dessus de la station de métro. (#275)
### Réparations :
 - Les métros devraient désormais diffuser un message PID à quai lorsqu'ils ont réservé et sont en train d'arriver en station. La ligne de code supposée le faire était présente mais n'a jamais fonctionné.
### Suppressions :
 - Les anciennes fonctions *tch:ligne/horaire/gen/message_quai* et *message_quai_fds*, qui affichaient le second message après l'annonce du PID "Un train est en approche", ont été retirées car elles faisaient doublon avec le code standalone du PID.

## [Version de développement - Refonte des annonces génériques (#156 #96 #236 #275)]	(06e17614)	- 2022-07-22
### Ajouts :
 - Le système des annonces se trouve désormais dans *tch:station/annonce/* et a été totalement réécrit. Les annonces sont générées toutes les 32 secondes par la fonction *tick_lent* (#156), et sont communes à toutes les stations, diffusées automatiquement sur les haut-parleurs appropriés selon les restrictions du message (#96). Le PID n'a pas été converti pour l'instant, et n'est pas encore fonctionnel.
 - Le tag de type d'entité *#tch:marqueur/station* a été ajouté. Il sert comme son nom l'indique à sélectionner les différents types d'entités pouvant matérialiser une station, et permettra à terme de smoother la transition entre armor stand et item frame, voire marker, pour la majorité des entités de TCH. (#153)
 - Les nouvelles fonctions *tch:id/ligne_proche* et *station_proche* permettent, exécutées par une entité, de lui donner pour score respectivement idLine/idLineMax à l'idLine/idLineMax de la NumLigne la plus proche, et idStation à l'idStation de la NomStation la plus proche.
### Modifications :
 - Les haut-parleurs (tag tchHP) récupèrent désormais automatiquement leur ID de ligne, ainsi que leur ID de station (celui de la station la plus proche, auxquels ils sont liés). Ces scores peuvent cependant toujours être définis manuellement, ils ne se réinitialiseront pas automatiquement à leur valeur "par défaut" s'ils ne valent pas 0. Pour signaler qu'un haut-parleur ne doit pas être lié à une ligne, il est toujours possible de lui donner le score idLine = -1. (#244)
 - Les annonces ont désormais des restrictions de mode de transport, stockés lors de leur diffusion dans #tchAnnonce idLine et idLineMax. Ils permettent de sélectionner soit l'ensemble des stations et des lignes (si idLine = 0), soit l'ensemble des lignes entre idLine et idLineMax (si idLine <= idLineMax), soit l'ensemble des lignes n'étant PAS entre idLine et idLineMax (si idLine > idLineMax). Toutes les définitions par annonce se trouvent avec la génération desdites annonces dans *tch:station/annonce/generique/gen_id/*. (#236)
 - Les playsound des annonces se trouvent désormais dans *tch:sound/annonce/*. Si l'entité (en principe le haut-parleur) qui l'exécute n'a pas le tag Clean, seuls les joueurs disposant du tag tchInside (donc ayant passé une entrée/sortie de station) entendront l'annonce, ce qui permet d'éviter que l'on entende lesdites annonces depuis la surface lorsqu'on se trouve au-dessus de la station de métro. (#275)
### Suppressions :
 - L'ancien système des annonces ne ticke plus (il a été retiré de *#minecraft:tick*) mais les fonctions associées sont toujours présentes.

## [Version de développement - Passagers RER invulnérables (#315)]	(ac699612)	- 2022-07-19
### Réparations :
 - Les passagers RER sont désormais invulnérables, et ne clignotent plus en rouge quand ils passent dans des barrières invisibles mal retirées ou d'autres éléments du décor. (#315)

## [Version de développement - Bugfixes RER (#63)]	(5f1f9690)	- 2022-07-19
### Réparations :
 - Correction d'un souci qui faisait qu'on était pas correctement TP à la position de notre marqueur lorsqu'on arrivait trop rapidement en station pour sortir du mode caméra "naturellement".
 - Correction d'un souci d'assignation de variable dans *tdh:rer/spawner/spawn/voiture/passagers/spawn_villageois* qui faisait qu'une grande majorité des villageois était nommée Vulcain.
 - Les passagers en mode creative ne devraient plus être placés de plus en plus haut en Y au fur et à mesure des stations (le fix est dans la fonction *tch:rer/rame/en_station/depart/ajouter_joueur/copier_donnees*).

## [Version de développement - Reload décalé (#314)]	(9ad6cb4d)	- 2022-07-19
### Ajouts :
 - Ajout de nouvelles fonctions au dossier *tdh:reload/* qui schedulent un reload dans une durée donnée. La syntaxe est toujours *tdh:reload/XXs* ou éventuellement *XXm*, respectivement pour les secondes et les minutes (par exemple *tdh:reload/20s* ou *tdh:reload/3m*).
### Modifications :
 - Les sous-fonctions de *tdh:reload/* ne font désormais plus partie de tdh-core, mais de **tdh-rp-sound** (car c'est bien ce dont il s'agit, après tout).

## [Version de développement - Optimisation du mouvement des RER (#63)]	(63229c92)	- 2022-07-18
### Modifications :
 - Les performances du code de mouvement des RER (*tch:rer/rame/mouvement*) et de toutes ses sous-fonctions ont été améliorées en séparant davantage les fonctions existantes selon l'exécutant pour limiter le nombre de sélecteurs, quitte à augmenter le nombre de fonctions. On se retrouve notamment avec de nouvelles fonctions numérotées de 1 à 32 dans */mouvement/voiture/test_trace/* et */mouvement/camera/*, qui intègrent des morceaux des anciennces fonctions */mouvement/1* à *32*. Ces dernières ont été déplacées dans */mouvement/avancer/*.
 - Les piglins et zombified piglins ont été ajoutés au tag *#tch:passager_rer*. Les joueurs sont désormais remplacés par des piglins en mode NoAI plutôt que par des items, pour éviter que lesdits items ne partent dans tous les sens quand ils sont momentanément à l'intérieur d'un bloc.
 - Par conséquent, les joueurs sont en spectateur de A à Z dès lors que le RER est en mouvement, et certaines parties du code qui les changeait de gamemode ou d'entité à spectate ont été déplacées.
 - La fonction *tch:itineraire/gen_panneau* a été déplacée à *tch:init/gen_panneau* car on en a d'ores et déjà besoin en-dehors du système d'itinéraire, donc c'est plus cohérent comme ça.
### Réparations :
 - Les armor stands du RER ont désormais le tag Marker pour désactiver toute interaction avec elles.
 - Les fonctions *tch:rer/rame/en_station/arrivee/reparer_position* et *reparer_position_joueur* testent désormais correctement la bonne altitude (ils testaient les blocs de 1 trop haut et téléportaient donc souvent dans le sol).
### Suppressions :
 - Les tags FinCamera et Corresp ne sont plus testés par la fonction *tch:rer/rame/mouvement/voiture/sauvegarder_vitesse* (devenue */voiture/aligner/sauvegarder_tags*) car il n'y en a plus besoin dans le code actuel. Ils sont désormais affichés en rouge par *tch:rer/trace/debug/* pour se souvenir de les retirer.

## [Version de développement - Réparation de bugs de l'écrasement RER (#303)]	(d4b788af)	- 2022-07-18
### Réparations :
 - Un nouveau tag donné aux joueurs qui meurent écrasés, "MortEcrase", permet d'éviter que le message de décès ne soit écrit plusieurs fois d'affilée. (#303)
 - Plusieurs fonctions qui avaient leur place dans **tdh-rp** ou **tdh-tch** ont été sorties du tick de **tdh-player** (des resets de tags et des tests de variables diverses).

## [Version de développement - Auto-détection hgive fond (#280)]	(ad8a828a)	- 2022-07-17
### Ajouts :
 - Le hgive fond (*tch:give/fond*) dispose désormais d'une auto-détection des blocs alentour. Le nom de la fonction principale à appeler depuis l'extérieur n'a pas changé (*tch:give/fond/_main*). (#280)
 - De (nombreux) nouveaux block tags ont été ajoutés pour permettre la détection des différents types de blocs proposés par hgive fond. Ils se trouvent tous dans *#tch:give/*. (#280)

## [Version de développement - Réparation config climat]	(706e4a5a)	- 2022-07-17
### Réparations :
 - Correction d'un oubli lors d'un copier-coller dans la fonction de **tdh-climate** *tdh:meteo/config/tick*, qui empêchait les variables d'être modifiées puisque leurs fonctions *_set* n'étaient pas exécutées.

## [Version de développement - Hotfix valideur Gold]	(a3bb5c25)	- 2022-07-17
### Réparations :
 - Restauration de la version précédente de *tch:station/valideur/gold/test_objet* qui avait été très bêtement modifiée par moi-même il y a quelques heures en croyant réparer quelque chose, et qui conduisait à considérer n'importe quel objet tenu en main comme un abonnement TCH invalide (en affichant donc un message d'erreur...).

## [Version de développement - Storage des abonnements TCH (#293)]	(f1e2a168)	- 2022-07-17
### Ajouts :
 - Un système d'affichage plus user-friendly des abonnements TCH enregistrés et archivés a été ajouté dans **tdh-tch**, dans *tch:abonnements/afficher/*.
 - Du code (pour l'instant dans *tch:abonnements/_init* et *gen_panneau*, mais qu'il faudra peut-être déplacer dans un coin plus générique d'un datapack plus générique que **tdh-tch** prochainement) permet d'initialiser un panneau au spawn qui sert à extraire du texte "fixé" de données dynamiques comme des noms ou des scores. On s'en sert actuellement pour enregistrer le nom des joueurs dans le fichier des abonnements.
### Modifications :
 - Le système des abonnements TCH utilise désormais un storage, tch:abonnement, au lieu de tags d'item frames. Par voie de conséquence, le système de recherche et d'actualisation des abonnements est désormais plus rapide. (#293)
 - Le système des abonnements TCH stocke désormais le nom du joueur ayant acheté un abonnement dans le storage. Il utilise pour ce faire une item frame "TexteTemporaire" toujours chargée (donc située au spawn) qui sert à récupérer au format texte un sélecteur (qu'on ne peut pas assigner tel quel).
### Réparations :
 - La fonction d'initialisation de tch (*tch:init*) est désormais bien appelée par le tag *#tdh:init* de **tdh-tch**.
 - La carte Gold est désormais correctement facturée 2 lingots de fer par l'agent TCH, au lieu de 3 (dans **tdh-dialogue** / *tdh:dialogue/answers/tch/abonnement/carte_gold/agent_tch/achat*).
 - La fonction d'initialisation de **tdh-tch**, *tch:init*, crée correctement la variable "tickValidation" utilisée par les valideurs Gold et Silver.
 - Correction d'un problème dans **tdh-time** / *tdh:time/tdh_tick* qui empêchait les function tags horaires de s'exécuter.
### Suppressions :
 - La fonction de backup de l'abonnement TCH (*tch:abonnement/backup_fichier*) a été supprimée, puisqu'on n'utilise plus d'item frame pour le stockage il y a moins de risque de le supprimer accidentellement et il n'y a pas plus de raison de faire un backup séparé pour ce storage-là que pour tous les autres.

## [Version de développement - Renommage Shikân->Le Roustiflet (#305)]	(4acda847)	- 2022-07-16
### Modifications :
 - La station "Shikân" a été renommée "Le Roustiflet" à tous les endroits applicables, à l'exception du système des annonces (*tch:auto/annonce/*) qui doit de toute façon être refait. (#305)

## [Version de développement - Couleur SISVE RER (#63)]	(6c5cc543)	- 2022-07-16
### Réparations :
 - Le SISVE RER est correctement affiché en blanc s'il fait sombre et en noir s'il fait lumineux, comme le SISVE métro.

## [Version de développement - Hotfix sauvegarder_queue du RER (#63)]	(c77b4cbc)	- 2022-07-16
### Réparations :
 - Les passagers ne sont plus envoyés à des milliers de blocs de là lorsqu'on fait une rotation en RER (on avait simplement oublié de renommer un appel à sauvegarder_queue dans *tch:rer/rame/mouvement/voiture/sauvegarder_centre*).

## [Version de développement - SISVE RER + tas de bugfixes (#63)]	(04fabf53/1b559254)	- 2022-07-16
### Modifications :
 - Le système d'itinéraire de TCH enregistre désormais le Hameau Rive Droite comme terminus de toutes les lignes ayant un ID de 2150-2199. La fonction *tch:tag/terminus_pc* a également été modifiée pour tenir compte de la nouvelle variable *idTerminusMax* qui permet d'avoir plusieurs ID line faisant terminus sur le même quai du système d'itinéraire.
 - Le tag PersistenceRequired des villageois et dereks invoqués par *tch:rer/spawner/spawn/voiture/passagers* est désormais bien défini à 0 (il se reset à 1 par défaut au moment de l'invocation lorsque c'est une créature passive).
 - Les entités de l'arrière des voitures de RER dissimulent désormais leur modèle lorsqu'on passe les spectateurs en mode caméra, et le restaurent lorsqu'on les repasse en mode inside. Les fonctions *tch:rer/rame/passager/vers_camera* et *vers_inside* ne sont plus exécutées à la position d'un passager joueur, mais filtrent elles-mêmes les passagers correspondants.
 - Le tag ArriveeStation ne sort plus automatiquement les joueurs passagers d'un RER du mode caméra ; c'est le rôle d'un nouveau tag spécifique, "FinCamera" (*tch:rer/rame/mouvement/voiture/sauvegarder_vitesse*). C'est aussi pris en compte dans le debug (*tch:rer/trace/debug/*).
 - La distance au-delà de laquelle les passagers invoqués déspawnent a été réduite à 54 blocs (dans *tch:station/passager/test_passager*).
### Réparations :
 - La réparation de la position des passagers lors de l'arrivée en gare (se remarque surtout lors de l'arrivée à Procyon) est désormais plus précise et fonctionne également avec les passagers non-joueurs.
 - Le tick joueur du SISVE (*tch:sisve/tick_joueur*) ne fait désormais plus que tagger les modes de transport contenant un joueur et devant être calculés, pour éviter que les modes de transport pouvant en contenir plusieurs ne calculent plusieurs fois leurs variables.
 - Certaines fonctions du SISVE ont vu leurs sélecteurs réparés pour préciser le type d'entité.
 - Les modes de caméra ont désormais des "fallbacks" pour éviter de déplacer la caméra à l'intérieur d'un mur. Par exemple si la caméra de côté est bouchée, on essaiera la vue du dessus, puis si celle-ci est bouchée également une vue intérieure.
 - La ligne de code qui vérifie si on se trouve dans un mur pour déplacer la caméra du RER (*tch:rer/rame/passager/tick_camera*) teste maintenant le bloc dans lequel se trouve la *tête* de l'armor stand, plutôt que ses pieds.
 - Les heures de pointe sont désormais correctement calculées par les fonctions *tch:ligne/config/duree_pointe1_set* et *duree_pointe2_set*, qui n'utilisent plus d'horaires hardcodés. Idem pour leur affichage dans les fonctions *select*.
 - Les passagers d'un RER se voient désormais appliquer l'effet "Résistance" pour éviter de crever lamentablement s'ils passent malencontreusement à travers une barrière. L'effet est retiré lors de l'arrêt en station.
 - On ne donne plus de profession aléatoire aux villageois passagers d'un RER puisqu'ils sont de toute façon amenés à la perdre très rapidement (lorsqu'ils réalisent qu'ils n'ont pas de point de travail).
 - Les sons arrivée station et alerte arrivée station du RER sont désormais joués à tous les joueurs matchant le tag approprié, au lieu de le diffuser basé sur la distance (ce qui empêchait de les entendre en mode caméra). Leur volume minimal a également été réglé à 1 pour la même raison.
 - Il n'est plus possible d'ajouter ou retirer des items "manuellement" sur les armor stands RER.
### Suppressions :
 - Les tags AlerteArriveeStation et ArriveeStation ne sont plus testés par la fonction *tch:rer/rame/mouvement/voiture/sauvegarder_vitesse* car il n'y en a plus besoin dans le code actuel du SISVE. Ils sont désormais affichés en rouge par *tch:rer/trace/debug/* pour se souvenir de les retirer.

## [Version de développement - Caméras RER (#63)]	(a9f4b1dd)	- 2022-07-11
### Ajouts :
 - Un nouveau block tag, *tch:camera_passable*, détermine tous les blocs pouvant contenir une caméra.
 - 4 nouveaux modes de caméra ont été ajoutés aux caméras RER (des vues éloignées plongeantes).
### Modifications :
 - L'altitude de repositionnement d'un passager lors de l'arrivée en station est désormais de 0.1 bloc au-dessus du sol (au lieu de 0.5 précédemment) et les passagers non-joueurs sont également inclus.
 - De nouveaux checks devraient empêcher qu'on arrive en station en étant dans une barrière invisible.
 - Un tag donné aux caméras de RER, "NoUpdate", désactive leur tick de repositionnement des joueurs s'il n'y en a aucun dans le RER actuel. Il est donné par défaut au moment du spawn, puisqu'aucun joueur ne peut apparaître avec un RER qui vient de spawner ; il est ensuite conditionnellement retiré ou donné à l'armor stand RERCamera au moment du départ d'une station.
### Réparations :
 - Une caméra RER qui se trouve à l'intérieur d'un bloc avance d'une unité vers le RER (pour tenter de se "libérer"). Le block tag *tch:camera_passable* est utilisé pour déterminer si le bloc dans lequel on se trouve est valide ou non.

## [Version de développement - Caméras RER (#63)]	(382ec99d)	- 2022-07-10
### Modifications :
 - La caméra RER est désormais une armor stand séparée, déplacée et tournée à chaque tick avec les passagers. De nouvelles fonctions (dans *tch:rer/rame/camera/*) permettent de changer régulièrement de caméra et sont exécutées par *#tdh:tick/1_8* (donc toutes les 8 secondes).
 - Les passagers sont désormais déplacés avant les voitures (pour éviter que "la plus proche" ne soit temporairement une voiture dans laquelle ils ne se trouvent pas réellement) dans *tch:rer/rame/mouvement/*.
### Réparations :
 - On a essayé une nouvelle fois de réparer l'altitude de positionnement du joueur lors de l'arrivée en station pour qu'il ne tombe pas dans le sol (dans *tch:rer/rame/en_station/arrivee/retirer_joueur*).

## [Version de développement - Réparation alignement et rotation des passagers RER (#63)]	(e031be00)	- 2022-07-09
### Modifications :
 - La rotation utilisée pour *tdh:math/sind* et *cosd* est désormais 10x la rotation en degrés (au lieu de la rotation en degrés non modifiée).
 - Les passagers invoqués par le RER ont désormais des prénoms calqués sur ceux présents dans les annonces Compose du RP.
 - Les passagers invoqués sont désormais légèrement moins nombreux en période d'heure de pointe.
### Réparations :
 - La fonction *tch:rer/rame/mouvement/aligner* déplace désormais correctement les passagers, grâce aux fonctions *tch:rer/rame/mouvement/voiture/aligner_passagers*.
 - Les soucis de rotation des passagers hors voiture de tête ont été ENFIN corrigés ; en conséquence, les passagers spawnent désormais également dans les voitures de queue.
 - Les passagers sont tp plus haut à l'arrivée en station afin d'éviter (on l'espère) tout problème de chute à travers la voie.
 - Le tag "NoAlign" est bien pris en compte et affiché par *tch:rer/trace/debug/*.
### Suppressions :
 - Les différentes versions de *tdh:math/sind* et *cosd* de **tdh-core** ont été retirées : on ne conserve qu'une seule version, pour l'instant toujours nommée 100, avec une meilleure approximation que précédemment. (puisque le multiplicateur de l'input ne change pas, ça n'avait pas de sens de conserver cette organisation)

## [Version de développement - Heure du PID (#278) et réparations debug RER]	(1a832aaf)	- 2022-07-08
### Modifications :
 - Le nouveau message de PID affiche désormais l'horaire d'arrivée en plus du temps d'attente en minutes.
 - Dans *tch:rer/trace/debug*, plusieurs traits horizontaux sont désormais affichés au même endroit que la vitesse pour chaque armor stand qui n'a pas de vitesse enregistrée. Cela permettra de rendre les tracés plus visibles quand on rendra les armor stands invisibles ou qu'on les remplacera par des markers.
### Réparations :
 - TOUJOURS en attendant de réécrire ce putain de système de merde, le tag "ourSoundSource" est correctement retiré d'un spawner câble ou RER qui n'a pas trouvé de PID et diffuse à sa propre position.
 - Les messages de debug des armor stands TraceRER (*tch:rer/trace/debug/*) sont désormais affichés plus bas pour éviter qu'ils ne passent à travers le plafond quand on est dans un tunnel, et vérifient si l'armor stand est petite ou grande pour déterminer leur position d'affichage.

## [Version de développement - Bugfix fin de service/ligne]	(ca9391ed)	- 2022-07-08
### Réparations :
 - Une ligne dont l'horaire de fin de service calculé le week-end (avec les extensions/réductions de service WE) était précisément à minuit ne faisait jamais fin de service, ce qui a été corrigé.

## [Version de développement - #63, #308, bugfix #278]	(db94c390)	- 2022-07-08
### Modifications :
 - Le SACD du PID reste verrouillé sur 00 après le spawn d'un véhicule et jusqu'à son départ du quai.
 - L'écrasement des entités par le RER a été amélioré, est plus sensible et écrase également à l'arrière du RER.
### Réparations :
 - Les barrières invisibles des RER en station devraient correctement se retirer lorsqu'ils sont détruits, et ce pour toutes les voitures (au lieu de seulement la voiture de tête comme précédemment).
 - Les vaches, cochons et moutons ont été ajoutés aux entités *#tch:rer_ecrasable*. (#308)

## [Version de développement - #63 + #278]	(92aa3cbe)	- 2022-07-08
### Ajouts :
 - Les PID fonctionnent désormais dans un code séparé (*tch:pid/*) indépendant du mode de transport employé. (#278)
 - Un système de debug des tracés de RER permet d'afficher les tags et les vitesses de toutes les armor stands SpawnRER (dans *tch:rer/trace/debug/*).
### Modifications :
 - Il est désormais possible de rendre un PID muet (n'affiche pas de texte dans le chat ni n'émet de son) en lui donnant le tag Muet.
 - Les PID devraient désormais diffuser des sons également pour les RER et les câbles (en attendant la réécriture de fond en comble de ce système d'annonces sonores...)
 - Les iron_bars ont été ajoutées au block tag *tch:passager_perdu*.
 - Un suffixe générique est désormais stocké avec les PC de ligne pour qualifier leurs véhicules. Il s'agit de "e" pour les noms féminins (donc à ce stade, uniquement "cabine" pour le câble) et d'une chaîne vide pour les noms masculins ("métro" et "train"). (**tdh-tch** / *tch:itineraire/_init*)
 - Il n'y a plus de limite à la distance à laquelle les spawners métro sont tickés, puisqu'ils ne génèrent de toute façon pas leur métro s'ils sont trop loin d'un joueur au moment du spawn.
 - De nombreuses fonctions de **tdh-tch**, en particulier concernant le calcul des horaires d'ouverture/fermeture des lignes, ne fait plus référence à des valeurs de durée hardcodées et calcule à la place les valeurs nécessaires à partir des variables de #temps (de **tdh-time**).
 - La proportion de derecks générés à l'intérieur des RER en tant que passagers a été réduite.
 - De nombreuses fonctions de *tch:ligne/incident/* utilisent désormais *tdh:random* comme générateur aléatoire.
 - Les nouveaux ID de ligne fonctionnels (RER A et câbles A/B) ont été ajoutés au vieux système d'annonce en attendant, à nouveau, de tout réécrire. Les PID de ces modes devraient donc bien diffuser les directions.
 - Les annonces sonores de PID calculent désormais correctement le nombre de minutes d'attente selon la durée actuelle du jour (en attendant de tout réécrire).
### Réparations :
 - Les passagers devraient être correctement TP sur leur marqueur lorsqu'ils ne l'atteignent pas, y compris lorsqu'ils en sont trop éloignés.
 - Le tick des lignes (*tch:ligne/tick*) prend désormais bien en compte la "vraie" durée du jour telle que définie dans la config de tdh-time, plutôt que d'utiliser des valeurs hardcodées.
 - Les spawners câble, RER et métro ont vu leur portée d'activation augmenter pour diffuser des sons y compris quand le joueur est loin (en attendant de réécrire tout le code des annonces...)

## [Version de développement - #63]	(911d3643)	- 2022-07-06
### Ajouts :
 - Des passagers sont désormais générés "naturellement" dans les RER, avec davantage de foule aux heures de pointe. (pour l'instant seulement dans les voitures de tête en attendant de débugger les rotations des passagers dans les suivantes)
 - Un système de guidage automatique des passagers en station permet à la fois de les évacuer des quais vers les sorties lorsqu'ils sortent d'un RER, et de diminuer le nombre d'entre eux qui tombe sur les voies et s'y fait écraser (en les téléportant sur le quai le plus proche s'il est possible d'en détecter un).
### Modifications :
 - La limite au-dessus de laquelle les RER n'avancent plus et s'autodétruisent est désormais de 160 de distance d'un joueur (au lieu de 120 précédemment) pour éviter des problèmes dans les très longues gares.
 - Les armor stands des RER sont désormais invisibles.
 - Les RER spawnent désormais à vitesse maximale au lieu de commencer à 0 et de devoir accélérer.
 - On n'essaie désormais plus d'assigner une destination qu'aux passagers "intelligents" (villager et wandering_trader). Ces deux-là sont donc désormais dans un tag *#tch:passager_intelligent*, lui-même inclus dans le tag *#tch:passager_rer*. L'assignation de la destination devrait également fonctionner pour les wandering_traders désormais.
 - La notification "Et vlan" (de **tdh-specifics**) a été désactivée car j'en avais plein le cul.
 - Les RER détectent les armor stands TraceRER à une distance légèrement plus élevée, pour éviter qu'ils ne "loupent" des stations quand ça lag (on verra bien si ça règle le problème du moins...)
 - Des messages de debug ont été ajoutés aux fonctions de **tdh-tch** *tch:rer/rame/mouvement/rotation_passagers* et *rotation_passager*.
 - Les RER émettent désormais le son *signal_depart* avant de démarrer.
### Réparations :
 - Les passagers sont désormais téléportés légèrement au-dessus de leur position théorique lors de l'arrivée en gare d'un RER pour éviter qu'ils tombent à travers le sol.
 - Les barrières invisibles seront désormais bien retirées lorsqu'on détruit un train situé en station.

## [Version de développement - #63]	(4de52f6a)	- 2022-07-03
### Ajouts :
 - Les RER peuvent désormais circuler sur des tracés prédéfinis. Le mouvement dans toutes les directions, la montée/descente de différentes entités (et des joueurs) en station, le spawn et le déspawn des rames sont fonctionnels, mais pas le SISVE ni la plupart des messages informatifs et annonces sur le quai. (#63)
 - Un nouvel entity tag de **tdh-tch**, *#tch:passager_rer*, liste les entités qui sont reconnues comme passagers d'un RER et peuvent être déplacés avec les voitures. Les joueurs n'en font pas partie car ils ont un code spécifique. Un autre entity tag, *#tch:rer_ecrasable*, liste les entités pouvant être écrasées par un RER.
 - Des versions de *tdh:math/cos* et *sin* fonctionnant en degrés au lieu des radians ont été ajoutées dans les dossiers *tdh:math/cosd* et *sind*.
### Modifications :
 - Le type "item_frame" est désormais spécifié explicitement dans tous les sélecteurs s'appliquant au PC d'une ligne. (#186)
 - Le code de réservation des voies (*tch:metro/voie/reservation*) a été déplacé dans *tch:voie/reservation*, et le code a été rendu suffisamment générique pour pouvoir être utilisé à la fois par les métros et par les RER.
 - Les ID de ligne du RER ont changé ; les deux derniers chiffres seront désormais toujours 00-49 dans une direction "générale" et 50-99 dans l'autre, pour permettre des checks plus aisés dans le code et en gare. Les branches A1 et A2 gardent donc l'ID 2101 et 2102, mais les directions générales Sud/Nord passent de 2112/2134 à 2100/2150.
### Réparations : 
 - La fonction *tch:metro/spawner/spawn/nom_voiture* utilise désormais bien les caractères custom au lieu des caractères unicode standard pour l'affichage des noms des lignes.
 - La fonction *tch:metro/rame/en_station/depart* vérifie désormais correctement que les chunks sont bien chargés. Ce n'était pas le cas en raison d'une erreur dans le nom de la fonction faisant la vérification.
 - Dans **tdh-core**, les fonctions *tdh:math/cos* et *sin* ne fonctionnaient que sur la moitié des intervalles (de 0 à pi pour sin, et de -pi/2 à pi/2 pour cos), car l'approximation utilisée n'était valable que sur celui-ci. Une conversion a été ajoutée et les fonctions trigonométriques ont été corrigées.
### Suppressions :
 - Le contenu de l'ancien dossier *tch:rer* basé sur des blocs (comme le câble) a été retiré pour laisser place au nouveau code.

## [1.0.0]	(9c4ece5b/1a931566)	- 2022-06-30
### Technique
#### Ajouts :
 - L'ensemble des fonctions cruciales/minimales du datapack a été déplacé dans un sous-dossier, **tdh-core** (88b54a8d), et de nouveaux packs ont été créés pour tout le reste : **tdh-time**, **tdh-climate**, **tdh-player**, **tdh-dialogue**, **tdh-tch**, **tdh-rp**, **tdh-rp-sound**, **tdh-specifics**, **tdh-porte**, **tdh-ascenseur**, **tdh-arena**, **tdh-health** et **tdh-updatemaps**. (9f2aef17/8f901aac/c03baa5c)
 - Une fonction de **tdh-core** (*tdh:on_load*) se lance automatiquement lorsque le datapack est activé pour la première fois. Elle boucle toutes les 30 secondes tant qu'aucun input n'a été détecté, et propose aux éventuels joueurs en ligne de lancer la configuration initiale de **tdh-core**. (2a60f6e7)
 - Un détecteur de niveau de lumière (*tdh/advancements/light_level*), similaire à *biomeinfo*, a été ajouté pour permettre de choisir la couleur du texte du SISVE. Les informations, chez chaque joueur, sont traitées par la fonction *tdh:player/scores/lumiere*. Pour le bon fonctionnement du système, des prédicats (*tdh/predicates/light_level/*) permettant de tester la luminosité du bloc situé juste au-dessus d'un joueur a été ajouté (sans cela, le test de luminosité renvoie toujours 0 lorsqu'on est dans un minecart, puisqu'on se trouve à l'intérieur du bloc). (dc8aa54c)
 - Un tag, **StoreDebugLog**, permet d'afficher les détails du processus de calcul des storages pour faciliter le debug. Il n'est pour l'instant activé que sur *tdh:store/day* et par conséquent *day_long*. (602219ab)
 - La fonction *tdh:item_frame/rotate* permet de faire tourner l'item frame qui l'exécute d'un angle défini. Le paramètre (nombre de tours dans le sens des aiguilles d'une montre) doit être passé via la variable **#itemFrame rotation**, et peut être négatif. (73876a96)
 - Diverses sous-fonctions ont été créées, et permettent d'omettre le paramètre #itemFrame rotation : il s'agit des fonctions *tdh:item_frame/rotate/-3*, *-2*, *-1*, *1*, *2*, *3* et *4* (c'est assez compréhensible il me semble). (73876a96)
 - La fonction *tdh:item_frame/refresh* permet de refresh l'output d'un comparateur (ou autre bloc redstone "abîmé" par les optimisations de Bukkit/Spigot) en plaçant/retirant un fil de redstone, ce qui trigger une actualisation du circuit. Il est placé à la position où la fonction est appelée. (73876a96)
 - Une nouvelle fonction a été ajoutée à la racine de chaque dossier *config*, et s'appelle toujours *prompt.mcfunction*. Elle contient un message cliquable à afficher à la personne exécutant la fonction, redirigeant vers la page d'accueil de l'interface de configuration correspondante. Un function tag, *#tdh:config/prompts* rassemble tous les prompts de tous les datapacks chargés. La liste de tous les prompts peut être affichée ingame via la fonction *tdh:_config*. (2a60f6e7)
 - Des configurateurs ont été ajoutés à **tdh-core** (*tdh:core/config*) (2a60f6e7), **tdh-climate** (*tdh:meteo/config*) (3ccfac2a).
 - De nombreux function tags ont été ajoutés dans **tdh-core** pour permettre à des fonctions de différents packs d'extension de se greffer dessus plus modulairement qu'actuellement. On a notamment le dossier *#tdh:tick/* qui permet d'appeler des fonctions à différentes fréquences (de *8* pour 8x/seconde à *1_32* pour 1x/32 secondes), le dossier *#tdh:time/* pour les fonctions à appeler chaque jour à un horaire précis (de *0h* à *22h* par intervalles de 2h), et le tag *#tdh:init* pour centraliser toutes les fonctions d'initialisation des différents datapacks. (9f2aef17/ee4d5125/c03baa5c)
 - Ajout à **tdh-core** de nouvelles fonctions mathématiques (*tdh:math/*) (c03baa5c) :
    + le dossier *pi/* définit la variable #pi temp à une approximation entière de pi (le niveau de précision dépend de la fonction : par exemple *tdh:math/pi/100* renverra 314, tandis que *tdh:math/pi/100000* renverra 314159), et définit la variable #pi temp2 à une approximation entière de pi².
	+ les dossiers *sin/* et *cos/* calculent respectivement le sinus et le cosinus d'un argument spécifié dans #sin temp ou #cos temp, et renvoient le résultat dans la même variable. Le niveau de précision dépend de la fonction : la valeur de pi correspondante sera utilisée, et l'argument doit être multiplié par (ce nombre / pi). Par exemple, *tdh:math/sin/100* avec #sin temp = 50 renverra un résultat égal à *(100 x sin(pi/2))*.
	Toutes les précisions ne sont pas supportées pour *sin* et *cos*, car cela entraînerait des overflows trop facilement (on est obligés de faire les calculs avec des entiers 32 bits...)
 - Un nouveau function tag a été ajouté à **tdh-core**, *tdh:player/on_begin_sleeping*. Il permet de stocker toutes les fonctions devant s'exécuter lorsque le joueur se couche dans un lit, et de déplacer l'appel fort disgracieux à *tdh:player/xp/maj_niveau* de **tdh-core** (*tdh:player/scores/test_lit/debut_sommeil*) vers **tdh-player** (dans le function tag cité précédemment). (3ccfac2a)
 - Une version de debug de la fonction *tdh:random* de **tdh-core** a été ajoutée et est nommée *tdh:random/debug_display*. Elle se comporte exactement comme la fonction *tdh:random*, mais affiche également le min/max et le résultat sous forme d'un tellraw. (54272e53)
#### Modifications :
 - Plusieurs fonctions *init* s'assurent désormais que des valeurs n'existent pas déjà avant de réécrire des valeurs par défaut. (c03baa5c)
 - Le détecteur de villageois en danger (*tdh:village/death_detector/*) ne checke plus que les pillagers et les zombies, puisque ce sont les seuls à attaquer les villageois. La liste d'entités est déterminée par le tag nouvellement créé *tdh:menace_villageois*. (8f901aac)
 - Des types d'entités sont maintenant spécifiés explicitement dans *tdh:ambient/phare/*, *tdh:porte/*... (#186) (ee4d5125)
 - Ajout des blocs de champignon au block tag *tdh:mushroom*. (c03baa5c)
 - Les *pack.mcmeta* des différents sous-packs indiquent désormais bien 6 (Minecraft 1.16.2-5) au lieu de 5 (Minecraft 1.15-1.16.1). (c03baa5c)
 - Le MOTD (dans **tdh-core**) est désormais un élément de storage au lieu d'être hard-codé. Il se trouve dans le storage tdh:core à l'entrée "motd", et peut être modifié, activé ou désactivé via le configurateur **tdh-core**. (2a60f6e7)
 - Dans **tdh-core**, la fonction *tdh:random* accepte un nouveau paramètre, "#random min", qui spécifie le nombre minimum (inclusif). Tous les appels existants à la fonction ont donc été modifiés pour inclure une définition de cette variable. (3ccfac2a/54272e53)
 - Une partie des checks de la fonction *tdh:random* de **tdh-core** ont été déplacés dans la sous-fonction *tdh:random/calcul* pour éviter de tester deux fois certaines conditions. (54272e53)
 - Dans **tdh-player**, la fonction d'initialisation de *tdh:player/xp* a été déplacée dans *tdh:player*. (3ccfac2a)
 - Le changelog a été renommé **CHANGELOG.md** et les parties qui n'étaient pas formatées en Markdown ont été réécrites. (#277) (dd2a7de6)
#### Réparations :
 - Correction d'un souci dans le détecteur de villageois qui faisait que le tag "RisqueDeDeces" n'était jamais retiré. (8f901aac)
 - Différents function tags dont l'extension était incorrectement définie comme ".mcfunction" ont été renommés en ".json". (ee4d5125)
 - Les phares devraient bien être allumés lorsqu'il pleut (on utilisait encore l'ancienne variable obsolète *CurrentWeather* pour faire le test...). (ee4d5125)
 - Correction d'un bug dans les interfaces de config qui risquait de définir les valeurs au minimum possible lorsqu'on cliquait sur "retour sans modifier la configuration". (c03baa5c)
 - La fonction *tdh:store/realtime* de **tdh-core** utilise désormais la bonne variable temporaire pour son calcul. (2a60f6e7)
 - La fonction générique *tdh:config/end* est désormais utilisée dans les fonctions *select* des systèmes de config des différents packs, à la fois pour plus de simplicité et pour éviter d'appeler plusieurs fois la même fonction (*tdh:config/end*) lorsqu'on utilisait le tag *#tdh:config/end_all*. Cette fonction générique (*end*) appelle déjà l'ensemble des fonctions */config/end* des packs chargés. (2a60f6e7)
 - La fonction *tdh:tick* est désormais bien listée dans le tag *minecraft:tick* de **tdh-core**. Les fonctions appelées par le tdh-tick s'exécutent donc normalement. (2a60f6e7)
#### Suppressions :
 - Les fonctions obsolètes de *tdh:repair/*, *tdh:anim/* et *tdh:duerrom/* ont été retirées. (8f901aac)
 - La fonction *tdh:nametag* a été retirée car on se sert désormais d'une alternative. (8f901aac)
 - Plusieurs fonctions intitulées *start* ou *_start* ont été retirées, puisqu'on le gérera désormais en activant le datapack correspondant. Pour se simplifier la vie, on conserve les fonctions *stop* ou *_stop* correspondantes, qui effectuent simplement un /datapack disable. (8f901aac/3ccfac2a)

### Temps/Calendrier
#### Ajouts :
 - Une interface de configuration a été ajoutée à **tdh-time** et permet de modifier les ratios jour/nuit aux équinoxes et aux solstices ; le nombre de mois dans l'année et de jours dans la semaine, ainsi que leurs noms ; ainsi que la durée d'une journée en temps IRL. Elle autorise également la modification de la date et de l'heure. (c03baa5c)
 - Une fonction de **tdh-time**, *tdh:time/date_check*, permet de vérifier que la date actuelle est permise par les paramètres actuels, et la répare dans le cas contraire. (c03baa5c)
 - Deux nouvelles versions des fonctions *tdh:store/day* et *day_long* (dans **tdh-time**) permettent de stocker la date sans inclure l'année. Elles ont le même nom, postfixé par *_noyear*. (2a60f6e7)
 - Deux nouvelles fonctions (*tdh:store/day_text* et *day_text_noyear*) permettent de stocker une date en incluant le mois en toutes lettres (avec ou sans le numéro de l'année). (2a60f6e7)
 - Une nouvelle fonction au nom barbare de **tdh-time**, *tdh:time/conv/jour_mois_annee_vers_idjour*, permet de convertir un input sous forme de dateJour/Mois/Annee en idJour. Elle permet notamment de modifier sereinement la date depuis l'interface sans que les dates déjà enregistrées ne soient affectées, par le biais de la fonction *tdh:time/storage/id_jour*. (2a60f6e7)
#### Modifications :
 - Dans la mesure du possible, le datapack **tdh-time** laisse désormais beaucoup plus de place à la personnalisation/configuration et de nombreuses variables ne sont plus hardcodées. (c03baa5c)
 - Une bonne partie des actualisations des storages de **tdh-time** ont été déplacés de *tdh:time/tdh_tick* vers *tdh:time/storage*, et peuvent désormais être appelées hors de la boucle principale et sans entraîner d'incrémentations de la date, pour permettre des checks d'intégrité de la date. (c03baa5c)
 - La configuration initiale de **tdh-time** reset désormais l'heure au lever du soleil, pour éviter d'avoir une journée complètement absurde et potentiellement buggée. Il est également possible de la reset manuellement depuis l'interface de config. (c03baa5c)
 - Les jours spéciaux de **tdh-time** sont désormais gérés par un function tag, *#tdh:time/jour_special_test*. Les fêtes ont été pour la plupart déplacées dans **tdh-specifics**, à l'exception des fêtes relativement génériques (solstices, équinoxes et nouvelle année restent dans **tdh-time**). (c03baa5c)
 - Les fonctions relatives à l'écoulement du temps et au calendrier (*tdh:time/*, *tdh:timeflow/* et *tdh:store/day/*) ont été déplacées dans un nouveau sous-pack, **tdh-time**. Une version "lite" du temps reste présente dans **tdh-core** et se contente de stocker le tick actuel et la période de la journée. (8f901aac)
 - La fonction *tdh:time/tdh_test_tick* a été optimisée et simplifiée. (8f901aac)- On utilise désormais un function tag pour l'ensemble des actions que l'on doit réaliser au moment de la mort d'un joueur : il s'agit de *#tdh:player/on_death*. (8f901aac)
 - Plusieurs mentions inutiles à des variables de **tdh-time** ont été remplacées par des appels à *tdh:random*. (8f901aac)
 - Le temps peut être mis en pause ou redémarré depuis le configurateur **tdh-time**. (2a60f6e7)
 - Le configurateur **tdh-time** permet désormais de modifier le jour de la semaine actuel. (2a60f6e7)
 - Le configurateur **tdh-time** permet désormais de définir le nombre d'heures par jour et de minutes par heure. (2a60f6e7)
 - Plusieurs fonctions supplémentaires de **tdh-time** ont été extraites des sous-fonctions de *tdh:time/tdh_tick* et sont également exécutées par /*tdh:time/date_check* : (re)calcul de la saison, des ratios jour/nuit, de l'heure et de la période... (2a60f6e7)
 - Dans **tdh-time**, la précision avec laquelle on décide si on doit incrémenter le tick Minecraft ou non a été réduite (de 10000 à 1000, dans *tdh:time/tdh_tick/1*, *2*, *3* et *4*) pour éviter l'overflow lorsqu'on a une durée des journées très élevée. (2a60f6e7)
#### Réparations :
 - Le configurateur de **tdh-time** contient désormais bien une fonction *end* qui retire le tag approprié. (2a60f6e7)
 - L'appel à la fonction *tdh:time/init/date* (dans **tdh-time**) a été déplacé de *tdh:time/init/subdivisions* à *tdh:time/init*, car *subdivisions* s'exécutait avant *fetes*, qui définissait la date de l'équinoxe d'automne, date dont dépendait la fonction *date*... (2a60f6e7)
 - Le calcul de l'année dans **tdh-time** (*tdh:store/day* et ses sous-fonctions) n'utilise plus de variables hardcodées et fonctionne donc correctement lorsqu'on modifie la durée des années. (2a60f6e7)
 - Différentes fonctions de **tdh-time** ont été modifiées pour fonctionner quelle que soit la durée en ticks de la journée, et quel que soit le nombre d'heures/minutes par jour. (2a60f6e7)
 - Les horaires de déclenchement des différents function tags de **tdh-time**/*time* ne sont désormais plus hardcodés mais stockés dans des variables (#2h currentTdhTick, #4h currentTdhTick, etc...) afin de fonctionner lorsque la durée d'une journée est différente du standard. (2a60f6e7)
 - Dans **tdh-time**, la durée du jour est désormais corrigée si la somme des différentes périodes calculées pour le jour actuel n'est pas exactement égale à la durée complète d'une journée. (2a60f6e7)
 - Dans **tdh-time** (*tdh:time/config/duree_journee_set/conversion_durees*), une bourde qui aboutissait à un calcul erroné des durées en ticks du coucher/lever du soleil a été corrigée. (2a60f6e7)
 - De nombreuses fonctions de **tdh-time** qui utilisaient encore la variable nommée "isBissextile" (remplacée dans une version précédente par "anneeBissextile") ont été corrigées. (2a60f6e7)
 - La fonction *tdh:store/day* ne foirera plus le calcul du jour et du mois si l'année actuelle est de la forme (4n+1). (#252) (602219ab)
#### Suppressions :
 - Les fonctions obsolètes de *tdh:calendrier/* ont été retirées. (8f901aac)

### TCH
#### Ajouts :
 - Les annonces des dernières versions du RP ont été ajoutées au système des annonces. (*achat_retour*, *fin_service*, *fin_service_station_ferme*, *oublie_sacs_bagages*, *pickpockets*, *trafic/*, *vendeur_sauvette*) (#155) (a42a2fc0)
 - Les lignes RER A/B et câble A/B ont été ajoutées à *tch:auto/get_id_line*, et leurs quais peuvent donc bien être détectés par les différentes parties du code de TCH (*station*, *ligne*, etc). (c5553104)
 - La fonction *tch:tag/terminus_pc* permet de récupérer le tchQuaiPC du terminus de notre ligne, en utilisant notre score idTerminus (en principe appelé par un métro ou autre mode de transport pendant son trajet pour l'affichage du SISVE). (8fd750dd)
 - Ajout des fonctions commandant les jingles *tch:sound/alerte_arrivee_station* et *tch:sound/arrivee_station* des lignes RER A, RER B, CA et CB. Réorganisation des sous-fonctions qui sont désormais rangées dans le dossier *tch:sound/(alerte_)arrivee_station*. (8fd750dd)
 - Un tag, **ValideurDebugLog**, permet d'afficher tous les détails du processus de validation au fur et à mesure qu'on valide, pour faciliter le debug. (6ed21d10)
 - Les fonds "ténèbres" et "lumière" de la dernière version du RP (rp@074c7ff4) ont été ajoutés à hgivef. (rp#95) Une redirection vers hgivef a aussi été ajoutée dans *tdh:give/rp* pour permettre un accès aisé même lorsqu'on n'est pas en train de construire une station. (9a2d4a9e)
 - Un sous-dossier de tch (*tch:metro/rail/*) contient des fonctions relatives à l'électrocution des joueurs et entités lorsqu'elles marchent sur une voie (= un bloc contenu dans le tag *tch/tags/blocks/rail*. Il y a un check toutes les secondes, intégré au tick métro, pour les entités non-joueuses qui correspondent aux types pouvant être électrocutés (*tch/tags/entity_types/electrocutable*) ; et un autre check, 2x par seconde, pour les joueurs, qui utilise un advancement (*tdh/advancements/bloc/rail*). (#265/#286) (e0511295/c12ae79e)
 - Les joueurs ne subissent aucune électrocution s'ils ont le tag *ImmuniteElectricite*, ou s'ils sont en créa/spectateur. (e0511295)
 - Le panneau Vente diagonal a été ajouté à l'interface hgive. (#220, rp#67) (fe6c465d)
 - De nouvelles pubs ont été ajoutées à **hgive pub** (4 campagnes TCH et un détournement de l'une d'entre elles). (*tch:give/pub/*) (b2c4e021)
 - Le panneau "réservé carte Gold" est disponible dans **hgive info** (*tch:give/siel/reserve_gold*). (rp#94) (b2c4e021)
 - De nouveaux logos sont disponibles dans **hgive logo** (uniquement depuis la liste complète) : les versions transparentes des logos des modes de transport (*m_transp*, *rer_transp* et *cable_transp*) et celles des logos des lignes de câble et RER (*ca*, *cb*, *rera*, *rerb*). (b2c4e021)
 - Ajout des panneaux multilignes (2+ lignes) dans **hgive corresp**. Ils sont disponibles dans une liste séparée (*tch:give/corresp/multiple*), accessible depuis la liste complète ou proposée dans l'auto-détection lorsqu'au moins 3 lignes passent à proximité. ***Comme dans le RP, les panneaux multi-modes ne sont pas encore disponibles.*** (rp#101) (b2c4e021/9988d6ad)
 - Ajout des panneaux multilignes (3+ lignes) dans **hgive acces_quais**. Ils sont disponibles dans une liste séparée (*tch:give/acces_quais/multiple*), accessible depuis la liste complète ou proposée dans l'auto-détection lorsqu'au moins 3 lignes passent à proximité. (rp#86) (b2c4e021)
 - Des panneaux *TVQ* ont été ajoutés dans **hgive acces_quais**. Ils sont disponibles dans la liste complète, et sont automatiquement proposés dans l'auto-détection lorsqu'au moins 2 lignes passent à proximité. (b2c4e021)
 - De nouveaux types de flèche existent dans **hgive acces_quais** : des versions *r_g* et *r_d* qui ne contiennent pas de flèche, et des versions *tvq_g* et *tvq_d* qui sont identiques mais n'incluent pas le trait bleu supérieur. Les deux dernières ne sont pas incluses dans la liste *tch:give/acces_quais/_choix_fleche*, mais dans *_choix_fleche_tvq* (qui ne contient que ces 2 flèches et n'est affiché que lorsqu'on demande un panneau TVQ). (b2c4e021/740b1f84)
 - Le panneau **hgive corresp** du RER B est disponible, avec la même structure que celui du RER A (ses directions affichées sont *est* et *ouest*). (b2c4e021)
#### Modifications :
 - Le système SISVE détecte désormais la luminosité ambiante pour choisir la couleur du texte à afficher (blanc/rouge ou noir/rouge sombre). Le seul texte avec lequel cela ne fonctionne pas encore est l'ID de la ligne, qui est toujours affiché en blanc, ce qui sera modifié lorsqu'on fera en sorte que les informations du SISVE soient extraites du système du PCC. (#154) (dc8aa54c/8fd750dd)
 - Le volume du câble est désormais beaucoup moins élevé lorsqu'on n'en est pas proche, et la manière dont le volume est choisi a été modifiée en réduisant drastiquement la portée d'audition du câble, qui n'est plus que de 25 blocs. (#221) (f7fd88bd)
 - Le code de détection d'entrée/sortie dans les stations TCH a été déplacé (de l'ancien emplacement *tch:auto/player/* vers *tch:station/entree/*). Il n'affiche plus de titre au centre de l'écran lorsqu'on quitte une station, simplement un message en actionbar pour faciliter le debug des stations ne disposant pas encore des item frames appropriées. (#222) (99b2733e)
 - Le message de sortie de station/gare utilise désormais la variable "lightLevel" du joueur pour choisir la couleur la plus appropriée pour l'affichage du message. (99b2733e)
 - Le code de détection d'entrée/sortie précise désormais explicitement les entités souhaitées dans ses sélecteurs. (#186) (99b2733e)
 - La fonction *tch:auto/annonce/player_tag* n'est plus utilisée lors de la diffusion des annonces sonores TCH ; depuis la suppression des annonces Far, ça n'avait plus vraiment de sens de conserver ce double mode de diffusion. (a42a2fc0)
 - Les probabilités de diffusion des annonces sur une ligne en service sont désormais séparées en 2 sous-fonctions : une pour les heures creuses, et une pour les heures de pointe. (a42a2fc0)
 - Les annonces existent désormais systématiquement en version clean et standard. La sélection se fait pour chaque haut parleur tchHP : il suffit de donner à un haut-parleur le tag Clean pour qu'il diffuse les sons sans reverb. (#155) (a42a2fc0)
 - Les annonces (hors PID) affichent désormais l'ID du sound event en cours de diffusion aux joueurs ayant le tag "AnnonceDebugLog", afin de faciliter le debug des sound events défectueux. (a42a2fc0)
 - Les horaires d'ouverture et de fermeture des stations sont désormais recalculées automatiquement lorsque l'action opposée se produit (par exemple lorsqu'une station ouvre, elle recalcule son horaire de fermeture). Le configurateur de station ne permet toujours pas d'entrer une valeur pour l'ouverture/fermeture de la station, mais cliquer sur ladite valeur permet de la faire recalculer (plus utile en cas de bug que de simplement partir pleurer sous la couette). (#197) (8b4c5f99)
 - Une station ou un guichet ne peuvent désormais plus ouvrir si un joueur en est trop éloigné (dans le but d'éviter des écueils de fonctionnement dûs au chargement d'une partie des chunks composant la station, mais pas de l'intégralité). (#152) (30b6f140)
 - Le système de génération d'horaires (*tch:ligne/horaire/*) a été amélioré ; en plus de préciser explicitement les sélecteurs dans les fonctions affectées (#186), il détermine également au moment de la génération d'un horaire si cela va être le dernier ou non, et se donner le tag Dernier en conséquence. Si il tente de générer un horaire en ayant le tag Dernier, tout se passe comme si c'était la fin de service. (#201) (a198e9e9)
 - L'accueil et la liste de *tch:give/logo* ont été réécrits pour intégrer les nouveaux caractères du RP et prendre moins de lignes dans le chat. Un système de sélection a également été ajouté, comme pour acces_quais ou corresp. (#220) (a3d2ca50)
 - Les panneaux de PID, de vente, d'accès aux quais et de correspondance du hgive ont été remplacés par les nouvelles versions du rp%3.0 ; le texte de l'interface utilisateur utilise également les nouveaux caractères introduits par ladite version du RP, et les nouvelles couleurs des lignes rendues possibles par la 1.15 (ou 16 ?) (#220) (729680af)
 - Les panneaux nom station de grande taille ont été ajoutés. (#220) (729680af)
 - L'affichage permanent des raccourcis permettant d'accéder à cet écran ont été retirés du hgive dans les sections modifiées. (729680af)
 - Le système de don des systèmes cités ci-dessus ont été améliorés et simplifiés, dans le but de diminuer au maximum la répétition de code identique et de simplifier l'expérience utilisateur. Les types de panneau (dimensions, + direction dans le cas des accès aux quais/correspondance) sont sélectionnés dans le `_main` de la catégorie, et s'appliquent à tous les objets sélectionnés dans cette catégorie jusqu'à changement de type de panneau par l'utilisateur. (729680af)
 - Le système hgive des sorties a été entièrement réécrit pour intégrer les items personnalisés du RP 3.0. (89316b04)
 - Le câble peut désormais ouvrir et fermer les barrières de sécurité en station, de la même manière que pour les portes du métro. Il faut donner au TraceCable le tag RetirerBarriere ou PlacerBarriere, et disposer sous les blocs devant supporter lesdites barrières des item frames ayant le tag "BarriereCable" et l'idLine correspondant à la direction du quai en question. (ba125f77)
 - Le câble peut désormais téléporter les joueurs aux bonnes positions lorsqu'il exécute une rotation. La rotation des entités est réalisée juste avant la prise en compte du mouvement, lorsqu'on se trouve sur un TraceCable ayant le tag RotationCW ou RotationCCW (pour 90° respectivement dans le sens des aiguilles d'une montre et dans le sens contraire). (ba125f77)
 - Le câble switche désormais d'ID de ligne lorsqu'un nœud qu'il rencontre comporte un nouvel idLine, et non plus automatiquement lorsqu'il arrive au terminus. La raison principale est de permettre de replacer la barrière du quai correctement (en détectant l'idLine) au moment où la cabine quitte le terminus. (ba125f77)
 - Les sons du câble arrivée et départ sont désormais joués au même volume que les autres bruitages du câble. (ba125f77)
 - Le SISVE est désormais un sous-dossier à la racine de tch (*tch:sisve/*) et s'applique à tous les modes de transport (pour l'instant métro et câble, mais dans le futur RER, bateaux et dirigeables pourront également être contrôlés selon la même logique). Pour améliorer les performances, on vérifie désormais pour chaque joueur s'il se trouve dans un mode de transport, plutôt que de vérifier pour chaque voiture/cabine si elle contient un joueur. (#243) (8fd750dd)
 - Pour la cohérence avec le système du métro, la cabine du câble se donne désormais le tag "PreStation" lorsqu'elle rencontre l'armor stand AlerteArriveeStation, et se tag "EnStation" plutôt que "Station" quand elle est en station. (8fd750dd)
 - Lorsqu'on détecte la pré-station dans le SISVE métro, on utilise désormais le PC quai pour vérifier s'il y a des correspondances, au lieu d'essayer de détecter les NumLigne proches. (#92) (8fd750dd)
 - On utilise désormais les PC quai pour afficher numéro de ligne et terminus en interstation. (#92) (8fd750dd)
 - Les phrases du SISVE se terminent désormais par des points. (8fd750dd)
 - Le système d'itinéraire utilise désormais lors de son initialisation les caractères custom du RP%3.0 pour les numéros de ligne. Il enregistre également bien les terminus des câbles A et B. (8fd750dd)
 - La porte du valideur reste ouverte un peu plus longtemps. (6ed21d10)
 - Les tests de porte et de station ouverte du valideur Silver ont, comme dans le valideur Gold, été déplacés dans des sous-fonctions spécifiques (*tch:station/valideur/silver/test_porte* et *test_station*). (6ed21d10)
 - Il faut attendre 2 fois plus longtemps avant de pouvoir ramasser son ticket après l'avoir passé dans un valideur (1.5 seconde contre 0.75 précédemment). (6ed21d10)
 - Lorsqu'on drop un objet sur la plaque de pression d'un valideur Silver, il est automatiquement placé au centre de ladite plaque de pression. (6ed21d10)
 - Le ticket avance désormais d'un bloc lorsqu'il a été validé avec succès, pour désactiver au plus vite la plaque de pression et permettre au joueur de le récupérer plus facilement s'il avance. Il recule d'un bloc s'il est invalide (y compris si ce n'est pas un ticket) pour ne pas bloquer la plaque de pression du valideur. (6ed21d10)
 - Le joueur ayant passé un ticket à un valideur est désormais détecté par UUID s'il n'a pas été possible de le détecter via sa proximité avec le valideur (pour qu'il puisse tout de même lire les éventuels messages d'erreur s'il était trop loin). (6ed21d10)
 - Les types d'entités sont bien précisés quand cela est possible dans le code du valideur. (#186) (6ed21d10)
 - La génération d'incident du valideur utilise désormais la fonction *tdh:random*. (6ed21d10)
 - Une cabine n'est plus forcément en mouvement, mais peut aussi être à l'arrêt si elle a le tag Interrompu. Il y a 3 cas de figure possibles : la fin de service (auquel cas la cabine s'interrompt en station jusqu'à réouverture de la ligne) ; l'arrêt incident (la cabine s'arrête également en station et attend la reprise du trafic) ; et l'arrêt d'urgence (la cabine ne se détruit plus lorsqu'elle détecte une autre cabine proche, mais se met simplement à l'arrêt). (c2492b1a/6186b712)
 - Le SISVE du câble a désormais une section dédiée qui informe lors d'une interruption due à un incident du motif de l'arrêt, ainsi que, lorsqu'il est connu, de l'horaire de reprise. (c2492b1a)
 - Le SISVE du câble a désormais une section dédiée qui informe lors d'une fin de service de l'horaire de réouverture. (c2492b1a)
 - Les fins de service sont désormais testées jusqu'à 4h, au lieu de 3h précédemment. (b5d37b96)
 - Les débuts de service sont désormais pris en compte jusqu'à 4h après l'heure prévue, au lieu de 1h précédemment. (#206) (b5d37b96)
 - Une cabine en fin de service ne récupère plus le tag Terminus, afin que l'on puisse détecter spécifiquement si on est au "vrai" terminus et que l'on puisse également faire fin de service au terminus. (#256) (6186b712)
 - Le hgive accès interdit donne désormais les items custom du RP, au lieu des cartes historiques. Le code a été en outre simplifié, avec de nombreuses suppressions de fonctions désormais inutiles (pour les différents fonds...) et des codes indiquant l'utilisation possible de hgiveai1/2/3 (ces raccourcis restent néanmoins fonctionnels). (#185) (9d79ba8d)
 - Le panneau "Danger – Accès interdit au public" est désormais disponible dans le hgive accès interdit. (#185) (1c45c4be)
 - Les fonds proposés dans le hgive sont désormais ceux récemment ajoutés au RP. Il n'y a plus qu'une fonction (*tch:give/fond/_main*) qui liste tous les fonds disponibles. (#220) (34c75de7)
 - Les fonctions du hgives permettent désormais d'obtenir la sortie 3 de la station Procyon. (rp#77) (d1d9681a)
 - Le logo TCH (dans *tch:give/logo/*) en version map a été remplacé par 3 versions du RP (*tch_rouge*, *tch_blanc* et *tch_noir*), toutes trois sur fond transparent, dans les deux tailles existantes (petit & normal) + une autre créée pour l'occasion (grand, de taille indicative 2x2 blocs) qui est accessible via l'écran de sélection habituel. *La taille "grand" donne des logos de taille "normal" si le logo en question n'existe pas en grand.* (b2c4e021)
 - Les panneaux RER A de **hgive corresp** n'affichent plus une direction précise, mais *nord* ou *sud*. (rp#69) (b2c4e021)
 - L'interface **hgive logo** permet d'obtenir les totems de station en tant qu'items du RP. (*tch:give/logo/totem/*) (b2c4e021)
 - Réduction du nombre de clics nécessaires pour changer le type de panneau dans **hgive acces_quais**, **logo**, **sortie** et **station**. Les fonctions *_choix_type_info* ont été supprimées de chacun de ces répertoires, et leur code intégré à *_choix_type* qui est désormais appelé directement à la fin de chaque itération du *_main* de ce répertoire (à la place de *_choix_type_info*).
    En clair, on propose directement à l'utilisateur de sélectionner le type de panneaux parmi une liste, au lieu de le faire cliquer sur "Choisir le type de panneaux..." puis de sélectionner ledit type de panneau sur l'écran suivant. (740b1f84)
 - Un titre est désormais affiché à chaque début de section **hgive** (= lorsqu'on rentre dans le *_main* de ce répertoire), afin d'améliorer la lisibilité. Dans plusieurs cas, cela vient remplacer le message "Choisissez les dimensions/le type du panneau xxx" qui y figurait précédemment. (740b1f84)
 - La fonction *tch:give/acces_interdit/main* propose désormais le panneau "Voies sous tension". (cdb789d3)
 - La fonction *tch:give/plan/reseau* propose désormais d'obtenir le plan du réseau en différentes versions : centré/décalé est désormais un choix possible, et 3 langues sont disponibles : français, elfique (quenya) et nain (khuzdul). (cdb789d3)
 - La fonction *tch:give/siel/serge_le_lapin* a été renommée *signal_sonore* et offre désormais la version équivalente à l'ancien panneau sous forme d'item du RP. (#220) (8d65c2fa)
 - Les panneaux travaux (*tch:give/travaux/*) ont été remplacés par leurs versions du RP (pour les panneaux "Chantier interdit au public", "Bientôt, ici" et "Un réseau plus beau") ou supprimés lorsqu'ils n'étaient plus utiles (Piétons, Station). (df58b47d)
 - Ajout des panneaux corresp multiples dans *tch:give/corresp/multiple*. (df58b47d)
 - Ajout des panneaux vides (aucune inscription, fond bleu) dans *tch:give/sortie/neutre*. (df58b47d)
 - Il est désormais possible d'obtenir deux directions supplémentaires pour le RER A dans **hgive acces_quais** : Sud (Procyon + Cyséal) et Nord (pour l'instant Le Hameau). (rp#68) (12b7373d)
 - L'actualisation des abonnements TCH s'effectue désormais à 2h du matin (contre minuit précédemment), pour tomber au moment où il y a le moins de lignes en service (ce serait peu judicieux de le faire à 4h, puisque certaines stations ouvrent dès 3h30). (c03baa5c)
 - Le délai d'attente entre 2 annonces est désormais plus long d'une minute si l'annonce précédente était "Ligne fermée" ou "Fin de service". (aa4eb0ac)
 - La fonction *tch:auto/debug/infostation* affiche désormais les spawners du câble en plus des spawners métro. (c5553104)
 - La fonction *tch:auto/debug/infostation* a désormais une portée un peu plus élevée pour l'affichage des directions. (c5553104)
#### Réparations :
 - La hauteur théorique de la cabine du câble a été réduite de 1, pour éviter d'embarquer de la neige lors du mouvement de la cabine après de fortes chutes de neige. (#227) (e92a4077)
 - L'horaire d'ouverture/fermeture d'une station ne peut plus dépendre des horaires d'ouverture/fermeture d'une ligne qui est hors-service. (#289) (aa4eb0ac)
 - Le PID affiche désormais correctement le texte du dernier train (avec "dernier" en rouge), et cela ne s'affichera bien que lorsque le train sera réellement le dernier. (#201) (a198e9e9)
 - Lorsqu'on va faire spawner un métro le lendemain car la **ligne** est en fin de service, on lui fait vérifier qu'elle est bien toujours en fin de service, pour éviter qu'une station "saute une journée" lorsque des joueurs dorment à proximité. Cela ne s'applique pas aux spawns le lendemain pour cause de tag "Dernier" du commit précédent. (#206) (60f1b7e6)
 - Il est possible d'obtenir depuis l'interface hgive les totems du M3 direction Villonne. (#127) (088c0a73)
 - Les joueurs en spectateur ne peuvent plus activer les valideurs Gold, ce qui était la cause du souci décrit par l'issue #230 (on recevait simultanément un message "Succès" et un message "Échec" lorsqu'on essayait de valider, et c'était en fait uniquement le cas lorsqu'on avait un spectateur) (8c0e3b5b)
 - Les logos des métros 7 bis et 8 bis sont à nouveau accessibles. (#216) (a3d2ca50)
 - Il est désormais possible d'obtenir les totems du câble B direction La Dodène. (729680af)
 - Asvaard-Rudelieu peut désormais bien être détectée par hgive station. (89316b04)
 - Une cabine de câble ne peut plus spawner si une autre est présente à proximité pour éviter tout risque de collision. (ba125f77)
 - Le spawn des cabines et des métros vérifie maintenant si la ligne en fin de service l'est bien avant de ne pas spawner. (ba125f77)
 - Le nettoyage des tags de la cabine du câble a été déplacé de *tch:cable/cabine/prochain_noeud* vers *tch:cable/cabine/mouvement*, ce qui permet d'éviter que le son d'arrivée ou de départ ne se joue plusieurs fois s'il y a plusieurs itérations entre deux nœuds du tracé du câble. (ba125f77)
 - Il est bien possible d'obtenir des totems du m5b en utilisant l'arborescence des lignes. (#235) (693477bf)
 - Il y a bien un jingle précédant l'annonce du terminus câble, puisqu'il fonctionne désormais avec un SISVE complet. (#226) (8fd750dd)
 - Les blocs du câble (*tch:tags/blocks/cable*) n'incluent plus la neige afin d'éviter de tout casser dans certains cas. (#227) (0f38bf87)
 - Les invocations d'items du valideur s'alignent désormais sur une position entière, pour éviter des soucis occasionnels de portes bloquées en position ouverte car leur item s'est fait la malle. (#245) (6ed21d10)
 - Les dossiers par direction des totems de la ligne M8 ont été inversés, car leurs IDs l'étaient également (les panneaux pour Procyon donnaient Géorlie, et vice-versa). (#247) (779711b9)
 - Les totems direction Le Relais de la M8bis sont désormais accessibles. (#247) (779711b9)
 - Les cabines du câble prennent bien en compte les fins de service et les incidents. (#254) (c2492b1a)
 - Il est désormais possible d'obtenir des logos du métro 8. (#262) (eed33054)
 - Les sons "fin de service" et "terminus" se jouent à nouveau correctement avec la dernière version de développement du RP (changements d'ID de sound events). (#272) (e58f4b2a)
 - Les annonces travaux RER A EN et DE ne se superposent plus. (#248) (d5cd8223)
 - Les annonces travaux M9 ne se superposent plus. (#248) (d5cd8223)
 - Les noms des sound events multilingues clean des annonces travaux M1 à M9 ont été modifiés (selon la nomenclature de la dernière version du RP). (d5cd8223)
 - L'autodétection du **hgive logo** permet désormais d'obtenir le logo du mode de transport en cliquant sur celui-ci, au lieu de donner le logo de la ligne dans tous les cas. (b2c4e021)
#### Suppressions :
 - L'ancien code du valideur a été retiré au profit du nouveau (*tch:station/valideur/*). On a simplement ajouté un message d'erreur et une redirection sur la fonction *tch:valideur/valideur* pour faciliter la détection d'anciens command blocks oubliés, en attendant de supprimer définitivement la fonction. (#223) (dce5c016)
 - Le système *tch:auto/cleanup* ne boucle plus automatiquement (histoire de tester si ça peut fonctionner sans lui avec simplement l'auto-détection en cas de réservation). Il n'est pas à proprement parler supprimé, mais ne fonctionne plus (il suffira si ça merde de décommenter la ligne appropriée dans */cleanup/tick*). (9f2aef17)
 - Plusieurs dossiers de *tch:auto/* dont(?) on est sûrs qu'ils ne sont plus utilisés ont été supprimés. (ee4d5125)
 - Plusieurs checks devenus inutiles d'ouverture et de fermeture ont été retirés du tick station (*tch:station/tick*) ; il ne reste plus qu'un calcul, celui de l'heure d'ouverture des stations fermées n'en possédant pas. L'horaire de fermeture étant automatiquement calculé lors de l'ouverture, le cycle peut ensuite fonctionner sans interruption. (30b6f140)
 - Les fonctions *tch:station/config/calcul_horaires* et *conversion_horaires* ont été retirées, puisqu'elles sont en doublon avec les nouvelles fonctions de gestion des horaires d'ouverture. (30b6f140)
 - Les anciennes pubs sous forme de maps (*tch* et *rakuten*) ont été supprimées de *tch:give/pub/*. (b2c4e021)
 - Les anciens totems station sous forme de maps ont été supprimés de *tch:give/logo/totem/*, ce qui entraîne l'indisponibilité des totems Bus de remplacement (les autres ont été simplement remplacés par leurs versions du RP). (b2c4e021)
 - Les fonctions legacy permettant d'obtenir les plans TCH et TDH en version map (*tch:give/plan/tch_2x2*, *tch_4x4_*, *tdh_* et *tdh*, ainsi que *tch:give/plan/nord/* et */surface/*) ont été retirées. (cdb789d3)
 - La fonction permettant d'obtenir les différentes parties du plan du réseau 4x4 (*tch:give/plan/reseau_4x4*) a été retirée, car il n'y a plus qu'un seul item même pour les plans 4x4. (cdb789d3)
 - L'ancienne catégorie *sb* de **hgive acces_quais** a été retirée. (12b7373d)

### Joueurs
#### Ajouts :
 - Un bonus de vitesse de déplacement est appliqué aux joueurs qui se déplacent sur une route, afin de les inciter à utiliser les chemins prévus à cet effet plutôt que d'errer dans la cambrousse. Le bonus n'est pas actif si la route est enneigée (ce n'est pas possible avec les *grass_path*, mais il y a d'autres blocs pouvant activer le bonus). (#249) (aeef0290/fd209151)
 - Un tag **tdh:route** contient les différents blocs desquels une route peut être constituée. Cela sert pour l'instant simplement à déterminer si le bonus de vitesse lié au déplacement sur route est actif. (aeef0290)
#### Modifications :
 - On utilise désormais un function tag pour tous les scores à calculer régulièrement chez les joueurs : 2 tags séparés existent, l'un spécifiquement pour les spectateurs (*#tdh:player/scores_spectateurs*) et l'autre pour tous les autres gamemodes (*#tdh:player/scores*). (8f901aac)
 - La fonction *tdh:player/on_connect/main* utilise désormais un function tag, *#tdh:player/on_connect*, pour déterminer quelles actions doivent être réalisées à la connexion d'un joueur en fonction des sous-packs chargés. (8f901aac)
 - La fonction *tdh:player/death* teste désormais les différentes méthodes de respawn avec un function tag (*/data/tdh/tags/functions/player/respawn_check.json*), pour permettre à différents packs de venir ajouter leurs propres méthodes de check. Pour l'instant, le seul pack qui l'utilise est **tdh-arena**. (9f2aef17/8f901aac)
#### Réparations :
 - La musique de combat devrait correctement s'arrêter lorsqu'on a un spectateur. (#207) (4b5f2fd4)
#### Suppressions :
 - Le mode combat a été désactivé pour l'instant devant son manque flagrant de complétude. Il ne devrait à terme être réimplémenté que pour les combats scriptés. (dans **tdh-player**/*tdh:player/combat/*) (3ccfac2a)

### Climat
#### Ajouts :
 - Un function tag de **tdh-climate**, "#tdh:meteo/on_intensity_changed", permet de lancer des fonctions lorsque l'intensité de la météo vient de changer. (3ccfac2a)
 - Une nouvelle fonction de l'initialisation de **tdh-climate**, *tdh:meteo/init/spawn*, génère l'item frame SpawnMonde, qui permet d'invoquer des entités à une position toujours chargée et invisible pour les joueurs. Cette item frame est notamment utilisée pour l'invocation des entités temporaires étant ensuite spreadées autour des joueurs pour les calculs de la neige, ou de celles matérialisant les pousses d'arbre permettant d'étendre les forêts. Un block tag de **tdh-core**, *#tdh:underground*, contient tous les blocs "naturels" pouvant être remplacés par cette fonction d'initialisation. (3ccfac2a)
 - La météo actuelle, ainsi que son intensité, sont indiquées par la commande **/meteo** (*tdh:meteo/auto/afficher_previsions*). (40e533ff)
 - Un message dans le chat a été ajouté lorsqu'on passe de la météo "orage" à "pluie", c'était le seul qui n'en disposait pas. (40e533ff)
 - Le calcul de la météo utilise désormais la fonction *tdh:random* pour la génération de ses nombres aléatoires. (40e533ff)
 - L'intensité du vent est légèrement plus stable (=varie moins vite) lorsque le temps est clair. (40e533ff)
#### Modifications :
 - Dans **tdh-climate**, la fonction *tdh:meteo/start* a été séparée en deux : *tdh:meteo/on_load* stoppe l'écoulement du cycle météorologique vanilla dès que le pack est chargé, et *tdh:meteo/start* génère des prévisions lors de l'initialisation du module. (3ccfac2a)
 - Dans **tdh-climate**, la fonction d'initialisation *tdh:meteo/init* a été séparée en plusieurs sous-fonctions, pour permettre d'initialiser le module sans reset toutes les variables. (3ccfac2a)
 - Dans **tdh-climate**, les phrases de transition (affichées lorsqu'on passe d'une météo à une autre) sont désormais stockées dans un storage et configurables. (3ccfac2a)
 - Dans **tdh-climate**, les prévisions (toujours hardcodées–j'ai essayé de mettre en place un système avec un storage, mais c'est clairement trop complexe pour permettre une configuration simple, et l'utilité n'est pas non plus énorme) sont désormais plus précises vis-à-vis de l'intensité. (3ccfac2a)
 - Dans **tdh-climate**, les prévisions sont désormais générées (par *tdh:meteo/auto/gen_semaine* et *tdh:meteo/auto/mois/...*) à partir de données entrées dans un storage au lieu d'utiliser des probabilités et des situations hardcodées. (3ccfac2a)
 - Dans **tdh-climate**, la variable aléatoire servant à déterminer si des éclairs seront produits ou non utilise désormais *tdh:random*. C'est également le cas de celles se trouvant dans le système de pousse des forêts (*tdh:meteo/effects/beau_temps/pousse_foret/creer_bloc*, *mouvement_aleatoire*...) (3ccfac2a)
 - Dans **tdh-climate**, la fonte ou non de la neige dépend désormais de la saison ("#temps saison") et pas du numéro de mois. (3ccfac2a)
#### Réparations :
 - Il n'y a plus de probabilité d'orage lorsque la météo est en mode "Beau temps". (#231) (40e533ff)
#### Suppressions :
 - Le mouvement forcé dû aux vents violents a été désactivé pour l'instant (dans **tdh-climate**). (3ccfac2a)

### RP/Specifics
#### Ajouts :
 - Les nouveaux modèles de WC du RP ont été ajoutés au **rpgive** (*tdh:give/rp/mobilier/wc*). (rp#25) (03973021)
 - Un dossier (*tdh:wc/*) permet de gérer automatiquement le changement de modèles et les bruitages lors de l'utilisation d'un WC. Simplement appeler à proximité d'un WC (de préférence depuis un command block) la fonction *tdh:wc/_flush*. (03973021)
 - Le block tag **#tdh:naturel** a été ajouté pour représenter les blocs pouvant abriter un émetteur sonore insecte ou oiseau. Il contient *#tdh:flora* et *#tdh:tree*, ainsi que la plupart des blocs organiques du sol (terre, herbe, sable, argile, mycélium, podzol...). (6b9282e4)
 - Les villes de Milloreau, Calmeflot, la Dodène, Argençon et Aulnoy ont été ajoutées à l'antispawn. (72fdd6d9/f291dfa3)
 - Le give de TDH contient désormais une section permettant de se donner tous les items custom du RP, dans *tdh:give/rp/*. (#271) Des raccourcis vers les fonctions principales de *tdh:give/* ont été ajoutés dans ce dossier (pour l'instant **deco**, **info**, **panneau** et **rp**). (639759fd)
#### Modifications :
 - Les sons d'insectes et d'oiseaux ne peuvent plus se jouer que depuis des blocs du tag **#tdh:naturel**. (#212) (6b9282e4)
 - Les sons d'insectes sont diffusés moins forts, mais sont désormais plus nombreux dans la plupart des biomes. (6b9282e4)
 - La vérification du nombre d'objets dynamiques a été déplacée dans une fonction spécifique (*tdh:player/objets* ou *objets_main_gauche/test_diviser_pile*) pour éviter de démultiplier du code identique. Elle a également été rajoutée au code des objets main gauche, où elle avait été totalement oubliée. (b0b99ab6)
#### Réparations :
 - Dans **tdh-rp-sound**, quelques références à l'item frame *SpawnMonde* ne précisaient pas son type d'entité explicitement. (3ccfac2a)
 - Les objets dynamiques sont correctement séparés avant d'être remplacés par le modèle suivant quand on en tient plusieurs en main. (#253) (b0b99ab6)

### Dialogues
#### Modifications :
 - Le dialogue est désormais l'état par défaut des PNJ. Ils n'ont plus besoin du tag Dialogue ; c'est ceux qui conservent le fonctionnement par défaut qui doivent être munis du tag **NoDialogue**. (#251) (8bee2eb4)
 - Le code de réparation du dialogue a été déplacé de *tdh:player/tick* à *tdh:dialogue/tick*, car je ne sais pas trop bien ce qu'il foutait dans player. (8bee2eb4)

### Ascenseurs
#### Ajouts :
 - Le tag *AscenseurDebugLog* permet d'afficher les messages de debug des ascenseurs situés à proximité afin de faciliter le debug du système. (42ab70cb)
 - Un ascenseur peut retourner automatiquement à un étage spécifique. Il faut lui donner le tag "RetourAuto" et un score "etageOrigine" égal à l'étage auquel on souhaite qu'il retourne automatiquement. (42ab70cb)
#### Modifications :
 - Le tick des ascenseurs est désormais exécuté en permanence, et non plus seulement quand des ascenseurs sont actifs. Pour activer/désactiver l'ensemble du système d'ascenseur, il est possible d'utiliser *tdh:ascenseur/_start* et *_stop*. (#40) (42ab70cb)
#### Réparations :
 - Un ascenseur ne s'arrête plus aux étages intermédiaires lorsqu'il a été appelé à un étage spécifique. (#260) (42ab70cb)
 - Un ascenseur ne refermera plus les portes au terminus de son trajet s'il reste des gens à l'intérieur, et les refermera automatiquement quand tout le monde sera sorti. (42ab70cb)

### *Détail des commits*
 + [Issue #228 (Réparations climat)]	(54272e53)	- 2022-06-30
 + [Issue #228]							(096f328b)	- 2022-06-29
 + [Issue #228]							(3ccfac2a)	- 2022-06-29
 + [Issue #228]							(a69e8563)	- 2021-11-28
 + [Issue #228]							(2a60f6e7)	- 2021-11-27
 + [Issue #228]							(c03baa5c)	- 2021-11-21
 + [Issue #228]							(8f901aac)	- 2021-07-06
 + [Issue #228]							(ee4d5125)	- 2021-07-05
 + [Issue #228]							(9f2aef17)	- 2021-07-04
 + [Issue #228]							(88b54a8d)	- 2021-07-04
 + [Issue #289]							(aa4eb0ac)	- 2021-07-03
 + [get_id_line RER/Câble]				(c5553104)	- 2021-07-03
 + [Issue rp#25 (WC)]					(03973021)	- 2021-07-02
 + [Issue rp#68]						(12b7373d)	- 2021-07-01
 + [Issue #286]							(c12ae79e)	- 2021-06-30
 + [Issue #220 (Travaux/corresp multi)]	(df58b47d)	- 2021-06-30
 + [Issue #220 (Signal sonore)]			(8d65c2fa)	- 2021-06-26
 + [Issue #220 (Plans multilingues)]	(cdb789d3)	- 2021-06-25
 + [Fonctions pour les item frames]		(73876a96)	- 2021-06-19
 + [Bugfix hgive corresp]				(9988d6ad)	- 2021-06-11
 + [UX hgive]							(740b1f84)	- 2021-06-11
 + [Issue #220]							(b2c4e021)	- 2021-06-11
 + [Issue #277]							(dd2a7de6)	- 2021-06-04
 + [Issue #220 (panneaux Vente)]		(fe6c465d)	- 2021-05-05
 + [Issue #265]							(e0511295)	- 2021-05-04
 + [Issue #248]							(d5cd8223)	- 2021-05-04
 + [Issue #272]							(e58f4b2a)	- 2021-05-03
 + [Issue rp#95]						(9a2d4a9e)	- 2021-05-03
 + [Issue #271]							(639759fd)	- 2021-05-01
 + [Issue rp#77]						(d1d9681a)	- 2021-04-30
 + [Issue #220 (nouveaux fonds)]		(34c75de7)	- 2021-04-28
 + [Issue #185]							(1c45c4be)	- 2021-04-28
 + [Issue #185]							(9d79ba8d)	- 2021-04-27
 + [Issue #262]							(eed33054)	- 2021-03-12
 + [Antispawn Argençon]					(f291dfa3)	- 2021-03-12
 + [Issue #253]							(b0b99ab6)	- 2021-03-12
 + [Issue #256]							(6186b712)	- 2021-03-08
 + [Issue #206]							(b5d37b96)	- 2021-03-08
 + [Issue #260]							(42ab70cb)	- 2021-03-08
 + [Issue #254]							(c2492b1a)	- 2021-03-07
 + [Issue #251]							(8bee2eb4)	- 2021-03-07
 + [Issue #247]							(779711b9)	- 2021-03-07
 + [Issue #252]							(602219ab)	- 2021-03-07
 + [Issue #245]							(6ed21d10)	- 2021-03-07
 + [Issue #249 (Commit 2)]				(fd209151)	- 2021-03-07
 + [Issue #249]							(aeef0290)	- 2021-03-06
 + [Antispawn La Dodène]				(72fdd6d9)	- 2021-03-05
 + [Issue #227]							(0f38bf87)	- 2021-03-04
 + [SISVE dont #243]					(8fd750dd)	- 2021-03-04
 + [Issue #235]							(693477bf)	- 2021-03-04
 + [Bugfix météo dont #231]				(40e533ff)	- 2021-03-02
 + [Bugfixes du câble]					(ba125f77)	- 2021-03-02
 + [Issue #220 (sorties)]				(89316b04)	- 2021-02-21
 + [Issue #220 - Commit 2]				(a3d2ca50)	- 2021-02-15
 + [Issue #220 - Commit 1]				(729680af)	- 2021-02-14
 + [Issue #230]							(8c0e3b5b)	- 2021-02-17
 + [Issue #127]							(088c0a73)	- 2021-02-17
 + [Issue #207]							(4b5f2fd4)	- 2021-02-17
 + [Issue #206]							(60f1b7e6)	- 2021-02-17
 + [Issue #201]							(a198e9e9)	- 2021-02-17
 + [Issue #152]							(30b6f140)	- 2021-02-17
 + [Issue #197]							(8b4c5f99)	- 2021-02-17
 + [Issue #212]							(6b9282e4)	- 2021-02-16
 + [Issue #155]							(a42a2fc0)	- 2021-02-15
 + [Issue #222]							(99b2733e)	- 2021-02-15
 + [Issue #227]							(e92a4077)	- 2021-02-15
 + [Issue #154]							(dc8aa54c)	- 2021-02-15
 + [Issue #221]							(f7fd88bd)	- 2021-02-15
 + [Issue #223]							(dce5c016)	- 2021-02-15
---


## [0.6.2b]	(c8952798)	- 2020-12-29
### Ajouts :
 - Rajout du block tag *tdh:passable*, qui a dû être rajouté dans une branche jamais terminée et implémentée mais qui se trouve être tout de même utilisé PARTOUT dans le code du master ! Il faut vraiment arrêter de faire des trucs comme ça hein. (du coup *tdh:passable* c'est un block tag qui contient tous les blocs non-solides permettant à, par exemple, un PNJ de passer)
 - Ajout du block tag *tdh:air*, qui contient les différents blocs d'air existants.
 - Ajout du block tag *tdh:torch*, qui contient les différentes torches existantes (normale, soul, redstone).
 - Ajout du block tag *tdh:mushroom*, qui contient les différents petits champignons (rouge, brun, crimson, warped).
### Modifications :
 - Lors de l'initialisation des noeuds au début d'un calcul d'itinéraire TCH, on vérifie si les lignes correspondantes sont fermées avant d'activer les noeuds. Il n'est donc plus possible d'emprunter une ligne dans un itinéraire si elle ne roule pas en ce moment. (#200)
 - L'agent TCH conseillera désormais de revenir le lendemain matin quand il ne trouve pas de résultat d'itinéraire et qu'il fait nuit. (en lien avec #200)
 - La fonction *tch:itineraire/calcul/voisins/trouver* stocke désormais ses variables temporaires dans un joueur fictif plutôt que dans ses propres scores, afin de simplifier la fonction et d'éviter un tas de sélecteurs. (#186)
 - Diverses fonctions du valideur utilisent désormais des sélecteurs explicites sur les types d'entités. (#186)
 - Un joueur qui a déjà eu un échec sur un valideur Gold ne peut désormais plus activer un valideur Gold tant qu'il a ce tag (qui est toujours retiré dès qu'il s'éloigne à plus de 1m d'un valideur Gold). Ceci afin d'éviter qu'un joueur restant longtemps dans un valideur fasse lagger tout le serveur.
 - On teste maintenant si le joueur est proche d'un valideur, plutôt que si le valideur est proche d'un joueur, car il y a tout de même en principe beaucoup plus de valideurs que de joueurs.
 - Le message d'erreur "Porte introuvable" du valideur Gold ne s'affichera plus en boucle.
 - Le block tag *tdh:flora* contient désormais également les pousses d'arbres, les "racines" du nether (crimson/warped) et les champignons (tag *tdh:mushroom*).

## [0.6.2]	(001530c1)	- 2020-12-29
### Modifications :
 - La fonction *tch:tag/voie* utilise désormais des sélecteurs explicites sur le type de l'entité, hormis pour NomStation dont je suis presque sûr qu'il existe à la fois des versions item frame et des versions armor stand. (#186)
 - La fonction *tch:metro/spawner/spawn/test_voie* vérifie désormais si la réservation qui bloque le spawn est bien justifiée, comme c'était le cas auparavant avec le système *tch:auto*. (#199)
 - La fonction *tch:metro/voie/reservation/test* exécute désormais un *test_reservation_fantome* avant de se mettre en file d'attente ; si le test se rend compte que la réservation effectuée ne correspond à aucun métro existant, il réserve tout de même la voie pour éviter de rester bloqué à l'infini. (#199)

## [0.6.1]	(3d5caea6)	- 2020-12-29
### Ajouts :
 - De nouveaux topics ont été ajoutés, le tabac et l'herbe, dans *tdh/advancements/dialogue/ressources*. Ils font partie de la sous-catégorie de ressources "drogue". Les deux topics sont complètement fonctionnels et ont des dialogues spécifiques avec les buralistes, les taverniers, les dealers et les agents TCH. (#190) (ffab3127/a1a8620e)
 - Des fonctions (*tdh:dialogue/calcul/avis_stupefiants* et sous-fonctions dans le dossier du même nom) permettent pour un PNJ de calculer son appréciation des stupéfiants (alcool, tabac, etc) et sa connaissance du sujet. (ffab3127/a1a8620e)
 - Des fonctions génériques des dialogues (*tdh:dialogue/extra*) permettent de générer des morceaux de phrase génériques pour utilisation dans des répliques. Existent pour l'instant *bonjour* et *bonne_journee* qui génèrent la bonne formule de cette forme en fonction de l'heure de la journée. (a1a8620e)
 - Des greetings spécifiques ont été ajoutés pour les buralistes et les dealers. (a1a8620e)
 - Un tag "entité importante" peut désormais être ajouté à une entité qu'on doit garder en mémoire pour une réplique de dialogue ; à la fin de l'affichage, le tag sera retiré et toutes les variables temporaires (temp à temp9) réinitialisées. (a1a8620e)
 - Les mots-clés génériques de *tdh:dialogue/topics/tick* sont désormais également stockés dans le storage temporaire. (a1a8620e)
### Modifications :
 - La mise en page de l'écran de sélection des topics (*tdh:dialogue/topics/choose_topic*) a été améliorée, et intègre désormais des retours à la ligne supplémentaires et des titres de sections. (ffab3127/a1a8620e)
 - La fonction random multiplie désormais la position aléatoire par un scalaire plus important pour éviter lors d'une sélection de grands nombres qu'ils finissent tous dans la même plage. (a1a8620e)
### Réparations :
 - Les topics relatifs aux ressources naturelles (mine/plantes) testent bien le bon advancement dans *tdh:dialogue/topics/tick*. (a1a8620e)
### *Détail des commits*
 + [Branche "vente_tabac", Commit 2] (a1a8620e)	- 2020-12-28
 + [Branche "vente_tabac", Commit 1] (ffab3127)	- 2020-12-27
---

## [0.6.0b]	(177aa3cb)	- 2020-12-27
### Réparations :
 - L'advancement *dialogue/tch/tarifs* n'a plus comme parent l'advancement supprimé *achat*, mais simplement *tch*.

## [0.6.0]	(9e83708e)	- 2020-12-27
### Technique :
#### Ajouts :
	- La fonction *tdh:random* renvoie dans #random temp un nombre aléatoire entre 0 et #random max. Elle a vocation à standardiser et simplifier la génération de nombres aléatoires plutôt que de réécrire le même genre de code chaque fois. (efb78fda)
	- La fonction *tdh:store/distance_format* permet de formater une distance (en blocs) pour afficher un nombre cohérent en mètres ou kilomètres selon l'ordre de grandeur de cette distance. (49b88683)
	- La fonction *tdh:math/sqrt* permet de calculer une racine carrée (l'input et l'output se font dans #sqrt temp). (49b88683)

### Gameplay :
#### Ajouts :
	- Un système de compétences très basique a été implémenté (dans *tdh:player/xp*). Il y a des niveaux de compétence d'éloquence, force, défense et agilité, qui influent sur les attributs du joueur (vitesse de déplacement, dommages, etc) (58d79eb4). Une partie de l'expérience est bien accordée en temps réel, mais le changement de niveau est impossible tant qu'on ne dort pas dans un lit. (5186d4e6)
	- De l'expérience est gagnée lors de la réalisation de diverses actions, comme tuer des mobs, pêcher, ou faire de la plongée. (5186d4e6)
	- Des sons ont été ajoutés au moment du level up de compétence. (#174) (5186d4e6)
#### Modifications :
	- Le test lit (*tdh:player/scores/test_lit*) a été séparé en sous-fonctions. (5186d4e6)

### Dialogues :
#### Ajouts :
	- Système de transaction générique dans *tdh:dialogue/transaction*. Il est possible de l'activer en donnant le tag Transaction au PNJ en dialogue, et en définissant le storage tel qu'expliqué dans *tdh:dialogue/affichage*. Les moyens de paiement supportés sont fer, or, diamant, et kubs. (0ca56063)
	- Le topic "torche" a été ajouté dans les advancements, ainsi que chez les taverniers et aubergistes. (#191) (58d79eb4)
	- Le topic "dialogue/auberge/reserver" permet de réserver une chambre à l'avance, mais n'est pas encore implémenté. (efb78fda)
	- Le topic "dialogue/tch/abonnement/aides" permet de demander des aides à l'administration de sa ville pour payer un abonnement TCH, mais n'est pas encore implémenté (à part chez l'agent TCH). (efb78fda)
	- Le topic *au_revoir* affiche une phrase d'au revoir avant d'afficher la fin de dialogue et a vocation à remplacer (du moins à "contenir") dans la plupart des cas l'appel à *end*. (49b88683)
	- Il est désormais possible de chercher une distance à vol d'oiseau avec le système d'itinéraire (renvoie aussi la direction) : soit à partir de 2 couples de variables #tchItineraire posX/posZ et deltaX/deltaZ, soit à partir de la position du joueur le plus proche de l'exécution et d'un ID station de destination dans #tchItineraire stationArrivee. (49b88683, b8032bc5)
	- Le lieu *tdh:advancements/dialogue/lieux/ile_du_singe* a été ajouté (en tant qu'enfant d'Évrocq). (efb78fda)
	- Le lieu *tdh:advancements/dialogue/lieux/grenat_meridional* a été ajouté (et c'est le nouveau parent de Fénicia et d'Oréa-sur-Mer, puisqu'il s'agit de la "région" dans laquelle ils se trouvent). (efb78fda)
#### Modifications :
	- La fonction *tdh:dialogue/tick* a été simplifiée et plusieurs checks inutiles supprimés. (efb78fda)
	- La fin de dialogue (*tdh:dialogue/end*) appelle désormais une sous-fonction (*end_npc*) pour réinitialiser le PNJ, afin de vérifier proprement si c'est bien le bon PNJ (= celui avec qui on parle, pas nécessairement le plus proche). (efb78fda)
	- L'utilisation du storage dans les dialogues a été généralisée. On enregistre désormais la phrase dans les sous-fonctions, mais la syntaxe de l'affichage est gérée directement dans une fonction spécifique (*tdh:dialogue/affichage*), afin d'éviter de démultiplier le copier-coller d'informations identiques (nom du joueur/du PNJ, début et fin du bloc de texte, coloration...). (efb78fda, 0ca56063, cc66dd87, 0ca56063, b8032bc5)
	- Il est possible, à des fins de debug, d'afficher un dump complet du storage de dialogue après chaque réplique, en se donnant le tag DialogueDebugLog. (0ca56063)
	- L'écran de sélection des lieux utilise désormais un storage au lieu d'une entité temporaire. (efb78fda, b8032bc5)
	- L'écran de sélection des sujets de conversation utilise désormais un storage au lieu d'une entité temporaire. (58d79eb4)
	- Le topic de dialogue "chambre" a été étoffé et est pratiquement complet, exception faite de la location proprement dite des chambres (à l'hôtel ou à l'auberge, on répondra toujours qu'on est complet pour l'instant). (efb78fda)
	- Les dialogues de la recherche d'itinéraire TCH ont été mis à jour, et il est désormais possible d'avoir une indication directionnelle simple lorsqu'on demande son itinéraire à quelqu'un d'autre qu'un agent TCH. (49b88683)
	- Sneak pendant une conversation définit le topic sur "Au revoir" au lieu de quitter la conversation sans réplique. (0ca56063)
	- Suppression du retour à la ligne supplémentaire après la réplique du PNJ lors d'un dialogue. (b8032bc5)
	- Suppression du séparateur de fin de réplique : la fin du bloc de texte ne consiste plus qu'en un retour à la ligne. (b8032bc5)
	- Suppression du séparateur de début de réplique : le début du bloc de texte ne consiste plus qu'en un retour à la ligne. (b8032bc5)
	- Les séparateurs sous forme d'étoile existent toujours, mais uniquement en début et fin de conversation. (b8032bc5)
	- L'ID de dialogue est désormais aléatoire au lieu d'avoir des ID incrémentés à partir de 0. (cc66dd87)
	- Le topic *tdh:advancements/dialogue/tch achat* a été renommé *ticket*, puisqu'il y avait une ambiguïté avec l'achat d'un abonnement/carte Gold et qu'on veut pouvoir discuter des tickets avec d'autres PNJ que les agents TCH, sans forcément en acheter. (efb78fda, 0ca56063)
	- Les topics de dialogue "acheter tickets fer"/"or"/"diamant" ont été supprimés : on utilise désormais la syntaxe Answer (avec des options cliquables lorsqu'on est dans le sujet "Acheter des tickets"), plutôt que des sujets de dialogue séparés. (efb78fda)

### TCH :
#### Ajouts :
	- La fonction tch:tag/station_pc permet de tagger le PC d'une station spécifique, de manière similaire à *tag/pc* et *tag/quai_pc*. (cc66dd87)
#### Modifications :
	- Le système d'itinéraire TCH a été révisé et se trouve maintenant dans *tch:itineraire*. De nombreux sélecteurs ont été améliorés (#186). Des messages de debug sont maintenant diffusés aux joueurs ayant le tag ItinDebugLog (affiche simplement le résultat) et ItinDetailLog (affiche les détails de toutes les stations calculées: SPAM !). Le système est maintenant asynchrone, et ne tente pas d'exécuter tous les calculs au cours du même tick. (#62) (4ab43dda, 49b88683)
	- L'initialisation du système d'itinéraire enregistre désormais la position absolue de chaque station afin de permettre des recherches "light" (quand on demande son chemin à quelqu'un d'autre qu'un agent TCH). (4ab43dda)
	- Le système d'itinéraires TCH enregistre désormais des données d'affichage précises, et la structure du NBT a été légèrement modifiée (par exemple, pour les noms de station et de lieux-dits, dans *Item.tag.display.station* (pour afficher par exemple "au Hameau" plutôt que "à Le Hameau")). (49b88683)
	- Un PNJ qui rentre dans un dialogue alors qu'il est déjà en mode NoAI le restera à la fin du dialogue. (49b88683)
	- Les réponses de dialogue négatives sont désormais génériques, avec -1 = quitter la conversation, -2 = autre sujet, -4 = sélection des lieux (49b88683)
	- Réécriture de *tch:auto/debug/mesure/* et déplacement dans *tch:debug/mesure/*. Le temps est désormais indiqué en ticks, et non plus en minutes, afin de permettre des calculs exacts même dans l'éventualité où on change la vitesse d'écoulement du temps un jour. Son fonctionnement a évolué davantage : en plus de l'activation manuelle, il est possible d'enclencher le mode auto (*tch:debug/mesure/auto/on*) pour activer au bon moment et afficher systématiquement les valeurs calculées sur toute une ligne. (b8032bc5)
	- Le décalage du SISVE est désormais indiqué par le système automatique de mesure (point ci-dessus), au lieu d'être rangé dans MetroDebugLog, afin de permettre des mesures de toutes les valeurs nécessaires sans être spammé par les incidents. (b8032bc5)
#### Réparations :
	- Les incidents sont bien affichés uniquement si on a le tag MetroDebugLog. (b8032bc5)
	- Quelques coquilles dans l'initialisation du système d'itinéraire TCH ont été corrigées. (b8032bc5)
	- L'initialisation de l'itinéraire TCH ne renomme plus la station Procyon en Crestali. (0ca56063)

### *Détail des commits*
 + [Bugfixes finaux]					(b8032bc5)	- 2020-12-27
 + [Màj dialogues partie 5 + Bugfixes]	(0ca56063)	- 2020-12-20
 + [Màj dialogues partie 4]				(cc66dd87)	- 2020-12-19
 + [Màj dialogues partie 3]				(49b88683)	- 2020-12-17
 + [Màj itinéraire]						(4ab43dda)	- 2020-12-16
 + [Màj dialogues partie 2]				(efb78fda)	- 2020-12-16
 + [Amélioration du système d'XP]		(5186d4e6)	- 2020-12-12
 + [Màj dialogues 1]					(58d79eb4)	- 2020-12-10
---


## [0.5.14d]	(2c20185e)	- 2020-12-12
### Réparations :
 - La musique de combat se joue à nouveau correctement.

## [0.5.14c_4]	(83be67bb)	- 2020-12-12
### Réparations :
 - Les titres d'entrée en ville fonctionnent de nouveau correctement. (#196)

## [0.5.14c_3]	(d824dd80)	- 2020-12-12
### Réparations :
 - La vitesse d'un joueur est bien recalculée après sa déconnexion/reconnexion. (#195)
 - Les titres d'entrée en ville ne se lancent plus en boucle. (#196)

## [0.5.14c_2]	(950ad997)	- 2020-12-10
### Réparations :
 - Le message de debug affichant les horaires de lever et de coucher du soleil n'est plus visible que pour les joueurs ayant le tag TimeDebugLog. (#188)

## [0.5.14c]	(2c83fc0a)	- 2020-12-10
### Modifications :
 - Le code de calcul de lever et de coucher du soleil (*tdh:time/compute_ratios*) a été simplifié et utilise désormais le code de tdh:store pour le calcul des horaires. Il ne stocke également plus l'heure et la minute du lever du soleil, mais simplement le tick (pour calcul lors de l'affichage par tdh:store).
 - L'initialisation de *tdh:time* a été réorganisée et séparée en sous-fonctions.
 - Le MOTD a été amélioré pour tenir compte des changements dans le stockage du lever/coucher du soleil, et les messages dépendant de l'heure de la journée sont à présent séparés en sous-fonctions (*tdh:player/on_connect/motd/*).
### Réparations :
 - Par conséquent, le lever/coucher du soleil ne peuvent plus se produire à N heures 60. (#188)

## [0.5.14b]	(2111c728)	- 2020-12-10
### Modifications :
 - La réduction de vitesse de déplacement due au vent est désormais un peu moins intense.
### Réparations :
 - La vitesse de déplacement n'est plus ralentie par les précipitations lorsqu'on se trouve à l'intérieur d'un bâtiment. (#187)
 - Le vent pousse moins fort et détecte mieux si l'espace est libre ou non pour pousser le joueur.

## [0.5.14_1]	(6d69afe1)	- 2020-12-10
### Réparations :
 - Correction d'un souci qui empêchait le temps de se mettre en pause lorsqu'aucun joueur n'était connecté. (#193)

## [0.5.14]	(56a9f534)	- 2020-12-10
### Ajouts :
 - Un système de scores remplace automatiquement les torches craftées ou ramassées par les joueurs par des torches du commit précédent, qui se consument en un temps limité (*tdh:player/objets/remplacer_torches*). (#192)
### Modifications :
 - La recette de craft des torches a été rendue bien moins rentable ; une seule torche est désormais fabriquée à chaque fois, et la recette nécessite une table de craft et un "tissu" à imbiber (cuir, laine ou papier). (#192)
 - Les torches allumées custom ont désormais toutes un tag inutile (*{tdhTorche:1b}*) pour permettre une détection moins fastidieuse depuis le code de remplacement des torches. Ça sera retiré dès que Minecraft permettra le NBT dans les outputs de recettes, en Java Edition 1.29 ^^^^^^^
 - Optimisation du code des objets main gauche car je savais pas qu'on pouvait utiliser des index négatifs dans un array.
### Réparations :
 - La combustion d'un objet dynamique ne fera plus disparaître les autres si on en a plus d'un dans le stack qu'on tient en main (on summonera à la place les objets supplémentaires au sol afin qu'on les ramasse à nouveau dans un autre stack).

## [0.5.13b]	(7166ab65)	- 2020-12-08
### Réparations :
 - Correction de CustomModelData incorrects lors du remplissage d'une pipe.

## [0.5.13]	(67a8371f)	- 2020-12-08
### Ajouts :
 - Des torches qui se consument en un temps limité (environ 6h) ont été ajoutées. Elles sont accessibles via la fonction *tdh:player/objets/objet/torche/_donner*. (#139)
 - Des cigarettes et pipes ont été ajoutées, sans modèles spécifiques pour l'instant mais qui les prendront en charge dès qu'ils seront correctement ajoutés dans le RP (voir rp#36). Les différents objets sont accessibles via les fonctions *tdh:player/objets/objet/<nom_objet>/_donner*. (#177)
### Modifications :
 - Un check superflu de la variable **deces** a été retiré du *tdh:player/tick*.
 - Les sélecteurs de *tdh:player/tick* ont été standardisés et optimisés (#186)

## [0.5.12b]	(643c9910)	- 2020-12-07
### Modifications :
 - Le code d'enregistrement de l'altitude est passé de *tdh:meteo/tick* à *tdh:player/scores/tick*.
 - Les effets de l'altitude sur la vitesse de déplacement des joueurs sont désormais combinés avec ceux des précipitations et de l'intensité du vent, jusqu'à atteindre une presque absence de mouvement pendant un orage violent au sommet d'une montagne. (#176)

## [0.5.12]	(1c4367c6)	- 2020-12-07
### Modifications :
 - Le calcul de toutes les variables joueur (y compris celui du biome et des combats) est désormais réalisé au même rythme pour tous les joueurs, au même rythme que le calcul de l'insideness, et en même temps pour tous les joueurs. (#39)
 - La fonction *tdh:player/fly_check* a été déplacée (*tdh:player/scores/test_vol*) et optimisée pour éviter d'accéder au NBT du joueur.
 - La fonction *tdh:player/sleep_check* a été déplacée (*tdh:player/scores/test_lit*) et optimisée pour éviter d'accéder au NBT du joueur.
 - La fonction *tdh:player/in_town_check* a été déplacée (*tdh:player/scores/test_ville*), réécrite et séparée en plusieurs fonctions. Elle ne devrait plus afficher de sous-titres intempestifs (#71). Le souci d'interruption répétée des musiques lorsqu'on est à la lisière de la zone de détection devrait désormais être moins gênant, car il faut désormais s'éloigner un peu de la ville avant qu'on en soit considérés comme sorti.
 - La fonction *tdh:player/inside_check* a été déplacée (*tdh:player/scores/test_interieur*) et optimisée pour éviter de nombreux checks redondants. L'algorithme de calcul de la valeur d'insideness a également été légèrement modifié.

## [0.5.11f]	(93badb4f)	- 2020-12-07
### Modifications :
 - Il n'est plus possible de valider deux fois de suite avec le même titre de transport dans un laps de temps de 15mn (#144). À ces fins, les tickets TCH contiennent désormais un tag supplémentaire, *derniere_validation*, qui enregistre le dernier passage à une borne couronné de succès de ce ticket. Les abonnements, quant à eux, enregistrent ces données avec celles de l'abonnement, dans le tag *valide_le*.
 - Les valideurs Gold testent maintenant l'ouverture de la station et la présence de la porte après les tests objet, pour éviter qu'on ait le message si on ne présente pas de carte Gold.
### Réparations :
 - Les incidents ne devraient plus se générer sur les valideurs Gold lorsqu'on ne présente pas de carte Gold. (#162)
### Suppressions :
 - Un appel dupliqué à *test_porte* dans *tch:station/valideur/gold/succes* a été retiré.

## [0.5.11e]	(6b8e0839)	- 2020-12-07
### Réparations :
 - Réparation d'une erreur qui dupliquait le test du distributeur depuis le commit précédent.

## [0.5.11d]	(c2c74097)	- 2020-12-06
### Réparations :
 - Correction d'une erreur dans le code des distributeurs du commit précédent.
### Suppressions :
 - Désactivation de la porte Glandebruine du Hameau et du tick associé.

## [0.5.11c]	(9ea21110)	- 2020-12-06
### Réparations :
 - Les tickets achetés par le distributeur ne peuvent plus être ramassés par un autre joueur que celui qui les a achetés.
 - Le son "Merci de préparer votre mode de paiement" se coupe lorsqu'on en diffuse un nouveau.
 - Le distributeur ne devrait plus autoriser l'activation par un joueur s'il est déjà réservé par un autre joueur. (#184)
 - Le distributeur ne devrait plus autoriser l'achat/vérification de tickets si on n'a pas encore vu l'écran d'accueil.

## [0.5.11b]	(4e12070e)	- 2020-12-06
### Modifications :
 - Le bouton d'appel de l'ascenseur est maintenant moins fort.
### Réparations :
 - Le son d'arrivée de l'ascenseur est correctement diffusé.
 - On ne devrait plus être plaqués au sol par un ascenseur lorsqu'il arrive à notre étage.

## [0.5.11]	(92a5944c)	- 2020-12-06
### Ajouts :
 - Le vent déplace désormais légèrement les joueurs lorsqu'ils sont exposés et que l'intensité de la météo est élevée. La direction du vent est la même sur toute la map. (#161)

## [0.5.10]	(ad5732e6)	- 2020-12-06
### Ajouts :
 - Les ascenseurs intègrent maintenant les sons génériques de la dernière version du RP.
### Réparations :
 - Il n'est plus possible de se retrouver coincé dans les grilles d'un ascenseur au moment où il part.

## [0.5.9d]	(a2116428)	- 2020-12-06
### Modifications :
 - Légère augmentation de la portée des distributeurs (lors de l'initialisation, on vérifie désormais la distance à partir de l'item frame Distributeur et non du command block).
 - Ajout de la station Asvaard-Rudelieu à *tch:station/get_id*.
### Réparations :
 - Les agents TCH sont désormais automatiquement réveillés à chaque fois qu'un joueur est proche. (#183)

## [0.5.9c]	(d9719dca)	- 2020-12-05
### Réparations :
 - Les agents TCH ne devraient plus être endormis en service. (#183)

## [0.5.9b]	(59b6c8d0)	- 2020-12-05
### Ajouts :
 - La fonction *tdh:store/day_long* permet, comme la fonction *day*, de stocker une date, mais la renvoie toujours sous forme longue avec 6 caractères (03/07/69 au lieu de 3/7/69).
### Modifications :
 - Les distributeurs ont été réécrits (avec l'objectif de les animer dans le futur via l'affichage d'un "écran" sur l'item frame) et placés dans le code des stations (*tch:station/distributeur*). L'ancienne fonction (*tch:distributeur/distributeur*) redirige vers la nouvelle fonction.
 - Les sélecteurs du tick des stations (*tch:station/tick*) précisent maintenant explicitement le type d'entité attendu, pour améliorer les performances.
 - La fonction de recherche d'abonnement (*tch:abonnement/_find*) renvoie désormais le nombre de jours restants si elle trouve l'abonnement, dans la variable #tch dureeValidite.

## [0.5.9]	(f556ecb9)	- 2020-12-05
### Modifications :
 - Le fichier automatisé des abonnements déplace désormais les abonnements non provisionnés vers une archive (tout en les supprimant du fichier principal) au bout d'un an. (#175)
 - Le fichier automatisé des abonnements fait désormais un backup des abonnements tous les jours, au cas où il arriverait malheur au fichier principal.

## [0.5.8b]	(07cbc023)	- 2020-12-05
### Réparations :
 - Le biome "stone_shore" est bien considéré comme enneigé au-dessus d'une altitude de 90. (#166)

## [0.5.8]	(18ea680b)	- 2020-12-05
### Ajouts :
 - Un joueur qui vient de se connecter est désormais invulnérable pendant 15 secondes. (#178)

## [0.5.7g]	(fc92a36c)	- 2020-12-05
### Réparations :
 - La taille du parallélépipède de détection des joueurs présents dans/sur une cabine a été agrandie, et ne devrait plus faire tomber les gens qui se situent au bord du bloc ou sur la vitre extérieure d'une cabine du câble.

## [0.5.7f]	(47eb01dd)	- 2020-12-04
### Modifications :
 - Les conditions de spawn des cabines ont été simplifiées ; comme on génère désormais des horaires précis, on n'a plus besoin de vérifier s'il y a une autre cabine de la même ligne à proximité.

## [0.5.7e]	(ed70fe3c)	- 2020-12-04
### Modifications :
 - Les câbles ont désormais un PC et prennent en compte les informations qui y figurent (horaires, fréquence de spawn...). (#181)
 - La génération d'horaires de spawn a été déplacée de *tch:metro/spawner/gen* vers *tch:ligne/horaire/gen* pour permettre son utilisation par d'autres modes de transport (pour l'instant le câble en plus du métro).
 - Les cabines de câble utilisent désormais la dalle de prismarine pour figurer les blocs d'air remplaçant des blocs de la frame précédente/suivante, à la place de la pink_glazed_terracotta.
 - Il est désormais possible d'activer et de désactiver les lignes au PC via une commande.
 - Les fonctions *tch:tag/quai_pc* et *pc* détectent maintenant spécifiquement des item frames. Cela n'a pas été fait sur les autres fonctions en attendant d'harmoniser le type d'entité entre les réseaux câble, RER et métro. (on a parfois des NomStation sous forme d'item frame notamment...).

## [0.5.7d]	(2c9346e1)	- 2020-12-04
### Modifications :
 - Une cabine ne peut plus spawner s'il n'y a pas de joueur à proximité de la station correspondante.
 - Le son de la cabine est diffusé moins fort aux joueurs qui se trouvent à proximité qu'aux joueurs plus éloignés.
 - La cabine détectera les joueurs un peu moins éloignés pour vérifier si elle peut dépop ou non, pour éviter de laisser des blocs volants résiduels chez les joueurs qui les voient encore.
 - La cabine ne s'arrêtera plus de bouger avant de dépop (la distance de détection des joueurs pour le dépop est maintenant supérieure à celle pour l'arrêt du déplacement).
 - La cabine se déplace désormais avec la neige qui la recouvre (les couches de neige ont été ajoutées au tag *tch:cable*).

## [0.5.7c]	(5fc2fe56)	- 2020-12-04
### Ajouts :
 - Des spawners ont été ajoutés pour le câble. Des cabines de câble peuvent spawner aux emplacements prévus à cet effet, un peu comme les métros, mais sans prendre en compte pour l'instant les PC de ligne (horaires, statut ouvert/fermé, incident...) puisqu'ils n'existent pas pour les câbles.
 - Un système d'annonces basique a été ajouté au câble : les sons d'arrivée en station qui indiquent le nom de la station sont diffusés par des noeuds du parcours, et le son Terminus est diffusé au moment où l'on s'y arrête.
### Modifications :
 - La fonction *tch:auto/get_id_station* a été déplacée dans *tch:station/get_id*. L'ancienne fonction redirige désormais vers la nouvelle, qui a vocation à être utilisée à la place de la précédente.
 - Une cabine de câble se détruit désormais lorsqu'elle est trop loin d'un joueur.
 - Les cabines de câble ne peuvent plus entrer en collision, et sont automatiquement supprimées si elles se rapprochent trop l'une de l'autre (fonctionne seulement si elles ont un idLine correctement assigné).
### Réparations :
 - Le câble ne se réinitialisera plus à la valeur de la dernière vitesse enregistrée depuis son tracé (la variable temporaire est bien supprimée après le traitement d'un noeud).

## [0.5.7b]	(301ba897)	- 2020-12-03
### Modifications :
 - Le code du câble (#9) ne fait désormais plus qu'un seul déplacement lorsqu'il se déplace selon plusieurs axes à la fois, ce qui démultiplie le nombre de fonctions liées au mouvement mais permet un important gain de performance ingame.
 - Pour améliorer les performances de chargement des modèles, les modèles autres que "Ouvert" ne contiennent désormais que leur différence avec le modèle "Ouvert" et pas le modèle dans son intégralité.
 - Il est désormais possible de définir instantanément la vitesse d'une cabine au lieu de jouer sur la vitesse maximale.

## [0.5.7]	(20c6f418)	- 2020-12-02
### Ajouts :
 - Le code du câble est maintenant générique, mais est encore en phase de test et ne prend pas en compte les stations. (#9)
 - Un block tag liste tous les blocs constitutifs d'une cabine de câble (*tch:tags/blocks/cable*).
### Modifications :
 - Les fonctions de sonorisation du câble existent maintenant en version stable à toutes les vitesses, et pas seulement au maximum. Des vitesses supérieures au maximum (120 et 140) ont également été ajoutées.
### Suppressions :
 - L'ancien code du câble A a été supprimé.

## [0.5.6b]	(6fe5fbd2)	- 2020-12-02
### Réparations :
 - Les ascenseurs ne peuvent plus déplacer les item frames.
 - Les ascenseurs ne peuvent plus poser leur câble de descente dans un bloc.

## [0.5.6]	(561154c7)	- 2020-12-02
### Ajouts :
 - Ajout des ascenseurs (*tdh:ascenseur*). (#170)

## [0.5.5g]	(c6d40f4e)	- 2020-12-02
### Modifications :
 - La musique est désormais réinitialisée au moment du décès. (#53)

## [0.5.5f]	(a7b2f8a7)	- 2020-12-01
### Ajouts :
 - Un jingle sonore est désormais joué à chaque joueur lors de sa connexion. (#88)

## [0.5.5e]	(f4392650)	- 2020-12-01
### Modifications :
 - La hauteur maximale et minimale de la neige dépend désormais de l'altitude et de la saison. (#168)
### Suppressions :
 - L'ancien système de sonorisation des oiseaux, insectes et effets météo a été supprimé (de *tdh:sound/auto*).

## [0.5.5d]	(ae50d918)	- 2020-12-01
### Ajouts :
 - Les éclairs sont désormais plus fréquents à mesure que l'intensité de la météo augmente pendant un orage. (#169)
### Modifications :
 - Les sons d'insectes sont désormais plus rares lorsqu'il y a des précipitations.
 - Les sons d'oiseaux sont désormais beaucoup plus rares lorsqu'il y a des précipitations.

## [0.5.5c]	(2ae1b3b6)	- 2020-12-01
### Réparations :
 - Suppression d'un message de debug oublié dans le code de fermeture du pont-levis de Fort du Val (#172)

## [0.5.5b]	(f9411563)	- 2020-12-01
### Modifications :
 - Lorsqu'un dialogue débute, le joueur regarde automatiquement dans la direction du PNJ qui débute le dialogue. (#130)
 - Quelques particules sont affichées au-dessus du PNJ qui débute le dialogue pour rendre l'interaction plus visuelle.

## [0.5.5]	(98cf83d0)	- 2020-12-01
### Ajouts :
 - Les sons d'insectes et d'oiseaux ont été réimplémentés (respectivement dans *tdh:sound/insecte* et *tdh:sound/oiseau*). (#19)
### Modifications :
 - Le système de détection des biomes a désormais des valeurs supplémentaires pour biomeType : 60 pour les forêts persistantes (spruce et dark oak), et 70 pour les marais.

## [0.5.4]	(8fe28f99/53785a28)	- 2020-11-30
### Ajouts :
 - Un code générique existe désormais dans *tdh:porte/porte* pour les grandes portes (portes constituées de plusieurs blocs solides). Seules les portes s'ouvrant vers le haut ("SlideHaut") sont disponibles pour l'instant (il manque SlideBas/Est/Ouest/Nord/Sud qui sont prévues). (#106)
 - L'animation du pont-levis de Fort du Val, ainsi que le code relatif à son ouverture/fermeture manuelles, ont été ajoutés dans *tdh:porte/pont_levis/fort_du_val*. Ce code n'est pas déclenché automatiquement pour l'instant, mais les joueurs sont correctement déplacés avec le pont-levis lorsqu'il est en cours de fermeture.
 - Les effets sonores des mécanismes des portes et des ponts-levis se trouvent respectivement dans *tdh:sound/porte* et *tdh:sound/pont_levis*.
 - Un tag (*tdh/tags/blocks/porte*) contient tous les blocs pouvant être utilisés pour les grandes portes/herses des villes et des châteaux : pour l'instant, ce sont les barres et les blocs de fer, ainsi que toutes les barrières quel que soit le matériau.
 - Un tag (*tdh/tags/blocks/pont_levis*) contient tous les blocs pouvant être utilisés pour les ponts-levis : pour l'instant, ce sont les dalles et les escaliers en bois (les iron bars ne sont pas incluses).

## [0.5.3_3]	(8f4d8d6b)	- 2020-11-23
### Réparations :
 - La neige peut bien fondre lorsqu'il fait beau au printemps/automne si elle a dépassé une hauteur de 1 bloc plein.

## [0.5.3_2]	(c18bbb96)	- 2020-11-22
### Réparations :
 - Le système météo ne devrait plus se bloquer lorsque le jour d'update a été dépassé.

## [0.5.3_1]	(4f4c1ff3)	- 2020-11-22
### Ajouts :
 - Corrections mineures et ajout de wording au système d'incident. (#41)

## [0.5.3]	(1a63f0737)	- 2020-11-19
### Ajouts :
 - Les valideurs Gold et Silver peuvent maintenant dysfonctionner (et être réparés le lendemain à l'ouverture de la station). La probabilité que cela arrive dépend des chances d'incident de la station. (#103)

## [0.5.2b]	(e18d9900)	- 2020-11-19
### Modifications :
 - Lorsqu'un sapling de spruce plante un bloc de flore, il s'agit désormais d'un buisson à baies plutôt que d'une fleur.

## [0.5.2]	(6ffb1b65)	- 2020-11-19
### Ajouts :
 - Lorsqu'on est à la bonne saison et dans un biome forestier, des pousses d'arbres et de la flore peuvent se planter seules. (#136) Bien que ce ne soit qu'à moitié logique, tout ceci est stocké et exécuté dans le code de la météo (*tdh:meteo/effects*).
 - Un tag (*tdh:tags/blocks/sapling_base*) détermine les blocs sur lesquels un sapling peut se planter.
### Modifications :
 - Le type de biome "pluie" (0-99) est maintenant séparé en type "standard" (0) et "forêt" (50) pour permettre de détecter si le replantage d'un arbre est possible ou non.
### Réparations :
 - De la neige ne sera plus posée par le système de hauteur variable de la neige dans des biomes où elle ne pourrait pas tomber, si on se trouve plus haut que la limite de neige.

## [0.5.1b]	(4212b02b)	- 2020-11-19
### Réparations :
 - Les entités temporaires pouvaient sous certaines conditions ne pas déspawner après avoir testé une position (dans le code du niveau variable de la neige).

## [0.5.1]	(c5065338)	- 2020-11-19
### Ajouts :
 - Le niveau de la neige monte lorsqu'il neige et baisse lorsque le temps est dégagé, aux alentours des joueurs se trouvant dans les biomes correspondants. (#142) Il monte plus rapidement lorsqu'il y a des précipitations intenses, et baisse plus vite en été (et pas du tout en hiver).
### Modifications :
 - Le type de biome (neige, pluie ou chaud) est désormais calculé en même temps que le biome actuel. Il est stocké dans la variable <player> biomeType.

## [0.5.0e]	(4e29385d)	- 2020-11-19
 - Les sons de vent et de pluie ne devraient plus partir en boucle infernale lorsque plusieurs joueurs se trouvent à la même position (càd quand quelqu'un est en spectateur de quelqu'un d'autre). (#147)
 - Les musiques ne devraient plus non plus être soumises aux problèmes inhérents à la présence de plusieurs joueurs à la même position.

## [0.5.0d]	(743af0ee)	- 2020-11-19
### Réparations :
 - Le distributeur ne pourra plus être activé par un spectateur. (#145)

## [0.5.0c]	(0b1b4a88)	- 2020-11-19
### Réparations :
 - La fonction *tdh:time/time* utilise désormais la fonction *tdh:store/time* pour le calcul du temps correct, et ne pourra plus être volée par un spectateur (elle s'affiche à tous les joueurs présents à cette position). (#146)

## [0.5.0b]	(486e7d0a)	- 2020-11-18
### Modifications :
 - Les annonces far ont été désactivées en attendant la modification du code des annonces et du RP.

## [0.5.0]	(b9bd744f)	- 2020-11-17
### Modifications :
 - Le tick du SISVE se décrémente même s'il est inférieur à 0 (pour permettre le comptage précis du retard de ticks en pré-station).
 - Les titres du SISVE en station ont été passés en couleurs sombres pour rester lisibles malgré la lumière ambiante.
 - Mise à jour de la version dans le README.
### Réparations :
 - Les trains seront bien placés en fin de service lorsque celle-ci s'est produite après leur mise en file d'attente.
 - Le train repart désormais toujours à vitesse maximale après s'être arrêté.
 - Le train redémarrera correctement après avoir fait un arrêt d'urgence.
 - Le son d'arrivée du métro sera stoppé s'il fait un arrêt d'urgence, et relancé lors de son redémarrage.
 - Le son d'arrivée du métro sera bien joué sur les quais en plus de l'être à l'intérieur du métro.
 - Le son d'arrivée du métro est joué plus tard.
 - Les displayTicks ne tickent plus lorsqu'on est en arrêt d'urgence, puisque c'est remainingTicks qui a ce rôle dans ce cas.
 - Le SISVE pré-station est maintenant séparé du SISVE arrêt d'urgence, pour éviter que le premier prenne le pas sur le second et empêche le métro de repartir normalement.
 - Le système anti-personnes sur les voies respecte bien le tag StationInversee.
 - Le SISVE affichera bien les correspondances lorsqu'on arrive dans une station avec correspondances.
 - La valeur par défaut du timer du SISVE a été légèrement augmentée pour coller davantage à la durée réelle. Il faudra néanmoins refaire un réglage manuel par voie pour que ce soit le plus correct possible.
 - Le joueur sera bien tp sur le quai lors d'un terminus si les portes de quai n'ont pas encore été configurées (la position sera simplement moins précise).

## [0.5.0_pre2]	(0c4baa5a)	- 2020-11-16
### Ajouts :
 - Une fonction *tch:tag/station* vient s'ajouter à celles de la version précédente. Elle sélectionne la station de l'entité qui exécute la fonction et lui donne le tag "ourStation".
### Modifications :
 - Le *tch:auto/debug/infostation* affiche de nouveau des informations correctes.
 - Les portes de quai se ferment désormais 1 seconde avant le départ du métro, pour des raisons de sécurité.
 - Les incidents nocturnes ont été désactivés, en attendant d'améliorer leur génération.
 - L'annonce du PID lorsqu'un train muni d'un joueur arrive en gare est désormais jouée un peu plus tard, pour se trouver immédiatement après la mise en pause éventuelle et d'être ainsi jouée après qu'on soit reparti.
 - Des titres actionbar sont affichés par le SISVE lorsqu'on est en arrêt d'urgence.
 - Les spawns métro ne sont plus traités s'il n'y a pas au moins un joueur proche.
### Réparations :
 - Le métro venant de spawner ne garde pas son tag temporaire et devrait donc plus faire dysfonctionner les annonces sonores (#148).
 - Les fonctions *tch:tag/quai* et *voie* utilisent désormais le score idStation de l'exécutant, s'il en possède un, et pas forcément l'ID de la station la plus proche (#148). Ils intègrent également des messages d'erreurs diffusés aux MetroWorker pour rendre le débuggage plus aisé.
 - Le métro ouvre à nouveau les portes de quai (pour peu qu'on ait correctement placé l'item frame PorteQuai).
 - Le métro s'arrête à nouveau en face des portes. (#151)
 - Les annonces sonores spécifiques aux terminus/non terminus sont désormais jouées correctement.
 - La vitesse d'un métro est désormais correctement enregistrée lorsqu'il arrive en station avec un passager. (#149)
 - L'heure s'affichera correctement quand on est en interstation (la fonction correspondante était manquante).
 - Les joueurs sont correctement mis sur le quai lors de l'arrivée à un terminus.
 - Le son d'arrivée du métro se joue moins longtemps avant l'arrivée.
 - On ne force plus le PID à jouer un message lorsqu'un train en fin de service entre en station.
 - Les messages en actionbar sont correctement affichés quand on est en train d'arriver en station.
 - L'arrêt d'urgence n'est plus exécuté en boucle lorsqu'on arrive en station alors que la voie est occupée.
 - Le tick de test de l'arrêt d'urgence sera bien décrémenté par *tch:metro/rame/pre_station/tick_arret_urgence*.
 - Les spawners ne devraient plus sauter une journée si leur ligne n'a pas encore ouvert.

## [0.5.0_pre1]	(ecb840f2)	- 2020-11-15
### Ajouts :
 - Les fonctions *tch:tag/pc*, *quai_pc*, *quai* et *voie* permettent de tagger les entités importantes sans répéter le même bout de code de comparaison scoreboard en BOUCLE. Il suffit d'appeler l'une d'entre elles en tant qu'une entité possédant un idLine pour ajouter un tag à l'entité tchPC, tchQuaiPC, Direction ou NumLigne correspondante.
 - Le système de téléportation automatique des joueurs se trouvant sur les voies sur les quais a été réimplémenté (il s'agit d'une sous fonction du tick station, dans *tch:station/quai/tick*).
### Modifications :
 - Le code de *tch:auto* et *auto/title* a été entièrement réécrit et déplacé (en grande partie vers *tch:metro*, mais certaines fonctions ont également été placées dans *tch:station* pour plus de cohérence).
 - Les incidents fin de service sont désormais générés, mais sont pour l'instant beaucoup trop fréquents.
 - Une ligne ne change plus ses horaires de début/fin de service en même temps; elle génère le prochain début de service en fin de service, et la prochaine fin de service en début de service.
 - Les spawners métro détectent désormais correctement s'ils doivent s'interrompre en raison d'un incident ou non.
 - Les accidents graves de personne durent maintenant plus longtemps.
 - Un incident ne s'arrête plus immédiatement à la fin de l'intervention ; il reste l'étape de reprise progressive du trafic (status = 4).
 - L'horaire de début de service aujourd'hui est désormais calculé juste après la fin du service, et l'horaire de fin de service aujourd'hui est calculé juste après le début du service.
 - Il n'est plus possible d'avoir une ligne qui fonctionne 24h/24 (le maximum est désormais de 23h, de 4h à 3h).
 - La fonction *tdh:time/tdh_test_tick* enregistre désormais une variable #temps timeFlowing (0 ou 1) pour que d'autres fonctions puissent savoir si l'écoulement du temps est actuellement en cours ou en pause.
### Réparations :
 - Les incidents ne devraient plus interrompre instantanément le trafic et respecteront bien un délai d'information minimal.
 - Un message de debug d'évolution d'incident a été modifié pour être visuellement similaire aux autres.
 - L'heure de fin d'incident est correctement calculée lorsqu'on dépasse minuit.
 - Correction d'un souci des valideurs qui pouvait aboutir à l'ouverture d'une porte différente de celle où on validait.

## [0.4.14]	(b83aa9d0)	- 2020-11-14
### Modifications :
 - Les agents TCH indiquent désormais si une ligne subit un incident actuellement lorsqu'on leur demande les horaires.
 - Le code des incidents (#40) a été amélioré, les incidents sont désormais générés au fil de la journée avec une évolution plus précise que "incident/pas incident". Il y a 5 étapes : génération de l'incident (des chances aléatoires toutes les 15mn) -> détection de l'incident par TCH et arrêt de la ligne -> génération d'un horaire de reprise -> intervention des équipes TCH -> reprise du trafic.
 - La génération est fonctionnelle mais n'a pas encore d'impact sur le trafic des lignes.
 - Les incidents sont très fréquents (bien qu'ils n'aient aucun impact) à des fins de test.
 - Il reste de nombreux messages de debug. Je suis fatigué et je changerai ça demain. RTL, 6h.

## [0.4.13]		(efa54933)	- 2020-11-13
### Ajouts :
 - Comme pour la commande *tdh:store/time*, la commande *tdh:store/time_delay* inclut désormais une fonction jumelle pour l'affichage de 2 délais en même temps (*tdh:store/time_delay2*).
### Réparations :
 - Les agents TCH affichent désormais les bons horaires pour toutes les lignes de métro, et l'ensemble de ces fonctions (*tdh:dialogue/topics/tch/horaires/agent_tch/*) ont été améliorées.
### Suppressions :
 - Les fonctions situées dans *tdh:dialogue/agent_tch* ont toutes été supprimées ; il s'agit de l'ancienne version du système de dialogue avec les agents, plus utilisée à ce jour.
 - Idem pour les fonctions situées dans *tdh:dialogue/grenat_recif*, qui contenaient les anciens dialogues des barmans du Ca'Phare.

## [0.4.12c]	(8983242d)	- 2020-11-13
### Réparations :
 - Correction d'un bug qui ne fermait pas correctement les guichets et stations lorsqu'il était plus de minuit mais que l'heure de fermeture était avant minuit.

## [0.4.12b]	(f620d2c7)	- 2020-11-13
### Modifications :
 - Il est maintenant possible de modifier dans le configurateur station les horaires d'ouverture du guichet séparément pour la semaine et le week-end.
 - Le test de porte de valideur (*tch:station/valideur/porte/test_porte*) comprend désormais le cas où la porte de valideur est 1 bloc plus bas qu'attendu (comme à la station Midès).
### Réparations :
 - Correction d'un bug qui tentait parfois d'ouvrir la porte au mauvais endroit lorsqu'on validait un ticket.
 - Correction de plusieurs bugs qui affichaient la mauvaise date de fin de validité lorsqu'on venait de valider un ticket.
 - Correction d'un bug qui faisait que les guichets ouvraient beaucoup trop tard le week-end.
 - Correction d'un bug qui répétait rapidement le message d'erreur "Porte introuvable" sur les valideurs Gold.
 - Les fonctions *tch:info/sortie* et *bienvenue* ne s'exécuteront plus s'il n'y a pas de joueur à proximité.

## [0.4.12]	(64ece674)	- 2020-11-12
### Ajouts :
 - Il y a désormais une fonction de recherche externe des abonnements (*tch:abonnement/_find*).
 - Il y a désormais une fonction permettant de calculer et d'afficher une date à partir d'un idJour (*tdh:store/day*).
 - Une seconde fonction d'enregistrement d'un horaire a été ajoutée (*tdh:store/time2*) ; elle est identique à *time*, mais permet l'affichage d'un second horaire dans la même commande (pour une plage, par exemple).
### Modifications :
 - Les valideurs TCH ont évolué : les tickets sont désormais validés sans bouton, par un simple drop sur la plaque de pression située au-dessus dudit bouton. L'ancien système n'est pas encore obsolète pour permettre la transition, et les nouvelles fonctions sont dans un sous-dossier de stations puisqu'elles sont soumises au tick station (*tch:station/valideur/*). (#135)
 - Les valideurs Gold ne nécessitent plus non plus d'appui sur un bouton, il suffit de passer le portique en tenant sa carte Gold en main. (#135)
 - Les stations ont maintenant un horaire d'ouverture et de fermeture séparé pour les guichets et la station elle-même. (L'horaire de l'ouverture de la station est calculé automatiquement en fonction des lignes présentes). L'horaire de la station détermine l'accessibilité des valideurs, et l'horaire du guichet les heures où les agents sont forcés d'être en poste.
 - Les nouvelles fonctions de validation et d'ouverture/fermeture des stations utilisent bien la variable idJour au lieu de dateJour/Mois/Annee, et la date n'est convertie qu'au moment de l'affichage. (#110)

## [0.4.11c]	(b7df1c53)	- 2020-11-11
### Réparations :
 - Un bug qui faisait apparaître 2 messages d'erreurs à la fois lorsqu'on validait un titre invalide a été réparé.

## [0.4.11b]	(d98708d6)	- 2020-11-11
### Modifications :
 - Le tick station (qui s'occupe de repositionner les vendeurs TCH à la bonne position) a été déplacé dans une fonction spécifique (*tch:station/tick*) pour permettre de l'exécuter plus fréquemment que le reste de la logique de *tch:auto/*.
### Réparations :
 - Correction d'un souci dans la détection des vendeurs TCH qui les faisait se re-téléporter à leur position chaque seconde, même s'ils étaient déjà au bon endroit.
 - Un vendeur TCH ne pourra plus être TP trop loin de son guichet.
 - Correction d'un souci qui faisait se tp les agents TCH dans le sol lorsqu'ils étaient sur une double dalle.
 - Les variables liées à la configuration devraient bien se réinitialiser lors d'une déconnexion/reconnexion, tout comme les tags correspondants.

## [0.4.11]	(4f8b3b61)	- 2020-11-10
### Ajouts :
 - Le configurateur de stations est maintenant fonctionnel. Il permet de régler les paramètres associés à cette station, comme les probabilités d'incident matériel ou les horaires d'ouverture des guichets.
 - Un système de configuration générique est désormais disponible, pour éviter la duplication de code similaire. Les configurateurs de ligne et de station utilisent donc cette nouvelle interface.
 - La fonction *tdh:store/time* prend un argument (sous forme de score) en ticks, et enregistre son écriture correcte dans un storage pour utilisation immédiate. (par ex, on lui donne "2000" et elle génère "1h00")
 - Les vendeurs TCH sont désormais replacés automatiquement devant le guichet lorsqu'on est dans les heures d'ouverture du guichet. Cette fonction ne sera pas active tant qu'elle ne sera pas paramétrée dans le configurateur de station, et nécessite plusieurs item frames pour marquer les différentes positions. (#111)
### Modifications :
 - La fonction *tch:auto/get_id_station* n'affichera plus de message d'erreur lorsqu'elle rencontrera 3 astérisques comme nom de station (ce qui correspond à "Aucune station", mais n'est pas une erreur en soi).
 - Le configurateur de ligne inclut désormais des infobulles qui indiquent la valeur précise fournie si on clique sur le bouton correspondant.
 
 ## [0.4.10b]	(c0c5c8c0)	- 2020-11-09
### Modifications :
 - La fonction *tch:ligne/reset* est maintenant séparée de l'initialisation *init* pour éviter de reset les réglages des lignes en faisant l'initialisation.

## [0.4.10]	(769ff081)	- 2020-11-09
### Ajouts :
 - Les playsound d'arrivée en station sont maintenant disponibles pour toutes les lignes de métro (on a ajouté 5a, 6, 7b, 8b).
### Modifications :
 - Il est désormais possible de rentrer une valeur précise dans le configurateur, au lieu de choisir une valeur prédéfinie.
 - Le configurateur affiche désormais tous les paramètres actuels de la ligne lorsqu'on est au menu principal.
### Réparations :
 - Correction de divers soucis d'affichage dans l'interface de configuration des lignes TCH.
 - Les temps de spawn des métros ont été corrigés, et ne devraient plus être démesurément courts par rapport à la durée théorique.
 - La variabilité des temps de spawn des métros a été réduite.
 - Les messages de prochain métro ne devraient plus pouvoir afficher "Xh60".
 - Les spawners de l'ancien système spawneront à nouveau correctement leurs métros.
 - La fin de service lorsqu'on est dans un métro a été restaurée.
 - Les fonctions hgive de totem M7 -> Evenis Tamlyn donnent désormais les bons panneaux.
 - La fin de service ne devrait plus s'exécuter de manière répétée tôt le matin.

## [0.4.9d]	(d47388ca)	- 2020-11-09
### Ajouts :
 - Ajout d'un biome de test, tch:tunnel_metro (il n'est pour l'instant pas fonctionnel en raison de l'impossibilité de le manipuler avec WorldEdit tant qu'une mise à jour ne supportera pas les biomes personnalisés). (#134)
 - Ajout d'une interface de paramétrage des lignes TCH (*tch/functions/ligne/choix_ligne* et *config*), qui permet de gérer des variables comme les horaires de service, les horaires de week-end ou la fréquence des rames.
### Modifications :
 - Le calcul des incidents est désormais lancé depuis *tdh:time/tdh_tick*, comme la plupart des événements qui doivent s'exécuter une fois par jour. Le calcul des horaires TCH du jour est lancé de la même manière, les deux étant regroupés dans une fonction *tch:auto/next_day* exécutée chaque jour à 4h du matin.
 - Remplacement de la variable "dateJour/Mois/Annee" par "idJour" dans le système *tch:auto*. (#110)
 - Réécriture du code générant l'heure de spawn des métros pour lui faire suivre les fréquences indiquées par la configuration de la ligne.
 - Les items stockant le motif incident ont été remplacés par un storage.
 - Les chauves-souris ne sont plus incluses dans *tdh:random* (ni, par conséquent, dans *tdh:amical*) en raison d'un souci de rotation dans leur NBT.
 - Les cartes de l'est de Midès/Litoréa sont désormais incluses dans *cgc:updatemaps*, et de nouvelles schedules ont été ajoutées spécifiquement pour les actualiser (il s'agit des fonctions *start_new* dans les dossiers *updatemaps/main/-1024*, *0*, *1024*, *2048* et *3072*).
 - *tch:auto/debug/infostation* affiche désormais l'ID des directions de chaque ligne.
### Réparations :
 - La zone d'affichage du message "Prochain train dans XX minutes" devrait désormais être plus réduite (#122)

## [0.4.9c]	(a63f0bbc)	- 2020-11-06
### Modifications :
 - Le point de spawn Chizân a été supprimé.
### Réparations :
 - Le point de spawn de l'auberge du Profane Éméché a été modifié, car le bâtiment ingame a changé. (#137)
 - Les rotations des points de spawn sont désormais correctement prises en compte.

## [0.4.9b]	(40abbb39)	- 2020-11-05
### Ajouts :
 - Les tags *tdh:neutre* et *tdh:amical* ont été ajoutés, et les mobs les plus récents ont été ajoutés à *tdh:hostile*. (#55)
 - Le tag *tdh:random* regroupe ces trois tags, ainsi que quelques autres entités, pour déterminer toutes celles qui peuvent être utilisées pour récupérer des nombres aléatoires. (#55)
### Modifications :
 - De nombreuses fonctions ont été modifiées pour inclure le tag #tdh:random comme générateur de nombre aléatoire au lieu de réaliser de nombreux checks de type d'entité, et certaines autres pour checker #tdh:hostile au lieu d'une liste longue comme le bras de mobs (même si la plupart de ces fonctions doivent être réécrites plus en profondeur). (#55)

## [0.4.9]	(fc4bef4a)	- 2020-11-05
### Ajouts :
 - Les fonctions *tdh:reload/begin* et *end* ont été implémentées. Elles sont destinées à supplanter la commande /minecraft:reload par l'introduction d'un alias bukkit plus court (**/rld**), qui pointe vers *begin* et écrit un message dans le chat au début et à la fin du reload et joue le son idoine (#117).
 - Quelques fêtes ont été ajoutées au calendrier, notamment la séparation de la "Fête Nationale" en plusieurs fêtes pour les villes principales. (#37)
### Modifications :
 - Suppression de plusieurs voix multilingues des annonces à bord pour accomoder une contrainte de non-séparation du jingle terminus d'avec la voix disant "Terminus" (#75), et réorganisation du fichier *tch:auto/title/tick_detail* pour plus de lisibilité.
 - Séparation de *tdh:time/init* et de l'initialisation des jours spéciaux (qui est déplacée dans *tdh:time/init_special_days*) pour permettre de les initialiser lors d'un update sans réinitialiser l'état du calendrier.
### Réparations :
 - Le jingle "Terminus" se jouera désormais correctement lors d'une fin de service (#124) ; la fonction *tch:auto/title/tags_check* vérifie si le minecart a le tag "FinDeService" et agira dans ce cas comme si c'était le terminus.
 - La fonction /day (*tdh:time/print_date_anytime*) n'affichera plus le jour spécial à tous les joueurs. (#128)

## [0.4.8g]	(8f77fb0f)	- 2020-10-26
### Modifications :
 - Inversion des lignes 1 et 2 sur les branches affectées, dans la quasi-totalité des fonctions concernées (itinéraire, id ligne, annonces...).
 - Renommage du fichier *main* en *_main* dans *hgive/logo*, *hgive/fond*, et *hgive/plan/lignes/totem*.
 - Les totems incluent désormais une détection de station, qui affiche tous les totems correspondant à cette station.
### Réparations :
 - Les noms de station Grenat–Ville, Le Hameau–Rive Gauche, –Rive Droite et –Glandebruine Nord sont désormais munis de tirets semi-cadratins au lieu de tirets simples, dans le fichier *get_id_station*.
 - Les ID station Désert Onyx et Alwin–Onyx ne sont plus inversés dans le hgive/station.

## [0.4.8f]	(179720fd)	- 2020-10-25
### Modifications :
 - Les logos du hgive ont désormais une détection des lignes proches. Certains logos manquants ont également été rajoutés, en compatibilité avec la mise à jour 3.0_pre13.6 du RP.

## [0.4.8e]	(26baa38c)	- 2020-10-25
### Ajouts :
 - Les panneaux correspondance et accès aux quais du hgive intègrent désormais une détection des lignes proches pour proposer les panneaux les plus pertinents, comme pour les panneaux nom station en 0.4.8.
### Modifications :
 - Renommage de plusieurs fonctions *main* de hgive en *_main*, pour rendre l'arborescence plus lisible.
 - Les couleurs du texte dans les tellraw des sections correspondance et accès aux quais du hgive sont désormais à la charte.

## [0.4.8d]	(6cb88e64)	- 2020-10-25
### Réparations :
 - Il y avait un lien mort dans le hgive lorsqu'on cliquait sur station, car il essayait de rediriger dans *station/sb* alors que le dossier en question a été supprimé en version 0.4.8b.

## [0.4.8c]	(119a067f)	- 2020-10-24
### Modifications :
 - La couleur de la 6 (à l'origine #007852) a été remplacée et est désormais unique (#60bb39).

## [0.4.8b]	(e93aad27)	- 2020-10-24
### Ajouts :
 - Les panneaux nom station existent désormais en version décalée (permettant l'affichage sur une largeur paire).

## [0.4.8]	(2438cd4b)	- 2020-10-24
### Ajouts :
 - Les totems du resource pack (câble et métro) ont été ajoutés au hgive. Les anciennes références aux totems des lignes 1 à 3 ont été supprimées.
 - Les nouveaux panneaux de nom de station ont été ajoutés au hgive, en version "taille métro" centrée (~= 2x1). Les stations qui disposaient d'autres panneaux que le standard ont été conservées dans le hgive.
 - Le plan du réseau V2 et sa légende ont été ajoutés aux plans du hgive.
### Modifications :
 - Les ID station d'Évrocq–Le Bas et Les Pics ont été ajoutés à *tch:auto/get_id_station*. XXMetroB a été renommée Moldor–Balchaïs dans le même fichier.

## [0.4.7d]	(0847e83e)	- 2020-10-18
### Réparations :
 - Corrections mineures.

## [0.4.7c]	(cc5d7ef5)	- 2020-10-18
### Réparations :
 - Corrections de plusieurs chemins d'accès éronnés à la suite de mises à jour supprimants les fonds stonebricks (*logo*, *stations* et *sorties* notamment impactés). (#114)
 - Déplacement des dossiers aux racines idoines sans le passage */sb** obligatoirement.
### Modifications :
 - Suppressions de tous les affichages n'étant pas en fond stonebrick dans le cadre du passage progressif en fond transparent. (#116)

## [0.4.7b]	(f57ea064)	- 2020-10-14
### Réparations :
 - Le MOTD n'affichera plus "Il fait jour" s'il fait nuit.

## [0.4.7]	(4b11f558)	- 2020-10-13
### Ajouts :
 - Les sujets de dialogue génériques **Métier**, **Météo**, **Manger** et **Boire** ont été implémentés. (**Chambre** existe également dans le code, mais n'est pas utilisable pour l'instant)
 - De nouveaux rôles (tags) pour les PNJ avec dialogues incluent Aubergiste, Tavernier et Hôtelier. Ces derniers ont de nouveaux greetings spécifiques.
 - Une fonction exécutée toutes les secondes par *tdh/functions/player/tick* (*tdh:dialogue/reparer_dialogue*) s'assure que les PNJ pouvant dialoguer ne génèreront pas de nouvelles offres (pour modifier le comportement par défaut de Minecraft qui re-génère les mêmes offres lorsqu'on décharge puis recharge un villageois dont on les a supprimées).
 - Une nouvelle fonction, *tdh:village/fill_inventory*, permet de remplir l'inventaire d'un PNJ pour éviter qu'il ne ramasse les objets qu'il donne au joueur.
### Modifications :
 - Le storage du mois actuel enregistre désormais une variable deDu (pour des formulations du type "au mois d'avril" ou "au mois de décembre").

## [0.4.6b]	(deba1757)	- 2020-10-12
### Ajouts :
 - Les sons de rafale de vent sont désormais joués lorsque l'intensité du vent est de 2 ou davantage. (#13)
 - Des particules sont désormais présentes lorsque l'intensité de la pluie ou de l'orage dépasse 2. (#26)
### Modifications :
 - Le threshold pour le passage de pluie à neige a été remonté de 3 blocs (par exemple de 90 à 93 dans les montagnes).
 - La vitesse des joueurs a été réduite à 70% de leur vitesse de base, dans le cadre de la réécriture du système d'altitude.
### Réparations :
 - La commande /vbrain devrait à nouveau afficher des informations correctes.

## [0.4.6]	(647b7b7c)	- 2020-10-11
### Ajouts :
 - Les sons de vent ont été ajoutés à nouveau et dépendent de l'intensité de la météo actuelle. (#13) (mais les sons de rafales ne sont pas encore ajoutés)
### Modifications :
 - L'intensité (*tdh:meteo/auto/tick/gen_intensite*) a été modifiée pour pouvoir exister également par temps calme (cela indique simplement du vent).
 - La fonction debug météo (*tdh:meteo/auto/afficher_previsions*) affiche désormais l'intensité prévue.
### Réparations :
 - L'intensité peut désormais varier pendant un orage.
 - Les sons de pluie ne devraient plus être audibles dans le biome snowy tundra.

## [0.4.5e]	(d7f51f22)	- 2020-10-11
### Réparations :
 - La fonction de recherche d'abonnement a été réparée, et ne devrait plus occasionner de messages type "0 jours d'abonnement restants" ou les données de l'abonnement d'une autre personne.
 - Les sons de pluie ne devraient plus être audibles dans les biomes trop chauds ou enneigés.

## [0.4.5d]	(87c5e229)	- 2020-10-11
### Réparations :
 - L'agent TCH a désormais bien une ligne de dialogue lorsqu'il donne sa carte Gold à un joueur. (#108)

## [0.4.5c]	(34b7df45)	- 2020-10-11
### Réparations :
 - Le bug qui donnait tout de même un ticket lorsqu'on validait avec l'inventaire plein a été corrigé. Un joueur qui essaie de valider sans avoir de place pour récupérer le ticket sera désormais notifié. (#107)

## [0.4.5b]	(f5dfbd23)	- 2020-10-11
### Réparations :
 - Suppression d'un message de debug qui s'affichait à chaque itération du système météo.

## [0.4.5]	(1a629e45)	- 2020-10-11
### Ajouts :
 - Les sons de pluie ont été réimplémentés, et sont compatibles avec la nomenclature du RP 3.0_pre12. (#13)
### Modifications :
 - Le système météorologique a été réécrit, et ne devrait plus causer de lag lors du calcul des variables aléatoires (#8).
 - Les probabilités de chaque type de météo ont été légèrement modifiées, notamment en automne et en hiver avec davantage de précipitations.
 - Les prévisions météo ont été modifiées ; elles sont toujours affichées le lundi à 8h, mais peuvent également être consultées à tout moment avec la commande /meteo (*function tdh:meteo/auto/afficher_previsions*)
 - Multiplication par 2 (10 -> 5 ticks) de la fréquence du calcul de l'insideness.
 - Implémentation de la variable idJour dans le système de calendrier, qui permet de faire une comparaison entre 2 dates en comparant une seule variable au lieu de 3. (#51)
### Réparations :
 - Correction d'un bug dans le message de nouvelle journée.

## [0.4.4_3] (7a90f83d)	- 2020-10-09
### Modifications :
 - Le skip automatique jusqu'au matin en fin de nuit lorsqu'on dort a été temporairement désactivé, en raison de problèmes qu'il entraînait sur la synchronisation du temps.
### Réparations :
 - Correction d'un souci qui entraînait un double affichage du message de nouvelle journée, et un affichage quotidien des prévisions météo.

## [0.4.4_2] (9338bb04)	- 2020-10-09
### Réparations :
 - Fix de toutes les différentes fonctions qui reposaient sur fakePlayer en les redirigeant vers les nouvelles variables (situées dans #temps).

## [0.4.4_1] (4fd10532)  - 2020-10-08
### Réparations :
 - Correction d'une erreur de syntaxe dans une fonction déclaré dans l'issue #87.

## [0.4.4] (a7390ac6)  - 2020-10-08
### Ajouts :
 - Le système de localisation (*tdh/functions/player/locate/*) permet de calculer la distance approximative de chaque ville des Terres du Hameau, et de savoir laquelle est la plus proche de la position d'un joueur.
### Modifications :
 - Le MOTD inclut désormais les informations de localisation de *player/locate* si le joueur est suffisamment proche d'une ville.
 - Le système calendaire utilise désormais des scoreboards spécifiques (#temps et #tempsOriginal). (#51)
 - Le système calendaire utilise désormais le data storage pour enregistrer le texte des jours et des mois. Ces derniers sont accessibles dans tdh:datetime.
 - Le système de jours "spéciaux" a été modifié pour permettre l'implémentation de dates supplémentaires, et faciliter le calcul de variables pour les dates existantes.
 - Le calcul des années bissextiles est désormais réalisé une seule fois par an, au moment du changement d'année, et stocké.
### Réparations :
 - Un bug qui faisait défiler le temps CGC trop vite par rapport au temps TDH lors du sommeil a été corrigé.

## [0.4.3] (a5e958de) - 2020-10-07
### Ajouts :
 - Le MOTD affiché lors de la connexion n'est plus géré par Essentials mais par le datapack (dans *tdh/functions/player/on_connect/*). Il inclut à présent l'heure réelle (calculée par le tick TDH). (#85)
### Modifications :
 - Le nom du jour de la semaine et du mois sont désormais stockés dans un /data storage, afin de ne faire le calcul qu'une fois par jour au lieu de le réitérer à chaque nouvel appel de la date.
 - La date affichée par la commande /day inclut désormais le mois en toutes lettres.
 - La réinitialisation de la musique lors de la connexion (*tdh/functions/sound/music/tick/reset_disconnect*) est désormais appelée par le tick de connexion global (*tdh/functions/player/tick*), et non plus par *sound/music/tick*.

## [0.4.2] (9481c5b9) - 2020-10-06
### Ajouts :
 - Le système /pgive (*tdh/functions/give/panneau/*) permet, en conjugaison avec le RP, de placer des panneaux directionnels.
### Modifications :
 - Les logos métro n'ont plus de nom, puisqu'ils s'affichaient sinon lors du pointage souris.

## [0.4.1] (00dae812) - 2020-10-04
### Ajouts :
 - De nouveaux tags de bloc (*tdh/tags/blocks/ground_air_water*, *ground_cave_air*, *ground_cave_air_water* et *river*), pouvant être utilisés avec des outils WorldEdit.
### Modifications :
 - Les logos métro (*tch/functions/give/logo/*) n'existent plus qu'en version transparente, et sont désormais des items avec modèle personnalisé (gérés par le resource pack) et non des maps.
 - La carte du système d'itinéraire et le réseau métro utilisent désormais des couleurs personnalisées.
### Réparations :
 - Les heads d'information (*tdh/functions/give/info/*) ont été convertis pour être compatibles avec la 1.16
 - Les blocs de début et de fin de dialogue (*tdh/functions/dialogue/text_block_*) ont été corrigés, car les espaces insécables ne sont désormais plus supportés.
 - Correction d'un bug dans la fonction *tdh/time/tdh_tick* qui entraînait le passage accéléré du temps de manière continue.
 - Correction de la fonction *tdh/tp/zombie_pigman*, puisque le nom du mob a changé pour zombified_piglin.
 - Correction des loot tables pour prendre en compte le changement de syntaxe des attributs en 1.16

## [0.4] (dfff33e3) - 2020-09-29
### Ajouts :
 - Pannonçeaux d'information d'accès TVQ (Transit Via Quai)
 - Ecrans d'information trafic 3x2 (v.4) proposant désormais plusieurs messages en rotation.
 - Ajout des panneaux de stations Argençon et Bussy-Nigel
### Modifications :
 - Corrections de certaines syntaxes et mises en forme de la catégorie Accès aux quais.
 - Adaptabilité de la majorité des panneaux pour le support de la transparence in-game.
 
## [0.3.3c] (2df089dc) - 2020-07-07
### Ajouts :
- Différents tags pour aider au worldeditage du monde : *tdh:tree* pour les blocs d'arbres (feuilles et troncs) avec une version supplémentaire par type d'arbre (ex: *spruce_tree*), *ground* pour le décor naturel (herbe, terre, pierre, minerais...), *ground_air* (idem + air, mais pas cave_air), *terracotta* (toutes les teintes de terracotta), *glazed_terracotta* (idem pour la glazed terracotta), *flora* (toute la petite végétation type fleurs / buissons / herbe).
### Divers :
- Mise à jour de la version du pack dans *pack.mcmeta*.

## [0.3.3b] (c8bec9a0) - 2020-07-07
### Ajouts :
- Un embryon de code pour les aiguillages, et un premier aiguillage gérable via ce code (l'inversion des lignes 1 et 2, au nord du Hameau).
### Réparations :
- La station Ygriak était nommée "Villonne" dans le système de calcul d'itinéraire.

## [0.3.3] (a2b5d6c2/0a740560) - 2020-07-05
### Ajouts :
 - Les BUS de remplacements sont maintenant disponible sur TDH (ajouts de sons relatifs).
 - Panneaux accès bus de remplacements créés.
 - Ecrans d'information trafic 2x2 (v.4) proposant désormais plusieurs messages en rotation.
### Modifications :
 - Mise à jour des panneaux des lignes 4 et 6 (sorties-correspondances) en lien avec les dernières modifications de tracés.
 - Mise à jour des PID de la ligne 6 inhérente aux dernières modifications de tracés.
### Suppressions :
 - Anciennes versions des ecrans d'information trafic (v.3).

## [0.3.2] (bf83072c) - 2020-07-03
### Modifications :
- L'initialisation de la carte du métro au PCC initialise également le texte des correspondances. (#92) Elle initialise aussi la nouvelle variable idTerminus, qui donne l'ID de la ligne dont ce quai est le terminus.
- Les fonctions de *tch/functions/auto/title/* utilisent désormais les données disponibles au PCC pour éviter les erreurs dues aux aléas de chargement des chunks (#92).
- Les titres affichés par toutes les fonctions de *tch/functions/auto/title/* utilisent désormais tous une charte graphique plus adaptée à la nouvelle luminosité des tunnels : dark_gray/dark_red. (#93)
- Les titres affichés par lesdites fonctions sont désormais affichés à *tous les joueurs* dans un rayon de 2 blocs, et non seulement le joueur le plus proche du métro.
### Réparations :
- Les métros n'afficheront plus "17h60" et autres soixante-et-unièmes minutes inopportunes.
### Suppressions :
- La fonction *tch/functions/auto/title/display* (vide) a été supprimée.
- Les anciennes versions des fonctions *display_xxx* (*tch/functions/auto/title/pre_station_xxx*) ont été retirées.

## [0.3.1] (a52587bc/e272a167/b0aeca07) - 2020-06-27
### Ajouts :
 - Midès a été ajoutée à l'antispawn.
 - Le RER est désormais présent dans le code, bien qu'il ne dispose pour l'instant d'aucune voie de circulation.
 - Le tag d'entités *tdh:hostile* liste tous les mobs étant affectés par l'antispawn (#55) et inclut désormais les slimes (#69).
 - Un système d'arène à une position aléatoire a été ajouté (dans *tdh:arena/wild*). Plusieurs loot tables ont été ajoutées dans *tdh:arena*. (#57)
 - Un tag IsSleeping est désormais présent sur les joueurs allongés, et est utilisé pour savoir s'ils sont allongés au lieu de toujours utiliser les données NBT.
 - Ajout des sons arrivée station de la ligne 5b dans *tch:sound*.
### Modifications :
 - L'antispawn supprime désormais les entités non traitées appartenant au tag *tdh:hostile*, et plus les types d'entités individuels. (#55)
 - Le respawn se fait désormais à proximité du point de décès, et non à un point préétabli, lorsqu'une arène est active.
 - L'annonce des PID "prochain train dans une minute" est désormais déclenchée par la fonction *tch:auto/spawn_minecart_tick*, et ne se jouera plus lorsqu'un incident est en cours ou, de manière plus générale, lorsqu'aucun métro n'a spawné. (#78)
 - Les annonces "fin de service" et "ligne fermée" ne commencent plus par "Votre attention s'il vous plaît", et ne se terminent plus par un dropmic. (#77)
 - Le jingle "Terminus" est désormais diffusé après le 2ème nom de station dans *tch:auto/title/tick_detail*. (#75)
 - La distance de détection des entrées TCH (tag tchEntrance) a été augmentée dans *tch:auto/player/tick*.
 - Ajout des panneaux accès aux quais avec fond transparent pour toutes les lignes de métro, le câble A et le RER A (*tch/functions/give/acces_quais/*).
### Suppressions :
 - Le message de debug "Incident sur la ligne MX" a été désactivé, puisque le système n'a pas montré de signes de dysfonctionnement.
### Réparations :
 - Les sons "terminus" et "rienoublier" se télescopent lorsqu'un terminus a également des correspondances. (#75)
 - Les annonces d'incident se coupent et se répètent, au lieu de se jouer du début à la fin, lorsque plusieurs métros tentent de spawner sur la même ligne. (#73)
 - Une ligne fermée ne spawnera plus de métros. (#83)
 - Le système d'itinéraire nommait le quai du M4 à Verchamps "Le Hameau M4".
 - Les horaires de reprise n'étaient pas correctement dits dans le fichier "tch:auto/annonce/sound/compose/horaire".
 - Le son Rien oublier n'est désormais diffusé que si une correspondance existe à la prochaine station.

## [0.3.0_3 (b7321943/9c39ec24)	- 2020-06-26
### Ajouts :
 - Ajout de la catégorie BUS et de plusieurs lignes dans les Totems de stations

## [0.3.0_2] (38e0b37b)	- 2020-05-29
### Réparations :
 - Correction de divers bugs liés aux mises à jour précédentes

## [0.3.0_1] (7874650f)	- 2020-05-23
### Ajouts :
 - Ajout de nouveaux items d'affichages dans *tch:give/station*
 - Mise à jour de certains visuels
 - Correction de divers bugs liés aux mises à jour précédentes

## [0.3.0] (aa853f60/e883984a)	- 2020-05-12
### Ajouts :
 - Le spawn du monde (PC global TCH) et Hadès ont été ajoutés à l'antispawn.
 - Les stations Rives de Laar, Fénicia, La Dodène et Oréa-sur-Mer ont été ajoutées au code de TCH.
 - Le système d'annonces en station est désormais opérationnel. Les PID (item frames spécifiquement paramétrées avec le tag **PID** et un score idLine) diffusent le temps d'attente avant le prochain métro, et les hauts-parleurs (item frames avec le tag **tchHP** et initialisées via *tch:auto/debug/infostation*) diffusent des messages génériques, ainsi que des annonces composées donnant des informations sur le trafic et les incidents. (*tch:auto/annonce*).
 - L'agent TCH peut désormais donner un itinéraire au joueur (*tdh:dialogue/topics/tch/itineraire/agent_tch/*). Pour l'instant, le calcul se fait toujours en mode 1 (le plus rapide).
 - Lorsqu'un combat se termine, la fin de musique de combat correspondant à la musique en cours sera jouée pour créer une transition plus douce entre combat et idle.
 - Un tag (*tch:tags/blocks/rer*) a été ajouté pour contenir tous les blocs pouvant constituer un RER, en vue de son implémentation prochaine.
 - Le haut parleur a été ajouté au système tgivei (*tdh:give/info/haut_parleur*).
 - Ajout de *tdh:player/title*, un début de centralisation des titres affichés au joueur en actionbar (et peut-être même, dans le futur, les titres principaux) afin d'éviter les checks redondants et les conflits entre plusieurs titres simultanés.
### Modifications :
 - Le tick du système de musiques (*tdh:sound/music/tick*) a été ramené de 1 seconde à 1 tick.
 - Pour préserver les performances, le système de debug des musiques et ambiances sonores ne fonctionne plus en tenant le CD ; il est désormais accessible via les commandes *tdh:player/title/_music* et *_ambient*, respectivement.
 - Le système d'annonce sonore en métro (*tch:auto/title/*) a été modifié pour intégrer les versions découpées des annonces. Les triggers PreStation50..200 ont donc été désactivés, et les annonces sonores sont désormais alignées sur le tick du titre en actionbar.
 - La manière dont le son d'arrivée métro est émise a été modifiée, pour tenir compte du découpage de ce son en deux fichiers sonores distincts dans le RP (un avant l'arrêt, et un après l'arrêt).
 - La station XXMetroA a été renommée "Quatre Chemins" (notamment dans *tch:auto/get_id_line* et *get_id_station*).
 - Les mots-clés de dialogue débloqués ont été condensés sur leur écran de sélection (*tdh:dialogue/topics/choose_topic*).
 - L'agent TCH n'accepte plus l'achat d'une carte Gold si on en a déjà une sur nous.
 - Un écran de sélection des villes a été mis en place dans *tdh:dialogue/topics/choose_lieu* (depuis *choose_topic*).
 - Le code de la recherche d'itinéraire a été modifié pour prendre en compte les changements de tracé des métros 5b et 6.
 - La recherche d'itinéraire sépare désormais conceptuellement les quais, stations et sorties.
 - L'output dans le chat de la recherche d'itinéraire a été désactivé, puisque les agents TCH peuvent désormais le donner.
 - La commande *tch:auto/debug/infostation* inclut désormais des informations sur les PID de chaque ligne.
 - La musique de combat est maintenant persistante dans un même combat ; elle se jouera en boucle pendant toute la durée du combat, et s'enchaîne avec elle-même de manière fluide.
 - Les fonctions random_music_xxx (dans *tdh:sound/music/*), qui s'occupaient de générer un morceau aléatoire et de jouer le fichier son correspondant au joueur, ont été séparées en deux : la seconde fonction est désormais remplie par music_xxx, dans le même dossier, ce qui permet de jouer à nouveau la même musique sans en générer une aléatoirement.
### Suppressions :
 - La musique "tdh:music.combat.risetoreality" a été retirée du système de musique, puisqu'elle l'a aussi été du resource pack en version 3.0_pre6b.
### Réparations :
 - Correction d'un bug dans la recherche d'itinéraire, qui listait plusieurs correspondances inutiles en début de trajet dans une station comprenant plusieurs lignes.
 - Correction d'un bug dans *tch:auto/pause_voie_occupee*, qui faisait que l'attente avant de vérifier si on pouvait redémarrer était extrêmement longue.
 - Les valideurs standard orientés vers l'ouest n'ouvraient plus la porte lorsqu'on tenait un titre de transport valable.
 - Le message de terminus affiche désormais la bonne ligne. (#67)
 - Les valideurs devraient désormais tester correctement la possibilité d'ouverture de la porte. (#64/#68).
 - Les phantoms devraient désormais rester en combat lorsqu'ils remontent dans les airs entre deux attaques.
 
## [0.2.4_1] (4bcb8962)	- 2020-05-10
### Ajouts :
 - Ajout de nouveaux items d'affichages dans *tch:give/sortie*
 - Mise à jour de certains visuels
 - Correction de divers bugs liés aux mises à jour précédentes
 - Ajout de logo TCH dans *tch:give/logo*
 - Restructuration des fichiers et dossiers contenus dans *tch:give/station*

## [0.2.4] (029aabe6)	- 2020-05-03
### Ajouts :
 - Ajout d'une centaines de nouveaux items d'affichages dans *tch:give/*.
 - Conversion des affichages *tch:give/station/sb/' en fond transparent en préparation de futures mises à jour
 - Harmonisation de certains coloris et fonds d'affichages
 - Suppression progressive des autres choix de fonds proposés

## [0.2.3] (029aabe6)	- 2020-04-29
### Ajouts :
 - Corrections d'erreurs liées à la mise à jour précédente
 - Ajout de panneaux TCH directionnels et correspondances

## [0.2.2b] (cbd8889c)	- 2020-04-28
### Ajouts :
 - La catégorie "Végétal" dans le /tgive (*tdh:give/deco/vegetal*).
 - Nouveau point de spawn à Litoréa, l'auberge du Grand Large (*tdh:player/death*).
 - Litoréa et XXMetroA (le nom définitif sera probablement "Quatre Chemins") ont été ajoutés à l'antispawn (*tdh:village/protect*).
 - Une famille d'advancements a été ajoutée pour stocker les *mots-clés de dialogue* débloqués par chaque joueur (*tdh/advancements/dialogue*).
 - Les stations Thoroïr–Valdée et Grenat–Arithmatie ont été ajoutées au système *tch:auto*.
 - Un système de calcul d'itinéraire a été ajouté à *tch:auto*. Il n'est pour l'heure utilisable qu'en entrant les commandes soi-même, mais sera bientôt intégré aux agents TCH (*tch:auto/itineraire*).
 - Les valideurs ne laisseront désormais plus rentrer personne si toutes les lignes qui passent à la station sont en fin de service.
 - Les cartes gold (abonnements TCH) sont maintenant disponibles (*tch:abonnement/*) ; obtenables auprès des vendeurs TCH (*tdh:dialogue/*), utilisables dans les valideurs dédiés (*tch:valideur/__gold_<DIRECTION>*), et rechargeables dans les distributeurs (*tch:distributeur/*). Le système s'actualise chaque jour à minuit depuis *tdh:time/nextday*.
 - Un système de mesure (*tch:auto/debug/mesure/*) permet de calculer la distance et le temps exacts entre 2 points du réseau métro.
### Modifications
 - L'interface /tgive est désormais totalement cliquable.
 - Le code des dialogues est maintenant générique (les agents TCH ne représentant qu'une des sources de dialogue potentielles). Tous les villageois pouvant commencer un dialogue doivent maintenant avoir le tag "Dialogue". (#59)
 - La portée de détection des PNJ lors du début de dialogue a été légèrement augmentée (de 4 à 4.5).
 - Les tickets TCH ont désormais leurs propres modèles personnalisés, et le code traitant les tickets (*distributeur*/*valideur*/*vendeur*) a été modifié en conséquence.
 - Les valideurs standard acceptent désormais les cartes gold, et leur code a été harmonisé avec celui des valideurs gold.
 - L'infostation (*tch:auto/debug/infostation*) affiche désormais les entrées et sorties enregistrées, et un message d'erreur s'il n'y en a pas.
### Réparations
 - Hotfix de la 0.2.2 (#49) : le temps était tout de même accéléré lorsqu'il n'y avait que des spectateurs.
 - Diverses corrections de bugs dans *tch:auto/*.

## [0.2.2] (9d209b64)	- 2020-04-12
### Ajouts
 - Il y a désormais plusieurs (9) points de spawn répartis dans le monde, à l'emplacement de (futures) auberges et de certaines villes. Le serveur choisit au moment de la mort le point de spawn le plus proche, et y fait réapparaître le joueur. (#35)
### Modifications
 - Les tests de décès du joueur ont été déplacés de *tdh:sound/tick* à *tdh:player/tick*, pour éviter de démultiplier les calculs de décès et tout lancer depuis la même fonction.
 - Un combat s'arrêtera désormais si 30 secondes sont passées sans qu'aucun dommage ne soit donné ou reçu.
### Réparations
 - Le temps est tout de même accéléré si les seuls joueurs qui ne dorment pas sont en mode spectateur. (Il reste néanmoins à vitesse normale lorsque tous les joueurs sont en mode spectateur) (#49)
 - Le nom de la ville ou du village devrait désormais bien s'afficher lorsqu'on y entre. (#44)
 - Le système de combat détecte désormais les ennemis dans un volume plus restreint autour du joueur, et devrait produire moins de faux positifs. (#50)
 - Le système de combat prend désormais en compte les slimes et les loups. (#31)

## [0.2.1] (aa65d5b2)	- 2020-04-12
### Ajouts
 - Lorsqu'une conversation débute, un titre est affiché au joueur pour lui conseiller d'ouvrir l'interface de chat.
 - Un système d'incidents. Chaque jour, chaque ligne de métro a une probabilité de subir un incident et de ne pas rouler pendant une durée plus ou moins longue (jusqu'à une journée). (#41)
 - Le son *tch:annonce.sans_voyageur* est désormais joué sur le quai lorsqu'un train en fin de service entre en station. Le son de fin de service est joué après l'arrivée du train. (#46)
### Modifications
 - Il est désormais possible d'activer/désactiver le code de *tch:vendeur*. (#40)
 - La fonction *tch:vendeur/tick* a été séparée en deux ; le code qui vérifie l'avancement dans la quête est désormais exécuté dans *tch:vendeur/tick_detail* pour limiter le nombre de calculs. Sa fréquence a également été légèrement réduite. (#39)
 - Le système *tch:auto* ne peut désormais plus envoyer de métro lorsqu'on est hors des horaires de service théoriques de la ligne. (#47)
 - Remplacement, sauf oubli, de la dénomination "Terminus exceptionnel" par "Terminus".
### Réparations
 - Le temps avant le départ d'un métro est désormais correctement calculé dans *tch:title/en_station_time*.
 - Le joueur sera désormais correctement placé sur le quai lors d'un terminus. (#45)
### Suppressions
 - La fonction *tch:pass*, qui n'est plus utilisée depuis au moins 1 an.
 - Tous les greetings contenus dans *tch:agent*, qui sont amenés à être remplacés par le système *tch:vendeur*.
 - Un fichier inutilisé présent dans *tch:annonces*.
 - La proto-quête *greet_sabl3* présente dans *tch:quete*, qui n'était franchement pas réussie, ni même terminée.
 - Le dossier *tch:title* (il était entièrement vide).

## [0.2.0] (f051067f)	- 2020-04-11
### Ajouts
 - Une vérification est faite au lancement d'un métro ; si le 2ème chunk dans la direction de son déplacement est déchargé, il est supprimé pour éviter de le faire entrer dans un chunk déchargé.
 - Les métros ayant dépassé l'heure de fin de service déposeront désormais leur voyageur à la prochaine station, comme si c'était le terminus. (#42)
 - Les son *tch:sound/metro/fin_service_**x*** ont été implémentés, et sont joués lors de la fin du service sur les quais des lignes correspondantes. (#38)
 - Un système de détection permet de savoir à tout moment si un joueur est dans une station TCH ou non, et est exécuté par le code de TCH auto dans *tch:auto/player/tick*. Le nom de la gare ou station dans laquelle il entre est affiché au joueur.
 - Un siège en quartz a été ajouté dans tous les nouveaux métros TCH.
### Modifications
 - Le système *tch:auto* a été réécrit pour diminuer la charge serveur et permettre son activation/désactivation. (#39/#40)
 - Les horaires de métro sont désormais bien générés tant que la ligne est chargée. C'est uniquement le spawn qui est conditionné par la présence ou non d'un joueur à proximité. (#33)
 - La fonction *tch:auto/spawn_minecart_step* a été renommée *tch:auto/tick*, car elle ne concerne plus seulement le spawn des minecarts.
 - La fonction *tch:auto/decrement* a été intégrée à la boucle principale *tch:auto/tick*, et sa fréquence a donc été réduite. (#39)
 - Les fonctions *tch:auto/terminus_**xxx*** ont été rendues obsolètes par la modification de la fonction *tch:auto/terminus*. Elles pointent désormais vers *tch:auto/terminus_v1* et ne devraient plus être utilisées.
 - La fréquence de *tch:auto/title/tick* a été réduite de moitié pour réduire la charge serveur. (#39)
### Réparations
 - La fonction *tch:debug/infostation* devrait désormais n'afficher que les spawners relevant de la bonne station, quelle que soit la position de la personne l'exécutant. (#30)
 - Le son d'arrivée station Le Hameau - Rive Gauche se joue désormais bien sur la ligne 4.
### Suppressions
 - Toutes les sous-fonctions *tch:auto/add_minecart_**xxx*** ont été supprimées, puisque la fonction *tch:auto/add_minecart* est maintenant générique.
 - Les fonctions *tch:auto/clear_track* et *clear_buggy_minecarts*, ainsi que *tch:auto/left_**X**s* ont été supprimées, car on espère ne plus en avoir besoin avec le nouveau système.

### --		(32296149)	- 2020-04-10
 - Le système *tdh:village/death_detector* a été réécrit pour diminuer la charge serveur et permettre son activation/désactivation. (#39/#40)
### --		(7e514f28)	- 2020-04-10
 - Les faisceaux des phares ont été ralentis pour réduire la charge serveur. (#39)
 - Les horloges ne font plus exploser leur cadre quand il est 5h30.

## [0.1.3] (19d0e746)	- 2020-04-08
### Ajouts
 - Un système d'horloge générique vient remplacer le code de l'horloge de la gare de Procyon ; il suffit d'ajouter ingame une armor stand ayant le tag "Horloge" et tournée **en direction des gens qui pourront la lire**. (#6)
 - Les ambiances de biome ont été ajoutées pour les villes, villages & donjons. (#20)
 - La musique précédente est désormais stoppée lorsqu'on passe d'un biome "naturel" (musiques d'exploration) à un biome "artificiel" (musiques de ville/village/donjon) et que l'on n'est pas en combat.
 - Écran de debug des ambiances musicales (tenir le CD 13 en main pour faire apparaître les infos techniques)
 - Un titre est désormais affiché lors de l'entrée dans une ville/un village.
### Modifications
 - Les ambiances de biome sont désormais jouées 10 secondes après l'entrée dans un biome, et plus instantanément, afin d'éviter d'avoir une ambiance alors qu'on est dans un biome minuscule que l'on traverse en 3 secondes. (#12)
 - Les ambiances de biome peuvent se jouer alors qu'on n'a pas changé de biome, si la précédente musique d'ambiance a été jouée il y a au moins 10 minutes, lors de l'enchaînement entre 2 musiques d'exploration.
 - Les musiques sont désormais séparées d'environ 10 secondes, au lieu de s'enchaîner instantanément.
 - Les sons d'ambiance aléatoires des ice spikes ont été rendus moins fréquents.
 - Le code lançant les ambiances de biome n'est plus appelé depuis tdh:player/tick, mais depuis tdh:sound/tick, afin que toutes les fonctions relatives aux ambiances de biome se trouvent dans le même sous-répertoire.
### Réparations
 - Correction de la durée de explore/day/sky7, qui était bien plus élevée dans le code que sa durée réelle.
 - Un combat de 1 tick ne se lance plus lorsqu'on attaque une entité passive. (#21)
 - Les phantoms sont désormais pris en compte par le système de combat. (#22)
 - Les redstone lamp des phares sont de nouveau synchronisées avec le faisceau lumineux. (#17)
### Suppressions
 - Le code de l'horloge a été supprimé de tdh:ambient/procyon/gare, puisqu'une version plus générique existe désormais dans tdh:ambient/horloge
 - Une partie du code dégueulasse présent dans tdh:time, qui servait à lancer les gongs et plus généralement à faire tourner l'horloge de Procyon

## [0.1.2] (5d36c653)	- 2020-04-06
### Ajouts
 - Interface de gestion des paramètres des phares (tdh:ambient/phare/settings) qui permet de régler vitesse de rotation et portée du faisceau lumineux (#3)
 - Écran de debug des musiques (tenir le CD 11 en main pour faire apparaître les infos techniques ; le jeter au sol pour passer au morceau suivant) (#4)
 - Système rudimentaire de détection des combats (#16)
 - Les musiques du nether et de combat peuvent désormais se jouer
 - La musique combat se coupe brutalement à la fin du combat, et remplace brutalement la précédente au début du combat
 - Création du changelog et du readme.
### Modifications
 - Les ID de musique explore/night ont été déplacés de [0,12] à [50,62] car ils étaient identiques aux ID d'explore/day
### Suppressions
 - Les fonctions tdh:phare/, tdh:ambient/villonne/phare/, tdh:ambient/procyon/phare/ et tdh:ambient/hameau/phare/ (versions + actuelles et flexibles existent depuis la 0.1 dans tdh:ambient/phare/)
 - Les fonctions tdh:repair/villonne_champs et ses sous-fonctions (le plugin FarmProtect prend ça en charge très bien)
 - Diverses fonctions orphelines et inutilisées (valideur_v1, quete)


## [0.1.1] (0a70ad9a)	- 2020-04-06
### Ajouts
 - Activer un distributeur en tenant un ticket TCH affiche désormais la date limite de validité du ticket. (#10)
 - Création de tick_rename, qui renomme automatiquement des entités sans nom pour leur donner des noms aléatoires (actif uniquement sur les marchands itinérants pour l'instant)
### Modifications
 - Activer un distributeur sans tenir de moyen de paiement n'affiche plus de message d'erreur, mais les infos du distributeur. (#15)
 - Les tickets ne sont plus invoqués en tant qu'items par le distributeur, mais directement /give à l'acheteur.
### Réparations
 - Les tickets affichent désormais correctement leur description. (#14)
 - Réintégration des dernières versions de certains fichiers main hgive.
 - Les marchands itinérants sont désormais toujours renommés, et plus uniquement lorsqu'ils spawnent en ville. (#2)
### Suppressions
 - Le code d'info distributeur (déclenché à l'origine par les plaques de pression situées juste devant) a été désactivé.
 - La fonction test_validite et les sous-fonctions qui en dépendaient ont été supprimées.
 - Plusieurs lignes de code renvoyant à des fonctions inexistantes ou obsolètes ont été supprimées dans minecraft/tags/tick.json


## [0.1] (c86c0298)		- 2020-04-05
 Migration de la version actuelle du datapack sur gitlab.
 Pas de changelog sur les versions précédentes.
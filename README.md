# Datapack des Terres du Hameau
Compatible avec Minecraft 1.18.2 et le resource pack TDH v3.0.

## English notice
This datapack is currently written *in the french language*. Since translating it would require to move each and every piece of text out of the data pack (and inside a resource pack), and since this option would require all players to own the resource pack to make sense of the chat messages, I deemed it too much work for now (and maybe forever).
I might eventually have the initialization of the data pack store localized sentences inside a Minecraft `storage`, but it is not yet the case, so if if bothers you I have three options for you :
- Do not install this datapack! I'm sure there are a lot more data packs written in english.
- Translate the messages yourself ; I've heard of some websites that can help you with that...
- Learn french! It is a beautiful language. Remember that all non-english speakers *have* to do this with *your* mother tongue as kids in order to be able to use the Internet.



## À-propos
Ce datapack ayant été conçu sur un VPS très limité en ressources, il est largement plus optimisé que la moyenne des datapacks que j'ai eu l'occasion d'expérimenter, même si certains de ses systèmes sont plus demandants en puissance de calcul que d'autres. On peut citer parmi eux :
- **tdh-meteo**, dont le tick des effets météorologiques tourne à chaque tick de Minecraft (pour actualiser la chute/fonte de la neige au-delà d'une couche d'épaisseur, et divers effets d'intensité de la météo parmi lesquels le vent, les particules de pluie et les éclairs supplémentaires). Diverses optimisations sont néanmoins accessibles après l'initialisation de TDH, et permettent de ralentir le rythme de plusieurs de ces opérations : il est possible de les paramétrer à l'aide de la commande `tdh:meteo/_config`.
- **tdh-tch**, qui gère le système de transports, est particulièrement intensif en raison du nombre d'opérations lourdes réalisées chaque seconde dans un certain nombre de contextes : lorsqu'un ou plusieurs trains sont chargés et en mouvement, lorsqu'une ou plusieurs cabines sont chargées et en mouvement, ou lorsque de nombreuses annonces sont diffusées. Le système TCH est encore à ce stade partiellement hard-codé, et par conséquent ne pourra pas fonctionner sur un monde autre que les *Terres du Hameau* sans réécrire intégralement la fonction d'initialisation des données des stations et des lignes de transport (`tch:itineraire/_init`), et comme cette fonction seule pèse 180 Ko ça risque de prendre un bon moment. Par conséquent, si vous êtes sur un monde différent de *Terres du Hameau*, vous pouvez désactiver sans crainte le datapack **tdh-tch**.



## Fonctionnalités et utilisabilité des différents datapacks de tdh
Les différents datapacks du pack **tdh** sont généralement indépendants, bien que certains soient pratiquement indispensables. Ceux qu'il est recommandé d'installer quel que soit votre monde sont **tdh-core** et **tdh-time**.

### tdh-core [STABLE]
- Système de répartition des fonctions répétitives (`tdh:tick`) : crée de nombreux function tags `#tdh:tick/`, répertoriant toutes les fonctions des différents datapacks devant être appelées à un intervalle de temps défini (par exemple chaque seconde pour `#tdh:tick/1`, 8 fois par seconde pour `#tdh:tick/8`, ou 1 fois toutes les 3 secondes pour `#tdh:tick/1_3`). Cela permet de "lisser" l'impact en puissance de calcul des ticks des différents systèmes en les répartissant sur l'ensemble des ticks disponibles (et éviter que, du fait du hasard des /schedule, on ne se retrouve avec un tick unique où s'exécuteraient toutes les fonctions des différents datapacks à la fois).
- Contient de nombreuses variables au nom générique utilisées par de nombreux autres packs de TDH.
- Système de temps basique ; suivi de l'heure ingame, fonctions d'affichage formaté du temps (`tdh:store/time`) et de la distance (`tdh:store/distance_format`).
- Fonctions mathématiques basiques pour obtenir des entiers aléatoires (`tdh:random`), des approximations entières de pi (`tdh:math/pi/`), des fonctions trigonométriques (`tdh:math/cos/`, `cosd/`, `sin/`, `sind/`) ou encore la racine carrée (`tdh:math/sqrt`).
- Quelques checks assignant des variables aux joueurs, comme le statut extérieur/intérieur (`tdh:player/scores/test_interieur`), allongé ou non (`tdh:player/scores/test_lit`), l'altitude (`tdh:player/scores/altitude`) ou encore le niveau de lumière (`tdh:player/scores/lumiere`), tous commandés par le tick des variables joueur également défini par ce pack (`tdh:player/scores/tick`).
- Un système de configuration générique, utilisé par de nombreux configurateurs d'autres datapacks de TDH.

### tdh-time [STABLE]
- Ajoute un système de calendrier, basé par défaut sur le calendrier grégorien (365 jours répartis en 12 mois, avec années bissextiles correctes) mais entièrement configurable via l'interface `tdh:time/_config` : il est possible, via le storage, de modifier les définitions et les nombres des mois et des jours de la semaine.
- Ralentit l'écoulement d'une journée, de sorte à pouvoir moduler les durées proportionnelles du jour et de la nuit en fonction de la saison. Par défaut, l'effet est assez prononcé (avec des jours d'environ 6h au solstice d'hiver et 18h au solstice d'été), mais toutes les valeurs sont également configurables.
- Ajoute un système de tags permettant d'exécuter des fonctions chaque jour à une heure spécifique (basée sur le temps "réel" et ne variant pas, comme le ferait un daylight sensor, selon l'heure du lever ou du coucher du soleil) : par exemple, `#tdh:time/6h` exécute toutes les fonctions qu'il contient à 6h du matin.
- Chaque jour du calendrier peut être associé à un événement spécial ; par défaut, il s'agit des 2 solstices et des 2 équinoxes, mais d'autres peuvent être définis en ajoutant au tag `#tdh:time/jour_special_test` des fonctions ayant une structure similaire aux checks d'origine.
- Modification du système de sommeil : désormais, s'allonger dans un lit ne fait pas passer la nuit automatiquement, mais accélère simplement l'écoulement du temps (par défaut, x100).
- Des fonctions permettant de formater une date de différentes manières sont ajoutées à `tdh:store/`.

### tdh-climate (et ses datapacks climatiques tdh-climate-s01..s12) [EXPÉRIMENTAL]
- Ajoute un système météorologique un poil moins irréaliste que celui de Minecraft vanilla. Les probabilités respectives de beau temps, de pluie et d'orage sont calculées une fois par jour, à partir de scénarios hebdomadaires dont les poids respectifs dépendent de la configuration du mois en cours ; ces probabilités journalières permettent ensuite, régulièrement au cours d'une journée, de modifier la météo actuelle.
- Douze datapacks climatiques redéfinissent les biomes de Minecraft vanilla en fonction de la progression dans le calendrier. De la neige tombera en plaine en hiver, et elle fondra bien plus haut à l'été, même en montagne. Les biomes enneigés sont simplement plus froids que la moyenne, mais leur couverture neigeuse peut fondre en été. Les savanes et quelques autres biomes ont une saison des pluies où ils verdoient, et une saison sèche où aucune précipitation n'y peut tomber. (*Bien que ces datapacks aient été pensés pour "coller" aux 12 mois de l'année, ils peuvent être utilisés avec d'autres configurations de mois, et seront activés à intervalles réguliers entre les dates des solstices et équinoxes.*)
- Une variable "intensité" détermine l'intensité du vent et des précipitations à un instant donné, et peut évoluer dynamiquement dans les limites prévues par chacun des scénarios ; lorsque l'intensité est élevée, le vent est plus intense, davantage d'éclairs se produisent s'il y a de l'orage, et davantage de particules sont affichées s'il pleut ou qu'il neige.
- Au printemps et en été, les forêts s'étendent toutes seules ; des pousses sont régulièrement générées sur les troncs et feuilles des arbres, et jetés dans une direction aléatoire pour tenter de se planter.
#### Bugs connus :
- Lors du changement de datapack saisonnier (l'activation d'un nouveau datapack **tdh-climate-s01..s12**), certains paramètres des biomes (particulièrement le type de précipitations) ne sont pas correctement actualisés par le serveur. Cela conduit à une désynchronisation visuelle des précipitations pour les joueurs : par exemple des particules additionnelles de pluie qui apparaissent dans un biome pour lequel le serveur croit encore qu'il n'y a pas de précipitations (et n'affiche donc pas la pluie "vanilla").
  Pour régler ce problème, il suffit de redémarrer le serveur (ou de fermer et rouvrir le monde, si on se trouve en solo). Pour savoir à quel moment redémarrer le serveur, il suffit à un administrateur de se donner le tag `MeteoDebugLog`, ce qui lui affichera à chaque changement de datapack un message d'information. Ce redémarrage pourrait, en principe, être automatisé par un `stop` (qui requerrait de passer, dans `server.properties`, le niveau de permission des fonctions de 2 à 4), mais c'est une solution qui ne me satisferait pas ; d'autant plus qu'elle risquerait d'interférer avec le fonctionnement normal d'autres datapacks ou de réinitialiser sans prévenir les tools/l'historique du chat des joueurs.
- En raison du bug [MC-186963](https://bugs.mojang.com/browse/MC-186963), un lag très intense se produit si des commandes `spreadplayers` sont effectuées lorsqu'aucun bloc solide n'est à portée. Les fonctions qui doivent utiliser `spreadplayers` sont protégées par des checks les empêchant de s'exécuter dans les biomes maritimes ; cependant, si de grands plans d'eau existent dans des zones dont le biome n'est pas maritime, il faudra choisir l'une de ces options :
    - veiller à ce que la taille du plan d'eau ne dépasse pas 2 fois le rayon d'actualisation de la *fonte de la neige* ou des *chutes de neige*,
    - veiller à ce que des blocs solides se trouvent régulièrement disposés à la surface du plan d'eau (par exemple des îles affleurant à la surface, des bateaux formés de blocs...)
    - modifier, grâce à WorldEdit ou un logiciel similaire, le biome du plan d'eau incriminé (penser à sélectionner *toute la hauteur* en faisant un `/expand vert` pour éviter d'avoir des problèmes si un joueur est trop haut ou trop bas)
	- désactiver soit le datapack **tdh-climate**, soit, dans le configurateur du pack (`tdh:meteo/_config`), à la fois les *chutes de neige* et la *fonte de la neige*
  Ce bug peut mettre n'importe quel serveur à genoux, et est malheureusement difficile à éviter complètement, mais il a moins de risques de se produire si la distance d'actualisation des deux systèmes incriminés est *la plus élevée possible*, puisqu'il y aura alors plus de chances pour la commande `spreadplayers` de trouver des positions valides quelle que soit sa position d'exécution.

### tdh-player [ALPHA]
- Ajoute un système d'expérience et de leveling (absolument pas terminé), qui permet de débloquer différentes compétences en progressant dans certaines activités.
- Modifie la vitesse du joueur en fonction de différents paramètres : la lumière ambiante, la météo actuelle et son intensité, le fait de marcher sur des blocs "appropriés" (comme de la stone brick ou des grass path).
- Augmente la différence de vitesse entre la marche à pied (trop rapide dans Minecraft vanilla, et donc ralentie) et le sprint.
- Calcule de nombreux scores du joueur utiles pour d'autres datapacks de TDH, mais non essentiels : la présence dans une ville ou un village, le type de biome actuel...

### tdh-dialogue [ALPHA]
- Ajoute un système de dialogue avec les PNJ, inspiré par celui de RPG comme *Morrowind* : discuter avec les PNJ permet d'entendre parler de différents *sujets*, qui peuvent ensuite être utilisés avec d'autres PNJ dans l'espoir d'en apprendre davantage ou de déclencher des *quêtes*.
- Ce pack est tout sauf achevé, de nombreux sujets ne sont pas encore fonctionnels et les mieux implémentés sont principalement ceux qui concernent **TCH**. Cependant, il peut être aisément utilisé comme base pour ajouter soi-même ses propres dialogues, grâce à sa syntaxe générique fonctionnant avec les `storage` de Minecraft (voir le fichier `tdh:dialogue/affichage` pour des infos sur sa structure).

### tdh-porte [ALPHA]
- Ajoute un système de portes et de ponts-levis formés par plusieurs blocs, et coulissant/pivotant lors de l'appel d'une fonction.

### tdh-ascenseur [STABLE]
- Ajoute des ascenseurs formés de blocs, qui permettent de déplacer des joueurs rapidement verticalement et peuvent être appelés par des command blocks.

### tdh-crafting [ALPHA]
- Ajoute des "machines" de craft permettant de fabriquer des objets avec du NBT, inspirées par le plugin *SlimeFun* (le travail sur ce datapack est loin d'être terminé).
- Les recettes de chacune de ces machines de craft sont stockées dans un `storage` : par conséquent, d'autres datapacks peuvent y ajouter leurs propres recettes ou modifier les originales.
- Certaines machines de craft consomment de l'électricité, et une machine spécifique, le générateur électrique, permet d'en fournir à toutes les autres machines environnantes.
- Toutes les entités ou presque ont désormais des drops : tous les monstres et animaux "charnus" droppent de la viande, y compris les villageois et d'autres entités passives. Ces viandes peuvent toutes ou presque être cuites dans les machines du datapack.
- Ajoute de nombreuses fonctions génériques permettant de manipuler les conteneurs.

### tdh-rp [STABLE]
- *Ce pack est spécifiquement dédié au resource pack des *Terres du Hameau*, et ne doit/peut pas être utilisé sans celui-ci.*
- Ajoute le `tdh:give/`, qui permet d'obtenir facilement des objets custom du RP.
- Ajoute des recettes à **tdh-crafting** qui permettent aux machines de crafter des objets custom du RP.
- Ajoute le système d'objets dynamiques, qui permet d'utiliser des cigarettes ou de donner aux torches tenues en main une durabilité limitée.

### tdh-rp-sound [STABLE]
- *Ce pack est spécifiquement dédié au resource pack des *Terres du Hameau*, et ne doit/peut pas être utilisé sans celui-ci.*
- Programme des sons du RP selon le biome et la météo actuelles : oiseaux et insectes dans les biomes forêt et marécages ; sons de la pluie, du vent et de la neige selon l'intensité de la météo.
- Joue les sons des portes, pont-levis et ascenseurs des packs **tdh-porte** et **tdh-ascenseur**.
- Joue les musiques custom du RP.

### tdh-specifics
- *Ce pack contient de nombreuses vieilles fonctions inapplicables hors du monde des *Terres du Hameau*, et dont le manque d'optimisation n'a d'égal que l'inutilité concrète.*



## Installation
- Copier tous les datapacks souhaités de ce répertoire dans le dossier `<monde_minecraft>/datapacks`. Il n'est pas nécessaire de copier les datapacks non désirés, mais ils pourront le cas échéant simplement être désactivés à l'aide de la commande `/datapack disable <datapack>`.
- Démarrer le serveur ou faire un `/minecraft:reload` pour charger le pack.
- Vérifiez bien que tous les datapacks désirés sont correctement chargés à l'aide de la commande `/datapack list`, puis lancez l'initialisation (soit en cliquant sur le message posté dans le chat, soit en écrivant la commande `/function tdh:on_load/start`).
- Une fois l'initialisation effectuée, un autre message vous proposera de configurer chaque datapack qui le permet. Cette liste de tous les datapacks configurables pourra être affichée à tout moment dans le futur en entrant la commande `/function tdh:_config`.


## Configuration
(en construction)


## Utilisation
(en construction)
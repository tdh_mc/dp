# On retire les effets
effect clear @p[tag=DiseaseCheck] mining_fatigue
effect clear @p[tag=DiseaseCheck] weakness
effect clear @p[tag=DiseaseCheck] slowness
effect clear @p[tag=DiseaseCheck] unluck
tag @p[tag=DiseaseCheck] remove Tremblements

# On ajoute les post-effets (un peu plus lent que d'habitude, et un peu malchanceux)
effect give @p[tag=DiseaseCheck] slowness 61 0 true
effect give @p[tag=DiseaseCheck] unluck 121 4 true

# On retire les tags éventuels
tag @p[tag=DiseaseCheck] remove Phase1
tag @p[tag=DiseaseCheck] remove Phase2a
tag @p[tag=DiseaseCheck] remove Phase2b

# On se donne le tag Pause
tag @p[tag=DiseaseCheck] add Pause

# Si on a pas le tag Tierce (2j) ou Quarte (3j), on en choisit un au pif
execute unless entity @p[tag=DiseaseCheck,tag=Tierce] unless entity @p[tag=DiseaseCheck,tag=Quarte] run function tdh:player/health/disease/malaria/random_period

# On définit un temps d'attente de 80000 (2j-8h) si Tierce, et de 128000 (3j-8h) si Quarte
execute if entity @p[tag=DiseaseCheck,tag=Tierce] run scoreboard players set @p[tag=DiseaseCheck] nextDiseaseCheck 80000
execute if entity @p[tag=DiseaseCheck,tag=Quarte] run scoreboard players set @p[tag=DiseaseCheck] nextDiseaseCheck 128000

# Si on a déjà été malade pendant 10j ou plus, on a une petite chance (1 sur 10) que cette phase soit la dernière
execute if score @p[tag=DiseaseCheck] sickDays matches 10.. as @e[sort=random,limit=1,type=#tdh:random] as @s[y_rotation=232..267] run function tdh:player/health/disease/malaria/end
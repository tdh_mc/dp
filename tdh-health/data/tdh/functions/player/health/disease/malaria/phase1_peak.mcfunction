# On se donne les effets du mal de tête
# fièvre & mal de tête = nausea + blindness à intervalles plus ou moins réguliers (+ jump boost négatif pour éviter de sauter 1 bloc)
effect give @p[tag=DiseaseCheck] nausea 13 0 true
effect give @p[tag=DiseaseCheck] blindness 10 0 true
#effect give @p[tag=DiseaseCheck] jump_boost 15 253 true

# On inverse l'effet tremblement
tag @p[tag=DiseaseCheck,tag=!Tremblements] add temptr
tag @p[tag=DiseaseCheck,tag=Tremblements] remove Tremblements
tag @p[tag=DiseaseCheck,tag=temptr] add Tremblements
tag @p[tag=DiseaseCheck,tag=temptr] remove temptr

# Si on n'a pas de pot, on récupère night_vision pendant 5 secondes (écran totalement noir)
execute as @e[type=#tdh:random,sort=random,limit=1] as @s[y_rotation=44..86] run effect give @p[tag=DiseaseCheck] night_vision 6 0 true

# On définit le prochain temps d'attente avant d'avoir un coup de mal de tête
function tdh:player/health/disease/malaria/phase1_headache_wait
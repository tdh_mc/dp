# Le temps d'attente entre 2 élancements de maux de tête est de 20 à 40 secondes (400-800t), et le mal de tête en question dure 10s
function tdh:player/health/disease/random_chance
scoreboard players set #disease temp2 25
scoreboard players operation #disease temp /= #disease temp2
scoreboard players add #disease temp 400
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck = #disease temp
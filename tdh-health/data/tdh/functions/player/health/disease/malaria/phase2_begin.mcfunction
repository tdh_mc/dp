tellraw @p[tag=DiseaseCheck] [{"text":"Une nouvelle phase de la ","color":"gold"},{"text":"malaria","color":"red"},{"text":" commence..."}]

# Les effets qu'on donne là sont ceux de la "montée"
tag @p[tag=DiseaseCheck] remove Pause
tag @p[tag=DiseaseCheck] add Phase2a
tag @p[tag=DiseaseCheck] add Tremblements

# On donne les effets pour 4000t (2 heures) soit 200s
effect give @p[tag=DiseaseCheck] mining_fatigue 201 1 true
effect give @p[tag=DiseaseCheck] weakness 201 0 true
effect give @p[tag=DiseaseCheck] unluck 201 8 true

# Le temps d'attente avant le plateau est de 4000t
scoreboard players set @p[tag=DiseaseCheck] nextDiseaseCheck 4000

# Le temps total de la phase est de 16000t
scoreboard players set @p[tag=DiseaseCheck] diseaseTicksLeft 16100
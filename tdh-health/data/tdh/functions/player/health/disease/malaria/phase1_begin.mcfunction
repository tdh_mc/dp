tellraw @p[tag=DiseaseCheck] [{"text":"La première phase de la ","color":"gold"},{"text":"malaria","color":"red"},{"text":" commence..."}]

# - 1ère phase toujours symptomatique : douleurs musculaires = mining fatigue, weakness / fièvre & mal de tête = nausea + blindness à intervalles plus ou moins réguliers
tag @p[tag=DiseaseCheck] add Phase1

# On donne les effets pour 96000t (2 jours) soit 4800s mais on définit aléatoirement la fin de la phase (qui les retirera)
effect give @p[tag=DiseaseCheck] mining_fatigue 4801 1 true
effect give @p[tag=DiseaseCheck] weakness 4801 1 true
effect give @p[tag=DiseaseCheck] unluck 4801 4 true

# On définit une durée aléatoire pour cette première phase (entre 6 et 24h)
function tdh:player/health/disease/random_chance
scoreboard players set #disease temp2 36000
scoreboard players operation #disease temp *= #disease temp2
scoreboard players operation #disease temp /= #disease diseaseOdds
scoreboard players add #disease temp 12000
scoreboard players operation @p[tag=DiseaseCheck] diseaseTicksLeft = #disease temp

# On définit le premier temps d'attente avant d'avoir un coup de mal de tête
function tdh:player/health/disease/malaria/phase1_headache_wait
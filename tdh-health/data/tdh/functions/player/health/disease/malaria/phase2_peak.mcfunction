# Les effets qu'on donne là sont ceux du plateau
tag @p[tag=DiseaseCheck] remove Phase2a
tag @p[tag=DiseaseCheck] add Phase2b
tag @p[tag=DiseaseCheck] remove Tremblements

# On donne les effets pour 7000t (3.5 heures) = 350s
effect give @p[tag=DiseaseCheck] mining_fatigue 351 3 true
effect give @p[tag=DiseaseCheck] weakness 351 1 true
effect give @p[tag=DiseaseCheck] slowness 351 2 true
effect give @p[tag=DiseaseCheck] blindness 251 0 true
effect give @p[tag=DiseaseCheck] unluck 351 12 true

# Le temps d'attente avant le plateau est de 7000t
scoreboard players set @p[tag=DiseaseCheck] nextDiseaseCheck 7000
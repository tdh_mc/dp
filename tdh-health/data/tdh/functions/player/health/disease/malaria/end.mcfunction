# On retire tous les tags éventuels
tag @p[tag=DiseaseCheck] remove HasDisease
tag @p[tag=DiseaseCheck] remove Phase1
tag @p[tag=DiseaseCheck] remove Phase2a
tag @p[tag=DiseaseCheck] remove Phase2b
tag @p[tag=DiseaseCheck] remove Pause
tag @p[tag=DiseaseCheck] remove Tierce
tag @p[tag=DiseaseCheck] remove Quarte
tag @p[tag=DiseaseCheck] remove Tremblements

# On reset toutes les variables scoreboard
scoreboard players reset @p[tag=DiseaseCheck] sickDays
scoreboard players reset @p[tag=DiseaseCheck] lastSickDay
scoreboard players reset @p[tag=DiseaseCheck] diseaseTicksLeft
scoreboard players set @p[tag=DiseaseCheck] nextDiseaseCheck 1200

tellraw @p[tag=DiseaseCheck] [{"text":"Vous êtes venu à bout de la ","color":"gold"},{"text":"malaria","color":"red"},{"text":"."}]
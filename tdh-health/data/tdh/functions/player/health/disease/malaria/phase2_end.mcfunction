# Les effets qu'on donne là sont ceux de la redescente

# On retire les effets précédents
effect clear @p[tag=DiseaseCheck] mining_fatigue
effect clear @p[tag=DiseaseCheck] weakness
effect clear @p[tag=DiseaseCheck] slowness
effect clear @p[tag=DiseaseCheck] blindness
effect clear @p[tag=DiseaseCheck] unluck

# On donne les effets pour 5000t (2.5 heures)
effect give @p[tag=DiseaseCheck] mining_fatigue 251 0 true
effect give @p[tag=DiseaseCheck] weakness 251 0 true
effect give @p[tag=DiseaseCheck] slowness 251 1 true
effect give @p[tag=DiseaseCheck] unluck 251 8 true

# Le temps d'attente avant la pause est de 5000t
scoreboard players set @p[tag=DiseaseCheck] nextDiseaseCheck 5000
# L'incubation dure entre 12h et 4 jours (IRL : 3 à 21 jours)
function tdh:player/health/disease/random_chance
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck = #disease temp
scoreboard players set #disease temp2 17
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck *= #disease temp2
scoreboard players add @p[tag=DiseaseCheck] nextDiseaseCheck 22000

# On donne un tag au joueur
tag @p[tag=DiseaseCheck] add FievreHemo

# Message de debug
tellraw @p[tag=DiseaseCheck] [{"text":"[DEBUG] Maladie obtenue : ","color":"gold"},{"text":"Fièvre hémorragique","color":"red"},{"text":".\nTemps d'incubation : "},{"score":{"name":"@p[tag=DiseaseCheck]","objective":"nextDiseaseCheck"},"color":"red"},{"text":" ticks."}]
# On stocke dans #disease temp une valeur aléatoire entre 0 et 9999
# On récupère la rotation d'un mob n'ayant pas la rotation par défaut (entre 1 et 359, donc 358 * 27.93 = 9999)
execute store result score #disease temp run data get entity @e[sort=random,limit=1,type=#tdh:random,y_rotation=1..359] Rotation[0] 27.93
scoreboard players remove #disease temp 27
# L'incubation dure entre 7 et 12h (IRL : 1 à 2 jours)
function tdh:player/health/disease/random_chance
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck = #disease temp
scoreboard players add @p[tag=DiseaseCheck] nextDiseaseCheck 14000

# On donne un tag au joueur
tag @p[tag=DiseaseCheck] add Grippe

# Message de debug
tellraw @p[tag=DiseaseCheck] [{"text":"[DEBUG] Maladie obtenue : ","color":"gold"},{"text":"Grippe","color":"red"},{"text":".\nTemps d'incubation : "},{"score":{"name":"@p[tag=DiseaseCheck]","objective":"nextDiseaseCheck"},"color":"red"},{"text":" ticks."}]
# L'incubation dure entre 16 et 36h (IRL : 4 à 7 jours)
function tdh:player/health/disease/random_chance
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck = #disease temp
scoreboard players set #disease temp2 4
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck *= #disease temp2
scoreboard players add @p[tag=DiseaseCheck] nextDiseaseCheck 32000

# On donne un tag au joueur
tag @p[tag=DiseaseCheck] add Chikungunya

# Message de debug
tellraw @p[tag=DiseaseCheck] [{"text":"[DEBUG] Maladie obtenue : ","color":"gold"},{"text":"Chikungunya","color":"red"},{"text":".\nTemps d'incubation : "},{"score":{"name":"@p[tag=DiseaseCheck]","objective":"nextDiseaseCheck"},"color":"red"},{"text":" ticks."}]
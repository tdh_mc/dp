# L'incubation dure entre 1 et 3 jours (IRL : 7 à 14 jours)
function tdh:player/health/disease/random_chance
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck = #disease temp
scoreboard players set #disease temp2 10
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck *= #disease temp2
scoreboard players add @p[tag=DiseaseCheck] nextDiseaseCheck 46000

# On donne un tag au joueur
tag @p[tag=DiseaseCheck] add Malaria

# Message de debug
tellraw @p[tag=DiseaseCheck] [{"text":"[DEBUG] Maladie obtenue : ","color":"gold"},{"text":"Malaria","color":"red"},{"text":".\nTemps d'incubation : "},{"score":{"name":"@p[tag=DiseaseCheck]","objective":"nextDiseaseCheck"},"color":"red"},{"text":" ticks."}]
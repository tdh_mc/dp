# L'incubation dure entre 3 et 15j (IRL : qqes semaines/mois)
function tdh:player/health/disease/random_chance
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck = #disease temp
scoreboard players set #disease temp2 57
scoreboard players operation @p[tag=DiseaseCheck] nextDiseaseCheck *= #disease temp2
scoreboard players add @p[tag=DiseaseCheck] nextDiseaseCheck 150000

# On donne un tag au joueur
tag @p[tag=DiseaseCheck] add Sommeil

# Message de debug
tellraw @p[tag=DiseaseCheck] [{"text":"[DEBUG] Maladie obtenue : ","color":"gold"},{"text":"Maladie du sommeil","color":"red"},{"text":".\nTemps d'incubation : "},{"score":{"name":"@p[tag=DiseaseCheck]","objective":"nextDiseaseCheck"},"color":"red"},{"text":" ticks."}]
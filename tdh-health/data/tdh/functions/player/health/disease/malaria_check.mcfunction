# La malaria IRL fonctionne par phases, avec une première phase symptomatique ou non ;
# fièvre, mal de tête, douleurs musculaires, troubles digestifs, de manière continue ou anarchique
# ensuite, ça revient par cycles d'environ 8h, soit tous les 2 jours, soit tous les 3 jours.
# Sans traitement, ça peut persister pendant des mois voire des années.

# IG, on simplifie un peu le fonctionnement :
# - 1ère phase toujours symptomatique : douleurs musculaires = mining fatigue, weakness / fièvre & mal de tête = nausea + blindness à intervalles plus ou moins réguliers
# - Ensuite, les phases se succèdent à intervalles réguliers (tous les 2 ou tous les 3 jours). Occasionnellement on peut "rater" une phase (= pas de symptômes).
# - Au bout d'un certain nombre de phases, la maladie disparaît d'elle-même.

# Si notre variable sickDays n'est pas définie, ça signifie le début des symptômes
execute unless score @p[tag=DiseaseCheck] sickDays matches 0.. run function tdh:player/health/disease/malaria/phase1_begin

# Si on est en phase 1, on applique l'effet mal de tête intermittent (pour l'instant nausea + blindness)
execute if entity @p[tag=DiseaseCheck,tag=Phase1] run function tdh:player/health/disease/malaria/phase1_peak

# Si on est en pause, cela signifie le début d'une nouvelle phase
execute if entity @p[tag=DiseaseCheck,tag=Pause] run function tdh:player/health/disease/malaria/phase2_begin

# Si on est en phase 2a, cela signifie le début du plateau
execute if entity @p[tag=DiseaseCheck,tag=Phase2a] run function tdh:player/health/disease/malaria/phase2_peak

# Si on est en phase 2b, cela signifie la fin du plateau
execute if entity @p[tag=DiseaseCheck,tag=Phase2b] run function tdh:player/health/disease/malaria/phase2_end

# Si on est arrivés à la fin de cette phase, on génère un temps d'attente avant la prochaine phase
execute if score @p[tag=DiseaseCheck] diseaseTicksLeft matches ..1 run function tdh:player/health/disease/malaria/pause
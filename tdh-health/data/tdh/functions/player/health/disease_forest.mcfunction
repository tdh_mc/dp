# On prend un modulo 10 pour choisir la maladie
scoreboard players set #disease temp2 10
scoreboard players operation #disease temp %= #disease temp2
scoreboard players reset #disease temp2

# 40% encéphalite ; 40% grippe ; 20% diphtérie
execute if score #disease temp matches 0..3 run function tdh:player/health/disease/encephalite_tique
execute if score #disease temp matches 4..7 run function tdh:player/health/disease/grippe
execute if score #disease temp matches 8..9 run function tdh:player/health/disease/diphterie
# On prend un modulo 10 pour choisir la maladie
scoreboard players set #disease temp2 10
scoreboard players operation #disease temp %= #disease temp2
scoreboard players reset #disease temp2

# 50% malaria ; 30% fièvre jaune ; 20% chikungunya
execute if score #disease temp matches 0..4 run function tdh:player/health/disease/malaria
execute if score #disease temp matches 5..7 run function tdh:player/health/disease/fievre_jaune
execute if score #disease temp matches 8..9 run function tdh:player/health/disease/chikungunya
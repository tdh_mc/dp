# On tag le joueur en cours
tag @p[gamemode=!spectator,gamemode=!creative] add DiseaseCheck

# Si le joueur est malade, on check l'évolution de sa maladie
execute if entity @p[tag=DiseaseCheck,tag=HasDisease] run function tdh:player/health/disease_check/sick_check
# Si le joueur n'est pas malade, on check s'il faut le rendre malade
execute if entity @p[tag=DiseaseCheck,tag=!HasDisease] run function tdh:player/health/disease_check/healthy_check

# On supprime les tags
tag @a[tag=DiseaseCheck] remove DiseaseCheck
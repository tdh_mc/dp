# Lorsqu'on est dans la savane, les chances de maladie sont modérées
# 7 chances sur 1000 toutes les 1mn =
# - 7% de chances d'être tombé malade au bout de 10mn
# - 14% au bout de 20mn
# - 25% après un jour complet (40mn)
scoreboard players set @p[tag=DiseaseCheck] diseaseOdds 70

# Si c'est la nuit, on a moins de chances (75%) de tomber malade
execute if score #temps timeOfDay matches 3 run function tdh:player/health/disease_check/decrease_odds_75

# S'il pleut ou qu'il y a de l'orage, on a moins de chances (75%) de tomber malade
execute if score fakeWeather CurrentWeather matches 2..3 run function tdh:player/health/disease_check/decrease_odds_75

# Si on est en train de dormir, on multiplie nos chances d'être malade par 2
execute if entity @p[tag=DiseaseCheck,tag=IsSleeping] run function tdh:player/health/disease_check/increase_odds_200
# Si tout le monde est en train de dormir, on multiplie nos chances d'être malade par 3
execute unless entity @p[tag=!IsSleeping] run function tdh:player/health/disease_check/increase_odds_300


# On récupère une variable aléatoire dans #disease temp (0<=temp<10000)
function tdh:player/health/disease/random_chance

# Selon la valeur de cette variable, on tombe ou non malade
execute if score #disease temp < @p[tag=DiseaseCheck] diseaseOdds run tag @p[tag=DiseaseCheck] add HasDisease

# Si on est tombé malade, on choisit de quelle maladie on souffrira
execute at @p[tag=DiseaseCheck,tag=HasDisease] run function tdh:player/health/disease_savanna
# Lorsqu'on est dans un marais, les chances de maladie sont assez importantes

# Si l'on est en fin de printemps/été (la pire saison) :
# 1 chance sur 50 toutes les 1mn =
# - 20% de chances d'être tombé malade au bout de 10mn
# - 33% au bout de 20mn
# - >55% après un jour complet (40mn)
execute if score #temps dateMois matches 5..8 run scoreboard players set @p[tag=DiseaseCheck] diseaseOdds 200

# Si l'on est en printemps/automne (moyenne saison) :
# 1 chance sur 100 toutes les 1mn =
# - 10% de chances d'être tombé malade au bout de 10mn
# - 20% au bout de 20mn
# - 33% après un jour complet (40mn)
execute if score #temps dateMois matches 3..11 unless score #temps dateMois matches 5..8 run scoreboard players set @p[tag=DiseaseCheck] diseaseOdds 100

# Si l'on est en hiver (saison ok) :
# 1 chance sur 250 toutes les 1mn =
# - 4% de chances d'être tombé malade au bout de 10mn
# - 8% au bout de 20mn
# - 15% après un jour complet (40mn)
execute unless score #temps dateMois matches 3..11 run scoreboard players set @p[tag=DiseaseCheck] diseaseOdds 40

# Si on est en train de dormir, on multiplie nos chances d'être malade par 2
execute if entity @p[tag=DiseaseCheck,tag=IsSleeping] run function tdh:player/health/disease_check/increase_odds_200
# Si tout le monde est en train de dormir, on multiplie nos chances d'être malade par 3
execute unless entity @p[tag=!IsSleeping] run function tdh:player/health/disease_check/increase_odds_300

# On récupère une variable aléatoire dans #disease temp (0<=temp<10000)
function tdh:player/health/disease/random_chance

# Selon la valeur de cette variable, on tombe ou non malade
execute if score #disease temp < @p[tag=DiseaseCheck] diseaseOdds run tag @p[tag=DiseaseCheck] add HasDisease

# Si on est tombé malade, on choisit de quelle maladie on souffrira
execute at @p[tag=DiseaseCheck,tag=HasDisease] run function tdh:player/health/disease_swamp
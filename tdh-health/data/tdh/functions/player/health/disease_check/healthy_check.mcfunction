# On se tag si on est dans un biome donnant des chances de maladie
execute if predicate tdh:biome/marais run tag @p[tag=DiseaseCheck] add DiseaseSwamp
execute if predicate tdh:biome/foret/conifere run tag @p[tag=DiseaseCheck] add DiseaseForest
execute if predicate tdh:biome/foret/decidue run tag @p[tag=DiseaseCheck] add DiseaseForest
execute if predicate tdh:biome/foret/mixte run tag @p[tag=DiseaseCheck] add DiseaseForest
execute if predicate tdh:biome/jungle run tag @p[tag=DiseaseCheck] add DiseaseJungle
execute if predicate tdh:biome/savane run tag @p[tag=DiseaseCheck] add DiseaseSavanna

# Si on est dans un biome de type marécage, on augmente nos chances de maladie
execute if entity @p[tag=DiseaseCheck,tag=DiseaseSwamp] run function tdh:player/health/disease_check/swamp
# Si on est dans un biome de type foret noire, on augmente nos chances de maladie
execute if entity @p[tag=DiseaseCheck,tag=DiseaseForest] run function tdh:player/health/disease_check/forest
# Si on est dans un biome de type jungle, on augmente nos chances de maladie
execute if entity @p[tag=DiseaseCheck,tag=DiseaseJungle] run function tdh:player/health/disease_check/jungle
# Si on est dans un biome de type savane, on augmente nos chances de maladie
execute if entity @p[tag=DiseaseCheck,tag=DiseaseSavanna] run function tdh:player/health/disease_check/savanna
# Sinon, on réinitialise nos chances de maladie
execute if entity @p[tag=DiseaseCheck,tag=!DiseaseSwamp,tag=!DiseaseForest,tag=!DiseaseJungle,tag=!DiseaseSavanna] run function tdh:player/health/disease_check/default

# Si le joueur n'a pas été rendu malade, on attend 1 minute (1200t) avant de refaire un check
scoreboard players set @p[tag=DiseaseCheck,tag=!HasDisease] nextDiseaseCheck 1200

# On supprime les tags
tag @a[tag=DiseaseCheck] remove DiseaseSwamp
tag @a[tag=DiseaseCheck] remove DiseaseForest
tag @a[tag=DiseaseCheck] remove DiseaseJungle
tag @a[tag=DiseaseCheck] remove DiseaseSavanna
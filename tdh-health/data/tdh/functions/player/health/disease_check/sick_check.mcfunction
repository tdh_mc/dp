# On choisit la fonction à lancer en fonction de notre maladie actuelle :
execute if entity @p[tag=DiseaseCheck,tag=Malaria] run function tdh:player/health/disease/malaria_check
execute if entity @p[tag=DiseaseCheck,tag=FievreHemo] run function tdh:player/health/disease/fievre_hemorragique_check
execute if entity @p[tag=DiseaseCheck,tag=FievreJaune] run function tdh:player/health/disease/fievre_jaune
execute if entity @p[tag=DiseaseCheck,tag=Chikungunya] run function tdh:player/health/disease/chikungunya_check
execute if entity @p[tag=DiseaseCheck,tag=Sommeil] run function tdh:player/health/disease/sommeil_check
execute if entity @p[tag=DiseaseCheck,tag=EncephaliteTique] run function tdh:player/health/disease/encephalite_tique_check
execute if entity @p[tag=DiseaseCheck,tag=Grippe] run function tdh:player/health/disease/grippe_check
execute if entity @p[tag=DiseaseCheck,tag=Diphterie] run function tdh:player/health/disease/diphterie_check

# Si on n'a pas de valeurs pour nos variables, on les met à leurs valeurs par défaut
execute unless score @p[tag=DiseaseCheck] sickDays matches 0.. run scoreboard players set @p[tag=DiseaseCheck] sickDays 0
execute unless score @p[tag=DiseaseCheck] lastSickDay matches 0.. run scoreboard players operation @p[tag=DiseaseCheck] lastSickDay = #temps dateJour

# On incrémente le nombre de jours écoulés si on est le jour suivant
execute unless score @p[tag=DiseaseCheck] lastSickDay = #temps dateJour run function tdh:player/health/disease_check/sick_check_nextday

# Si le temps d'attente avant le prochain "jour" n'a pas été défini (=pas de symptômes aigus),
# on le met à sa valeur par défaut (1 jour = 48000t)
scoreboard players set @p[tag=DiseaseCheck,scores={nextDiseaseCheck=0}] nextDiseaseCheck 48000
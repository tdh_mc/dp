# Lorsqu'on est dans la forêt noire ou de spruce, les chances de maladie sont assez faibles
# 15 chances sur 10000 toutes les 1mn =
# - 1.5% de chances d'être tombé malade au bout de 10mn
# - 3% au bout de 20mn
# - 6% après un jour complet (40mn)
# - 21% après 4 jours (2h40mn)
# - 50% après 10 jours passés en forêt (6h40mn)
scoreboard players set @p[tag=DiseaseCheck] diseaseOdds 15

# S'il pleut ou qu'il y a de l'orage, on a plus de chances (133%) de tomber malade
execute if score fakeWeather CurrentWeather matches 2..3 run function tdh:player/health/disease_check/increase_odds_133

# Si on est en train de dormir, on multiplie nos chances d'être malade par 2
execute if entity @p[tag=DiseaseCheck,tag=IsSleeping] run function tdh:player/health/disease_check/increase_odds_200
# Si tout le monde est en train de dormir, on multiplie nos chances d'être malade par 3
execute unless entity @p[tag=!IsSleeping] run function tdh:player/health/disease_check/increase_odds_300


# On récupère une variable aléatoire dans #disease temp (0<=temp<10000)
function tdh:player/health/disease/random_chance

# Selon la valeur de cette variable, on tombe ou non malade
execute if score #disease temp < @p[tag=DiseaseCheck] diseaseOdds run tag @p[tag=DiseaseCheck] add HasDisease

# Si on est tombé malade, on choisit de quelle maladie on souffrira
execute at @p[tag=DiseaseCheck,tag=HasDisease] run function tdh:player/health/disease_forest
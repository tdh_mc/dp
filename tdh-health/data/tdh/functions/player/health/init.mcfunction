# Variables relatives aux maladies
scoreboard objectives add nextDiseaseCheck dummy "Prochain check de maladie"
scoreboard objectives add diseaseOdds dummy "Chances de maladie"
# Les chances de maladie sont comptées pour 10'000.
scoreboard players set #disease diseaseOdds 10000

scoreboard objectives add diseaseTicksLeft dummy "Ticks restants à la phase de maladie"
scoreboard objectives add sickDays dummy "Jours depuis le début de la maladie"
scoreboard objectives add lastSickDay dummy "Jour d'actualisation de la maladie"
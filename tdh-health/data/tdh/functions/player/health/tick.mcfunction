# ---- CALCUL DES MALADIES ----
# Si on a atteint le tick où l'on fait le calcul, on teste si les autres conditions sont remplies
execute at @a[gamemode=!spectator,gamemode=!creative] unless score @p[gamemode=!spectator,gamemode=!creative] nextDiseaseCheck matches 1.. run function tdh:player/health/disease_check
# On applique les tremblements
execute at @a[tag=HasDisease,tag=Tremblements] rotated as @e[type=#tdh:random,sort=random,limit=1] run tp @p[tag=Tremblements] ^0.05 ^ ^
# Sinon, on décrémente le nombre de ticks restants
# (320 car on a passé la fréquence de "chaque tick" à "toutes les 16 secondes". De toute façon il faut réécrire tout le code hein)
scoreboard players remove @a[gamemode=!spectator,gamemode=!creative,scores={nextDiseaseCheck=1..}] nextDiseaseCheck 320
scoreboard players remove @a[gamemode=!spectator,gamemode=!creative,scores={diseaseTicksLeft=1..}] diseaseTicksLeft 320
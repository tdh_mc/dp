# On prend un modulo 10 pour choisir la maladie
scoreboard players set #disease temp2 10
scoreboard players operation #disease temp %= #disease temp2
scoreboard players reset #disease temp2

# 70% malaria ; 30% fièvre hémorragique
execute if score #disease temp matches 0..6 run function tdh:player/health/disease/malaria
execute if score #disease temp matches 7..9 run function tdh:player/health/disease/fievre_hemorragique
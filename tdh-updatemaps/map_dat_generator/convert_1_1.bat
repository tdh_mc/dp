@echo off
echo "Begin"

setlocal EnableDelayedExpansion

if not exist output\NUL mkdir output
if not exist output\dat\NUL mkdir output\dat

set "xMin=0"
set "zMin=0"
set "xMax=100"
set "zMax=100"

for /l %%x in (%xMin%, 1, %xMax%) do (
	set "formX=000%%x"
	set /a multX = 128 * %%x
	for /l %%z in (%zMin%, 1, %zMax%) do (
		set "formZ=000%%z"
		set /a multZ = 128 * %%z
		set "mapID=1!formX:~-3!!formZ:~-3!"
		if %%z EQU 10 echo "Processing map %%x,%%z of %xMax%,%zMax% (id !mapID!)"
		
		@echo !multX! >x.txt
		@echo !multZ! >z.txt
		copy map_pattern_a.json+x.txt+map_pattern_b.json+z.txt+map_pattern_c.json /a "output/json.json" /b >nul
		nbt2json.exe -b -r -z -i "output/json.json" -o "output/dat/map_!mapID!.dat"
	)
)

echo "Finished!"
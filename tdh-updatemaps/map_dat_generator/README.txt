How to use :
- Download nbt2json.exe into this folder (Download on https://github.com/midnightfreddie/nbt2json/releases ; tested with version 0.4.0)
- Modify the "xMax" and "zMax" variables in the 4 convert_X_Z.bat files. By default they are equal to 100 (i.e. the script will generate a 201x201 square centered on 0,0).
  Do not modify the xMin and zMin variables.
- Run "convert.bat" (or the convert_X_Z.bat files individually) from the command processor. The generated .dat files will be in the folder /output/dat/.
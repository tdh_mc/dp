# On exécute cette fonction en tant que n'importe qui pour lancer la génération des cartes
# Doivent être définies les variables #cartographieX min/max et #cartographieZ min/max, en coordonnées BLOCK

# On affiche d'abord un message de confirmation au joueur
tellraw @s [{"text":"[CGC]","color":"gold"},{"text":" : Initialisation de la génération...","color":"gray"}]

# On ramène sur une grille de 128 de toutes nos variables d'entrée,
# pour le cas où le type qui lance la génération est nul en calcul mental
scoreboard players set #cartographie temp8 64
scoreboard players set #cartographie temp9 128
scoreboard players operation #cartographieX min += #cartographie temp8
scoreboard players operation #cartographieX min /= #cartographie temp9
scoreboard players operation #cartographieX min *= #cartographie temp9

scoreboard players operation #cartographieX max -= #cartographie temp8
scoreboard players operation #cartographieX max /= #cartographie temp9
scoreboard players operation #cartographieX max *= #cartographie temp9
scoreboard players operation #cartographieX max += #cartographie temp9

scoreboard players operation #cartographieZ min += #cartographie temp8
scoreboard players operation #cartographieZ min /= #cartographie temp9
scoreboard players operation #cartographieZ min *= #cartographie temp9

scoreboard players operation #cartographieZ max -= #cartographie temp8
scoreboard players operation #cartographieZ max /= #cartographie temp9
scoreboard players operation #cartographieZ max *= #cartographie temp9
scoreboard players operation #cartographieZ max += #cartographie temp9

# On a un certain nombre de préconditions :
scoreboard players set #cartographie status 0
# (-10) - aucune génération ne doit être en cours (on = 0)
execute unless score #cartographie on matches 0 run scoreboard players set #cartographie status -10
# (-20) - il doit y avoir un joueur ayant le tag CGCCartographe
execute if score #cartographie status matches 0 unless entity @p[tag=CGCCartographe] run scoreboard players set #cartographie status -20
# (-30/-35) - les min et max spécifiés doivent être valides (min <= max, et dans les limites de la worldborder)
execute if score #cartographie status matches 0 unless score #cartographieX min <= #cartographieX max run scoreboard players set #cartographie status -30
execute if score #cartographie status matches 0 unless score #cartographieZ min <= #cartographieZ max run scoreboard players set #cartographie status -31

execute if score #cartographie status matches 0 unless score #cartographieX min >= #worldborderX min run scoreboard players set #cartographie status -32
execute if score #cartographie status matches 0 unless score #cartographieX max <= #worldborderX max run scoreboard players set #cartographie status -33
execute if score #cartographie status matches 0 unless score #cartographieZ min >= #worldborderZ min run scoreboard players set #cartographie status -34
execute if score #cartographie status matches 0 unless score #cartographieZ max <= #worldborderZ max run scoreboard players set #cartographie status -35

# Si toutes les préconditions ont passé, on lance la fonction de démarrage
execute if score #cartographie status matches 0 run function cgc:cartographie/debut/succes
execute unless score #cartographie status matches 0 run function cgc:cartographie/debut/echec
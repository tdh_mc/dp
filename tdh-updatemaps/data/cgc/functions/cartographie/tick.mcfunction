# Ce tick est exécuté par tdh-tick toutes les 5 secondes si la cartographie est activée
# (= si un rendu de carte est actuellement en cours) et qu'un joueur faisant le rendu est présent
# Cette fonction est exécutée à la position du joueur faisant le rendu
# (il n'est pas recommandé d'avoir plusieurs joueurs avec le tag CGCCartographe car cela risque de provoquer des problèmes)

# On vérifie tout d'abord que le chunk est chargé
# La méthode est un peu tirée par les cheveux mais elle fonctionne :
# Tout d'abord on définit une variable temporaire à 0
scoreboard players set #cartographie temp 0
# Ensuite, on vérifie si le bloc à 64,64 de notre position est de l'air, ou n'est pas de l'air,
# et on répète le même check dans les 4 coins (+-64,+-64)
# Si l'une des conditions ne passe pas (temp < 4), on ne change pas de carte à ce tick
execute positioned ~-64 64 ~-64 run function cgc:cartographie/tick/test_position
execute positioned ~-64 64 ~64 run function cgc:cartographie/tick/test_position
execute positioned ~64 64 ~-64 run function cgc:cartographie/tick/test_position
execute positioned ~64 64 ~64 run function cgc:cartographie/tick/test_position

# Si notre score est <4, on affiche un message d'avertissement au joueur cartographe
execute unless score #cartographie temp matches 4.. run tellraw @a[tag=CGCCartographe] [{"text":"[CGC]","color":"gold"},{"text":" On saute une itération, car les chunks ne semblent pas avoir chargé.","color":"gray"}]
# Si notre score est 4, on peut passer à la carte suivante, car on a réussi à charger le chunk actuel
execute if score #cartographie temp matches 4.. run function cgc:cartographie/prochaine_carte
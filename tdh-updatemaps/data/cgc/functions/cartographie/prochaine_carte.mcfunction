# Cette fonction est exécutée à la position d'un joueur qui fait le rendu de la cartographie,
# lorsqu'on peut passer à la carte suivante (=le chunk actuel a chargé)

# On commence par calculer notre prochaine position
function cgc:cartographie/prochaine_carte/calcul_pos
# Une fois passé par cette fonction, nos variables #cartographie posX et posZ ont été modifiées en conséquence
# Si on est arrivés au bout, on aura défini #cartographie on = 0, donc on fait le test

execute if score #cartographie on matches 1 run function cgc:cartographie/prochaine_carte/succes
execute unless score #cartographie on matches 1 run function cgc:cartographie/prochaine_carte/fin
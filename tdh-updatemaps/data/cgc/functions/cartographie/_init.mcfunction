# Le statut 0 = pas de génération de carte en cours
# (sauf si une génération est DÉJÀ en cours)
execute unless score #cartographie on matches 1 run scoreboard players set #cartographie on 0

# On initialise les bounds de la carte
# Sur TDH 2022 c'est +-7168 dans toutes les directions, mais à éditer selon les paramètres de votre monde
# C'est juste pour éviter d'envoyer les joueurs/entités dans des chunks complètement pas générés
scoreboard players set #worldborderX min -7168
scoreboard players set #worldborderX max 7168
scoreboard players set #worldborderZ min -7168
scoreboard players set #worldborderZ max 7168
# On informe le système qu'on vient de lancer le calcul
scoreboard players set #cartographie on 1

# On définit notre origine au minimum, et notre mode à 1 (descendant)
scoreboard players operation #cartographie posX = #cartographieX min
scoreboard players operation #cartographie posZ = #cartographieZ min
scoreboard players set #cartographie mode 1

# On place le joueur cartographe le plus proche en creative
execute store result score @p[tag=CGCCartographe] mode run data get entity @p[tag=CGCCartographe] playerGameType
gamemode creative @p[tag=CGCCartographe]

# On lance ensuite la fonction de succès comme si on venait d'avancer d'une carte
function cgc:cartographie/prochaine_carte/succes
# On calcule l'ID de carte correspondant à nos pos X/Z actuelles
# Le stockage des ID de carte est réalisé de la manière suivante :
# 1xxxzzz = X positif ou nul, Z positif ou nul (X = 128*xxx, Z = 128*zzz)
# 2xxxzzz = X positif ou nul, Z négatif (X = 128*xxx, Z = -128*zzz)
# 3xxxzzz = X négatif, Z positif ou nul (X = -128*xxx, Z = 128*zzz)
# 4xxxzzz = X négatif, Z négatif (X = -128*xxx, Z = -128*zzz)

# On a donc 4 sous-fonctions pour ces 4 quartiers du monde
execute if score #cartographie posX matches 0.. run function cgc:cartographie/prochaine_carte/calcul_id_carte/x_pos
execute if score #cartographie posX matches ..-1 run function cgc:cartographie/prochaine_carte/calcul_id_carte/x_neg

# On stocke notre nouvel ID de carte dans cgc:cartographie prochaine_carte.id
execute store result storage cgc:cartographie prochaine_carte.id int 1 run scoreboard players get #cartographie id
# On calcule l'ID de carte correspondant à nos pos X/Z actuelles
# Le stockage des ID de carte est réalisé de la manière suivante :
# 3xxxzzz = X négatif, Z positif ou nul (X = -128*xxx, Z = 128*zzz)
# 4xxxzzz = X négatif, Z négatif (X = -128*xxx, Z = -128*zzz)

# On a donc 2 sous-fonctions pour Z pos/neg
execute if score #cartographie posZ matches 0.. run function cgc:cartographie/prochaine_carte/calcul_id_carte/x_neg/z_pos
execute if score #cartographie posZ matches ..-1 run function cgc:cartographie/prochaine_carte/calcul_id_carte/x_neg/z_neg
# On est dans le cas X >= 0, Z >= 0
# On calcule d'abord l'ID de carte ;
# On ajoute 64 avant de diviser par 128 pour toujours se trouver dans la bonne carte,
# même dans le cas (normalement impossible puisqu'on prend le modulo dans l'init...)
# où on est pas au centre de la carte

# On calcule (Z + 64)/128
scoreboard players operation #cartographie deltaZ = #cartographie posZ
scoreboard players set #cartographie temp3 64
scoreboard players operation #cartographie deltaZ += #cartographie temp3
scoreboard players set #cartographie temp3 128
scoreboard players operation #cartographie deltaZ /= #cartographie temp3

# On calcule ((-X + 64)/128) * 1000 (puisque l'ID est de forme xxxzzz)
scoreboard players operation #cartographie deltaX = #cartographie posX
scoreboard players set #cartographie temp3 -1
scoreboard players operation #cartographie deltaX *= #cartographie temp3
scoreboard players set #cartographie temp3 64
scoreboard players operation #cartographie deltaX += #cartographie temp3
scoreboard players set #cartographie temp3 128
scoreboard players operation #cartographie deltaX /= #cartographie temp3
scoreboard players set #cartographie temp3 1000
scoreboard players operation #cartographie deltaX *= #cartographie temp3

# On calcule l'ID en additionnant tout ce beau monde
scoreboard players set #cartographie id 3000000
scoreboard players operation #cartographie id += #cartographie deltaX
scoreboard players operation #cartographie id += #cartographie deltaZ
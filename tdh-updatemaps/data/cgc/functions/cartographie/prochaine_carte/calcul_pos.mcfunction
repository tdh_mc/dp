# On calcule notre nouvelle position
# Notre ancienne position est stockée dans #cartographie posX et posZ
# Les limites de nos coordonnées sont stockées dans #cartographieX min/max et #cartographieZ min/max

# Pour l'instant on fait un truc très basique :
# On va tjrs de gauche à droite, de bas en haut puis de haut en bas (selon notre variable "mode")

# TODO Coder un truc qui charge moins de chunks style une courbe de Hilbert mais pas sûr de la faisabilité
# de ce genre de truc dans Minecraft dans un délai temporel raisonnable
# (peut-être en précalculant tout dans un storage ?)

# On fait une étape avec une variable temporaire car il peut y avoir réassignation de "mode" dans les fonctions exécutées
scoreboard players operation #cartographie temp6 = #cartographie mode

execute if score #cartographie temp6 matches 2 run function cgc:cartographie/prochaine_carte/calcul_pos/asc
execute unless score #cartographie temp6 matches 2 run function cgc:cartographie/prochaine_carte/calcul_pos/desc

scoreboard players reset #cartographie temp6
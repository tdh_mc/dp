# On affiche au joueur un message indiquant qu'on a fini le rendu
tellraw @a[tag=CGCCartographe] [{"text":"[CGC]","color":"gold"},{"text":" Fin du rendu de [","color":"gray","extra":[{"score":{"name":"#cartographieX","objective":"min"}},{"text":";"},{"score":{"name":"#cartographieZ","objective":"min"}},{"text":"] à ["},{"score":{"name":"#cartographieX","objective":"max"}},{"text":";"},{"score":{"name":"#cartographieZ","objective":"max"}},{"text":"]."}]}]

# On désactive la cartographie
scoreboard players set #cartographie on 0

# On replace le joueur cartographe dans son gamemode d'origine
scoreboard players operation #cartographie mode = @p[tag=CGCCartographe] mode
execute if score #cartographie mode matches 0 run gamemode survival @p[tag=CGCCartographe]
execute if score #cartographie mode matches 1 run gamemode creative @p[tag=CGCCartographe]
execute if score #cartographie mode matches 2 run gamemode adventure @p[tag=CGCCartographe]
execute if score #cartographie mode matches 3 run gamemode spectator @p[tag=CGCCartographe]

# On le met au niveau du sol
execute at @p[tag=CGCCartographe] run spreadplayers ~ ~ 0 1 false @p[tag=CGCCartographe]
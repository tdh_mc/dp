# Succès : On passe à la carte suivante
# On calcule notre ID de carte et on le stocke dans le storage
function cgc:cartographie/prochaine_carte/calcul_id_carte

# On affiche un message au joueur cartographe
tellraw @a[tag=CGCCartographe] [{"text":"[CGC]","color":"gold"},{"text":" Rendu de la carte en [","color":"gray","extra":[{"score":{"name":"#cartographie","objective":"posX"}},{"text":";"},{"score":{"name":"#cartographie","objective":"posZ"}},{"text":"], d'ID "},{"score":{"name":"#cartographie","objective":"id"}},{"text":"."}]}]

# On donne au joueur cartographe la nouvelle carte
# (après avoir mis l'ancienne en main gauche juste au cas où)
item replace entity @p[tag=CGCCartographe] weapon.offhand from entity @p[tag=CGCCartographe] weapon.mainhand
item replace entity @p[tag=CGCCartographe] weapon.mainhand with filled_map{map:1000000} 1
item modify entity @p[tag=CGCCartographe] weapon.mainhand cgc:copier_prochaine_carte

# On invoque un item à notre position (mais tjrs à l'altitude 240)
# (comme ça même s'il y a un souci il dépopera au bout de quelques instants dans le chunk)
summon item ~ 240 ~ {NoGravity:1b,Age:5950s,PickupDelay:32767s,Item:{id:"minecraft:stone",Count:1b,tag:{BlockMerging:1b}},Tags:["CGCTempMarker"]}
# On exécute la téléportation proprement dite en tant que cet item
execute as @e[type=item,tag=CGCTempMarker,limit=1] run function cgc:cartographie/prochaine_carte/tp_marqueur
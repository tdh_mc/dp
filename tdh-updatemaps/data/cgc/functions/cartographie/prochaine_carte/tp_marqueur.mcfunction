# On exécute cette fonction en tant qu'une entité temporaire "marqueur",
# pour téléporter le joueur avec le tag CGCCartographe à notre position,
# après l'avoir définie

# On enregistre la nouvelle position
execute store result entity @s Pos[0] double 1 run scoreboard players get #cartographie posX
execute store result entity @s Pos[2] double 1 run scoreboard players get #cartographie posZ

# On téléporte le joueur dessus
execute at @s run tp @p[tag=CGCCartographe] ~ ~ ~

# On se tue
kill @s
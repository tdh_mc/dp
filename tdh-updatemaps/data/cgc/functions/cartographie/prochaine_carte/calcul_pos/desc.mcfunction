# On calcule notre nouvelle position
# Notre ancienne position est stockée dans #cartographie posX et posZ
# Les limites de nos coordonnées sont stockées dans #cartographieX min/max et #cartographieZ min/max

# On est actuellement en mode DESCENDANT
# Si on est au maximum en Z, on ajoute 128 à notre score X, et on inverse notre mode
execute if score #cartographie posZ >= #cartographieZ max run function cgc:cartographie/prochaine_carte/calcul_pos/end_colonne
# Si on n'est pas au maximum en Z, on ajoute 128 à notre score Z
execute if score #cartographie posZ < #cartographieZ max run function cgc:cartographie/prochaine_carte/calcul_pos/desc_step
# On passe à la colonne suivante
scoreboard players add #cartographie posX 128

# On définit notre mode à ASCENDANT si on est au max et DESCENDANT si on est au min
execute if score #cartographie posZ >= #cartographieZ max run scoreboard players set #cartographie mode 2
execute if score #cartographie posZ <= #cartographieZ min run scoreboard players set #cartographie mode 1
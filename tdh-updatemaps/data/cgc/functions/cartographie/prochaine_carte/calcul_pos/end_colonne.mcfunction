# On calcule notre nouvelle position
# Notre ancienne position est stockée dans #cartographie posX et posZ
# Les limites de nos coordonnées sont stockées dans #cartographieX min/max et #cartographieZ min/max

# On est arrivés au maximum de notre colonne
# Si notre position X est également au maximum, on termine la génération de carte
execute if score #cartographie posX >= #cartographieX max run function cgc:cartographie/prochaine_carte/calcul_pos/end
# Si notre position X n'est pas au maximum, on l'augmente d'une carte
execute if score #cartographie posX < #cartographieX max run function cgc:cartographie/prochaine_carte/calcul_pos/prochaine_colonne
# Cette fonction est exécutée 4 fois par seconde pour tester si une porte a besoin d'être ouverte

# Pour les ponts-levis
execute as @e[type=armor_stand,tag=PontLevis] at @s run function tdh:porte/pont_levis/tick

# Pour les portes
execute as @e[type=armor_stand,tag=Porte] at @s run function tdh:porte/porte/tick
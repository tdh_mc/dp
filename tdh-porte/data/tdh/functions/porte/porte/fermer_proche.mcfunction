# On ferme la porte la plus proche si elle est ouverte
execute as @e[tag=Porte,sort=nearest,limit=1,distance=..200] run tag @s[tag=Ouvert] add Fermeture
execute as @e[tag=Porte,sort=nearest,limit=1,distance=..200] run tag @s[tag=Ouvert] remove Ouvert
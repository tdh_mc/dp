# On ouvre la porte la plus proche si elle est fermée
execute as @e[tag=Porte,sort=nearest,limit=1,distance=..200] run tag @s[tag=Ferme] add Ouverture
execute as @e[tag=Porte,sort=nearest,limit=1,distance=..200] run tag @s[tag=Ferme] remove Ferme
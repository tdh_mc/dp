# En fonction du type de porte actuel

# Les portes "slide" se décalent simplement sur un axe donné
# Par exemple slideUp correspond à une porte qui s'ouvre vers le haut, comme une herse
# slideNorth correspondra à une porte qui s'ouvre vers le nord et se referme vers le sud
execute as @s[tag=SlideHaut] run function tdh:porte/porte/slide_haut/tick
execute as @s[tag=SlideBas] run function tdh:porte/porte/slide_bas/tick
execute as @s[tag=SlideEst] run function tdh:porte/porte/slide_est/tick
execute as @s[tag=SlideOuest] run function tdh:porte/porte/slide_ouest/tick
execute as @s[tag=SlideNord] run function tdh:porte/porte/slide_nord/tick
execute as @s[tag=SlideSud] run function tdh:porte/porte/slide_sud/tick
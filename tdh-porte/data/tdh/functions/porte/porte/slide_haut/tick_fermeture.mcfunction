# On décale d'abord la porte de 1 vers le haut
execute as @s[tag=Grande,tag=X] run clone ~-6 ~0 ~ ~6 ~-12 ~ ~-6 ~-13 ~ filtered #tdh:porte move
execute as @s[tag=Moyenne,tag=X] run clone ~-4 ~0 ~ ~4 ~-10 ~ ~-4 ~-11 ~ filtered #tdh:porte move
execute as @s[tag=Petite,tag=X] run clone ~-2 ~0 ~ ~2 ~-7 ~ ~-2 ~-8 ~ filtered #tdh:porte move

execute as @s[tag=Grande,tag=Z] run clone ~ ~0 ~-6 ~ ~-12 ~6 ~ ~-13 ~-6 filtered #tdh:porte move
execute as @s[tag=Moyenne,tag=Z] run clone ~ ~0 ~-4 ~ ~-10 ~4 ~ ~-11 ~-4 filtered #tdh:porte move
execute as @s[tag=Petite,tag=Z] run clone ~ ~0 ~-2 ~ ~-7 ~2 ~ ~-8 ~-2 filtered #tdh:porte move

# On joue un son si on est au début de la fermeture
execute if score @s status = @s max run function tdh:sound/porte/fermeture

# On retire 1 à notre variable de contrôle
scoreboard players remove @s status 1

# On slide avec la porte
tp @s ~ ~-1 ~

# Si on est arrivés au minimum, on se retire le tag
execute if score @s status matches ..0 run function tdh:porte/porte/slide_haut/fin_fermeture
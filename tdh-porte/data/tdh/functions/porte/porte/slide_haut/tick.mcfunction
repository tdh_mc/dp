# On est une porte Slide Haut (s'ouvre/ferme de haut en bas)
# Le statut de la porte détermine sa position ;
# Tag "Ferme" et status = 0 : la porte est fermée
# Tag "Ouvert" et status > 0 : la porte est totalement ouverte
# Aucun de ces deux tags : la porte est en cours d'ouverture ("Ouverture") ou de fermeture ("Fermeture")

# Si on est en cours d'ouverture
execute if score @s[tag=Ouverture] max matches 1.. run function tdh:porte/porte/slide_haut/tick_ouverture

# Si on est en cours de fermeture
execute as @s[tag=Fermeture] run function tdh:porte/porte/slide_haut/tick_fermeture
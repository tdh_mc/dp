fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 spruce_slab[type=top]


fill ~-2 ~-18 ~-05 ~2 ~-18 ~-05 spruce_stairs[facing=south,half=top]


fill ~-2 ~-15 ~-09 ~2 ~-15 ~-09 spruce_slab[type=top]
fill ~-2 ~-15 ~-08 ~2 ~-15 ~-08 spruce_slab[type=double]
fill ~-2 ~-15 ~-07 ~2 ~-15 ~-07 spruce_slab[type=bottom]


fill ~-2 ~-14 ~-10 ~2 ~-14 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-14 ~-09 ~2 ~-14 ~-09 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-14 ~-08 ~2 ~-14 ~-08 air


fill ~-2 ~-13 ~-11 ~2 ~-13 ~-11 spruce_stairs[facing=south,half=top]
fill ~-2 ~-13 ~-10 ~2 ~-13 ~-10 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-13 ~-09 ~2 ~-13 ~-09 air


fill ~-2 ~-12 ~-12 ~2 ~-12 ~-12 spruce_stairs[facing=south,half=top]
fill ~-2 ~-12 ~-11 ~2 ~-12 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-12 ~-10 ~2 ~-12 ~-10 air


fill ~-2 ~-11 ~-13 ~2 ~-11 ~-13 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-12 ~2 ~-11 ~-12 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-11 ~-11 ~2 ~-11 ~-11 air


fill ~-2 ~-10 ~-15 ~2 ~-10 ~-15 spruce_slab[type=top]
fill ~-2 ~-10 ~-14 ~2 ~-10 ~-14 spruce_slab[type=double]
fill ~-2 ~-10 ~-13 ~2 ~-10 ~-13 spruce_slab[type=bottom]
fill ~-2 ~-10 ~-12 ~2 ~-10 ~-12 air


fill ~-2 ~-09 ~-16 ~2 ~-09 ~-16 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-15 ~2 ~-09 ~-15 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-09 ~-14 ~2 ~-09 ~-13 air


fill ~-2 ~-08 ~-17 ~2 ~-08 ~-17 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-16 ~2 ~-08 ~-16 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-08 ~-15 ~2 ~-08 ~-14 air


setblock ~-3 ~-07 ~-18 spruce_slab[type=top]
setblock ~3 ~-07 ~-18 spruce_slab[type=top]
fill ~-2 ~-07 ~-18 ~2 ~-07 ~-18 spruce_stairs[facing=south,half=top]
fill ~-2 ~-07 ~-17 ~2 ~-07 ~-17 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-07 ~-16 ~2 ~-07 ~-15 air


setblock ~-3 ~-06 ~-19 spruce_stairs[facing=south,half=top]
setblock ~3 ~-06 ~-19 spruce_stairs[facing=south,half=top]
fill ~-3 ~-06 ~-18 ~-3 ~-06 ~-16 iron_bars
fill ~3 ~-06 ~-18 ~3 ~-06 ~-16 iron_bars

fill ~-2 ~-06 ~-18 ~2 ~-06 ~-18 spruce_slab[type=bottom]
fill ~-2 ~-06 ~-17 ~2 ~-06 ~-16 air


fill ~-3 ~-05 ~-18 ~-3 ~-05 ~-17 air
fill ~3 ~-05 ~-18 ~3 ~-05 ~-17 air
setblock ~-3 ~-05 ~-14 iron_bars
setblock ~3 ~-05 ~-14 iron_bars

fill ~-2 ~-05 ~-17 ~2 ~-05 ~-17 air


setblock ~-3 ~-04 ~-15 air
setblock ~3 ~-04 ~-15 air
setblock ~-3 ~-04 ~-12 iron_bars
setblock ~3 ~-04 ~-12 iron_bars


setblock ~-3 ~-03 ~-13 air
setblock ~3 ~-03 ~-13 air
setblock ~-3 ~-03 ~-10 iron_bars
setblock ~3 ~-03 ~-10 iron_bars


setblock ~-3 ~-02 ~-11 air
setblock ~3 ~-02 ~-11 air
setblock ~-3 ~-02 ~-08 iron_bars
setblock ~3 ~-02 ~-08 iron_bars


setblock ~-3 ~-01 ~-09 air
setblock ~3 ~-01 ~-09 air



setblock ~-3 ~0 ~7 iron_block
setblock ~-3 ~0 ~8 chain[axis=z]
setblock ~3 ~0 ~7 iron_block
setblock ~3 ~0 ~8 chain[axis=z]

scoreboard players set @s status 18
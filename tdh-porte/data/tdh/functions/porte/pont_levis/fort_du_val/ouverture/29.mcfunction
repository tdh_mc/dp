fill ~-2 ~-20 ~-08 ~2 ~-20 ~-07 spruce_slab[type=top]


fill ~-3 ~-19 ~-23 ~3 ~-19 ~-23 spruce_slab[type=top]
fill ~-2 ~-19 ~-22 ~2 ~-19 ~-17 spruce_slab[type=top]
fill ~-2 ~-19 ~-16 ~2 ~-19 ~-11 spruce_slab[type=double]
fill ~-2 ~-19 ~-08 ~2 ~-19 ~-06 spruce_slab[type=bottom]


fill ~-3 ~-18 ~-23 ~-3 ~-18 ~-22 iron_bars
fill ~3 ~-18 ~-23 ~3 ~-18 ~-22 iron_bars

fill ~-2 ~-18 ~-23 ~2 ~-18 ~-17 spruce_slab[type=bottom]
fill ~-2 ~-18 ~-16 ~2 ~-18 ~-11 air


setblock ~-3 ~-17 ~-23 air
setblock ~3 ~-17 ~-23 air
setblock ~-3 ~-17 ~-21 iron_bars
setblock ~3 ~-17 ~-21 iron_bars

fill ~-2 ~-17 ~-23 ~2 ~-17 ~-20 air


setblock ~-3 ~-16 ~-22 air
setblock ~3 ~-16 ~-22 air
setblock ~-3 ~-16 ~-20 iron_bars
setblock ~3 ~-16 ~-20 iron_bars


setblock ~-3 ~-15 ~-21 air
setblock ~3 ~-15 ~-21 air
setblock ~-3 ~-15 ~-19 iron_bars
setblock ~3 ~-15 ~-19 iron_bars


setblock ~-3 ~-14 ~-20 air
setblock ~3 ~-14 ~-20 air
setblock ~-3 ~-14 ~-18 iron_bars
setblock ~3 ~-14 ~-18 iron_bars


setblock ~-3 ~-13 ~-19 air
setblock ~3 ~-13 ~-19 air
setblock ~-3 ~-13 ~-17 iron_bars
setblock ~3 ~-13 ~-17 iron_bars


setblock ~-3 ~-12 ~-18 air
setblock ~3 ~-12 ~-18 air
setblock ~-3 ~-12 ~-16 iron_bars
setblock ~3 ~-12 ~-16 iron_bars


setblock ~-3 ~-11 ~-17 air
setblock ~3 ~-11 ~-17 air


setblock ~-3 ~-08 ~-11 iron_bars
setblock ~3 ~-08 ~-11 iron_bars


setblock ~-3 ~-07 ~-12 air
setblock ~3 ~-07 ~-12 air
setblock ~-3 ~-07 ~-10 iron_bars
setblock ~3 ~-07 ~-10 iron_bars


setblock ~-3 ~-06 ~-11 air
setblock ~3 ~-06 ~-11 air
setblock ~-3 ~-06 ~-09 iron_bars
setblock ~3 ~-06 ~-09 iron_bars


setblock ~-3 ~-05 ~-10 air
setblock ~3 ~-05 ~-10 air



setblock ~-3 ~0 ~1 iron_block
setblock ~-3 ~0 ~2 chain[axis=z]
setblock ~3 ~0 ~1 iron_block
setblock ~3 ~0 ~2 chain[axis=z]

scoreboard players set @s status 6
fill ~-3 ~-25 ~-22 ~3 ~-25 ~-22 spruce_slab[type=top]


setblock ~-3 ~-24 ~-22 iron_bars
setblock ~3 ~-24 ~-22 iron_bars
fill ~-2 ~-24 ~-22 ~2 ~-24 ~-21 spruce_slab[type=bottom]
fill ~-2 ~-24 ~-20 ~2 ~-24 ~-19 spruce_slab[type=double]
fill ~-2 ~-24 ~-18 ~2 ~-24 ~-16 spruce_slab[type=top]


fill ~-2 ~-23 ~-22 ~2 ~-23 ~-19 air
fill ~-2 ~-23 ~-18 ~2 ~-23 ~-17 spruce_slab[type=bottom]
fill ~-2 ~-23 ~-16 ~2 ~-23 ~-15 spruce_slab[type=double]
fill ~-2 ~-23 ~-14 ~2 ~-23 ~-13 spruce_slab[type=top]


setblock ~-3 ~-22 ~-21 iron_bars
setblock ~3 ~-22 ~-21 iron_bars

fill ~-2 ~-22 ~-18 ~2 ~-22 ~-15 air
fill ~-2 ~-22 ~-14 ~2 ~-22 ~-13 spruce_slab[type=bottom]
fill ~-2 ~-22 ~-12 ~2 ~-22 ~-11 spruce_slab[type=double]
fill ~-2 ~-22 ~-10 ~2 ~-22 ~-09 spruce_slab[type=top]


setblock ~-3 ~-21 ~-22 air
setblock ~3 ~-21 ~-22 air
setblock ~-3 ~-21 ~-20 iron_bars
setblock ~3 ~-21 ~-20 iron_bars

fill ~-2 ~-21 ~-13 ~2 ~-21 ~-11 air
fill ~-2 ~-21 ~-10 ~2 ~-21 ~-09 spruce_slab[type=bottom]
fill ~-2 ~-21 ~-07 ~2 ~-21 ~-07 spruce_slab[type=double]
fill ~-2 ~-21 ~-05 ~2 ~-21 ~-05 spruce_slab[type=top]


setblock ~-3 ~-20 ~-21 air
setblock ~3 ~-20 ~-21 air

fill ~-2 ~-20 ~-08 ~2 ~-20 ~-07 air
fill ~-2 ~-20 ~-06 ~2 ~-20 ~-05 spruce_slab[type=bottom]
fill ~-2 ~-20 ~-03 ~2 ~-20 ~-03 spruce_slab[type=double]


setblock ~-3 ~-19 ~-19 iron_bars
setblock ~3 ~-19 ~-19 iron_bars

fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 air


setblock ~-3 ~-18 ~-20 air
setblock ~3 ~-18 ~-20 air
setblock ~-3 ~-18 ~-18 iron_bars
setblock ~3 ~-18 ~-18 iron_bars


setblock ~-3 ~-17 ~-19 air
setblock ~3 ~-17 ~-19 air
setblock ~-3 ~-17 ~-17 iron_bars
setblock ~3 ~-17 ~-17 iron_bars


setblock ~-3 ~-16 ~-18 air
setblock ~3 ~-16 ~-18 air
setblock ~-3 ~-16 ~-16 iron_bars
setblock ~3 ~-16 ~-16 iron_bars


setblock ~-3 ~-15 ~-17 air
setblock ~3 ~-15 ~-17 air


setblock ~-3 ~-14 ~-15 iron_bars
setblock ~3 ~-14 ~-15 iron_bars


setblock ~-3 ~-13 ~-16 air
setblock ~3 ~-13 ~-16 air
setblock ~-3 ~-13 ~-14 iron_bars
setblock ~3 ~-13 ~-14 iron_bars


setblock ~-3 ~-12 ~-15 air
setblock ~3 ~-12 ~-15 air
setblock ~-3 ~-12 ~-13 iron_bars
setblock ~3 ~-12 ~-13 iron_bars


setblock ~-3 ~-11 ~-14 air
setblock ~3 ~-11 ~-14 air
setblock ~-3 ~-11 ~-12 iron_bars
setblock ~3 ~-11 ~-12 iron_bars


setblock ~-3 ~-10 ~-13 air
setblock ~3 ~-10 ~-13 air



setblock ~-3 ~0 ~-2 iron_block
setblock ~-3 ~0 ~-1 chain[axis=z]
setblock ~3 ~0 ~-2 iron_block
setblock ~3 ~0 ~-1 chain[axis=z]

scoreboard players set @s status 0
tag @s remove Ouverture
tag @s add Ouvert
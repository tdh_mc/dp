fill ~-3 ~-22 ~-22 ~3 ~-22 ~-22 spruce_slab[type=top]
fill ~-2 ~-22 ~-21 ~2 ~-22 ~-19 spruce_slab[type=top]


setblock ~-3 ~-21 ~-22 iron_bars
setblock ~3 ~-21 ~-22 iron_bars

fill ~-3 ~-21 ~-23 ~3 ~-21 ~-23 air
fill ~-2 ~-21 ~-22 ~2 ~-21 ~-19 spruce_slab[type=bottom]
fill ~-2 ~-21 ~-18 ~2 ~-21 ~-15 spruce_slab[type=double]
fill ~-2 ~-21 ~-14 ~2 ~-21 ~-10 spruce_slab[type=top]


setblock ~-3 ~-20 ~-23 air
setblock ~3 ~-20 ~-23 air
setblock ~-3 ~-20 ~-22 iron_bars
setblock ~3 ~-20 ~-22 iron_bars

fill ~-2 ~-20 ~-23 ~2 ~-20 ~-15 air
fill ~-2 ~-20 ~-14 ~2 ~-20 ~-10 spruce_slab[type=bottom]
fill ~-2 ~-20 ~-08 ~2 ~-20 ~-06 spruce_slab[type=double]


setblock ~-3 ~-19 ~-23 air
setblock ~3 ~-19 ~-23 air
setblock ~-3 ~-19 ~-21 iron_bars
setblock ~3 ~-19 ~-21 iron_bars

fill ~-2 ~-19 ~-08 ~2 ~-19 ~-06 air


setblock ~-3 ~-18 ~-22 air
setblock ~3 ~-18 ~-22 air
setblock ~-3 ~-18 ~-20 iron_bars
setblock ~3 ~-18 ~-20 iron_bars


setblock ~-3 ~-17 ~-21 air
setblock ~3 ~-17 ~-21 air

scoreboard players set @s status 3
fill ~-3 ~-23 ~-22 ~3 ~-23 ~-22 spruce_slab[type=top]
fill ~-2 ~-23 ~-21 ~2 ~-23 ~-20 spruce_slab[type=top]


setblock ~-3 ~-22 ~-22 iron_bars
setblock ~3 ~-22 ~-22 iron_bars

fill ~-2 ~-22 ~-22 ~2 ~-22 ~-20 spruce_slab[type=bottom]
fill ~-2 ~-22 ~-19 ~2 ~-22 ~-17 spruce_slab[type=double]
fill ~-2 ~-22 ~-16 ~2 ~-22 ~-14 spruce_slab[type=top]


fill ~-2 ~-21 ~-22 ~2 ~-21 ~-17 air
fill ~-2 ~-21 ~-16 ~2 ~-21 ~-14 spruce_slab[type=bottom]
fill ~-2 ~-21 ~-13 ~2 ~-21 ~-11 spruce_slab[type=double]
fill ~-2 ~-21 ~-09 ~2 ~-21 ~-08 spruce_slab[type=top]


setblock ~-3 ~-20 ~-21 iron_bars
setblock ~3 ~-20 ~-21 iron_bars

fill ~-2 ~-20 ~-14 ~2 ~-20 ~-11 air
fill ~-2 ~-20 ~-09 ~2 ~-20 ~-08 spruce_slab[type=bottom]
fill ~-2 ~-20 ~-05 ~2 ~-20 ~-05 spruce_slab[type=double]


setblock ~-3 ~-19 ~-22 air
setblock ~3 ~-19 ~-22 air
setblock ~-3 ~-19 ~-20 iron_bars
setblock ~3 ~-19 ~-20 iron_bars

fill ~-2 ~-19 ~-05 ~2 ~-19 ~-05 air


setblock ~-3 ~-18 ~-21 air
setblock ~3 ~-18 ~-21 air


setblock ~-3 ~-17 ~-19 iron_bars
setblock ~3 ~-17 ~-19 iron_bars


setblock ~-3 ~-16 ~-20 air
setblock ~3 ~-16 ~-20 air
setblock ~-3 ~-16 ~-18 iron_bars
setblock ~3 ~-16 ~-18 iron_bars


setblock ~-3 ~-15 ~-19 air
setblock ~3 ~-15 ~-19 air
setblock ~-3 ~-15 ~-17 iron_bars
setblock ~3 ~-15 ~-17 iron_bars


setblock ~-3 ~-14 ~-18 air
setblock ~3 ~-14 ~-18 air
setblock ~-3 ~-14 ~-16 iron_bars
setblock ~3 ~-14 ~-16 iron_bars


setblock ~-3 ~-13 ~-17 air
setblock ~3 ~-13 ~-17 air



setblock ~-3 ~0 ~-1 iron_block
setblock ~-3 ~0 ~0 chain[axis=z]
setblock ~3 ~0 ~-1 iron_block
setblock ~3 ~0 ~0 chain[axis=z]

scoreboard players set @s status 2
fill ~-2 ~-18 ~-11 ~2 ~-18 ~-11 spruce_slab[type=top]
fill ~-2 ~-18 ~-09 ~2 ~-18 ~-09 spruce_slab[type=double]
fill ~-2 ~-18 ~-07 ~2 ~-18 ~-07 spruce_slab[type=bottom]

fill ~-2 ~-17 ~-15 ~2 ~-17 ~-14 spruce_slab[type=top]
fill ~-2 ~-17 ~-13 ~2 ~-17 ~-12 spruce_slab[type=double]
fill ~-2 ~-17 ~-11 ~2 ~-17 ~-10 spruce_slab[type=bottom]
fill ~-2 ~-17 ~-09 ~2 ~-17 ~-09 air

fill ~-2 ~-16 ~-19 ~2 ~-16 ~-18 spruce_slab[type=top]
fill ~-2 ~-16 ~-17 ~2 ~-16 ~-16 spruce_slab[type=double]
fill ~-2 ~-16 ~-15 ~2 ~-16 ~-14 spruce_slab[type=bottom]
fill ~-2 ~-16 ~-13 ~2 ~-16 ~-12 air

fill ~-3 ~-15 ~-22 ~3 ~-15 ~-22 spruce_slab[type=top]
fill ~-2 ~-15 ~-21 ~2 ~-15 ~-20 spruce_slab[type=double]
fill ~-2 ~-15 ~-19 ~2 ~-15 ~-18 spruce_slab[type=bottom]
fill ~-2 ~-15 ~-17 ~2 ~-15 ~-15 air


fill ~-3 ~-14 ~-22 ~-3 ~-14 ~-21 iron_bars
fill ~3 ~-14 ~-22 ~3 ~-14 ~-21 iron_bars

fill ~-2 ~-14 ~-22 ~2 ~-14 ~-22 spruce_slab[type=bottom]
fill ~-2 ~-14 ~-21 ~2 ~-14 ~-18 air


setblock ~-3 ~-13 ~-22 air
setblock ~3 ~-13 ~-22 air
setblock ~-3 ~-13 ~-20 iron_bars
setblock ~3 ~-13 ~-20 iron_bars

fill ~-2 ~-13 ~-22 ~2 ~-13 ~-21 air


setblock ~-3 ~-12 ~-21 air
setblock ~3 ~-12 ~-21 air
fill ~-3 ~-12 ~-19 ~-3 ~-12 ~-18 iron_bars
fill ~3 ~-12 ~-19 ~3 ~-12 ~-18 iron_bars


fill ~-3 ~-11 ~-20 ~-3 ~-11 ~-19 air
fill ~3 ~-11 ~-20 ~3 ~-11 ~-19 air
setblock ~-3 ~-11 ~-17 iron_bars
setblock ~3 ~-11 ~-17 iron_bars


setblock ~-3 ~-10 ~-18 air
setblock ~3 ~-10 ~-18 air
setblock ~-3 ~-10 ~-16 iron_bars
setblock ~3 ~-10 ~-16 iron_bars


setblock ~-3 ~-09 ~-17 air
setblock ~3 ~-09 ~-17 air
fill ~-3 ~-09 ~-15 ~-3 ~-09 ~-14 iron_bars
fill ~3 ~-09 ~-15 ~3 ~-09 ~-14 iron_bars


fill ~-3 ~-08 ~-16 ~-3 ~-08 ~-15 air
fill ~3 ~-08 ~-16 ~3 ~-08 ~-15 air
setblock ~-3 ~-08 ~-13 iron_bars
setblock ~3 ~-08 ~-13 iron_bars


setblock ~-3 ~-07 ~-14 air
setblock ~3 ~-07 ~-14 air
setblock ~-3 ~-07 ~-12 iron_bars
setblock ~3 ~-07 ~-12 iron_bars


setblock ~-3 ~-06 ~-13 air
setblock ~3 ~-06 ~-13 air
setblock ~-3 ~-06 ~-11 iron_bars
setblock ~3 ~-06 ~-11 iron_bars


setblock ~-3 ~-05 ~-12 air
setblock ~3 ~-05 ~-12 air
setblock ~-3 ~-05 ~-09 iron_bars
setblock ~3 ~-05 ~-09 iron_bars


setblock ~-3 ~-04 ~-10 air
setblock ~3 ~-04 ~-10 air
setblock ~-3 ~-04 ~-08 iron_bars
setblock ~3 ~-04 ~-08 iron_bars


setblock ~-3 ~-03 ~-09 air
setblock ~3 ~-03 ~-09 air


setblock ~-3 ~-01 ~-04 iron_bars
setblock ~3 ~-01 ~-04 iron_bars


setblock ~-3 ~-00 ~-05 air
setblock ~3 ~-00 ~-05 air



setblock ~-3 ~0 ~3 iron_block
setblock ~-3 ~0 ~4 chain[axis=z]
setblock ~3 ~0 ~3 iron_block
setblock ~3 ~0 ~4 chain[axis=z]

scoreboard players set @s status 10
fill ~-2 ~-18 ~-03 ~2 ~-18 ~-03 spruce_stairs[facing=south,half=top]


fill ~-2 ~-17 ~-03 ~2 ~-17 ~-03 spruce_slab[type=double]
fill ~-2 ~-17 ~-02 ~2 ~-17 ~-02 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-16 ~-02 ~2 ~-16 ~-02 air


fill ~-2 ~-14 ~-04 ~2 ~-14 ~-04 spruce_stairs[facing=south,half=top]


fill ~-2 ~-13 ~-04 ~2 ~-13 ~-04 spruce_slab[type=double]
fill ~-2 ~-13 ~-03 ~2 ~-13 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-12 ~-03 ~2 ~-12 ~-03 air


fill ~-2 ~-11 ~-05 ~2 ~-11 ~-05 spruce_stairs[facing=south,half=top]


fill ~-2 ~-10 ~-05 ~2 ~-10 ~-05 spruce_slab[type=double]
fill ~-2 ~-10 ~-04 ~2 ~-10 ~-04 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-09 ~-05 ~2 ~-09 ~-05 spruce_slab[type=double]
fill ~-2 ~-09 ~-04 ~2 ~-09 ~-04 air


fill ~-2 ~-08 ~-04 ~2 ~-08 ~-04 air


fill ~-2 ~-07 ~-06 ~2 ~-07 ~-06 spruce_stairs[facing=south,half=top]


fill ~-2 ~-06 ~-06 ~2 ~-06 ~-06 spruce_slab[type=double]
fill ~-2 ~-06 ~-05 ~2 ~-06 ~-05 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-05 ~-06 ~2 ~-05 ~-06 spruce_slab[type=double]
fill ~-2 ~-05 ~-05 ~2 ~-05 ~-05 air


fill ~-2 ~-04 ~-07 ~2 ~-04 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-04 ~-05 ~2 ~-04 ~-05 air


fill ~-2 ~-03 ~-07 ~2 ~-03 ~-07 spruce_slab[type=double]
fill ~-2 ~-03 ~-06 ~2 ~-03 ~-06 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-02 ~-07 ~2 ~-01 ~-07 spruce_slab[type=double]
fill ~-2 ~-02 ~-06 ~2 ~-01 ~-06 air


setblock ~-3 ~-00 ~-06 iron_bars
setblock ~3 ~-00 ~-06 iron_bars
fill ~-2 ~-00 ~-06 ~2 ~-00 ~-06 air
setblock ~-3 ~-00 ~-07 spruce_stairs[facing=south,half=top]
setblock ~3 ~-00 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-00 ~-07 ~2 ~-00 ~-07 spruce_slab[type=double]



setblock ~-3 ~0 ~16 iron_block
setblock ~-3 ~0 ~17 chain[axis=z]
setblock ~3 ~0 ~16 iron_block
setblock ~3 ~0 ~17 chain[axis=z]

scoreboard players set @s status 30
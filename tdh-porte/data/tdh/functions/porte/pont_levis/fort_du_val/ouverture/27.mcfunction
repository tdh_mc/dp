fill ~-2 ~-20 ~-05 ~2 ~-20 ~-04 spruce_slab[type=top]


fill ~-2 ~-19 ~-11 ~2 ~-19 ~-09 spruce_slab[type=top]
fill ~-2 ~-19 ~-07 ~2 ~-19 ~-06 spruce_slab[type=double]
fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 spruce_slab[type=bottom]


fill ~-2 ~-18 ~-18 ~2 ~-18 ~-15 spruce_slab[type=top]
fill ~-2 ~-18 ~-14 ~2 ~-18 ~-12 spruce_slab[type=double]
fill ~-2 ~-18 ~-11 ~2 ~-18 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-18 ~-10 ~2 ~-18 ~-08 spruce_slab[type=bottom]
fill ~-2 ~-18 ~-07 ~2 ~-18 ~-06 air


fill ~-3 ~-17 ~-23 ~3 ~-17 ~-23 spruce_slab[type=top]
fill ~-2 ~-17 ~-22 ~2 ~-17 ~-22 spruce_slab[type=top]
fill ~-2 ~-17 ~-21 ~2 ~-17 ~-19 spruce_slab[type=double]
fill ~-2 ~-17 ~-18 ~2 ~-17 ~-18 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-17 ~-17 ~2 ~-17 ~-15 spruce_slab[type=bottom]
fill ~-2 ~-17 ~-14 ~2 ~-17 ~-11 air


fill ~-3 ~-16 ~-23 ~-3 ~-16 ~-22 iron_bars
fill ~3 ~-16 ~-23 ~3 ~-16 ~-22 iron_bars

fill ~-2 ~-16 ~-23 ~2 ~-16 ~-22 spruce_slab[type=bottom]
fill ~-2 ~-16 ~-21 ~2 ~-16 ~-16 air


fill ~-2 ~-15 ~-22 ~2 ~-15 ~-21 air


setblock ~-3 ~-10 ~-15 iron_bars
setblock ~3 ~-10 ~-15 iron_bars


setblock ~-3 ~-09 ~-16 air
setblock ~3 ~-09 ~-16 air


setblock ~-3 ~-07 ~-11 iron_bars
setblock ~3 ~-07 ~-11 iron_bars


setblock ~-3 ~-06 ~-12 air
setblock ~3 ~-06 ~-12 air



setblock ~-3 ~0 ~2 iron_block
setblock ~-3 ~0 ~3 chain[axis=z]
setblock ~3 ~0 ~2 iron_block
setblock ~3 ~0 ~3 chain[axis=z]

scoreboard players set @s status 8
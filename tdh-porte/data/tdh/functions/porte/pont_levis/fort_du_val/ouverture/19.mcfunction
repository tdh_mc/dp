fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 spruce_slab[type=top]
fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 spruce_slab[type=double]
fill ~-2 ~-19 ~-02 ~2 ~-19 ~-02 spruce_slab[type=bottom]


fill ~-2 ~-18 ~-06 ~2 ~-18 ~-06 spruce_slab[type=top]
fill ~-2 ~-18 ~-05 ~2 ~-18 ~-05 spruce_slab[type=double]
fill ~-2 ~-18 ~-04 ~2 ~-18 ~-04 spruce_slab[type=bottom]
fill ~-2 ~-18 ~-03 ~2 ~-18 ~-03 air


fill ~-2 ~-17 ~-07 ~2 ~-17 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-17 ~-06 ~2 ~-17 ~-06 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-17 ~-05 ~2 ~-17 ~-05 air


fill ~-2 ~-16 ~-09 ~2 ~-16 ~-09 spruce_slab[type=top]
fill ~-2 ~-16 ~-08 ~2 ~-16 ~-08 spruce_slab[type=double]
fill ~-2 ~-16 ~-07 ~2 ~-16 ~-07 spruce_slab[type=bottom]
fill ~-2 ~-16 ~-06 ~2 ~-16 ~-06 air


fill ~-2 ~-15 ~-11 ~2 ~-15 ~-11 spruce_slab[type=top]
fill ~-2 ~-15 ~-10 ~2 ~-15 ~-10 spruce_slab[type=double]
fill ~-2 ~-15 ~-09 ~2 ~-15 ~-09 spruce_slab[type=bottom]
fill ~-2 ~-15 ~-08 ~2 ~-15 ~-08 air


fill ~-2 ~-14 ~-12 ~2 ~-14 ~-12 spruce_stairs[facing=south,half=top]
fill ~-2 ~-14 ~-11 ~2 ~-14 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-14 ~-10 ~2 ~-14 ~-09 air


fill ~-2 ~-13 ~-14 ~2 ~-13 ~-14 spruce_slab[type=top]
fill ~-2 ~-13 ~-13 ~2 ~-13 ~-13 spruce_slab[type=double]
fill ~-2 ~-13 ~-12 ~2 ~-13 ~-12 spruce_slab[type=bottom]
fill ~-2 ~-13 ~-11 ~2 ~-13 ~-11 air


fill ~-2 ~-12 ~-16 ~2 ~-12 ~-16 spruce_slab[type=top]
fill ~-2 ~-12 ~-15 ~2 ~-12 ~-15 spruce_slab[type=double]
fill ~-2 ~-12 ~-14 ~2 ~-12 ~-14 spruce_slab[type=bottom]
fill ~-2 ~-12 ~-13 ~2 ~-12 ~-12 air


fill ~-2 ~-11 ~-17 ~2 ~-11 ~-17 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-16 ~2 ~-11 ~-16 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-11 ~-15 ~2 ~-11 ~-14 air


fill ~-2 ~-10 ~-19 ~2 ~-10 ~-19 spruce_slab[type=top]
fill ~-2 ~-10 ~-18 ~2 ~-10 ~-18 spruce_slab[type=double]
fill ~-2 ~-10 ~-17 ~2 ~-10 ~-17 spruce_slab[type=bottom]
fill ~-2 ~-10 ~-16 ~2 ~-10 ~-15 air


setblock ~-3 ~-09 ~-20 spruce_slab[type=top]
setblock ~3 ~-09 ~-20 spruce_slab[type=top]
fill ~-2 ~-09 ~-20 ~2 ~-09 ~-20 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-19 ~2 ~-09 ~-19 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-09 ~-18 ~2 ~-09 ~-17 air


fill ~-2 ~-08 ~-20 ~2 ~-08 ~-20 spruce_slab[type=bottom]
fill ~-3 ~-08 ~-20 ~-3 ~-08 ~-19 iron_bars
fill ~3 ~-08 ~-20 ~3 ~-08 ~-19 iron_bars
fill ~-2 ~-08 ~-19 ~2 ~-08 ~-18 air


setblock ~-3 ~-07 ~-20 air
setblock ~3 ~-07 ~-20 air
fill ~-2 ~-07 ~-19 ~2 ~-07 ~-19 air



setblock ~-3 ~0 ~6 iron_block
setblock ~-3 ~0 ~7 chain[axis=z]
setblock ~3 ~0 ~6 iron_block
setblock ~3 ~0 ~7 chain[axis=z]

scoreboard players set @s status 16
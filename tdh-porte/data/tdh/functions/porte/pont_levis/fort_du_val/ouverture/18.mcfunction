fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 air


fill ~-2 ~-18 ~-05 ~2 ~-18 ~-05 spruce_slab[type=top]


fill ~-2 ~-17 ~-06 ~2 ~-17 ~-06 spruce_slab[type=top]


fill ~-2 ~-16 ~-08 ~2 ~-16 ~-08 spruce_slab[type=top]
fill ~-2 ~-16 ~-07 ~2 ~-16 ~-07 spruce_slab[type=double]
fill ~-2 ~-16 ~-06 ~2 ~-16 ~-06 spruce_slab[type=bottom]


fill ~-2 ~-15 ~-08 ~2 ~-15 ~-08 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-15 ~-07 ~2 ~-15 ~-07 air


fill ~-2 ~-14 ~-11 ~2 ~-14 ~-11 spruce_slab[type=top]
fill ~-2 ~-14 ~-10 ~2 ~-14 ~-10 spruce_slab[type=double]
fill ~-2 ~-14 ~-09 ~2 ~-14 ~-09 spruce_slab[type=bottom]


fill ~-2 ~-13 ~-12 ~2 ~-13 ~-12 spruce_slab[type=top]
fill ~-2 ~-13 ~-11 ~2 ~-13 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-13 ~-10 ~2 ~-13 ~-10 air


fill ~-2 ~-12 ~-14 ~2 ~-12 ~-14 spruce_slab[type=top]
fill ~-2 ~-12 ~-13 ~2 ~-12 ~-13 spruce_slab[type=double]
fill ~-2 ~-12 ~-12 ~2 ~-12 ~-12 spruce_slab[type=bottom]
fill ~-2 ~-12 ~-11 ~2 ~-12 ~-11 air


fill ~-2 ~-11 ~-15 ~2 ~-11 ~-15 spruce_slab[type=top]
fill ~-2 ~-11 ~-14 ~2 ~-11 ~-14 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-11 ~-13 ~2 ~-11 ~-12 air


fill ~-2 ~-10 ~-17 ~2 ~-10 ~-17 spruce_slab[type=top]
fill ~-2 ~-10 ~-16 ~2 ~-10 ~-16 spruce_slab[type=double]
fill ~-2 ~-10 ~-15 ~2 ~-10 ~-15 spruce_slab[type=bottom]
fill ~-2 ~-10 ~-14 ~2 ~-10 ~-13 air


fill ~-2 ~-09 ~-18 ~2 ~-09 ~-18 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-17 ~2 ~-09 ~-17 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-09 ~-16 ~2 ~-09 ~-15 air


setblock ~-3 ~-08 ~-19 spruce_slab[type=top]
setblock ~3 ~-08 ~-19 spruce_slab[type=top]
fill ~-2 ~-08 ~-19 ~2 ~-08 ~-19 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-18 ~2 ~-08 ~-18 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-08 ~-17 ~2 ~-08 ~-16 air


setblock ~-3 ~-07 ~-20 spruce_stairs[facing=south,half=top]
setblock ~3 ~-07 ~-20 spruce_stairs[facing=south,half=top]
fill ~-3 ~-07 ~-19 ~-3 ~-07 ~-17 iron_bars
fill ~3 ~-07 ~-19 ~3 ~-07 ~-17 iron_bars

fill ~-2 ~-07 ~-19 ~2 ~-07 ~-19 spruce_slab[type=bottom]
fill ~-2 ~-07 ~-18 ~2 ~-07 ~-17 air


setblock ~-3 ~-06 ~-19 air
setblock ~3 ~-06 ~-19 air
setblock ~-3 ~-06 ~-15 iron_bars
setblock ~3 ~-06 ~-15 iron_bars

fill ~-3 ~-06 ~-18 ~3 ~-06 ~-18 air


setblock ~-3 ~-05 ~-16 air
setblock ~3 ~-05 ~-16 air
setblock ~-3 ~-05 ~-13 iron_bars
setblock ~3 ~-05 ~-13 iron_bars


setblock ~-3 ~-04 ~-14 air
setblock ~3 ~-04 ~-14 air
setblock ~-3 ~-04 ~-11 iron_bars
setblock ~3 ~-04 ~-11 iron_bars


setblock ~-3 ~-03 ~-12 air
setblock ~3 ~-03 ~-12 air
setblock ~-3 ~-03 ~-09 iron_bars
setblock ~3 ~-03 ~-09 iron_bars


setblock ~-3 ~-02 ~-10 air
setblock ~3 ~-02 ~-10 air
setblock ~-3 ~-02 ~-07 iron_bars
setblock ~3 ~-02 ~-07 iron_bars


setblock ~-3 ~-01 ~-08 air
setblock ~3 ~-01 ~-08 air
setblock ~-3 ~-01 ~-05 iron_bars
setblock ~3 ~-01 ~-05 iron_bars


setblock ~-3 ~-00 ~-06 air
setblock ~3 ~-00 ~-06 air

scoreboard players set @s status 17
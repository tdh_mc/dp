fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 air


fill ~-2 ~-18 ~-03 ~2 ~-18 ~-03 spruce_stairs[facing=south,half=top]


fill ~-2 ~-17 ~-04 ~2 ~-17 ~-04 air


fill ~-2 ~-14 ~-04 ~2 ~-14 ~-04 spruce_slab[type=bottom]


fill ~-2 ~-12 ~-07 ~2 ~-12 ~-07 spruce_slab[type=top]
fill ~-2 ~-12 ~-05 ~2 ~-12 ~-05 air


fill ~-2 ~-11 ~-07 ~2 ~-11 ~-07 spruce_slab[type=double]
fill ~-2 ~-11 ~-06 ~2 ~-11 ~-06 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-10 ~-08 ~2 ~-10 ~-08 spruce_stairs[facing=south,half=top]
fill ~-2 ~-10 ~-06 ~2 ~-10 ~-06 air


fill ~-2 ~-09 ~-08 ~2 ~-09 ~-08 spruce_slab[type=double]
fill ~-2 ~-09 ~-07 ~2 ~-09 ~-07 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-08 ~-09 ~2 ~-08 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-07 ~2 ~-08 ~-07 air


fill ~-2 ~-07 ~-09 ~2 ~-07 ~-09 spruce_slab[type=double]
fill ~-2 ~-07 ~-08 ~2 ~-07 ~-08 spruce_slab[type=bottom]


fill ~-2 ~-06 ~-10 ~2 ~-06 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-08 ~2 ~-06 ~-08 air


fill ~-2 ~-05 ~-10 ~2 ~-05 ~-10 spruce_slab[type=double]
fill ~-2 ~-05 ~-09 ~2 ~-05 ~-09 air


fill ~-2 ~-04 ~-11 ~2 ~-04 ~-11 spruce_slab[type=double]
fill ~-2 ~-04 ~-10 ~2 ~-04 ~-10 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-04 ~-09 ~2 ~-04 ~-09 air


fill ~-2 ~-03 ~-11 ~2 ~-03 ~-11 spruce_slab[type=double]
fill ~-2 ~-03 ~-10 ~2 ~-03 ~-10 air


fill ~-2 ~-02 ~-12 ~2 ~-02 ~-12 spruce_stairs[facing=south,half=top]
fill ~-2 ~-02 ~-11 ~2 ~-02 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-02 ~-10 ~2 ~-02 ~-10 air


setblock ~-3 ~-01 ~-07 air
setblock ~3 ~-01 ~-07 air

fill ~-2 ~-01 ~-11 ~2 ~-01 ~-11 air
setblock ~-3 ~-01 ~-11 iron_bars
setblock ~3 ~-01 ~-11 iron_bars
fill ~-2 ~-01 ~-12 ~2 ~-01 ~-12 spruce_slab[type=double]
setblock ~-3 ~-01 ~-12 spruce_stairs[facing=south,half=top]
setblock ~3 ~-01 ~-12 spruce_stairs[facing=south,half=top]


setblock ~-3 ~-00 ~-08 iron_bars
setblock ~3 ~-00 ~-08 iron_bars



setblock ~-3 ~0 ~11 iron_block
setblock ~-3 ~0 ~12 chain[axis=z]
setblock ~3 ~0 ~11 iron_block
setblock ~3 ~0 ~12 chain[axis=z]

scoreboard players set @s status 25
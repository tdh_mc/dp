fill ~-2 ~-16 ~-03 ~2 ~-16 ~-03 spruce_slab[type=double]


fill ~-2 ~-15 ~-03 ~2 ~-15 ~-03 spruce_slab[type=double]
fill ~-2 ~-15 ~-02 ~2 ~-15 ~-02 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-14 ~-03 ~2 ~-14 ~-03 spruce_slab[type=double]
fill ~-2 ~-14 ~-02 ~2 ~-14 ~-02 air


fill ~-2 ~-13 ~-02 ~2 ~-13 ~-02 air


fill ~-2 ~-11 ~-04 ~2 ~-11 ~-04 spruce_stairs[facing=south,half=top]


fill ~-2 ~-10 ~-04 ~2 ~-10 ~-04 spruce_slab[type=double]
fill ~-2 ~-10 ~-03 ~2 ~-10 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-09 ~-04 ~2 ~-07 ~-04 spruce_slab[type=double]
fill ~-2 ~-09 ~-03 ~2 ~-07 ~-03 air


fill ~-2 ~-06 ~-05 ~2 ~-06 ~-05 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-03 ~2 ~-06 ~-03 air


fill ~-2 ~-05 ~-05 ~2 ~-05 ~-05 spruce_slab[type=double]
fill ~-2 ~-05 ~-04 ~2 ~-05 ~-04 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-04 ~-05 ~2 ~-01 ~-05 spruce_slab[type=double]
fill ~-2 ~-04 ~-04 ~2 ~-01 ~-04 air


setblock ~-3 ~-00 ~-04 iron_bars
setblock ~3 ~-00 ~-04 iron_bars
fill ~-2 ~-00 ~-04 ~2 ~-00 ~-04 air
setblock ~-3 ~-00 ~-05 spruce_stairs[facing=south,half=top]
setblock ~3 ~-00 ~-05 spruce_stairs[facing=south,half=top]
fill ~-2 ~-00 ~-05 ~2 ~-00 ~-05 spruce_slab[type=double]



setblock ~-3 ~0 ~18 iron_block
setblock ~-3 ~0 ~19 chain[axis=z]
setblock ~3 ~0 ~18 iron_block
setblock ~3 ~0 ~19 chain[axis=z]

scoreboard players set @s status 32
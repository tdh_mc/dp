fill ~-2 ~-14 ~-03 ~2 ~-14 ~-03 spruce_stairs[facing=south,half=top]


fill ~-2 ~-13 ~-03 ~2 ~-13 ~-03 spruce_slab[type=double]
fill ~-2 ~-13 ~-02 ~2 ~-13 ~-02 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-12 ~-03 ~2 ~-10 ~-03 spruce_slab[type=double]
fill ~-2 ~-12 ~-02 ~2 ~-10 ~-02 air


fill ~-2 ~-09 ~-02 ~2 ~-09 ~-02 air


fill ~-2 ~-07 ~-04 ~2 ~-07 ~-04 spruce_stairs[facing=south,half=top]


fill ~-2 ~-06 ~-04 ~2 ~-06 ~-04 spruce_slab[type=double]
fill ~-2 ~-06 ~-03 ~2 ~-06 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-05 ~-04 ~2 ~-01 ~-04 spruce_slab[type=double]
fill ~-2 ~-05 ~-03 ~2 ~-01 ~-03 air


setblock ~-3 ~-00 ~-03 iron_bars
setblock ~3 ~-00 ~-03 iron_bars
fill ~-2 ~-00 ~-03 ~2 ~-00 ~-03 air
setblock ~-3 ~-00 ~-04 spruce_stairs[facing=south,half=top]
setblock ~3 ~-00 ~-04 spruce_stairs[facing=south,half=top]
fill ~-2 ~-00 ~-04 ~2 ~-00 ~-04 spruce_slab[type=double]



setblock ~-3 ~0 ~19 iron_block
setblock ~-3 ~0 ~20 chain[axis=z]
setblock ~3 ~0 ~19 iron_block
setblock ~3 ~0 ~20 chain[axis=z]

scoreboard players set @s status 33
fill ~-2 ~-10 ~-03 ~2 ~-10 ~-03 spruce_stairs[facing=south,half=top]

fill ~-2 ~-09 ~-03 ~2 ~-09 ~-03 spruce_slab[type=double]
fill ~-2 ~-09 ~-02 ~2 ~-09 ~-02 spruce_stairs[facing=north,half=bottom]

fill ~-2 ~-08 ~-03 ~2 ~-01 ~-03 spruce_slab[type=double]
fill ~-2 ~-08 ~-02 ~2 ~-01 ~-02 air


setblock ~-3 ~-00 ~-02 iron_bars
setblock ~3 ~-00 ~-02 iron_bars
fill ~-2 ~-00 ~-02 ~2 ~-00 ~-02 air
setblock ~-3 ~-00 ~-03 spruce_stairs[facing=south,half=top]
setblock ~3 ~-00 ~-03 spruce_stairs[facing=south,half=top]
fill ~-2 ~-00 ~-03 ~2 ~-00 ~-03 spruce_slab[type=double]



setblock ~-3 ~0 ~20 iron_block
setblock ~3 ~0 ~20 iron_block

tag @s remove Ferme
scoreboard players set @s status 34
fill ~-3 ~-24 ~-22 ~3 ~-24 ~-22 spruce_slab[type=top]
fill ~-2 ~-24 ~-21 ~2 ~-24 ~-21 spruce_slab[type=top]


setblock ~-3 ~-23 ~-22 iron_bars
setblock ~3 ~-23 ~-22 iron_bars
fill ~-2 ~-23 ~-22 ~2 ~-23 ~-22 spruce_slab[type=bottom]
fill ~-2 ~-23 ~-21 ~2 ~-23 ~-21 spruce_stairs[facing=south,half=bottom]
fill ~-2 ~-23 ~-20 ~2 ~-23 ~-19 spruce_slab[type=double]
fill ~-2 ~-23 ~-18 ~2 ~-23 ~-16 spruce_slab[type=top]


fill ~-2 ~-22 ~-22 ~2 ~-22 ~-19 air
fill ~-2 ~-22 ~-18 ~2 ~-22 ~-17 spruce_slab[type=bottom]
fill ~-2 ~-22 ~-16 ~2 ~-22 ~-16 spruce_stairs[facing=south,half=bottom]
fill ~-2 ~-22 ~-15 ~2 ~-22 ~-14 spruce_slab[type=double]
fill ~-2 ~-22 ~-13 ~2 ~-22 ~-11 spruce_slab[type=top]


setblock ~-3 ~-21 ~-21 iron_bars
setblock ~3 ~-21 ~-21 iron_bars

fill ~-2 ~-21 ~-16 ~2 ~-21 ~-14 air
fill ~-2 ~-21 ~-13 ~2 ~-21 ~-12 spruce_slab[type=bottom]
fill ~-2 ~-21 ~-11 ~2 ~-21 ~-11 spruce_stairs[facing=south,half=bottom]
fill ~-2 ~-21 ~-10 ~2 ~-21 ~-08 spruce_slab[type=double]
fill ~-2 ~-21 ~-07 ~2 ~-21 ~-06 spruce_slab[type=top]


setblock ~-3 ~-20 ~-22 air
setblock ~3 ~-20 ~-22 air
setblock ~-3 ~-20 ~-20 iron_bars
setblock ~3 ~-20 ~-20 iron_bars

fill ~-2 ~-20 ~-10 ~2 ~-20 ~-09 air
fill ~-2 ~-20 ~-07 ~2 ~-20 ~-07 spruce_slab[type=bottom]
fill ~-2 ~-20 ~-06 ~2 ~-20 ~-06 spruce_stairs[facing=south,half=bottom]
fill ~-2 ~-20 ~-04 ~2 ~-20 ~-04 spruce_slab[type=double]


setblock ~-3 ~-19 ~-21 air
setblock ~3 ~-19 ~-21 air

fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 air


setblock ~-3 ~-18 ~-19 iron_bars
setblock ~3 ~-18 ~-19 iron_bars


setblock ~-3 ~-17 ~-20 air
setblock ~3 ~-17 ~-20 air
setblock ~-3 ~-17 ~-18 iron_bars
setblock ~3 ~-17 ~-18 iron_bars


setblock ~-3 ~-16 ~-19 air
setblock ~3 ~-16 ~-19 air
setblock ~-3 ~-16 ~-17 iron_bars
setblock ~3 ~-16 ~-17 iron_bars


setblock ~-3 ~-15 ~-18 air
setblock ~3 ~-15 ~-18 air
setblock ~-3 ~-15 ~-16 iron_bars
setblock ~3 ~-15 ~-16 iron_bars


setblock ~-3 ~-14 ~-17 air
setblock ~3 ~-14 ~-17 air


setblock ~-3 ~-13 ~-15 iron_bars
setblock ~3 ~-13 ~-15 iron_bars


setblock ~-3 ~-12 ~-16 air
setblock ~3 ~-12 ~-16 air
setblock ~-3 ~-12 ~-14 iron_bars
setblock ~3 ~-12 ~-14 iron_bars


setblock ~-3 ~-11 ~-15 air
setblock ~3 ~-11 ~-15 air
setblock ~-3 ~-11 ~-13 iron_bars
setblock ~3 ~-11 ~-13 iron_bars


setblock ~-3 ~-10 ~-14 air
setblock ~3 ~-10 ~-14 air
setblock ~-3 ~-10 ~-12 iron_bars
setblock ~3 ~-10 ~-12 iron_bars


setblock ~-3 ~-09 ~-13 air
setblock ~3 ~-09 ~-13 air
setblock ~-3 ~-09 ~-11 iron_bars
setblock ~3 ~-09 ~-11 iron_bars


setblock ~-3 ~-08 ~-12 air
setblock ~3 ~-08 ~-12 air
setblock ~-3 ~-08 ~-10 iron_bars
setblock ~3 ~-08 ~-10 iron_bars


setblock ~-3 ~-07 ~-11 air
setblock ~3 ~-07 ~-11 air
setblock ~-3 ~-07 ~-09 iron_bars
setblock ~3 ~-07 ~-09 iron_bars


setblock ~-3 ~-06 ~-10 air
setblock ~3 ~-06 ~-10 air
setblock ~-3 ~-06 ~-08 iron_bars
setblock ~3 ~-06 ~-08 iron_bars


setblock ~-3 ~-05 ~-09 air
setblock ~3 ~-05 ~-09 air
setblock ~-3 ~-05 ~-07 iron_bars
setblock ~3 ~-05 ~-07 iron_bars


setblock ~-3 ~-04 ~-08 air
setblock ~3 ~-04 ~-08 air

scoreboard players set @s status 1
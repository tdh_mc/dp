fill ~-2 ~-18 ~-05 ~2 ~-18 ~-05 spruce_slab[type=top]
fill ~-2 ~-18 ~-04 ~2 ~-18 ~-04 spruce_slab[type=double]
fill ~-2 ~-18 ~-03 ~2 ~-18 ~-03 spruce_slab[type=bottom]


fill ~-2 ~-17 ~-06 ~2 ~-17 ~-06 spruce_stairs[facing=south,half=top]
fill ~-2 ~-17 ~-05 ~2 ~-17 ~-05 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-17 ~-04 ~2 ~-17 ~-04 air


fill ~-2 ~-16 ~-07 ~2 ~-16 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-16 ~-06 ~2 ~-16 ~-06 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-16 ~-05 ~2 ~-16 ~-05 air


fill ~-2 ~-15 ~-08 ~2 ~-15 ~-08 spruce_stairs[facing=south,half=top]
fill ~-2 ~-15 ~-07 ~2 ~-15 ~-07 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-15 ~-06 ~2 ~-15 ~-06 air


fill ~-2 ~-14 ~-09 ~2 ~-14 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-14 ~-08 ~2 ~-14 ~-08 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-14 ~-07 ~2 ~-14 ~-07 air


fill ~-2 ~-13 ~-10 ~2 ~-13 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-13 ~-09 ~2 ~-13 ~-09 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-13 ~-08 ~2 ~-13 ~-08 air


fill ~-2 ~-12 ~-11 ~2 ~-12 ~-11 spruce_stairs[facing=south,half=top]
fill ~-2 ~-12 ~-10 ~2 ~-12 ~-10 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-12 ~-09 ~2 ~-12 ~-09 air


fill ~-2 ~-11 ~-12 ~2 ~-11 ~-12 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-11 ~2 ~-11 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-11 ~-10 ~2 ~-11 ~-10 air


fill ~-2 ~-10 ~-13 ~2 ~-10 ~-13 spruce_stairs[facing=south,half=top]
fill ~-2 ~-10 ~-12 ~2 ~-10 ~-12 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-10 ~-11 ~2 ~-10 ~-11 air


fill ~-2 ~-09 ~-14 ~2 ~-09 ~-14 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-13 ~2 ~-09 ~-13 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-09 ~-12 ~2 ~-09 ~-12 air


fill ~-2 ~-08 ~-15 ~2 ~-08 ~-15 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-14 ~2 ~-08 ~-14 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-08 ~-13 ~2 ~-08 ~-13 air


fill ~-2 ~-07 ~-16 ~2 ~-07 ~-16 spruce_stairs[facing=south,half=top]
fill ~-2 ~-07 ~-15 ~2 ~-07 ~-15 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-07 ~-14 ~2 ~-07 ~-14 air


setblock ~-3 ~-06 ~-17 spruce_slab[type=top]
setblock ~3 ~-06 ~-17 spruce_slab[type=top]
fill ~-2 ~-06 ~-17 ~2 ~-06 ~-17 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-16 ~2 ~-06 ~-16 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-06 ~-15 ~2 ~-06 ~-14 air


setblock ~-3 ~-05 ~-18 spruce_stairs[facing=south,half=top]
setblock ~3 ~-05 ~-18 spruce_stairs[facing=south,half=top]
fill ~-3 ~-05 ~-17 ~-3 ~-05 ~-15 iron_bars
fill ~3 ~-05 ~-17 ~3 ~-05 ~-15 iron_bars

fill ~-2 ~-05 ~-17 ~2 ~-05 ~-17 spruce_slab[type=bottom]
fill ~-2 ~-05 ~-16 ~2 ~-05 ~-15 air


fill ~-3 ~-04 ~-17 ~-3 ~-04 ~-16 air
fill ~3 ~-04 ~-17 ~3 ~-04 ~-16 air
setblock ~-3 ~-04 ~-13 iron_bars
setblock ~3 ~-04 ~-13 iron_bars

fill ~-2 ~-04 ~-16 ~2 ~-04 ~-16 air


setblock ~-3 ~-03 ~-14 air
setblock ~3 ~-03 ~-14 air
setblock ~-3 ~-03 ~-11 iron_bars
setblock ~3 ~-03 ~-11 iron_bars


setblock ~-3 ~-02 ~-12 air
setblock ~3 ~-02 ~-12 air

scoreboard players set @s status 19
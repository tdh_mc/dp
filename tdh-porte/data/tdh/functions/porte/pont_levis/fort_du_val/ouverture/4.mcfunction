fill ~-2 ~-17 ~-03 ~2 ~-17 ~-03 spruce_stairs[facing=south,half=top]


fill ~-2 ~-16 ~-03 ~2 ~-16 ~-03 spruce_slab[type=double]
fill ~-2 ~-16 ~-02 ~2 ~-16 ~-02 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-15 ~-02 ~2 ~-15 ~-02 air


fill ~-2 ~-13 ~-04 ~2 ~-13 ~-04 spruce_stairs[facing=south,half=top]


fill ~-2 ~-12 ~-04 ~2 ~-12 ~-04 spruce_slab[type=double]
fill ~-2 ~-12 ~-03 ~2 ~-12 ~-03 spruce_slab[type=bottom]


fill ~-2 ~-11 ~-04 ~2 ~-11 ~-04 spruce_slab[type=double]
fill ~-2 ~-11 ~-03 ~2 ~-11 ~-03 air


fill ~-2 ~-10 ~-03 ~2 ~-10 ~-03 air


fill ~-2 ~-09 ~-05 ~2 ~-09 ~-05 spruce_stairs[facing=south,half=top]


fill ~-2 ~-08 ~-05 ~2 ~-08 ~-05 spruce_slab[type=double]
fill ~-2 ~-08 ~-04 ~2 ~-08 ~-04 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-07 ~-05 ~2 ~-06 ~-05 spruce_slab[type=double]
fill ~-2 ~-07 ~-04 ~2 ~-06 ~-04 air


fill ~-2 ~-05 ~-06 ~2 ~-05 ~-06 spruce_stairs[facing=south,half=top]
fill ~-2 ~-05 ~-04 ~2 ~-05 ~-04 air


fill ~-2 ~-04 ~-06 ~2 ~-04 ~-06 spruce_slab[type=double]
fill ~-2 ~-04 ~-05 ~2 ~-04 ~-05 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-03 ~-06 ~2 ~-01 ~-06 spruce_slab[type=double]
fill ~-2 ~-03 ~-05 ~2 ~-01 ~-05 air


setblock ~-3 ~-00 ~-05 iron_bars
setblock ~3 ~-00 ~-05 iron_bars
fill ~-2 ~-00 ~-05 ~2 ~-00 ~-05 air
setblock ~-3 ~-00 ~-06 spruce_stairs[facing=south,half=top]
setblock ~3 ~-00 ~-06 spruce_stairs[facing=south,half=top]
fill ~-2 ~-00 ~-06 ~2 ~-00 ~-06 spruce_slab[type=double]



setblock ~-3 ~0 ~17 iron_block
setblock ~-3 ~0 ~18 chain[axis=z]
setblock ~3 ~0 ~17 iron_block
setblock ~3 ~0 ~18 chain[axis=z]

scoreboard players set @s status 31
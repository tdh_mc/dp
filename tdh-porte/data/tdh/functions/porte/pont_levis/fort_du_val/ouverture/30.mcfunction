fill ~-3 ~-20 ~-23 ~3 ~-20 ~-23 spruce_slab[type=top]
fill ~-2 ~-20 ~-22 ~2 ~-20 ~-09 spruce_slab[type=top]


fill ~-3 ~-19 ~-23 ~-3 ~-19 ~-22 iron_bars
fill ~3 ~-19 ~-23 ~3 ~-19 ~-22 iron_bars

fill ~-2 ~-19 ~-23 ~2 ~-19 ~-09 spruce_slab[type=bottom]


setblock ~-3 ~-18 ~-23 air
setblock ~3 ~-18 ~-23 air
setblock ~-3 ~-18 ~-21 iron_bars
setblock ~3 ~-18 ~-21 iron_bars

fill ~-2 ~-18 ~-23 ~2 ~-18 ~-17 air


setblock ~-3 ~-17 ~-22 air
setblock ~3 ~-17 ~-22 air
setblock ~-3 ~-17 ~-20 iron_bars
setblock ~3 ~-17 ~-20 iron_bars


setblock ~-3 ~-16 ~-21 air
setblock ~3 ~-16 ~-21 air
setblock ~-3 ~-16 ~-19 iron_bars
setblock ~3 ~-16 ~-19 iron_bars


setblock ~-3 ~-15 ~-20 air
setblock ~3 ~-15 ~-20 air
setblock ~-3 ~-15 ~-18 iron_bars
setblock ~3 ~-15 ~-18 iron_bars


setblock ~-3 ~-14 ~-19 air
setblock ~3 ~-14 ~-19 air
setblock ~-3 ~-14 ~-17 iron_bars
setblock ~3 ~-14 ~-17 iron_bars


setblock ~-3 ~-13 ~-18 air
setblock ~3 ~-13 ~-18 air
setblock ~-3 ~-13 ~-16 iron_bars
setblock ~3 ~-13 ~-16 iron_bars


setblock ~-3 ~-12 ~-17 air
setblock ~3 ~-12 ~-17 air
setblock ~-3 ~-12 ~-15 iron_bars
setblock ~3 ~-12 ~-15 iron_bars


setblock ~-3 ~-11 ~-16 air
setblock ~3 ~-11 ~-16 air
setblock ~-3 ~-11 ~-14 iron_bars
setblock ~3 ~-11 ~-14 iron_bars


setblock ~-3 ~-10 ~-15 air
setblock ~3 ~-10 ~-15 air
setblock ~-3 ~-10 ~-13 iron_bars
setblock ~3 ~-10 ~-13 iron_bars


setblock ~-3 ~-09 ~-14 air
setblock ~3 ~-09 ~-14 air
setblock ~-3 ~-09 ~-12 iron_bars
setblock ~3 ~-09 ~-12 iron_bars


setblock ~-3 ~-08 ~-13 air
setblock ~3 ~-08 ~-13 air

scoreboard players set @s status 5
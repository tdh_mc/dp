fill ~-2 ~-16 ~-11 ~2 ~-16 ~-10 spruce_slab[type=top]
fill ~-2 ~-16 ~-09 ~2 ~-16 ~-09 spruce_slab[type=double]
fill ~-2 ~-16 ~-08 ~2 ~-16 ~-08 spruce_slab[type=bottom]


fill ~-2 ~-15 ~-13 ~2 ~-15 ~-13 spruce_slab[type=top]
fill ~-2 ~-15 ~-12 ~2 ~-15 ~-12 spruce_slab[type=double]
fill ~-2 ~-15 ~-11 ~2 ~-15 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-15 ~-10 ~2 ~-15 ~-10 spruce_slab[type=bottom]
fill ~-2 ~-15 ~-09 ~2 ~-15 ~-09 air


fill ~-2 ~-14 ~-15 ~2 ~-14 ~-15 spruce_slab[type=top]
fill ~-2 ~-14 ~-14 ~2 ~-14 ~-14 spruce_slab[type=double]
fill ~-2 ~-14 ~-13 ~2 ~-14 ~-13 spruce_slab[type=bottom]
fill ~-2 ~-14 ~-12 ~2 ~-14 ~-11 air


fill ~-2 ~-13 ~-17 ~2 ~-13 ~-17 spruce_slab[type=top]
fill ~-2 ~-13 ~-16 ~2 ~-13 ~-16 spruce_slab[type=double]
fill ~-2 ~-13 ~-15 ~2 ~-13 ~-15 spruce_slab[type=bottom]
fill ~-2 ~-13 ~-14 ~2 ~-13 ~-13 air


fill ~-2 ~-12 ~-19 ~2 ~-12 ~-19 spruce_slab[type=top]
fill ~-2 ~-12 ~-18 ~2 ~-12 ~-18 spruce_slab[type=double]
fill ~-2 ~-12 ~-17 ~2 ~-12 ~-17 spruce_slab[type=bottom]
fill ~-2 ~-12 ~-16 ~2 ~-12 ~-15 air


fill ~-3 ~-11 ~-21 ~3 ~-11 ~-21 spruce_slab[type=top]
fill ~-2 ~-11 ~-20 ~2 ~-11 ~-20 spruce_slab[type=double]
fill ~-2 ~-11 ~-19 ~2 ~-11 ~-19 spruce_slab[type=bottom]
fill ~-2 ~-11 ~-18 ~2 ~-11 ~-17 air


fill ~-3 ~-10 ~-21 ~-3 ~-10 ~-20 iron_bars
fill ~3 ~-10 ~-21 ~3 ~-10 ~-20 iron_bars
fill ~-2 ~-10 ~-21 ~2 ~-10 ~-21 spruce_slab[type=bottom]
fill ~-2 ~-10 ~-20 ~2 ~-10 ~-18 air


setblock ~-3 ~-09 ~-18 iron_bars
setblock ~3 ~-09 ~-18 iron_bars

fill ~-2 ~-09 ~-20 ~2 ~-09 ~-20 air


setblock ~-3 ~-08 ~-19 air
setblock ~3 ~-08 ~-19 air


setblock ~-3 ~-07 ~-15 iron_bars
setblock ~3 ~-07 ~-15 iron_bars


setblock ~-3 ~-06 ~-16 air
setblock ~3 ~-06 ~-16 air


setblock ~-3 ~-05 ~-12 iron_bars
setblock ~3 ~-05 ~-12 iron_bars


setblock ~-3 ~-04 ~-13 air
setblock ~3 ~-04 ~-13 air



setblock ~-3 ~0 ~5 iron_block
setblock ~-3 ~0 ~6 chain[axis=z]
setblock ~3 ~0 ~5 iron_block
setblock ~3 ~0 ~6 chain[axis=z]

scoreboard players set @s status 14
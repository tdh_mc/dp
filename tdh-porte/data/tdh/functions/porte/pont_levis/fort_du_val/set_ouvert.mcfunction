# Cette fonction est exécutée par une armor stand "Porte" "PontLevisFDV" à sa position,
# pour définir instantanément l'état du pont-levis à "ouvert"

# On vide d'abord les blocs éventuellement présents venant de n'importe quelle frame
function tdh:porte/fort_du_val/pont_levis/supprimer


# On pose ensuite les blocs
# Le câble à l'ouest, de bas en haut
fill ~-3 ~-24 ~-22 ~-3 ~-22 ~-22 iron_bars
fill ~-3 ~-22 ~-21 ~-3 ~-21 ~-21 iron_bars
fill ~-3 ~-21 ~-20 ~-3 ~-19 ~-20 iron_bars
fill ~-3 ~-19 ~-19 ~-3 ~-18 ~-19 iron_bars
fill ~-3 ~-18 ~-18 ~-3 ~-17 ~-18 iron_bars
fill ~-3 ~-17 ~-17 ~-3 ~-16 ~-17 iron_bars
fill ~-3 ~-16 ~-16 ~-3 ~-14 ~-16 iron_bars
fill ~-3 ~-14 ~-15 ~-3 ~-13 ~-15 iron_bars
fill ~-3 ~-13 ~-14 ~-3 ~-12 ~-14 iron_bars
fill ~-3 ~-12 ~-13 ~-3 ~-11 ~-13 iron_bars
fill ~-3 ~-11 ~-12 ~-3 ~-09 ~-12 iron_bars
fill ~-3 ~-09 ~-11 ~-3 ~-08 ~-11 iron_bars
fill ~-3 ~-08 ~-10 ~-3 ~-07 ~-10 iron_bars
fill ~-3 ~-07 ~-09 ~-3 ~-06 ~-09 iron_bars
fill ~-3 ~-06 ~-08 ~-3 ~-05 ~-08 iron_bars
fill ~-3 ~-05 ~-07 ~-3 ~-03 ~-07 iron_bars
fill ~-3 ~-03 ~-06 ~-3 ~-02 ~-06 iron_bars
fill ~-3 ~-02 ~-05 ~-3 ~-01 ~-05 iron_bars
fill ~-3 ~-01 ~-04 ~-3 ~-00 ~-04 iron_bars
setblock ~-3 ~-00 ~-03 iron_bars
setblock ~-3 ~-00 ~-02 iron_block

# Le câble à l'est, de bas en haut
fill ~3 ~-24 ~-22 ~3 ~-22 ~-22 iron_bars
fill ~3 ~-22 ~-21 ~3 ~-21 ~-21 iron_bars
fill ~3 ~-21 ~-20 ~3 ~-19 ~-20 iron_bars
fill ~3 ~-19 ~-19 ~3 ~-18 ~-19 iron_bars
fill ~3 ~-18 ~-18 ~3 ~-17 ~-18 iron_bars
fill ~3 ~-17 ~-17 ~3 ~-16 ~-17 iron_bars
fill ~3 ~-16 ~-16 ~3 ~-14 ~-16 iron_bars
fill ~3 ~-14 ~-15 ~3 ~-13 ~-15 iron_bars
fill ~3 ~-13 ~-14 ~3 ~-12 ~-14 iron_bars
fill ~3 ~-12 ~-13 ~3 ~-11 ~-13 iron_bars
fill ~3 ~-11 ~-12 ~3 ~-09 ~-12 iron_bars
fill ~3 ~-09 ~-11 ~3 ~-08 ~-11 iron_bars
fill ~3 ~-08 ~-10 ~3 ~-07 ~-10 iron_bars
fill ~3 ~-07 ~-09 ~3 ~-06 ~-09 iron_bars
fill ~3 ~-06 ~-08 ~3 ~-05 ~-08 iron_bars
fill ~3 ~-05 ~-07 ~3 ~-03 ~-07 iron_bars
fill ~3 ~-03 ~-06 ~3 ~-02 ~-06 iron_bars
fill ~3 ~-02 ~-05 ~3 ~-01 ~-05 iron_bars
fill ~3 ~-01 ~-04 ~3 ~-00 ~-04 iron_bars
setblock ~3 ~-00 ~-03 iron_bars
setblock ~3 ~-00 ~-02 iron_block

# On pose le pont-levis de l'extérieur vers l'intérieur
# On commence par la barre transversale collée à la route
fill ~-3 ~-25 ~-22 ~3 ~-25 ~-22 spruce_slab[type=top]

# On pose ensuite le pont couche par couche, de l'extérieur vers l'intérieur
fill ~-2 ~-24 ~-22 ~2 ~-24 ~-21 spruce_slab[type=bottom]
fill ~-2 ~-24 ~-20 ~2 ~-24 ~-19 spruce_slab[type=double]
fill ~-2 ~-24 ~-18 ~2 ~-24 ~-16 spruce_slab[type=top]

fill ~-2 ~-23 ~-18 ~2 ~-23 ~-17 spruce_slab[type=bottom]
fill ~-2 ~-23 ~-16 ~2 ~-23 ~-15 spruce_slab[type=double]
fill ~-2 ~-23 ~-14 ~2 ~-23 ~-13 spruce_slab[type=top]

fill ~-2 ~-22 ~-14 ~2 ~-22 ~-13 spruce_slab[type=bottom]
fill ~-2 ~-22 ~-12 ~2 ~-22 ~-11 spruce_slab[type=double]
fill ~-2 ~-22 ~-10 ~2 ~-22 ~-09 spruce_slab[type=top]

fill ~-2 ~-21 ~-10 ~2 ~-21 ~-09 spruce_slab[type=bottom]
fill ~-2 ~-21 ~-08 ~2 ~-21 ~-07 spruce_slab[type=double]
fill ~-2 ~-21 ~-06 ~2 ~-21 ~-05 spruce_slab[type=top]

fill ~-2 ~-20 ~-06 ~2 ~-20 ~-05 spruce_slab[type=bottom]
fill ~-2 ~-20 ~-04 ~2 ~-20 ~-03 spruce_slab[type=double]
fill ~-2 ~-20 ~-02 ~2 ~-20 ~-02 spruce_slab[type=top]

fill ~-2 ~-19 ~-02 ~2 ~-19 ~-02 spruce_slab[type=bottom]


# On pose enfin les câbles à l'état "ouvert" dans la salle des machines
fill ~-3 ~0 ~-1 ~-3 ~0 ~20 chain[axis=z]
fill ~3 ~0 ~-1 ~3 ~0 ~20 chain[axis=z]
# Si on est en cours d'ouverture
execute as @s[tag=Ouverture] run function tdh:porte/pont_levis/fort_du_val/tick_ouverture

# Si on est en cours de fermeture
execute as @s[tag=Fermeture] run function tdh:porte/pont_levis/fort_du_val/tick_fermeture
# On déplace les joueurs qui se trouvent sur le pont-levis
execute positioned ~-3 ~-24 ~-23 run tag @a[gamemode=!spectator,dx=7,dy=24,dz=21] add MouvementAvecPontLevis
execute at @a[tag=MouvementAvecPontLevis] unless block ~ ~ ~ #tdh:pont_levis unless block ~ ~-1 ~ #tdh:pont_levis run tag @p[tag=MouvementAvecPontLevis] remove MouvementAvecPontLevis
execute if score @s status matches 15..32 at @a[tag=MouvementAvecPontLevis] facing entity @s feet rotated ~ 0 run tp @p[tag=MouvementAvecPontLevis] ^ ^.5 ^1
execute unless score @s status matches 15.. at @a[tag=MouvementAvecPontLevis] facing entity @s feet rotated ~ 0 run tp @p[tag=MouvementAvecPontLevis] ^ ^1 ^.25
execute unless score @s status matches ..32 at @a[tag=MouvementAvecPontLevis] facing entity @s feet rotated ~ 0 run tp @p[tag=MouvementAvecPontLevis] ^ ^ ^1.2
tag @a[tag=MouvementAvecPontLevis] remove MouvementAvecPontLevis

# Selon l'étape actuelle
execute if score @s status matches 34 run function tdh:porte/pont_levis/fort_du_val/fermeture/35
execute if score @s status matches 33 run function tdh:porte/pont_levis/fort_du_val/fermeture/34
execute if score @s status matches 32 run function tdh:porte/pont_levis/fort_du_val/fermeture/33
execute if score @s status matches 31 run function tdh:porte/pont_levis/fort_du_val/fermeture/32
execute if score @s status matches 30 run function tdh:porte/pont_levis/fort_du_val/fermeture/31
execute if score @s status matches 29 run function tdh:porte/pont_levis/fort_du_val/fermeture/30
execute if score @s status matches 28 run function tdh:porte/pont_levis/fort_du_val/fermeture/29
execute if score @s status matches 27 run function tdh:porte/pont_levis/fort_du_val/fermeture/28
execute if score @s status matches 26 run function tdh:porte/pont_levis/fort_du_val/fermeture/27
execute if score @s status matches 25 run function tdh:porte/pont_levis/fort_du_val/fermeture/26
execute if score @s status matches 24 run function tdh:porte/pont_levis/fort_du_val/fermeture/25
execute if score @s status matches 23 run function tdh:porte/pont_levis/fort_du_val/fermeture/24
execute if score @s status matches 22 run function tdh:porte/pont_levis/fort_du_val/fermeture/23
execute if score @s status matches 21 run function tdh:porte/pont_levis/fort_du_val/fermeture/22
execute if score @s status matches 20 run function tdh:porte/pont_levis/fort_du_val/fermeture/21
execute if score @s status matches 19 run function tdh:porte/pont_levis/fort_du_val/fermeture/20
execute if score @s status matches 18 run function tdh:porte/pont_levis/fort_du_val/fermeture/19
execute if score @s status matches 17 run function tdh:porte/pont_levis/fort_du_val/fermeture/18
execute if score @s status matches 16 run function tdh:porte/pont_levis/fort_du_val/fermeture/17
execute if score @s status matches 15 run function tdh:porte/pont_levis/fort_du_val/fermeture/16
execute if score @s status matches 14 run function tdh:porte/pont_levis/fort_du_val/fermeture/15
execute if score @s status matches 13 run function tdh:porte/pont_levis/fort_du_val/fermeture/14
execute if score @s status matches 12 run function tdh:porte/pont_levis/fort_du_val/fermeture/13
execute if score @s status matches 11 run function tdh:porte/pont_levis/fort_du_val/fermeture/12
execute if score @s status matches 10 run function tdh:porte/pont_levis/fort_du_val/fermeture/11
execute if score @s status matches 09 run function tdh:porte/pont_levis/fort_du_val/fermeture/10
execute if score @s status matches 08 run function tdh:porte/pont_levis/fort_du_val/fermeture/9
execute if score @s status matches 07 run function tdh:porte/pont_levis/fort_du_val/fermeture/8
execute if score @s status matches 06 run function tdh:porte/pont_levis/fort_du_val/fermeture/7
execute if score @s status matches 05 run function tdh:porte/pont_levis/fort_du_val/fermeture/6
execute if score @s status matches 04 run function tdh:porte/pont_levis/fort_du_val/fermeture/5
execute if score @s status matches 03 run function tdh:porte/pont_levis/fort_du_val/fermeture/4
execute if score @s status matches 02 run function tdh:porte/pont_levis/fort_du_val/fermeture/3
execute if score @s status matches 01 run function tdh:porte/pont_levis/fort_du_val/fermeture/2
execute unless score @s status matches 01.. run function tdh:porte/pont_levis/fort_du_val/fermeture/1

# On diffuse régulièrement des bruitages de machinerie
execute if score @s status matches 01 run function tdh:sound/pont_levis/mecanisme
execute if score @s status matches 19 run function tdh:sound/pont_levis/mecanisme
execute if score @s status matches 35 run function tdh:sound/pont_levis/mecanisme_stop
fill ~-2 ~-20 ~-06 ~2 ~-20 ~-06 air


fill ~-2 ~-19 ~-15 ~2 ~-19 ~-12 air
fill ~-2 ~-19 ~-10 ~2 ~-19 ~-08 spruce_slab[type=top]
fill ~-2 ~-19 ~-06 ~2 ~-19 ~-05 spruce_slab[type=double]
fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-18 ~-23 ~3 ~-18 ~-23 air
fill ~-2 ~-18 ~-22 ~2 ~-18 ~-19 air
fill ~-2 ~-18 ~-18 ~2 ~-18 ~-15 spruce_slab[type=top]
fill ~-2 ~-18 ~-14 ~2 ~-18 ~-12 spruce_slab[type=double]
fill ~-2 ~-18 ~-11 ~2 ~-18 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-18 ~-10 ~2 ~-18 ~-08 spruce_slab[type=bottom]


setblock ~-3 ~-17 ~-22 air
setblock ~3 ~-17 ~-22 air

fill ~-3 ~-17 ~-23 ~3 ~-17 ~-23 spruce_slab[type=top]
fill ~-2 ~-17 ~-22 ~2 ~-17 ~-22 spruce_slab[type=top]
fill ~-2 ~-17 ~-21 ~2 ~-17 ~-19 spruce_slab[type=double]
fill ~-2 ~-17 ~-18 ~2 ~-17 ~-18 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-17 ~-17 ~2 ~-17 ~-15 spruce_slab[type=bottom]


setblock ~-3 ~-16 ~-23 iron_bars
setblock ~3 ~-16 ~-23 iron_bars
setblock ~-3 ~-16 ~-21 air
setblock ~3 ~-16 ~-21 air

fill ~-2 ~-16 ~-23 ~2 ~-16 ~-22 spruce_slab[type=bottom]


setblock ~-3 ~-15 ~-22 iron_bars
setblock ~3 ~-15 ~-22 iron_bars
setblock ~-3 ~-15 ~-20 air
setblock ~3 ~-15 ~-20 air


setblock ~-3 ~-14 ~-21 iron_bars
setblock ~3 ~-14 ~-21 iron_bars
setblock ~-3 ~-14 ~-19 air
setblock ~3 ~-14 ~-19 air


setblock ~-3 ~-13 ~-20 iron_bars
setblock ~3 ~-13 ~-20 iron_bars
setblock ~-3 ~-13 ~-18 air
setblock ~3 ~-13 ~-18 air


setblock ~-3 ~-12 ~-19 iron_bars
setblock ~3 ~-12 ~-19 iron_bars
setblock ~-3 ~-12 ~-17 air
setblock ~3 ~-12 ~-17 air


setblock ~-3 ~-11 ~-18 iron_bars
setblock ~3 ~-11 ~-18 iron_bars
fill ~-3 ~-11 ~-16 ~-3 ~-11 ~-15 air
fill ~3 ~-11 ~-16 ~3 ~-11 ~-15 air


fill ~-3 ~-10 ~-17 ~-3 ~-10 ~-16 iron_bars
fill ~3 ~-10 ~-17 ~3 ~-10 ~-16 iron_bars
setblock ~-3 ~-10 ~-14 air
setblock ~3 ~-10 ~-14 air


setblock ~-3 ~-09 ~-15 iron_bars
setblock ~3 ~-09 ~-15 iron_bars
setblock ~-3 ~-09 ~-13 air
setblock ~3 ~-09 ~-13 air


setblock ~-3 ~-08 ~-14 iron_bars
setblock ~3 ~-08 ~-14 iron_bars
setblock ~-3 ~-08 ~-12 air
setblock ~3 ~-08 ~-12 air


setblock ~-3 ~-07 ~-13 iron_bars
setblock ~3 ~-07 ~-13 iron_bars


setblock ~-3 ~-05 ~-08 air
setblock ~3 ~-05 ~-08 air


setblock ~-3 ~-04 ~-09 iron_bars
setblock ~3 ~-04 ~-09 iron_bars
setblock ~-3 ~-04 ~-07 air
setblock ~3 ~-04 ~-07 air


setblock ~-3 ~-03 ~-08 iron_bars
setblock ~3 ~-03 ~-08 iron_bars
setblock ~-3 ~-03 ~-06 air
setblock ~3 ~-03 ~-06 air


setblock ~-3 ~-02 ~-07 iron_bars
setblock ~3 ~-02 ~-07 iron_bars
setblock ~-3 ~-02 ~-05 air
setblock ~3 ~-02 ~-05 air


setblock ~-3 ~-01 ~-06 iron_bars
setblock ~3 ~-01 ~-06 iron_bars

scoreboard players set @s status 8
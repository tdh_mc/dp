fill ~-2 ~-20 ~-03 ~2 ~-20 ~-03 air


fill ~-2 ~-19 ~-07 ~2 ~-19 ~-06 air
fill ~-2 ~-19 ~-05 ~2 ~-19 ~-04 spruce_slab[type=top]
fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 spruce_slab[type=double]
fill ~-2 ~-19 ~-02 ~2 ~-19 ~-02 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-18 ~-10 ~2 ~-18 ~-09 air
fill ~-2 ~-18 ~-08 ~2 ~-18 ~-07 spruce_slab[type=top]
fill ~-2 ~-18 ~-06 ~2 ~-18 ~-06 spruce_slab[type=double]
fill ~-2 ~-18 ~-05 ~2 ~-18 ~-05 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-18 ~-04 ~2 ~-18 ~-04 spruce_slab[type=bottom]


fill ~-2 ~-17 ~-13 ~2 ~-17 ~-12 air
fill ~-2 ~-17 ~-11 ~2 ~-17 ~-10 spruce_slab[type=top]
fill ~-2 ~-17 ~-09 ~2 ~-17 ~-09 spruce_slab[type=double]
fill ~-2 ~-17 ~-08 ~2 ~-17 ~-08 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-17 ~-07 ~2 ~-17 ~-07 spruce_slab[type=bottom]


fill ~-2 ~-16 ~-16 ~2 ~-16 ~-15 air
fill ~-2 ~-16 ~-14 ~2 ~-16 ~-13 spruce_slab[type=top]
fill ~-2 ~-16 ~-12 ~2 ~-16 ~-12 spruce_slab[type=double]
fill ~-2 ~-16 ~-11 ~2 ~-16 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-16 ~-10 ~2 ~-16 ~-10 spruce_slab[type=bottom]


fill ~-2 ~-15 ~-19 ~2 ~-15 ~-18 air
fill ~-2 ~-15 ~-17 ~2 ~-15 ~-16 spruce_slab[type=top]
fill ~-2 ~-15 ~-15 ~2 ~-15 ~-15 spruce_slab[type=double]
fill ~-2 ~-15 ~-14 ~2 ~-15 ~-14 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-15 ~-13 ~2 ~-15 ~-13 spruce_slab[type=bottom]


fill ~-3 ~-14 ~-22 ~3 ~-14 ~-22 air
fill ~-2 ~-14 ~-21 ~2 ~-14 ~-21 air
fill ~-2 ~-14 ~-20 ~2 ~-14 ~-19 spruce_slab[type=top]
fill ~-2 ~-14 ~-18 ~2 ~-14 ~-18 spruce_slab[type=double]
fill ~-2 ~-14 ~-17 ~2 ~-14 ~-17 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-14 ~-16 ~2 ~-14 ~-16 spruce_slab[type=bottom]


setblock ~-3 ~-13 ~-21 air
setblock ~3 ~-13 ~-21 air

fill ~-3 ~-13 ~-22 ~3 ~-13 ~-22 spruce_slab[type=top]
fill ~-2 ~-13 ~-21 ~2 ~-13 ~-21 spruce_slab[type=double]
fill ~-2 ~-13 ~-20 ~2 ~-13 ~-20 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-13 ~-19 ~2 ~-13 ~-19 spruce_slab[type=bottom]


setblock ~-3 ~-12 ~-22 iron_bars
setblock ~3 ~-12 ~-22 iron_bars
setblock ~-3 ~-12 ~-20 air
setblock ~3 ~-12 ~-20 air

fill ~-2 ~-12 ~-22 ~2 ~-12 ~-22 spruce_slab[type=bottom]


setblock ~-3 ~-11 ~-21 iron_bars
setblock ~3 ~-11 ~-21 iron_bars
setblock ~-3 ~-11 ~-18 air
setblock ~3 ~-11 ~-18 air


setblock ~-3 ~-10 ~-19 iron_bars
setblock ~3 ~-10 ~-19 iron_bars
setblock ~-3 ~-10 ~-17 air
setblock ~3 ~-10 ~-17 air


setblock ~-3 ~-09 ~-18 iron_bars
setblock ~3 ~-09 ~-18 iron_bars


setblock ~-3 ~-08 ~-14 air
setblock ~3 ~-08 ~-14 air


setblock ~-3 ~-07 ~-15 iron_bars
setblock ~3 ~-07 ~-15 iron_bars


setblock ~-3 ~-05 ~-10 air
setblock ~3 ~-05 ~-10 air


setblock ~-3 ~-04 ~-11 iron_bars
setblock ~3 ~-04 ~-11 iron_bars


setblock ~-3 ~-03 ~-07 air
setblock ~3 ~-03 ~-07 air


setblock ~-3 ~-02 ~-08 iron_bars
setblock ~3 ~-02 ~-08 iron_bars

scoreboard players set @s status 12
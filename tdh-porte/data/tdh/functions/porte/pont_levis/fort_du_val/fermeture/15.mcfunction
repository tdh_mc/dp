fill ~-2 ~-16 ~-11 ~2 ~-16 ~-10 air
fill ~-2 ~-16 ~-09 ~2 ~-16 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-16 ~-08 ~2 ~-16 ~-08 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-15 ~-13 ~2 ~-15 ~-12 air
fill ~-2 ~-15 ~-11 ~2 ~-15 ~-11 spruce_slab[type=top]
fill ~-2 ~-15 ~-10 ~2 ~-15 ~-10 spruce_slab[type=double]
fill ~-2 ~-15 ~-09 ~2 ~-15 ~-09 spruce_slab[type=bottom]


fill ~-2 ~-14 ~-15 ~2 ~-14 ~-14 air
fill ~-2 ~-14 ~-13 ~2 ~-14 ~-13 spruce_slab[type=top]
fill ~-2 ~-14 ~-12 ~2 ~-14 ~-12 spruce_slab[type=double]
fill ~-2 ~-14 ~-11 ~2 ~-14 ~-11 spruce_slab[type=bottom]


fill ~-2 ~-13 ~-17 ~2 ~-13 ~-16 air
fill ~-2 ~-13 ~-15 ~2 ~-13 ~-15 spruce_slab[type=top]
fill ~-2 ~-13 ~-14 ~2 ~-13 ~-14 spruce_slab[type=double]
fill ~-2 ~-13 ~-13 ~2 ~-13 ~-13 spruce_slab[type=bottom]


fill ~-2 ~-12 ~-19 ~2 ~-12 ~-18 air
fill ~-2 ~-12 ~-17 ~2 ~-12 ~-17 spruce_slab[type=top]
fill ~-2 ~-12 ~-16 ~2 ~-12 ~-16 spruce_slab[type=double]
fill ~-2 ~-12 ~-15 ~2 ~-12 ~-15 spruce_slab[type=bottom]


fill ~-3 ~-11 ~-21 ~3 ~-11 ~-21 air
fill ~-2 ~-11 ~-20 ~2 ~-11 ~-19 air
fill ~-2 ~-11 ~-18 ~2 ~-11 ~-18 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-17 ~2 ~-11 ~-17 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-10 ~-21 ~3 ~-10 ~-21 air
fill ~-3 ~-10 ~-20 ~3 ~-10 ~-20 spruce_slab[type=top]
fill ~-2 ~-10 ~-19 ~2 ~-10 ~-19 spruce_slab[type=double]
fill ~-2 ~-10 ~-18 ~2 ~-10 ~-18 spruce_slab[type=bottom]


setblock ~-3 ~-09 ~-18 air
setblock ~3 ~-09 ~-18 air

fill ~-2 ~-09 ~-20 ~2 ~-09 ~-20 spruce_slab[type=bottom]


setblock ~-3 ~-08 ~-19 iron_bars
setblock ~3 ~-08 ~-19 iron_bars


setblock ~-3 ~-07 ~-15 air
setblock ~3 ~-07 ~-15 air


setblock ~-3 ~-06 ~-16 iron_bars
setblock ~3 ~-06 ~-16 iron_bars


setblock ~-3 ~-05 ~-12 air
setblock ~3 ~-05 ~-12 air


setblock ~-3 ~-04 ~-13 iron_bars
setblock ~3 ~-04 ~-13 iron_bars



setblock ~-3 ~0 ~5 iron_bars
setblock ~-3 ~0 ~6 iron_block
setblock ~3 ~0 ~5 iron_bars
setblock ~3 ~0 ~6 iron_block

scoreboard players set @s status 15
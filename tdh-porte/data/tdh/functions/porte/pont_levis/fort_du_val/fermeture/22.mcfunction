fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 spruce_stairs[facing=south,half=top]


fill ~-2 ~-17 ~-04 ~2 ~-17 ~-04 spruce_slab[type=double]
fill ~-2 ~-17 ~-03 ~2 ~-17 ~-03 spruce_slab[type=bottom]


fill ~-2 ~-16 ~-05 ~2 ~-16 ~-05 spruce_slab[type=double]
fill ~-2 ~-16 ~-04 ~2 ~-16 ~-04 spruce_slab[type=bottom]


fill ~-2 ~-15 ~-06 ~2 ~-15 ~-06 spruce_slab[type=double]
fill ~-2 ~-15 ~-05 ~2 ~-15 ~-05 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-14 ~-07 ~2 ~-14 ~-07 spruce_stairs[facing=south,half=top]


fill ~-2 ~-13 ~-06 ~2 ~-13 ~-06 air


fill ~-2 ~-12 ~-08 ~2 ~-12 ~-08 spruce_slab[type=double]


fill ~-2 ~-11 ~-08 ~2 ~-11 ~-08 spruce_slab[type=double]


fill ~-2 ~-10 ~-10 ~2 ~-10 ~-10 air
fill ~-2 ~-10 ~-09 ~2 ~-10 ~-09 spruce_slab[type=double]


fill ~-2 ~-09 ~-11 ~2 ~-09 ~-11 air
fill ~-2 ~-09 ~-10 ~2 ~-09 ~-10 spruce_slab[type=double]
fill ~-2 ~-09 ~-09 ~2 ~-09 ~-09 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-08 ~-12 ~2 ~-08 ~-12 air
fill ~-2 ~-08 ~-11 ~2 ~-08 ~-11 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-10 ~2 ~-08 ~-10 spruce_slab[type=double]


fill ~-2 ~-07 ~-12 ~2 ~-07 ~-12 air
fill ~-2 ~-07 ~-11 ~2 ~-07 ~-11 spruce_slab[type=double]


fill ~-2 ~-06 ~-13 ~2 ~-06 ~-13 air
fill ~-2 ~-06 ~-12 ~2 ~-06 ~-12 spruce_slab[type=double]
fill ~-2 ~-06 ~-11 ~2 ~-06 ~-11 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-05 ~-14 ~2 ~-05 ~-14 air
fill ~-2 ~-05 ~-13 ~2 ~-05 ~-13 spruce_stairs[facing=south,half=top]
fill ~-2 ~-05 ~-12 ~2 ~-05 ~-12 spruce_slab[type=double]


fill ~-2 ~-04 ~-15 ~2 ~-04 ~-14 air
fill ~-2 ~-04 ~-13 ~2 ~-04 ~-13 spruce_slab[type=double]


fill ~-3 ~-03 ~-16 ~-3 ~-03 ~-12 air
fill ~3 ~-03 ~-16 ~3 ~-03 ~-12 air

fill ~-2 ~-03 ~-15 ~2 ~-03 ~-15 air
fill ~-2 ~-03 ~-14 ~2 ~-03 ~-14 spruce_stairs[facing=south,half=top]
fill ~-2 ~-03 ~-13 ~2 ~-03 ~-13 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-02 ~-14 ~-3 ~-02 ~-13 iron_bars
fill ~3 ~-02 ~-14 ~3 ~-02 ~-13 iron_bars
setblock ~-3 ~-02 ~-09 air
setblock ~3 ~-02 ~-09 air

setblock ~-3 ~-02 ~-15 spruce_stairs[facing=south,half=top]
setblock ~3 ~-02 ~-15 spruce_stairs[facing=south,half=top]
fill ~-2 ~-02 ~-14 ~2 ~-02 ~-14 spruce_slab[type=bottom]


setblock ~-3 ~-01 ~-10 iron_bars
setblock ~3 ~-01 ~-10 iron_bars

scoreboard players set @s status 22
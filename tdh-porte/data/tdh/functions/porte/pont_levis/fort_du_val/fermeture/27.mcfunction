fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 air


fill ~-2 ~-18 ~-03 ~2 ~-18 ~-03 spruce_stairs[facing=south,half=top]
fill ~-2 ~-18 ~-02 ~2 ~-18 ~-02 spruce_slab[type=double]


fill ~-2 ~-17 ~-04 ~2 ~-17 ~-04 air
fill ~-2 ~-17 ~-02 ~2 ~-17 ~-02 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-16 ~-04 ~2 ~-16 ~-04 spruce_stairs[facing=south,half=top]
fill ~-2 ~-16 ~-03 ~2 ~-16 ~-03 spruce_slab[type=double]


fill ~-2 ~-15 ~-05 ~2 ~-15 ~-05 air
fill ~-2 ~-15 ~-03 ~2 ~-15 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-14 ~-05 ~2 ~-14 ~-05 spruce_stairs[facing=south,half=top]
fill ~-2 ~-14 ~-04 ~2 ~-14 ~-04 spruce_slab[type=double]


fill ~-2 ~-13 ~-06 ~2 ~-13 ~-06 air
fill ~-2 ~-13 ~-04 ~2 ~-13 ~-04 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-12 ~-06 ~2 ~-12 ~-06 spruce_stairs[facing=south,half=top]
fill ~-2 ~-12 ~-05 ~2 ~-12 ~-05 spruce_slab[type=double]


fill ~-2 ~-11 ~-07 ~2 ~-11 ~-07 air
fill ~-2 ~-11 ~-05 ~2 ~-11 ~-05 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-10 ~-07 ~2 ~-10 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-10 ~-06 ~2 ~-10 ~-06 spruce_slab[type=double]


fill ~-2 ~-09 ~-08 ~2 ~-09 ~-08 air
fill ~-2 ~-09 ~-06 ~2 ~-09 ~-06 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-08 ~-08 ~2 ~-08 ~-08 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-07 ~2 ~-08 ~-07 spruce_slab[type=double]


fill ~-2 ~-07 ~-09 ~2 ~-07 ~-09 air
fill ~-2 ~-07 ~-07 ~2 ~-07 ~-07 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-06 ~-09 ~2 ~-06 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-08 ~2 ~-06 ~-08 spruce_slab[type=double]


fill ~-2 ~-05 ~-10 ~2 ~-05 ~-10 air
fill ~-2 ~-05 ~-08 ~2 ~-05 ~-08 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-04 ~-10 ~2 ~-04 ~-10 air
fill ~-2 ~-04 ~-09 ~2 ~-04 ~-09 spruce_slab[type=double]


fill ~-2 ~-03 ~-10 ~2 ~-03 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-03 ~-09 ~2 ~-03 ~-09 spruce_slab[type=double]


fill ~-2 ~-02 ~-11 ~2 ~-02 ~-11 air
fill ~-2 ~-02 ~-10 ~2 ~-02 ~-10 spruce_slab[type=double]
fill ~-2 ~-02 ~-09 ~2 ~-02 ~-09 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-01 ~-11 ~3 ~-01 ~-11 air
setblock ~-3 ~-01 ~-10 spruce_stairs[facing=south,half=top]
setblock ~3 ~-01 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-01 ~-10 ~2 ~-01 ~-10 spruce_slab[type=double]



setblock ~-3 ~0 ~12 iron_bars
setblock ~-3 ~0 ~13 iron_block
setblock ~3 ~0 ~12 iron_bars
setblock ~3 ~0 ~13 iron_block

scoreboard players set @s status 27
fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 spruce_slab[type=top]
fill ~-2 ~-19 ~-02 ~2 ~-19 ~-02 spruce_slab[type=double]


fill ~-2 ~-18 ~-04 ~2 ~-18 ~-04 air
fill ~-2 ~-18 ~-03 ~2 ~-18 ~-03 spruce_slab[type=double]


fill ~-2 ~-17 ~-05 ~2 ~-17 ~-05 air
fill ~-2 ~-17 ~-04 ~2 ~-17 ~-04 spruce_stairs[facing=south,half=top]
fill ~-2 ~-17 ~-03 ~2 ~-17 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-16 ~-06 ~2 ~-16 ~-06 air
fill ~-2 ~-16 ~-05 ~2 ~-16 ~-05 spruce_stairs[facing=south,half=top]
fill ~-2 ~-16 ~-04 ~2 ~-16 ~-04 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-15 ~-07 ~2 ~-15 ~-07 air
fill ~-2 ~-15 ~-06 ~2 ~-15 ~-06 spruce_slab[type=top]
fill ~-2 ~-15 ~-05 ~2 ~-15 ~-05 spruce_slab[type=double]


fill ~-2 ~-14 ~-08 ~2 ~-14 ~-07 air
fill ~-2 ~-14 ~-06 ~2 ~-14 ~-06 spruce_slab[type=double]


fill ~-2 ~-13 ~-09 ~2 ~-13 ~-08 air
fill ~-2 ~-13 ~-07 ~2 ~-13 ~-07 spruce_slab[type=double]
fill ~-2 ~-13 ~-06 ~2 ~-13 ~-06 spruce_slab[type=bottom]


fill ~-2 ~-12 ~-10 ~2 ~-12 ~-09 air
fill ~-2 ~-12 ~-08 ~2 ~-12 ~-08 spruce_stairs[facing=south,half=top]
fill ~-2 ~-12 ~-07 ~2 ~-12 ~-07 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-11 ~-11 ~2 ~-11 ~-10 air
fill ~-2 ~-11 ~-09 ~2 ~-11 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-08 ~2 ~-11 ~-08 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-10 ~-12 ~2 ~-10 ~-11 air
fill ~-2 ~-10 ~-10 ~2 ~-10 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-10 ~-09 ~2 ~-10 ~-09 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-09 ~-13 ~2 ~-09 ~-12 air
fill ~-2 ~-09 ~-11 ~2 ~-09 ~-11 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-10 ~2 ~-09 ~-10 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-08 ~-14 ~2 ~-08 ~-13 air
fill ~-2 ~-08 ~-12 ~2 ~-08 ~-12 spruce_slab[type=top]
fill ~-2 ~-08 ~-11 ~2 ~-08 ~-11 spruce_slab[type=double]


fill ~-2 ~-07 ~-14 ~2 ~-07 ~-14 air
fill ~-2 ~-07 ~-12 ~2 ~-07 ~-12 spruce_slab[type=double]


fill ~-2 ~-06 ~-15 ~2 ~-06 ~-14 air
fill ~-2 ~-06 ~-13 ~2 ~-06 ~-13 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-12 ~2 ~-06 ~-12 spruce_slab[type=bottom]


fill ~-2 ~-05 ~-16 ~2 ~-05 ~-15 air
fill ~-2 ~-05 ~-14 ~2 ~-05 ~-14 spruce_stairs[facing=south,half=top]
fill ~-2 ~-05 ~-13 ~2 ~-05 ~-13 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-04 ~-17 ~-3 ~-04 ~-14 air
fill ~3 ~-04 ~-17 ~3 ~-04 ~-14 air

fill ~-2 ~-04 ~-16 ~2 ~-04 ~-16 air
fill ~-2 ~-04 ~-15 ~2 ~-04 ~-15 spruce_stairs[facing=south,half=top]
fill ~-2 ~-04 ~-14 ~2 ~-04 ~-14 spruce_stairs[facing=north,half=bottom]


setblock ~-3 ~-03 ~-15 iron_bars
setblock ~3 ~-03 ~-15 iron_bars

setblock ~-3 ~-03 ~-16 spruce_stairs[facing=south,half=top]
setblock ~3 ~-03 ~-16 spruce_stairs[facing=south,half=top]
fill ~-2 ~-03 ~-15 ~2 ~-03 ~-15 spruce_slab[type=bottom]



setblock ~-3 ~0 ~8 iron_bars
setblock ~-3 ~0 ~9 iron_block
setblock ~3 ~0 ~8 iron_bars
setblock ~3 ~0 ~9 iron_block

scoreboard players set @s status 21
fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 spruce_slab[type=top]


fill ~-2 ~-18 ~-05 ~2 ~-18 ~-05 spruce_stairs[facing=south,half=top]


fill ~-2 ~-17 ~-06 ~2 ~-17 ~-06 spruce_stairs[facing=south,half=top]


fill ~-2 ~-16 ~-08 ~2 ~-16 ~-08 air
fill ~-2 ~-16 ~-07 ~2 ~-16 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-16 ~-06 ~2 ~-16 ~-06 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-15 ~-08 ~2 ~-15 ~-08 spruce_slab[type=double]
fill ~-2 ~-15 ~-07 ~2 ~-15 ~-07 spruce_slab[type=bottom]


fill ~-2 ~-14 ~-11 ~2 ~-14 ~-11 air
fill ~-2 ~-14 ~-10 ~2 ~-14 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-14 ~-09 ~2 ~-14 ~-09 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-13 ~-12 ~2 ~-13 ~-12 air
fill ~-2 ~-13 ~-11 ~2 ~-13 ~-11 spruce_stairs[facing=south,half=top]
fill ~-2 ~-13 ~-10 ~2 ~-13 ~-10 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-12 ~-14 ~2 ~-12 ~-13 air
fill ~-2 ~-12 ~-12 ~2 ~-12 ~-12 spruce_stairs[facing=south,half=top]
fill ~-2 ~-12 ~-11 ~2 ~-12 ~-11 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-11 ~-15 ~2 ~-11 ~-14 air
fill ~-2 ~-11 ~-13 ~2 ~-11 ~-13 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-12 ~2 ~-11 ~-12 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-10 ~-17 ~2 ~-10 ~-16 air
fill ~-2 ~-10 ~-15 ~2 ~-10 ~-15 spruce_slab[type=top]
fill ~-2 ~-10 ~-14 ~2 ~-10 ~-14 spruce_slab[type=double]
fill ~-2 ~-10 ~-13 ~2 ~-10 ~-13 spruce_slab[type=bottom]


fill ~-2 ~-09 ~-18 ~2 ~-09 ~-17 air
fill ~-2 ~-09 ~-16 ~2 ~-09 ~-16 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-15 ~2 ~-09 ~-15 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-08 ~-19 ~3 ~-08 ~-19 air
fill ~-2 ~-08 ~-18 ~2 ~-08 ~-18 air
fill ~-2 ~-08 ~-17 ~2 ~-08 ~-17 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-16 ~2 ~-08 ~-16 spruce_stairs[facing=north,half=bottom]


setblock ~-3 ~-07 ~-20 air
setblock ~3 ~-07 ~-20 air
setblock ~-3 ~-07 ~-17 air
setblock ~3 ~-07 ~-17 air

fill ~-3 ~-07 ~-19 ~3 ~-07 ~-19 air
setblock ~-3 ~-07 ~-18 spruce_slab[type=top]
setblock ~3 ~-07 ~-18 spruce_slab[type=top]
fill ~-2 ~-07 ~-18 ~2 ~-07 ~-18 spruce_stairs[facing=south,half=top]
fill ~-2 ~-07 ~-17 ~2 ~-07 ~-17 spruce_stairs[facing=north,half=bottom]


setblock ~-3 ~-06 ~-18 iron_bars
setblock ~3 ~-06 ~-18 iron_bars
setblock ~-3 ~-06 ~-15 air
setblock ~3 ~-06 ~-15 air

setblock ~-3 ~-06 ~-19 spruce_stairs[facing=south,half=top]
setblock ~3 ~-06 ~-19 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-18 ~2 ~-06 ~-18 spruce_slab[type=bottom]


setblock ~-3 ~-05 ~-16 iron_bars
setblock ~3 ~-05 ~-16 iron_bars
setblock ~-3 ~-05 ~-13 air
setblock ~3 ~-05 ~-13 air


setblock ~-3 ~-04 ~-14 iron_bars
setblock ~3 ~-04 ~-14 iron_bars
setblock ~-3 ~-04 ~-11 air
setblock ~3 ~-04 ~-11 air


setblock ~-3 ~-03 ~-12 iron_bars
setblock ~3 ~-03 ~-12 iron_bars
setblock ~-3 ~-03 ~-09 air
setblock ~3 ~-03 ~-09 air


setblock ~-3 ~-02 ~-10 iron_bars
setblock ~3 ~-02 ~-10 iron_bars
setblock ~-3 ~-02 ~-07 air
setblock ~3 ~-02 ~-07 air


setblock ~-3 ~-01 ~-08 iron_bars
setblock ~3 ~-01 ~-08 iron_bars
setblock ~-3 ~-01 ~-05 air
setblock ~3 ~-01 ~-05 air


setblock ~-3 ~-00 ~-06 iron_bars
setblock ~3 ~-00 ~-06 iron_bars

scoreboard players set @s status 18
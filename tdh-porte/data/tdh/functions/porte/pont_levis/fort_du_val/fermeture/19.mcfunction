fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 air


fill ~-2 ~-18 ~-05 ~2 ~-18 ~-05 spruce_slab[type=top]


fill ~-2 ~-15 ~-09 ~2 ~-15 ~-09 air
fill ~-2 ~-15 ~-08 ~2 ~-15 ~-08 spruce_stairs[facing=south,half=top]
fill ~-2 ~-15 ~-07 ~2 ~-15 ~-07 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-14 ~-10 ~2 ~-14 ~-10 air
fill ~-2 ~-14 ~-09 ~2 ~-14 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-14 ~-08 ~2 ~-14 ~-08 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-13 ~-11 ~2 ~-13 ~-11 air
fill ~-2 ~-13 ~-10 ~2 ~-13 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-13 ~-09 ~2 ~-13 ~-09 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-12 ~-12 ~2 ~-12 ~-12 air
fill ~-2 ~-12 ~-11 ~2 ~-12 ~-11 spruce_stairs[facing=south,half=top]
fill ~-2 ~-12 ~-10 ~2 ~-12 ~-10 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-11 ~-13 ~2 ~-11 ~-13 air
fill ~-2 ~-11 ~-12 ~2 ~-11 ~-12 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-11 ~2 ~-11 ~-11 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-10 ~-15 ~2 ~-10 ~-14 air
fill ~-2 ~-10 ~-13 ~2 ~-10 ~-13 spruce_stairs[facing=south,half=top]
fill ~-2 ~-10 ~-12 ~2 ~-10 ~-12 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-09 ~-16 ~2 ~-09 ~-15 air
fill ~-2 ~-09 ~-14 ~2 ~-09 ~-14 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-13 ~2 ~-09 ~-13 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-08 ~-17 ~2 ~-08 ~-16 air
fill ~-2 ~-08 ~-15 ~2 ~-08 ~-15 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-14 ~2 ~-08 ~-14 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-07 ~-18 ~3 ~-07 ~-18 air
fill ~-2 ~-07 ~-17 ~2 ~-07 ~-17 air
fill ~-2 ~-07 ~-16 ~2 ~-07 ~-16 spruce_stairs[facing=south,half=top]
fill ~-2 ~-07 ~-15 ~2 ~-07 ~-15 spruce_stairs[facing=north,half=bottom]


setblock ~-3 ~-06 ~-19 air
setblock ~3 ~-06 ~-19 air
setblock ~-3 ~-06 ~-16 air
setblock ~3 ~-06 ~-16 air

fill ~-3 ~-06 ~-18 ~3 ~-06 ~-18 air
setblock ~-3 ~-06 ~-17 spruce_slab[type=top]
setblock ~3 ~-06 ~-17 spruce_slab[type=top]
fill ~-2 ~-06 ~-17 ~2 ~-06 ~-17 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-16 ~2 ~-06 ~-16 spruce_stairs[facing=north,half=bottom]


setblock ~-3 ~-05 ~-17 iron_bars
setblock ~3 ~-05 ~-17 iron_bars
setblock ~-3 ~-05 ~-14 air
setblock ~3 ~-05 ~-14 air

setblock ~-3 ~-05 ~-18 spruce_stairs[facing=south,half=top]
setblock ~3 ~-05 ~-18 spruce_stairs[facing=south,half=top]
fill ~-2 ~-05 ~-17 ~2 ~-05 ~-17 spruce_slab[type=bottom]


setblock ~-3 ~-04 ~-15 iron_bars
setblock ~3 ~-04 ~-15 iron_bars
setblock ~-3 ~-04 ~-12 air
setblock ~3 ~-04 ~-12 air


setblock ~-3 ~-03 ~-13 iron_bars
setblock ~3 ~-03 ~-13 iron_bars
setblock ~-3 ~-03 ~-10 air
setblock ~3 ~-03 ~-10 air


setblock ~-3 ~-02 ~-11 iron_bars
setblock ~3 ~-02 ~-11 iron_bars
setblock ~-3 ~-02 ~-08 air
setblock ~3 ~-02 ~-08 air


setblock ~-3 ~-01 ~-09 iron_bars
setblock ~3 ~-01 ~-09 iron_bars



setblock ~-3 ~0 ~7 iron_bars
setblock ~-3 ~0 ~8 iron_block
setblock ~3 ~0 ~7 iron_bars
setblock ~3 ~0 ~8 iron_block

scoreboard players set @s status 19
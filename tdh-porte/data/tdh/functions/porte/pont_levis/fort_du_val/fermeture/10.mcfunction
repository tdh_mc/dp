fill ~-2 ~-19 ~-08 ~2 ~-19 ~-08 air
fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 spruce_slab[type=bottom]

fill ~-2 ~-18 ~-13 ~2 ~-18 ~-12 air
fill ~-2 ~-18 ~-10 ~2 ~-18 ~-10 spruce_slab[type=top]
fill ~-2 ~-18 ~-08 ~2 ~-18 ~-08 spruce_slab[type=double]

fill ~-2 ~-17 ~-18 ~2 ~-17 ~-16 air
fill ~-2 ~-17 ~-15 ~2 ~-17 ~-14 spruce_slab[type=top]
fill ~-2 ~-17 ~-13 ~2 ~-17 ~-12 spruce_slab[type=double]
fill ~-2 ~-17 ~-10 ~2 ~-17 ~-10 spruce_slab[type=bottom]


fill ~-3 ~-16 ~-22 ~3 ~-16 ~-22 air
fill ~-2 ~-16 ~-21 ~2 ~-16 ~-20 air
fill ~-2 ~-16 ~-19 ~2 ~-16 ~-18 spruce_slab[type=top]
fill ~-2 ~-16 ~-17 ~2 ~-16 ~-16 spruce_slab[type=double]
fill ~-2 ~-16 ~-15 ~2 ~-16 ~-14 spruce_slab[type=bottom]


setblock ~-3 ~-15 ~-21 air
setblock ~3 ~-15 ~-21 air

fill ~-3 ~-15 ~-22 ~3 ~-15 ~-22 spruce_slab[type=top]
fill ~-2 ~-15 ~-21 ~2 ~-15 ~-20 spruce_slab[type=double]
fill ~-2 ~-15 ~-19 ~2 ~-15 ~-18 spruce_slab[type=bottom]


setblock ~-3 ~-14 ~-22 iron_bars
setblock ~3 ~-14 ~-22 iron_bars
setblock ~-3 ~-14 ~-20 air
setblock ~3 ~-14 ~-20 air

fill ~-2 ~-14 ~-22 ~2 ~-14 ~-22 spruce_slab[type=bottom]


setblock ~-3 ~-13 ~-21 iron_bars
setblock ~3 ~-13 ~-21 iron_bars
setblock ~-3 ~-13 ~-19 air
setblock ~3 ~-13 ~-19 air


setblock ~-3 ~-12 ~-20 iron_bars
setblock ~3 ~-12 ~-20 iron_bars


setblock ~-3 ~-06 ~-10 air
setblock ~3 ~-06 ~-10 air


setblock ~-3 ~-05 ~-11 iron_bars
setblock ~3 ~-05 ~-11 iron_bars

scoreboard players set @s status 10
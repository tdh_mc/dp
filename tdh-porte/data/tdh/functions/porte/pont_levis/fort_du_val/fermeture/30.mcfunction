fill ~-2 ~-15 ~-04 ~2 ~-15 ~-04 air


fill ~-2 ~-14 ~-04 ~2 ~-14 ~-04 spruce_stairs[facing=south,half=top]
fill ~-2 ~-14 ~-03 ~2 ~-14 ~-03 spruce_slab[type=double]


fill ~-2 ~-13 ~-03 ~2 ~-13 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-12 ~-05 ~2 ~-12 ~-05 air


fill ~-2 ~-11 ~-05 ~2 ~-11 ~-05 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-04 ~2 ~-11 ~-04 spruce_slab[type=double]


fill ~-2 ~-10 ~-04 ~2 ~-10 ~-04 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-09 ~-06 ~2 ~-09 ~-06 air


fill ~-2 ~-08 ~-06 ~2 ~-08 ~-06 air
fill ~-2 ~-08 ~-05 ~2 ~-08 ~-05 spruce_slab[type=double]


fill ~-2 ~-07 ~-06 ~2 ~-07 ~-06 spruce_stairs[facing=south,half=top]
fill ~-2 ~-07 ~-05 ~2 ~-07 ~-05 spruce_slab[type=double]


fill ~-2 ~-06 ~-07 ~2 ~-06 ~-07 air
fill ~-2 ~-06 ~-05 ~2 ~-06 ~-05 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-05 ~-07 ~2 ~-05 ~-07 air
fill ~-2 ~-05 ~-06 ~2 ~-05 ~-06 spruce_slab[type=double]


fill ~-2 ~-04 ~-07 ~2 ~-04 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-04 ~-06 ~2 ~-04 ~-06 spruce_slab[type=double]


fill ~-2 ~-03 ~-08 ~2 ~-03 ~-08 air
fill ~-2 ~-03 ~-06 ~2 ~-03 ~-06 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-02 ~-08 ~2 ~-02 ~-08 air
fill ~-2 ~-02 ~-07 ~2 ~-02 ~-07 spruce_slab[type=double]


fill ~-2 ~-01 ~-08 ~2 ~-01 ~-08 air
fill ~-2 ~-01 ~-07 ~2 ~-01 ~-07 spruce_slab[type=double]


fill ~-3 ~-00 ~-08 ~3 ~-00 ~-08 air
setblock ~-3 ~-00 ~-07 spruce_stairs[facing=south,half=top]
setblock ~3 ~-00 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-00 ~-07 ~2 ~-00 ~-07 spruce_slab[type=double]



setblock ~-3 ~0 ~15 iron_bars
setblock ~-3 ~0 ~16 iron_block
setblock ~3 ~0 ~15 iron_bars
setblock ~3 ~0 ~16 iron_block

scoreboard players set @s status 30
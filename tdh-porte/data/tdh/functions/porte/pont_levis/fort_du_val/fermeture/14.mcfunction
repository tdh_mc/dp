fill ~-2 ~-19 ~-05 ~2 ~-19 ~-05 air


fill ~-2 ~-18 ~-07 ~2 ~-18 ~-07 air
fill ~-2 ~-18 ~-06 ~2 ~-18 ~-06 spruce_slab[type=top]
fill ~-2 ~-18 ~-05 ~2 ~-18 ~-05 spruce_slab[type=double]


fill ~-2 ~-17 ~-09 ~2 ~-17 ~-09 air
fill ~-2 ~-17 ~-08 ~2 ~-17 ~-08 spruce_slab[type=top]
fill ~-2 ~-17 ~-07 ~2 ~-17 ~-07 spruce_slab[type=double]
fill ~-2 ~-17 ~-06 ~2 ~-17 ~-06 spruce_slab[type=bottom]


fill ~-2 ~-16 ~-12 ~2 ~-16 ~-12 air
fill ~-2 ~-16 ~-10 ~2 ~-16 ~-10 spruce_slab[type=top]
fill ~-2 ~-16 ~-09 ~2 ~-16 ~-09 spruce_slab[type=double]
fill ~-2 ~-16 ~-08 ~2 ~-16 ~-08 spruce_slab[type=bottom]


fill ~-2 ~-15 ~-14 ~2 ~-15 ~-14 air
fill ~-2 ~-15 ~-13 ~2 ~-15 ~-13 spruce_slab[type=top]
fill ~-2 ~-15 ~-12 ~2 ~-15 ~-12 spruce_slab[type=double]
fill ~-2 ~-15 ~-11 ~2 ~-15 ~-11 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-15 ~-10 ~2 ~-15 ~-10 spruce_slab[type=bottom]


fill ~-2 ~-14 ~-16 ~2 ~-14 ~-16 air
fill ~-2 ~-14 ~-15 ~2 ~-14 ~-15 spruce_slab[type=top]
fill ~-2 ~-14 ~-14 ~2 ~-14 ~-14 spruce_slab[type=double]
fill ~-2 ~-14 ~-13 ~2 ~-14 ~-13 spruce_slab[type=bottom]


fill ~-2 ~-13 ~-19 ~2 ~-13 ~-18 air
fill ~-2 ~-13 ~-17 ~2 ~-13 ~-17 spruce_slab[type=top]
fill ~-2 ~-13 ~-16 ~2 ~-13 ~-16 spruce_slab[type=double]
fill ~-2 ~-13 ~-15 ~2 ~-13 ~-15 spruce_slab[type=bottom]


fill ~-3 ~-12 ~-21 ~3 ~-12 ~-21 air
fill ~-2 ~-12 ~-20 ~2 ~-12 ~-20 air
fill ~-2 ~-12 ~-19 ~2 ~-12 ~-19 spruce_slab[type=top]
fill ~-2 ~-12 ~-18 ~2 ~-12 ~-18 spruce_slab[type=double]
fill ~-2 ~-12 ~-17 ~2 ~-12 ~-17 spruce_slab[type=bottom]


setblock ~-3 ~-11 ~-20 air
setblock ~3 ~-11 ~-20 air

fill ~-3 ~-11 ~-21 ~3 ~-11 ~-21 spruce_slab[type=top]
fill ~-2 ~-11 ~-20 ~2 ~-11 ~-20 spruce_slab[type=double]
fill ~-2 ~-11 ~-19 ~2 ~-11 ~-19 spruce_slab[type=bottom]


setblock ~-3 ~-10 ~-21 iron_bars
setblock ~3 ~-10 ~-21 iron_bars
fill ~-3 ~-10 ~-19 ~-3 ~-10 ~-18 air
fill ~3 ~-10 ~-19 ~3 ~-10 ~-18 air

fill ~-2 ~-10 ~-21 ~2 ~-10 ~-21 spruce_slab[type=bottom]


fill ~-3 ~-09 ~-20 ~-3 ~-09 ~-19 iron_bars
fill ~3 ~-09 ~-20 ~3 ~-09 ~-19 iron_bars
setblock ~-3 ~-09 ~-17 air
setblock ~3 ~-09 ~-17 air


setblock ~-3 ~-08 ~-18 iron_bars
setblock ~3 ~-08 ~-18 iron_bars
fill ~-3 ~-08 ~-16 ~-3 ~-08 ~-15 air
fill ~3 ~-08 ~-16 ~3 ~-08 ~-15 air


fill ~-3 ~-07 ~-17 ~-3 ~-07 ~-16 iron_bars
fill ~3 ~-07 ~-17 ~3 ~-07 ~-16 iron_bars
setblock ~-3 ~-07 ~-14 air
setblock ~3 ~-07 ~-14 air


setblock ~-3 ~-06 ~-15 iron_bars
setblock ~3 ~-06 ~-15 iron_bars
fill ~-3 ~-06 ~-13 ~-3 ~-06 ~-12 air
fill ~3 ~-06 ~-13 ~3 ~-06 ~-12 air


fill ~-3 ~-05 ~-14 ~-3 ~-05 ~-13 iron_bars
fill ~3 ~-05 ~-14 ~3 ~-05 ~-13 iron_bars
setblock ~-3 ~-05 ~-11 air
setblock ~3 ~-05 ~-11 air


setblock ~-3 ~-04 ~-12 iron_bars
setblock ~3 ~-04 ~-12 iron_bars
fill ~-3 ~-04 ~-10 ~-3 ~-04 ~-09 air
fill ~3 ~-04 ~-10 ~3 ~-04 ~-09 air


fill ~-3 ~-03 ~-11 ~-3 ~-03 ~-10 iron_bars
fill ~3 ~-03 ~-11 ~3 ~-03 ~-10 iron_bars
setblock ~-3 ~-03 ~-08 air
setblock ~3 ~-03 ~-08 air


setblock ~-3 ~-02 ~-09 iron_bars
setblock ~3 ~-02 ~-09 iron_bars
setblock ~-3 ~-02 ~-06 air
setblock ~3 ~-02 ~-06 air


setblock ~-3 ~-01 ~-07 iron_bars
setblock ~3 ~-01 ~-07 iron_bars

scoreboard players set @s status 14
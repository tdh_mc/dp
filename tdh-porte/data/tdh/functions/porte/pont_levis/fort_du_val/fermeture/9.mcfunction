fill ~-2 ~-20 ~-05 ~2 ~-20 ~-04 air


fill ~-2 ~-19 ~-11 ~2 ~-19 ~-09 air
fill ~-2 ~-19 ~-07 ~2 ~-19 ~-06 spruce_slab[type=top]
fill ~-2 ~-19 ~-04 ~2 ~-19 ~-04 spruce_slab[type=double]
fill ~-2 ~-19 ~-03 ~2 ~-19 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-18 ~-18 ~2 ~-18 ~-14 air
fill ~-2 ~-18 ~-13 ~2 ~-18 ~-11 spruce_slab[type=top]
fill ~-2 ~-18 ~-10 ~2 ~-18 ~-09 spruce_slab[type=double]
fill ~-2 ~-18 ~-08 ~2 ~-18 ~-08 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-18 ~-07 ~2 ~-18 ~-06 spruce_slab[type=bottom]


fill ~-3 ~-17 ~-23 ~3 ~-17 ~-23 air
fill ~-2 ~-17 ~-22 ~2 ~-17 ~-19 air
fill ~-2 ~-17 ~-18 ~2 ~-17 ~-16 spruce_slab[type=top]
fill ~-2 ~-17 ~-15 ~2 ~-17 ~-14 spruce_slab[type=double]
fill ~-2 ~-17 ~-13 ~2 ~-17 ~-13 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-17 ~-12 ~2 ~-17 ~-11 spruce_slab[type=bottom]


fill ~-3 ~-16 ~-23 ~3 ~-16 ~-23 air
fill ~-3 ~-16 ~-22 ~3 ~-16 ~-22 spruce_slab[type=top]
fill ~-2 ~-16 ~-21 ~2 ~-16 ~-21 spruce_slab[type=top]
fill ~-2 ~-16 ~-20 ~2 ~-16 ~-19 spruce_slab[type=double]
fill ~-2 ~-16 ~-18 ~2 ~-16 ~-18 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-16 ~-17 ~2 ~-16 ~-16 spruce_slab[type=bottom]


fill ~-2 ~-15 ~-22 ~2 ~-15 ~-21 spruce_slab[type=bottom]


setblock ~-3 ~-10 ~-15 air
setblock ~3 ~-10 ~-15 air


setblock ~-3 ~-09 ~-16 iron_bars
setblock ~3 ~-09 ~-16 iron_bars


setblock ~-3 ~-07 ~-11 air
setblock ~3 ~-07 ~-11 air


setblock ~-3 ~-06 ~-12 iron_bars
setblock ~3 ~-06 ~-12 iron_bars



setblock ~-3 ~0 ~2 iron_bars
setblock ~-3 ~0 ~3 iron_block
setblock ~3 ~0 ~2 iron_bars
setblock ~3 ~0 ~3 iron_block

scoreboard players set @s status 9
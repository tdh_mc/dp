fill ~-2 ~-12 ~-05 ~2 ~-12 ~-05 spruce_stairs[facing=south,half=top]


fill ~-2 ~-11 ~-04 ~2 ~-11 ~-04 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-10 ~-06 ~2 ~-10 ~-06 air


fill ~-2 ~-09 ~-06 ~2 ~-09 ~-06 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-05 ~2 ~-09 ~-05 spruce_slab[type=double]


fill ~-2 ~-08 ~-05 ~2 ~-08 ~-05 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-07 ~-07 ~2 ~-07 ~-07 air


fill ~-2 ~-06 ~-07 ~2 ~-06 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-06 ~2 ~-06 ~-06 spruce_slab[type=double]


fill ~-2 ~-05 ~-08 ~2 ~-05 ~-08 air
fill ~-2 ~-05 ~-06 ~2 ~-05 ~-06 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-04 ~-08 ~2 ~-04 ~-08 air
fill ~-2 ~-04 ~-07 ~2 ~-04 ~-07 spruce_slab[type=double]


fill ~-2 ~-03 ~-08 ~2 ~-03 ~-08 spruce_stairs[facing=south,half=top]
fill ~-2 ~-03 ~-07 ~2 ~-03 ~-07 spruce_slab[type=double]


fill ~-2 ~-02 ~-09 ~2 ~-02 ~-09 air
fill ~-2 ~-02 ~-07 ~2 ~-02 ~-07 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-01 ~-09 ~2 ~-01 ~-09 air
fill ~-2 ~-01 ~-08 ~2 ~-01 ~-08 spruce_slab[type=double]


fill ~-3 ~-00 ~-09 ~3 ~-00 ~-09 air
setblock ~-3 ~-00 ~-08 spruce_stairs[facing=south,half=top]
setblock ~3 ~-00 ~-08 spruce_stairs[facing=south,half=top]
fill ~-2 ~-00 ~-08 ~2 ~-00 ~-08 spruce_slab[type=double]



setblock ~-3 ~0 ~14 iron_bars
setblock ~-3 ~0 ~15 iron_block
setblock ~3 ~0 ~14 iron_bars
setblock ~3 ~0 ~15 iron_block

scoreboard players set @s status 29
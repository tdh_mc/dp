fill ~-2 ~-19 ~-02 ~2 ~-19 ~-02 spruce_slab[type=bottom]


fill ~-2 ~-18 ~-08 ~2 ~-18 ~-08 air


fill ~-2 ~-17 ~-11 ~2 ~-17 ~-10 air
fill ~-2 ~-17 ~-09 ~2 ~-17 ~-09 spruce_slab[type=top]
fill ~-2 ~-17 ~-08 ~2 ~-17 ~-08 spruce_slab[type=double]


fill ~-2 ~-16 ~-14 ~2 ~-16 ~-13 air
fill ~-2 ~-16 ~-12 ~2 ~-16 ~-11 spruce_slab[type=top]
fill ~-2 ~-16 ~-10 ~2 ~-16 ~-10 spruce_slab[type=double]
fill ~-2 ~-16 ~-09 ~2 ~-16 ~-09 spruce_slab[type=bottom]


fill ~-2 ~-15 ~-17 ~2 ~-15 ~-15 air
fill ~-2 ~-15 ~-14 ~2 ~-15 ~-14 spruce_slab[type=top]
fill ~-2 ~-15 ~-13 ~2 ~-15 ~-13 spruce_slab[type=double]
fill ~-2 ~-15 ~-12 ~2 ~-15 ~-12 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-15 ~-11 ~2 ~-15 ~-11 spruce_slab[type=bottom]


fill ~-2 ~-14 ~-20 ~2 ~-14 ~-17 air
fill ~-2 ~-14 ~-16 ~2 ~-14 ~-16 spruce_slab[type=top]
fill ~-2 ~-14 ~-15 ~2 ~-14 ~-15 spruce_slab[type=double]
fill ~-2 ~-14 ~-14 ~2 ~-14 ~-14 spruce_slab[type=bottom]


fill ~-3 ~-13 ~-22 ~3 ~-13 ~-22 air
fill ~-2 ~-13 ~-21 ~2 ~-13 ~-20 air
fill ~-2 ~-13 ~-19 ~2 ~-13 ~-18 spruce_slab[type=top]
fill ~-2 ~-13 ~-17 ~2 ~-13 ~-17 spruce_slab[type=double]
fill ~-2 ~-13 ~-16 ~2 ~-13 ~-16 spruce_slab[type=bottom]


fill ~-3 ~-12 ~-22 ~3 ~-12 ~-22 air
fill ~-3 ~-12 ~-21 ~3 ~-12 ~-21 spruce_slab[type=top]
fill ~-2 ~-12 ~-20 ~2 ~-12 ~-20 spruce_slab[type=double]
fill ~-2 ~-12 ~-19 ~2 ~-12 ~-19 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-12 ~-18 ~2 ~-12 ~-18 spruce_slab[type=bottom]


setblock ~-3 ~-11 ~-19 air
setblock ~3 ~-11 ~-19 air

fill ~-2 ~-11 ~-21 ~2 ~-11 ~-21 spruce_slab[type=bottom]


setblock ~-3 ~-10 ~-20 iron_bars
setblock ~3 ~-10 ~-20 iron_bars


setblock ~-3 ~-09 ~-16 air
setblock ~3 ~-09 ~-16 air


setblock ~-3 ~-08 ~-17 iron_bars
setblock ~3 ~-08 ~-17 iron_bars


setblock ~-3 ~-07 ~-13 air
setblock ~3 ~-07 ~-13 air


setblock ~-3 ~-06 ~-14 iron_bars
setblock ~3 ~-06 ~-14 iron_bars



setblock ~-3 ~0 ~4 iron_bars
setblock ~-3 ~0 ~5 iron_block
setblock ~3 ~0 ~4 iron_bars
setblock ~3 ~0 ~5 iron_block

scoreboard players set @s status 13
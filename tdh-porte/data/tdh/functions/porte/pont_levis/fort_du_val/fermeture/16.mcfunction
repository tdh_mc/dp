fill ~-2 ~-17 ~-08 ~2 ~-17 ~-08 air
fill ~-2 ~-17 ~-07 ~2 ~-17 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-17 ~-06 ~2 ~-17 ~-06 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-16 ~-09 ~2 ~-16 ~-09 spruce_slab[type=top]
fill ~-2 ~-16 ~-08 ~2 ~-16 ~-08 spruce_slab[type=double]
fill ~-2 ~-16 ~-07 ~2 ~-16 ~-07 spruce_slab[type=bottom]


fill ~-2 ~-14 ~-13 ~2 ~-14 ~-13 air
fill ~-2 ~-14 ~-12 ~2 ~-14 ~-12 spruce_stairs[facing=south,half=top]
fill ~-2 ~-14 ~-11 ~2 ~-14 ~-11 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-13 ~-15 ~2 ~-13 ~-15 air
fill ~-2 ~-13 ~-14 ~2 ~-13 ~-14 spruce_slab[type=top]
fill ~-2 ~-13 ~-13 ~2 ~-13 ~-13 spruce_slab[type=double]
fill ~-2 ~-13 ~-12 ~2 ~-13 ~-12 spruce_slab[type=bottom]


fill ~-2 ~-12 ~-17 ~2 ~-12 ~-17 air
fill ~-2 ~-12 ~-16 ~2 ~-12 ~-16 spruce_slab[type=top]
fill ~-2 ~-12 ~-15 ~2 ~-12 ~-15 spruce_slab[type=double]
fill ~-2 ~-12 ~-14 ~2 ~-12 ~-14 spruce_slab[type=bottom]


fill ~-2 ~-11 ~-18 ~2 ~-11 ~-18 air
fill ~-2 ~-11 ~-17 ~2 ~-11 ~-17 spruce_stairs[facing=south,half=top]
fill ~-2 ~-11 ~-16 ~2 ~-11 ~-16 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-10 ~-20 ~3 ~-10 ~-20 air
fill ~-2 ~-10 ~-19 ~2 ~-10 ~-19 spruce_slab[type=top]
fill ~-2 ~-10 ~-18 ~2 ~-10 ~-18 spruce_slab[type=double]
fill ~-2 ~-10 ~-17 ~2 ~-10 ~-17 spruce_slab[type=bottom]


setblock ~-3 ~-09 ~-19 air
setblock ~3 ~-09 ~-19 air

setblock ~-3 ~-09 ~-20 spruce_slab[type=top]
setblock ~3 ~-09 ~-20 spruce_slab[type=top]
fill ~-2 ~-09 ~-20 ~2 ~-09 ~-20 spruce_stairs[facing=south,half=top]
fill ~-2 ~-09 ~-19 ~2 ~-09 ~-19 spruce_stairs[facing=north,half=bottom]


setblock ~-3 ~-08 ~-20 iron_bars
setblock ~3 ~-08 ~-20 iron_bars
fill ~-3 ~-08 ~-18 ~-3 ~-08 ~-17 air
fill ~3 ~-08 ~-18 ~3 ~-08 ~-17 air

fill ~-2 ~-08 ~-20 ~2 ~-08 ~-20 spruce_slab[type=bottom]


fill ~-3 ~-07 ~-19 ~-3 ~-07 ~-18 iron_bars
fill ~3 ~-07 ~-19 ~3 ~-07 ~-18 iron_bars
setblock ~-3 ~-07 ~-16 air
setblock ~3 ~-07 ~-16 air


setblock ~-3 ~-06 ~-17 iron_bars
setblock ~3 ~-06 ~-17 iron_bars
setblock ~-3 ~-06 ~-14 air
setblock ~3 ~-06 ~-14 air


setblock ~-3 ~-05 ~-15 iron_bars
setblock ~3 ~-05 ~-15 iron_bars

scoreboard players set @s status 16
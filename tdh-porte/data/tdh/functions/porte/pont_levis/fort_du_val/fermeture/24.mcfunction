fill ~-2 ~-18 ~-02 ~2 ~-18 ~-02 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-16 ~-03 ~2 ~-16 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-15 ~-05 ~2 ~-15 ~-05 spruce_stairs[facing=south,half=top]
fill ~-2 ~-15 ~-04 ~2 ~-15 ~-04 spruce_slab[type=double]


fill ~-2 ~-14 ~-06 ~2 ~-14 ~-06 air
fill ~-2 ~-14 ~-04 ~2 ~-14 ~-04 spruce_slab[type=bottom]


fill ~-2 ~-13 ~-06 ~2 ~-13 ~-06 spruce_stairs[facing=south,half=top]
fill ~-2 ~-13 ~-05 ~2 ~-13 ~-05 spruce_slab[type=double]


fill ~-2 ~-12 ~-07 ~2 ~-12 ~-07 air
fill ~-2 ~-12 ~-06 ~2 ~-12 ~-06 spruce_slab[type=double]


fill ~-2 ~-11 ~-08 ~2 ~-11 ~-08 air
fill ~-2 ~-11 ~-06 ~2 ~-11 ~-06 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-10 ~-08 ~2 ~-10 ~-08 spruce_slab[type=top]
fill ~-2 ~-10 ~-07 ~2 ~-10 ~-07 spruce_slab[type=double]


fill ~-2 ~-09 ~-09 ~2 ~-09 ~-09 air
fill ~-2 ~-09 ~-08 ~2 ~-09 ~-08 spruce_slab[type=double]
fill ~-2 ~-09 ~-07 ~2 ~-09 ~-07 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-08 ~-10 ~2 ~-08 ~-10 air
fill ~-2 ~-08 ~-09 ~2 ~-08 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-08 ~-08 ~2 ~-08 ~-08 spruce_slab[type=double]


fill ~-2 ~-07 ~-10 ~2 ~-07 ~-10 air
fill ~-2 ~-07 ~-09 ~2 ~-07 ~-09 spruce_slab[type=double]
fill ~-2 ~-07 ~-08 ~2 ~-07 ~-08 spruce_slab[type=bottom]


fill ~-2 ~-06 ~-11 ~2 ~-06 ~-11 air
fill ~-2 ~-06 ~-10 ~2 ~-06 ~-10 spruce_stairs[facing=south,half=top]
fill ~-2 ~-06 ~-09 ~2 ~-06 ~-09 spruce_slab[type=double]


fill ~-2 ~-05 ~-12 ~2 ~-05 ~-11 air
fill ~-2 ~-05 ~-10 ~2 ~-05 ~-10 spruce_slab[type=double]


fill ~-2 ~-04 ~-12 ~2 ~-04 ~-12 air
fill ~-2 ~-04 ~-11 ~2 ~-04 ~-11 spruce_slab[type=double]
fill ~-2 ~-04 ~-10 ~2 ~-04 ~-10 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-03 ~-13 ~2 ~-03 ~-12 air
fill ~-2 ~-03 ~-11 ~2 ~-03 ~-11 spruce_slab[type=double]


fill ~-3 ~-02 ~-14 ~-3 ~-02 ~-10 air
fill ~3 ~-02 ~-14 ~3 ~-02 ~-10 air

fill ~-2 ~-02 ~-13 ~2 ~-02 ~-13 air
fill ~-2 ~-02 ~-12 ~2 ~-02 ~-12 spruce_stairs[facing=south,half=top]
fill ~-2 ~-02 ~-11 ~2 ~-02 ~-11 spruce_stairs[facing=north,half=bottom]


fill ~-3 ~-01 ~-12 ~-3 ~-01 ~-11 iron_bars
fill ~3 ~-01 ~-12 ~3 ~-01 ~-11 iron_bars
fill ~-3 ~-01 ~-07 ~-3 ~-01 ~-06 air
fill ~3 ~-01 ~-07 ~3 ~-01 ~-06 air

fill ~-2 ~-01 ~-12 ~2 ~-01 ~-12 spruce_slab[type=bottom]
setblock ~-3 ~-01 ~-13 spruce_stairs[facing=south,half=top]
setblock ~3 ~-01 ~-13 spruce_stairs[facing=south,half=top]


fill ~-3 ~-00 ~-08 ~-3 ~-00 ~-07 iron_bars
fill ~3 ~-00 ~-08 ~3 ~-00 ~-07 iron_bars

scoreboard players set @s status 24
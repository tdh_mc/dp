fill ~-2 ~-18 ~-11 ~2 ~-18 ~-11 air
fill ~-2 ~-18 ~-09 ~2 ~-18 ~-09 spruce_slab[type=top]
fill ~-2 ~-18 ~-07 ~2 ~-18 ~-07 spruce_stairs[facing=north,half=bottom]

fill ~-2 ~-17 ~-15 ~2 ~-17 ~-14 air
fill ~-2 ~-17 ~-13 ~2 ~-17 ~-12 spruce_slab[type=top]
fill ~-2 ~-17 ~-11 ~2 ~-17 ~-11 spruce_slab[type=double]
fill ~-2 ~-17 ~-10 ~2 ~-17 ~-10 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-17 ~-09 ~2 ~-17 ~-09 spruce_slab[type=bottom]

fill ~-2 ~-16 ~-19 ~2 ~-16 ~-17 air
fill ~-2 ~-16 ~-16 ~2 ~-16 ~-15 spruce_slab[type=top]
fill ~-2 ~-16 ~-14 ~2 ~-16 ~-14 spruce_slab[type=double]
fill ~-2 ~-16 ~-13 ~2 ~-16 ~-13 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-16 ~-12 ~2 ~-16 ~-12 spruce_slab[type=bottom]

fill ~-3 ~-15 ~-22 ~3 ~-15 ~-22 air
fill ~-2 ~-15 ~-21 ~2 ~-15 ~-20 air
fill ~-2 ~-15 ~-19 ~2 ~-15 ~-18 spruce_slab[type=top]
fill ~-2 ~-15 ~-17 ~2 ~-15 ~-17 spruce_slab[type=double]
fill ~-2 ~-15 ~-16 ~2 ~-15 ~-16 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-15 ~-15 ~2 ~-15 ~-15 spruce_slab[type=bottom]


setblock ~-3 ~-14 ~-21 air
setblock ~3 ~-14 ~-21 air

fill ~-3 ~-14 ~-22 ~3 ~-14 ~-22 spruce_slab[type=top]
fill ~-2 ~-14 ~-21 ~2 ~-14 ~-21 spruce_slab[type=top]
fill ~-2 ~-14 ~-20 ~2 ~-14 ~-20 spruce_slab[type=double]
fill ~-2 ~-14 ~-19 ~2 ~-14 ~-19 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-14 ~-18 ~2 ~-14 ~-18 spruce_slab[type=bottom]


setblock ~-3 ~-13 ~-22 iron_bars
setblock ~3 ~-13 ~-22 iron_bars
setblock ~-3 ~-13 ~-20 air
setblock ~3 ~-13 ~-20 air

fill ~-2 ~-13 ~-22 ~2 ~-13 ~-21 spruce_slab[type=bottom]


setblock ~-3 ~-12 ~-21 iron_bars
setblock ~3 ~-12 ~-21 iron_bars
fill ~-3 ~-12 ~-19 ~-3 ~-12 ~-18 air
fill ~3 ~-12 ~-19 ~3 ~-12 ~-18 air


fill ~-3 ~-11 ~-20 ~-3 ~-11 ~-19 iron_bars
fill ~3 ~-11 ~-20 ~3 ~-11 ~-19 iron_bars
setblock ~-3 ~-11 ~-17 air
setblock ~3 ~-11 ~-17 air


setblock ~-3 ~-10 ~-18 iron_bars
setblock ~3 ~-10 ~-18 iron_bars
setblock ~-3 ~-10 ~-16 air
setblock ~3 ~-10 ~-16 air


setblock ~-3 ~-09 ~-17 iron_bars
setblock ~3 ~-09 ~-17 iron_bars
fill ~-3 ~-09 ~-15 ~-3 ~-09 ~-14 air
fill ~3 ~-09 ~-15 ~3 ~-09 ~-14 air


fill ~-3 ~-08 ~-16 ~-3 ~-08 ~-15 iron_bars
fill ~3 ~-08 ~-16 ~3 ~-08 ~-15 iron_bars
setblock ~-3 ~-08 ~-13 air
setblock ~3 ~-08 ~-13 air


setblock ~-3 ~-07 ~-14 iron_bars
setblock ~3 ~-07 ~-14 iron_bars
setblock ~-3 ~-07 ~-12 air
setblock ~3 ~-07 ~-12 air


setblock ~-3 ~-06 ~-13 iron_bars
setblock ~3 ~-06 ~-13 iron_bars
setblock ~-3 ~-06 ~-11 air
setblock ~3 ~-06 ~-11 air


setblock ~-3 ~-05 ~-12 iron_bars
setblock ~3 ~-05 ~-12 iron_bars
setblock ~-3 ~-05 ~-09 air
setblock ~3 ~-05 ~-09 air


setblock ~-3 ~-04 ~-10 iron_bars
setblock ~3 ~-04 ~-10 iron_bars
setblock ~-3 ~-04 ~-08 air
setblock ~3 ~-04 ~-08 air


setblock ~-3 ~-03 ~-09 iron_bars
setblock ~3 ~-03 ~-09 iron_bars


setblock ~-3 ~-01 ~-04 air
setblock ~3 ~-01 ~-04 air


setblock ~-3 ~-00 ~-05 iron_bars
setblock ~3 ~-00 ~-05 iron_bars



setblock ~-3 ~0 ~3 iron_bars
setblock ~-3 ~0 ~4 iron_block
setblock ~3 ~0 ~3 iron_bars
setblock ~3 ~0 ~4 iron_block

scoreboard players set @s status 11
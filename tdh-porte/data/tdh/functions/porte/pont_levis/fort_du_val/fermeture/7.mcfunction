fill ~-2 ~-20 ~-08 ~2 ~-20 ~-07 air


fill ~-3 ~-19 ~-23 ~3 ~-19 ~-23 air
fill ~-2 ~-19 ~-22 ~2 ~-19 ~-16 air
fill ~-2 ~-19 ~-15 ~2 ~-19 ~-11 spruce_slab[type=top]
fill ~-2 ~-19 ~-08 ~2 ~-19 ~-07 spruce_slab[type=double]
fill ~-2 ~-19 ~-06 ~2 ~-19 ~-06 spruce_stairs[facing=north,half=bottom]


setblock ~-3 ~-18 ~-22 air
setblock ~3 ~-18 ~-22 air

fill ~-3 ~-18 ~-23 ~3 ~-18 ~-23 spruce_slab[type=top]
fill ~-2 ~-18 ~-22 ~2 ~-18 ~-20 spruce_slab[type=top]
fill ~-2 ~-18 ~-19 ~2 ~-18 ~-16 spruce_slab[type=double]
fill ~-2 ~-18 ~-15 ~2 ~-18 ~-15 spruce_stairs[facing=north,half=bottom]
fill ~-2 ~-18 ~-14 ~2 ~-18 ~-11 spruce_slab[type=bottom]


setblock ~-3 ~-17 ~-23 iron_bars
setblock ~3 ~-17 ~-23 iron_bars
setblock ~-3 ~-17 ~-21 air
setblock ~3 ~-17 ~-21 air

fill ~-2 ~-17 ~-23 ~2 ~-17 ~-20 spruce_slab[type=bottom]


setblock ~-3 ~-16 ~-22 iron_bars
setblock ~3 ~-16 ~-22 iron_bars
setblock ~-3 ~-16 ~-20 air
setblock ~3 ~-16 ~-20 air


setblock ~-3 ~-15 ~-21 iron_bars
setblock ~3 ~-15 ~-21 iron_bars
setblock ~-3 ~-15 ~-19 air
setblock ~3 ~-15 ~-19 air


setblock ~-3 ~-14 ~-20 iron_bars
setblock ~3 ~-14 ~-20 iron_bars
setblock ~-3 ~-14 ~-18 air
setblock ~3 ~-14 ~-18 air


setblock ~-3 ~-13 ~-19 iron_bars
setblock ~3 ~-13 ~-19 iron_bars
setblock ~-3 ~-13 ~-17 air
setblock ~3 ~-13 ~-17 air


setblock ~-3 ~-12 ~-18 iron_bars
setblock ~3 ~-12 ~-18 iron_bars
setblock ~-3 ~-12 ~-16 air
setblock ~3 ~-12 ~-16 air


setblock ~-3 ~-11 ~-17 iron_bars
setblock ~3 ~-11 ~-17 iron_bars


setblock ~-3 ~-08 ~-11 air
setblock ~3 ~-08 ~-11 air


setblock ~-3 ~-07 ~-12 iron_bars
setblock ~3 ~-07 ~-12 iron_bars
setblock ~-3 ~-07 ~-10 air
setblock ~3 ~-07 ~-10 air


setblock ~-3 ~-06 ~-11 iron_bars
setblock ~3 ~-06 ~-11 iron_bars
setblock ~-3 ~-06 ~-09 air
setblock ~3 ~-06 ~-09 air


setblock ~-3 ~-05 ~-10 iron_bars
setblock ~3 ~-05 ~-10 iron_bars



setblock ~-3 ~0 ~1 iron_bars
setblock ~-3 ~0 ~2 iron_block
setblock ~3 ~0 ~1 iron_bars
setblock ~3 ~0 ~2 iron_block

scoreboard players set @s status 7
fill ~-2 ~-16 ~-04 ~2 ~-16 ~-04 air


fill ~-2 ~-15 ~-04 ~2 ~-15 ~-04 spruce_stairs[facing=south,half=top]
fill ~-2 ~-15 ~-03 ~2 ~-15 ~-03 spruce_slab[type=double]


fill ~-2 ~-14 ~-05 ~2 ~-14 ~-05 air
fill ~-2 ~-14 ~-03 ~2 ~-14 ~-03 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-13 ~-05 ~2 ~-13 ~-05 air
fill ~-2 ~-13 ~-04 ~2 ~-13 ~-04 spruce_slab[type=double]


fill ~-2 ~-12 ~-06 ~2 ~-12 ~-06 air
fill ~-2 ~-12 ~-04 ~2 ~-12 ~-04 spruce_slab[type=double]


fill ~-2 ~-11 ~-06 ~2 ~-11 ~-06 air
fill ~-2 ~-11 ~-05 ~2 ~-11 ~-05 spruce_slab[type=double]


fill ~-2 ~-10 ~-07 ~2 ~-10 ~-07 air
fill ~-2 ~-10 ~-06 ~2 ~-10 ~-06 spruce_stairs[facing=south,half=top]
fill ~-2 ~-10 ~-05 ~2 ~-10 ~-05 spruce_slab[type=double]


fill ~-2 ~-09 ~-07 ~2 ~-09 ~-07 air
fill ~-2 ~-09 ~-06 ~2 ~-09 ~-06 spruce_slab[type=double]
fill ~-2 ~-09 ~-05 ~2 ~-09 ~-05 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-08 ~-08 ~2 ~-08 ~-07 air
fill ~-2 ~-08 ~-06 ~2 ~-08 ~-06 spruce_slab[type=double]


fill ~-2 ~-07 ~-08 ~2 ~-07 ~-08 air
fill ~-2 ~-07 ~-07 ~2 ~-07 ~-07 spruce_stairs[facing=south,half=top]
fill ~-2 ~-07 ~-06 ~2 ~-07 ~-06 spruce_slab[type=double]


fill ~-2 ~-06 ~-09 ~2 ~-06 ~-08 air
fill ~-2 ~-06 ~-07 ~2 ~-06 ~-07 spruce_slab[type=double]


fill ~-2 ~-05 ~-09 ~2 ~-05 ~-09 air
fill ~-2 ~-05 ~-08 ~2 ~-05 ~-08 spruce_slab[type=top]
fill ~-2 ~-05 ~-07 ~2 ~-05 ~-07 spruce_slab[type=double]


fill ~-2 ~-04 ~-09 ~2 ~-04 ~-09 air
fill ~-2 ~-04 ~-08 ~2 ~-04 ~-08 spruce_slab[type=double]
fill ~-2 ~-04 ~-07 ~2 ~-04 ~-07 spruce_stairs[facing=north,half=bottom]


fill ~-2 ~-03 ~-10 ~2 ~-03 ~-09 air
fill ~-2 ~-03 ~-08 ~2 ~-03 ~-08 spruce_slab[type=double]


fill ~-2 ~-02 ~-10 ~2 ~-02 ~-10 air
fill ~-2 ~-02 ~-09 ~2 ~-02 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-02 ~-08 ~2 ~-02 ~-08 spruce_slab[type=double]


fill ~-3 ~-01 ~-10 ~-3 ~-01 ~-07 air
fill ~3 ~-01 ~-10 ~3 ~-01 ~-07 air

fill ~-2 ~-01 ~-10 ~2 ~-01 ~-10 air
fill ~-2 ~-01 ~-09 ~2 ~-01 ~-09 spruce_slab[type=double]


setblock ~-3 ~-00 ~-08 iron_bars
setblock ~3 ~-00 ~-08 iron_bars

setblock ~-3 ~-00 ~-09 spruce_stairs[facing=south,half=top]
setblock ~3 ~-00 ~-09 spruce_stairs[facing=south,half=top]
fill ~-2 ~-00 ~-09 ~2 ~-00 ~-09 spruce_slab[type=double]



setblock ~-3 ~0 ~13 iron_bars
setblock ~-3 ~0 ~14 iron_block
setblock ~3 ~0 ~13 iron_bars
setblock ~3 ~0 ~14 iron_block

scoreboard players set @s status 28
# Cette fonction est exécutée par une armor stand "Porte" "PontLevisFDV" à sa position,
# pour effacer tous les blocs résiduels des différentes frames du pont

# On a d'abord un gros fill qui concerne la zone où il n'y a rien d'autre que le pont-levis
fill ~-3 ~-24 ~-22 ~3 ~-22 ~-4 air
fill ~-3 ~-21 ~-23 ~3 ~0 ~-4 air

# Ensuite, on efface les éventuels blocs restants
# Les 2 extrémités des câbles (contre le mur)
fill ~-3 ~0 ~-3 ~-3 ~0 ~-2 air
fill ~3 ~0 ~-3 ~3 ~0 ~-2 air

# La partie du pont-levis collée au mur
fill ~-2 ~-20 ~-3 ~2 ~0 ~-2 air

# La partie du pont-levis collée à la route
fill ~-3 ~-25 ~-22 ~3 ~-25 ~-22 air
# Cette fonction est exécutée par une armor stand "Porte" "PontLevisFDV" à sa position,
# pour définir instantanément l'état du pont-levis à "fermé"

# On vide d'abord les blocs éventuellement présents venant de n'importe quelle frame
function tdh:porte/fort_du_val/pont_levis/supprimer


# On pose ensuite les blocs
# On commence par les "attaches" au sommet (aux extrémités des câbles)
setblock ~-3 ~0 ~-2 spruce_stairs[facing=south,half=top]
setblock ~3 ~0 ~-2 spruce_stairs[facing=south,half=top]

# On pose ensuite le pont couche par couche, de bas en haut
fill ~-2 ~-20 ~-2 ~2 ~-20 ~-2 spruce_slab[type=bottom]
fill ~-2 ~-19 ~-2 ~2 ~-00 ~-2 spruce_slab[type=double]


# On pose enfin les câbles à l'état "fermé" dans la salle des machines
fill ~-3 ~0 ~-1 ~-3 ~0 ~20 iron_bars
fill ~3 ~0 ~-1 ~3 ~0 ~20 iron_bars